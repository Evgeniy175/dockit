module.exports = {
  __API_BASE__: JSON.stringify('https://api.dockit.me'),
  __APP_BASE__: JSON.stringify('https://dockitcalendar.com'),
  'process.env': {
    NODE_ENV: JSON.stringify('production')
  }
};
