module.exports = {
  __API_BASE__: JSON.stringify('http://dockit-staging-api.us-west-2.elasticbeanstalk.com'),
  __APP_BASE__: JSON.stringify('http://localhost:8080'),
  'process.env': {
    NODE_ENV: JSON.stringify('staging')
  }
};
