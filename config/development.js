module.exports = {
  __API_BASE__: JSON.stringify('http://localhost:3000'),
  __APP_BASE__: JSON.stringify('http://localhost:8080'),
  'process.env': {
    NODE_ENV: JSON.stringify('development')
  }
};
