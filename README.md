# DockIt Web Client

## Install

Run `npm install` to install project dependencies.

## Build

Run `npm run build` to build the project for production.<br />
Run `npm run build-staging` to build the project for staging.<br />
Run `npm run build-dev` to build the project for development.

## Run

Run `npm run serve` for launch app. Will points to the production API without uglify.<br />
Run `npm run serve-prod` for launch app. Will points to the production API with uglify.<br />
Run `npm run serve-dev` for launch app. Will points to the localhost API.<br />
Run `npm run serve-staging` Will points to the staging API.

## Using

`http://localhost:8080/` for open DockIt web client if it run locally.<br />
`https://dockitcalendar.com/` deployed production.
