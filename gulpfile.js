const argv = require('yargs').argv;
const gulp = require('gulp');
const awspublish = require('gulp-awspublish');
const run = require('gulp-run');

const REGION = 'us-west-2';

const BUCKETS = {
  PRODUCTION: 'dockitcalendar.com',
  STAGING: 'dockit-web-client-staging',
};

gulp.task('build', () => run('npm run build').exec());
gulp.task('publish', () => doPublish(BUCKETS.PRODUCTION));

gulp.task('build-staging', () => run('npm run build-staging').exec());
gulp.task('publish-staging', () => doPublish(BUCKETS.STAGING));

function doPublish(bucket) {
  const headers = {
    'Cache-Control': 'max-age=10080, no-transform, public'
  };
  const publisher = getPublisher(bucket);
  
  return gulp.src('./dist/**')
  .pipe(awspublish.gzip())
  .pipe(publisher.publish(headers))
  .pipe(publisher.sync())
  .pipe(awspublish.reporter());
}

function getPublisher(bucket) {
  const accessKeyId = argv.accessKeyId;
  const secretAccessKey = argv.secretAccessKey;
  
  return awspublish.create({
    region: REGION,
    params: {
      Bucket: bucket
    },
    accessKeyId,
    secretAccessKey
  });
}
