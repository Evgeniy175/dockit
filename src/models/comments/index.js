import {BaseModel} from '../base';
import {ENDPOINT, TARGET, TARGETS, MODEL_PLURAL, DEFAULT_COMMENT_IMAGE_RESOLUTION} from './constants';
import {getImageUrl, getImageSrcSetItem} from '../../utils/formatting/image';

import {getResolution} from '../../utils/images/resolution';



export default class CommentModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  static getTarget(comment = {}) {
    for (const key of TARGETS)
      if (comment[key])
        return key;
  }

  static getTargetThrowId(comment = {}) {
    for (const key of TARGETS)
      if (comment[`${key}_id`])
        return key;
  }
  
  fetchComment(commentId, config = {}) {
    config.url = ENDPOINT.FETCH_COMMENT(commentId);
    return this.get(config);
  }

  fetchCommentAnswers(commentId, config = {}) {
    config.url = ENDPOINT.FETCH_COMMENT_ANSWERS(commentId);
    return this.get(config);
  }

  fetchEventComments(eventId, config = {}) {
    config.url = ENDPOINT.FETCH_COMMENTS(TARGET.EVENT, eventId);
    return this.get(config);
  }
  
  fetchScheduleComments(scheduleId, config = {}) {
    config.url = ENDPOINT.FETCH_COMMENTS(TARGET.SCHEDULE, scheduleId);
    return this.get(config);
  }

  attachPictureToComment(commentId, config = {}) {
    config.url = ENDPOINT.ATTACH_PICTURE_TO_COMMENT(commentId);
    config.headers = { };
    return this.create(config);
  }

  static formatImageUrl(imageKey, resolution = DEFAULT_COMMENT_IMAGE_RESOLUTION) {
    if (!imageKey) return null;
    return getImageUrl(MODEL_PLURAL, imageKey, getResolution(resolution));
  }

  static getSrcSetData(imageKey, resolutions = []) {
    return imageKey
    ? resolutions.map(width => `${getImageSrcSetItem(MODEL_PLURAL, imageKey, width)} ${width}w`).join(',')
    : null;
  }

  addCommentAnswer(commentId, text) {
    let config = { body: JSON.stringify({message: text}) };
    config.url = ENDPOINT.ADD_ANSWER(commentId);
    return this.create(config);
  }

  deleteComment(commentId, config = {}) {
    config.url = ENDPOINT.DELETE(commentId);
    return this.delete(config);
  }

  editComment(commentId, text) {
    let config = {
      body: JSON.stringify({ message: text }),
      url: ENDPOINT.UPDATE_COMMENT(commentId)
    };
    return this.update(config);
  }

  addEventComment(eventId, message) {
    return this.addComment(TARGET.EVENT, eventId, message);
  }

  addScheduleComment(scheduleId, message) {
    return this.addComment(TARGET.SCHEDULE, scheduleId, message);
  }

  addCommentToUser(userId, message) {
    return this.addComment(TARGET.USER, userId, message);
  }

  addComment(target, targetId, message) {
    let config = {
      body: JSON.stringify({ message }),
      url: ENDPOINT.ADD_COMMENT(target, targetId)
    };
    return this.create(config);
  }

  addCommentToComment(targetId, message) {
    let config = {
      body: JSON.stringify({ message }),
      url: ENDPOINT.ADD_COMMENT_TO_COMMENT(targetId)
    };
    return this.create(config);
  }
}
