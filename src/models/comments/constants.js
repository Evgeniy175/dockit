import {API_BASE} from '../../constants';

import {MODEL as EVENT_MODEL, MODEL_PLURAL as EVENT_MODEL_PLURAL} from '../events/constants';
import {MODEL as SCHEDULE_MODEL, MODEL_PLURAL as SCHEDULE_MODEL_PLURAL} from '../schedules/constants';
import {MODEL as USER_MODEL, MODEL_PLURAL as USER_MODEL_PLURAL} from '../users/constants';

import {DEFAULT_RESOLUTION} from '../../utils/images/resolution/constants';

const IMAGES_SERVER_URL = 'https://dockit-comment-images.s3.amazonaws.com';

export const MODEL = 'comment';
export const MODEL_PLURAL = 'comments';

export const TARGET = Object.freeze({
  COMMENT: MODEL_PLURAL,
  EVENT: EVENT_MODEL_PLURAL,
  SCHEDULE: SCHEDULE_MODEL_PLURAL,
  USER: USER_MODEL_PLURAL,
});

export const TARGETS = [
  EVENT_MODEL,
  SCHEDULE_MODEL,
  USER_MODEL,
];

export const ENDPOINT = Object.freeze({
  LIST: () => `${API_BASE}/${MODEL_PLURAL}`,
  DELETE: id => `${API_BASE}/${MODEL_PLURAL}/${id}`,
  FETCH_COMMENTS: (target, id) => `${API_BASE}/${MODEL_PLURAL}/${target}/${id}/decorated`,
  FETCH_COMMENT: id => `${API_BASE}/${MODEL_PLURAL}/${id}/decorated`,
  FETCH_COMMENT_ANSWERS: id => `${API_BASE}/${MODEL_PLURAL}/${id}/${MODEL_PLURAL}/decorated`,
  ADD_COMMENT:  (target, id) => `${API_BASE}/${MODEL_PLURAL}/${target}/${id}`,
  ADD_COMMENT_TO_COMMENT:  id => `${API_BASE}/${MODEL_PLURAL}/${id}/${MODEL_PLURAL}`,
  UPDATE_COMMENT:  id => `${API_BASE}/${MODEL_PLURAL}/${id}`,
  DECORATED: (id) =>`${API_BASE}/${MODEL_PLURAL}/${id}/decorated`,
  ADD_ANSWER:  (commentId) => `${API_BASE}/${MODEL_PLURAL}/${commentId}/comments`,
  LIST_BY_EVENT: (eventId) => `${API_BASE}/${MODEL_PLURAL}/${EVENT_MODEL_PLURAL}/${eventId}/decorated`,
  ATTACH_PICTURE_TO_COMMENT: (commentId) => `${API_BASE}/${MODEL_PLURAL}/${commentId}/image`,
  ATTACHED_IMAGE_URL: imageKey => `${IMAGES_SERVER_URL}/${imageKey}`,
});

export const DEFAULT_COMMENT_IMAGE_RESOLUTION = DEFAULT_RESOLUTION;
