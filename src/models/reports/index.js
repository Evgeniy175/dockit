import { BaseModel } from '../base';
import { ENDPOINT, TARGET } from './constants';


export default class ReportModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  addEventReport(eventId, text, config = {}) {
    Object.assign(config, {
      body: JSON.stringify({reason: text}),
      url: ENDPOINT.ADD_REPORT(TARGET.EVENT, eventId)
    });

    return this.create(config);
  }

  addCommentReport(commentId, text, config = {}) {
    Object.assign(config, {
      body: JSON.stringify({reason: text}),
      url: ENDPOINT.ADD_REPORT(TARGET.COMMENT, commentId)
    });

    return this.create(config);
  }
  
  addScheduleReport(scheduleId, text, config = {}) {
    Object.assign(config, {
      body: JSON.stringify({reason: text}),
      url: ENDPOINT.ADD_REPORT(TARGET.SCHEDULE, scheduleId)
    });

    return this.create(config);
  }
}
