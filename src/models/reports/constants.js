import { API_BASE } from '../../constants';

import { MODEL_PLURAL as COMMENT_MODEL_PLURAL } from '../comments/constants';
import { MODEL_PLURAL as EVENT_MODEL_PLURAL } from '../events/constants';
import { MODEL_PLURAL as SCHEDULE_MODEL_PLURAL } from '../schedules/constants';

export const MODEL = 'reports';
export const MODEL_PLURAL = 'content-flags';

export const TARGET = Object.freeze({
  COMMENT: COMMENT_MODEL_PLURAL,
  EVENT: EVENT_MODEL_PLURAL,
  SCHEDULE: SCHEDULE_MODEL_PLURAL
});

export const ENDPOINT = Object.freeze({
  ADD_REPORT: (parent, referenceId) => `${API_BASE}/${MODEL_PLURAL}/${parent}/${referenceId}`,
});
