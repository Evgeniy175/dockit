import {BaseModel, p} from '../base';
import {ENDPOINT} from './constants';

/**
 *
 */
export default class NotableModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  fetchFeed(config = {}) {
    config.url = ENDPOINT.FEED();
    return this.get(config);
  }
  
  fetchProfileFeed(userId, config = {}) {
    config.url = ENDPOINT.FEED_FOR_USER(userId);
    return this.list(config);
  }
}
