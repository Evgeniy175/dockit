import { API_BASE } from '../../constants';

export const MODEL = 'notable';
export const MODEL_PLURAL = 'notables';

export const ENDPOINT = Object.freeze({
  LIST: () => `${API_BASE}/${MODEL_PLURAL}`,
  FEED: () => `${API_BASE}/feed`,
  FEED_FOR_USER: (userId) => `${API_BASE}/${MODEL_PLURAL}/users/${userId}`
});