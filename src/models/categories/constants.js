import { API_BASE } from '../../constants';
import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../schedules/constants';

export const MODEL = 'category';
export const MODEL_PLURAL = 'categories';

export const TARGETS = {
  EVENTS: EVENTS_MODEL_PLURAL,
  SCHEDULES: SCHEDULES_MODEL_PLURAL
};

export const ENDPOINT = Object.freeze({
  ADD: (target, id) => `${API_BASE}/${MODEL_PLURAL}/${target}/${id}`,
  GET: (target, id) => `${API_BASE}/${MODEL_PLURAL}`
});
