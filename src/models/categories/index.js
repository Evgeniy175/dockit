import { BaseModel } from '../base';
import { ENDPOINT, TARGETS } from './constants';

/**
 *
 */
export default class CategoriesModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }
  
  fetch(config = {}) {
    config.url = ENDPOINT.GET();
    return this.get(config);
  }

  addTo(target, id, categoriesIds) {
    let config = { body: JSON.stringify({ ids: categoriesIds })};
    config.url = ENDPOINT.ADD(target, id);
    return this.update(config);
  }

  addToEvent(id, categoriesIds) {
    let config = { body: JSON.stringify({ ids: categoriesIds })};
    config.url = ENDPOINT.ADD(TARGETS.EVENTS, id);
    return this.update(config);
  }

  addToSchedule(id, categoriesIds) {
    let config = { body: JSON.stringify({ ids: categoriesIds })};
    config.url = ENDPOINT.ADD(TARGETS.SCHEDULES, id);
    return this.update(config);
  }
}
