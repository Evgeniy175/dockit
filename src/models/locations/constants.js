import { API_BASE } from '../../constants';

export const MODEL = 'location';
export const MODEL_PLURAL = 'locations';

export const ENDPOINT = Object.freeze({
  FETCH_BY_ID: eventId => `${API_BASE}/${MODEL_PLURAL}/${locationId}`,
  FETCH_BY_IP: () => `${API_BASE}/${MODEL_PLURAL}/by-ip`,
  FETCH_BY_PLACE_ID: () => `${API_BASE}/${MODEL_PLURAL}/by-placeid`,
  PLACE_AUTOCOMPLETE: () => `${API_BASE}/${MODEL_PLURAL}/place-autocomplete`,
  TIMEZONE_BY_COORDS: () => `${API_BASE}/${MODEL_PLURAL}/timezone-by-coords`,
});

export const PLACE_RADIUS = 40000;
