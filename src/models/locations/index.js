import { BaseModel } from '../base';
import { ENDPOINT } from './constants';

/**
 *
 */
export default class LocationModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  fetchLocationById(locationId, config = {}) {
    config.url = ENDPOINT.FETCH_BY_ID(locationId);
    return this.get(config);
  }

  fetchLocationByIp(config = {}) {
    config.url = ENDPOINT.FETCH_BY_IP();
    return this.get(config);
  }

  fetchLocationByPlaceId(placeId, config = {}) {
    config.url = ENDPOINT.FETCH_BY_PLACE_ID();
    config.params = {
      place_id: placeId
    };
    return this.get(config);
  }

  getPlaceAutocomplete(locationName, config = {}) {
    config.url = ENDPOINT.PLACE_AUTOCOMPLETE() + `/${locationName}`;
    return this.get(config);
  }

  getTimezoneByCoords(config = {}) {
    config.url = ENDPOINT.TIMEZONE_BY_COORDS();
    return this.get(config);
  }
}
