import {http} from '../http';

import { CONTENT_TYPE } from './constants';

// This establishes a private namespace.
const namespace = new WeakMap();
export function p(object) {
  if (!namespace.has(object)) namespace.set(object, {});
  return namespace.get(object);
}

/**
 *
 */
export class BaseModel {
  constructor(config = {}) {
    p(this).endpoint = Object.assign({}, config.endpoint || {});
    p(this).endpoint.get = config.endpoint.get || config.endpoint.GET;
    p(this).endpoint.list = config.endpoint.list || config.endpoint.LIST;
    p(this).endpoint.create = config.endpoint.create || config.endpoint.CREATE;
    p(this).endpoint.update = config.endpoint.update || config.endpoint.UPDATE;
  }

  get(config = {}) {
    config.method = 'GET';
    config.url = config.url || p(this).endpoint.get(config);
    return http.get(config);
  }

  create(config = {}) {
    config.method = 'POST';
    config.url = config.url || p(this).endpoint.create(config);
    config.headers = config.headers || CONTENT_TYPE;
    return http.post(config);
  }

  update(config = {}) {
    config.method = 'PUT';
    config.url = config.url || p(this).endpoint.update(config);
    config.headers = config.headers || CONTENT_TYPE;
    return http.put(config);
  }

  list(config = {}) {
    config.method = 'GET';
    config.url = config.url || p(this).endpoint.list(config);
    return http.get(config);
  }

  delete(config = {}) {
    config.method = 'DELETE';
    config.url = config.url || p(this).endpoint.delete(config);
    return http.delete(config);
  }
}