import {API_BASE} from '../../constants';

export const MODEL = 'invite';
export const MODEL_PLURAL = 'invites';

export const ENDPOINT = Object.freeze({
  SEND_EVENT_INVITE: (eventId, userId) => `${API_BASE}/${MODEL_PLURAL}/events/${eventId}/users/${userId}`,
  SEND_SCHEDULE_INVITE: (scheduleId, userId) => `${API_BASE}/${MODEL_PLURAL}/schedules/${scheduleId}/users/${userId}`
});

