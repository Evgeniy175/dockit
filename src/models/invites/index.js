import {BaseModel, p} from '../base';
import {ENDPOINT, MODEL_PLURAL} from './constants';
import {Auth} from '../../auth';
import queryString from 'query-string';



export default class LocationModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }
  
  sendEventInvite(eventId, userId, config = {}) {
    config.url = ENDPOINT.SEND_EVENT_INVITE(eventId, userId);
    return this.create(config);
  }
  
  sendScheduleInvite(scheduleId, userId, config = {}) {
    config.url = ENDPOINT.SEND_SCHEDULE_INVITE(scheduleId, userId);
    return this.create(config);
  }
}
