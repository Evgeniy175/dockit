import { BaseModel, p } from '../base';
import { ENDPOINT, MODEL_PLURAL } from './constants';



export default class SavesModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  save(config = {}) {
    config.url = ENDPOINT.SAVE(config);
    return this.create(config);
  }

  deleteSave(config = {}) {
    config.url = ENDPOINT.DELETE_SAVE(config);
    return this.delete(config);
  }
}
