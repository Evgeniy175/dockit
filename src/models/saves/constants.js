import { API_BASE } from '../../constants';
import { get } from 'lodash';

export const MODEL = 'save';
export const MODEL_PLURAL = 'saves';

export const ENDPOINT = Object.freeze({
  SAVE: config => `${API_BASE}/${MODEL_PLURAL}/${get(config, 'routing.target')}/${get(config, 'routing.id')}`,
  DELETE_SAVE: config => `${API_BASE}/${MODEL_PLURAL}/${get(config, 'routing.target')}/${get(config, 'routing.id')}`,
});