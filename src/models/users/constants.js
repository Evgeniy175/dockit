import { API_BASE } from '../../constants';
import {DEFAULT_RESOLUTION} from '../../utils/images/resolution/constants';

const PROFILE_IMAGES_SERVER_URL = "https://dockit-profile-images.s3.amazonaws.com";
const COVER_IMAGES_SERVER_URL = "https://s3.amazonaws.com/dockit-cover-images";

export const MODEL = 'user';
export const MODEL_PLURAL = 'users';

export const PROFILE_PIC_SECTION_NAME = 'users-profile';
export const COVER_PIC_SECTION_NAME = 'users-cover';

export const ENDPOINT = Object.freeze({
  CREATE: () => `${API_BASE}/${MODEL_PLURAL}`,
  LOGIN: () => `${API_BASE}/${MODEL_PLURAL}/login`,
  DELETE: () => `${API_BASE}/${MODEL_PLURAL}`,
  PROFILE: () => `${API_BASE}/${MODEL_PLURAL}/current`,
  OTHER_PROFILE: id => `${API_BASE}/${MODEL_PLURAL}/${id}/decorated`,
  FOLLOWERS: userId => `${API_BASE}/${MODEL_PLURAL}/${userId}/followers/decorated`,
  FOLLOWINGS: userId => `${API_BASE}/${MODEL_PLURAL}/${userId}/followings/decorated`,
  UPDATE: () => `${API_BASE}/${MODEL_PLURAL}`,
  LOGOUT: () => `${API_BASE}/${MODEL_PLURAL}/logout`,
  FORGOT_PASSWORD: () => `${API_BASE}/${MODEL_PLURAL}/forgot-password`,
  RESET_PASSWORD: () => `${API_BASE}/${MODEL_PLURAL}/reset-password`,
  COVER_PHOTO: imageKey => `${COVER_IMAGES_SERVER_URL}/${imageKey}`,
  PROFILE_PHOTO: imageKey => `${PROFILE_IMAGES_SERVER_URL}/${imageKey}`,
  UPDATE_PROFILE_PHOTO: () => `${API_BASE}/${MODEL_PLURAL}/image/profile`,
  UPDATE_PROFILE_COVER: () => `${API_BASE}/${MODEL_PLURAL}/image/cover`,
  FOLLOWINGS_WHO_INTENDS: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/followings-who-intends/${id}`,
  NONFOLLOWINGS_WHO_INTENDS: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/nonfollowings-who-intends/${id}`,
  FOLLOWINGS_WHO_SAVED: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/followings-who-saved/${id}`,
  NONFOLLOWINGS_WHO_SAVED: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/nonfollowings-who-saved/${id}`,
  FOLLOWINGS_WHO_INVITED: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/followings-who-invited/${id}`,
  NONFOLLOWINGS_WHO_INVITED: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/nonfollowings-who-invited/${id}`,
  FOLLOWINGS_WHO_LIKED: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/followings-who-liked/${id}`,
  NONFOLLOWINGS_WHO_LIKED: (targetType, id) => `${API_BASE}/${MODEL_PLURAL}/${targetType}/nonfollowings-who-liked/${id}`,
  REMOVE_LOCATION: () => `${API_BASE}/${MODEL_PLURAL}/locations`,
  DELETE_PROFILE_PHOTO: () => `${API_BASE}/${MODEL_PLURAL}/image/profile`,
  DELETE_PROFILE_COVER: () => `${API_BASE}/${MODEL_PLURAL}/image/cover`,
  SEARCH: () => `${API_BASE}/${MODEL_PLURAL}/decorated`,
  GOOGLE_CALENDAR_SIGN_OUT: scheduleId => `${API_BASE}/${MODEL_PLURAL}/google-disconnecting`,
  FETCH_INVITED_PEOPLE: (target, id) => `${API_BASE}/${MODEL_PLURAL}/${target}/${id}/invited`,
  ALERTS: () => `${API_BASE}/${MODEL_PLURAL}/alerts`,
});

export const DEFAULT_PROFILE_IMAGE_RESOLUTION = DEFAULT_RESOLUTION;
