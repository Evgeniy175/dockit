import { BaseModel } from '../base';
import { ENDPOINT, DEFAULT_PROFILE_IMAGE_RESOLUTION, PROFILE_PIC_SECTION_NAME, COVER_PIC_SECTION_NAME } from './constants';
import { HOME_PATH } from '../../constants';
import { Auth } from '../../auth';
import { getImageUrl, getImageSrcSetItem } from '../../utils/formatting/image';
import deepAssign from 'deep-assign';

import DEFAULT_PROFILE_PICTURE from '../../assets/images/default-images/profile-picture.png';

import {setLocation} from '../../utils/dom/window';
import {getResolution} from '../../utils/images/resolution';



export default class UsersModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }
  
  createUser(config = {}) {
    config.url = ENDPOINT.CREATE();
    return this.create(config);
  }
  
  deleteUser(config = {}) {
    config.url = ENDPOINT.DELETE();
    return this.delete(config)
    .then(() => {
      Auth.removeActiveToken();
      Auth.removeActiveUser();
      setLocation(HOME_PATH);
      return Promise.resolve();
    });
  }

  login(config = {}) {
    config.url = ENDPOINT.LOGIN();
    return this.create(config);
  }
  
  fetchProfileInfo(config = {}) {
    config.url = ENDPOINT.PROFILE();
    return this.get(config);
  }
  
  fetchProfileById(id, config = {}) {
    config.url = ENDPOINT.OTHER_PROFILE(id);
    return this.get(config);
  }
  
  static formatImageUrl(imageKey, resolution = DEFAULT_PROFILE_IMAGE_RESOLUTION, defaultImage = DEFAULT_PROFILE_PICTURE) {
    if (!imageKey) return defaultImage;
    return getImageUrl(PROFILE_PIC_SECTION_NAME, imageKey, getResolution(resolution));
  }

  static getSrcSetData(imageKey, resolutions = []) {
    return imageKey
    ? resolutions.map(width => `${getImageSrcSetItem(PROFILE_PIC_SECTION_NAME, imageKey, width)} ${width}w`).join(',')
    : null;
  }

  formatProfileCoverUrl(imageKey) {
    if (!imageKey) return null;
    return getImageUrl(COVER_PIC_SECTION_NAME, imageKey, getResolution(screen.height));
  }

  formatUpdateProfilePhotoUrl(){
    return ENDPOINT.UPDATE_PROFILE_PHOTO();
  }

  fetchFollowers(userId, limit, page, config = {}) {
    const defaultConfig = {
      params: {
        limit,
        page,
        order: [
          'name',
          'asc'
        ],
      },
      url: ENDPOINT.FOLLOWERS(userId),
    };

    config = deepAssign({}, defaultConfig, config);
    return this.get(config);
  }

  fetchFollowings(userId, limit, page, config = {}){
    const defaultConfig = {
      params: {
        limit: limit,
        page: page,
        order: [
          'name',
          'asc'
        ],
      },
      url: ENDPOINT.FOLLOWINGS(userId),
    };

    config = deepAssign({}, defaultConfig, config);
    return this.get(config);
  }

  fetchFollowingsByConfig(userId, config = {}){
    const defaultConfig = {
      params: {
        order: [
          'name',
          'asc'
        ],
      },
      url: ENDPOINT.FOLLOWINGS(userId),
    };

    config = deepAssign({}, defaultConfig, config);
    return this.get(config);
  }

  updateProfile(config = {}){
    config.url = ENDPOINT.UPDATE();
    return this.update(config);
  }
  
  updateProfileCover(config = {}) {
    config.url = ENDPOINT.UPDATE_PROFILE_COVER();
    config.headers = { };
    return this.create(config);
  }
  
  updateProfilePhoto(config = {}){
    config.url = ENDPOINT.UPDATE_PROFILE_PHOTO();
    config.headers = { };
    return this.create(config);
  }

  deleteProfileCover(config = {}){
    config.url = ENDPOINT.DELETE_PROFILE_COVER();
    return this.delete(config);
  }

  deleteProfilePhoto(config = {}){
    config.url = ENDPOINT.DELETE_PROFILE_PHOTO();
    return this.delete(config);
  }

  logout(config = {}) {
    config.url = ENDPOINT.LOGOUT();
    config.fullResponse = true;
    return this.create(config);
  }

  forgotPassword(config = {}) {
    config.url = ENDPOINT.FORGOT_PASSWORD();
    config.fullResponse = true;
    return this.create(config);
  }

  resetPassword(config = {}) {
    config.url = ENDPOINT.RESET_PASSWORD();
    config.fullResponse = true;
    return this.create(config);
  }

  fetchFollowingsWhoIntended(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.FOLLOWINGS_WHO_INTENDS(targetType, id);
    return this.get(config);
  }

  fetchNonFollowingsWhoIntended(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.NONFOLLOWINGS_WHO_INTENDS(targetType, id);
    return this.get(config);
  }

  fetchFollowingsWhoSaved(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.FOLLOWINGS_WHO_SAVED(targetType, id);
    return this.get(config);
  }

  fetchNonFollowingsWhoSaved(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.NONFOLLOWINGS_WHO_SAVED(targetType, id);
    return this.get(config);
  }

  fetchFollowingsWhoInvited(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.FOLLOWINGS_WHO_INVITED(targetType, id);
    return this.get(config);
  }

  fetchNonFollowingsWhoInvited(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.NONFOLLOWINGS_WHO_INVITED(targetType, id);
    return this.get(config);
  }
  
  fetchFollowingsWhoLiked(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.FOLLOWINGS_WHO_LIKED(targetType, id);
    return this.get(config);
  }
  
  fetchNonFollowingsWhoLiked(targetType, id, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.NONFOLLOWINGS_WHO_LIKED(targetType, id);
    return this.get(config);
  }

  fetchInvitedPeople(target, id, config = {}) {
    config.url = ENDPOINT.FETCH_INVITED_PEOPLE(target, id);
    return this.get(config);
  }
  
  removeLocation(config = {}) {
    config.url = ENDPOINT.REMOVE_LOCATION();
    return this.delete(config);
  }
  
  search(searchQuery, limit, page = 1, config = {}) {
    config.params = config.params || {};
    config.url = ENDPOINT.SEARCH();
    Object.assign(config.params, {
      page,
      limit,
      query: {
        $or: [
          {
            email: {
              $ilike: searchQuery
            }
          },
          {
            name: {
              $ilike: searchQuery
            }
          },
          {
            username: {
              $ilike: searchQuery
            }
          }
        ]
      }
    });
    return this.get(config);
  }
  
  googleCalendarSignOut() {
    const config = {
      url: ENDPOINT.GOOGLE_CALENDAR_SIGN_OUT()
    };
    return this.delete(config);
  }

  addAlerts(config = {}) {
    config.url = ENDPOINT.ALERTS();
    return this.update(config);
  }
}
