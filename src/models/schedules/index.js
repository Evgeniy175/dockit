import { BaseModel } from '../base';
import { getImageUrl, getImageSrcSetItem } from '../../utils/formatting/image';
import {getResolution} from '../../utils/images/resolution';

import DefaultImage from '../../assets/images/default-images/event-background-large.jpg'

import { ENDPOINT, MODEL_PLURAL, PLACE_RADIUS } from './constants';
import {MAX_RESOLUTION} from '../../utils/images/resolution/constants';



export default class ScheduleModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }
  
  createOne(config) {
    config.url = ENDPOINT.CREATE();
    config.fullResponse = true;
    return this.create(config);
  }

  feed(config = {}) {
    config.url = ENDPOINT.FEED();
    return this.list(config);
  }
  
  fetchById(scheduleId, config = {}) {
    config.url = ENDPOINT.GET(scheduleId);
    return this.get(config);
  }
  
  fetchSchedulesForUser(userId, limit, createdAt, config = {}) {
    config.params = {
      query: {
        user_id: userId,
        id_iCal: '<null>',
        id_google: '<null>',
        permission: 'global',
        created_at: {
          $lt: createdAt
        }
      },
      limit: limit,
      order: [
        'created_at',
        'desc'
      ]
    };
    
    if (!createdAt) delete config.params.query.created_at;
    
    config.url = ENDPOINT.SCHEDULES_FOR_USER();
    return this.list(config);
  }
  
  fetchDockedSchedulesForUser(userId) {
    let config = {};
    config.params = {
      query: {
        user_id: userId
      }
    };
    config.url = ENDPOINT.DOCKED_SCHEDULES_FOR_USER();
    return this.list(config);
  }

  static formatImageUrl(imageKey, defaultImage = DefaultImage, resolution = MAX_RESOLUTION) {
    if (!imageKey) return defaultImage;
    return getImageUrl(MODEL_PLURAL, imageKey, getResolution(resolution));
  }

  static getSrcSetData(imageKey, resolutions = []) {
    return imageKey
    ? resolutions.map(width => `${getImageSrcSetItem(MODEL_PLURAL, imageKey, width)} ${width}w`).join(',')
    : null;
  }
  
  uploadImage(id, config = {}) {
    config.url = ENDPOINT.IMAGE_UPLOAD(id);
    config.headers = { };
    return this.create(config);
  }

  deleteImage(scheduleId, config = {}){
    config.url = ENDPOINT.DELETE_COVER_IMAGE(scheduleId);
    return this.delete(config);
  }

  discoverFollowers(config = {}) {
    config.url = ENDPOINT.DISCOVER_FOLLOWERS();
    return this.get(config);
  }

  discover(config = {}, pos) {
    config.url = ENDPOINT.DISCOVER();
    Object.assign(config.params, {
      query: {
        'lat': pos.lat,
        'lng': pos.lng,
        'radius': PLACE_RADIUS
      }
    });
    return this.get(config);
  }

  discoverByPlaceId(config = {}) {
    config.url = ENDPOINT.DISCOVER_BY_PLACE_ID();
    config.params.query.radius = PLACE_RADIUS;
    return this.get(config);
  }
  
  fetchVirtualizedEvents(startDate, endDate, id, config = {}) {
    config.url = ENDPOINT.FETCH_VIRTUALIZED_EVENTS(id);
    config.params = {
      fromDate: startDate,
      toDate: endDate
    };
    return this.get(config);
  }
  
  fetchVirtualizedEventsForSchedule(id, startDate, endDate, max, config = {}) {
    config.url = ENDPOINT.FETCH_VIRTUALIZED_EVENTS_FOR_SCHEDULE(id);
    config.params = {
      fromDate: startDate,
      toDate: endDate,
      max: max
    };
    return this.get(config);
  }
  
  updateOne(id, config = {}){
    config.url = ENDPOINT.UPDATE(id);
    return this.update(config);
  }
  
  deleteOne(id, config = {}) {
    config.url = ENDPOINT.DELETE(id);
    return this.delete(config);
  }
  
  getPublicCreationsCount(id, config = {}) {
    config.url = ENDPOINT.CREATIONS_COUNT(id);
    return this.get(config);
  }
  
  search(searchQuery, limit, page = 1, config = {}) {
    config.params = config.params || {};
    //config.url = ENDPOINT.GET_ORDERED_BY_DISTANCE();
    config.url = ENDPOINT.SEARCH();
    //const location = DeviceLocation.getDeviceLocation();
    Object.assign(config.params, {
      page,
      limit,
      order: [
        'startTime',
        'asc'
      ],
      query: {
        //lat: location ? location.lat : null,
        //lng: location ? location.lon : null,
        //$and: [
          //{
            $or: [
              {
                title: {
                  $ilike: searchQuery
                }
              },
              {
                description: {
                  $ilike: searchQuery
                }
              },
              {
                'locationRaw.city': {
                  $ilike: searchQuery
                }
              }
            ],
            //}
        //]
      }
    });
    return this.get(config);
  }

  removeLocation(scheduleId, config = {}) {
    config.url = ENDPOINT.DELETE_LOCATION(scheduleId);
    return this.delete(config);
  }

  addAlerts(scheduleId, config = {}) {
    config.url = ENDPOINT.ALERTS(scheduleId);
    return this.update(config);
  }
}
