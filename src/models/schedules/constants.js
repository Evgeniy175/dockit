import { API_BASE } from '../../constants';

export const MODEL = 'schedule';
export const MODEL_PLURAL = 'schedules';

export const ENDPOINT = Object.freeze({
  CREATE: id => `${API_BASE}/${MODEL_PLURAL}`,
  LIST: () => `${API_BASE}/${MODEL_PLURAL}`,
  DISCOVER: () => `${API_BASE}/${MODEL_PLURAL}/discover`,
  DISCOVER_BY_PLACE_ID: () => `${API_BASE}/${MODEL_PLURAL}/discover`,
  DISCOVER_FOLLOWERS: () => `${API_BASE}/${MODEL_PLURAL}/discover-followers`,
  SCHEDULES_FOR_USER: () => `${API_BASE}/${MODEL_PLURAL}/decorated`,
  GET: id => `${API_BASE}/${MODEL_PLURAL}/${id}/decorated`,
  GET_ORDERED_BY_DISTANCE: () => `${API_BASE}/${MODEL_PLURAL}/order-distance/decorated`,
  DOCKED_SCHEDULES_FOR_USER: () => `${API_BASE}/${MODEL_PLURAL}/my-docked`,
  FETCH_VIRTUALIZED_EVENTS: id => `${API_BASE}/${MODEL_PLURAL}/with-virtualized-dates${id ? `/${id}` : ''}`,
  FETCH_VIRTUALIZED_EVENTS_FOR_SCHEDULE: (id) => `${API_BASE}/${MODEL_PLURAL}/${id}/with-virtualized-dates`,
  IMAGE_UPLOAD: id => `${API_BASE}/${MODEL_PLURAL}/${id}/image`,
  DELETE_COVER_IMAGE: id => `${API_BASE}/${MODEL_PLURAL}/${id}/image`,
  UPDATE: id => `${API_BASE}/${MODEL_PLURAL}/${id}`,
  DELETE: id => `${API_BASE}/${MODEL_PLURAL}/${id}`,
  CREATIONS_COUNT: id => `${API_BASE}/${MODEL_PLURAL}/count-creations/${id}`,
  SEARCH: id => `${API_BASE}/${MODEL_PLURAL}/decorated`,
  DELETE_LOCATION: scheduleId => `${API_BASE}/${MODEL_PLURAL}/locations/${scheduleId}`,
  ALERTS: id => `${API_BASE}/${MODEL_PLURAL}/${id}/alerts`,
});

export const PLACE_RADIUS = 40000;
