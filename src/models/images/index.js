import { BaseModel } from '../base';
import { ENDPOINT } from './constants';


export default class ImagesModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  attachImage(eventId, target, config = {}){
    config.url = ENDPOINT.ATTACH_IMAGE(eventId, target);
    config.headers = { };
    return this.create(config);
  }

  fetchImages(targetId, target, config = {}){
    config.url = ENDPOINT.FETCH_IMAGES(targetId, target);
    return this.get(config);
  }
}
