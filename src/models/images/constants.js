import { API_BASE } from '../../constants';
import { MODEL as EVENT_MODEL, MODEL_PLURAL as EVENT_MODEL_PLURAL } from '../events/constants';
import { MODEL as SCHEDULE_MODEL, MODEL_PLURAL as SCHEDULE_MODEL_PLURAL } from '../schedules/constants';
import { DEFAULT_RESOLUTION } from '../../utils/images/resolution/constants';

export const MODEL = 'images';
export const MODEL_PLURAL = 'images';

export const TARGET = Object.freeze({
  COMMENT: MODEL_PLURAL,
  EVENT: EVENT_MODEL_PLURAL,
  SCHEDULE: SCHEDULE_MODEL_PLURAL,
});

export const ENDPOINT = Object.freeze({
  ATTACH_IMAGE: (id, target) => `${API_BASE}/${MODEL_PLURAL}/${target}/${id}`,
  FETCH_IMAGES: (id, target) => `${API_BASE}/${MODEL_PLURAL}/${target}/${id}`,
  DELETE: () => `${API_BASE}/${MODEL_PLURAL}/`,
});

export const DEFAULT_ATTACHED_IMAGE_RESOLUTION = DEFAULT_RESOLUTION;
