import {BaseModel, p} from '../base';
import {ENDPOINT, MODEL_PLURAL} from './constants';
import {Auth} from '../../auth';
import queryString from 'query-string';

export default class FollowModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  accept(followId, config = {}) {
    config.url = ENDPOINT.ACCEPT(followId);
    return this.update(config);
  }

  reject(followId, config = {}) {
    config.url = ENDPOINT.REJECT(followId);
    return this.delete(config);
  }
  
  follow(userId, config = {}) {
    config.url = ENDPOINT.FOLLOW(userId);
    return this.create(config);
  }
  
  unfollow(userId, config = {}) {
    config.url = ENDPOINT.UNFOLLOW(userId);
    return this.delete(config);
  }
}
