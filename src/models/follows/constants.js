import {API_BASE} from '../../constants';

export const MODEL = 'follow';
export const MODEL_PLURAL = 'follows';

export const ENDPOINT = Object.freeze({
  ACCEPT: (followId) => `${API_BASE}/${MODEL_PLURAL}/accept/${followId}`,
  REJECT: (followId) => `${API_BASE}/${MODEL_PLURAL}/reject/${followId}`,
  FOLLOW: (userId) => `${API_BASE}/${MODEL_PLURAL}/users/${userId}`,
  UNFOLLOW: (userId) => `${API_BASE}/${MODEL_PLURAL}/users/${userId}`
});