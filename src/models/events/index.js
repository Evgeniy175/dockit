import {BaseModel} from '../base';
import {getImageUrl, getImageSrcSetItem} from '../../utils/formatting/image';
import {getResolution} from '../../utils/images/resolution';
import deepAssign from 'deep-assign';
import moment from 'moment-timezone';

import DefaultImage from '../../assets/images/default-images/event-background-large.jpg';

import {ENDPOINT, MODEL_PLURAL} from './constants';
import {MAX_RESOLUTION} from '../../utils/images/resolution/constants';



export default class EventModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }
  
  fetchEventsForUser(userId, limit, createdAt, config = {}) {
    config.params = {
      query: {
        user_id: userId,
        id_iCal: '<null>',
        id_google: '<null>',
        schedule_id: '<null>',
        permission: 'global',
        created_at: {
          $lt: createdAt
        }
      },
      limit: limit,
      order: [
        'created_at',
        'desc'
      ]
    };
    
    if (!createdAt) delete config.params.query.created_at;
    
    config.url = ENDPOINT.EVENTS_FOR_USER();
    return this.list(config);
  }
  
  fetchById(eventId, config = {}) {
    config.url = ENDPOINT.GET(eventId);
    return this.get(config);
  }

  fetchDockedEventsForUser(time) {
    let config = {};
    config.params = {
      query: {
        startTime: {
          $between: time
        }
      }
    };
    config.url = ENDPOINT.DOCKED_EVENTS_FOR_USER();
    return this.list(config);
  }

  fetchDockedEventsForOtherUser(time, id = '') {
    let config = {};
    config.params = {
      query: {
        startTime: {
          $between: time
        }
      }
    };
    config.url = ENDPOINT.DOCKED_EVENTS_FOR_OTHER_USER(id);
    return this.get(config);
  }

  fetchEventsByLocationInRadius(config = {}) {
    config.url = ENDPOINT.DISCOVER();
    return this.list(config);
  }

  static formatImageUrl(imageKey, defaultImage = DefaultImage, resolution = MAX_RESOLUTION) {
    if (!imageKey) return defaultImage;
    return getImageUrl(MODEL_PLURAL, imageKey, getResolution(resolution));
  }

  static getSrcSetData(imageKey, resolutions = []) {
    return imageKey
    ? resolutions.map(width => `${getImageSrcSetItem(MODEL_PLURAL, imageKey, width)} ${width}w`).join(',')
    : null;
  }
  
  uploadImage(id, config = {}) {
    config.url = ENDPOINT.IMAGE_UPLOAD(id);
    config.headers = { };
    return this.create(config);
  }

  deleteImage(eventId, config = {}){
    config.url = ENDPOINT.DELETE_COVER_IMAGE(eventId);
    return this.delete(config);
  }

  discoverFollowers(config = {}) {
    config.url = ENDPOINT.DISCOVER_FOLLOWERS();
    return this.get(config);
  }

  discover(config = {}, pos) {
    config.url = ENDPOINT.DISCOVER();
    config.params = {
      query: {
        'lat': pos.lat,
        'lng': pos.lng
      }
    };
    return this.get(config);
  }

  discoverByPlaceId(config = { params: {} }) {
    config.url = ENDPOINT.DISCOVER_BY_PLACE_ID();
    return this.get(config);
  }

  createOne(config) {
    config.url = ENDPOINT.CREATE();
    return this.create(config);
  }
  
  updateOne(id, config = {}){
    config.url = ENDPOINT.UPDATE(id);
    return this.update(config);
  }
  
  deleteOne(id, config = {}) {
    config.url = ENDPOINT.DELETE(id);
    return this.delete(config);
  }
  
  getPublicCreationsCount(id, config = {}) {
    config.url = ENDPOINT.CREATIONS_COUNT(id);
    return this.get(config);
  }
  
  fromSchedule(scheduleId, config) {
    config.url = ENDPOINT.FROM_SCHEDULE(scheduleId);
    return this.create(config);
  }
  
  search(searchQuery, limit, page = 1, config = {}) {
    config.params = config.params || {};
    //config.url = ENDPOINT.GET_ORDERED_BY_DISTANCE();
    config.url = ENDPOINT.SEARCH();
    //const location = DeviceLocation.getDeviceLocation();
    Object.assign(config.params, {
      page,
      limit,
      query: {
        //lat: location ? location.lat : null,
        //lng: location ? location.lon : null,
        //$and: [
        //  {
            $or: [
              {
                title: {
                  $ilike: searchQuery,
                },
              },
              {
                description: {
                  $ilike: searchQuery,
                },
              },
              {
                'locationRaw.city': {
                  $ilike: searchQuery,
                },
              },
            ],
          //},
        //],
      },
    });
    return this.get(config);
  }
  
  googleCalendarSignIn(token) {
    const config = {
      body: JSON.stringify({
        token: {
          access_token: token.access_token
        }
      }),
      url: ENDPOINT.GOOGLE_CALENDAR_SIGN_IN()
    };
    
    return this.create(config);
  }

  removeLocation(eventId, config = {}) {
    config.url = ENDPOINT.DELETE_LOCATION(eventId);
    return this.delete(config);
  }

  fetchEventsForSchedule(scheduleId, config = {}) {
    config.url = ENDPOINT.FETCH_EVENTS_FOR_SCHEDULE(scheduleId);
    return this.get(config);
  }

  addAlerts(eventId, config = {}) {
    config.url = ENDPOINT.ALERTS(eventId);
    return this.update(config);
  }

  fetchUpcoming(startDate, endDate, config = {}) {
    deepAssign(config, {
      url: ENDPOINT.UPCOMING(),
      params: {
        timezone: moment.tz.guess(),
        fromDate: startDate,
        toDate: endDate,
      },
    });
    return this.get(config);
  }
}
