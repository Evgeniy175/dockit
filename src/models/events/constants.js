import { API_BASE } from '../../constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../schedules/constants';

export const MODEL = 'event';
export const MODEL_PLURAL = 'events';

export const ENDPOINT = Object.freeze({
  LIST: () => `${API_BASE}/${MODEL_PLURAL}`,
  DISCOVER: () => `${API_BASE}/${MODEL_PLURAL}/discover`,
  DISCOVER_BY_PLACE_ID: () => `${API_BASE}/${MODEL_PLURAL}/discover`,
  DISCOVER_FOLLOWERS: () => `${API_BASE}/${MODEL_PLURAL}/discover-followers`,
  EVENTS_FOR_USER: () => `${API_BASE}/${MODEL_PLURAL}/decorated`,
  GET: id => `${API_BASE}/${MODEL_PLURAL}/${id}/decorated`,
  GET_ORDERED_BY_DISTANCE: () => `${API_BASE}/${MODEL_PLURAL}/order-distance/decorated`,
  DOCKED_EVENTS_FOR_USER: () => `${API_BASE}/${MODEL_PLURAL}/my-docked`,
  DOCKED_EVENTS_FOR_OTHER_USER: id => `${API_BASE}/${MODEL_PLURAL}/public-docked/${id}`,
  CREATE: () => `${API_BASE}/${MODEL_PLURAL}/`,
  UPDATE: id => `${API_BASE}/${MODEL_PLURAL}/${id}`,
  IMAGE_UPLOAD: id => `${API_BASE}/${MODEL_PLURAL}/${id}/image`,
  DELETE_COVER_IMAGE: id => `${API_BASE}/${MODEL_PLURAL}/${id}/image`,
  DELETE: id => `${API_BASE}/${MODEL_PLURAL}/${id}`,
  CREATIONS_COUNT: id => `${API_BASE}/${MODEL_PLURAL}/count-creations/${id}`,
  FROM_SCHEDULE: eventId => `${API_BASE}/${MODEL_PLURAL}/from-schedule/${eventId}`,
  SEARCH: eventId => `${API_BASE}/${MODEL_PLURAL}/decorated`,
  GOOGLE_CALENDAR_SIGN_IN: eventId => `${API_BASE}/${MODEL_PLURAL}/google-calendar`,
  DELETE_LOCATION: eventId => `${API_BASE}/${MODEL_PLURAL}/locations/${eventId}`,
  FETCH_EVENTS_FOR_SCHEDULE: scheduleId => `${API_BASE}/${MODEL_PLURAL}/${SCHEDULES_MODEL_PLURAL}/${scheduleId}`,
  ALERTS: id => `${API_BASE}/${MODEL_PLURAL}/${id}/alerts`,
  UPCOMING: () => `${API_BASE}/${MODEL_PLURAL}/upcoming`,
});
