import { BaseModel } from '../../base';
import { ENDPOINT, TARGETS } from './constants';

/**
 *
 */
export default class LikeModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  static getTarget(item) {
    for (const key of TARGETS)
      if (item[key])
        return key;
  }
  
  deleteOne(config = {}) {
    config.url = ENDPOINT.DELETE(config);
    return this.delete(config);
  }
}
