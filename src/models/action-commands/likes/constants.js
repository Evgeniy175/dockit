import { API_BASE } from '../../../constants';
import { get } from 'lodash';

import { MODEL as EVENT_MODEL } from '../../events/constants';
import { MODEL as SCHEDULE_MODEL } from '../../schedules/constants';
import { MODEL as COMMENT_MODEL } from '../../comments/constants';

export const MODEL = 'like';
export const MODEL_PLURAL = 'likes';



export const TARGETS = [
  EVENT_MODEL,
  SCHEDULE_MODEL,
  COMMENT_MODEL,
];

export const ENDPOINT = Object.freeze({
  CREATE: config => `${API_BASE}/${MODEL_PLURAL}/${get(config, 'target.type')}/${get(config, 'target.id')}`,
  DELETE: config => `${API_BASE}/${MODEL_PLURAL}/${get(config, 'target.type')}/${get(config, 'target.id')}`
});
