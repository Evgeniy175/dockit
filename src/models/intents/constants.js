import { API_BASE } from '../../constants';
import { get } from 'lodash';

export const MODEL = 'intent';
export const MODEL_PLURAL = 'intents';

export const TYPES = {
  DOCKED: 'docked',
  NOT_GOING: 'not_going',
  MAYBE: 'maybe',
};

export const ENDPOINT = Object.freeze({
  UPDATE_INTENT: config => `${API_BASE}/${MODEL_PLURAL}/${get(config, 'routing.type')}/${get(config, 'routing.id')}`,
});