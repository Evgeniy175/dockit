import { BaseModel, p } from '../base';
import { ENDPOINT, MODEL_PLURAL } from './constants';



export default class IntentModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  updateIntent(config = {}) {
    config.url = ENDPOINT.UPDATE_INTENT(config);
    return this.update(config);
  }
}
