import { API_BASE } from '../../constants';

export const MODEL = 'notification';
export const MODEL_PLURAL = 'notifications';

export const ENDPOINT = Object.freeze({
  NOTIFICATIONS: (request) => `${API_BASE}/notifications`,
  UNREAD_COUNT_NOTIFICATIONS: () => `${API_BASE}/notifications/unread-count-notifications`,
  UNREAD_COUNT_REQUESTS: () => `${API_BASE}/notifications/unread-count-requests`,
  MARK_READ_NOTIFICATIONS: () => `${API_BASE}/notifications/mark-read-notifications`,
  MARK_READ_REQUESTS: () => `${API_BASE}/notifications/mark-read-requests`,
});
