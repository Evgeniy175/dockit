import {BaseModel, p} from '../base';
import {ENDPOINT} from './constants';

/**
 *
 */
export default class NotificationModel extends BaseModel {
  constructor(config = {}) {
    config.endpoint = ENDPOINT;
    super(config);
  }

  notification(config = {}) {
    config.url = ENDPOINT.NOTIFICATIONS();
    return this.get(config);
  }

  getUnreadNotificationsCount(config = {}) {
    config.url = ENDPOINT.UNREAD_COUNT_NOTIFICATIONS();
    return this.get(config);
  }

  getUnreadRequestsCount(config = {}) {
    config.url = ENDPOINT.UNREAD_COUNT_REQUESTS();
    return this.get(config);
  }

  markReadNotifications(config = {}) {
    config.url = ENDPOINT.MARK_READ_NOTIFICATIONS();
    return this.update(config);
  }

  markReadRequests(config = {}) {
    config.url = ENDPOINT.MARK_READ_REQUESTS();
    return this.update(config);
  }
}
