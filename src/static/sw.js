'use strict';

const version = '20180313';
const staticCacheName = `${version}static`;
const pagesCacheName = `${version}pages`;
const URLs = [
  '/',
  '/js/index.js',
  '/js/vendor.js',
  '/css/index.css',
];

function updateStaticCache() {
  return caches.open(staticCacheName)
  .then(cache => cache.addAll(URLs));
}

function stashPages() {
  return caches.open(pagesCacheName)
  .then(cache => {
    return URLs.forEach(url => {
      fetch(url, { credentials: 'include' })
      .then(response => cache.put(url, response))
    });
  });
}

function clearOldCaches() {
  return caches.keys()
  .then(keys => {
    return Promise.all(
      keys.filter(key => key.indexOf(version) !== 0).map(key => caches.delete(key))
    );
  });
}

addEventListener('install', event => {
  event.waitUntil(
    updateStaticCache().then(event.skipWaiting)
  );
});

addEventListener('activate', event => {
  event.waitUntil(
    clearOldCaches()
    .then(stashPages)
    .then(clients.claim)
  );
});

addEventListener('fetch', event => {});
