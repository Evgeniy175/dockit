import React, { Component } from 'react';
import { action, observable, useStrict, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Link, browserHistory } from 'react-router';
import { Grid, Row, Navbar } from 'react-bootstrap';

import { Auth } from './auth';

import Logo from './assets/images/brand_beta.png';
import SearchIcon from './assets/images/icons/search.svg';

import { init as initMenuHandlers, dispose as disposeMenuHandlers } from './utils/menu';

import DockItNavbarLeft from './shared-components/navbar/left/index.jsx';
import DockItNavbarRight from './shared-components/navbar/right/index.jsx';

import jq from './utils/jquery';
import {DeviceLocation} from './utils/location';

import {SEARCH_PATH} from './constants';

import DataState from './state/data';
import UiState from './state/ui';

useStrict(true);



@observer class DockIt extends Component {
  deviceLocation = new DeviceLocation();

  constructor(props) {
    super(props);

    this.deviceLocation.getDeviceLocation();

    this.onSearchExpand = ::this.onSearchExpandHandler;
    this.onNotificationsCounterFetched = this.onNotificationsCounterFetchedHandler;
    this.onForceSearchExpandFinished = ::this.onForceSearchExpandFinishedHandler;
    this.onItemSelect = ::this.onItemSelectHandler;
    this.onSearchClick = ::this.onSearchClickHandler;
    this.onNotificationsRead = ::this.onNotificationsReadHandler;
    this.onRequestsRead = ::this.onRequestsReadHandler;

    Object.assign(this, {
      data: new DataState(),
      ui: new UiState(),
    });
  }

  componentDidMount() {
    initMenuHandlers();
  }

  componentWillUnmount() {
    this.onSearchExpand = null;
    this.onNotificationsCounterFetched = null;
    this.onForceSearchExpandFinished = null;
    this.onItemSelect = null;
    this.onSearchClick = null;
    this.onNotificationsRead = null;
    this.onRequestsRead = null;
    this.refresher.clear();
    this.refresher = null;
    disposeMenuHandlers();
    this.data.clear();
  }

  onNotificationsReadHandler() {
    this.data.setNotificationsCounter(0);
  }

  onRequestsReadHandler() {
    this.data.setRequestsCounter(0);
  }
  
  onSearchExpandHandler() {
    if (jq.wrap('#search-block').isHidden()) return browserHistory.push(SEARCH_PATH);
    this.ui.setIsForceSearchExpand(true);
  }

  onNotificationsCounterFetchedHandler(value) {

  }
  
  onForceSearchExpandFinishedHandler() {
    this.ui.setIsForceSearchExpand(false);
  }

  onItemSelectHandler() {
    this.ui.setNavbarExpanded(false);
  }

  onSearchClickHandler() {
    browserHistory.push(SEARCH_PATH);
  }
  
  render() {
    const isAuthorized = Auth.getActiveToken();
    const { isForceSearchExpand, isNavbarExpanded } = this.ui;
    const { notificationsCount, requestsCount } = this.data;
    
    const childProps = {
      nOfNotifications: notificationsCount,
      nOfRequests: requestsCount,
      handlers: {
        onSearchExpand: this.onSearchExpand,
        onNotificationsRead: this.onNotificationsRead,
        onRequestsRead: this.onRequestsRead,
      }
    };
    const child = React.cloneElement(this.props.children, childProps);
    
    const navBarProps = {
      values: {
        isForceSearchExpand: toJS(isForceSearchExpand),
        nOfNotifications: notificationsCount,
        nOfRequests: requestsCount,
      },
      handlers: {
        onNotificationsCounterFetched: this.onNotificationsCounterFetched,
        onForceSearchExpandFinished: this.onForceSearchExpandFinished,
        onSelect: this.onItemSelect,
      }
    };
    
    return (
      <Grid fluid={true}>
        <Navbar className='dockit-navbar' collapseOnSelect expanded={isNavbarExpanded} fixedTop>
          <Navbar.Header>
            <span className='search-icon-container' onClick={this.onSearchClick}>
              <img src={SearchIcon} />
            </span>
            <Navbar.Brand>
              <Link to='/'>
                <img src={Logo} />
              </Link>
            </Navbar.Brand>
            { isAuthorized && <Navbar.Toggle /> }
          </Navbar.Header>
          <Navbar.Collapse>
            <DockItNavbarLeft values={navBarProps.values} handlers={navBarProps.handlers} />
            <DockItNavbarRight values={navBarProps.values} handlers={navBarProps.handlers} />
          </Navbar.Collapse>
        </Navbar>
        <Row className='content-container'>
          {/*<Devtools />*/}
          {/** This is where nested view gets rendered. **/}
          { child }
        </Row>
      </Grid>
    );
  }
}

export default DockIt;
