import { browserHistory } from 'react-router';
import { get } from 'lodash';

import UsersModel from './models/users';

import { initNotifications, removeJobs } from './utils/notifications';
import { setLocation } from './utils/dom/window';

import { HOME_PATH, FEED_PATH, CHECK_EMAIL_PATH } from './constants';

const usersModel = new UsersModel();



class Auth {
  static signUp = (username, password, name, email) => {
    const config = {
      body: JSON.stringify({
        name,
        email,
        password,
        username
      }),
    };
    
    return usersModel.createUser(config)
    .then(res => {
      if (!res || !res.token || !res.user) return Promise.reject('Sign Up failed');
      
      Auth.setActiveToken(res.token);
      Auth.setActiveUser(res.user);

      setLocation(FEED_PATH);
      return initNotifications();
    });
  };

  static login = (email, password) => {
    const config = {
      body: JSON.stringify({
        'email': email,
        'password': password
      })
    };
    
    return usersModel.login(config)
    .then(res => {
      if (!res || !res.token || !res.user) return Promise.reject('Sign In failed');

      Auth.setActiveToken(res.token);
      Auth.setActiveUser(res.user);

      setLocation(FEED_PATH);
      return initNotifications();
    });
  };

  static logout = () => {
    return usersModel.logout()
    .then(() => {
      Auth.removeActiveToken();
      Auth.removeActiveUser();
      Auth.removeActiveGoogleAuth();

      setLocation(HOME_PATH);
      removeJobs();
    })
    .catch(console.error);
  };

  static isSigned = () => !!Auth.getActiveUser();

  static forgotPassword = email => {
    const config = {
      body: JSON.stringify({
        email
      })
    };
    
    return usersModel.forgotPassword(config)
    .then(() => browserHistory.push(CHECK_EMAIL_PATH));
  };

  static resetPassword = (email, code, password, confirmPassword) => {
    const config = {
      body: JSON.stringify({
        email,
        code,
        password,
        confirmPassword
      })
    };

    return usersModel.resetPassword(config);
  };

  static getActiveToken = () => {
    return localStorage.getItem('token');
  };
  
  static getActiveUser = () => {
    const user = localStorage.getItem('user');
    return user ? JSON.parse(user) : user;
  };
  
  static getActiveGoogleAuth = () => {
    const data = localStorage.getItem('google-auth-data');
    return data ? JSON.parse(data) : data;
  };
  
  static setActiveToken = token => {
    return localStorage.setItem('token', token);
  };

  static setActiveUser = user => {
    const u = {
      id: user.id,
      name: user.name,
      username: user.username,
      profilePicture: user.profilePicture,
      isPublic: user.public,
      location: {
        place_id: get(user, 'location.place_id')
      },
      isUpcomingDisabled: user.isUpcomingDisabled
    };
  
    return localStorage.setItem('user', JSON.stringify(u));
  };
  
  static setActiveGoogleAuth = authInstance => {
    const data = get(authInstance, '$K.Q7');
    const exists = !!data;
    return exists && localStorage.setItem('google-auth-data', JSON.stringify(data));
  };
  
  static removeActiveToken = () => {
    localStorage.removeItem('token');
  };
  
  static removeActiveUser = () => {
    localStorage.removeItem('user');
  };
  
  static removeActiveGoogleAuth = () => {
    return localStorage.removeItem('google-auth-data');
  };
}

export { Auth };
