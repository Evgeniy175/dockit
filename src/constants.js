import { isMobileOrTablet } from './utils/device';

export const API_BASE = __API_BASE__;
export const APP_BASE = __APP_BASE__;
export const HOME_PATH = '/home';
export const LOGIN_PATH = '/login';
export const SIGN_UP_PATH = '/signup';
export const FORGOT_PASSWORD_PATH = '/users/forgot-password';
export const RESET_PASSWORD_PATH = '/users/reset-password';
export const CHECK_EMAIL_PATH = '/users/check-email';
export const SUCCESS_PASSWORD_RESET_PATH = '/users/success-password-reset';
export const FEED_PATH = '/feed';
export const LINK_CALENDARS_PATH = '/link-calendars';
export const UPCOMING_PATH = '/upcoming';
export const SEARCH_PATH = '/search';
export const IS_MOBILE_OR_TABLET = isMobileOrTablet();

export const FORBIDDEN_STATUS = 403;

export const KEY_CODES = {
  BACKSPACE: 8,
  TAB: 9,
  ENTER: 13,
  ESC: 27,
  PAGE_UP: 33,
  PAGE_DOWN: 34,
  LEFT_ARROW: 37,
  UP_ARROW: 38,
  RIGHT_ARROW: 39,
  DOWN_ARROW: 40,
  AT: 50,
};

export const EMPTY_CHAR = '·';

export const EVENTS_CONSTANTS = {
  DATE_FORMAT: 'YYYY-MM-DD HH:mm:ss.ssss ZZ',
  AFTER_CREATING_ROUTE: '/feed',
};

export const EVENT_SHOW_CONSTANTS = {
  NEVER_ENDS: 'Never ends',
  DATE_FORMAT: 'll',
  TIME_FORMAT: 'LT',
  COMMENT_DATE_FORMAT: 'LLL',
  COMMENTS_FROM_BOTTOM_MIN_PERCENT: 10,
  COMMENTS_PER_PORTION: 50,
};

export const INPUT_FORMATS = {
  DATE: 'MM/DD/YYYY',
  TIME: 'HH:mm',
};

export const DAYS_OF_WEEK = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
