import { action, observable, toJS } from 'mobx';
import Promise from 'bluebird';

import NotificationsModel from '../models/notifications';

import DataState from '../utils/state/data';
import Refresher from '../utils/refresher';

import { NOTIFICATIONS_FETCH_DELAY_MS } from './constants';

const notificationModel = new NotificationsModel();



class AppState extends DataState {
  @observable notificationsCount = 0;
  @observable requestsCount = 0;

  refresher;

  constructor(props) {
    super(props);
    this.fetchNotificationsCountHandler();
    const refresherData = {
      cb: ::this.fetchNotificationsCountHandler,
      timeout: NOTIFICATIONS_FETCH_DELAY_MS,
    };
    this.refresher = new Refresher(refresherData);
  }

  fetchNotificationsCountHandler() {
    return Promise.all([
      notificationModel.getUnreadNotificationsCount(),
      notificationModel.getUnreadRequestsCount(),
    ])
    .spread((notificationsCount, requestsCount) => {
      this.setNotificationsCounter(notificationsCount);
      this.setRequestsCounter(requestsCount);
      return Promise.resolve();
    })
    .catch(console.error);
  }

  @action
  setNotificationsCounter(value) {
    this.notificationsCount = value;
  }

  @action
  setRequestsCounter(value) {
    this.requestsCount = value;
  }

  @action
  clear() {
    this.refresher.clear();
  }
}

export default AppState;
