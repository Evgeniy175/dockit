import { action, observable, toJS } from 'mobx';

import UIState from '../utils/state/data';

import jq from '../utils/jquery';



class AppUiState extends UIState {
  @observable isForceSearchExpand = false;
  @observable isNavbarExpanded = false;

  constructor(props = {}) {
    super(props);

    jq.attachEvent(document, 'click', e => {
      const target = jq.wrap(e.target);
      if (!target.isValid()) return;
      this.setNavbarExpanded(target.hasClass('navbar-toggle') ? !this.isNavbarExpanded : false);
    });

    jq.attachEvent(document, 'touchmove', () => {
      this.setNavbarExpanded(false);
    });

    jq.attachEvent(document, 'wheel', () => {
      this.setNavbarExpanded(false);
    });
  }

  @action
  setIsForceSearchExpand(value) {
    this.isForceSearchExpand = value;
  }

  @action
  setNavbarExpanded(value) {
    this.isNavbarExpanded = value;
  }
}

export default AppUiState;
