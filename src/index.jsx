import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';
import './index.scss';

import { Auth } from './auth';
import { requestNotificationsPermission, initNotifications } from './utils/notifications';

import DockIt from './app.jsx';
import DockItUnauthorized from './views/auth/index.jsx';

import NewDiscover from './views/new-discover/index.jsx';
import Feed from './views/feed/index.jsx';
import Notifications from './views/notifications/index.jsx';
import NotificationsNotifications from './views/notifications/notifications/index.jsx';
import Requests from './views/notifications/requests/index.jsx';
import UserProfile from './views/profile/index.jsx';
import ProfileSettings from './views/profile/settings/index.jsx';
import PrivacyPolicy from './views/privacy-policy/index.jsx';
import Terms from './views/terms/index.jsx';
import Home from './views/auth/home/index.jsx';
import Login from './views/auth/login/index.jsx';
import SignUp from './views/auth/signup/index.jsx';
import ForgotPassword from './views/auth/forgot-password/index.jsx';
import ResetPassword from './views/auth/reset-password/index.jsx';
import CheckEmail from './views/auth/check-email/index.jsx';
import SuccessPasswordReset from './views/auth/success/index.jsx';
import AddEvent from './views/creations/creating/index.jsx';
import CreationDetails from './views/creations/details/index.jsx';
import CreationEdit from './views/creations/update/index.jsx';
import Upcoming from './views/upcoming/index.jsx';
import ContentFlag from './views/reports/creating/index.jsx';
import SearchMobile from './views/search/mobile/index.jsx';
import Posts from './views/posts/index.jsx';
//import LinkCalendars from './shared-components/link-calendars/index.jsx';
import ImagesView from './views/images/index.jsx';

import { Redirect, Router, Route, IndexRoute, IndexRedirect, browserHistory } from 'react-router';

import { TYPES as CREATION_TYPES } from './views/creations/constants';

import { getLocation } from './utils/dom/window';

ReactGA.initialize('UA-115556007-1');

const isAuthorized = !!Auth.getActiveToken();

if (isAuthorized) {
  ReactDOM.render(
    <Router history={ browserHistory } onUpdate={onUpdateHandler}>
      <Route path='/' component={ DockIt }>
        <IndexRoute component={ Feed } />
        <Route path='feed' component={ Feed } />
        <Route path='discover' component={ NewDiscover } />
        <Route path='notifications' component={ Notifications }>
          <IndexRedirect to='notifications' />
          <Route path='requests' component={ Requests } />
          <Route path='notifications' component={ NotificationsNotifications } />
        </Route>
        <Route path='users/forgot-password' component={ ForgotPassword } />
        <Route path='users/reset-password' component={ ResetPassword } />
        <Route path='users/check-email' component={ CheckEmail } />
        <Route path='users/success-password-reset' component={SuccessPasswordReset} />
        <Route path='users/:id' component={ UserProfile } />
        <Route path='profile/settings' component={ ProfileSettings } />

        <Route path={CREATION_TYPES.EVENT} component={ AddEvent } />
        <Route path={`${CREATION_TYPES.EVENT}/:id`} component={ CreationDetails } />
        <Route path={`${CREATION_TYPES.EVENT}/:id/edit`} component={ CreationEdit } />
        <Route path={`${CREATION_TYPES.EVENT}/:id/images`} component={ ImagesView }/>

        <Route path={`${CREATION_TYPES.SCHEDULE}/:id`} component={ CreationDetails } />
        <Route path={`${CREATION_TYPES.SCHEDULE}/:id/edit`} component={ CreationEdit } />
        <Route path={`${CREATION_TYPES.SCHEDULE}/:id/images`} component={ ImagesView }/>

        <Route path='terms' component={ Terms } />
        <Route path='privacy-policy' component={ PrivacyPolicy } />
        <Route path='upcoming' component={ Upcoming } />
        <Route path='reports/:target/:id' component={ ContentFlag } />
        {/*<Route path='link-calendars' component={LinkCalendars} />*/}
        <Route path='search' component={SearchMobile} />
        <Route path='posts/:id' component={Posts} />
        <Redirect from='*' to='feed' />
      </Route>
    </Router>,
    document.getElementById('root')
  );
}
else {
  ReactDOM.render(
    <Router history={ browserHistory } onUpdate={onUpdateHandler}>
      <Route path='/' component={ DockItUnauthorized }>
        <IndexRoute component={ Home } />
        <Route path='home' component={ Home } />
        <Route path='login' component={ Login } />
        <Route path='signup' component={ SignUp } />
        <Route path='users/forgot-password' component={ ForgotPassword } />
        <Route path='users/reset-password' component={ ResetPassword } />
        <Route path='users/check-email' component={ CheckEmail } />
        <Route path='users/success-password-reset' component={SuccessPasswordReset} />
        <Route path='terms' component={ Terms } />
        <Route path='privacy-policy' component={ PrivacyPolicy } />

        { /* public creations start */ }
        <Route path={`${CREATION_TYPES.EVENT}/:id`} component={ CreationDetails } />
        <Route path={`${CREATION_TYPES.SCHEDULE}/:id`} component={ CreationDetails } />
        <Route path='users/:id' component={ UserProfile } />
        { /* public creations end */ }

        <Redirect from='*' to='home' />
      </Route>
    </Router>,
    document.getElementById('root')
  );
}

function onUpdateHandler() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
  ReactGA.pageview(getLocation().pathname + getLocation().search);
}

requestNotificationsPermission()
.then(initNotifications)
.catch(console.error);
