export const WRAPPER_CLASS_NAME = 'background-image-container';
export const IMAGE_CLASS_NAME = 'background-image';
export const MAX_16x9_WIDTH = 768;
