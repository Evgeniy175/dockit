import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Image from '../../image/index.jsx';

import { WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME, MAX_16x9_WIDTH } from './constants';
import { ASPECT_RATIOS, MEASURES } from '../../image/constants';



@observer
class BackgroundImageWrapper extends Component {
  @observable values;
  handlers;

  constructor(props) {
    super(props);
    this.setValues({
      src: this.props.image,
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: this.getAspectRatio(),
      prepareWrapper: true,
    });
    this.handlers = {
      onClick: ::this.onClickHandler,
    };
    this.onResize = ::this.onResizeHandler;
  }

  componentWillMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
    this.setValues(null);
    this.handlers = null;
    this.onResize = null;
  }

  componentWillReceiveProps(nextProps) {
    const { imageUrl, isDefaultImage } = this.props;
    if (nextProps.imageUrl !== imageUrl) this.setSrc(nextProps.imageUrl);
    if (nextProps.isDefaultImage !== isDefaultImage) this.setClassName(nextProps);
  }

  onClickHandler() {
    const handler = get(this.props, 'onClick');
    if (handler) handler();
  }

  onResizeHandler() {
    const newRatio = this.getAspectRatio();
    const curr = this.values.aspectRatio;
    if (curr && newRatio[MEASURES.WIDTH] === curr[MEASURES.WIDTH] && newRatio[MEASURES.HEIGHT] === curr[MEASURES.HEIGHT]) return;
    this.setAspectRatio(newRatio);
  }

  getAspectRatio() {
    return window.innerWidth < MAX_16x9_WIDTH ? ASPECT_RATIOS.s16x9 : ASPECT_RATIOS.s16x3;
  }

  @action
  setValues(value) {
    this.values = value;
  }

  @action
  setSrc(value) {
    this.values.src = value;
  }

  @action
  setClassName(props) {
    this.values.wrapperClassName = `${WRAPPER_CLASS_NAME}${props.isDefaultImage ? '' : ' hoverable'}`;
  }

  @action
  setAspectRatio(value) {
    this.values.aspectRatio = value;
  }
  
  render() {
    if (!this.props.imageUrl) return null;
    return <Image values={this.values} handlers={this.handlers} />;
  }
}

BackgroundImageWrapper.propTypes = {
  imageUrl: PropTypes.string,
  isDefaultImage: PropTypes.bool,
  onClick: PropTypes.func,
};

export default BackgroundImageWrapper;
