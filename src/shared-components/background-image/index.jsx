import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { MENU_ITEMS_TITLES, IMAGE_RATIO } from './constants';

import ImageCropper from '../image-crop/index.jsx';
import CoverImage from './image/index.jsx';
import Loader from '../loader/index.jsx';
import PhotoAttribution from '../photo-attribution/index.jsx';

import ImageDropdown from '../profile/image-dropdown/index.jsx';
import LightboxWrapper from '../lightbox-wrapper/index.jsx';

import DataState from './state/data';



@observer
class BackgroundImage extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
    });

    this.onChange = ::this.onChangeHandler;
    this.onImageClickForLightbox = ::this.onImageClickForLightboxHandler;
    this.onCropFinish = ::this.onCropFinishHandler;
    this.onCropCancel = ::this.onCropCancelHandler;
    this.setCropper = ::this.onCropSet;
    this.onImageLightboxClose = ::this.onImageLightboxCloseHandler;
    this.onImageLightboxChange = ::this.onImageLightboxChangeHandler;

    this.menuItems = [];
    if (props.isCreating) {
      this.menuItems.push({
        title: MENU_ITEMS_TITLES.UPDATE_CURRENT,
        action: ::this.onUpdateCurrentClickHandler,
      });
    }
    this.menuItems.push(...[
      {
        title: MENU_ITEMS_TITLES.UPDATE,
        action: ::this.onUpdateClickHandler,
      },
      {
        title: MENU_ITEMS_TITLES.DELETE,
        action: ::this.onDeleteClickHandler,
      },
    ]);
  }
  
  componentDidMount() {
    this.data.setImageUrl(this.props.imageUrl);
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.setImageUrl(nextProps.imageUrl);
  }
  
  onCropSet(value) {
    this.data.setCropperHandler(value);
  }
  
  onChangeHandler(e) {
    let files;
    
    e.preventDefault();
    
    if (e.dataTransfer) files = e.dataTransfer.files;
    else if (e.target) files = e.target.files;
    
    if (!files || !files[0]) return;
    
    const reader = new FileReader();
    
    reader.onload = () => { this.data.setImage(reader.result); };
    reader.readAsDataURL(files[0]);
    
    this.data.setCropVisibility(true);
  }

  onUpdateCurrentClickHandler() {
    this.data.setCropVisibility(true);
  }
  
  onUpdateClickHandler() {
    if (this.data.isCropVisible) return;
  
    this.data.cropper = {};
    
    const uploadInput = document.getElementById('upload-input');
    uploadInput.value = null;
    uploadInput.click();
  }
  
  onDeleteClickHandler() {
    if (!this.props.onDelete) return Promise.resolve();
    return this.props.onDelete()
    .then(() => { this.data.setIsDefaultImage(true); this.data.setImageUrl(); return Promise.resolve(); })
    .catch(console.error);
  }
  
  onImageClickForLightboxHandler() {
    if (toJS(this.data.isDefaultImage)) return;
    this.data.setIsLightboxOpen(true);
  }
  
  onImageLightboxCloseHandler() {
    this.data.setIsLightboxOpen(false);
  }

  onImageLightboxChangeHandler(value) {
    this.data.setImageIdx(value);
  }
  
  onCropFinishHandler() {
    this.data.setIsImageUploadingPerforms(true);
    
    const croppedImage = this.data.cropper.getCroppedCanvas().toDataURL('image/jpeg');
    this.data.setCroppedImage(croppedImage);
    
    this.props.onChange(this.data.croppedImage)
    .then(url => {
      this.data.setImageUrl(url);
      this.data.setIsImageUploadingPerforms(false);
    });
    
    this.data.setCropVisibility(false);
  }

  onCropCancelHandler() {
    this.data.setCropVisibility(false);
  }
  
  render() {
    const { isCurrentUser, attributions } = this.props;
    const { image, imageUrl, isLightboxOpen, isDefaultImage, isCropVisible, isImageUploadingPerforms } = this.data;

    const photoAttributionsValues = {
      idx: this.data.imageIdx,
      data: attributions,
    };

    const lightboxImages = !imageUrl && toJS(isDefaultImage) ? null : [{ src: imageUrl, caption: attributions && <PhotoAttribution values={photoAttributionsValues} /> }];

    return (
      <div className='profile-background-image'>
        { isImageUploadingPerforms && <Loader /> }
        { isCurrentUser && <input id='upload-input' type='file' onChange={this.onChange} /> }
        <CoverImage imageUrl={imageUrl} isDefaultImage={isDefaultImage} onClick={this.onImageClickForLightbox} />
        <ImageDropdown currentUserProfile={isCurrentUser} menuItems={this.menuItems} isDefaultImage={isDefaultImage} parentClassName='profile-background-image' />
        <ImageCropper src={image} horizontalRatio={IMAGE_RATIO.W/IMAGE_RATIO.H} verticalRatio={IMAGE_RATIO.H/IMAGE_RATIO.W} isVisible={!!isCropVisible} setCropper={this.setCropper} onCropFinish={this.onCropFinish} onCancel={this.onCropCancel} />
        { lightboxImages && <LightboxWrapper open={isLightboxOpen} images={lightboxImages} onClose={this.onImageLightboxClose} onIdxChange={this.onImageLightboxChange} /> }
      </div>
    );
  }
}

BackgroundImage.propTypes = {
  isCreating: PropTypes.bool,
  isCurrentUser: PropTypes.bool,
  imageUrl: PropTypes.string,
  attributions: PropTypes.array,
  type: PropTypes.string,
  onChange: PropTypes.func,
  onClickForLightboxOpen: PropTypes.func,
  onDelete: PropTypes.func,
};

BackgroundImage.defaultProps = {
  onClickForLightboxOpen: () => {},
};

export default BackgroundImage;
