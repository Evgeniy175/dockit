export const IMAGE_RATIO = {
  H: 9,
  W: 16,
};

export const MENU_ITEMS_TITLES = {
  UPDATE_CURRENT: 'Edit',
  UPDATE: 'Upload new image',
  DELETE: 'Delete'
};
