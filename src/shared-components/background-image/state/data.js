import { action, observable, toJS } from 'mobx';

import DataState from '../../../utils/state/data';

import DefaultImage from '../../../assets/images/default-images/event-background-large.jpg';



class ProfileBackgroundImageDataState extends DataState {
  @observable isLightboxOpen = false;
  
  @observable isCropVisible = false;
  @observable isImageUploadingPerforms = false;
  
  @observable image = '';
  @observable croppedImage = {};
  
  cropper = {};
  @observable imageUrl = '';
  @observable imageIdx = 0;
  
  @observable isDefaultImage = true;
  
  constructor(config = {}) {
    super(config);
  }

  @action
  setImageUrl(value) {
    this.imageUrl = value || DefaultImage;
    this.setIsDefaultImage(toJS(this.imageUrl) === DefaultImage);
  }

  @action
  setImageIdx(value) {
    this.imageIdx = value;
  }

  @action
  setImage(value) {
    this.image = value;
  }
  
  @action
  setIsDefaultImage(value) {
    this.isDefaultImage = value;
  }
  
  @action
  setIsLightboxOpen(value) {
    this.isLightboxOpen = value;
  }
  
  @action
  setCropVisibility(value) {
    this.isCropVisible = value;
    this.setCropperHandler(null);
  }
  
  @action
  setCropperHandler(value) {
    this.cropper = value;
  }
  
  @action
  setIsImageUploadingPerforms(value) {
    this.isImageUploadingPerforms = value;
  }
  
  @action
  setCroppedImage(value) {
    this.croppedImage = value;
  }
}

export default ProfileBackgroundImageDataState;
