import { action, computed, observable, toJS } from 'mobx';
import { get } from 'lodash';

import { DEFAULT_TITLE } from '../constants';

import DataState from '../../../utils/state/data';

import CategoriesModel from '../../../models/categories/index'

const categoriesModel = new CategoriesModel();



class CategoriesModalDataState extends DataState {
  @observable categories = [];
  @observable isOpen = false;
  @observable title = '';
  
  constructor(config = {}) {
    super(config);
    this.init(config, true);
  }
  
  init(config, isInitial = false) {
    const data = get(config, 'data');
    this.setIsOpen(get(data, 'isOpen', false));
    this.setTitle(get(data, 'title', DEFAULT_TITLE));

    if (!isInitial) return;

    this.fetch()
    .then(() => this.initCategories(get(data, 'selectedCategories', [])));
  }

  fetch() {
    return categoriesModel.fetch()
    .then(res => { this.setCategories(res); return Promise.resolve(); });
  }
  
  getCheckedCategories() {
    return this.categories.filter(category => category.isChecked);
  }

  @action
  onClick(title) {
    const categories = toJS(this.categories);
    const category = categories.find(category => category.category === title);
    
    if (!category) return;

    category.isChecked = !category.isChecked;
    this.setCategories(categories);
  }

  @action
  setCategories(value) {
    this.categories = value;
  }

  @action
  initCategories(selectedCategories) {
    selectedCategories.forEach(selectedCategory => {
      const category = this.categories.find(item => item.id === selectedCategory.id);
      if (category) category.isChecked = true;
    });
  }
  
  @action
  setIsOpen(value) {
    this.isOpen = value;
  }
  
  @action
  setTitle(value) {
    this.title = value;
  }
}

export default CategoriesModalDataState;
