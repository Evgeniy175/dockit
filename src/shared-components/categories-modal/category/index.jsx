import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import DataState from './state/data';

import CheckedIcon from '../../../assets/images/icons/checked.svg';



@observer
class CategoriesModalItem extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onClickHandler() {
    const handler = get(this.props, 'handlers.onClick');
    
    if (handler) handler(this.data.title);
  }
  
  render() {
    const title = this.data.title;
    const isChecked = toJS(this.data.isChecked);
    
    return (
      <div className='categories-modal-item' onClick={this.onClick}>
        <span className='categories-modal-item-title'>
          { title }
        </span>
        <span className='categories-modal-item-checked'>
          {
            isChecked && <img src={CheckedIcon}/>
          }
        </span>
      </div>
    );
  }
}

CategoriesModalItem.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.object
};

CategoriesModalItem.defaultProps = {
  
};

export default CategoriesModalItem;
