import { action, computed, observable, toJS } from 'mobx';
import { get } from 'lodash';

import DataState from '../../../../utils/state/data';



class CategoriesModalItemDataState extends DataState {
  @observable title = '';
  @observable isChecked = false;
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }
  
  init(config) {
    const data = get(config, 'data');
    this.setTitle(get(data, 'category'));
    this.setIsChecked(get(data, 'isChecked', false));
  }
  
  @action
  setTitle(value) {
    this.title = value;
  }
  
  @action
  setIsChecked(value) {
    this.isChecked = value;
  }
}

export default CategoriesModalItemDataState;
