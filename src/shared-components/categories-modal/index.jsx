import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { get } from 'lodash';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';

import Category from './category/index.jsx';
import DoneButton from '../modal/done-button/index.jsx';

import DataState from './state/data';



@observer
class CategoriesModal extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
    this.onClose = ::this.onCloseHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onClickHandler(title) {
    this.data.onClick(title);
  }
  
  onCloseHandler() {
    const handler = get(this.props, 'handlers.onClose');
    const checkedCategories = this.data.getCheckedCategories();
    
    this.data.setIsOpen(false);

    if (handler) handler(checkedCategories);
  }
  
  render() {
    const isOpen = this.data.isOpen;
    const title = this.data.title;
    const categories = this.data.categories;
    
    const handlers = {
      onClick: this.onClick
    };
    
    return (
      <Modal show={isOpen} onHide={this.onClose} className='categories-modal no-select'>
        <Modal.Header>
          <Modal.Title id={title}>{ title }</Modal.Title>
          <DoneButton />
        </Modal.Header>
        <Modal.Body>
          { categories.map(category => <Category key={ category.category } data={ category } handlers={ handlers } />) }
        </Modal.Body>
      </Modal>
    );
  }
}

CategoriesModal.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.object
};

CategoriesModal.defaultProps = {
  
};

export default CategoriesModal;
