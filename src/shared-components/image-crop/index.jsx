import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Nav, Navbar, NavItem, Grid } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import jq from '../../utils/jquery';

import { CROPPER_NAVBAR_CLASS_NAME, MIN_CONTAINER_WIDTH, MIN_CONTAINER_HEIGHT } from './constants';



@observer
class DockItImageCrop extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { src, isVisible, horizontalRatio, verticalRatio, setCropper } = this.props;
    //const isHorizontalOrientation = window.innerHeight < window.innerWidth;
    const cropperClass = `dockit-image-crop${isVisible ? '' : ' hidden'}`;
    
    return (
      <Grid className={cropperClass}>
        <Cropper className='cropper dockit-cropper center-block centered'
                 src={src}
                 viewMode={2}
                 guides={false}
                 aspectRatio={horizontalRatio}
                 ref={cropper => setCropper(cropper)} />
        <Navbar className={CROPPER_NAVBAR_CLASS_NAME} inverse fixedBottom>
          <Nav>
            <NavItem eventKey={1} className='apply-btn' onClick={this.props.onCropFinish}>Apply</NavItem>
            <NavItem eventKey={2} className='cancel-btn' onClick={this.props.onCancel}>Cancel</NavItem>
          </Nav>
        </Navbar>
      </Grid>
    );
  }
}

DockItImageCrop.propTypes = {
  isVisible: PropTypes.bool,
  src: PropTypes.string.isRequired,
  horizontalRatio: PropTypes.number.isRequired,
  verticalRatio: PropTypes.number.isRequired,
  setCropper: PropTypes.func.isRequired,
  onCropFinish: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

export default DockItImageCrop;
