export const MIN_CONTAINER_WIDTH = 480;
export const MIN_CONTAINER_HEIGHT = 600;

export const CROPPER_NAVBAR_CLASS_NAME = 'cropper-navbar';
