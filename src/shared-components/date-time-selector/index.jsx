import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { INPUT_FORMATS } from '../../constants';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import CalendarDropdown from '../calendar-dropdown/index.jsx';
import TimeInput from '../time/index.jsx';



@observer
class DateTimeSelector extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDateChange = ::this.dateChangeHandler;
  }
  
  dateChangeHandler(value) {
    this.props.dateChangeHandler(value);
  }
  
  render() {
    const { date, time, dateLabel, timeLabel, timeShown, errors } = this.props;
    const { timeChangeHandler } = this.props;
    const className = `date-time-selector ${this.props.className}${get(errors, 'both') ? ' error-text' : ''}`;
    const dateClassName = `date-block${get(errors, 'date') ? ' error-text' : ''}`;

    const timeData = {
      values: {
        label: timeLabel,
        value: time,
        errors,
      },
      handlers: {
        onChange: timeChangeHandler,
      },
    };
    
    return (
      <div className={className}>
        <span className={dateClassName}>
          <label className='date-label' htmlFor='date'>{ dateLabel }</label>
          <CalendarDropdown pattern={INPUT_FORMATS.DATE} value={date} onChange={this.onDateChange} />
        </span>
        { timeShown && <TimeInput values={timeData.values} handlers={timeData.handlers} /> }
      </div>
    );
  }
}

DateTimeSelector.propTypes = {
  isAllDay: PropTypes.bool,
  dateLabel: PropTypes.string,
  timeLabel: PropTypes.string,
  date: PropTypes.string,
  time: PropTypes.string,
  errors: PropTypes.object,
  timeShown: PropTypes.bool,
  dateChangeHandler: PropTypes.func,
  timeChangeHandler: PropTypes.func,
  className: PropTypes.string
};

DateTimeSelector.defaultProps = {
  className: '',
  timeShown: true
};

export default DateTimeSelector;
