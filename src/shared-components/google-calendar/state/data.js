import { action, observable, computed } from 'mobx';

import { CREDENTIALS, SCOPES, DISCOVERY_DOCS } from '../constants';

import { Auth } from '../../../auth';

import DataState from '../../../utils/state/data';

class GoogleCalendarDataState extends DataState {
  constructor(props = {}) {
    super(props);
  
    this.initClient = ::this.initClientHandler;
    this.updateSigninStatus = ::this.updateSigninStatusHandler;
  }
  
  /**
   *  On load, called to load the auth2 library and API client library.
   */
  authorize() {
    gapi.load('client:auth2', this.initClient);
  }
  
  /**
   *  Initializes the API client library and sets up sign-in state
   *  listeners.
   */
  initClientHandler() {
    gapi.client.init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: CREDENTIALS.clientId,
      scope: SCOPES
    })
    .then(() => {
      const auth = gapi.auth2.getAuthInstance();
      Auth.setActiveGoogleAuth(auth);
      
      // Listen for sign-in state changes.
      auth.isSignedIn.listen(this.updateSigninStatus);
      
      // Handle the initial sign-in state.
      this.updateSigninStatus(auth.isSignedIn.get());
    });
  }
  
  /**
   *  Called when the signed in status changes, to update the UI
   *  appropriately. After a sign-in, the API is called.
   */
  updateSigninStatusHandler(isSignedIn) {
    const authorizeButton = document.getElementById('authorize-button');
    const signoutButton = document.getElementById('signout-button');
    
    if (isSignedIn) {
      authorizeButton.style.display = 'none';
      signoutButton.style.display = 'block';
    } else {
      authorizeButton.style.display = 'block';
      signoutButton.style.display = 'none';
    }
  }
}


export default GoogleCalendarDataState;
