import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action, toJS } from 'mobx';
import PropTypes from 'prop-types';

import DataState from './state/data';

@observer
class GoogleCalendar extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
  }
  
  componentWillMount() {
    this.data.authorize();
  
    this.onAuthClick = ::this.handleAuthClickHandler;
    this.onSignoutClick = ::this.handleSignoutClickHandler;
  }
  
  /**
   *  Sign in the user upon button click.
   */
  handleAuthClickHandler() {
    gapi.auth2.getAuthInstance().signIn();
  }
  
  /**
   *  Sign out the user upon button click.
   */
  handleSignoutClickHandler() {
    gapi.auth2.getAuthInstance().signOut();
  }

  render() {
    return (
      <div className='google-calendar-wrapper'>
        <button id="authorize-button" style={{ display: 'none' }} onClick={this.onAuthClick}>Authorize</button>
        <button id="signout-button" style={{ display: 'none' }} onClick={this.onSignoutClick}>Sign Out</button>
      </div>
    );
  }
}

GoogleCalendar.propTypes = {
  
};

export default GoogleCalendar;
