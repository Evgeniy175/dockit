import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../../models/events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../../models/schedules/constants';



@observer
class ReplyActionIcon extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  componentWillUnmount() {
    this.onClick = null;
  }

  onClickHandler() {
    const { comment, onClick } = this.props;
    if (onClick) return onClick(comment);
    const isEvent = !!comment.event_id;
    const target = isEvent ? EVENTS_MODEL_PLURAL : SCHEDULES_MODEL_PLURAL;
    const id = isEvent ? comment.event_id : comment.schedule_id;
    browserHistory.push(`/${target}/${id}?fromUser=${comment.user.username}`);
  }

  render() {
    return <div className='reply-icon-wrapper' onClick={this.onClick}>Reply</div>;
  }
}

ReplyActionIcon.propTypes = {
  comment: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

export default ReplyActionIcon;