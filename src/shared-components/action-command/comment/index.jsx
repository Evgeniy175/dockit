import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import Svg from 'react-svg';

import { formatNumber } from '../../../utils/formatting/number';

import Icon from '../../../assets/images/icons/comment.svg';

@observer
class CommentActionIcon extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { type, id } = this.props.data;
    browserHistory.push(`/${type}/${id}#comments`);
  }

  render() {
    const { data, postfix } = this.props;
    const count = formatNumber(data.count);
    const postfixText = count === 1 ? ' comment' : ' comments';
    
    return (
      <div className='comment action-item' onClick={this.onClick}>
        <div className='action'>
          <Svg path={Icon} className='action-icon no-select' />
        </div>
        <div className='counter'>{count}{postfix ? postfixText : ''}</div>
      </div>
    );
  }
}

CommentActionIcon.protTypes = {
  data: PropTypes.object.isRequired,
  postfix: PropTypes.bool
};

CommentActionIcon.defaultProps = {
  postfix: false
};

export default CommentActionIcon;