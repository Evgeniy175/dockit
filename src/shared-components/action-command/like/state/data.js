import { action, observable, computed, toJS } from 'mobx';

import { get, isEmpty } from 'lodash';
import Promise from 'bluebird';

import { Auth } from '../../../../auth';

import DataState from '../../../../utils/state/data';

import LikeModel from '../../../../models/action-commands/likes';
import UserModel from '../../../../models/users';

import { LOAD_LIMIT } from '../constants';

const likeModel = new LikeModel();
const userModel = new UserModel();



class LikeActionDataState extends DataState {
  isCanBePerformed = true;
  
  @observable isModalOpen = false;
  @observable isUnauthModalOpen = false;
  
  @observable targetType = '';
  @observable targetId = '';
  
  @observable count = 0;
  @observable mine = false;
  
  @observable followings = {};
  isMoreFollowingsAvailable = true;
  
  @observable nonFollowings = {};
  isMoreNonFollowingsAvailable = true;

  get isSigned() {
    return Auth.isSigned();
  }
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }

  init(props) {
    this.setItemInfo(props);
  }
  
  getUsers() {
    const followings = toJS(get(this.followings, 'data', []));
    const nonFollowings = toJS(get(this.nonFollowings, 'data', []));
    return followings.concat(nonFollowings);
  }
  
  canLoadMore() {
    return this.isMoreFollowingsAvailable || this.isMoreNonFollowingsAvailable;
  }
  
  fetchPeople() {
    if (!this.isSigned) return Promise.resolve();

    const config = {
      params: {
        limit: LOAD_LIMIT
      }
    };
    return userModel.fetchFollowingsWhoLiked(this.targetType, this.targetId, config)
    .then(followings => {
      this.setFollowings(followings);
      
      return this.followings.data.length < LOAD_LIMIT
      ? userModel.fetchNonFollowingsWhoLiked(this.targetType, this.targetId, config)
      : Promise.resolve();
    })
    .then(nonFollowings => { if (!isEmpty(nonFollowings)) this.setNonFollowings(nonFollowings); return Promise.resolve(); });
  }
  
  @action
  setFollowings(value) {
    this.followings = value;
    this.isMoreFollowingsAvailable = !!this.followings.nextPage;
  }
  
  @action
  setNonFollowings(value) {
    this.nonFollowings = value;
    this.isMoreNonFollowingsAvailable = !!this.nonFollowings.nextPage;
  }
  
  fetchMorePeople() {
    if (!this.isSigned) return Promise.resolve();

    const config = {
      params: {
        limit: LOAD_LIMIT,
        page: this.followings.nextPage
      }
    };

    if (this.isMoreFollowingsAvailable) {
      return userModel.fetchFollowingsWhoLiked(this.targetType, this.targetId, config)
      .then(followings => { this.attachFollowings(followings); return Promise.resolve(); });
    }
  
    return userModel.fetchNonFollowingsWhoLiked(this.targetType, this.targetId, config)
    .then(nonFollowings => { this.attachNonFollowings(nonFollowings); return Promise.resolve(); });
  }
  
  @action
  attachFollowings(followings) {
    const currFollowings = get(this.followings, 'data', []);
    
    this.followings = {
      data: currFollowings.concat(followings.data),
      nextPage: followings.nextPage
    };
    
    this.isMoreFollowingsAvailable = !!this.followings.nextPage;
  }
  
  @action
  attachNonFollowings(nonFollowings) {
    const currNonFollowings = get(this.nonFollowings, 'data', []);
    
    this.nonFollowings = {
      data: currNonFollowings.concat(nonFollowings.data),
      nextPage: nonFollowings.nextPage
    };
    
    this.isMoreNonFollowingsAvailable = !!this.nonFollowings.nextPage;
  }
  
  fetchedFollowingsHandler(followings) {
    this.setFollowings(followings.data);
    this.isMoreFollowingsAvailable = !!followings.nextPage;
  }
  
  fetchedNonFollowingsHandler(nonFollowings) {
    this.setNonFollowings(nonFollowings.data);
    this.isMoreNonFollowingsAvailable = !!nonFollowings.nextPage;
  }
  
  like() {
    this._addLike();
    
    return likeModel.create({ target: { id: this.targetId, type: this.targetType }})
    .catch(() => { this._removeLike() });
  }
  
  dislike() {
    this._removeLike();
    
    return likeModel.deleteOne({ target: { id: this.targetId, type: this.targetType }})
    .catch(() => { this._addLike() });
  }
  
  _addLike() {
    this.setLikeItem({
      count: this.count + 1,
      mine: true
    });
  }
  
  _removeLike() {
    this.setLikeItem({
      count: this.count - 1,
      mine: false
    });
  }
  
  @action
  setItemInfo(item) {
    this.targetType = item.type;
    this.targetId = item.id;
    this.setLikeItem(item.likes);
  }
  
  @action
  setLikeItem(likes) {
    this.count = get(likes, 'count', 0);
    this.mine = get(likes, 'mine', false);
  }
  
  @action
  setIsModalOpen(value) {
    this.isModalOpen = value;
  }

  @action
  setIsUnauthModalOpen(value) {
    this.isUnauthModalOpen = value;
  }

  @action
  clear() {
    this.isCanBePerformed = null;
    this.isModalOpen = null;
    this.isUnauthModalOpen = null;
    this.targetType = null;
    this.targetId = null;
    this.count = null;
    this.mine = null;
    this.followings = null;
    this.isMoreFollowingsAvailable = null;
    this.nonFollowings = null;
    this.isMoreNonFollowingsAvailable = null;
  }
  
  @action
  shallowClear() {
    this.followings = {};
    this.nonFollowings = {};
    this.isMoreFollowingsAvailable = true;
    this.isMoreNonFollowingsAvailable = true;
  }
}

export default LikeActionDataState;
