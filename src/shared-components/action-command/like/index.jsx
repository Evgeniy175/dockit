import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Svg from 'react-svg';

import Icon from '../../../assets/images/icons/like.svg';
import ActiveIcon from '../../../assets/images/icons/like-active.svg';

import Modal from '../modal/index.jsx';
import UnauthModal from '../../unauthorized/modal/index.jsx';

import { formatNumber } from '../../../utils/formatting/number';
import { handleButton } from '../../../utils/scrolling';
import { TYPES } from '../constants';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class LikeAction extends Component {
  get onIconClickResolver() {
    return this.data.isSigned ? this.onClick : this.onUnauthModalOpen;
  }

  get onPostfixClickResolver() {
    return this.data.isSigned ? this.onPostfixClick : this.onUnauthModalOpen;
  }

  constructor(props) {
    super(props);

    this.onWheel = ::this.onWheelHandler;
    this.onClick = ::this.onClickHandler;
    this.onPostfixClick = ::this.onPostfixClickHandler;
    this.onModalClose = ::this.onModalCloseHandler;
    this.onUnauthModalOpen = ::this.onUnauthModalOpenHandler;
    this.onUnauthModalClose = ::this.onUnauthModalCloseHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
    this.getContainer = ::this.getContainerHandler;
    
    Object.assign(this, {
      data: new DataState(this.props.data),
      ui: new UiState(),
    });
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps.data);
  }
  
  componentWillUnmount() {
    this.onWheel = null;
    this.onClick = null;
    this.onPostfixClick = null;
    this.onModalClose = null;
    this.onUnauthModalOpen = null;
    this.onUnauthModalClose = null;
    this.onKeyDown = null;
    this.onButtonScroll = null;
    this.getContainer = null;
    this.data.clear();
    this.ui = null;
    this.data = null;
  }
  
  onWheelHandler() {
    if (!this.data.canLoadMore()) return;
    
    const isNeedToLoad = this.ui.isNeedToLoadMore();
    
    if (!isNeedToLoad || !this.ui.isLoaded) return;
    
    this.ui.setIsLoaded(false);
    
    return this.data.fetchMorePeople()
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onClickHandler() {
    if (!this.data.isCanBePerformed) return;
  
    this.data.isCanBePerformed = false;
    
    const req = this.data.mine ? this.data.dislike() : this.data.like();
    return req.finally(() => { this.data.isCanBePerformed = true; });
  }
  
  onPostfixClickHandler() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);
    
    this.ui.setIsLoaded(false);
    this.data.setIsModalOpen(true);

    return this.data.fetchPeople()
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onModalCloseHandler() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
    
    this.data.setIsModalOpen(false);
    this.data.shallowClear();
  }

  onUnauthModalOpenHandler() {
    this.data.setIsUnauthModalOpen(true);
  }

  onUnauthModalCloseHandler() {
    this.data.setIsUnauthModalOpen(false);
  }

  onKeyDownHandler(e) {
    return handleButton(e, this.getContainer, this.onButtonScroll);
  }

  onButtonScrollHandler() {
    this.onWheel();
  }

  getContainerHandler() {
    return document.getElementsByClassName('modal-body')[0];
  }

  render() {
    const { isSigned, isUnauthModalOpen, isModalOpen } = this.data;
    const count = formatNumber(this.data.count);
    const isPostfixVisible = this.props.postfix;
    const postfix = count == 1 ? ' like' : ' likes';
    const isLiked = this.data.mine;
    const isLoaded = isSigned && this.ui.isLoaded;
    const users = isSigned && this.data.getUsers();
    return (
      <div className='like action-item'>
        <div className='action' onClick={this.onIconClickResolver}>
          <Svg path={isLiked ? ActiveIcon : Icon} className='like-icon-wrapper' />
        </div>
        <div className='counter no-wrapping-text' onClick={this.onPostfixClickResolver}>
          {`${count}${isPostfixVisible ? postfix : ''}`}
        </div>
        { isSigned && <Modal type={TYPES.LIKE} data={users} open={isModalOpen} loaded={isLoaded} onClose={this.onModalClose} /> }
        { !isSigned && <UnauthModal isOpen={isUnauthModalOpen} onClose={this.onUnauthModalClose} /> }
      </div>
    );
  }
}

LikeAction.propTypes = {
  postfix: PropTypes.bool
};

LikeAction.defaultProps = {
  postfix: false
};

export default LikeAction;
