import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { TITLES, EMPTY_TEXTS } from './constants';
import { TYPES } from '../constants';

import UserList from '../../users/list/index.jsx';
import EventList from '../../event-list/index.jsx';
import CenteredText from '../../centered-text/index.jsx';



@observer
class ActionsModal extends Component {
  RESOLVERS = {
    [TYPES.LIKE]: (users, isLoaded) => <UserList users={users} loaded={isLoaded} onClick={this.onClose} />,
    [TYPES.DOCK]: (users, isLoaded) => <UserList users={users} loaded={isLoaded} onClick={this.onClose} />,
    [TYPES.EVENT_LIST]: (data, isLoaded) => <EventList data={data} loaded={isLoaded} onClick={this.onClose} />
  };
  
  EMPTY_TEXTS_RESOLVERS = {
    [TYPES.LIKE]: isLoaded => <CenteredText text={EMPTY_TEXTS[TYPES.LIKE]} loaded={isLoaded} />,
    [TYPES.DOCK]: isLoaded => <CenteredText text={EMPTY_TEXTS[TYPES.DOCK]} loaded={isLoaded} />,
    [TYPES.EVENT_LIST]: isLoaded => <CenteredText text={EMPTY_TEXTS[TYPES.EVENT_LIST]} loaded={isLoaded} />
  };
  
  constructor(props) {
    super(props);
    this.onClose = ::this.onCloseHandler;
  }
  
  onCloseHandler() {
    this.props.onClose();
  }
  
  render() {
    const { type, open, data, loaded } = this.props;
    const title = TITLES[type];
    const isElemsExists = get(data, 'length') > 0 || get(data, 'dates.length', 0) > 0;
    
    return (
      <Modal className='action-command-modal' show={open} onHide={this.onClose}>
        <Modal.Header closeButton>
          <Modal.Title id={title}>{ title }</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            isElemsExists
            ? this.RESOLVERS[type](data, loaded)
            : this.EMPTY_TEXTS_RESOLVERS[type](loaded)
          }
        </Modal.Body>
      </Modal>
    );
  }
}

ActionsModal.propTypes = {
  type: PropTypes.string.isRequired,
  data: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ]).isRequired,
  open: PropTypes.bool.isRequired,
  loaded: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

export default ActionsModal;
