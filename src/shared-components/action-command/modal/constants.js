import { TYPES } from '../constants.js'

export const TITLES = {
  [TYPES.LIKE]: 'Likes',
  [TYPES.DOCK]: 'Docks',
  [TYPES.EVENT_LIST]: 'Event List'
};

export const EMPTY_TEXTS = {
  [TYPES.LIKE]: 'This hasn\'t been liked yet',
  [TYPES.DOCK]: 'This hasn\'t been docked yet',
  [TYPES.EVENT_LIST]: 'Here is no events'
};
