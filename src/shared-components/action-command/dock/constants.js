export const OUTPUT_TYPES = {
  NORMAL: 'normal',
  TEXT_ONLY: 'text only',
};

export const POSTFIX_POSITIONS = {
  LEFT: 'left',
  RIGHT: 'right',
};

export const LOAD_LIMIT = 20;
export const LOAD_BUFFER_FROM_BOTTOM = 25;
