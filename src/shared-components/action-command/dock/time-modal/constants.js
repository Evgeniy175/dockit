export const TITLE = `If you'd like, choose a different start time to customize your schedule! This change will only apply to you.`;
export const FORMATTER = 'HH:mm';
export const DEFAULT = '00:00';
export const MAX = '23:59:59';
export const LOCALSTORAGE_KEY = 'dock date modal show forbidden';
