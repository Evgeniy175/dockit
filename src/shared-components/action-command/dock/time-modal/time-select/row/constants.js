export const TYPES = {
  ONE_BEFORE: 'one before',
  CURRENT: 'current',
  ONE_AFTER: 'one after',
};

export const FIELDS = {
  H: 'h',
  M: 'm',
  A: 'a',
};

export const FIELD_ORDER = [
  FIELDS.H,
  FIELDS.M,
  FIELDS.A,
];

export const TIME_CONVENTION = {
  AM: 'AM',
  PM: 'PM',
};

export const FIELD_HANDLERS_NAMES = {
  [FIELDS.H]: 'onHourClick',
  [FIELDS.M]: 'onMinuteClick',
  [FIELDS.A]: 'onTimeConventionClick',
};

export const FIELDS_CLASSES = {
  [FIELDS.H]: 'hour-column',
  [FIELDS.M]: 'minutes-column',
  [FIELDS.A]: 'part-of-day-column',
};

export const FIELDS_MAPPING = [
  { key: FIELDS.H, value: FIELDS_CLASSES[FIELDS.H], },
  { key: FIELDS.M, value: FIELDS_CLASSES[FIELDS.M], },
  { key: FIELDS.A, value: FIELDS_CLASSES[FIELDS.A], },
];

export const MOUSE_WHEEL_DIRECTIONS = {
  UP: 'up',
  DOWN: 'down',
};
