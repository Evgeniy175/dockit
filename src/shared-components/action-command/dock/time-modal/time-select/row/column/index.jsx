import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';



@observer
class DockActionTimeSelectColumn extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const handler = get(this.props, 'handlers.onClick');
    if (handler) handler();
  }

  render() {
    const { className, value, isFocused } = this.props.values;

    const classNameResult = `time-select-column no-select${className ? ` ${className}${isFocused ? ' focused-time-select-column' : ''}` : ''}`;

    return (
      <div className={classNameResult} onClick={this.onClick}>
        {value}
      </div>
    );
  }
}

DockActionTimeSelectColumn.propTypes = {
  values: PropTypes.shape({
    className: PropTypes.string,
    value: PropTypes.string,
    isFocused: PropTypes.bool,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func,
  }),
};

export default DockActionTimeSelectColumn;
