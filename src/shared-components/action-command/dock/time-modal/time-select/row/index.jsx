import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Column from './column/index.jsx';

import { TYPES, FIELDS, FIELDS_CLASSES, FIELD_HANDLERS_NAMES, TIME_CONVENTION } from './constants';
import { format } from '../../../../../../utils/formatting/time';



@observer
class DockActionTimeSelectRow extends Component {
  constructor(props) {
    super(props);

    this.PART_OF_DAY_VALUE_RESOLVERS = {
      [TYPES.ONE_BEFORE]: currentValue => currentValue === TIME_CONVENTION.PM ? TIME_CONVENTION.AM : null,
      [TYPES.CURRENT]: currentValue => currentValue,
      [TYPES.ONE_AFTER]: currentValue => currentValue === TIME_CONVENTION.AM ? TIME_CONVENTION.PM : null,
    };
  }

  render() {
    const { className, type, selectedTime } = this.props.values;
    const classNameResult = `time-select-row${className ? ` ${className}` : ''}`;

    const values = format(type, selectedTime);

    const hourData = {
      values: this.getValues(FIELDS.H, values),
      handlers: this.getHandlers(FIELDS.H),
    };

    const minutesData = {
      values: this.getValues(FIELDS.M, values),
      handlers: this.getHandlers(FIELDS.M),
    };

    const partOfDayData = {
      values: this.getDayPartValues(type, values[FIELDS.A]),
      handlers: this.getHandlers(FIELDS.A),
    };

    return (
      <div className={classNameResult}>
        <Column values={hourData.values} handlers={hourData.handlers} />
        <Column values={minutesData.values} handlers={minutesData.handlers} />
        <Column values={partOfDayData.values} handlers={partOfDayData.handlers} />
      </div>
    );
  }

  getDayPartValues(type, currentValue) {
    const { focusedField } = this.props.values;
    const field = FIELDS.A;

    return {
      className: FIELDS_CLASSES[field],
      value: this.PART_OF_DAY_VALUE_RESOLVERS[type](currentValue),
      isFocused: focusedField === field,
    };
  }

  getValues(field, values) {
    const { focusedField } = this.props.values;

    return {
      className: FIELDS_CLASSES[field],
      value: values[field],
      isFocused: focusedField === field,
    };
  }

  getHandlers(field) {
    const { handlers } = this.props;
    return handlers
    ? { onClick: handlers[FIELD_HANDLERS_NAMES[field]] }
    : {};
  }
}

DockActionTimeSelectRow.propTypes = {
  values: PropTypes.shape({
    className: PropTypes.string,
    type: PropTypes.string.isRequired,
    selectedTime: PropTypes.object.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onHourClick: PropTypes.func,
    onMinuteClick: PropTypes.func,
    onTimeConventionClick: PropTypes.func,
    wheelHandlers: PropTypes.object,
  }),
};

export default DockActionTimeSelectRow;
