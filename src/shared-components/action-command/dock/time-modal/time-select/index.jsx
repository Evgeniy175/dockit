import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import moment from 'moment';

import Row from './row/index.jsx';

import { TYPES, FIELDS, FIELD_ORDER, FIELDS_CLASSES, TIME_CONVENTION, MOUSE_WHEEL_DIRECTIONS, FIELDS_MAPPING } from './row/constants';
import { KEY_CODES } from '../../../../../constants';

import { formatPartOfDay } from '../../../../../utils/formatting/time';
import { values } from '../../../../../utils/object';



@observer
class DockActionTimeSelect extends Component {
  @observable focusedField = null;
  touchTarget = null;

  constructor(props) {
    super(props);

    this.onWheel = ::this.onWheelHandler;
    this.onKeyDown = ::this.onKeyDownHandler;

    this.onHoursDown = ::this.onHoursDownHandler;
    this.onHoursUp = ::this.onHoursUpHandler;
    this.onHoursFocus = ::this.onHoursFocusHandler;

    this.onMinutesDown = ::this.onMinutesDownHandler;
    this.onMinutesUp = ::this.onMinutesUpHandler;
    this.onMinutesFocus = ::this.onMinutesFocusHandler;

    this.onPartOfDayUp = ::this.onPartOfDayUpHandler;
    this.onPartOfDayDown = ::this.onPartOfDayDownHandler;
    this.onPartOfDayFocus = ::this.onPartOfDayFocusHandler;

    this.onTouchStart = ::this.onTouchStartHandler;
    this.onTouchMove = ::this.onTouchMoveHandler;

    this.onWheelHandlers = {
      [FIELDS.H]: {
        [MOUSE_WHEEL_DIRECTIONS.UP]: this.onHoursDown,
        [MOUSE_WHEEL_DIRECTIONS.DOWN]: this.onHoursUp,
      },
      [FIELDS.M]: {
        [MOUSE_WHEEL_DIRECTIONS.UP]: this.onMinutesDown,
        [MOUSE_WHEEL_DIRECTIONS.DOWN]: this.onMinutesUp,
      },
      [FIELDS.A]: {
        [MOUSE_WHEEL_DIRECTIONS.UP]: this.onPartOfDayDown,
        [MOUSE_WHEEL_DIRECTIONS.DOWN]: this.onPartOfDayUp,
      },
    };
  }

  onTouchStartHandler(e) {
    this.touchTarget = this.getTargetClass(e);
    if (!this.touchTarget) return;

    e.preventDefault();
    e.stopPropagation();

    this.touchStart = e.touches[0].clientY;
  }

  getTargetClass(e) {
    const className = e.target.className;
    return values(FIELDS_CLASSES).find(item => className.includes(item));
  }

  onTouchMoveHandler(e) {
    if (!this.touchTarget) return;
    const target = FIELDS_MAPPING.find(item => item.value === this.touchTarget);
    if (!target) return;
    const currTouchPosition = e.changedTouches[0].clientY;
    let handled = true;

    e.preventDefault();
    e.stopPropagation();

    if (this.touchStart > currTouchPosition + 5) this.onWheelHandlers[target.key][MOUSE_WHEEL_DIRECTIONS.UP]();
    else if (this.touchStart < currTouchPosition - 5) this.onWheelHandlers[target.key][MOUSE_WHEEL_DIRECTIONS.DOWN]();
    else handled = false;

    if (handled) this.touchStart = currTouchPosition;
  }

  componentDidMount() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);

    const container = document.getElementsByClassName('modal-content')[0];
    container.addEventListener('touchstart', this.onTouchStart, false);
    container.addEventListener('touchmove', this.onTouchMove, false);
  }

  componentWillUnmount() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('mousewheel', this.onWheel);

    const container = document.getElementsByClassName('modal-content')[0];
    container.removeEventListener('touchstart', this.onTouchStart, false);
    container.removeEventListener('touchmove', this.onTouchMove, false);
  }

  onWheelHandler(e) {
    const targetClassName = e.target.className;
    const target = this.getWheelTarget(targetClassName);

    if (!target) return;

    const delta = e.wheelDelta;
    const mouseWheelDirection = delta > 0 ? MOUSE_WHEEL_DIRECTIONS.DOWN : MOUSE_WHEEL_DIRECTIONS.UP;
    const handler = get(this.onWheelHandlers, `${target}.${mouseWheelDirection}`);
    if (handler) handler();
  }

  getWheelTarget(className) {
    const keys = Object.keys(FIELDS_CLASSES);

    for (const key of keys)
      if (className.includes(FIELDS_CLASSES[key]))
        return key;
  }

  onKeyDownHandler(e) {
    if (!this.focusedField) return;

    switch (e.keyCode) {
      case KEY_CODES.ESC: return this.setFocusedField();
      case KEY_CODES.LEFT_ARROW: return this.handleLeftArrow();
      case KEY_CODES.UP_ARROW: return get(this.onWheelHandlers, `${this.focusedField}.${MOUSE_WHEEL_DIRECTIONS.DOWN}`)();
      case KEY_CODES.RIGHT_ARROW: return this.handleRightArrow();
      case KEY_CODES.DOWN_ARROW: return get(this.onWheelHandlers, `${this.focusedField}.${MOUSE_WHEEL_DIRECTIONS.UP}`)();
      default: break;
    }
  }

  handleLeftArrow() {
    const currIdx = this.getCurrentIdx();
    if (currIdx > 0) this.setFocusedField(FIELD_ORDER[currIdx - 1]);
  }

  handleRightArrow() {
    const currIdx = this.getCurrentIdx();
    if (currIdx < FIELD_ORDER.length - 1) this.setFocusedField(FIELD_ORDER[currIdx + 1]);
  }

  getCurrentIdx() {
    return FIELD_ORDER.findIndex(field => field === this.focusedField);
  }

  onHoursDownHandler() {
    this.onChangeHandler(this.getSelectedTime().add(1, 'hours'));
    this.onHoursFocusHandler();
  }

  onHoursUpHandler() {
    this.onChangeHandler(this.getSelectedTime().subtract(1, 'hours'));
    this.onHoursFocusHandler();
  }

  onHoursFocusHandler() {
    this.focusField(FIELDS.H);
  }

  onMinutesDownHandler() {
    this.onChangeHandler(this.getSelectedTime().add(1, 'minutes'));
    this.onMinutesFocusHandler();
  }

  onMinutesUpHandler() {
    this.onChangeHandler(this.getSelectedTime().subtract(1, 'minutes'));
    this.onMinutesFocusHandler();
  }

  onMinutesFocusHandler() {
    this.focusField(FIELDS.M);
  }

  onPartOfDayUpHandler() {
    this.onPartOfDayChangeHandler(TIME_CONVENTION.AM, this.getSelectedTime().add(12, 'hours'));
    this.onPartOfDayFocusHandler();
  }

  onPartOfDayDownHandler() {
    this.onPartOfDayChangeHandler(TIME_CONVENTION.PM, this.getSelectedTime().subtract(12, 'hours'));
    this.onPartOfDayFocusHandler();
  }

  onPartOfDayFocusHandler() {
    this.focusField(FIELDS.A);
  }

  focusField(key) {
    this.setFocusedField(key);
  }

  onPartOfDayChangeHandler(type, newValue) {
    const { selectedTime } = this.props.values;
    if (formatPartOfDay(selectedTime) !== type) return;
    this.onChangeHandler(newValue);
  }

  onChangeHandler(newValue) {
    const handler = get(this.props, 'handlers.onChange');
    if (handler) handler(newValue);
  }

  getSelectedTime() {
    const { selectedTime } = this.props.values;
    return moment(selectedTime);
  }

  @action
  setFocusedField(value) {
    this.focusedField = value;
  }
  
  render() {
    const rowsData = [
      {
        values: this.getValues('before-time', TYPES.ONE_BEFORE),
        handlers: this.getHandlers(this.onHoursUp, this.onMinutesUp, this.onPartOfDayUp),
      },
      {
        values: this.getValues('selected-time', TYPES.CURRENT),
        handlers: this.getHandlers(this.onHoursFocus, this.onMinutesFocus, this.onPartOfDayFocus),
      },
      {
        values: this.getValues('after-time', TYPES.ONE_AFTER),
        handlers: this.getHandlers(this.onHoursDown, this.onMinutesDown, this.onPartOfDayDown),
      },
    ];

    return (
      <div className='time-select-wrapper' id='time-select-wrapper'>
        { rowsData.map((data, idx) => <Row key={idx} values={data.values} handlers={data.handlers} />) }
      </div>
    );
  }

  getValues(className, type) {
    const { selectedTime } = this.props.values;

    return {
      type,
      className,
      selectedTime,
      focusedField: this.focusedField,
    };
  }

  getHandlers(onHourClick, onMinuteClick, onTimeConventionClick) {
    return {
      onHourClick,
      onMinuteClick,
      onTimeConventionClick,
      wheelHandlers: this.onWheelHandlers,
    };
  }
}

DockActionTimeSelect.propTypes = {
  values: PropTypes.shape({
    selectedTime: PropTypes.object.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

export default DockActionTimeSelect;
