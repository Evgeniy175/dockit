import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import moment from 'moment';

import { TITLE, FORMATTER, DEFAULT, LOCALSTORAGE_KEY } from './constants';

import TimeSelect from './time-select/index.jsx';
import Button from '../../../button/index.jsx';
import TimeLine from './time/index.jsx';

import { CSS_MAPPING } from '../../../button/constants';

import { getClosest } from '../../../../utils/moment';



@observer
class DockActionTimeModal extends Component {
  @observable time = DEFAULT;
  startTime = DEFAULT;
  endTime = DEFAULT;

  constructor(props) {
    super(props);

    this.startTime = moment(get(props, 'values.startTime', DEFAULT));
    this.endTime = moment(get(props, 'values.endTime', DEFAULT));
    this.setTime(this.startTime);

    this.onChange = ::this.onChangeHandler;
    this.onClose = ::this.onCloseHandler;
    this.onForbid = ::this.onForbidHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.startTime = moment(get(nextProps, 'values.startTime', DEFAULT));
    this.endTime = moment(get(nextProps, 'values.endTime', DEFAULT));
  }

  onChangeHandler(value) {
    const current = moment(value);
    const isBetween = current.isBetween(this.startTime, this.endTime, 'hour,minute', '[]');

    if (isBetween || this.startTime.isSame(this.endTime)) return this.setTime(current);

    this.setTime(getClosest(current, [this.startTime, this.endTime]));
  }

  onCloseHandler() {
    const { onClose } = this.props.handlers;
    onClose(this.time.format(FORMATTER));
  }

  onForbidHandler() {
    const { onClose } = this.props.handlers;
    localStorage.setItem(LOCALSTORAGE_KEY, true);
    onClose('');
  }

  @action
  setTime(value) {
    this.time = value;
  }
  
  render() {
    const { open } = this.props.values;

    const timeSelectData = {
      values: {
        selectedTime: moment(this.time),
      },
      handlers: {
        onChange: this.onChange,
      },
    };

    return (
      <Modal show={open} onHide={this.onClose} className='dock-time-modal'>
        <Modal.Header closeButton>
          <Modal.Title id={TITLE}>{TITLE}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className='time-modal-content-wrapper'>
            <TimeLine values={this.props.values} />
            <TimeSelect values={timeSelectData.values} handlers={timeSelectData.handlers} />
            <div className='buttons'>
              <Button className={CSS_MAPPING.BLACK} text='Okay' onClick={this.onClose} />
            </div>
            <div className='not-show' onClick={this.onForbid}>Don't show this again</div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

DockActionTimeModal.propTypes = {
  values: PropTypes.shape({
    open: PropTypes.bool.isRequired,
    startTime: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClose: PropTypes.func.isRequired,
  }).isRequired,
};

export default DockActionTimeModal;
