import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import moment from 'moment';



@observer
class DockActionTimeModalTimeLine extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { startTime, endTime } = this.props.values;
    const start = moment(startTime);
    const end = moment(endTime);
    const isAllDay = start.isSame(end);

    return (
      <div className='time-line'>
        { isAllDay && <span>{start.format('lll')}, All Day</span> }
        { !isAllDay && <span>{start.format('lll')} - {end.format('LT')}</span> }
      </div>
    );
  }
}

DockActionTimeModalTimeLine.propTypes = {
  values: PropTypes.shape({
    startTime: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
  }).isRequired,
};

export default DockActionTimeModalTimeLine;
