import { action, computed, observable, toJS } from 'mobx';
import { get, set, map, clone } from 'lodash';
import moment from 'moment';

import DataState from '../../../../utils/state/data';
import IntentModel from '../../../../models/intents';
import PeopleFetch from '../../../../utils/people-fetch';

import { LOAD_LIMIT } from '../constants';
import { TYPES as TARGETS } from '../../../../views/creations/constants';
import { TABS, REQUESTS_TYPE_MAPPING, REQUEST_TYPES } from '../modal/constants';
import { TYPES } from '../../../../models/intents/constants';
import { REQUEST_TARGETS, REQUEST_TARGETS_TYPES } from '../../../../utils/people-fetch/constants';

const intentModel = new IntentModel();
const peopleFetch = new PeopleFetch();



class DockActionDataState extends DataState {
  isCanBePerformed = true;
  isPeopleLoaded = true;
  
  @observable isModalOpen = false;
  @observable isTimeModalOpen = false;
  @observable isUnauthModalOpen = false;

  @observable event = {};
  type;

  @observable followings = {};
  @observable nonFollowings = {};

  @observable activeTab = TABS.DOCKS;
  
  @computed
  get users() {
    const eventFollowings = get(this.followings, 'eventData', []);
    const scheduleFollowings = get(this.followings, 'scheduleData', []);
    const eventNonFollowings = get(this.nonFollowings, 'eventData', []);
    const scheduleNonFollowings = get(this.nonFollowings, 'scheduleData', []);
    return eventFollowings.concat(scheduleFollowings, eventNonFollowings, scheduleNonFollowings);
  }

  @computed
  get count() {
    return get(this.event, 'decorated.intents.docked_count', 0) + get(this.event, 'schedule.decorated.intents.docked_count', 0);
  }

  constructor(config = {}) {
    super(config);
    this.init(config);
    this.setFollowingsHandler = ::this.setFollowings;
    this.setNonFollowingsHandler = ::this.setNonFollowings;
  }

  init(props) {
    this.setEvent(props.event);
    this.type = props.type;
  }
  
  canLoadMore() {
    return get(this.followings, 'eventsNextPage', 0) > 0
        || get(this.followings, 'schedulesNextPage', 0) > 0
        || get(this.nonFollowings, 'eventsNextPage', 0) > 0
        || get(this.nonFollowings, 'schedulesNextPage', 0) > 0;
  }
  
  fetchPeople() {
    this.isPeopleLoaded = false;

    const target = this.getTarget();

    const values = {
      requestType: REQUESTS_TYPE_MAPPING[this.activeTab],
      eventId: target === TARGETS.EVENT ? this.getEventId() : null,
      scheduleId: this.getScheduleId(),
      limit: LOAD_LIMIT,
      config: {
        params: {
          limit: LOAD_LIMIT,
          query: REQUESTS_TYPE_MAPPING[this.activeTab] === REQUEST_TYPES.INTEND ? { intent: this.activeTab, } : {},
        },
      },
    };

    const handlers = {
      setFollowings: ::this.setFollowingsHandler,
      setNonFollowings: ::this.setNonFollowingsHandler,
    };

    return peopleFetch.fetch(values, handlers).then(() => this.isPeopleLoaded = true);
  }

  @action
  setFollowings(eventFollowings, scheduleFollowings) {
    this.followings = {
      eventData: get(eventFollowings, 'data', []),
      scheduleData: get(scheduleFollowings, 'data', []),
      eventsNextPage: get(eventFollowings, 'nextPage'),
      schedulesNextPage: get(scheduleFollowings, 'nextPage'),
    };
  }
  
  @action
  setNonFollowings(eventNonFollowings, scheduleNonFollowings) {
    this.nonFollowings = {
      eventData: get(eventNonFollowings, 'data', []),
      scheduleData: get(scheduleNonFollowings, 'data', []),
      eventsNextPage: get(eventNonFollowings, 'nextPage'),
      schedulesNextPage: get(scheduleNonFollowings, 'nextPage'),
    };
  }
  
  fetchMorePeople() {
    this.isPeopleLoaded = false;

    const values = {
      requestType: REQUESTS_TYPE_MAPPING[this.activeTab],
      eventId: this.getEventId(),
      scheduleId: this.getScheduleId(),
      limit: LOAD_LIMIT,
      activeTab: this.activeTab,
      sizes: {
        [REQUEST_TARGETS.FOLLOWINGS]: {
          [REQUEST_TARGETS_TYPES.EVENTS]: get(this.followings, 'eventData.length', 0),
          [REQUEST_TARGETS_TYPES.SCHEDULES]: get(this.followings, 'scheduleData.length', 0),
        },
        [REQUEST_TARGETS.NON_FOLLOWINGS]: {
          [REQUEST_TARGETS_TYPES.EVENTS]: get(this.nonFollowings, 'eventData.length', 0),
          [REQUEST_TARGETS_TYPES.SCHEDULES]: get(this.nonFollowings, 'scheduleData.length', 0),
        },
      },
      nextPages: {
        [REQUEST_TARGETS.FOLLOWINGS]: {
          [REQUEST_TARGETS_TYPES.EVENTS]: get(this.followings, 'eventsNextPage', 0),
          [REQUEST_TARGETS_TYPES.SCHEDULES]: get(this.followings, 'schedulesNextPage', 0),
        },
        [REQUEST_TARGETS.NON_FOLLOWINGS]: {
          [REQUEST_TARGETS_TYPES.EVENTS]: get(this.nonFollowings, 'eventsNextPage', 0),
          [REQUEST_TARGETS_TYPES.SCHEDULES]: get(this.nonFollowings, 'schedulesNextPage', 0),
        },
      },
      config: {
        params: {
          limit: LOAD_LIMIT,
          query: REQUESTS_TYPE_MAPPING[this.activeTab] === REQUEST_TYPES.INTEND ? { intent: this.activeTab, } : {},
        },
      },
    };

    const handlers = {
      attachFollowings: ::this.attachFollowings,
      attachNonFollowings: ::this.attachNonFollowings,
    };

    return peopleFetch.fetchMore(values, handlers).then(() => this.isPeopleLoaded = true);
  }
  
  @action
  attachFollowings(eventFollowings, scheduleFollowings) {
    const eventsData = map(get(eventFollowings, 'data', []), clone).splice(0, LOAD_LIMIT);
    const schedulesData = map(get(scheduleFollowings, 'data', []), clone).splice(0, LOAD_LIMIT - eventsData.length);
    this.followings = {
      eventData: get(this.followings, 'eventData', []).concat(eventsData),
      scheduleData: get(this.followings, 'scheduleData', []).concat(schedulesData),
      eventsNextPage: get(eventFollowings, 'nextPage', eventFollowings ? LOAD_LIMIT : 0),
      schedulesNextPage: get(scheduleFollowings, 'nextPage', scheduleFollowings ? LOAD_LIMIT : 0),
    };
  }
  
  @action
  attachNonFollowings(eventNonFollowings, scheduleNonFollowings) {
    const limit = get(eventNonFollowings, 'limit', get(scheduleNonFollowings, 'limit', 0));
    const eventsData = map(get(eventNonFollowings, 'data', []), clone).splice(0, LOAD_LIMIT - limit);
    const schedulesData = map(get(scheduleNonFollowings, 'data', []), clone).splice(0, LOAD_LIMIT - limit - eventsData.length);
    const limitExceeded = get(eventNonFollowings, 'limitExceeded', get(scheduleNonFollowings, 'limitExceeded'));
    this.nonFollowings = {
      eventData: get(this.nonFollowings, 'eventData', []).concat(eventsData),
      scheduleData: get(this.nonFollowings, 'scheduleData', []).concat(schedulesData),
      eventsNextPage: get(eventNonFollowings, 'nextPage', limitExceeded ? LOAD_LIMIT : 0),
      schedulesNextPage: get(scheduleNonFollowings, 'nextPage', limitExceeded ? LOAD_LIMIT : 0),
    };
  }

  doDockAction(time) {
    const routing = {
      type: this.getTarget(),
      id: this.getId(),
    };
    this._addDock(routing.type);

    return intentModel.updateIntent({
      routing,
      body: JSON.stringify({
        intent: TYPES.DOCKED,
        arrival_time: this.getArrivalTime(time),
      }),
    })
    .catch(() => this._removeDock(routing.type));
  }

  getArrivalTime(time) {
    if (!time) return;
    time = time.split(':');
    const startTime = moment(get(this.event, 'startTime'));
    return startTime.set({ hour: time[0], minute: time[1], }).format();
  }

  deleteDockAction() {
    const routing = {
      type: this.getTarget(),
      id: this.getId(),
    };
    this._removeDock(routing.type);
    return intentModel.updateIntent({
      routing,
      body: JSON.stringify({
        intent: TYPES.NOT_GOING,
      }),
    })
    .catch(() => this._addDock(routing.type));
  }

  @action
  _addDock(target) {
    const current = get(this.event, 'decorated.intents.docked_count', 0);
    set(this.event, 'decorated.intents.docked_count', current + 1);

    if (this.type === TARGETS.SCHEDULE) return set(this.event, 'decorated.intents.mine.intent', TYPES.DOCKED);

    switch (target) {
      case TARGETS.EVENT: return set(this.event, 'decorated.intents.mine.intent', TYPES.DOCKED);
      case TARGETS.SCHEDULE: return set(this.event, 'schedule.decorated.intents.mine.intent', TYPES.DOCKED);
    }
  }

  @action
  _removeDock(target) {
    const current = get(this.event, 'decorated.intents.docked_count', 0);
    set(this.event, 'decorated.intents.docked_count', current - 1);

    if (this.type === TARGETS.SCHEDULE) return set(this.event, 'decorated.intents.mine.intent', TYPES.NOT_GOING);

    switch (target) {
      case TARGETS.EVENT: return set(this.event, 'decorated.intents.mine.intent', TYPES.NOT_GOING);
      case TARGETS.SCHEDULE: return set(this.event, 'schedule.decorated.intents.mine.intent', TYPES.NOT_GOING);
    }
  }
  
  @action
  setIsModalOpen(value) {
    this.isModalOpen = value;
  }

  @action
  setIsTimeModalOpen(value) {
    this.isTimeModalOpen = value;
  }

  @action
  setIsUnauthModalOpen(value) {
    this.isUnauthModalOpen = value;
  }

  @action
  setEvent(value) {
    this.event = value;
  }

  @action
  clear() {
    this.setFollowingsHandler = null;
    this.setNonFollowingsHandler = null;
    this.isModalOpen = null;
    this.isTimeModalOpen = null;
    this.isUnauthModalOpen = null;
    this.event = null;
    this.type = null;
    this.followings = null;
    this.observable = null;
    this.activeTab = null;
  }
  
  @action
  shallowClear() {
    this.isCanBePerformed = true;
    this.followings = {};
    this.nonFollowings = {};
  }

  @action
  setModalActiveTab(value) {
    if (!this.isPeopleLoaded) return;

    this.activeTab = value;
    return this.fetchPeople();
  }

  getTarget() {
    return this.type === TARGETS.SCHEDULE || this.isDockedSchedule() ? TARGETS.SCHEDULE : TARGETS.EVENT;
  }

  getId() {
    return this.isDockedSchedule() ? this.getScheduleId() : this.getEventId();
  }

  getEventId() {
    return get(this.event, 'id');
  }

  getScheduleId() {
    const target = this.getTarget();
    return get(this.event, 'schedule.id', target === TARGETS.SCHEDULE ? get(this.event, 'id') : null);
  }

  isDocked() {
    return this.isDockedEvent() || this.isDockedSchedule();
  }

  isDockedEvent() {
    return get(this.event, 'decorated.intents.mine.intent') === TYPES.DOCKED;
  }

  isDockedSchedule() {
    return get(this.event, 'schedule.decorated.intents.mine.intent') === TYPES.DOCKED;
  }
}

export default DockActionDataState;
