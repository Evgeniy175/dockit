export const TITLE = 'Guests';
export const ADMIN_TITLE = 'Guests - Admin View';

export const TABS = {
  DOCKS: `docked`,
  MAYBE: `maybe`,
  INVITED: `invited`,
  SAVED: `saved`,
  NOT_GOING: `not_going`,
};

export const REQUEST_TYPES = {
  INTEND: 'intend',
  INVITE: 'invite',
  SAVE: 'save',
};

export const REQUESTS_TYPE_MAPPING = {
  [TABS.DOCKS]: REQUEST_TYPES.INTEND,
  [TABS.MAYBE]: REQUEST_TYPES.INTEND,
  [TABS.INVITED]: REQUEST_TYPES.INVITE,
  [TABS.SAVED]: REQUEST_TYPES.SAVE,
  [TABS.NOT_GOING]: REQUEST_TYPES.INTEND,
};

export const TABS_BY_PERMISSIONS = {
  CREATOR: [
    TABS.DOCKS,
    TABS.MAYBE,
    TABS.INVITED,
    TABS.SAVED,
    TABS.NOT_GOING,
  ],
  INVITED: [
    TABS.DOCKS,
    TABS.MAYBE,
    TABS.INVITED,
  ],
  OTHER: [
    TABS.DOCKS,
  ],
};

export const TABS_TITLES = {
  [TABS.DOCKS]: `Docks`,
  [TABS.MAYBE]: `Maybe`,
  [TABS.INVITED]: `Invited`,
  [TABS.SAVED]: `Saved`,
  [TABS.NOT_GOING]: `Can't go`,
};

export const EMPTY_TEXTS = {
  [TABS.DOCKS]: `Nobody has responded "dock" yet`,
  [TABS.MAYBE]: `Nobody has responded "maybe" yet`,
  [TABS.INVITED]: `No one has been invited yet. Check back soon!`,
  [TABS.SAVED]: `Nobody has responded "save" yet`,
  [TABS.NOT_GOING]: `Nobody has responded "can't go" yet`,
};
