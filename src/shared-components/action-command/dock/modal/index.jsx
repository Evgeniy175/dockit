import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { ADMIN_TITLE, TITLE, TABS, TABS_TITLES, EMPTY_TEXTS, TABS_BY_PERMISSIONS } from './constants';

import UserList from '../../../users/list/index.jsx';
import CenteredText from '../../../centered-text/index.jsx';
import Menu from './menu/index.jsx';



@observer
class DockActionModal extends Component {
  EMPTY_TEXTS_RESOLVERS = {
    [TABS.DOCKS]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.DOCKS]} loaded={isLoaded} />,
    [TABS.MAYBE]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.MAYBE]} loaded={isLoaded} />,
    [TABS.INVITED]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.INVITED]} loaded={isLoaded} />,
    [TABS.SAVED]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.SAVED]} loaded={isLoaded} />,
    [TABS.NOT_GOING]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.NOT_GOING]} loaded={isLoaded} />,
  };
  
  constructor(props) {
    super(props);
  }
  
  render() {
    const { activeTab, open, data, loaded } = this.props.values;
    const { onClose } = this.props.handlers;
    const isDataExists = get(data, 'length') > 0 || get(data, 'dates.length', 0) > 0;
    const menuValues = Object.assign({}, this.props.values, { tabs: this.getTabs() });
    const title = this.getTitle(menuValues.tabs.length, activeTab);
    return (
      <Modal show={open} ope onHide={onClose} className='dock-modal'>
        <Modal.Header closeButton>
          <Modal.Title id={title}>{ title }</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Menu values={menuValues} handlers={this.props.handlers} />
          {
            isDataExists
            ? <UserList users={data} loaded={loaded} onClick={onClose} />
            : this.EMPTY_TEXTS_RESOLVERS[activeTab](loaded)
          }
        </Modal.Body>
      </Modal>
    );
  }

  getTabs() {
    const { isUserCreator, isUserInvited } = this.props.values;
    if (isUserCreator) return TABS_BY_PERMISSIONS.CREATOR;
    if (isUserInvited) return TABS_BY_PERMISSIONS.INVITED;
    return TABS_BY_PERMISSIONS.OTHER;
  }

  getTitle(length, activeTab) {
    if (length === 1) return TABS_TITLES[activeTab];
    return length === 5 ? ADMIN_TITLE : TITLE;
  }
}

DockActionModal.propTypes = {
  values: PropTypes.shape({
    activeTab: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired,
    open: PropTypes.bool.isRequired,
    loaded: PropTypes.bool.isRequired,
    isUserCreator: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onTabChange: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
  }).isRequired,
};

export default DockActionModal;
