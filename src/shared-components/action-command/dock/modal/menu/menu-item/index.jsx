import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { TITLE, TABS, TABS_TITLES, EMPTY_TEXTS } from '../../constants';



@observer
class DockActionModalMenuItem extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { tabKey } = this.props.values;
    const { onClick } = this.props.handlers;
    onClick(tabKey);
  }
  
  render() {
    const { tabKey, active } = this.props.values;
    const className = `dock-action-modal-menu-item${active ? ' active' : ''} ${this.props.values.className}`;

    return (
      <div className={className} onClick={this.onClick}>
        <span className='text'>{ TABS_TITLES[tabKey] }</span>
      </div>
    );
  }
}

DockActionModalMenuItem.propTypes = {
  values: PropTypes.shape({
    tabKey: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    className: PropTypes.string.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default DockActionModalMenuItem;
