import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { TITLE, TABS, TABS_TITLES, EMPTY_TEXTS, TABS_BY_PERMISSIONS } from '../constants';

import MenuItem from './menu-item/index.jsx';



@observer
class DockActionModalMenu extends Component {
  EMPTY_TEXTS_RESOLVERS = {
    [TABS.DOCKS]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.DOCKS]} loaded={isLoaded} />,
    [TABS.MAYBE]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.MAYBE]} loaded={isLoaded} />,
    [TABS.INVITED]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.INVITED]} loaded={isLoaded} />,
    [TABS.SAVED]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.SAVED]} loaded={isLoaded} />,
    [TABS.NOT_GOING]: isLoaded => <CenteredText text={EMPTY_TEXTS[TABS.NOT_GOING]} loaded={isLoaded} />,
  };
  
  constructor(props) {
    super(props);
  }
  
  render() {
    const { tabs, activeTab } = this.props.values;
    const { onTabChange } = this.props.handlers;

    if (tabs.length <= 1) return null;
    
    return (
      <div className='dock-action-modal-menu'>
        { tabs.map(tabKey => <MenuItem key={tabKey} values={this.getMenuItemValues(tabs, tabKey, activeTab)} handlers={{ onClick: onTabChange }} />) }
      </div>
    );
  }

  getMenuItemValues(tabs, tabKey, activeTab) {
    return {
      className: `tabs-${tabs.length}`,
      tabKey: tabKey,
      active: tabKey === activeTab,
    };
  }
}

DockActionModalMenu.propTypes = {
  values: PropTypes.shape({
    tabs: PropTypes.array.isRequired,
    activeTab: PropTypes.string.isRequired,
    isUserCreator: PropTypes.bool.isRequired,
    isUserInvited: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onTabChange: PropTypes.func.isRequired,
  }).isRequired,
};

export default DockActionModalMenu;
