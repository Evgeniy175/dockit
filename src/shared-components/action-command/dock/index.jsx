import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import Svg from 'react-svg';

import Icon from '../../../assets/images/icons/dock.svg';
import ActiveIcon from '../../../assets/images/icons/dock-active.svg';

import Modal from './modal/index.jsx';
import TimeModal from './time-modal/index.jsx';
import UnauthModal from '../../unauthorized/modal/index.jsx';

import { formatNumber } from '../../../utils/formatting/number';
import { handleButton } from '../../../utils/scrolling';

import { OUTPUT_TYPES, POSTFIX_POSITIONS } from './constants';
import { LOCALSTORAGE_KEY } from './time-modal/constants';

import DataState from './state/data';
import UiState from './state/ui';

import { Auth } from '../../../auth';



@observer
class DockAction extends Component {
  constructor(props) {
    super(props);

    Object.assign(this, {
      data: new DataState(this.props.data),
      ui: new UiState(),
    });

    this.OUTPUT_HANDLERS = {
      [OUTPUT_TYPES.NORMAL]: ::this.getNormalOutput,
      [OUTPUT_TYPES.TEXT_ONLY]: ::this.getTextOutput,
    };

    this.onWheel = ::this.onWheelHandler;
    this.onDockIconClick = ::this.onDockIconClickHandler;
    this.onSubmit = ::this.onSubmitHandler;
    this.onModalOpen = ::this.onModalOpenHandler;
    this.onModalTabChange = ::this.onModalTabChangeHandler;
    this.onModalClose = ::this.onModalCloseHandler;
    this.onUnauthModalOpen = ::this.onUnauthModalOpenHandler;
    this.onUnauthModalClose = ::this.onUnauthModalCloseHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.getContainer = ::this.getContainerHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps.data);
  }
  
  componentWillUnmount() {
    this.onWheel = null;
    this.onDockIconClick = null;
    this.onSubmit = null;
    this.onModalOpen = null;
    this.onModalTabChange = null;
    this.onModalClose = null;
    this.onUnauthModalOpen = null;
    this.onUnauthModalClose = null;
    this.onKeyDown = null;
    this.getContainer = null;
    this.onButtonScroll = null;
    this.data.clear();
    this.data = null;
    this.ui = null;
  }
  
  onWheelHandler() {
    if (!this.data.canLoadMore()) return;
  
    const isNeedToLoad = this.ui.isNeedToLoadMore();
    
    if (!isNeedToLoad || !this.ui.isLoaded) return;
    
    this.ui.setIsLoaded(false);
    
    return this.data.fetchMorePeople()
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }

  onDockIconClickHandler() {
    const isModalForbidden = localStorage.getItem(LOCALSTORAGE_KEY);
    return isModalForbidden || this.data.isDocked() ? this.onSubmitHandler() : this.data.setIsTimeModalOpen(true);
  }

  onSubmitHandler(time) {
    if (!this.data.isCanBePerformed) return;

    this.data.setIsTimeModalOpen(false);
    this.data.isCanBePerformed = false;

    return (this.data.isDocked() ? this.data.deleteDockAction() : this.data.doDockAction(time))
    .finally(() => { this.data.isCanBePerformed = true; });
  }
  
  onModalOpenHandler() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);
  
    this.ui.setIsLoaded(false);
    this.data.setIsModalOpen(true);
    return this.data.fetchPeople()
    .finally(() => this.ui.setIsLoaded(true));
  }

  onModalTabChangeHandler(value) {
    this.ui.setIsLoaded(false);

    this.data.setModalActiveTab(value)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onModalCloseHandler() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
    
    this.data.setIsModalOpen(false);
    this.data.shallowClear();
  }

  onUnauthModalOpenHandler() {
    this.data.setIsUnauthModalOpen(true);
  }

  onUnauthModalCloseHandler() {
    this.data.setIsUnauthModalOpen(false);
  }

  onKeyDownHandler(e) {
    return handleButton(e, this.getContainer, this.onButtonScroll);
  }

  getContainerHandler() {
    return document.getElementsByClassName('modal-body')[0];
  }

  onButtonScrollHandler() {
    this.onWheel();
  }

  render() {
    const { activeTab, event, users, isModalOpen, isTimeModalOpen, isUnauthModalOpen } = this.data;
    const { isLoaded } = this.ui;
    const outputType = get(this.props, 'data.outputType', OUTPUT_TYPES.NORMAL);
    const isSigned = Auth.isSigned();
    const activeUser = isSigned && Auth.getActiveUser();

    const modalData = isSigned && {
      values: {
        activeTab,
        data: toJS(users),
        open: toJS(isModalOpen),
        loaded: toJS(isLoaded),
        isUserCreator: isSigned && get(this.data, 'event.user_id') === activeUser.id,
        isUserInvited: !!get(this.data, 'event.decorated.intents.mine')
      },
      handlers: {
        onTabChange: this.onModalTabChange,
        onClose: this.onModalClose,
      },
    };
    const timeModalData = isSigned && {
      values: {
        open: toJS(isTimeModalOpen),
        startTime: get(event, 'startTime'),
        endTime: get(event, 'endTime'),
      },
      handlers: {
        onClose: this.onSubmit,
      },
    };
    
    return (
      <div className='dock action-item'>
        { this.OUTPUT_HANDLERS[outputType]() }
        { isSigned && modalData.values.open && <Modal values={modalData.values} handlers={modalData.handlers} /> }
        { isSigned && timeModalData.values.open && <TimeModal values={timeModalData.values} handlers={timeModalData.handlers} /> }
        { !isSigned && <UnauthModal isOpen={isUnauthModalOpen} onClose={this.onUnauthModalClose} /> }
      </div>
    );
  }

  getNormalOutput() {
    const { postfixPosition } = this.props;
    const isDocked = this.data.isDocked();
    const handler = Auth.isSigned() ? this.onDockIconClick : this.onUnauthModalOpen;
    return (
      <div className='dock-action-output-wrapper'>
        { postfixPosition === POSTFIX_POSITIONS.LEFT && this.getPostfixText() }
        <div className='action' onClick={handler}>
          <Svg path={isDocked ? ActiveIcon : Icon} className='dock-icon-wrapper' />
        </div>
        { postfixPosition === POSTFIX_POSITIONS.RIGHT && this.getPostfixText() }
      </div>
    );
  }

  getPostfixText() {
    const isCountShown = this.props.countShown;
    const count = formatNumber(this.data.count);
    const isPostfixVisible = this.props.postfix;
    const postfix = count == 1 ? ' dock' : ' docks';
    const handler = Auth.isSigned() ? this.onModalOpen : this.onUnauthModalOpen;
    return (
      <div className='counter no-wrapping-text' onClick={handler}>
        { isCountShown && `${count}${isPostfixVisible ? postfix : ''}` }
      </div>
    );
  }

  getTextOutput() {
    const text = get(this.props, 'data.text', '');
    const handler = Auth.isSigned() ? this.onModalOpen : this.onUnauthModalOpen;
    return <div className='dock-action-output-wrapper link' onClick={handler}>{ text }</div>;
  }
}

DockAction.propTypes = {
  data: PropTypes.shape({
    item: PropTypes.object,
    type: PropTypes.string,
    text: PropTypes.string,
    outputType: PropTypes.string,
  }).isRequired,
  docked: PropTypes.bool,
  countShown: PropTypes.bool,
  postfix: PropTypes.bool,
  postfixPosition: PropTypes.string,
};

DockAction.defaultProps = {
  docked: false,
  countShown: true,
  postfix: false,
  postfixPosition: POSTFIX_POSITIONS.LEFT,
};

export default DockAction;