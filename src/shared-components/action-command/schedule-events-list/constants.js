export const LOAD_LIMIT = 20;
export const LOAD_BUFFER_FROM_BOTTOM = 10;
export const FORMATTER_DATE_TEMP = 'YYYY-MM-DD';
