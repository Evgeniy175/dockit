import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Svg from 'react-svg';

import Icon from '../../../assets/images/icons/schedule-events-list.svg';

import Modal from '../modal/index.jsx';
import UnauthModal from '../../unauthorized/modal/index.jsx';

import { TYPES } from '../constants';
import { handleButton } from '../../../utils/scrolling';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class ScheduleEventListAction extends Component {
  get onClickResolver() {
    return this.onClick;
  }

  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(this.props),
      ui: new UiState(),
    });

    this.onClick = ::this.onClickHandler;
    this.onWheel = ::this.onWheelHandler;
    this.onModalClose = ::this.onModalCloseHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
    this.getContainer = ::this.getContainerHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps.schedule);
  }
  
  componentWillUnmount() {
    this.data.clear();
    this.ui.clear();
    this.data = null;
    this.ui = null;
    this.onClick = null;
    this.onWheel = null;
    this.onModalClose = null;
    this.onKeyDown = null;
    this.onButtonScroll = null;
    this.getContainer = null;
  }
  
  onClickHandler() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);
  
    this.ui.setIsLoaded(false);
    this.ui.setIsModalOpen(true);
    this.data.init(this.props.schedule);
  
    return this.data.fetchDates()
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onWheelHandler() {
    if (!this.data.isMoreAvailable) return;
    
    const isNeedToLoad = this.ui.isNeedToLoadMore();
    
    if (!isNeedToLoad || !this.ui.isLoaded) return;
    
    this.ui.setIsLoaded(false);
    
    return this.data.fetchMoreDates()
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onModalCloseHandler() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
    
    this.ui.setIsModalOpen(false);
    this.data.shallowClear();
    this.ui.shallowClear();
  }

  onKeyDownHandler(e) {
    return handleButton(e, this.getContainer, this.onButtonScroll);
  }

  onButtonScrollHandler() {
    this.onWheel();
  }

  getContainerHandler() {
    return document.getElementsByClassName('modal-body')[0];
  }

  render() {
    const imageClassName = `action-icon no-select`;
    const isLoaded = this.ui.isLoaded;
    const isModalOpen = this.ui.isModalOpen;
    const data = {
      schedule: this.props.schedule,
      dates: toJS(this.data.mixedEvents)
    };

    return (
      <div className='events-list action-item'>
        <div className='action' onClick={this.onClickResolver}>
          <Svg className={imageClassName} path={Icon} />
        </div>
        <Modal type={TYPES.EVENT_LIST} data={data} open={isModalOpen} loaded={isLoaded} onClose={this.onModalClose} />
      </div>
    );
  }
}

ScheduleEventListAction.propTypes = {
  schedule: PropTypes.object.isRequired
};

ScheduleEventListAction.defaultProps = {
  
};

export default ScheduleEventListAction;
