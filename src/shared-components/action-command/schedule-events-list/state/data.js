import { action, observable, computed, toJS } from 'mobx';

import Promise from 'bluebird';
import moment from 'moment';

import DataState from '../../../../utils/state/data';

import ScheduleModel from '../../../../models/schedules';
import EventModel from '../../../../models/events';

import { Auth } from '../../../../auth';

import { LOAD_LIMIT, FORMATTER_DATE_TEMP } from '../constants';

const scheduleModel = new ScheduleModel();
const eventModel = new EventModel();



class ScheduleEventListActionDataState extends DataState {
  isCanBePerformed = true;
  
  @observable isMoreAvailable = true;
  @observable schedule = {};
  @observable targetId = '';
  @observable startDate = moment().format();
  @observable endDate = moment().format();
  @observable endTime = moment().format();
  @observable dates = [];
  @observable events = [];

  @computed
  get mixedEvents() {
    return this.dates.map(d => {
      const date = moment(d).format(FORMATTER_DATE_TEMP);
      let event = this.events.find(e => moment(e.startTime).format(FORMATTER_DATE_TEMP) === date);
      return {
        id: this.schedule.id,
        title: event ? event.title : this.schedule.title,
        date: moment(event ? event.startTime : date).format(),
        dateOnly: moment(event ? event.startTime : date).format(FORMATTER_DATE_TEMP),
        start: moment(event ? event.startTime : date).format(),
        end: event ? moment(event.endTime).format() : this.endTime,
        schedule: event ? event : this.schedule,
      };
    });
  }

  constructor(config = {}) {
    super(config);
    this.init(config.schedule);
  }
  
  fetchDates() {
    const eventsConfig = {
      params: {
        fromDate: this.startDate,
        toDate: this.endDate,
        max: LOAD_LIMIT,
      },
    };
    return Promise.all([
      eventModel.fetchEventsForSchedule(this.targetId, eventsConfig),
      scheduleModel.fetchVirtualizedEventsForSchedule(this.targetId, this.startDate, this.endDate, LOAD_LIMIT),
    ])
    .spread((events, dates) => {
      this.setEvents(events);
      this.setDates(dates);
      this.initDates();
      return Promise.resolve(dates);
    });
  }

  @action
  setDates(value) {
    this.dates = value || [];
    this.isMoreAvailable = value && value.length >= LOAD_LIMIT;
  }

  @action
  setEvents(value) {
    this.events = value.data || [];
    this.isMoreEventsAvailable = this.events && this.events.length >= LOAD_LIMIT;
  }
  
  fetchMoreDates() {
    if (!this.isMoreAvailable) return Promise.resolve();
    return Promise.all([
      this.isMoreAvailable ? eventModel.fetchEventsForSchedule(this.targetId) : Promise.resolve(),
      this.isMoreAvailable ? scheduleModel.fetchVirtualizedEventsForSchedule(this.targetId, this.startDate, this.endDate, LOAD_LIMIT) : Promise.resolve(),
    ])
    .spread((events, dates) => {
      this.attachEvents(events.data);
      this.attachDates(dates);
      this.initDates();
      return Promise.resolve(dates);
    });
  }

  @action
  attachDates(newDates) {
    if (!newDates || !newDates.length) {
      this.isMoreAvailable = false;
      return;
    }

    this.dates = this.dates.concat(newDates);
    this.isMoreAvailable = newDates && newDates.length === LOAD_LIMIT;
  }

  @action
  attachEvents(newEvents = []) {
    this.events = (this.events || []).concat(newEvents);
  }

  @action
  setSchedule(value) {
    this.schedule = value;
  }
  
  @action
  init(item) {
    this.schedule = item;
    this.targetId = item.id;
    this.startDate = item.startTime;
    this.endDate = item.endDate;
    this.endTime = item.endTime;
  }

  @action
  initDates() {
    const events = this.mixedEvents;
    if (events.length === 0) return;
    const startDateBuff = moment(events[events.length - 1].date).add(1, 'day');
    this.startDate = startDateBuff.format();
  }
  
  @action
  clear() {
    this.shallowClear();
  
    this.isCanBePerformed = true;
    this.targetId = null;
    this.schedule = null;
  }
  
  @action
  shallowClear() {
    this.isMoreAvailable = true;
    this.isMoreEventsAvailable = true;
    this.dates = [];
    this.events = [];
    this.startDate = moment().format();
    this.endDate = moment().format();
    this.endTime = moment().format();
  }
}

export default ScheduleEventListActionDataState;
