import { action, observable, toJS } from 'mobx';

import UIState from '../../../../utils/state/data';

import { LOAD_BUFFER_FROM_BOTTOM } from '../constants';

class ScheduleEventListActionUiState extends UIState {
  @observable isLoaded = false;
  @observable isModalOpen = false;
  
  constructor(props = {}) {
    super(props);
  }
  
  isNeedToLoadMore() {
    const container = document.getElementsByClassName('modal-body')[0];
    
    if (!container) return false;
    
    const fullHeight = container.scrollHeight;
    const currentScrollHeight = container.scrollTop + container.clientHeight;
    const currentPercent = currentScrollHeight / fullHeight * 100;
    return 100 - currentPercent < LOAD_BUFFER_FROM_BOTTOM;
  }
  
  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }
  
  @action
  setIsModalOpen(value) {
    this.isModalOpen = value;
  }
  
  @action
  clear() {
    this.shallowClear();
    
    this.isModalOpen = false;
  }
  
  @action
  shallowClear() {
    this.isLoaded = false;
  }
}

export default ScheduleEventListActionUiState;
