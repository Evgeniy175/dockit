import { action, computed, observable, toJS } from 'mobx';
import { get, isEmpty, isEqual } from 'lodash';

import { DEFAULT_TITLE, ALERTS } from '../constants';

import DataState from '../../../utils/state/data';



class AlertsModalDataState extends DataState {
  @observable alertsGroups = [
    {
      label: 'First Alert',
      alerts: ALERTS,
      isVisible: true,
      isToggleShown: false,
    },
    {
      label: 'Second Alert',
      alerts: ALERTS,
    },
  ];
  @observable isOpen = false;
  @observable title = '';

  constructor(config = {}) {
    super(config);
    this.init(config, true);
  }
  
  init(config, isInitial = false) {
    const data = get(config, 'data');
    this.setIsOpen(get(data, 'isOpen', false));
    this.setTitle(get(data, 'title', DEFAULT_TITLE));

    if (!isInitial) return;

    this.initAlerts(get(data, 'selectedAlerts', []));
  }

  getAlerts() {
    const clonedAlerts = JSON.parse(JSON.stringify(this.alertsGroups.filter(group => group.isVisible)));
    return clonedAlerts.reduce((arr, curr) => {
      const alerts = curr.alerts.filter(alert => alert.isChecked).map(alert => alert.data);
      return arr.concat(alerts);
    }, []);
  }

  @action
  initAlerts(selectedAlerts) {
    if (!selectedAlerts || selectedAlerts.length === 0) return;

    for (let i = 0; i < selectedAlerts.length && i < this.alertsGroups.length; i++) {
      this.alertsGroups[i].isVisible = true;
      this.selectAlert(i, selectedAlerts[i]);
    }
  }

  @action
  selectAlert(idx, alertData) {
    const alerts = this.getAlertByIdx(idx).alerts;
    const isSelectedDisabled = alerts.find(alert => alert.isDisabled && isEqual(alert.data, alertData));

    if (isSelectedDisabled) return;

    this.handleCheckedAlerts(alerts, alertData);
    this.handleDisabledAlerts();
    this.alertsGroups = toJS(this.alertsGroups);
  }

  handleCheckedAlerts(alerts, alertData) {
    alerts.forEach(alert => {
      alert.isChecked = isEqual(alert.data, alertData) ? !alert.isChecked : false;
    });
  }

  handleDisabledAlerts() {
    const selectedAlerts = this.alertsGroups.reduce((set, group) => {
      const selectedAlert = group.alerts.find(alert => alert.isChecked);
      return selectedAlert ? set.add(JSON.stringify(selectedAlert.data)) : set;
    }, new Set());
    this.alertsGroups.forEach(group => {
      group.alerts.forEach(alert => {
        alert.isDisabled = !alert.isChecked && selectedAlerts.has(JSON.stringify(alert.data));
      });
    });
  }

  @action
  setIsOpen(value) {
    this.isOpen = value;
  }
  
  @action
  setTitle(value) {
    this.title = value;
  }

  @action
  toggleVisibility(idx) {
    const alert = this.getAlertByIdx(idx);
    alert.isVisible = !alert.isVisible;
    this.alertsGroups = toJS(this.alertsGroups);
  }

  getAlertByIdx(idx) {
    return idx < 0 || idx >= this.alertsGroups.length ? null : this.alertsGroups[idx];
  }
}

export default AlertsModalDataState;
