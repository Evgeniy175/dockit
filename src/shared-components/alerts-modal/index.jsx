import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import AlertsGroup from './list/index.jsx';
import DoneButton from '../modal/done-button/index.jsx';

import DataState from './state/data';



@observer
class AlertsModal extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
    this.onClick = ::this.onClickHandler;
    this.onClose = ::this.onCloseHandler;
    this.onVisibilityChange = ::this.onVisibilityChangeHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onClickHandler(idx, alertData) {
    this.data.selectAlert(idx, alertData);
  }
  
  onCloseHandler() {
    const handler = get(this.props, 'handlers.onClose');
    const alerts = this.data.getAlerts();
    this.data.setIsOpen(false);
    if (handler) handler(alerts);
  }

  onVisibilityChangeHandler(idx) {
    this.data.toggleVisibility(idx);
  }
  
  render() {
    const { isOpen, title, alertsGroups } = this.data;
    const handlers = {
      onClick: this.onClick,
      onVisibilityChange: this.onVisibilityChange,
    };
    return (
      <Modal show={isOpen} onHide={this.onClose} className='alerts-modal no-select'>
        <Modal.Header>
          <Modal.Title id={title}>{ title }</Modal.Title>
          <DoneButton />
        </Modal.Header>
        <Modal.Body>
          { alertsGroups.map((group, idx) => <AlertsGroup key={group.label} data={Object.assign({ idx }, group)} handlers={handlers} />) }
        </Modal.Body>
      </Modal>
    );
  }
}

AlertsModal.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.object
};

AlertsModal.defaultProps = {};

export default AlertsModal;
