import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import DataState from './state/data';

import Toggle from '../../toggle/index.jsx';
import Alert from './alert/index.jsx';



@observer
class AlertsModalItemsGroup extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
    this.onClick = ::this.onClickHandler;
    this.onVisibilityChange = ::this.onVisibilityChangeHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onClickHandler(alertData) {
    const handler = get(this.props, 'handlers.onClick');
    if (handler) handler(this.data.idx, alertData);
  }

  onVisibilityChangeHandler() {
    const handler = get(this.props, 'handlers.onVisibilityChange');
    if (handler) handler(this.data.idx);
  }
  
  render() {
    const { isVisible, isToggleShown, alerts, label } = this.data;
    const handlers = Object.assign(Object.assign({}, this.props.handlers), { onClick: this.onClick });
    return (
      <div className='alert-list-group'>
        <Toggle label={label} isBoldLabel={true} isToggleShown={isToggleShown} value={isVisible} onClick={this.onVisibilityChange} />
        { isVisible && alerts.map(alert => <Alert key={alert.title} data={alert} handlers={handlers} />) }
      </div>
    );
  }
}

AlertsModalItemsGroup.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    onVisibilityChange: PropTypes.func.isRequired,
  }).isRequired,
};

AlertsModalItemsGroup.defaultProps = {};

export default AlertsModalItemsGroup;
