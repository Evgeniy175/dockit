import { action, computed, observable, toJS } from 'mobx';
import { get } from 'lodash';

import DataState from '../../../../utils/state/data';



class AlertsModalItemsGroupDataState extends DataState {
  idx;

  @observable label = '';
  @observable isVisible = false;
  @observable isToggleShown = true;
  @observable alerts = [];
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }
  
  init(config) {
    const data = get(config, 'data');
    this.idx = get(data, 'idx');
    this.setLabel(get(data, 'label'));
    this.setIsVisible(get(data, 'isVisible', false));
    this.setToggleVisibility(get(data, 'isToggleShown'));
    this.setAlerts(get(data, 'alerts', false));
  }

  @action
  setLabel(value) {
    this.label = value;
  }

  @action
  setIsVisible(value) {
    this.isVisible = value;
  }

  @action
  setAlerts(value) {
    this.alerts = value;
  }

  @action
  setToggleVisibility(value) {
    this.isToggleShown = value;
  }
}

export default AlertsModalItemsGroupDataState;
