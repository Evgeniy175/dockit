import { action, computed, observable, toJS } from 'mobx';
import { get } from 'lodash';

import DataState from '../../../../../utils/state/data';



class AlertsModalItemDataState extends DataState {
  @observable label = '';
  @observable title = '';
  @observable alertData = '';
  @observable isChecked = false;
  @observable isDisabled = false;
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }
  
  init(config) {
    const data = get(config, 'data');
    this.setTitle(get(data, 'title'));
    this.setAlertData(get(data, 'data'));
    this.setIsChecked(get(data, 'isChecked', false));
    this.setIsDisabled(get(data, 'isDisabled', false));
  }

  @action
  setLabel(value) {
    this.label = value;
  }

  @action
  setTitle(value) {
    this.title = value;
  }

  @action
  setAlertData(value) {
    this.alertData = value;
  }
  
  @action
  setIsChecked(value) {
    this.isChecked = value;
  }

  @action
  setIsDisabled(value) {
    this.isDisabled = value;
  }
}

export default AlertsModalItemDataState;
