import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import DataState from './state/data';

import CheckedIcon from '../../../../assets/images/icons/checked.svg';



@observer
class AlertsModalItem extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onClickHandler() {
    const handler = get(this.props, 'handlers.onClick');
    if (handler) handler(this.data.alertData);
  }
  
  render() {
    const title = this.data.title;
    const isChecked = toJS(this.data.isChecked);
    const onVisibilityChange = get(this.props, 'handlers.onVisibilityChange');
    const wrapperClassName = `alerts-modal-item${this.data.isDisabled ? ' disabled' : ''}`;
    return (
      <div className={wrapperClassName} onClick={this.onClick}>
        <span className='alerts-modal-item-title'>
          { title }
        </span>
          <span className='alerts-modal-item-checked'>
          { isChecked && <img src={CheckedIcon}/> }
        </span>
      </div>
    );
  }
}

AlertsModalItem.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.shape({
    onClick: PropTypes.func,
    onVisibilityChange: PropTypes.func.isRequired,
  }).isRequired,
};

AlertsModalItem.defaultProps = {
  
};

export default AlertsModalItem;
