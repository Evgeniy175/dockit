export const DEFAULT_TITLE = 'Alerts';

export const UNITS = {
  SECONDS: 's',
  MINUTES: 'm',
  HOURS: 'h',
  DAYS: 'd',
  WEEKS: 'w',
  MONTHS: 'M',
  NEVER: 'never'
};

export const MOMENT_UNITS = {
  [UNITS.SECONDS]: 'seconds',
  [UNITS.MINUTES]: 'minutes',
  [UNITS.HOURS]: 'hours',
  [UNITS.DAYS]: 'days',
  [UNITS.WEEKS]: 'weeks',
  [UNITS.MONTHS]: 'months',
};

export const TEXTS_FOR_ONE = {
  [UNITS.SECONDS]: 'second',
  [UNITS.MINUTES]: 'minute',
  [UNITS.HOURS]: 'hour',
  [UNITS.DAYS]: 'day',
  [UNITS.WEEKS]: 'week',
  [UNITS.MONTHS]: 'month',
};

export const TEXTS_FOR_MANY = {
  [UNITS.SECONDS]: 'seconds',
  [UNITS.MINUTES]: 'minutes',
  [UNITS.HOURS]: 'hours',
  [UNITS.DAYS]: 'days',
  [UNITS.WEEKS]: 'weeks',
  [UNITS.MONTHS]: 'months',
};

export const ALERTS = [
  {
    title: 'None',
    data: {
      unit: UNITS.NEVER,
      amount: 0,
    },
  },
  {
    title: 'At time of event',
    data: {
      unit: UNITS.SECONDS,
      amount: 0,
    },
  },
  {
    title: '5 minutes before',
    data: {
      unit: UNITS.MINUTES,
      amount: 5,
    },
  },
  {
    title: '10 minutes before',
    data: {
      unit: UNITS.MINUTES,
      amount: 10,
    },
  },
  {
    title: '15 minutes before',
    data: {
      unit: UNITS.MINUTES,
      amount: 15,
    },
  },
  {
    title: '30 minutes before',
    data: {
      unit: UNITS.MINUTES,
      amount: 30,
    },
  },
  {
    title: '1 hour before',
    data: {
      unit: UNITS.HOURS,
      amount: 1,
    },
  },
  {
    title: '2 hours before',
    data: {
      unit: UNITS.HOURS,
      amount: 2,
    },
  },
  {
    title: '1 day before',
    data: {
      unit: UNITS.DAYS,
      amount: 1,
    },
  },
  {
    title: '1 week before',
    data: {
      unit: UNITS.WEEKS,
      amount: 1,
    },
  },
];

export const DEFAULT_PROFILE_ALERTS = [
  {
    unit: UNITS.HOURS,
    amount: 1,
  }
];
