import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class Validation extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const errors = this.props.errors.map(e => <li key={e}>{e}</li>);
    
    return (
      <Grid fluid={true}>
        <Row>
          <Col xs={12} className='validation'>
            <ul>{ errors }</ul>
          </Col>
        </Row>
      </Grid>
    );
  }
}

Validation.propTypes = {
  errors: PropTypes.array.isRequired
};

export default Validation;
