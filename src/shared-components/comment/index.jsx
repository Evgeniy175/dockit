import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import CommentData from './state/data';

import CommentTop from './top/index.jsx';
import CommentText from './text/index.jsx';
import UserImage from './user-image/index.jsx';
import AttachedImage from './attached-image/index.jsx';
import CommentBottom from './bottom/index.jsx';
import AttachedYoutube from '../attached-youtube/index.jsx';



@observer
class Comment extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new CommentData(),
    });
    this.data.init(props);
    this.onCommentTextClick = ::this.onCommentTextClickHandler;
    this.onEditClick = ::this.onEditClickHandler;

    this.commentTextHandlers = {
      setImageFile: this.onCommentTextClick,
      onTextClick: this.onCommentTextClick,
      onEditFinished: ::this.onEditFinishedHandler,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    this.data = null;
    this.onCommentTextClick = null;
    this.onEditClick = null;
    this.commentTextHandlers = null;
  }

  onCommentTextClickHandler() {
    this.data.setIsCommentTextExpanded(!this.data.isCommentTextExpanded);
  }

  onEditClickHandler() {
    const { comment } = this.data;
    const { isEditing } = this.data;
    this.data.setIsEditing(!isEditing);
    this.props.handlers.onEditStart(comment.id);
  }

  onEditFinishedHandler(comment) {
    this.data.setIsEditing(false);
    this.props.handlers.onEdit(comment);
  }

  render() {
    const { isUserCreation, handlers } = this.props;
    const { comment, isCommentTextExpanded, isEditing } = this.data;
    const className = `view-comment ${comment.id}`;
    const likeActionData = this.data.getLikeActionData();
    const createTime = this.data.getCreatedAgoTime();
    const imageUrl = this.data.getImageUrl();

    const newHandlers = Object.assign({}, Object.assign({}, handlers), {
      onEditClick: this.onEditClick
    });
    const commentTextValues = {
      comment,
      isEditing,
      isCommentTextExpanded,
    };

    return (
      <div className={className}>
        <div>
          <UserImage user={comment.user} />
          <div className='right'>
            <CommentTop comment={comment} isUserCreation={isUserCreation} expanded={isCommentTextExpanded} handlers={newHandlers} onCommentTextClick={this.onCommentTextClick} />
            <CommentText values={commentTextValues} handlers={this.commentTextHandlers} />
            { !isEditing && <AttachedImage img={imageUrl} /> }
            { !isEditing && !imageUrl && <AttachedYoutube text={comment.message} /> }
            <CommentBottom likeActionData={likeActionData} createdTime={createTime} {...this.props} />
          </div>
        </div>
      </div>
    );
  }
}

Comment.propTypes = {
  comment: PropTypes.object.isRequired,
  isUserCreation: PropTypes.bool.isRequired,
  handlers: PropTypes.object,
};

export default Comment;
