import { MODEL as EVENTS_MODEL, MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../models/events/constants';
import { MODEL as SCHEDULES_MODEL, MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../models/schedules/constants';
import { MODEL as COMMENTS_MODEL, MODEL_PLURAL as COMMENTS_MODEL_PLURAL } from '../../models/comments/constants';

export const TARGETS = {
  EVENT: {
    MODEL: EVENTS_MODEL,
    MODEL_PLURAL: EVENTS_MODEL_PLURAL,
  },
  SCHEDULE: {
    MODEL: SCHEDULES_MODEL,
    MODEL_PLURAL: SCHEDULES_MODEL_PLURAL,
  },
  COMMENT: {
    MODEL: COMMENTS_MODEL,
    MODEL_PLURAL: COMMENTS_MODEL_PLURAL,
  },
};