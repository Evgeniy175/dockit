import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Promise from 'bluebird';
import { get } from 'lodash';

import { imageToFormData } from '../../../utils/formatting/image';
import { getRandomInt } from '../../../utils/random';

import EditComment from '../../../shared-components/edit-comment/index.jsx';
import ExpandableText from '../../../shared-components/expandable-text/index.jsx';

import { EMPTY_CHAR } from '../../../constants';

import CommentsModel from '../../../models/comments';

const commentsModel = new CommentsModel();



@observer
class CommentText extends Component {
  @observable isExpandable = true;

  constructor(props) {
    super(props);
    this.id = `comment${getRandomInt()}`;

    this.onTextClick = ::this.onTextClickHandler;
    this.editComment = ::this.editCommentHandler;
  }

  componentDidMount() {
    const elem = document.getElementById(this.id);
    if (!elem) return;
    this.setIsExpandable(elem.offsetHeight !== elem.scrollHeight);
  }

  onTextClickHandler() {
    this.props.handlers.onTextClick();
  }

  editCommentHandler(result) {
    const { comment } = this.props.values;
    result.id = comment.id;

    return this.attachImage(result)
    .then(() => this.sendEditComment(result));
  }

  attachImage(comment) {
    if (!comment.image) return Promise.resolve();

    const formData = imageToFormData(comment.image);
    const config = { body: formData };
    return commentsModel.attachPictureToComment(comment.id, config);
  }

  sendEditComment(newComment) {
    return commentsModel.editComment(newComment.id, newComment.message)
    .then(data => {
      if (newComment.image) newComment.imageUrl = data.imageUrl;
      if (newComment.isImageRemoved) newComment.imageUrl = null;

      this.props.handlers.onEditFinished(newComment);
      return Promise.resolve();
    });
  }

  @action
  setIsExpandable(value) {
    this.isExpandable = value;
  }

  render() {
    const { comment, isEditing, isCommentTextExpanded } = this.props.values;

    if (!isEditing && get(comment, 'message', EMPTY_CHAR) === EMPTY_CHAR) return null;

    const isExpandable = toJS(this.isExpandable);
    const className = `text ${!isCommentTextExpanded && isExpandable ? ' collapsed' : ''}${isExpandable ? ' expandable-comment' : ''}`;

    return (
      <div className={className} id={this.id}>
        {
          isEditing
          ? <EditComment comment={comment} editComment={this.editComment} />
          : <ExpandableText text={toJS(comment.message)} usertags={toJS(comment.userTags)} isNeedToHandle={true} />
        }
      </div>
    );
  }
}

CommentText.propTypes = {
  values: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired,
};

export default CommentText;
