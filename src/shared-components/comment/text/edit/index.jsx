import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class CommentEditing extends Component {
  @observable comment = true;

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmitHandler;
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  onSubmitHandler() {
    this.props.onSubmit();
  }

  init(props) {
    this.setComment(props.comment);
  }

  @action
  setComment(value) {
    this.comment = value;
  }

  render() {
    return (
      <div className='comment-editing'>
        <textarea>{ this.comment.message }</textarea>

      </div>
    );
  }
}

CommentEditing.propTypes = {
  comment: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
};

CommentEditing.defaultProps = {

};

export default CommentEditing;
