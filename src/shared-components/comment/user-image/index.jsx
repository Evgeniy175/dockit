import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import UserImage from '../../creations/event-member-image/index.jsx';


@observer
class CommentUserImage extends Component {
  render() {
    const user = this.props.user;

    return (
      <div className='left'>
        <UserImage user={user} />
      </div>
    );
  }
}

CommentUserImage.propTypes = {
  user: PropTypes.object.isRequired
};

export default CommentUserImage;
