import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import LightboxWrapper from '../../lightbox-wrapper/index.jsx';
import Image from '../../image/index.jsx';

import { WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../image/constants';



@observer
class CommentShowAttachedImage extends Component {
  @observable isOpened = false;

  constructor(props) {
    super(props);
    this.onImageClose = ::this.onImageCloseHandler;

    this.values = {
      src: props.img,
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s16x9,
      prepareWrapper: true,
    };
    this.handlers = {
      onClick: ::this.onImageOpenHandler,
    };
  }

  onImageOpenHandler() {
    this.setIsOpened(true);
  }

  onImageCloseHandler() {
    this.setIsOpened(false);
  }

  @action
  setIsOpened(value) {
    this.isOpened = value;
  }

  render() {
    const { img } = this.props;
    if (!img) return null;
    const lightboxImages = [{ src: img }];
    return (
      <div className='comment-attached-image'>
        <LightboxWrapper open={this.isOpened} images={lightboxImages} onClose={this.onImageClose} />
        <Image values={this.values} handlers={this.handlers} />
      </div>
    );
  }
}

CommentShowAttachedImage.propTypes = {
  img: PropTypes.string
};

export default CommentShowAttachedImage;
