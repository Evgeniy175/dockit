import { action, observable, computed } from 'mobx';
import moment from 'moment';
import { get } from 'lodash';

import DataState from '../../../utils/state/data';

import CommentModel from '../../../models/comments/index';
import UserModel from '../../../models/users/index';

import { formatFromNow } from '../../../utils/formatting/time';
import { imageToFormData } from '../../../utils/formatting/image';

import { TARGETS } from '../constants';

const commentModel = new CommentModel();



class CommentDataState extends DataState {
  @observable comment = {};
  @observable isCommentTextExpanded = false;
  @observable isEditing = false;

  constructor(props = {}) {
    super(props);
    this.addEventComment = ::commentModel.addEventComment;
    this.addScheduleComment = ::commentModel.addScheduleComment;
  }

  init(props) {
    this.setComment(props.comment);
  }

  getTarget() {
    for (const key in TARGETS)
      if (this.comment[`${TARGETS[key].MODEL}_id`])
        return TARGETS[key];
  }

  getCreatedAgoTime() {
    const date = moment(this.comment.created_at);
    return formatFromNow(date);
  }
  
  getImageUrl() {
    return CommentModel.formatImageUrl(this.comment.imageUrl);
  }

  getLikeActionData() {
    return {
      type: TARGETS.COMMENT.MODEL_PLURAL,
      id: get(this.comment, 'id'),
      likes: get(this.comment, 'decorated.likes'),
    };
  }

  @action
  setComment(value) {
    this.comment = value;
  }
  
  @action
  setIsCommentTextExpanded(value) {
    this.isCommentTextExpanded = value;
  }

  @action
  setIsEditing(value) {
    this.isEditing = value;
  }
}

export default CommentDataState;
