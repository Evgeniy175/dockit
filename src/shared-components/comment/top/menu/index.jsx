import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';
import Svg from 'react-svg';
import { get } from 'lodash';

import DotsImage from '../../../../assets/images/three-dots.svg';

import DropdownPositionHelper from '../../../../utils/dropdown-menu/position';

import { Auth } from '../../../../auth';

import { getRandomInt } from '../../../../utils/random';

import CommentsModel from '../../../../models/comments';

const commentsModel = new CommentsModel();



@observer
class CommentMenu extends Component {
  id = `comment-menu${getRandomInt()}`;
  dropdownPositionHelper = new DropdownPositionHelper({
    buttonSelector: `#${this.id}`,
  });

  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onEdit = ::this.onEditHandler;
    this.onDelete = ::this.onDeleteHandler;
    this.onReport = ::this.onReportHandler;
  }

  componentWillUnmount() {
    this.onEdit = null;
    this.onDelete = null;
    this.onReport = null;
    this.id = null;
    this.dropdownPositionHelper.clear();
    this.dropdownPositionHelper = null;
  }

  onEditHandler() {
    const { comment } = this.props.values;
    const handler = this.getEditHandler();
    if (handler) handler(comment.id);
  }

  getEditHandler() {
    return get(this.props.handlers, 'onEditClick');
  }

  onDeleteHandler() {
    const { comment } = this.props.values;
    const handler = get(this.props.handlers, 'onDelete');
    if (handler) handler(comment.id);
    commentsModel.deleteComment(comment.id)
    .then(() => {
      const handler = get(this.props.handlers, 'onRemoved');
      if (handler) handler(comment);
    });
  }
  
  onReportHandler() {
    const { comment } = this.props.values;
    const target = 'comment';
    browserHistory.push(`reports/${target}s/${comment.id}`);
  }
  
  render() {
    const items = this.getItems();
    return (
      <span className='menu'>
        <DropdownButton id={this.id} title={<Svg path={DotsImage} />} onToggle={this.dropdownPositionHelper.onToggle}>
          { items.map(item => item.isShown ? item.render() : null) }
        </DropdownButton>
      </span>
    );
  }

  getItems() {
    const { comment, forceEnableDeleteAction } = this.props.values;
    const currentUser = Auth.getActiveUser();
    const isCurrentUserOwner = Auth.isSigned() && currentUser.id === comment.user_id;
    return [
      {
        isShown: isCurrentUserOwner && !!this.getEditHandler(),
        render: () => <MenuItem key='menuItem1' eventKey='101' onClick={this.onEdit}>Edit</MenuItem>,
      },
      {
        // todo when API support, uncomment
        isShown: /*forceEnableDeleteAction || */isCurrentUserOwner,
        render: () => <MenuItem key='menuItem2' eventKey='102' onClick={this.onDelete}>Delete</MenuItem>,
      },
      {
        isShown: !isCurrentUserOwner,
        render: () => <MenuItem key='menuItem3' eventKey='103' onClick={this.onReport}>Report</MenuItem>,
      },
    ];
  }
}

CommentMenu.propTypes = {
  values: PropTypes.shape({
    comment: PropTypes.object.isRequired,
    forceEnableDeleteAction: PropTypes.bool,
  }).isRequired,
  handlers: PropTypes.object,
};

export default CommentMenu;
