import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

import Menu from './menu/index.jsx';



@observer
class CommentTop extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onCommentTextClick = ::this.onCommentTextClickHandler;
  }
  
  onCommentTextClickHandler() {
    this.props.onCommentTextClick();
  }

  render() {
    const { isUserCreation, handlers } = this.props;
    const comment = toJS(this.props.comment);
    const menuValues = {
      comment,
      forceEnableDeleteAction: isUserCreation,
    };
    return (
      <div className='top'>
        <Link key={comment.id} to={`/users/${comment.user.id}`}>{comment.user.username}</Link>
        <Menu values={menuValues} handlers={handlers} />
      </div>
    );
  }
}

CommentTop.propTypes = {
  comment: PropTypes.object.isRequired,
  isUserCreation: PropTypes.bool,
  expanded: PropTypes.bool.isRequired,
  onCommentTextClick: PropTypes.func.isRequired,
  handlers: PropTypes.object
};

CommentTop.defaultProps = {
  collapsed: false
};

export default CommentTop;
