import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import LikeAction from '../../../shared-components/action-command/like/index.jsx';
import Reply from '../../action-command/reply/index.jsx';



@observer
class CommentBottom extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { comment, likeActionData, createdTime, handlers } = this.props;
    return (
      <div className='bottom'>
        <span className='time no-wrapping-text'>{ createdTime }</span>
        <Reply comment={comment} onClick={handlers.onReply} />
        <LikeAction data={likeActionData} />
      </div>
    );
  }
}

CommentBottom.propTypes = {
  comment: PropTypes.object.isRequired,
  likeActionData: PropTypes.object.isRequired,
  createdTime: PropTypes.string,
  handlers: PropTypes.object.isRequired,
};

export default CommentBottom;
