import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';
import PropTypes from 'prop-types';

import YouTube from 'react-youtube';

import jq from '../../utils/jquery';

import { DEFAULT_HEIGHT, DEFAULT_WIDTH, WRAPPER_ID } from './constants';
import { getRandomInt } from '../../utils/random';



@observer
class YouTubeContainer extends Component {
  player;
  wrapperId;

  constructor(props) {
    super(props);
    this.wrapperId = `${WRAPPER_ID}${getRandomInt()}`;
    this.onReady = ::this.onReadyHandler;
  }

  componentDidMount() {
    const { height } = this.props;
    if (height) return;
    this.prepareWrapper();
  }

  onReadyHandler(event) {
    const { muted, autoplay } = this.props;

    this.player = event.target;

    if (autoplay && this.isVisible()) this.player.playVideo();
    if (muted) this.player.mute();
  }

  prepareWrapper() {
    const wrapper = jq.wrap(`#${this.wrapperId}`);
    wrapper.css('height', Math.round(wrapper.offsetWidth / 16 * 9));
  }
  
  render() {
    const { id, height, width } = this.props;

    const opts = {
      height: height || DEFAULT_HEIGHT,
      width: width || DEFAULT_WIDTH,
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 0,
      },
    };

    return (
      <div id={this.wrapperId} className='dockit-youtube-container no-select'>
        <YouTube videoId={id} opts={opts} onReady={this.onReady} />
      </div>
    );
  }

  isVisible() {
    return jq.wrap(`#${this.wrapperId}`).isVisible();
  }
}

YouTubeContainer.propTypes = {
  id: PropTypes.string.isRequired,
  height: PropTypes.string,
  width: PropTypes.string,
  autoplay: PropTypes.bool,
  muted: PropTypes.bool,
};

export default YouTubeContainer;
