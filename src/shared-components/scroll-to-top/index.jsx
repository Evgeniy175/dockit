import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';

import { isMobileOrTablet } from '../../utils/device';
import jq from '../../utils/jquery';
import ScrollHelper from '../../utils/dom/events/scroll-helper';

import { ID, TEXT, HIDDEN_CLASS, SCROLL_TO_TOP_MIN } from './constants';
import { EVENTS } from '../../utils/dom/events/scroll-helper/constants';



@observer
class ScrollToTop extends Component {
  isMobileOrTablet = isMobileOrTablet();
  isHidden = true;
  wheelHelper;

  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
    this.onScroll = ::this.onScrollHandler;

    this.wheelHelper = new ScrollHelper({
      handlers: {
        [EVENTS.WHEEL.name]: this.onScroll,
        [EVENTS.TOUCH_MOVE.name]: this.onScroll,
        [EVENTS.KEY_DOWN.name]: this.onScroll,
      },
      doWork: false,
    });
  }
  
  componentDidMount() {
    this.onClick();
    this.onScroll();
  }

  componentWillUnmount() {
    this.wheelHelper.dispose();
    this.wheelHelper = null;
    this.onClick = null;
  }
  
  onClickHandler() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.onScroll();
  }

  onScrollHandler() {
    const body = document.body;
    const scrollTop = body.scrollTop;
    const shouldBeHidden = scrollTop < SCROLL_TO_TOP_MIN;
    if ((this.isHidden && shouldBeHidden) || (!this.isHidden && !shouldBeHidden)) return;
    const context = jq.wrap(`#${ID}`);
    this.isHidden = shouldBeHidden;
    if (shouldBeHidden) context.addClass(HIDDEN_CLASS);
    else context.removeClass(HIDDEN_CLASS);
  }

  render() {
    if (this.isMobileOrTablet) return null;
    return (
      <div id={ID} className='scroll-to-top-wrapper hide'>
        <div className='scroll-to-top'>
          <div className='content no-select' onClick={this.onClick}>
            <div className='arrow fa fa-angle-up fa-2x' />
            <div className='text'>{ TEXT }</div>
          </div>
        </div>
      </div>
    );
  }
}

export default ScrollToTop;
