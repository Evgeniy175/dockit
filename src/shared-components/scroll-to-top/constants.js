export const ID = 'scroll-to-top';
export const TEXT = 'Go up';
export const HIDDEN_CLASS = 'hide';
export const SCROLL_TO_TOP_MIN = screen.height;
