import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap-modal';

import Button from '../button/index.jsx';



@observer
class QuestionModal extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClose = ::this.onCloseHandler;
  }
  
  onCloseHandler() {
    const closeHandler = this.props.onClose;
    if (closeHandler) closeHandler();
  }
  
  render() {
    const { title, text, buttons, open } = this.props;
    const className = `question-modal ${this.props.className ? this.props.className : ''}`;
    return (
      <div>
        <Modal show={open} onHide={this.onClose} className={className} id='question-modal'>
          { title && <Modal.Header><Modal.Title>{title}</Modal.Title></Modal.Header> }
          { text && <Modal.Body><div className='question-modal-text'>{text}</div></Modal.Body> }
          <Modal.Footer>
            { buttons.map(btn => <Button key={btn.text} className={btn.className} text={btn.text} onClick={btn.onClick} />) }
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

QuestionModal.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
  ]),
  buttons: PropTypes.array,
  open: PropTypes.bool,
  onClose: PropTypes.func
};

QuestionModal.defaultProps = {
  buttons: [],
  open: false
};

export default QuestionModal;
