import React, {Component} from 'react';

export default class ReachedTheEndText extends Component {
  render() {
    return <div className="end-text">You've reached the end!</div>;
  }
};