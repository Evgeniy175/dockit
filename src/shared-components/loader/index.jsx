import React, { Component } from 'react';
import Svg from 'react-svg';

import Icon from '../../assets/images/loader.svg';



class Loader extends Component {
  render() {
    return <Svg path={Icon} className='loader-icon-wrapper' />;
  }
}

export default Loader;
