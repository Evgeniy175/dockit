import React, { Component } from 'react';
import Svg from 'react-svg';

import Icon from '../../assets/images/loader.svg';



class Loader extends Component {
  render() {
    return (
      <div className='dockit-loader-wrapper'>
        <Svg path={Icon} className='dockit-loader' />
      </div>
    );
  }
}

export default Loader;
