export const CSS_MAPPING = {
  BLUE: 'blue',
  BLUE_FILLED: 'blue-filled',
  GRAY_BLUE: 'gray-blue',
  GRAY_BLUE_FILLED: 'gray-blue-filled',
  RED: 'red',
  RED_FILLED: 'red-filled',
  GREEN: 'green',
  GREEN_FILLED: 'green-filled',
  DARK_GREEN: 'green',
  DARK_GREEN_FILLED: 'green-filled',
  WHITE: 'white',
  WHITE_FILLED: 'white-filled',
  GRAY: 'gray',
  GRAY_FILLED: 'gray-filled',
  BLACK: 'black',
  BLACK_FILLED: 'black-filled',
  TOGGLE: 'toggle'
};
