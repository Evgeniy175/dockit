import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { CSS_MAPPING } from './constants';

@observer
class DockitButton extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler(e) {
    this.props.onClick(e);
  }
  
  render() {
    const text = this.props.text;
    const wrapperClassName = `dockit-button-wrapper no-select ${this.props.className}`;
    const iconShown = this.props.iconShown;
    const type = this.props.type;
    
    return (
      <span className={wrapperClassName} onClick={this.onClick}>
        <input className='dockit-button' type={type} value={text} />
        { iconShown && <i className='fa fa-check' /> }
      </span>
    );
  }
}

DockitButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  iconClassName: PropTypes.string,
  iconShown: PropTypes.bool,
  type: PropTypes.string
};

DockitButton.defaultProps = {
  className: CSS_MAPPING.BLUE,
  iconClassName: '',
  iconShown: false,
  type: 'button'
};

export default DockitButton;
