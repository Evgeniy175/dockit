import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import {OPTION_CLASS_NAME} from './constants';



@observer
class LocationSelectOptionsItem extends Component {
  constructor() {
    super();
    this.onSelect = ::this.onSelectHandler;
  }

  onSelectHandler() {
    this.props.handlers.onSelect(this.props.values.option);
  }

  render() {
    const { isFocused, option } = this.props.values;
    const className = `${OPTION_CLASS_NAME}${isFocused ? ' location-select-option-focused' : ''}`
    return <div className={className} onClick={this.onSelect}>{option.label}</div>;
  }
}

LocationSelectOptionsItem.propTypes = {
  values: PropTypes.shape({
    isFocused: PropTypes.bool.isRequired,
    option: PropTypes.object.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onSelect: PropTypes.func.isRequired,
  }).isRequired,
};

export default LocationSelectOptionsItem;
