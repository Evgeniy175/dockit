import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import Option from './option/index.jsx';

import {CLASS_NAME, PLACEHOLDER_CLASS_NAME, BEHAVIORS, PLACEHOLDERS} from './constants';



@observer
class LocationSelectOptions extends Component {
  get placeholder() {
    const { isSearchPerformed, options } = this.props.values;
    let behavior;
    if (!isSearchPerformed) behavior = BEHAVIORS.NO_REQUEST;
    else if (!options || options.length === 0) behavior = BEHAVIORS.NO_DATA;
    return PLACEHOLDERS[behavior];
  }

  render() {
    const {isFocused} = this.props.values;
    if (!isFocused) return null;
    const placeholder = this.placeholder;
    if (placeholder) return this.renderWrapper(<div className={PLACEHOLDER_CLASS_NAME}>{placeholder}</div>);
    const options = this.renderOptions();
    return this.renderWrapper(options);
  }

  renderOptions() {
    const { values, handlers } = this.props;
    const { options } = values;
    return options.map((option, idx) => <Option key={option.value} values={this.getOptionValues(option, idx)} handlers={handlers} />);
  }

  getOptionValues(option, idx) {
    const { focusedOptionIdx } = this.props.values;
    return {
      option,
      isFocused: idx === focusedOptionIdx,
    };
  }

  renderWrapper(data) {
    return <div className={CLASS_NAME}>{data}</div>;
  }
}

LocationSelectOptions.propTypes = {
  values: PropTypes.shape({
    isFocused: PropTypes.bool.isRequired,
    isSearchPerformed: PropTypes.bool.isRequired,
    options: PropTypes.array.isRequired,
    focusedOptionIdx: PropTypes.number,
  }).isRequired,
  handlers: PropTypes.shape({
    onSelect: PropTypes.func.isRequired,
  }).isRequired,
};

export default LocationSelectOptions;
