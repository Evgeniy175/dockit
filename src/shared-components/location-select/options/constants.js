export const CLASS_NAME = 'location-select-options-wrapper';
export const PLACEHOLDER_CLASS_NAME = 'location-select-options-placeholder';

export const BEHAVIORS = {
  NO_DATA: 'no data',
  NO_REQUEST: 'no request',
};

export const PLACEHOLDERS = {
  [BEHAVIORS.NO_DATA]: 'No locations found...',
  [BEHAVIORS.NO_REQUEST]: 'Search for locations...',
};
