import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import {WRAPPER_CLASS_NAME, CLASS_NAME} from './constants';



@observer
class LocationSelectInput extends Component {
  render() {
    const { value, placeholder } = this.props.values;
    const { onChange, onFocus, onBlur } = this.props.handlers;
    return (
      <div className={WRAPPER_CLASS_NAME}>
        <input className={CLASS_NAME}
               value={value}
               placeholder={placeholder}
               onChange={onChange}
               onFocus={onFocus}
               onBlur={onBlur} />
      </div>
    );
  }
}

LocationSelectInput.propTypes = {
  values: PropTypes.shape({
    value: PropTypes.string,
    placeholder: PropTypes.string,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
  }).isRequired,
};

export default LocationSelectInput;
