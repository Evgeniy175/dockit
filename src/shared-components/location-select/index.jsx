import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Input from './input/index.jsx';
import Options from './options/index.jsx';

import DataState from './state/data';

import {WRAPPER_CLASS_NAME} from './constants';



@observer
class LocationSelect extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
  }

  componentWillReceiveProps(nextProps) {
    this.data.reinit(nextProps)
  }

  componentWillUnmount() {
    this.data.dispose();
    this.data = null;
  }

  render() {
    const { inputValues, inputHandlers, optionsValues, optionsHandlers, value } = this.data;
    return (
      <div className={WRAPPER_CLASS_NAME}>
        <Input values={inputValues} handlers={inputHandlers} />
        <Options values={optionsValues} handlers={optionsHandlers} />
      </div>
    );
  }
}

LocationSelect.propTypes = {
  values: PropTypes.shape({
    placeholder: PropTypes.string,
    value: PropTypes.string,
    selected: PropTypes.object,
  }),
  handlers: PropTypes.shape({
    onSelect: PropTypes.func,
    onInputChange: PropTypes.func,
    onInputFocus: PropTypes.func,
    onInputBlur: PropTypes.func,
  }),
};

export default LocationSelect;
