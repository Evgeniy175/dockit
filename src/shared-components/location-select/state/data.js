import {action, computed, observable, toJS} from 'mobx';
import Promise from 'bluebird';

import LocationModel from '../../../models/locations';

import {getRandomInt} from "../../../utils/random";
import {wrap} from "../../../utils/jquery/dom";
import {isArrow} from "../../../utils/dom/events/keyboard";
import DataState from '../../../utils/state/data';

import {DELAY} from './constants';
import {WRAPPER_CLASS_NAME} from '../constants';
import {KEY_CODES} from '../../../constants';

const locationModel = new LocationModel();



class LocationSelectDataState extends DataState {
  isDisposed = false;

  onFetch;
  onClick;
  onKeyDown;

  @observable isFocused = false;
  @observable isOptionFocused = false;
  @observable isSearchPerformed = false;

  @observable values;
  @observable handlers;

  inputHandlers;
  optionsHandlers;

  @computed
  get value() {
    const value = this.values.get('value');
    const selected = this.selected;
    return selected && selected.label ? selected.label : (value || '');
  }
  @action
  setValue(value) {
    this.values.set('value', value);
    this.setSelected(null);
  }

  @computed
  get options() {
    return toJS(this.values.get('options')) || [];
  }
  @action
  setOptions(value) {
    if (this.isDisposed) return;
    this.values.set('options', value);
    this.setIsSearchPerformed(true);
  }

  @computed
  get selected() {
    return toJS(this.values.get('selected')) || [];
  }
  @action
  setSelected(value) {
    this.values.set('selected', value);
  }

  @computed
  get placeholder() {
    return this.values.get('placeholder');
  }
  @action
  setPlaceholder(value) {
    if (this.isDisposed) return;
    this.values.set('placeholder', value);
  }

  @computed
  get inputValues() {
    const value = this.values.get('value');
    const selected = this.selected;
    return {
      value: selected && selected.label ? selected.label : (value || ''),
      placeholder: this.placeholder,
    };
  }

  @computed
  get isFocusedIdxExists() {
    return this.focusedOptionIdx !== undefined && this.focusedOptionIdx !== null;
  }

  @computed
  get focusedOptionIdx() {
    if (!this.isOptionFocused) return;
    return this.values.get('focusedOptionIdx');
  }
  @action
  setFocusedOptionIdx(value) {
    const isValid = value === null || value === undefined;
    if (isValid) return this.values.set('focusedOptionIdx', value);
    if (value >= this.options.length) value = 0;
    else if (value < 0) value = this.options.length - 1;
    this.values.set('focusedOptionIdx', value);
  }

  @computed
  get optionsValues() {
    return {
      isFocused: this.isFocused,
      isSearchPerformed: this.isSearchPerformed,
      options: this.options,
      focusedOptionIdx: this.focusedOptionIdx,
    };
  }
  
  constructor(props = {}) {
    super(props);
    this.init(props);
    this.onFetch = ::this.fetchLocations;
    this.onClick = ::this.onClickHandler;
    this.onKeyDown = ::this.onKeyDownHandler;

    this.inputHandlers = {
      onChange: ::this.onInputChange,
      onFocus: ::this.onInputFocus,
      onBlur: ::this.onInputBlur,
    };
    this.optionsHandlers = {
      onSelect: ::this.onSelect,
    };

    this.setFocusedOptionIdx(null);
  }

  init(props) {
    this.setValues(props.values);
    this.setHandlers(props.handlers);
    if (this.value) this.fetchLocations();
  }

  reinit(props) {
    const { values } = props;
    if (!values.reset) return;
    this.setPlaceholder(values.placeholder);
    this.setSelected(values.selected);
    this.setValue('');
    if (this.value) this.fetchLocations();
  }

  onInputChange(e) {
    this.setValue(e.target.value);
    this.initFetcher();
    if (this.handlers.onInputChange) this.handlers.onInputChange(e);
  }

  initFetcher() {
    this.fetchId = getRandomInt();
    setTimeout(this.onFetch, DELAY, this.fetchId);
  }

  onInputFocus(e) {
    this.setIsFocused(true);
    if (this.handlers.onInputFocus) this.handlers.onInputFocus(e);
  }

  onInputBlur(e) {
    this.setIsOptionFocused(false);
    if (this.handlers.onInputBlur) this.handlers.onInputBlur(e);
  }

  onSelect(value) {
    this.setSelected(value);
    this.setIsFocused(false);
    if (this.handlers.onSelect) this.handlers.onSelect(value);
  }

  fetchLocations(id) {
    if (this.fetchId !== id) return Promise.resolve();
    return locationModel.getPlaceAutocomplete(this.value)
    .then(res => this.setOptions(this.formatFetched(res)));
  }

  formatFetched(res) {
    return res.map(item => { return { label: item.description, value: item.place_id, }; })
  }

  onClickHandler(e) {
    const target = wrap(e.target);
    const hasParents = target.hasParents(`.${WRAPPER_CLASS_NAME}`);
    if (!hasParents) return this.setIsFocused(false);
  }

  onKeyDownHandler(e) {
    if (e.keyCode === KEY_CODES.TAB || e.keyCode === KEY_CODES.ENTER) return this.setSelectedItem();
    else if (isArrow(e)) return this.handleArrow(e);
    this.setIsOptionFocused(false);
  }

  setSelectedItem() {
    if (!this.isFocusedIdxExists) return this.setIsFocused(false);
    this.setSelected(this.options[this.focusedOptionIdx]);
    this.setIsFocused(false);
  }

  handleArrow({ keyCode }) {
    this.setIsOptionFocused(true);
    if (!this.isFocusedIdxExists) return this.setFocusedOptionIdx(0);
    const isPrev = keyCode === KEY_CODES.UP_ARROW || keyCode === KEY_CODES.LEFT_ARROW;
    this.setFocusedOptionIdx(isPrev ? this.focusedOptionIdx - 1 : this.focusedOptionIdx + 1);
  }

  @action
  setValues(value) {
    this.values = observable.map(value) || {};
  }

  @action
  setHandlers(value) {
    this.handlers = value || {};
  }

  @action
  setIsFocused(value) {
    const handler = value ? 'addEventListener' : 'removeEventListener';
    document.body[handler]('click', this.onClick);
    document.body[handler]('keydown', this.onKeyDown);
    this.isFocused = value;
  }

  @action
  setIsOptionFocused(value) {
    if (this.isOptionFocused === value) return;
    this.isOptionFocused = value;
    this.setFocusedOptionIdx(null);
  }

  @action
  setIsSearchPerformed(value) {
    if (this.isSearchPerformed === value) return;
    this.isSearchPerformed = value;
  }
  
  @action
  dispose() {
    this.isDisposed = true;
    this.values = null;
    this.handlers = null;
    this.onFetch = null;
    this.onClick = null;
    this.onKeyDown = null;
    this.isSearchPerformed = null;
    this.inputHandlers = null;
    this.optionsHandlers = null;
  }
}

export default LocationSelectDataState;
