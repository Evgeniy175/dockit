import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class Input extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const value = this.props.value || '';
    const placeholder = this.props.placeholder;
    const type = this.props.type;
    
    return (
      <div className='form-input'>
        <input type={type} placeholder={placeholder} value={value} onChange={this.props.changeHandler} />
      </div>
    );
  }
}

Input.propTypes = {
  value: PropTypes.string,
  changeHandler: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string
};

Input.defaultProps = {
  type: 'text'
};

export default Input;
