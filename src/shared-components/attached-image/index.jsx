import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Svg from 'react-svg';

import LightboxWrapper from '../lightbox-wrapper/index.jsx';

import RemoveIcon from '../../assets/images/icons/remove-image.svg';



@observer
class CommentAttachedImage extends Component {
  @observable isOpened = false;

  constructor(props) {
    super(props);
    this.removeAttachedImage = ::this.removeAttachedImageHandler;
    this.onImageOpen = ::this.onImageOpenHandler;
    this.onImageClose = ::this.onImageCloseHandler;
  }

  removeAttachedImageHandler() {
    this.props.removeAttachedImage();
  }

  onImageOpenHandler() {
    this.setIsOpened(true);
  }

  onImageCloseHandler() {
    this.setIsOpened(false);
  }

  @action
  setIsOpened(value) {
    this.isOpened = value;
  }

  render() {
    const { imageFile } = this.props;
    const lightboxImages = [{ src: imageFile }];

    return (
      <div className='attached-image'>
        <LightboxWrapper open={this.isOpened} images={lightboxImages} onClose={this.onImageClose} />
        <img className='attached-image-show' src={imageFile} onClick={this.onImageOpen} />
        <span onClick={this.removeAttachedImage}>
          <Svg className='remove' path={RemoveIcon} />
        </span>
      </div>
    );
  }
}

CommentAttachedImage.propTypes = {
  imageFile: PropTypes.string.isRequired,
  removeAttachedImage: PropTypes.func.isRequired
};

export default CommentAttachedImage;
