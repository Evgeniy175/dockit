import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Button } from 'react-bootstrap';

import CommentTextArea from '../textarea/index.jsx';
import PictureUploadButton from './picture-upload-button/index.jsx';
import AttachedImage from '../attached-image/index.jsx';
import UsersDropdown from '../users/dropdown/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class EditComment extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState(props),
      ui: new UiState(),
    }));

    this.data.init(props);

    this.onPictureUploadButtonClick = ::this.onPictureUploadButtonClickHandler;
    this.onCommentChange = ::this.onCommentChangeHandler;
    this.onPictureInputChange = ::this.pictureInputChangeHandler;
    this.onApply = ::this.onApplyHandler;
    this.removeAttachedImage = ::this.removeAttachedImageHandler;
    this.onPaste = ::this.onPasteHandler;
    this.onUsersDropdownSelect = ::this.onUsersDropdownSelectHandler;
    this.onResize = ::this.onResizeHandler;
  }

  componentDidMount() {
    const id = this.ui.getTextAreaId();
    const elem = document.getElementById(id);
    elem.addEventListener('paste', this.onPaste);
  }

  componentWillUnmount() {
    const id = this.ui.getTextAreaId();
    const elem = document.getElementById(id);
    if (!elem) return;
    elem.removeEventListener('paste', this.onPaste);
  }

  getIdOfPictureUploadInput() {
    return `comment-picture-upload-input_${this.props.comment.id}`;
  }

  onPictureUploadButtonClickHandler(e) {
    e.preventDefault();
    const uploadInput = document.getElementById(this.getIdOfPictureUploadInput());
    uploadInput.value = null;
    uploadInput.click();
  }

  onCommentChangeHandler(e) {
    this.data.setComment(e.target.value);
  }

  pictureInputChangeHandler(e) {
    this.data.onPictureInputChange(e);
  }

  onApplyHandler(e) {
    e.preventDefault();
    
    if (this.data.isCommentWindowBlocked) return;
    
    const result = this.data.onPostCommentButtonClick(e);

    if (!result) return Promise.resolve();

    this.data.setIsCommentWindowBlocked(true);

    return this.props.editComment(result)
    .finally(() => {
      this.data.setComment();
      this.data.setImageFile();
      this.data.setIsCommentWindowBlocked(false);
    });
  }

  removeAttachedImageHandler() {
    this.data.removeAttachedImage();
  }

  onPasteHandler(e) {
    const items = e.clipboardData.items;

    for (const item of items) {
      if (item.kind !== 'file') continue;
      const blob = item.getAsFile();
      const reader = new FileReader();
      reader.onload = event => { this.data.setImageFile(event.target.result); };
      reader.readAsDataURL(blob);
      e.preventDefault();
      return;
    }
  }

  onUsersDropdownSelectHandler(userName) {
    this.data.attachUsernameToComment(userName);
    this.ui.setCursorToTheEndInTextArea();
  }

  onResizeHandler() {
    this.ui.setUsersDropdownWidth();
  }

  render() {
    const { comment, usersDropdownValue, isUsersDropdownShown } = this.data;
    const isImageAttached = this.data.isImageAttached();
    const buttonClass = `edit-comment-btn no-select${comment.length || isImageAttached ? ' active' : ''}`;

    const textAreaValues = {
      id: this.ui.getTextAreaId(),
      value: comment,
      rows: 2,
      placeholder: 'Write your comment...',
    };

    const textAreaHandlers = {
      onChange: this.onCommentChange,
    };

    const usersDropdownCss = this.ui.getUsersDropdownCss();
    const isUsersDropdownVisible = toJS(isUsersDropdownShown);

    return (
      <div className='add-comment'>
        <div  className='edit-comment-wrapper'>
          <div className='edit-comment-inner-wrapper'>
            <div className='right'>
              <UsersDropdown css={usersDropdownCss} open={isUsersDropdownVisible} value={usersDropdownValue} onClick={this.onUsersDropdownSelect} />
              <CommentTextArea values={textAreaValues} handlers={textAreaHandlers} />
              <PictureUploadButton id={this.props.comment.id} onChange={this.onPictureInputChange} onClick={this.onPictureUploadButtonClick} />
              <Button className={buttonClass} disabled={this.data.isCommentWindowBlocked} onClick={this.onApply}>Edit</Button>
            </div>
          </div>
          {
            isImageAttached && <AttachedImage imageFile={this.data.imageFile} removeAttachedImage={this.removeAttachedImage} />
          }
        </div>
      </div>
    );
  }
}

EditComment.propTypes = {
  comment: PropTypes.object.isRequired,
  editComment: PropTypes.func.isRequired
};

export default EditComment;
