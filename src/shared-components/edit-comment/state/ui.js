import { action, observable, toJS } from 'mobx';
import uuid from 'uuid/v1';

import jq from '../../../utils/jquery';

import UIState from '../../../utils/state/data';




class FeedUIState extends UIState {
  textAreaId;
  
  constructor(props = {}) {
    super(props);
    this.textAreaId = uuid();
  }

  setUsersDropdownWidth() {
    const css = this.getUsersDropdownCss();
    const list = jq.wrap('#users-dropdown-list');
    if (!list.isValid()) return;
    list.css('width', css.width);
  }

  getUsersDropdownCss() {
    const textarea = jq.wrap(`#${this.getTextAreaId()}`);
    if (!textarea.isValid()) return;
    const width = textarea.width();
    return {
      width: width ? `${width + 5}px` : '',
    };
  }

  setCursorToTheEndInTextArea() {
    const textarea = jq.wrap(`#${this.getTextAreaId()}`);
    if (!textarea.isValid()) return;
    textarea.focus();
  }

  getTextAreaId() {
    return `comment-input_${this.textAreaId}`;
  }
}

export default FeedUIState;
