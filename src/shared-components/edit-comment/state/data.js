import { action, observable, computed } from 'mobx';
import { get } from 'lodash';

import DataState from '../../../utils/state/data';

import { EMPTY_CHAR } from '../../../constants';
import { USERTAG_START } from '../../../utils/formatting/constants';

import CommentsModel from '../../../models/comments';



class EditCommentDataState extends DataState {
  @observable comment = '';
  @observable imageFile = null;

  isImageRemoved = false;
  initialImage = null;

  @observable isCommentWindowBlocked = false;

  @observable isUsersDropdownShown = false;
  @observable usersDropdownValue = '';

  constructor(props = {}) {
    super(props);
    this.initialImage = this.getImageUrl(get(props, 'comment.imageUrl'));
  }

  init(props) {
    const msg = get(props, 'comment.message', '');
    this.setComment(msg === EMPTY_CHAR ? '' : msg);
    this.setImageFile(this.getImageUrl(get(props, 'comment.imageUrl')));
  }

  getImageUrl(id) {
    return CommentsModel.formatImageUrl(id);
  }

  onPictureInputChange(e) {
    let files;

    e.preventDefault();

    if (e.dataTransfer) files = e.dataTransfer.files;
    else if (e.target) files = e.target.files;

    if (!files || !files[0]) return;

    const reader = new FileReader();

    reader.onload = () => {
      const imageUrl = reader.result;
      this.setImageFile(imageUrl);
    };
    reader.readAsDataURL(files[0]);

  }

  onPostCommentButtonClick() {
    const length = this.comment.length;

    if (!length && !this.isImageAttached()) return;

    let data = {
      message: length ? this.comment : EMPTY_CHAR,
      isImageRemoved: this.isImageRemoved
    };

    if (this.imageFile !== this.initialImage) data.image = this.isImageRemoved ? null : this.imageFile;

    return data;
  }

  removeAttachedImage() {
    this.setImageFile();
    this.isImageRemoved = true;
  }

  isImageAttached() {
    return !!this.imageFile;
  }

  @action
  setIsCommentWindowBlocked(value) {
    this.isCommentWindowBlocked = value;
  }

  @action
  setComment(value = '') {
    this.comment = value;
    this.handleCommentChange();
  }

  @action
  setImageFile(value = null) {
    this.imageFile = value;
  }

  handleCommentChange() {
    const idx = this.comment.lastIndexOf(USERTAG_START);

    if (idx === -1) return this.setUsersDropdownVisibility(false);

    const text = this.comment.substring(idx);
    const isUsertagInputting = !/\s/g.test(text);

    if (!isUsertagInputting) return this.setUsersDropdownVisibility(false);
    if (!this.isUsersDropdownShown) this.setUsersDropdownVisibility(true);

    this.setUsersDropdownValue(text);
  }

  @action
  attachUsernameToComment(value = '') {
    const beforeLastUsertagIdx = this.comment.lastIndexOf(USERTAG_START);

    if (beforeLastUsertagIdx === -1) return;

    this.comment = `${this.comment.substring(0, beforeLastUsertagIdx)}${USERTAG_START}${value} `;
    this.setUsersDropdownVisibility(false);
  }

  @action
  setUsersDropdownVisibility(value) {
    this.isUsersDropdownShown = value;
  }

  @action
  setUsersDropdownValue(value) {
    this.usersDropdownValue = value;
  }
}

export default EditCommentDataState;
