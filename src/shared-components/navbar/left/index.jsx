import React, { Component } from 'react';
import { action, observable, useStrict, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Nav } from 'react-bootstrap';
import PropTypes from 'prop-types';

import FeedIcon from '../../../assets/images/icons/navbar/home.png';
import UpcomingIcon from '../../../assets/images/icons/navbar/upcoming.png';
import DiscoverIcon from '../../../assets/images/icons/navbar/discover.png';
import ProfileIcon from '../../../assets/images/icons/navbar/profile.png';
import NotificationsIcon from '../../../assets/images/icons/navbar/notifications.png';
import AddEventIcon from '../../../assets/images/icons/navbar/add-event.png';

import { Auth } from '../../../auth';

import { INNER_ITEMS } from './constants';

import Item from './item/index.jsx';



@observer class DockItNavbarLeft extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { values, handlers } = this.props;
    const { nOfRequests, nOfNotifications } = values;
    const currentDate = (new Date()).getDate();
    const isNotificationsExists = nOfRequests > 0 || nOfNotifications > 0;
    const notificationsInnerImage = isNotificationsExists ? INNER_ITEMS.NOTIFICATION_MARKER : null;

    return (
      <Nav>
        <Item handlers={handlers} link='/feed' text='Feed' icon={FeedIcon} />
        <Item handlers={handlers} link='/upcoming' text='Upcoming' icon={UpcomingIcon} innerText={currentDate} showInner={true} />
        <Item handlers={handlers} link='/discover' text='Discover' icon={DiscoverIcon} />
        <Item handlers={handlers} link={`/users/${Auth.getActiveUser().id}`} text='Profile' icon={ProfileIcon} />
        <Item handlers={handlers} link='/notifications' text='Notifications' icon={NotificationsIcon} innerImage={notificationsInnerImage} />
        <Item handlers={handlers} link='/events' text='Create event' icon={AddEventIcon} onlyInDropdown={true} />
      </Nav>
    );
  }
}

DockItNavbarLeft.propTypes = {
  values: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired,
};

export default DockItNavbarLeft;
