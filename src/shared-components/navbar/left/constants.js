export const INNER_ITEMS = {
  NOTIFICATION_MARKER: 'notification marker',
};

export const INNER_ITEMS_CLASSES = {
  [INNER_ITEMS.NOTIFICATION_MARKER]: 'notification-marker',
};
