import React, { Component } from 'react';
import { action, observable, useStrict } from 'mobx';
import { observer } from 'mobx-react';
import { LinkContainer } from 'react-router-bootstrap';
import { NavItem } from 'react-bootstrap';
import PropTypes from 'prop-types';

import NotificationMarker from '../../../../assets/images/icons/notifications-marker.png';

import { INNER_ITEMS, INNER_ITEMS_CLASSES } from '../constants';

import { getRandomInt } from '../../../../utils/random';

const INNER_ITEMS_DATA = {
  [INNER_ITEMS.NOTIFICATION_MARKER]: NotificationMarker,
};



@observer class DockItNavbarLeftItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { link, text, icon, showInner, innerClass, innerImage, innerText, handlers } = this.props;
    const isVisibleOnlyInDropdown = this.props.onlyInDropdown;

    const innerItemClass = `icon-text ${innerClass}`;
    const className = `icon${isVisibleOnlyInDropdown ? ' in-dropdown-menu-item' : ''}`;

    const innerItemsClass = innerImage ? INNER_ITEMS_CLASSES[innerImage] : null;
    
    return (
      <LinkContainer to={link}>
        <NavItem eventKey={getRandomInt()} className={className} onClick={handlers.onSelect}>
          <div className='nav-item-icon'>
            <img src={icon} />
            { showInner && <div className={innerItemClass}>{ innerText }</div> }
            { innerImage && <img className={innerItemsClass} src={INNER_ITEMS_DATA[innerImage]} /> }
          </div>
          <span className='visible-xs-inline'>{ text }</span>
        </NavItem>
      </LinkContainer>
    );
  }
}

DockItNavbarLeftItem.propTypes = {
  link: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  innerImage: PropTypes.string,
  innerClass: PropTypes.string,
  innerText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  showInner: PropTypes.bool,
  onlyInDropdown: PropTypes.bool,
  handlers: PropTypes.object.isRequired
};

DockItNavbarLeftItem.defaultProps = {
  onlyInDropdown: false,
  showInner: false,
};

export default DockItNavbarLeftItem;
