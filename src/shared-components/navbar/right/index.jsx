import React, { Component } from 'react';
import { action, observable, useStrict } from 'mobx';
import { observer } from 'mobx-react';
import { LinkContainer } from 'react-router-bootstrap';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import PropTypes from 'prop-types';

import SearchBox from '../../../views/search/box/index.jsx';

import AddEventIcon from '../../../assets/images/icons/navbar/add-event.png';

import { getRandomInt } from '../../../utils/random';


@observer class DockItNavbarRight extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { values, handlers } = this.props;
    return (
      <span className='right-navbar-wrapper'>
        <Navbar.Collapse>
          <Nav pullRight>
            <NavItem eventKey={getRandomInt()} className='navbar-search-box-container'>
              <SearchBox values={values} handlers={handlers} />
            </NavItem>
            <LinkContainer to='/events'>
              <NavItem eventKey={getRandomInt()}>
                <img className='add-event-icon' src={AddEventIcon} />
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </span>
    );
  }
}

DockItNavbarRight.propTypes = {
  handlers: PropTypes.object,
  values: PropTypes.object,
};

export default DockItNavbarRight;
