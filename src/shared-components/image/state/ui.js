import { action, observable, toJS } from 'mobx';

import UIState from '../../../utils/state/data';

import ElementWrapper from '../../../utils/jquery/wrapper';

import jq from '../../../utils/jquery';
import { getRandomInt } from '../../../utils/random';

import { WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME, MEASURES, SHIFT_PROPS } from '../constants';



class DockItImageUiState extends UIState {
  @observable config;
  @observable style;

  lastSize = {};

  wrapperId = `${WRAPPER_CLASS_NAME}-${getRandomInt()}`;
  id = `${IMAGE_CLASS_NAME}-${getRandomInt()}`;

  get type() {
    return this.config.type;
  }

  get src() {
    return this.config.src;
  }

  get aspectRatio() {
    return this.config.aspectRatio;
  }

  get wrapper() {
    return jq.getElem(`#${this.wrapperId}`);
  }

  get elem() {
    return jq.getElem(`#${this.id}`);
  }

  get wrapperClassName() {
    return `${WRAPPER_CLASS_NAME}${this.config.wrapperClassName ? ` ${this.config.wrapperClassName}` : ''}`;
  }

  get className() {
    return `${IMAGE_CLASS_NAME}${this.config.className ? ` ${this.config.className}` : ''}`;
  }

  constructor(props = {}) {
    super(props);
    this.init(props);
  }

  init(props) {
    this.setConfig(props.values);
  }

  getPropsToChange(current, target) {
    const currentAspectRatio = this.calcAspectRatio(current);
    const targetAspectRatio = this.calcAspectRatio(target);
    let measureToChange = MEASURES.UNKNOWN;
    if (currentAspectRatio < targetAspectRatio) measureToChange = MEASURES.WIDTH;
    if (currentAspectRatio > targetAspectRatio) measureToChange = MEASURES.HEIGHT;
    return measureToChange === MEASURES.UNKNOWN ? {} : {
      changeProp: measureToChange,
      otherProp: measureToChange === MEASURES.WIDTH ? MEASURES.HEIGHT : MEASURES.WIDTH,
    };
  }

  calcAspectRatio(data) {
    if (data instanceof ElementWrapper) data = this.getDomElementMeasures(data);
    if (!data || !data[MEASURES.WIDTH] || !data[MEASURES.HEIGHT]) data = DEFAULT_ASPECT_RATIO;
    return data[MEASURES.WIDTH] / data[MEASURES.HEIGHT];
  }

  handleElemOnLoad(propsToChange) {
    this.elem.css(propsToChange.changeProp, '100%');
  }

  prepareWrapper() {
    const wrapper = this.wrapper;
    const newProps = this.getDomElementMeasuresWithBorder(wrapper);
    const height = newProps[MEASURES.WIDTH] / this.aspectRatio[MEASURES.WIDTH] * this.aspectRatio[MEASURES.HEIGHT];
    wrapper.css(MEASURES.WIDTH, `${newProps[MEASURES.WIDTH]}px`);
    wrapper.css(MEASURES.HEIGHT, `${Math.round(height)}px`);
  }

  centerElem(props) {
    const { wrapper, elem } = this;
    const wrapperMeasures = this.getDomElementMeasures(wrapper);
    const elemMeasures = this.getDomElementMeasures(elem);
    elem.css(SHIFT_PROPS[props.otherProp], `-${Math.round((elemMeasures[props.otherProp] - wrapperMeasures[props.otherProp]) / 2)}px`);
    this.lastSize = this.getDomElementMeasures(wrapper.parent);
  }

  getDomElementMeasures(elem) {
    return {
      [MEASURES.WIDTH]: elem.width(),
      [MEASURES.HEIGHT]: elem.height(),
    };
  }

  getDomElementMeasuresWithBorder(elem) {
    return {
      [MEASURES.WIDTH]: elem.offsetWidth,
      [MEASURES.HEIGHT]: elem.offsetHeight,
    };
  }

  isElemSizeChanged() {
    const measures = this.getDomElementMeasures(this.wrapper.parent);
    return Object.keys(measures).some(key => measures[key] !== this.lastSize[key]);
  }

  clearWrapperStyle() {
    const { wrapper } = this;
    wrapper.css(MEASURES.WIDTH, 'auto');
    wrapper.css(MEASURES.HEIGHT, 'auto');
  }

  clearElemStyle() {
    const { elem } = this;
    elem.css(MEASURES.WIDTH, 'auto');
    elem.css(MEASURES.HEIGHT, 'auto');
    elem.css(SHIFT_PROPS[MEASURES.WIDTH], 'auto');
    elem.css(SHIFT_PROPS[MEASURES.HEIGHT], 'auto');
  }

  @action
  setConfig(value) {
    this.config = value;
  }

  @action
  setStyle(value) {
    this.style = value;
  }

  clear() {
    this.setConfig(null);
    this.setStyle(null);
    this.lastSize = null;
    this.wrapperId = null;
    this.id = null;
  }
}

export default DockItImageUiState;
