import { action, observable, toJS, computed } from 'mobx';

import DataState from '../../../utils/state/data';



class DockItImageDataState extends DataState {
  @observable config;
  @observable handlers;

  get videoValues() {
    return this.config.videoValues || {};
  }

  get isNeedToPrepareWrapper() {
    return this.config.prepareWrapper && this.config.src;
  }

  constructor(props) {
    super(props);
    this.init(props);
  }

  init(props) {
    this.setConfig(props.values);
    this.setHandlers(props.handlers);
  }

  @action
  setConfig(value) {
    this.config = value;
  }

  @action
  setHandlers(value) {
    this.handlers = value;
  }

  clear() {
    this.setConfig(null);
    this.setHandlers(null);
  }
}

export default DockItImageDataState;
