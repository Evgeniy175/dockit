export const WRAPPER_CLASS_NAME = 'dockit-image-wrapper';
export const IMAGE_CLASS_NAME = 'dockit-image';

export const MEASURES = {
  WIDTH: 'width',
  HEIGHT: 'height',
  UNKNOWN: 'unknown',
};

export const SHIFT_PROPS = {
  [MEASURES.WIDTH]: 'marginLeft',
  [MEASURES.HEIGHT]: 'marginTop',
};

export const ASPECT_RATIOS = {
  s1x1: {
    [MEASURES.WIDTH]: 1,
    [MEASURES.HEIGHT]: 1,
  },
  s3x16: {
    [MEASURES.WIDTH]: 3,
    [MEASURES.HEIGHT]: 16,
  },
  s9x16: {
    [MEASURES.WIDTH]: 9,
    [MEASURES.HEIGHT]: 16,
  },
  s16x3: {
    [MEASURES.WIDTH]: 16,
    [MEASURES.HEIGHT]: 3,
  },
  s16x9: {
    [MEASURES.WIDTH]: 16,
    [MEASURES.HEIGHT]: 9,
  },
};

export const DEFAULT_ASPECT_RATIO = ASPECT_RATIOS.s1x1;

export const isEqualAspectRatios = (ar1, ar2) => {
  return !!ar1 && !!ar2
  && ar1[MEASURES.WIDTH] === ar2[MEASURES.WIDTH]
  && ar1[MEASURES.HEIGHT] === ar2[MEASURES.HEIGHT];
};
