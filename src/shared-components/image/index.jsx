import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action, toJS } from 'mobx';
import PropTypes from 'prop-types';

import DataState from './state/data';
import UiState from './state/ui';

import { MEASURES } from './constants';



@observer
class DockItContentWrapper extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(props),
    });
    this.onResize = ::this.onResizeHandler;
    this.onLoad = ::this.onLoadHandler;
  }

  componentDidMount() {
    if (this.data.isNeedToPrepareWrapper) this.ui.prepareWrapper();
    window.addEventListener('resize', this.onResize, false);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize, false);
    this.onResize = null;
    this.onLoad = null;
    this.data.clear();
    this.data = null;
    this.ui.clear();
    this.ui = null;
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
    this.ui.init(nextProps);
  }

  onResizeHandler() {
    if (!this.ui.isElemSizeChanged()) return;
    this.ui.clearWrapperStyle();
    this.ui.clearElemStyle();
    this.onLoad();
  }

  onLoadHandler() {
    const { aspectRatio, wrapper, elem } = this.ui;
    if (!aspectRatio || !wrapper.isValid() || !elem.isValid() || elem.height() === 0 || elem.width() === 0) return;
    const measureProps = this.ui.getPropsToChange(elem, aspectRatio);
    if (!measureProps.changeProp || !measureProps.otherProp) return elem.css(MEASURES.WIDTH, '100%');
    this.ui.prepareWrapper();
    this.ui.handleElemOnLoad(measureProps);
    this.ui.centerElem(measureProps);
  }

  render() {
    const { handlers } = this.data;
    const { wrapperId, wrapperClassName, id, src, className, style } = this.ui;
    return (
      <div id={wrapperId} className={wrapperClassName}>
        <img id={id}
             className={className}
             src={src}
             style={style}
             onLoad={this.onLoad}
             onClick={handlers && handlers.onClick} />
      </div>
    );
  }
}

DockItContentWrapper.propTypes = {
  values: PropTypes.shape({
    src: PropTypes.string,
    className: PropTypes.string,
    wrapperClassName: PropTypes.string,
    aspectRatio: PropTypes.object.isRequired,
    prepareWrapper: PropTypes.bool,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func,
  }),
};

export default DockItContentWrapper;
