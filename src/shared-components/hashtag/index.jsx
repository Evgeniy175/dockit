import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';



@observer
class Hashtag extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler(e) {
    e.preventDefault();
    e.stopPropagation();
    const { hashtag } = this.props;
    browserHistory.push(`/discover?hashtag=${hashtag && hashtag.length > 0 ? hashtag.substring(1) : ''}`);
  }

  render() {
    const { hashtag } = this.props;
    return <span className='dockit-hashtag' onClick={this.onClick}>{hashtag}</span>;
  }
}

Hashtag.propTypes = {
  hashtag: PropTypes.string.isRequired,
};

export default Hashtag;
