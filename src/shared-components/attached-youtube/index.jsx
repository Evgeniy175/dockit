import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import qs from 'query-string';

import Youtube from '../youtube/index.jsx';

import { isContainsYoutubeLinks, getUrls } from '../../utils/formatting/text';



@observer
class CommentAttachedYoutube extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let videoId;
    const { text } = this.props;
    if (!text || text.length === 0) return null;

    const urls = getUrls(text);
    const url = Array.from(urls).find(url => isContainsYoutubeLinks(url));

    if (!url) return null;
    let splittedUrl = url.split('?');
    if (splittedUrl.length <= 1) videoId = url.slice(url.lastIndexOf('/') + 1);
    if (!videoId && splittedUrl.length <= 1) return null;
    if (!videoId) videoId = qs.parse(splittedUrl[1]).v;
    return videoId ? <Youtube id={videoId} /> : null;
  }
}

CommentAttachedYoutube.propTypes = {
  text: PropTypes.string,
};

export default CommentAttachedYoutube;
