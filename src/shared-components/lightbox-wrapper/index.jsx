import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action, toJS } from 'mobx';
import PropTypes from 'prop-types';
import Lightbox from 'react-images';

import jq from '../../utils/jquery';



@observer
class LightboxWrapper extends Component {
  @observable currentImage = 0;
  
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onOpen = ::this.onOpenHandler;
    this.onPrevClick = ::this.onPrevClickHandler;
    this.onNextClick = ::this.onNextClickHandler;
    this.onClose = ::this.onCloseHandler;
  }

  componentWillUnmount() {
    this.setCurrentImage(null);
    this.onOpen = null;
    this.onPrevClick = null;
    this.onNextClick = null;
    this.onClose = null;
  }
  
  onOpenHandler(idx) {
    this.setCurrentImage(idx);
  }
  
  onPrevClickHandler() {
    this.setCurrentImage(this.currentImage - 1);
  }
  
  onNextClickHandler() {
    this.setCurrentImage(this.currentImage + 1);
  }
  
  onCloseHandler(e) {
    if (jq.wrap(e.target).is('a')) return;
    this.setCurrentImage(0);
    this.props.onClose();
  }
  
  @action
  setCurrentImage(value) {
    this.currentImage = value;
    if (this.props.onIdxChange) this.props.onIdxChange(value);
  }

  render() {
    const images = this.props.images;
    const isOpen = this.props.open;
    
    if (!images || images.length === 0) return null;

    return (
      <Lightbox images={images}
                currentImage={this.currentImage}
                backdropClosesModal={true}
                showImageCount={images.length > 1}
                isOpen={isOpen}
                onClickPrev={this.onPrevClick}
                onClickNext={this.onNextClick}
                onClose={this.onClose} />
    );
  }
}

LightboxWrapper.propTypes = {
  images: PropTypes.array.isRequired,
  onClose: PropTypes.func.isRequired,
  onIdxChange: PropTypes.func,
  open: PropTypes.bool,
};

LightboxWrapper.defaultProps = {
  open: false
};

export default LightboxWrapper;
