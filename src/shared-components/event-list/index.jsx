import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Event from './event/index.jsx';
import Loader from '../loader/index.jsx';



@observer
class EventList extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    this.props.onClick();
  }
  
  render() {
    const { data, loaded } = this.props;
    const { dates } = data;

    
    return (
      <div className='event-list'>
        { !loaded && <Loader /> }
        { dates.map(item => <Event key={item.dateOnly} data={item} schedule={item.schedule} onClick={this.onClick} />) }
      </div>
    );
  }
}

EventList.propTypes = {
  data: PropTypes.object.isRequired,
  loaded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default EventList;
