import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { HEADER_DAY_FORMAT } from '../constants';

@observer
class EventListDayHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const date = this.props.date;
    const header = moment(date).format(HEADER_DAY_FORMAT);
    
    return (
      <h2 className='day-header'>
        <span>
          { header }
        </span>
      </h2>
    );
  }
}

EventListDayHeader.propTypes = {
  date: PropTypes.string.isRequired
};

export default EventListDayHeader;
