import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';
import moment from 'moment';

import DayHeader from './day-header/index.jsx';
import Left from './left/index.jsx';
import Time from './right/time/index.jsx';

import { HEADER_DAY_FORMAT, DATE_FORMAT } from './constants';

import EventModel from '../../../models/events/index';

const eventModel = new EventModel();



@observer
class EventListItem extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const { data } = this.props;
    const config = {
      body: JSON.stringify({ date: moment(data.date).format() })
    };
  
    return eventModel.fromSchedule(data.id, config)
    .then(res => { browserHistory.push(`/events/${res.id}`); return Promise.resolve(); })
    .catch(console.error);
  }
  
  render() {
    const { data, schedule } = this.props;
    return (
      <div>
        <DayHeader date={data.date} />
        <div className='event' onClick={this.onClick}>
          <Left data={data} schedule={schedule} />
          <Time data={data} />
        </div>
      </div>
    );
  }
}

EventListItem.propTypes = {
  data: PropTypes.object.isRequired,
  schedule: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

export default EventListItem;
