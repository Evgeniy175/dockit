import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { TIME_FORMAT, ALL_DAY_TEXT } from '../../constants';



@observer
class EventListItemTime extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { data } = this.props;
    const startTime = moment(data.start).format(TIME_FORMAT);
    const endTime = moment(data.end).format(TIME_FORMAT);
    const isAllDay = startTime === endTime;
    const className = data.location ? 'all-day-with-location' : 'all-day';
    
    if (isAllDay) return (
      <div className='right'>
        <div className={className}>{ ALL_DAY_TEXT }</div>
      </div>
    );
    
    return (
      <div className='right'>
        <div className='time'>{ startTime }</div>
        <div className='time'>{ endTime }</div>
      </div>
    );
  }
}

EventListItemTime.propTypes = {
  data: PropTypes.object.isRequired
};

export default EventListItemTime;
