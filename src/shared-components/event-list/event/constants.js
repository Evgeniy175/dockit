export const HEADER_DAY_FORMAT = 'ddd, ll';
export const TIME_FORMAT = 'LT';
export const DATE_FORMAT = 'YYYY-MM-DD';
export const ALL_DAY_TEXT = 'All day';
export const MAX_TITLE_LENGTH = 35;
export const MAX_LOCATION_LENGTH = 35;
