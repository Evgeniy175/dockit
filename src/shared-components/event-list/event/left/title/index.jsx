import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

@observer
class EventListItemTitle extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { text } = this.props;
    return <div className='title'>{ text }</div>;
  }
}

EventListItemTitle.propTypes = {
  text: PropTypes.string
};

export default EventListItemTitle;
