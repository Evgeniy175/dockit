import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Title from './title/index.jsx';
import Location from './location/index.jsx';



@observer
class EventListItemLeftPart extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { data, schedule } = this.props;
    return (
      <div className='left'>
        <Title text={data.title} />
        <Location schedule={schedule} />
      </div>
    );
  }
}

EventListItemLeftPart.propTypes = {
  data: PropTypes.object.isRequired,
  schedule: PropTypes.object.isRequired,
};

export default EventListItemLeftPart;
