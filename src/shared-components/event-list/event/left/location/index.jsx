import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { MAX_LOCATION_LENGTH } from '../../constants';
import { truncateString } from '../../../../../utils/formatting/text';
import { getLocation } from '../../../../../utils/location';

@observer
class EventListItemLocation extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const schedule = this.props.schedule;
    let location = getLocation(schedule);
    if (location && location.length > 0) location = truncateString(location, MAX_LOCATION_LENGTH);
    
    return (
      <div className='location'>
        { location }
      </div>
    );
  }
}

EventListItemLocation.propTypes = {
  schedule: PropTypes.object.isRequired
};

export default EventListItemLocation;
