import React, { Component } from 'react';
import {observer} from 'mobx-react';

// States
import RadioToggleDataState from './state/data';
import RadioToggleUIState from './state/ui';


/**
 *
 */
@observer
class RadioToggle extends Component {
  constructor(props) {
    super(props);

    this.state = Object.freeze({
      data: new RadioToggleDataState(),
      ui: new RadioToggleUIState(props)
    });
  }

  render() {
    return (
      <div className={`radio-toggle ${this.state.ui.toggled ? 'on' : ''}`}>
        <div className="radio-switch"
             ng-click="radioDirectiveCtrl.toggle($event)">
        </div>
      </div>
      <div className="like action-item">
        <div className="counter">{this.state.data.count}</div>
        <div className="action" onClick={this.onActionClick}>
          <img className="action-icon" src={this.state.data.mine ? icon_like_active : icon_like}/>
        </div>
      </div>
    )
  }
}

export default RadioToggle;