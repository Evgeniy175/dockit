import {action, observable, toJS} from 'mobx';
import {observer} from 'mobx-react';

import UIState from '../../../../utils/state/data';


class RadioToggleUIState extends UIState {

  @observable toggled = false;

  constructor(props = {}) {
    super(props);

    // this.setToggleState(props.toggled);
  }

  @action
  setToggleState(value) {
    this.toggled = !!value;
  }

  @action
  toggle() {
    this.toggled = !this.toggled;
  }
}

export default RadioToggleUIState;
