import {action, observable, toJS} from 'mobx';
import {observer} from 'mobx-react';

import DataState from '../../../utils/state/data';

// Models.


class RadioToggleDataState extends DataState {
  constructor(config = {}) {
    super(config);
  }
}

export default RadioToggleDataState;
