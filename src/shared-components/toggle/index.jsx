import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Toggle from 'react-toggle';



@observer
class ToggleCheckbox extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { value, label, isBoldLabel, isToggleShown } = this.props;
    
    return (
      <div className='toggle-checkbox'>
        <label className={`checkbox-label${isBoldLabel ? ' bold' : ''}`}>
          {label}
        </label>
        {
          isToggleShown &&
          <div className='toggler'>
            <Toggle className='pull-right' defaultChecked={value} checked={value} onChange={this.props.onClick} />
          </div>
        }
      </div>
    );
  }
}

ToggleCheckbox.propTypes = {
  value: PropTypes.bool,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  isBoldLabel: PropTypes.bool,
  isToggleShown: PropTypes.bool,
};

ToggleCheckbox.defaultProps = {
  isBoldLabel: true,
  isToggleShown: true,
};

export default ToggleCheckbox;
