import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

import LikeAction from '../action-command/like/index.jsx';
import DockAction from '../action-command/dock/index.jsx';
import ScheduleEventListAction from '../action-command/schedule-events-list/index.jsx';

import { POSTFIX_POSITIONS } from '../action-command/dock/constants';



@observer
class ActionBar extends Component {
  render() {
    const { dockActionData, likeActionData, schedule, isSchedule } = this.props;
    return (
      <div className='action-bar'>
        <DockAction data={dockActionData} postfix postfixPosition={POSTFIX_POSITIONS.RIGHT} />
        <LikeAction data={likeActionData} postfix />
        { isSchedule && <ScheduleEventListAction schedule={schedule} /> }
      </div>
    )
  }
}

ActionBar.propTypes = {
  dockActionData: PropTypes.object.isRequired,
  likeActionData: PropTypes.object.isRequired,
  schedule: PropTypes.object,
  isSchedule: PropTypes.bool,
};

export default ActionBar;
