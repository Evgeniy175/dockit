import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class UserDropdownListItem extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { item, onClick } = this.props;
    if (onClick) onClick(item.username);
  }
  
  render() {
    const { item } = this.props;

    return (
      <div className='users-dropdown-list-item' onClick={this.onClick}>
        <span className='image-container'>
          <img src={item.profilePicture} />
        </span>
        <span className='text-container'>
          <span className='username'>{item.username}</span>
          <span className='dot'>·</span>
          <span className='name'>{item.name}</span>
        </span>
      </div>
    );
  }
}

UserDropdownListItem.propTypes = {
  item: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default UserDropdownListItem;
