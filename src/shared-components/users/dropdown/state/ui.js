import { action, observable, toJS } from 'mobx';

import jq from '../../../../utils/jquery';

import UIState from '../../../../utils/state/data';

import { LOAD_BUFFER_FROM_BOTTOM } from '../constants';



class UserDropdownListUiState extends UIState {
  constructor(props = {}) {
    super(props);
  }
  
  isNeedToLoadMore() {
    const container = jq.wrap('#users-dropdown-list');
    const fullHeight = container.scrollHeight;
    const currentScrollHeight = container.scrollTop + container.clientHeight;
    const currentPercent = currentScrollHeight / fullHeight * 100;
    return 100 - currentPercent < LOAD_BUFFER_FROM_BOTTOM;
  }
}

export default UserDropdownListUiState;
