import { action, observable, toJS } from 'mobx';

import DataState from '../../../../utils/state/data';
import { Auth } from '../../../../auth';

import UsersModel from '../../../../models/users';

import { PORTION_SIZE, PROFILE_PICS_RESOLUTION } from '../constants';

const userModel = new UsersModel();



class UserDropdownListDataState extends DataState {
  @observable items = [];

  nextPage;
  maxPage;

  value;
  @observable isOpen;
  isBusy = false;

  get isLoadMoreAvailable() {
    return !!this.nextPage;
  }

  constructor(props = {}) {
    super(props);
    this.init(props, true);
    this.fetchFollowings();
  }

  init(props, isInitial = false) {
    const isNeedToReload = !isInitial && this.value !== props.value;
    this.value = props.value;
    this.setVisibility(props.open);
    if (isNeedToReload) this.fetchFollowings();
  }

  @action
  setVisibility(value) {
    this.isOpen = value;
  }

  fetchFollowings() {
    const config = {
      params: {
        query: {
          username: {
            $ilike: `${this.value.substring(1)}%`,
          },
        },
      },
    };
    const userId = Auth.getActiveUser().id;

    return userModel.fetchFollowings(userId, PORTION_SIZE, null, config)
    .then(res => {
      this.parseResponse(res);
      return Promise.resolve();
    });
  }

  fetchMoreFollowings() {
    const config = {
      params: {
        limit: PORTION_SIZE,
        page: this.nextPage,
        query: {
          username: {
            $ilike: `${this.value.substring(1)}%`,
          },
        },
      },
    };
    const userId = Auth.getActiveUser().id;
    return userModel.fetchFollowingsByConfig(userId, config)
    .then(res => {
      this.attachItems(res);
      return Promise.resolve();
    });
  }

  @action
  parseResponse(res) {
    this.nextPage = res.nextPage;
    this.items = res.data.map(item => item.data);
    this.items.forEach(item => item.profilePicture = UsersModel.formatImageUrl(item.profilePicture, PROFILE_PICS_RESOLUTION));
  }

  @action
  attachItems(res) {
    this.nextPage = res.nextPage;
    const itemsBuff = res.data.map(item => item.data);
    itemsBuff.forEach(item => item.profilePicture = UsersModel.formatImageUrl(item.profilePicture, PROFILE_PICS_RESOLUTION));
    this.items = this.items.concat(itemsBuff);
  }

  @action
  clear() {
    this.items = null;
  }
}

export default UserDropdownListDataState;
