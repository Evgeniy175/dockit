import {MIN_RESOLUTION} from '../../../utils/images/resolution/constants';

export const PORTION_SIZE = 20;

export const PROFILE_PICS_RESOLUTION = MIN_RESOLUTION;

export const DEFAULT_ITEM_HEIGHT = 38;

export const MAX_ITEMS_IN_LIST = 7;
export const LOAD_BUFFER_FROM_BOTTOM = 30;

export const INTERVAL_TIMEOUT_MS = 10;
export const INTERVAL_MAX_COUNT = 100;

export const POSITIONS = {
  TOP: 'top',
  RIGHT: 'right',
  BOTTOM: 'bottom',
  LEFT: 'left',
};

export const DEFAULT_POSITION = POSITIONS.TOP;
