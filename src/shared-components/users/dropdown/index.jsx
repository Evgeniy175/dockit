import React, {Component} from 'react';
import {action, observable, toJS} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import User from './user/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import jq from '../../../utils/jquery';

import {MAX_ITEMS_IN_LIST, INTERVAL_MAX_COUNT, INTERVAL_TIMEOUT_MS, DEFAULT_ITEM_HEIGHT, POSITIONS, DEFAULT_POSITION} from './constants';



@observer
class UserDropdownList extends Component {
  isScrollEventsInitCompleted = false;

  intervalId;
  intervalCount = 0;

  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(props),
    });
    this.onWheel = ::this.onWheelHandler;
    this.setHeight = ::this.setHeightHandler;
    this.positionSetters = {
      [POSITIONS.TOP]: ::this.setTop,
      [POSITIONS.RIGHT]: null /*todo*/,
      [POSITIONS.BOTTOM]: ::this.setBottom,
      [POSITIONS.LEFT]: null /*todo*/,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
    if (this.data.isOpen) this.initContainerHeight();
    else this.isScrollEventsInitCompleted = false;
  }

  componentDidUpdate() {
    this.initScrollEvents();
    if (this.data.isOpen) this.initContainerHeight();
  }

  initScrollEvents() {
    if (this.isScrollEventsInitCompleted) return;
    const container = jq.wrap('#users-dropdown-list');
    if (!container.isValid()) return;
    container.on('mousewheel', this.onWheel);
    this.isScrollEventsInitCompleted = true;
  }

  initContainerHeight() {
    this.setHeight();
    this.intervalId = setInterval(this.setHeight, INTERVAL_TIMEOUT_MS);
  }

  setHeightHandler(length = MAX_ITEMS_IN_LIST) {
    if (this.intervalCount++ > INTERVAL_MAX_COUNT) return this.clearInterval();
    this.initItemHeight();
    const list = jq.wrap('#users-dropdown-list');
    if (!list.isValid()) return;
    //todo

    const newHeight = this.itemHeight * (this.getLength(length)) + 1;
    list.css('height', newHeight);
    if (this.positionSetters[this.props.position]) this.positionSetters[this.props.position](list);
    this.clearInterval();
  }

  setTop(list) {
    list.css('top', `${-list.height() + 10}px`);
  }

  setBottom(list) {
    if (!list.parent.isValid()) return;
    list.css('top', `${list.parent.height()}px`);
  }

  initItemHeight() {
    const items = jq.getElems('.users-dropdown-list-item');
    this.itemHeight = !this.itemHeight && items.length > 0 ? items[0].offsetHeight : DEFAULT_ITEM_HEIGHT;
  }

  clearInterval() {
    clearInterval(this.intervalId);
    this.intervalCount = 0;
  }

  getLength(length) {
    if (!length) return 1;
    return length > MAX_ITEMS_IN_LIST ? MAX_ITEMS_IN_LIST : length;
  }

  onWheelHandler(e) {
    if (this.data.isBusy || !this.ui.isNeedToLoadMore(e)) return;
    this.data.isBusy = true;
    this.data.fetchMoreFollowings()
    .finally(() => this.data.isBusy = false);
  }
  
  render() {
    const { css, onClick } = this.props;
    const { isOpen, items } = this.data;

    if (!isOpen) return null;

    return (
      <div className='users-dropdown-list' id='users-dropdown-list' style={css}>
        { items.length === 0 && <div className='no-items'>No results</div> }
        { items.map(item => <User key={item.id} onClick={onClick} item={item} />) }
      </div>
    );
  }
}

UserDropdownList.propTypes = {
  css: PropTypes.object,
  open: PropTypes.bool,
  value: PropTypes.string,
  position: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

UserDropdownList.defaultProps = {
  position: DEFAULT_POSITION,
};

export default UserDropdownList;
