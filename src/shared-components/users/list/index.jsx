import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import User from './user/index.jsx';
import Loader from '../../loader/index.jsx';



@observer
class UserList extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    this.props.onClick();
  }
  
  render() {
    const { users, loaded } = this.props;
    
    return (
      <div className='user-list'>
        { !loaded && <Loader /> }
        { users.map(user => <User key={user.data.id} user={user} onClick={this.onClick} />) }
      </div>
    );
  }
}

UserList.propTypes = {
  users: PropTypes.array.isRequired,
  loaded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default UserList;
