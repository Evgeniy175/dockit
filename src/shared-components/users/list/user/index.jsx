import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import {get} from 'lodash';
import PropTypes from 'prop-types';

import UsersMenu from '../../../users/menu/index.jsx';

import UserModel from '../../../../models/users';

import {isTargetIcon} from '../../../../utils/dom/users-menu-helpers';

import { Auth } from '../../../../auth';



@observer
class UserListItem extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler(e) {
    if (isTargetIcon(e)) return;
    const userId = this.props.user.data.id;
    browserHistory.push(`/users/${userId}`);
    this.props.onClick();
  }
  
  render() {
    const { user } = this.props;
    const { data, follows } = user;
    const name = data.name;
    const username = `@${data.username}`;
    const profilePicture = UserModel.formatImageUrl(data.profilePicture);
    const isSigned = Auth.isSigned();
    const currUserId = Auth.getActiveUser().id;

    const usersMenuValues = {
      userId: data.id,
      personName: name,
      isUserPrivate: !data.public,
      isFollowed: !!follows.mine,
      isAccepted: !!get(follows, 'mine.accepted'),
    };

    return (
      <div className='user' onClick={this.onClick}>
        <div className='user-pic'>
          <img src={profilePicture} />
        </div>
        <div className='user-data'>
          <div className='name'>{ name }</div>
          <div className='username'>{ username }</div>
        </div>
        { isSigned && data.id !== currUserId && <UsersMenu values={usersMenuValues} /> }
      </div>
    );
  }
}

UserListItem.propTypes = {
  user: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

export default UserListItem;
