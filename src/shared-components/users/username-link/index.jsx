import React, { Component } from 'react';
import { Link } from 'react-router';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';



@observer
class UsernameLink extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const userName = this.props.self ? 'You' : get(this.props, 'user.username');
    return <Link to={`/users/${get(this.props, 'user.id')}`} className='username-link'>{ userName }</Link>;
  }
}

UsernameLink.propTypes = {
  user: PropTypes.object,
  self: PropTypes.bool,
};

UsernameLink.defaultProps = {
  self: false,
};

export default UsernameLink;