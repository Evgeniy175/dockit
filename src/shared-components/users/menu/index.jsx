import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Modal from '../../question-modal/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import { MODAL_TITLE } from './constants';
import { CSS_MAPPING } from '../../button/constants';



@observer
class UsersMenu extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(props),
    });
    this.openModal = this.data.onModalOpen.bind(this.data);
    this.onFollow = this.data.onFollow.bind(this.data);
    this.onModalClose = this.data.onModalClose.bind(this.data);

    this.modalButtons = [
      {
        text: 'Unfollow',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: this.data.onUnfollow.bind(this.data),
      },
      {
        text: 'Cancel',
        className: CSS_MAPPING.GRAY,
        onClick: this.data.onCancelClick.bind(this.data),
      }
    ];
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    this.data.clear();
    this.data = null;
    this.ui = null;
    this.openModal = null;
    this.onFollow = null;
    this.onModalClose = null;
    this.modalButtons = null;
  }

  render() {
    const { isFollowed, isModalOpen, text } = this.data;
    const icon = this.ui.getIcon(this.data);
    const handler = isFollowed ? this.openModal : this.onFollow;
    return (
      <span className='users-menu'>
        <img src={icon.img} className={icon.className} onClick={handler} />
        <Modal open={isModalOpen} title={MODAL_TITLE} text={text} onClose={this.onModalClose} buttons={this.modalButtons} />
      </span>
    );
  }
}

UsersMenu.propTypes = {
  values: PropTypes.shape({
    userId: PropTypes.string.isRequired,
    personName: PropTypes.string.isRequired,
    isUserPrivate: PropTypes.bool.isRequired,
    isFollowed: PropTypes.bool.isRequired,
    isAccepted: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onAdding: PropTypes.func,
    onDeleting: PropTypes.func,
  }),
};

export default UsersMenu;
