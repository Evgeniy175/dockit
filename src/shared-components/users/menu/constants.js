import AddIcon from '../../../assets/images/icons/add-icon.svg';
import PendingIcon from '../../../assets/images/icons/pending.png';
import RejectIcon from '../../../assets/images/icons/green-check.png';

export const ICONS = {
  ADD: 'add',
  PENDING: 'pending',
  REJECT: 'reject'
};

export const IMAGES = {
  [ICONS.ADD]: AddIcon,
  [ICONS.PENDING]: PendingIcon,
  [ICONS.REJECT]: RejectIcon
};

export const CLASSES = {
  [ICONS.ADD]: 'add-icon',
  [ICONS.PENDING]: 'pending-icon',
  [ICONS.REJECT]: 'reject-icon'
};

export const DEFAULT_CLASS = 'users-menu-icon';

export const MODAL_TITLE = 'Are you sure?';
