import { action, observable, toJS } from 'mobx';

import UIState from '../../../../utils/state/data';

import { ICONS, DEFAULT_CLASS, CLASSES, IMAGES } from '../constants';



class UsersMenuUiState extends UIState {
  getIcon({ isFollowed, isAccepted }) {
    let key = isFollowed ? (isAccepted ? ICONS.REJECT : ICONS.PENDING) : ICONS.ADD;
    return {
      img: IMAGES[key],
      className: `${DEFAULT_CLASS} ${CLASSES[key]}`,
    };
  }
}

export default UsersMenuUiState;
