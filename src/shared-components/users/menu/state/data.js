import { action, observable, computed } from 'mobx';

import FollowModel from '../../../../models/follows';

import DataState from '../../../../utils/state/data';

const followModel = new FollowModel();



class UsersMenuDataState extends DataState {
  isDisposed = false;
  isAnyChanges = false;
  @observable values;
  @observable handlers;
  keys = {
    userId: 'userId',
    personName: 'personName',
    isFollowed: 'isFollowed',
    isFollowActionAvailable: 'isFollowActionAvailable',
    isAccepted: 'isAccepted',
    isModalOpen: 'isModalOpen',
    isUserPrivate: 'isUserPrivate',
    addHandler: 'onAdding',
    deleteHandler: 'onDeleting',
  };

  get userId() {
    return this._getValue(this.keys.userId);
  }
  setUserId(value) {
    this._setValue(this.keys.userId, value);
  }

  get personName() {
    return this._getValue(this.keys.personName);
  }
  setPersonName(value) {
    this._setValue(this.keys.personName, value);
  }

  get isFollowed() {
    return this._getValue(this.keys.isFollowed);
  }
  setIsFollowed(value) {
    this._setValue(this.keys.isFollowed, value);
  }

  get isFollowActionAvailable() {
    return this._getValue(this.keys.isFollowActionAvailable);
  }
  setIsFollowActionAvailable(value) {
    this._setValue(this.keys.isFollowActionAvailable, value);
  }

  get isAccepted() {
    return this._getValue(this.keys.isAccepted);
  }
  setIsAccepted(value) {
    this._setValue(this.keys.isAccepted, value);
  }

  get isModalOpen() {
    return this._getValue(this.keys.isModalOpen);
  }
  setIsModalOpen(value) {
    this._setValue(this.keys.isModalOpen, value);
  }

  get isUserPrivate() {
    return this._getValue(this.keys.isUserPrivate);
  }
  setIsUserPrivate(value) {
    this._setValue(this.keys.isUserPrivate, value);
  }

  get addHandler() {
    return this._getHandler(this.keys.addHandler);
  }
  get deleteHandler() {
    return this._getHandler(this.keys.deleteHandler);
  }

  get text() {
    return this.isUserPrivate
    ? `If you change your mind, you'll have to request to follow ${this.personName} again.`
    : `Unfollow ${this.personName}?`;
  }

  constructor(props = {}) {
    super(props);
    this.init(props);
  }

  onFollow(e) {
    e.stopPropagation();

    if (!this.isFollowActionAvailable) return;

    this.setIsFollowed(true);
    this.setIsFollowActionAvailable(false);
    this.setIsAccepted(false);

    followModel.follow(this.userId)
    .then(data => { this.setIsAccepted(data.accepted); if (this.addHandler) this.addHandler(data.accepted); })
    .catch(err => { console.error(err); this.setIsFollowed(false); })
    .finally(() => { this.setIsFollowActionAvailable(true); });
  }

  onUnfollow(e) {
    e.stopPropagation();

    if (!this.isFollowActionAvailable) return;

    this.setIsFollowed(false);
    this.setIsFollowActionAvailable(false);
    this.setIsModalOpen(false);

    followModel.unfollow(this.userId)
    .then(() => { if (this.deleteHandler) this.deleteHandler(this.isAccepted); })
    .catch(err => { console.error(err); this.setIsFollowed(true); })
    .finally(() => { this.setIsFollowActionAvailable(true); });
  }

  onModalOpen() {
    this.setIsModalOpen(true);
  }

  onModalClose() {
    this.setIsModalOpen(false);
  }

  onCancelClick() {
    this.setIsModalOpen(false);
  }

  @action
  init(props) {
    if (this.isAnyChanges) return;
    this.values = observable.map(props.values);
    this.handlers = observable.map(props.handlers);
    if (this.isFollowActionAvailable === undefined) this._setValue(this.keys.isFollowActionAvailable, true, true);
  }

  @action
  clear() {
    this.isDisposed = true;
    this.values = null;
    this.handlers = null;
  }

  _getValue(key) {
    return this.isDisposed ? false : this.values.get(key);
  }

  @action
  _setValue(key, value, isInitial = false) {
    if (this.isDisposed) return;
    if (!isInitial) this.isAnyChanges = true;
    this.values.set(key, value);
  }

  _getHandler(key) {
    return this.isDisposed ? false : this.handlers.get(key);
  }

  @action
  _setHandler(key, value) {
    if (this.isDisposed) return;
    this.handlers.set(key, value);
  }
}

export default UsersMenuDataState;
