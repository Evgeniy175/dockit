import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import { DEFAULT_PREFIX, DEFAULT_INPUT_CLASS_NAME } from './constants';



@observer
class AuthInput extends Component {
  value = '';
  inputType = '';
  placeholder = '';
  prefix = '';
  error = '';
  isChanged = false;
  
  constructor(props) {
    super(props);
    this.init(props);
  }
  
  componentWillMount() {
    this.onChange = ::this.onChangeHandler;
    this.onFocus = ::this.onFocusHandler;
    this.onBlur = ::this.onBlurHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }
  
  init(props) {
    this.value = props.value;
    this.inputType = props.inputType;
    this.placeholder = props.placeholder;
    this.prefix = props.prefix;
    this.error = props.error;
    this.isChanged = false;
  }
  
  onChangeHandler(e) {
    const fieldPrefix = this.props.prefix;
  
    if (!e) return;
    
    e.preventDefault();
    e.stopPropagation();
    
    if (e.target.selectionStart > fieldPrefix.length) {
      this.props.onChange(e.target.value);
      this.isChanged = true;
      return;
    }
  
    const prefix = e.target.value.startsWith(fieldPrefix) ? DEFAULT_PREFIX : fieldPrefix;
    const newValue =  `${prefix}${e.target.value === prefix ? DEFAULT_PREFIX: e.target.value}`;
    const occurrences = fieldPrefix === DEFAULT_PREFIX ? 0 : newValue.split(fieldPrefix).length - 1;
    
    if (occurrences > 1) return;
    
    this.props.onChange(newValue);
    this.isChanged = true;
  }
  
  onFocusHandler(e) {
    const prefix = this.prefix;
    if (e.target.value === DEFAULT_PREFIX) this.props.onChange(prefix);
    if (this.props.onFocus) this.props.onFocus(e);
  }
  
  onBlurHandler(e) {
    const prefix = this.prefix;
    const isNeedToClearInput = e.target.value === prefix;
    if (isNeedToClearInput) this.onChange(DEFAULT_PREFIX);
    if (this.props.onBlur) this.props.onBlur(e);
  }

  render() {
    const value = this.value;
    const inputType = this.inputType;
    const placeholder = this.placeholder;
    const isMessageShown = this.props.messageShown;
    const error = this.error;
    const isErrorShown = error && !this.isChanged;
    const inputClassName = `${DEFAULT_INPUT_CLASS_NAME}${isErrorShown ? ` error` : ''}`;

    return (
      <div className='validation-input-row'>
        { isMessageShown && isErrorShown && <div className='error-text'>{ error }</div> }
        <input className={inputClassName} value={value} type={inputType} placeholder={placeholder} onChange={this.onChange} onFocus={this.onFocus} onBlur={this.onBlur} />
      </div>
    );
  }
}

AuthInput.propTypes = {
  value: PropTypes.string.isRequired,
  inputType: PropTypes.string,
  placeholder: PropTypes.string,
  prefix: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  messageShown: PropTypes.bool
};

AuthInput.defaultProps = {
  inputType: 'text',
  prefix: DEFAULT_PREFIX,
  error: '',
  messageShown: true
};

export default AuthInput;
