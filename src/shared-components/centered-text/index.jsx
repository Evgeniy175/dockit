import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Loader from '../loader/index.jsx';
import OopsFace from '../../assets/images/amazed.png';

import { handleUris } from '../../utils/formatting/text';



@observer
class CenteredModalText extends Component {
  render() {
    const text = handleUris([this.props.text]);
    const isLoaded = this.props.loaded;
  
    if (!isLoaded) return <Loader />;
    
    return (
      <div>
        <div className='centered-text'>
          <img className='icon' src={OopsFace} />
          <div className='text'>{text}</div>
        </div>
      </div>
    );
  }
}

CenteredModalText.propTypes = {
  text: PropTypes.string.isRequired,
  loaded: PropTypes.bool
};

CenteredModalText.defaultProps = {
  loaded: true
};

export default CenteredModalText;
