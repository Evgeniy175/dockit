import { action, observable, computed, toJS } from 'mobx';

import DataState from '../../../utils/state/data';

import { getLocation, getLocationCoordinates } from '../../../utils/location';



class LocationDataState extends DataState {
  item = {};

  get location() {
    return getLocation(this.item);
  }

  get locationCoords() {
    return getLocationCoordinates(this.item);
  }

  constructor(props = {}) {
    super(props);
    this.init(props);
  }
  
  init(props) {
    this.item = props.item;
  }
}

export default LocationDataState;
