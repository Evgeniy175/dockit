import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import DataState from './state/data';

import {GOOGLE_MAPS_BASE} from './constants';



@observer
class LocationWrapper extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  render() {
    const { location, locationCoords } = this.data;
    const href = locationCoords ? `${GOOGLE_MAPS_BASE}?ll=${locationCoords.lat},${locationCoords.lng}` : `${GOOGLE_MAPS_BASE}maps/search/${location}`;
    return (
      <div className='location-block'>
        <a className='location' href={href} target='_blank'>{location}</a>
      </div>
    );
  }
}

LocationWrapper.propTypes = {
  item: PropTypes.object.isRequired,
};

export default LocationWrapper;
