import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';



@observer
class ModalDoneButton extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return <Modal.Dismiss className='modal-done-button'>Done</Modal.Dismiss>;
  }
}

export default ModalDoneButton;
