import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get, set } from 'lodash';

import Button from '../../../button/index.jsx';

import { BUTTONS, TITLES, CLASSES } from './constants';
import { TYPES } from '../../../../models/intents/constants';
import { TYPES as ITEMS_TYPES } from '../../../../views/creations/constants';

import IntentModel from '../../../../models/intents';

const intentModel = new IntentModel();

const INTENT_PATH = 'decorated.intents.mine.intent';



@observer
class IntentPanel extends Component {
  @observable isBusy = false;
  @observable data;
  @observable type;
  @observable dockData;

  ITEM_RESOLVERS = {
    [ITEMS_TYPES.EVENT]: () => this.data.event,
    [ITEMS_TYPES.SCHEDULE]: () => this.data.schedule,
  };

  constructor(props) {
    super(props);
    this.init(props);

    this.buttons = [
      {
        key: BUTTONS.DOCK,
        handler: ::this.doDock,
      },
      {
        key: BUTTONS.MAYBE,
        handler: ::this.doMaybe,
      },
      {
        key: BUTTONS.NOT_GOING,
        handler: ::this.doNotGoing,
      },
    ];
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  doDock() {
    this.handleNewIntent(TYPES.DOCKED);
  }

  doMaybe() {
    this.handleNewIntent(TYPES.MAYBE);
  }

  doNotGoing() {
    this.handleNewIntent(TYPES.NOT_GOING);
  }

  handleNewIntent(intent) {
    if (toJS(this.isBusy)) return;

    this.setIsBusy(true);

    const config = {
      routing: {
        type: this.type,
        id: get(this.ITEM_RESOLVERS[this.type](), 'id'),
      },
      body: JSON.stringify({
        intent,
      }),
    };

    return intentModel.updateIntent(config)
    .then(() => this.setIntent(intent))
    .finally(() => this.setIsBusy(false));
  }

  init(props) {
    this.setData(get(props, 'values.data'));
    this.setType(get(props, 'values.type'));
    this.setDockData(get(props, 'values.dockData'));
  }

  @action
  setIsBusy(value) {
    this.isBusy = value;
  }

  @action
  setData(value) {
    this.data = value;
  }

  @action
  setType(value) {
    this.type = value;
  }

  @action
  setDockData(value) {
    this.dockData = value;
  }

  @action
  setIntent(value) {
    set(this.data, INTENT_PATH, value);
  }

  getIntent() {
    return get(this.data, INTENT_PATH)
  }

  render() {
    const intent = this.getIntent();

    return (
      <div className='intent-panel'>
        {
          this.buttons.map(button =>
            <div className='btn-container' key={button.key}>
              <Button text={TITLES[button.key]}
                      className={intent === button.key ? CLASSES[button.key].ACTIVE : CLASSES[button.key].NORMAL}
                      onClick={button.handler} />
            </div>
          )
        }
      </div>
    );
  }
}

IntentPanel.propTypes = {
  values: PropTypes.shape({
    data: PropTypes.object.isRequired,
    type: PropTypes.string.isRequired,
    dockData: PropTypes.object.isRequired,
  }).isRequired,
};

export default IntentPanel;
