import { CSS_MAPPING } from '../../../button/constants';
import { TYPES as INTENTS_TYPES } from '../../../../models/intents/constants';

export const BUTTONS = {
  DOCK: INTENTS_TYPES.DOCKED,
  MAYBE: INTENTS_TYPES.MAYBE,
  NOT_GOING: INTENTS_TYPES.NOT_GOING,
};

export const TITLES = {
  [BUTTONS.DOCK]: `Dock`,
  [BUTTONS.MAYBE]: `Maybe`,
  [BUTTONS.NOT_GOING]: `Can't go`
};

export const CLASSES = {
  [BUTTONS.DOCK]: {
    NORMAL: CSS_MAPPING.GRAY,
    ACTIVE: CSS_MAPPING.BLUE_FILLED,
  },
  [BUTTONS.MAYBE]: {
    NORMAL: CSS_MAPPING.GRAY,
    ACTIVE: CSS_MAPPING.GRAY_FILLED,
  },
  [BUTTONS.NOT_GOING]: {
    NORMAL: CSS_MAPPING.GRAY,
    ACTIVE: CSS_MAPPING.RED_FILLED,
  },
};
