import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Radio } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class Radiobutton extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onChange = ::this.changeHandler;
  }
  
  changeHandler() {
    this.props.changeHandler(this.props.data);
  }
  
  render() {
    const groupName = this.props.groupName;
    const value = this.props.value;
    const isChecked = this.props.isChecked;
    const classPostfix = isChecked ? 'active' : '';
    
    return (
      <span className='radiobutton'>
        <Radio className={`btn btn-primary ${classPostfix}`} inline={true} name={groupName} id={value} onChange={this.onChange} checked={isChecked}>
          {this.props.data.title}
        </Radio>
      </span>
    );
  }
}

Radiobutton.propTypes = {
  groupName: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  value: PropTypes.string.isRequired,
  isChecked: PropTypes.bool.isRequired,
  changeHandler: PropTypes.func.isRequired
};

export default Radiobutton;
