export const CREDENTIALS = {
  clientId: '981506575917-ts9722kp6fpvsvdcrdturhgo5osahirr.apps.googleusercontent.com'
};

export const SCOPES = 'https://www.googleapis.com/auth/calendar.readonly';
export const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'];