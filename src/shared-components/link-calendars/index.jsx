import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import { setTitle } from '../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';

import DataState from './state/data';

import Button from '../button/index.jsx';

import { CSS_MAPPING } from '../button/constants';
import { UPCOMING_PATH } from '../../constants';

import LinkCalendarsIcon from '../../assets/images/icons/link-calendars.png';

@observer
class LinkCalendars extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.LINK_CALENDARS]());
  }
  
  componentWillMount() {
    this.data.loadLib();
    this.onGoogleCalendarClick = ::this.onGoogleCalendarClickHandler;
    this.onNotNowClick = ::this.onNotNowClickHandler;
  }
  
  onGoogleCalendarClickHandler() {
    const isSignedToGoogle = toJS(this.data.isSignedToGoogle);
    
    if (isSignedToGoogle) { this.data.doGoogleSignOut(); return; }
    
    this.data.doGoogleSignIn();
  }
  
  onNotNowClickHandler() {
    browserHistory.push(UPCOMING_PATH);
  }
  
  render() {
    const isSignedToGoogle = toJS(this.data.isSignedToGoogle);
    const googleClassName = isSignedToGoogle ? CSS_MAPPING.DARK_GREEN : CSS_MAPPING.BLACK;
    
    return (
      <div>
        <div className='link-calendars'>
          <div className='link-calendars-icon'>
            <img src={LinkCalendarsIcon} />
          </div>
          <div className='link-calendars-top-text'>
            Link your DockIt account with other calendars
          </div>
          <div className='link-calendars-middle-text'>
            All imported items become private events that only you can see!
          </div>
          <div className='link-calendars-buttons'>
            <div>
              <Button text='Google Calendar' className={googleClassName} iconShown={isSignedToGoogle} onClick={this.onGoogleCalendarClick} />
            </div>
            <div>
              <Button text='Not now' className={CSS_MAPPING.BLUE} onClick={this.onNotNowClick} />
            </div>
          </div>
          <div className='link-calendars-bottom-text'>
            To link accounts at another time visit the settings page
          </div>
        </div>
      </div>
    );
  }
}

export default LinkCalendars;
