import { action, observable, computed } from 'mobx';

import { CREDENTIALS, SCOPES, DISCOVERY_DOCS } from '../constants';
import { Auth } from '../../../auth';

import DataState from '../../../utils/state/data';

import EventsModel from '../../../models/events';
import UsersModel from '../../../models/users';

const eventsModel = new EventsModel();
const usersModel = new UsersModel();

class LinkCalendarsDataState extends DataState {
  @observable isSignedToGoogle;
  
  isInitialLoadFinished = false;
  
  constructor(props = {}) {
    super(props);
  
    this.initClient = ::this.initClientHandler;
    this.updateSigninStatus = ::this.updateSigninStatusHandler;
    
    const gAuth = Auth.getActiveGoogleAuth();
    this.setIsSignedToGoogle(!!gAuth);
  }
  
  /**
   *  On load, called to load the auth2 library and API client library.
   */
  loadLib() {
    gapi.load('client:auth2', this.initClient);
  }
  
  doGoogleSignIn() {
    gapi.auth2.getAuthInstance().signIn();
  }
  
  doGoogleSignOut() {
    gapi.auth2.getAuthInstance().signOut();
  }
  
  /**
   *  Initializes the API client library and sets up sign-in state
   *  listeners.
   */
  initClientHandler() {
    gapi.client.init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: CREDENTIALS.clientId,
      scope: SCOPES
    })
    .then(() => {
      const auth = gapi.auth2.getAuthInstance();
      
      // Listen for sign-in state changes.
      auth.isSignedIn.listen(this.updateSigninStatus);
      
      // Handle the initial sign-in state.
      this.updateSigninStatus(auth.isSignedIn.get());
    });
  }
  
  /**
   *  Called when the signed in status changes, to update the UI
   *  appropriately. After a sign-in, the API is called.
   */
  updateSigninStatusHandler(isSignedIn) {
    const auth = gapi.auth2.getAuthInstance();
    const handler = isSignedIn ? ::Auth.setActiveGoogleAuth : ::Auth.removeActiveGoogleAuth;
    handler(auth);
    
    this.setIsSignedToGoogle(isSignedIn);
    
    if (!this.isInitialLoadFinished) { this.isInitialLoadFinished = true; return; }
  
    const apiHandler = isSignedIn ? ::eventsModel.googleCalendarSignIn : ::usersModel.googleCalendarSignOut;
    
    return apiHandler(Auth.getActiveGoogleAuth())
    .catch(console.error);
  }
  
  @action
  setIsSignedToGoogle(value) {
    this.isSignedToGoogle = value;
  }
}


export default LinkCalendarsDataState;
