import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { action, observable, computed } from 'mobx';
import { browserHistory } from 'react-router';

import Image from '../../../../../../shared-components/image/index.jsx';

import { WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../../../../../shared-components/image/constants';



class DiscoverEventImage extends Component {
  @observable values;
  handlers;

  constructor(props) {
    super(props);

    this.setValues({
      src: this.props.image,
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s16x9,
      prepareWrapper: true,
    });
    this.handlers = {
      onClick: ::this.onClickHandler,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.image !== this.props.image) this.setSrc(this.props.image);
  }

  componentWillUnmount() {
    this.setValues(null);
    this.handlers = null;
  }

  onClickHandler() {
    const { type, id } = this.props;
    browserHistory.push(`/${type}/${id}`);
  }

  @action
  setValues(value) {
    this.values = value;
  }

  @action
  setSrc(value) {
    this.values.src = value;
  }

  render() {
    return <Image values={this.values} handlers={this.handlers} />;
  }
}

DiscoverEventImage.propTypes = {
  id: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
};

export default DiscoverEventImage;
