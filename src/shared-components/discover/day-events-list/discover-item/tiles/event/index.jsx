import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import DataState from './state/data';

import DiscoverEventImage from '../image/index.jsx';
import DockAction from '../../../../../action-command/dock/index.jsx';
import UserImage from '../../../../../creations/event-member-image/index.jsx';
import EventLink from '../../../../../creations/event-link/index.jsx';
import Location from '../../../../../../shared-components/location/index.jsx';
import UserLink from '../../../../../users/username-link/index.jsx';

import { metersToMiles } from '../../../../../../utils/converters/distance';
import { POSTFIX_POSITIONS } from '../../../../../action-command/dock/constants';
import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../../../../../models/events/constants';



@observer
class DiscoverEvent extends Component {
  isUnmounted = false;

  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState(props),
    }));
  }

  componentWillReceiveProps(nextProps) {
    const eventId = nextProps.event && nextProps.event.id;
    if (!eventId) return;
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    this.data.clear();
    this.data = null;
    this.isUnmounted = true;
  }

  render() {
    if (this.isUnmounted) return null;
    const { event, user, image, location, time, dockActionData, distance } = this.data;
    return (
      <div className='discover-tile'>
        <div className='discover-event discover-content'>
          <DiscoverEventImage id={event.id} type={EVENTS_MODEL_PLURAL} image={image} />
          <UserImage user={user} />
          <div className='discover-info'>
            <div className='details'>
              <div className='item-container'>
                <EventLink event={event} />
                { distance || distance === 0 ? <div className='distance'>{ metersToMiles(distance) }mi</div> : null}
              </div>
              <div className='item-container'>
                <div className='date-time-wrapper'>
                  <div className='time no-wrapping-text'>{ time }</div>
                </div>
                { location && <Location item={event} /> }
              </div>
              <div className='item-container'>
                <UserLink user={event.user} />
                <DockAction data={dockActionData} postfix postfixPosition={POSTFIX_POSITIONS.LEFT} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DiscoverEvent.propTypes = {
  event: PropTypes.object,
  needDate: PropTypes.bool,
};

DiscoverEvent.defaultProps = {
  needDate: false,
};

export default DiscoverEvent;
