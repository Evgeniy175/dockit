import { action, observable, computed } from 'mobx';
import moment from 'moment';
import { get } from 'lodash';

import { formatInterval, formatDate } from '../../../../../../../utils/formatting/date';
import { getLocation } from '../../../../../../../utils/location';

import { TYPES } from '../../../../../../../views/creations/constants';
import { EVENT_SHOW_CONSTANTS } from '../../../../../../../constants';

import EventModel from '../../../../../../../models/events';
import ScheduleModel from '../../../../../../../models/schedules';

import DataState from '../../../../../../../utils/state/data';



class DiscoverEventDataState extends DataState {
  @observable event = {};
  @observable user = {};
  @observable image;
  @observable location = {};
  @observable isDateNeeded = false;
  @observable time;
  @observable dockActionData;
  @observable distance;

  constructor(config = {}) {
    super(config);
    this.init(config);
  }

  init(props) {
    const { event, needDate } = props;
    this.setEvent(event);
    this.setUser(event.user);
    this.setDateVisibility(needDate);
    this.setImage(!event.schedule_id ? EventModel.formatImageUrl(event.image) : ScheduleModel.formatImageUrl(get(event, 'schedule.image')));
    this.setLocation(this.getLocation());
    this.setTime(this.getFormattedTime());
    this.setDockActionData(this.getDockActionData());
    this.setDistance(get(event, 'location.distance'));
  }

  getDockActionData() {
    return {
      type: TYPES.EVENT,
      event: this.event,
    };
  }

  getLocation() {
    return getLocation(this.event);
  }

  getFormattedTime() {
    return formatInterval(moment(this.event.startTime), moment(this.event.endTime));
  }

  getFormattedDate() {
    return formatDate(moment(this.event.startTime), EVENT_SHOW_CONSTANTS.DATE_FORMAT);
  }

  @action
  setEvent(event = {}) {
    this.event = event;
  }

  @action
  setUser(value) {
    this.user = value;
  }

  @action
  setDateVisibility(value) {
    this.isDateNeeded = value;
  }

  @action
  setImage(value) {
    this.image = value;
  }

  @action
  setLocation(value) {
    this.location = value;
  }

  @action
  setTime(value) {
    this.time = value;
  }

  @action
  setDockActionData(value) {
    this.dockActionData = value;
  }

  @action
  setDistance(value) {
    this.distance = value;
  }

  @action
  clear() {
    this.event = null;
  }
}

export default DiscoverEventDataState;
