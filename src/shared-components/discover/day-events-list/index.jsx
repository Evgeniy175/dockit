import React, { Component } from 'react';
import { action, observable, computed, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Event from './discover-item/tiles/event/index.jsx';
import ExpandButton from './expand-button/index.jsx';



@observer
class DiscoverLocationEventsList extends Component {
  constructor(props) {
    super(props);
    this.onExpand = ::this.onExpandHandler;
  }

  componentWillUnmount() {
    this.onExpand = null;
  }

  onExpandHandler() {
    const { data, handlers } = this.props;
    const { date }  = data;
    if (!handlers || !handlers.onExpand) return;
    handlers.onExpand(date);
  }

  render() {
    const { events, title, isFullyLoaded } = this.props.data;
    if (!events || !events.length) return null;
    return (
      <div>
        <span className='day-title'><span>{title}</span></span>
        { events.map(event => <Event key={event.id} event={event} needDate={false} />) }
        { !isFullyLoaded && <ExpandButton onClick={this.onExpand} /> }
      </div>
    );
  }
}

DiscoverLocationEventsList.propTypes = {
  data: PropTypes.shape({
    date: PropTypes.object,
    title: PropTypes.string.isRequired,
    events: PropTypes.array.isRequired,
    isFullyLoaded: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onExpand: PropTypes.func.isRequired,
  }),
};

export default DiscoverLocationEventsList;