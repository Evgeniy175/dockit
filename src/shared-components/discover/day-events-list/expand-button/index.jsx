import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class DayLocationExpandButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div className='day-content-expand-button' onClick={this.props.onClick}>SEE MORE FOR TODAY</div>;
  }
}

DayLocationExpandButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default DayLocationExpandButton;