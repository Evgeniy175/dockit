import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import AlertsModal from '../alerts-modal/index.jsx';

import { getSelectedAlertsAsText } from '../../utils/alerts';



@observer
class EventAlerts extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const data = this.props.data;
    const handlers = this.props.handlers;
    const selectedAlerts = getSelectedAlertsAsText(this.props.data.selectedAlerts);
    const labelClassName = `alerts-label${data.boldLabel ? ' bold-label' : ''}`;
    return (
      <FormGroup controlId='alerts' className='alerts-wrapper' onClick={this.props.handlers.onOpen}>
        <span className={labelClassName}>Alerts</span>
        <span className='chevron-wrapper'>
          <i className='arrow fa fa-angle-right fa-2x' />
        </span>
        <span className='alerts-list'>{selectedAlerts}</span>
        <AlertsModal data={data} handlers={handlers} />
      </FormGroup>
    );
  }
}

EventAlerts.propTypes = {
  data: PropTypes.shape({
    selectedAlerts: PropTypes.array.isRequired,
    boldLabel: PropTypes.bool,
  }).isRequired,
  handlers: PropTypes.shape({
    onOpen: PropTypes.func.isRequired,
  }).isRequired,
};

EventAlerts.defaultProps = {};

export default EventAlerts;
