import { action, observable, computed } from 'mobx';
import { get } from 'lodash';
import uuid from 'uuid/v1';

import DataState from '../../../utils/state/data';

import { EMPTY_CHAR } from '../../../constants';
import { USERTAG_START } from '../../../utils/formatting/constants';



class CreateCommentDataState extends DataState {
  id;

  @observable userTags;
  @observable user;
  @observable comment = '';
  @observable imageFile = null;

  @observable isCommentWindowBlocked = false;

  @observable isUsersDropdownShown = false;
  @observable usersDropdownValue = '';

  constructor(props = {}) {
    super(props);
    this.init(props);
    this.id = uuid();
  }

  init(props) {
    this.setUser(props.values.user);
    this.setUserTags(props.values.userTags);
    if (!this.comment) this.setComment(this.userTags ? Array.from(this.userTags).map(userTag => `@${userTag} `).join('') : '');
  }

  isTextAreaCanBeFocused() {
    return this.userTags && this.userTags.size > 0;
  }

  onCommentChange(e) {
    this.setComment(e.target.value);
  }

  onPictureInputChange(e) {
    let files;

    e.preventDefault();

    if (e.dataTransfer) files = e.dataTransfer.files;
    else if (e.target) files = e.target.files;

    if (!files || !files[0]) return;

    const reader = new FileReader();

    reader.onload = () => {
      const imageUrl = reader.result;
      this.setImageFile(imageUrl);
    };
    reader.readAsDataURL(files[0]);

  }

  onPostCommentButtonClick() {
    if (this.comment == '' && !this.isImageAttached()) return;

    return {
      text: this.comment.length ? this.comment : EMPTY_CHAR,
      image: this.imageFile
    };
  }

  removeAttachedImage() {
    this.setImageFile();
  }

  isImageAttached() {
    return !!this.imageFile;
  }

  @action
  setUser(value) {
    this.user = value;
  }

  @action
  setIsCommentWindowBlocked(value) {
    this.isCommentWindowBlocked = value;
  }

  @action
  setUsersDropdownValue(value) {
    this.usersDropdownValue = value;
  }

  @action
  setUserTags(value) {
    this.userTags = value;
  }

  @action
  setComment(value = '') {
    this.comment = value;
    this.handleCommentChange();
  }

  handleCommentChange() {
    const idx = this.comment.lastIndexOf(USERTAG_START);

    if (idx === -1) return this.setUsersDropdownVisibility(false);

    const text = this.comment.substring(idx);
    const re = new RegExp(`(^|\\s)${USERTAG_START}\\w*$`, 'g');
    const isUsertagInputting = re.test(this.comment);

    if (!isUsertagInputting) return this.setUsersDropdownVisibility(false);
    if (!this.isUsersDropdownShown) this.setUsersDropdownVisibility(true);

    this.setUsersDropdownValue(text);
  }

  @action
  setUsersDropdownVisibility(value) {
    this.isUsersDropdownShown = value;
  }

  @action
  attachUsernameToComment(value = '') {
    const beforeLastUsertagIdx = this.comment.lastIndexOf(USERTAG_START);

    if (beforeLastUsertagIdx === -1) return;

    this.comment = `${this.comment.substring(0, beforeLastUsertagIdx)}${USERTAG_START}${value} `;
    this.setUsersDropdownVisibility(false);
  }

  @action
  setImageFile(value = null) {
    this.imageFile = value;
  }

  getIdOfPictureUploadInput() {
    return `comment-picture-upload-input_${this.id}`;
  }
}

export default CreateCommentDataState;
