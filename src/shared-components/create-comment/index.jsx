import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Button } from 'react-bootstrap';

import EventMemberImage from '../creations/event-member-image/index.jsx';
import CommentTextArea from '../textarea/index.jsx';
import PictureUploadButton from './picture-upload-button/index.jsx';
import Image from './image/index.jsx';
import UsersDropdown from '../users/dropdown/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import { Auth } from '../../auth';

import { TYPES, BUTTONS_TEXTS, DEFAULT_PLACEHOLDER } from './constants';



@observer
class CreateComment extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(),
    });

    this.onPictureUploadButtonClick = ::this.onPictureUploadButtonClickHandler;
    this.onCommentChange = ::this.commentChangeHandler;
    this.onPictureInputChange = ::this.pictureInputChangeHandler;
    this.onApply = ::this.onApplyHandler;
    this.removeAttachedImage = ::this.removeAttachedImageHandler;
    this.onPaste = ::this.onPasteHandler;
    this.onUsersDropdownSelect = ::this.onUsersDropdownSelectHandler;
    this.onResize = ::this.onResizeHandler;
  }

  componentDidMount() {
    const elem = document.getElementById('create-comment-wrapper');
    if (!elem) return;
    window.addEventListener('resize', this.onResize);
    elem.addEventListener('paste', this.onPaste);
    if (this.props.handlers.onRendered) this.props.handlers.onRendered();
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    const elem = document.getElementById('create-comment-wrapper');
    if (!elem) return;
    window.removeEventListener('resize', this.onResize);
    elem.removeEventListener('paste', this.onPaste);
  }

  onPictureUploadButtonClickHandler(e) {
    e.preventDefault();
    const uploadInput = document.getElementById(this.data.getIdOfPictureUploadInput());
    uploadInput.value = null;
    uploadInput.click();
  }

  commentChangeHandler(e) {
    this.data.onCommentChange(e);
  }

  pictureInputChangeHandler(e) {
    this.data.onPictureInputChange(e);
  }

  onApplyHandler(e) {
    e.preventDefault();
    if (this.data.isCommentWindowBlocked) return;
    const result = this.data.onPostCommentButtonClick(e);
    if (!result) return Promise.resolve();
    this.data.setIsCommentWindowBlocked(true);
    
    return this.props.handlers.onSubmit(result.text, result.image)
    .finally(() => {
      this.data.setComment();
      this.data.setImageFile();
      this.data.setIsCommentWindowBlocked(false);
    });
  }

  removeAttachedImageHandler() {
    this.data.removeAttachedImage();
  }

  onUsersDropdownSelectHandler(userName) {
    this.data.attachUsernameToComment(userName);
  }

  onResizeHandler() {
    this.ui.setUsersDropdownWidth();
  }

  onPasteHandler(e) {
    if (this.props.values.isAnyCommentEditing) return;
    const items = e.clipboardData.items;

    for (const item of items)
      if (item.kind === 'file')
        return this.getAsFile(e, item);
  }

  getAsFile(e, item) {
    const blob = item.getAsFile();
    const reader = new FileReader();
    reader.onload = event => { this.data.setImageFile(event.target.result); };
    reader.readAsDataURL(blob);
    e.preventDefault();
  }

  render() {
    const user = Auth.getActiveUser();

    if (!user) return null;

    const isImageAttached = this.data.isImageAttached();
    const { imageFile, comment, isUsersDropdownShown, usersDropdownValue } = this.data;
    const buttonClass = `create-comment-btn no-select${comment === '' && !isImageAttached ? '' : ' active'}`;
    const isUsersDropdownVisible = toJS(isUsersDropdownShown);
    const usersDropdownCss = this.ui.getUsersDropdownCss();

    const textAreaValues = {
      id: this.ui.getTextAreaId(),
      value: comment,
      rows: this.props.values.rows,
      placeholder: this.props.values.placeholder || DEFAULT_PLACEHOLDER,
    };

    const textAreaHandlers = {
      onChange: this.onCommentChange,
    };

    return (
      <div className='add-comment' id='create-comment-wrapper'>
        <div  className='create-comment-wrapper'>
          <div className='create-comment-inner-wrapper'>
            <div className='left'>
              <EventMemberImage user={user} />
            </div>
            <div className='right'>
              <UsersDropdown css={usersDropdownCss} open={isUsersDropdownVisible} position={this.props.values.usersDropdownPosition} value={usersDropdownValue} onClick={this.onUsersDropdownSelect} />
              <CommentTextArea values={textAreaValues} handlers={textAreaHandlers} />
              <PictureUploadButton id={this.data.id} onChange={this.onPictureInputChange} onClick={this.onPictureUploadButtonClick} />
              <Button className={buttonClass} disabled={this.data.isCommentWindowBlocked} onClick={this.onApply}>Post</Button>
            </div>
          </div>
          <Image isImageAttached={isImageAttached} imageFile={imageFile} removeAttachedImage={this.removeAttachedImage} />
        </div>
      </div>
    );
  }
}

CreateComment.propTypes = {
  values: PropTypes.shape({
    user: PropTypes.object,
    userTags: PropTypes.object, // list of usertags that will be printed at the front of the comment. It's set of the usertags
    isAnyCommentEditing: PropTypes.bool,
    rows: PropTypes.number, // number of shown rows in textarea
    placeholder: PropTypes.string, // placeholder that will be used
    usersDropdownPosition: PropTypes.string, // position from `POSITIONS` object
  }).isRequired,
  handlers: PropTypes.shape({
    onSubmit: PropTypes.func.isRequired,
    onRendered: PropTypes.func,
  }).isRequired,
};

export default CreateComment;
