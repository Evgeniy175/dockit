import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Svg from 'react-svg';

import PhotoIcon from '../../../assets/images/icons/photo.svg';



@observer
class PictureUploadButton extends Component {
  id;

  constructor(props) {
    super(props);
    this.id = props.id;
    this.onChange = ::this.onChangeHandler;
    this.onClick = ::this.onClickHandler;
  }

  onChangeHandler(e) {
    this.props.onChange(e);
  }

  onClickHandler(e) {
    this.props.onClick(e);
  }

  getIdOfPictureUploadInput() {
    return `comment-picture-upload-input_${this.id}`;
  }

  render() {
    return (
      <div>
        <input className="comment-picture-upload-input" id={this.getIdOfPictureUploadInput()} type='file' onChange={this.onChange} />
        <span className='picture-upload-btn' onClick={this.onClick}>
          <Svg path={PhotoIcon} />
        </span>
      </div>
    );
  }
}

PictureUploadButton.propTypes = {
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default PictureUploadButton;
