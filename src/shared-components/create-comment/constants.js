export const TYPES = {
  CREATE: 'create',
  EDIT: 'update',
};

export const BUTTONS_TEXTS = {
  [TYPES.CREATE]: 'Post',
  [TYPES.EDIT]: 'Edit',
};

export const DEFAULT_PLACEHOLDER = 'Write your comment...';
