import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Svg from 'react-svg';

import RemoveIcon from '../../../assets/images/icons/remove-image.svg';
import LightboxWrapper from '../../lightbox-wrapper/index.jsx';



@observer
class CreateCommentAttachedImage extends Component {
  @observable isLightboxOpen = false;

  constructor(props) {
    super(props);
    this.onLightboxOpen = ::this.onLightboxOpenHandler;
    this.onLightboxClose = ::this.onLightboxCloseHandler;
  }

  onLightboxOpenHandler() {
    this.setLightBoxVisibility(true);
  }

  onLightboxCloseHandler() {
    this.setLightBoxVisibility(false);
  }

  @action
  setLightBoxVisibility(value) {
    this.isLightboxOpen = value;
  }

  render() {
    const { isImageAttached, imageFile, removeAttachedImage } = this.props;

    if (!isImageAttached) return null;

    const lightboxImages = [{ src: imageFile }];

    return (
      <div className='attached-image'>
        <img src={imageFile} onClick={this.onLightboxOpen} />
        <span onClick={removeAttachedImage}>
          <Svg className='remove' path={RemoveIcon} />
        </span>
        <LightboxWrapper open={this.isLightboxOpen} images={lightboxImages} onClose={this.onLightboxClose} />
      </div>
    );
  }
}

CreateCommentAttachedImage.propTypes = {
  isImageAttached: PropTypes.bool,
  imageFile: PropTypes.string,
  removeAttachedImage: PropTypes.func,
};

export default CreateCommentAttachedImage;
