import { action, observable, computed } from 'mobx';
import { imageToFormData } from './../../../../utils/formatting/image';
import ImagesModel from '../../../../models/images/index';

const imagesModel = new ImagesModel();


class EventPhotosDataState {
  @observable imageFile = null;

  onPictureInputChange(e, id, target) {
    let files;

    e.preventDefault();

    if (e.dataTransfer) files = e.dataTransfer.files;
    else if (e.target) files = e.target.files;

    if (!files || !files[0]) return;

    const reader = new FileReader();

    reader.onload = () => {
      const imageUrl = reader.result;
      this.setImageFile(imageUrl);
      this.attachImage(id, target);
    };
    reader.readAsDataURL(files[0]);
  }

  attachImage(id, target){
    const formData = imageToFormData(this.imageFile);
    return imagesModel.attachImage(id, target, { body: formData });
  }

  clear(){
    this.setImageFile();
  }

  @action
  setImageFile(value = null) {
    this.imageFile = value;
  }
}

export default EventPhotosDataState;