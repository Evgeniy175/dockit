export const RESOLUTION = 160;

export const WRAPPER_CLASS_NAME = 'attached-preview-image-item-wrapper';
export const IMAGE_CLASS_NAME = 'attached-preview-image-item';
export const EVENT_POSTFIX = '-album';