import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import DataState from './state/data.js';
import AttachImage from './../attach-image/index.jsx';
import AttachedImagePreviewitem from './preview-image-item/index.jsx';

@observer
class EventDescriptionImages extends Component {
  constructor(props) {
    super(props);

    Object.assign(this, {
      data: new DataState()
    });

    this.onAddImage = ::this.onAddPhotoHandler;
  }

  componentWillUnmount(){
    this.onAddImage = null;
    this.data.clear();
    this.data = null;
  }

  onAddPhotoHandler(e){
    const { id, target } = this.props.values;
    this.data.onPictureInputChange(e, id, target);
  }

  render() {
    const { id, target, images } = this.props.values;
    const isImagesExists = images.length > 0;
    const attachImageValues = {
      id: id,
      isImagesExists: isImagesExists
    };
    const attachImageHandlers = {
      onAddImage: this.onAddImage
    };

    if(!isImagesExists)
      return (
        <div className="event-images-wrapper">
          <AttachImage values={attachImageValues} handlers={attachImageHandlers}/>
        </div>
      );

    return (
      <div className="event-images-wrapper">
        <div className="attached-image-item-preview-wrapper">
          <div className="attached-image-item-preview">
            <AttachImage values={attachImageValues} handlers={attachImageHandlers}/>
            { images.map(image => <AttachedImagePreviewitem target={target} image={image} key={image.id}/>) }
          </div>
        </div>
        <a className="see-all-images-btn" href={`/${target}/${id}/images`}>See all</a>
      </div>
    );
  }
}

EventDescriptionImages.propTypes = {
  images: PropTypes.array,
  values: PropTypes.shape({
    target: PropTypes.string,
    id: PropTypes.string
  }),
};

export default EventDescriptionImages;