import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { getImageUrl } from '../../../../utils/formatting/image';
import {getResolution} from '../../../../utils/images/resolution';
import { ASPECT_RATIOS } from '../../../../shared-components/image/constants.js';
import Image from '../../../../shared-components/image/index.jsx';
import { RESOLUTION, WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME, EVENT_POSTFIX } from './constants.js';

@observer
class ImageItem extends Component {
  constructor(props){
    super(props);

    this.imageValues = {
      src: getImageUrl(`${props.target}${EVENT_POSTFIX}`, props.value.url, getResolution(RESOLUTION)),
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s1x1,
      prepareWrapper: true,
    };
  }

  render() {
    return (
      <div className="image-item-wrapper">
        <Image values={this.imageValues}/>
      </div>
    );
  }
}

ImageItem.propTypes = {
  value: PropTypes.object,
  target: PropTypes.string
};


export default ImageItem;