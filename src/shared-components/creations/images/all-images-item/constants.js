export const RESOLUTION = 640;

export const WRAPPER_CLASS_NAME = 'see-all-attached-image-item-wrapper';
export const IMAGE_CLASS_NAME = 'see-all-attached-image-item';
export const EVENT_POSTFIX = '-album';