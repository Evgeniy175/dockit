import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Textarea from '../../textarea/index.jsx';

@observer
class EventCreateDescription extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDescriptionChange = ::this.onDescriptionChangeHandler;
  }
  
  onDescriptionChangeHandler(e) {
    this.props.onDescriptionChange(e.target.value);
  }
  
  render() {
    const values = {
      id: 'event-description',
      value: this.props.value,
      placeholder: 'Description',
    };
    const handlers = {
      onChange: this.onDescriptionChange,
    };
    
    return (
      <FormGroup controlId='description'>
        <Textarea values={values} handlers={handlers} />
      </FormGroup>
    );
  }
}

EventCreateDescription.propTypes = {
  value: PropTypes.string.isRequired,
  onDescriptionChange: PropTypes.func.isRequired
};

export default EventCreateDescription;
