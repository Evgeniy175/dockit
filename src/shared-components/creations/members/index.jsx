import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import EventMemberImage from '../event-member-image/index.jsx';

@observer
class EventDescriptionMembers extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const members = this.props.members;
    const isMembersExists = members.length > 0;
    
    return isMembersExists &&
      <div className='members'>
        { members.map(member => <EventMemberImage key={member.data.id} user={member} />) }
      </div>;
  }
}

EventDescriptionMembers.propTypes = {
  members: PropTypes.array.isRequired
};

export default EventDescriptionMembers;
