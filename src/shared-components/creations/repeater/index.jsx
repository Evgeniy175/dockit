import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import RepeatView from '../repeat/index.jsx';


@observer
class EventCreateRepeater extends Component {
  render() {
    const { onRepeatDataChanged } = this.props;
    
    return (
      <FormGroup controlId='repeat' className='no-border'>
        <RepeatView {...this.props} scheduleConfigHandler={onRepeatDataChanged} />
      </FormGroup>
    );
  }
}

EventCreateRepeater.propTypes = {
  isBlocked: PropTypes.bool,
  scheduleConfig: PropTypes.object,
  scheduleEnd: PropTypes.object,
  errors: PropTypes.object,
  onRepeatDataChanged: PropTypes.func,
};

export default EventCreateRepeater;
