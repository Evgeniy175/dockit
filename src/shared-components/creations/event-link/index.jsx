import React, { Component } from 'react';
import { Link } from 'react-router';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class EventLink extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { event, message } = this.props;
    const eventUrl = `/events/${event.id}`;
    return (
      <span>
        <Link to={eventUrl} className="event-link no-wrapping-text">{event.title}</Link>
        { message }
      </span>
    );
  }
}

EventLink.propTypes = {
  event: PropTypes.object.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ])
};

export default EventLink;