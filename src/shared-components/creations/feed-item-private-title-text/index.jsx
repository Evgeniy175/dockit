import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import LockClosed from '../../../assets/images/icons/lock-closed.svg';



@observer
class PrivateEventText extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { type, isPrivate } = this.props;

    if (!isPrivate) return null;

    return (
      <div className='private-event-text'>
        <img className='lock-closed' src={LockClosed} />
        This is a private {type}
      </div>
    );
  }
}

PrivateEventText.propTypes = {
  type: PropTypes.string.isRequired,
  isPrivate: PropTypes.bool,
};

export default PrivateEventText;
