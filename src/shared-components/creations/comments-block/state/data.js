import { action, observable, computed, toJS } from 'mobx';
import Promise from 'bluebird';
import uniqBy from 'lodash/uniqBy';

import { COMMENT_SCROLL_TIMEOUT, COMMENT_AFTER_SCROLL_TIMEOUT, MAX_TIMES } from '../constants';
import { EVENT_SHOW_CONSTANTS } from '../../../../constants';

import { Auth } from '../../../../auth';
import { imageToFormData } from '../../../../utils/formatting/image';

import CommentModel from '../../../../models/comments';
import UserModel from '../../../../models/users';

import jq from '../../../../utils/jquery';

import DataState from '../../../../utils/state/data';

const commentModel = new CommentModel();
const userModel = new UserModel();



class CommentsBlockDataState extends DataState {
  @observable comments = {};
  @observable user;
  @observable fromComment;
  @observable editingComments = [];

  fromUser;

  id;
  isForSchedule = false;

  isMoreCommentsLoadingAvailable = true;

  scrollToComment;
  intervalId;
  counter = 0;

  get isSigned() {
    return Auth.isSigned();
  }

  constructor(props = {}) {
    super(props);
    this.init(props);
    this.tryToScrollToComment = ::this.tryToScrollToCommentHandler;
  }

  init(props) {
    this.id = props.id;
    this.isForSchedule = props.isForSchedule;
  }

  fetchCurrentUser() {
    return userModel.fetchProfileInfo()
      .then(result => this.setUser(result));
  }

  fetchComments() {
    const config = {
      params: {
        limit: EVENT_SHOW_CONSTANTS.COMMENTS_PER_PORTION,
        page: 1,
      }
    };

    const fetchPromise = this.isForSchedule
      ? commentModel.fetchScheduleComments(this.id, config)
      : commentModel.fetchEventComments(this.id, config);

    return fetchPromise.then(comments => {
      this.setComments(comments);
      this.initScrollToComment();
      return Promise.resolve();
    });
  }

  initScrollToComment() {
    if (!this.scrollToComment || this.intervalId) return;
    this.intervalId = setInterval(this.tryToScrollToComment, COMMENT_SCROLL_TIMEOUT);
  }

  tryToScrollToCommentHandler() {
    const elem = jq.wrap(`.${this.scrollToComment}`);
    if (this.counter++ > MAX_TIMES) return clearInterval(this.intervalId);
    if (!elem.isValid()) return;
    elem.centerVertically();
    elem.addClass('target-comment');
    clearInterval(this.intervalId);
    setTimeout(() => elem.removeClass('target-comment'), COMMENT_AFTER_SCROLL_TIMEOUT);
  }

  fetchMoreComments() {
    const config = {
      params: {
        limit: EVENT_SHOW_CONSTANTS.COMMENTS_PER_PORTION,
        page: this.comments.data && this.comments.nextPage ? this.comments.nextPage : 1
      }
    };

    const fetchPromise = this.isForSchedule ? commentModel.fetchScheduleComments(this.id, config) : commentModel.fetchEventComments(this.id, config);
    return fetchPromise.then(comments => this.appendComments(comments));
  }

  isCommentsLoaded() {
    return this.comments && this.comments.data;
  }

  addComment(text, image) {
    const addCommentPromise = this.isForSchedule ? commentModel.addScheduleComment(this.id, text) : commentModel.addEventComment(this.id, text);

    return addCommentPromise
    .then(newComment => {
      this.appendNewComment(newComment);
      this.setFromComment();

      if (!image) return Promise.resolve();

      const formData = imageToFormData(image);
      const config = { body: formData };

      return commentModel.attachPictureToComment(newComment.id, config);
    });
  }

  @action
  editComment(comment) {
    const idx = this.comments.data.findIndex(c => comment.id === c.id);
    this.comments.data[idx] = Object.assign(this.comments.data[idx], comment);
    this.comments = toJS(this.comments);
  }

  @action
  deleteComment(id) {
    this.comments.data = this.comments.data.filter(comment => comment.id !== id);
  }

  @action
  attachImage(value) {
    if(!value) return;
    const comment = this.comments.data.find(comment => comment.id === value.id);
    comment.imageUrl = value.imageUrl;
  }

  @action
  appendNewComment(newComment) {
    this.comments.data.unshift(newComment);
  }

  @action
  appendComments(comments) {
    this.comments = observable({
      data: uniqBy(this.comments.data.concat(comments.data), (comment) => comment.id),
      nextPage: comments.nextPage,
      maxPage: comments.maxPage,
    });
  }

  @action
  setComments(value = {}) {
    this.comments = value;
  }

  @action
  setUser(value) {
    this.user = value;
  }

  @action
  setFromComment(value) {
    this.fromComment = value;
  }

  @action
  addEditingComment(id) {
    this.editingComments.push(id);
  }

  @action
  removeEditingComment(id) {
    this.editingComments = this.editingComments.filter(comment => comment !== id);
  }
}


export default CommentsBlockDataState;
