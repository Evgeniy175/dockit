import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import qs from 'query-string';

import Comment from '../../comment/index.jsx';
import CommentAdd from '../../create-comment/index.jsx';
import UnauthText from '../../unauthorized/text/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import jq from '../../../utils/jquery';

import { handleButton } from '../../../utils/scrolling';
import { getLocation } from '../../../utils/dom/window';

import { TARGETS } from '../../unauthorized/text/constants';
import { TYPES } from '../../create-comment/constants';



@observer
class CommentsBlock extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState(props),
      ui: new UiState()
    }));

    this.onWheel = ::this.onWheelHandler;
    this.addComment = ::this.addCommentHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.getContainer = ::this.getContainerHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;

    this.params = qs.parse(getLocation().search);
    this.data.scrollToComment = this.params.commentId;
    if (this.params.fromUser) this.data.fromUser = { user: { username: this.params.fromUser } };

    this.handlers = {
      onEditStart: ::this.onEditStartHandler,
      onReply: ::this.onReplyOnCommentHandler,
      onEdit: ::this.editCommentHandler,
      onDelete: ::this.deleteCommentHandler
    };
  }

  componentWillMount() {
    if (!this.data.isSigned) return;
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);

    this.data.fetchCurrentUser();
    this.data.fetchComments();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.data.isSigned || this.data.id === nextProps.id) return;
    this.data.init(nextProps);
    this.data.fetchComments();
  }

  componentDidUpdate() {
    const isScrollToComments = getLocation().hash === `#comments`;
    const isLoaded = this.data.isCommentsLoaded();

    this.params = qs.parse(getLocation().search);
    this.data.scrollToComment = this.params.commentId;

    if (this.params.fromUser) this.data.fromUser = { user: { username: this.params.fromUser } };
    if (!isScrollToComments || !isLoaded || this.isScrollPerformed) return;

    const item = jq.wrap('#comments');
    if (!item.isValid()) return;
    item.centerVertically();
    this.isScrollPerformed = true;
  }

  componentWillUnmount() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
  }

  onWheelHandler() {
    if (!this.data.comments.nextPage) return;

    const isNeedToLoad = this.ui.isNeedToLoadMore();

    if (!isNeedToLoad || !this.data.isMoreCommentsLoadingAvailable) return;

    this.data.isMoreCommentsLoadingAvailable = false;

    return this.data.fetchMoreComments()
    .finally(() => this.data.isMoreCommentsLoadingAvailable = true);
  }

  addCommentHandler(text, image) {
    return this.data.addComment(text, image)
    .then(res => this.data.attachImage(res));
  }

  onReplyOnCommentHandler(comment) {
    this.ui.commentCreating.centerVertically();
    this.data.setFromComment(comment);
  }

  onEditStartHandler(id) {
    this.data.addEditingComment(id);
  }

  editCommentHandler(comment) {
    this.data.removeEditingComment(comment.id);
    this.data.editComment(comment);
  }

  deleteCommentHandler(id) {
    this.data.deleteComment(id);
  }

  onKeyDownHandler(e) {
    const isModalTarget = e.target.className.includes('modal');
    return isModalTarget ? null : handleButton(e, this.getContainer, this.onButtonScroll);
  }

  getContainerHandler() {
    return document.body;
  }

  onButtonScrollHandler() {
    this.onWheel();
  }

  render() {
    if (!this.data.isSigned) return <UnauthText target={TARGETS.COMMENTS} />;
    const { isUserCreation } = this.props;
    const { editingComments, fromComment, fromUser } = this.data;
    const isAnyCommentEditing = editingComments && editingComments.length > 0;
    const isLoaded = this.data.isCommentsLoaded();
    const comments = isLoaded ? toJS(this.data.comments.data) : [];
    const userTagsData = fromComment && fromComment.user
      ? fromComment.user.username
      : fromUser ? fromUser.username : null;

    const commentValues = {
      type: TYPES.CREATE,
      isAnyCommentEditing: isAnyCommentEditing,
      userTags: userTagsData ? new Set([userTagsData]) : null,
      user: this.data.user,
    };

    const commentHandlers = {
      onSubmit: this.addComment,
    };

    return (
      <div className='comments-block' id='comments'>
        <CommentAdd values={commentValues} handlers={commentHandlers} />
        { comments.map(comment => <Comment key={`${comment.id}${comment.message}${comment.imageUrl}`} isUserCreation={isUserCreation} handlers={this.handlers} comment={comment} />) }
      </div>
    );
  }
}

export default CommentsBlock;
