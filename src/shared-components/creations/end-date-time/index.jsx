import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { get, isEmpty } from 'lodash';

import DateTimeSelector from '../../date-time-selector/index.jsx';



@observer
class EventCreateEndDateTime extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onEndDateChange = ::this.onEndDateChangeHandler;
    this.onEndTimeChange = ::this.onEndTimeChangeHandler;
  }
  
  onEndDateChangeHandler(value) {
    this.props.onEndDateChange(value);
  }
  
  onEndTimeChangeHandler(value) {
    this.props.onEndTimeChange(value || '00:00');
  }
  
  render() {
    const { isAllDay, endDate, endTime, errors } = this.props;
    const isErrorExists = get(errors, 'date') || get(errors, 'time') || get(errors, 'both');
    const className = `${isErrorExists ? ` form-group-error` : ''}`;
    
    return (
      <FormGroup controlId='endDateTime' className={className}>
        <DateTimeSelector dateLabel='Ends'
                          date={endDate}
                          dateChangeHandler={this.onEndDateChange}
                          timeShown={!isAllDay}
                          timeLabel='Time'
                          time={endTime}
                          timeChangeHandler={this.onEndTimeChange}
                          errors={errors} />
      </FormGroup>
    );
  }
}

EventCreateEndDateTime.propTypes = {
  isAllDay: PropTypes.bool.isRequired,
  endDate: PropTypes.string.isRequired,
  endTime: PropTypes.string.isRequired,
  errors: PropTypes.object,
  onEndDateChange: PropTypes.func.isRequired,
  onEndTimeChange: PropTypes.func.isRequired
};

export default EventCreateEndDateTime;
