import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Checkbox from '../../toggle/index.jsx';

@observer
class EventCreateAllDay extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onAllDayChange = ::this.onAllDayChangeHandler;
  }
  
  onAllDayChangeHandler() {
    this.props.onAllDayChange();
  }
  
  render() {
    const isAllDay = this.props.isAllDay;
    
    return (
      <FormGroup controlId='allDayCheckbox'>
        <Checkbox label='All day' value={isAllDay} onClick={this.onAllDayChange} />
      </FormGroup>
    );
  }
}

EventCreateAllDay.propTypes = {
  isAllDay: PropTypes.bool.isRequired,
  onAllDayChange: PropTypes.func.isRequired
};

export default EventCreateAllDay;
