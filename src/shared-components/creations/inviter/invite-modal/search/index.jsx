import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { PLACEHOLDER } from './constants';



@observer
class InvitePeopleModalSearch extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { value, handlers } = this.props;

    return (
      <div className='invite-modal-search'>
        <input type='text' placeholder={PLACEHOLDER} value={value} onChange={handlers.onChange} />
      </div>
    );
  }

  getItems(sorted, key) {
    const upperItems = get(sorted, key, []);
    const lowerItems = get(sorted, key.toLowerCase(), []);
    return upperItems.concat(lowerItems);
  }
}

InvitePeopleModalSearch.propTypes = {
  value: PropTypes.string.isRequired,
  handlers: PropTypes.object,
};

export default InvitePeopleModalSearch;
