import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { PROFILE_PICTURE_RESOLUTION } from './constants';

import CheckedImage from '../../../../../../../assets/images/icons/checked.svg';

import UserModel from '../../../../../../../models/users';



@observer
class InvitePeopleModalItem extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const { item, handlers } = this.props;

    if (item.isAlreadyInvited) return;

    const handler = get(handlers, 'onItemClick');
    handler(item.id);
  }
  
  render() {
    const { item } = this.props;
    const selected = item.isChecked || item.isAlreadyInvited;
    const profilePicture = UserModel.formatImageUrl(item.profilePicture, PROFILE_PICTURE_RESOLUTION);
    
    return (
      <div className='invite-people-modal-item no-select' onClick={this.onClick}>
        <span className='image-wrapper'>
          <img src={profilePicture} />
        </span>
        <span className='person-data'>
          <div className='person-name'>{item.name}</div>
          <div className='person-username'>{`@${item.username}`}</div>
        </span>
        <span className='checked-icon'>
          { selected && <img src={CheckedImage} /> }
        </span>
      </div>
    );
  }
}

InvitePeopleModalItem.propTypes = {
  item: PropTypes.object.isRequired,
  handlers: PropTypes.object
};

export default InvitePeopleModalItem;
