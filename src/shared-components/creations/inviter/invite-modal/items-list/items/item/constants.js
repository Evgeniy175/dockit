import {MIN_RESOLUTION} from '../../../../../../../utils/images/resolution/constants';

export const PROFILE_PICTURE_RESOLUTION = MIN_RESOLUTION;
