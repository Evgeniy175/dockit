import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Item from './item/index.jsx';



@observer
class InvitePeopleModalItems extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { items, symbol, handlers } = this.props;
    
    return (
      <div className='invite-people-modal-items'>
        <div className='list-symbol'>
          { symbol }
        </div>
        <div className='invite-people-modal-items-part'>
          {
            items.map(item => <Item key={item.id} item={item} handlers={handlers} />)
          }
        </div>
      </div>
    );
  }
}

InvitePeopleModalItems.propTypes = {
  symbol: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  handlers: PropTypes.object
};

export default InvitePeopleModalItems;
