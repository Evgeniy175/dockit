import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get, groupBy, uniq } from 'lodash';

import { EMPTY_TEXT } from '../constants';

import Items from './items/index.jsx';
import CenteredText from '../../../../centered-text/index.jsx';



@observer
class InvitePeopleModalItemsList extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { loaded, items, handlers } = this.props;
    const sorted = groupBy(items, 'name[0]');
    const keys = Object.keys(sorted).map(key => key.toUpperCase());
    const sortedKeys = uniq(keys).sort((a, b) => a.localeCompare(b));

    return (
      <div className='modal-data-container invite-people-modal-items-list'>
        { !loaded && <CenteredText text={EMPTY_TEXT} /> }
        { sortedKeys.map(key => <Items key={key} symbol={key} items={this.getItems(sorted, key)} handlers={handlers} />) }
      </div>
    );
  }

  getItems(sorted, key) {
    const upperItems = get(sorted, key, []);

    if (key === key.toLowerCase()) return upperItems;

    const lowerItems = get(sorted, key.toLowerCase(), []);
    return upperItems.concat(lowerItems);
  }
}

InvitePeopleModalItemsList.propTypes = {
  loaded: PropTypes.bool.isRequired,
  items: PropTypes.array.isRequired,
  handlers: PropTypes.object
};

export default InvitePeopleModalItemsList;
