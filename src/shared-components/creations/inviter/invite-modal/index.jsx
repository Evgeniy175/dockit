import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { TITLE, TYPES } from './constants';

import { handleButton } from '../../../../utils/scrolling';

import Search from './search/index.jsx';
import ItemsList from './items-list/index.jsx';
import ModalDoneButton from '../../../modal/done-button/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class InvitePeopleModal extends Component {
  constructor(props) {
    super(props);
    this.onItemClick = ::this.onItemClickHandler;
    this.onClose = ::this.onCloseHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
    this.onInviteModalWheel = ::this.onInviteModalWheelHandler;
    this.getContainer = ::this.getContainerHandler;
    this.onSearchChange = ::this.onSearchChangeHandler;

    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(),
    });

    this.closeHandlers = {
      [TYPES.EVENT]: ::this.data.getEventNotificationsRequests,
      [TYPES.SCHEDULE]: ::this.data.getScheduleNotificationsRequests,
    };

    this.data.fetchPeopleForInvite()
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
    if (!nextProps.open) return;
    document.body.addEventListener('mousewheel', this.onInviteModalWheel);
    document.body.addEventListener('touchmove', this.onInviteModalWheel);
    document.body.addEventListener('keydown', this.onKeyDown);
  }

  onModalCloseHandler() {
    this.props.onClose();
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onInviteModalWheel);
    document.body.removeEventListener('mousewheel', this.onInviteModalWheel);
  }

  onKeyDownHandler(e) {
    return handleButton(e, this.getContainer, this.onButtonScroll);
  }

  onButtonScrollHandler() {
    this.onInviteModalWheelHandler();
  }

  onInviteModalWheelHandler() {
    if (!get(this.data, 'followers.nextPage') && !get(this.data, 'followings.nextPage')) return;

    const isNeedToLoad = this.ui.isNeedToLoadMore();

    if (!isNeedToLoad || !this.ui.isLoaded) return;

    this.ui.setIsLoaded(false);

    return this.data.fetchMorePeopleForInvite()
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }

  getContainerHandler() {
    return document.getElementsByClassName('modal-body')[0];
  }
  
  onItemClickHandler(value) {
    this.data.handleItemClick(value);
  }
  
  onCloseHandler() {
    const { type } = this.data;
    if (this.closeHandlers[type]) this.closeHandlers[type]();
    this.props.onClose(toJS(this.data.peopleForInvite));
  }

  onSearchChangeHandler(e) {
    e.preventDefault();
    this.data.setSearchValue(e.target.value);
  }
  
  render() {
    const { open } = this.props;
    const { peopleForInvite, searchValue } = this.data;
    const { isLoaded } =this.ui;
    
    const handlers = {
      onItemClick: this.onItemClick
    };

    const searchHandlers = {
      onChange: this.onSearchChange,
    };

    return (
      <div>
        <Modal id='invite-modal-wrapper' show={open} onHide={this.onClose}>
          <Modal.Header>
            <Modal.Title id={TITLE}>{ TITLE }</Modal.Title>
            <ModalDoneButton />
          </Modal.Header>
          <Modal.Body>
            <Search value={searchValue} handlers={searchHandlers} />
            <ItemsList loaded={isLoaded} items={peopleForInvite} handlers={handlers} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

InvitePeopleModal.propTypes = {
  values: PropTypes.shape({
    invitedPeople: PropTypes.array,
  }),
  item: PropTypes.object,
  type: PropTypes.string,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default InvitePeopleModal;
