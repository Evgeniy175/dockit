import { action, observable, toJS } from 'mobx';

import UIState from '../../../../../utils/state/data';

import { LOAD_BUFFER_FROM_BOTTOM } from '../constants';



class InvitePeopleModalUiState extends UIState {
  @observable isLoaded = false;
  
  constructor(props = {}) {
    super(props);
  }
  
  isNeedToLoadMore() {
    const container = document.getElementsByClassName('modal-body')[0];

    if (!container) return false;

    const fullHeight = container.scrollHeight;
    const currentScrollHeight = container.scrollTop + container.clientHeight;
    const currentPercent = currentScrollHeight / fullHeight * 100;
    return 100 - currentPercent < LOAD_BUFFER_FROM_BOTTOM;
  }
  
  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }
}

export default InvitePeopleModalUiState;
