import { action, observable, computed, toJS } from 'mobx';
import { Auth } from '../../../../../auth';
import { get, uniqBy, debounce } from 'lodash';
import Promise from 'bluebird';

import UserModel from '../../../../../models/users';
import InviteModel from '../../../../../models/invites';


import DataState from '../../../../../utils/state/data';

import { PEOPLE_FOR_INVITE_LIMIT, FETCH_INVITED_RESOLVERS, SEARCH_DELAY } from '../constants';

const userModel = new UserModel();
const inviteModel = new InviteModel();



class InvitePeopleModalDataState extends DataState {
  followersPage = 1;

  @observable followers = {};
  @observable followings = {};

  @observable item;
  @observable type;

  @observable searchValue = '';

  @computed
  get peopleForInvite() {
    const followers = get(this.followers, 'data', []);
    const followings = get(this.followings, 'data', []);
    const people = followers.concat(followings).map(person => person.data);
    return uniqBy(people, person => person.id);
  }
  
  constructor(props = {}) {
    super(props);
    this.init(props);
    this.doSearch = ::this.doSearchHandler;
  }

  init(props) {
    this.setItem(props.item);
    this.setType(props.type);
  }

  doSearchHandler() {
    this.fetchPeopleForInvite();
  }

  fetchPeopleForInvite() {
    const userId = Auth.getActiveUser().id;
    const config = this.getConfig();

    return Promise.all([
      userModel.fetchFollowers(userId, PEOPLE_FOR_INVITE_LIMIT, this.followersPage, config),
      userModel.fetchFollowings(userId, PEOPLE_FOR_INVITE_LIMIT, this.followersPage, config),
    ])
    .spread((followers, followings) => this.handleFetch(followers, followings))
    .then(() => this.fetchAlreadyInvited())
    .then(res => this.setAlreadyInvitedPeople(get(res, 'data', [])))
    .catch(err => { console.error(err); return Promise.reject(); });
  }

  handleFetch(followers, followings) {
    this.setFollowers(followers);
    this.setFollowings(followings);
    return Promise.resolve();
  }

  fetchAlreadyInvited() {
    const id = get(this.item, 'id');
    return id && FETCH_INVITED_RESOLVERS[this.type] ? FETCH_INVITED_RESOLVERS[this.type](id) : Promise.resolve();
  }

  fetchMorePeopleForInvite() {
    const userId = Auth.getActiveUser().id;
    const followersPage = get(this.followers, 'nextPage', get(this.followers, 'maxPage'));
    const followingsPage = get(this.followings, 'nextPage', get(this.followings, 'maxPage'));
    const config = this.getConfig();

    return Promise.all([
      userModel.fetchFollowers(userId, PEOPLE_FOR_INVITE_LIMIT, followersPage + 1, config),
      userModel.fetchFollowings(userId, PEOPLE_FOR_INVITE_LIMIT, followingsPage + 1, config),
    ])
    .spread((followings, followers) => this.handleFetchMore(followers, followings))
    .catch(err => { console.error(err); return Promise.reject(); });
  }

  getConfig() {
    return {
      params: {
        query: {
          $or: [
            {
              name: {
                $ilike: `${this.searchValue}%`,
              },
            },
            {
              username: {
                $ilike: `${this.searchValue}%`,
              },
            },
          ],
        },
      },
    };
  }

  handleFetchMore(followers, followings) {
    this.appendFollowers(followers);
    this.appendFollowings(followings);
    return Promise.resolve();
  }

  getScheduleNotificationsRequests() {
    const items = this.getItemsForInvite();
    return items.map(item => inviteModel.sendScheduleInvite(this.item.id, item.id).then(() => items.forEach(elem => elem.isAlreadyInvited = true)));
  }

  getEventNotificationsRequests() {
    const items = this.getItemsForInvite();
    return items.map(item => inviteModel.sendEventInvite(this.item.id, item.id).then(() => items.forEach(elem => elem.isAlreadyInvited = true)));
  }

  getItemsForInvite() {
    return this.peopleForInvite.filter(item => item.isChecked && !item.isAlreadyInvited);
  }

  @action
  setItem(value) {
    this.item = value;
  }

  @action
  setType(value) {
    this.type = value;
  }

  @action
  setFollowers(value) {
    this.followers = value;
  }

  @action
  setFollowings(value) {
    this.followings = value;
  }

  @action
  appendFollowers(value) {
    this.followers = {
      data: this.followers.data.concat(get(value, 'data', [])),
      nextPage: value.nextPage,
      maxPage: value.maxPage,
    };
  }

  @action
  appendFollowings(value) {
    this.followings = {
      data: this.followings.data.concat(get(value, 'data', [])),
      nextPage: value.nextPage,
      maxPage: value.maxPage,
    };
  }

  @action
  setAlreadyInvitedPeople(people) {
    people.forEach(invitedPerson => {
      const invitedFollower = this.followers.data.find(person => person.data.id === invitedPerson.id);
      if (invitedFollower) return invitedFollower.data.isAlreadyInvited = true;

      const invitedFollowing = this.followings.data.find(person => person.data.id === invitedPerson.id);
      if (invitedFollowing) return invitedFollowing.data.isAlreadyInvited = true;
    });
  }

  @action
  handleItemClick(id) {
    const follower = get(this.followers, 'data', []).find(item => item.data.id === id);
    const following = get(this.followings, 'data', []).find(item => item.data.id === id);

    if (follower) {
      follower.data.isChecked = !follower.data.isChecked;
      return this.followers = toJS(this.followers);
    }
    if (!following) return;

    following.data.isChecked = !following.data.isChecked;
    this.followings = toJS(this.followings);
  }

  @action
  setSearchValue(value) {
    this.searchValue = value;

    if (!value || value.length === 0) return this.doSearch();

    const searchInvoker = debounce(this.doSearch, SEARCH_DELAY, {
      leading: false,
      trailing: true
    });

    searchInvoker();
  }
}

export default InvitePeopleModalDataState;
