import Emoji from 'node-emoji';

import UsersModel from '../../../../models/users';

import { TYPES as CREATIONS_TYPES } from '../../../../views/creations/constants';

export const TYPES = CREATIONS_TYPES;

export const PEOPLE_FOR_INVITE_LIMIT = 500;
export const LOAD_BUFFER_FROM_BOTTOM = 20;
export const SEARCH_DELAY = 250;

const userModel = new UsersModel();

export const FETCH_INVITED_RESOLVERS = {
  [TYPES.EVENT]: id => userModel.fetchInvitedPeople(TYPES.EVENT, id),
  [TYPES.SCHEDULE]: id => userModel.fetchInvitedPeople(TYPES.SCHEDULE, id),
};

export const TITLE = 'Invite people';
export const EMPTY_TEXT = Emoji.emojify(`If you haven't connected with anyone yet, send some friends the DockIt link! https://dockitcalendar.com :thumbsup:`);
