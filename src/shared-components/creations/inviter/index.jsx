import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import InviteModal from './invite-modal/index.jsx';



@observer
class EventCreateInviter extends Component {
  @observable isModalOpen = false;
  
  constructor(props) {
    super(props);
    this.onModalOpen = ::this.onModalOpenHandler;
    this.onModalClose = ::this.onModalCloseHandler;
    this.onInvitedPeopleChange = ::this.onInvitedPeopleChangeHandler;
  }
  
  onModalOpenHandler() {
    const { onOpen } = this.props.handlers;
    this.setModalVisibility(true);
    if (onOpen) onOpen();
  }
  
  onModalCloseHandler(value) {
    const { onInvitedPeopleChange, onClose } = this.props.handlers;
    this.setModalVisibility(false);
    if (onInvitedPeopleChange) onInvitedPeopleChange(value);
    if (onClose) onClose();
  }
  
  onInvitedPeopleChangeHandler(value) {
    const { onInvitedPeopleChange } = this.props.handlers;
    if (onInvitedPeopleChange) onInvitedPeopleChange(value);
  }
  
  @action
  setModalVisibility(value) {
    this.isModalOpen = value;
  }
  
  render() {
    const { item, type } = this.props;
    const isModalOpen = toJS(this.isModalOpen);
    
    return (
      <FormGroup controlId='peopleForInvite' className='invite-people-row' onClick={this.onModalOpen}>
        <span className='label-text'>
          Invite
        </span>
        <span className='chevron-wrapper'>
          <i className='fa fa-chevron-right' />
        </span>
        <InviteModal open={isModalOpen} item={item} type={type} onClose={this.onModalClose} />
      </FormGroup>
    );
  }
}

EventCreateInviter.propTypes = {
  item: PropTypes.object,
  type: PropTypes.string,
  handlers: PropTypes.shape({
    onInvitedPeopleChange: PropTypes.func.isRequired,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
  }).isRequired,
};

export default EventCreateInviter;
