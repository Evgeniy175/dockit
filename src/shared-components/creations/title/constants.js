import { TYPES } from '../../../views/creations/constants';

export const TITLES = {
  [TYPES.EVENT]: 'event',
  [TYPES.SCHEDULE]: 'schedule',
};
