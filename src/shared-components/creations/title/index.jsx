import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';

import { TITLES } from './constants';
import { FEED_PATH } from '../../../constants';

import Menu from '../menu/index.jsx';

import LockClosed from '../../../assets/images/icons/lock-closed.svg';

import { isPrivate } from '../../../utils/creations';



@observer
class EventScheduleInfoTitle extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDelete = ::this.onDeleteHandler;
  }
  
  onDeleteHandler() {
    browserHistory.push(FEED_PATH);
  }
  
  render() {
    const { item, type } = this.props;
    const { title, permission } = item;
    if (isPrivate(permission)) return this.renderPrivate();
    
    return (
      <div className='event-title'>
        <div className='text'>{ title }</div>
        <Menu type={type} item={item} onDelete={this.onDelete} />
      </div>
    );
  }

  renderPrivate() {
    const { item, type } = this.props;
    const { title } = item;

    return (
      <div className='event-title'>
        <div>
          <div className='title-private-event-text'>This is a private {TITLES[type]}</div>
        </div>
        <div className='text'>
          <Menu type={type} item={item} onDelete={this.onDelete} />
          { title }
          <img className='private-icon' src={LockClosed} />
        </div>
      </div>
    );
  }
}

EventScheduleInfoTitle.propTypes = {
  type: PropTypes.string.isRequired,
  item: PropTypes.object.isRequired,
};

export default EventScheduleInfoTitle;
