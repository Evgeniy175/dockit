import { TYPES as CALENDAR_TYPES } from '../../calendar-dropdown/constants.js';

export const REPEAT_TYPES = [
  {
    title: 'Never'
  }, {
    title: 'Every Day',
    scheduleConfigRepeat: {
      'type': 'd',
      'frequency': 1
    }
  }, {
    title: 'Every Week',
    commonData: {
      title: 'Days',
      data: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ]},
    scheduleConfigRepeat: {
      'type': 'w',
      'frequency': 1
    }
  }, {
    title: 'Every Other Week',
    commonData: {
      title: 'Days',
      data: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ]},
    scheduleConfigRepeat: {
      'type': 'w',
      'frequency': 2
    }
  }, {
    title: 'Every Month',
    scheduleConfigRepeat: {
      'type': 'm',
      'frequency': 1
    },
    calendarType: CALENDAR_TYPES.DAYS_SELECT
  }, {
    title: 'Every Year',
    scheduleConfigRepeat: {
      'type': 'y',
      'frequency': 1
    }
  }
];
