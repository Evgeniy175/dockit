import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Select from '../../../select/index.jsx';

@observer
class RepeatTypeSelector extends Component {
  render() {
    const {isBlocked, selectedRepeatType, onRepeatTypeChange} = this.props;
    let {repeatTypes} = this.props;
    repeatTypes = repeatTypes && repeatTypes.map(elem => { return { label: elem.title, value: elem.title }; });
    const className = `repeat${isBlocked ? ' blocked-repeat' : ''}`;
    return (
      <div className={className}>
        <div className='title'>
          Repeat
        </div>
        {
          !isBlocked &&
          <Select value={selectedRepeatType}
                  values={repeatTypes}
                  placeholder='Repeat type'
                  searchable={false}
                  clearable={false}
                  changeHandler={onRepeatTypeChange} />
        }
      </div>
    );
  }
}

RepeatTypeSelector.propTypes = {
  isBlocked: PropTypes.bool,
  repeatTypes: PropTypes.array,
  selectedRepeatType: PropTypes.object,
  onRepeatTypeChange: PropTypes.func
};


export default RepeatTypeSelector;
