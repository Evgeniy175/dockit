import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import DateTimeSelector from '../../../date-time-selector/index.jsx';
import Toggle from '../../../toggle/index.jsx';



@observer
class RepeatEndsSection extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onNeverEndsToggle = ::this.onNeverEndsToggleHandler;
    this.onRepeatEndDateChange = ::this.onRepeatEndDateChangeHandler;
    this.onRepeatEndTimeChange = ::this.onRepeatEndTimeChangeHandler;
  }
  
  onNeverEndsToggleHandler(repeatType) {
    this.props.onNeverEndsToggle(repeatType);
  }
  
  onRepeatEndDateChangeHandler(value) {
    this.props.onRepeatEndDateChange(value);
  }
  
  onRepeatEndTimeChangeHandler(value) {
    this.props.onRepeatEndTimeChange(value);
  }
  
  render() {
    const isHidden = this.props.isHidden;
    const isNeverEnds = this.props.isNeverEnds;
    const endDate = this.props.endDate;
    const endTime = this.props.endTime;
    const errors = this.props.errors;
    const className = `ends-section ${isHidden ? 'show' : 'hide'}`;
    
    return (
      <span className={className}>
        <Toggle label='Never ends' value={isNeverEnds} onClick={this.onNeverEndsToggle} isBoldLabel={false} />
        <DateTimeSelector className={isNeverEnds ? 'hide' : 'show'}
                          dateLabel='End'
                          date={endDate}
                          dateChangeHandler={this.onRepeatEndDateChange}
                          timeLabel='Time'
                          time={endTime}
                          errors={errors}
                          timeChangeHandler={this.onRepeatEndTimeChange} />
      </span>
    );
  }
}

RepeatEndsSection.propTypes = {
  isHidden: PropTypes.bool,
  isNeverEnds: PropTypes.bool,
  endDate: PropTypes.string,
  endTime: PropTypes.string,
  errors: PropTypes.object,
  onNeverEndsToggle: PropTypes.func,
  onRepeatEndDateChange: PropTypes.func,
  onRepeatEndTimeChange: PropTypes.func
};

RepeatEndsSection.defaultProps = {
  isHidden: false,
  isNeverEnds: false,
  endDate: '00:00',
  endTime: '01:00'
};


export default RepeatEndsSection;
