import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Header from './header/index.jsx';
import DetailsData from './details-data/index.jsx';


@observer
class RepeaterDetails extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDayCheckboxClick = ::this.onDayCheckboxClickHandler;
  }
  
  onDayCheckboxClickHandler(idx) {
    this.props.onDayCheckboxClick(idx);
  }
  
  render() {
    const details = this.props.details;
    const weekdays = this.props.weekdays;
    const errors = this.props.errors;
    
    if (!details) return null;
  
    return (
      <div>
        <Header title={details.title} />
        <DetailsData details={details} weekdays={weekdays} errors={errors} onDayCheckboxClick={this.onDayCheckboxClick} />
      </div>
    );
  }
}

RepeaterDetails.propTypes = {
  details: PropTypes.object,
  weekdays: PropTypes.array,
  errors: PropTypes.object,
  onDayCheckboxClick: PropTypes.func
};


export default RepeaterDetails;
