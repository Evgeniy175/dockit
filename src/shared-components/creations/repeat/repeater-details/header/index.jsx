import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

@observer
class RepeaterDetailsHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const title = this.props.title;
    
    return (
      <p className='common-data-title'>{title}</p>
    );
  }
}

RepeaterDetailsHeader.propTypes = {
  title: PropTypes.string.isRequired
};


export default RepeaterDetailsHeader;
