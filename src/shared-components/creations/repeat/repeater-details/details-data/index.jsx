import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Checkbox from '../../../../checkbox/index';



@observer
class RepeaterDetailsData extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDayCheckboxClick = ::this.onDayCheckboxClickHandler;
  }
  
  onDayCheckboxClickHandler(idx) {
    this.props.onDayCheckboxClick(idx);
  }
  
  render() {
    const details = this.props.details;
    const data = details.data;
    const weekdays = this.props.weekdays;
    const errors = this.props.errors;
    const className = get(errors, 'day') ? 'day-errors' : '';
    
    return (
      <div className={className}>
        {
          data.map((day, idx) => (
            <Checkbox key={`${details.title}_${day}`}
                      label={day}
                      isChecked={weekdays.includes(idx)}
                      handleCheckboxChange={() => this.onDayCheckboxClick(idx)} />
          ))
        }
      </div>
    );
  }
}

RepeaterDetailsData.propTypes = {
  details: PropTypes.object.isRequired,
  weekdays: PropTypes.array.isRequired,
  errors: PropTypes.object,
  onDayCheckboxClick: PropTypes.func.isRequired
};


export default RepeaterDetailsData;
