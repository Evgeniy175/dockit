import { action, observable, computed, extendObservable, toJS } from 'mobx';
import moment from 'moment';

import { REPEAT_TYPES } from '../constants';
import { INPUT_FORMATS, EVENTS_CONSTANTS } from '../../../../constants';

import { get } from 'lodash';

import DataState from '../../../../utils/state/data';

class ViewRepeatDataState extends DataState {
  @observable isBlocked = false;
  @observable selectedRepeatType = {};
  @observable isNeverEnds = true;
  @observable selectedDays = [];
  
  @observable scheduleConfig = {};
  scheduleConfigHandler = null;

  scheduleEnd;
  
  @computed
  get repeatEndDate() {
    const endDateTime = moment(this.scheduleConfig.endDate);
    return this.isNeverEnds ? null : endDateTime.format(INPUT_FORMATS.DATE);
  }
  set repeatEndDate(value) {
    value = moment(value, INPUT_FORMATS.DATE);
    this.scheduleConfig.endDate = moment(this.scheduleConfig.endDate).set({ year: value.get('year'), month: value.get('month'), date: value.get('date') });
  }
  
  @computed
  get repeatEndTime() {
    const endDateTime = moment(this.scheduleConfig.endDate);
    return this.isNeverEnds ? null : endDateTime.format(INPUT_FORMATS.TIME);
    
  }
  set repeatEndTime(value) {
    value = moment(value, INPUT_FORMATS.TIME);
    this.scheduleConfig.endDate = moment(this.scheduleConfig.endDate).set({ hour: value.get('hour'), minute: value.get('minute'), second: value.get('second') });
  }
  
  @computed
  get selectedRepeatTypeFull() {
    return REPEAT_TYPES.find(elem => elem.title == this.selectedRepeatType.label);
  }
  
  @computed
  get isSchedule() {
    const selectedRepeatType = this.selectedRepeatTypeFull;
    return !!(selectedRepeatType && selectedRepeatType.scheduleConfigRepeat && selectedRepeatType.scheduleConfigRepeat.type);
  }
  
  constructor(props = {}) {
    super(props);
    const { isBlocked, scheduleConfig, scheduleEnd, scheduleConfigHandler } = props;
    this.setIsBlocked(isBlocked);
    if (isBlocked) return;
    this.setScheduleConfig(scheduleConfig);
    this.scheduleEnd = scheduleEnd;
    this.scheduleConfigHandler = scheduleConfigHandler;
    this.setIsNeverEnds(!get(this.scheduleConfig, 'endDate'));
  }
  
  @action
  setRepeatEndDate(value) {
    this.repeatEndDate = value;
  }
  
  @action
  setRepeatEndTime(value) {
    this.repeatEndTime = value;
  }
  
  @action
  setIsBlocked(value) {
    this.isBlocked = value;
  }

  @action
  setScheduleConfig(value) {
    this.scheduleConfig = JSON.parse(JSON.stringify(value));
  }
  
  @action
  setSelectedRepeatType(value) {
    this.selectedRepeatType = value;
    this.initScheduleConfig();
    if (!this.selectedRepeatTypeFull.scheduleConfigRepeat) delete this.scheduleConfig.details;
  }
  
  @action
  toggleIsNeverEnds() {
    this.isNeverEnds = !this.isNeverEnds;
    this.handleIsNeverEndsChanged();
    this.scheduleConfig.endDate = this.isNeverEnds ? null : moment(this.scheduleEnd).add(1, 'hours').format();
  }
  
  @action
  setIsNeverEnds(value) {
    this.isNeverEnds = value;
    this.handleIsNeverEndsChanged();
  }
  
  handleIsNeverEndsChanged() {
    if (!this.isNeverEnds && this.scheduleConfig.endDate == null)
      this.scheduleConfig.endDate = moment().add(1, 'hour').startOf('hour').format();
  }
  
  @action
  handleSelectedDay(idx) {
    let weekdays = this.scheduleConfig.weekdays;
    const elemIdx = weekdays.indexOf(idx);
    
    if (elemIdx === -1) weekdays.push(idx);
    else weekdays.splice(elemIdx, 1);
  }
  
  @action
  updateScheduleConfig() {
    let newScheduleConfig = JSON.parse(JSON.stringify(this.scheduleConfig));

    if (this.isNeverEnds) delete newScheduleConfig.endDate;
    else newScheduleConfig.endDate = moment(newScheduleConfig.endDate).format(EVENTS_CONSTANTS.DATE_FORMAT);
    
    newScheduleConfig.daysOfMonth = toJS(this.selectedDays);
    this.scheduleConfigHandler(newScheduleConfig);
    this.scheduleConfig = newScheduleConfig;
  }
  
  @action
  initScheduleConfig() {
    const selectedRepeatType = this.selectedRepeatTypeFull;
    
    this.scheduleConfig = observable({
      type: get(selectedRepeatType, 'scheduleConfigRepeat.type', null),
      frequency: get(selectedRepeatType, 'scheduleConfigRepeat.frequency', null),
      weekdays: get(this.scheduleConfig, 'weekdays', []),
      endDate: get(this.scheduleConfig, 'endDate', moment().startOf('hour')),
      daysOfMonth: []
    });
    
    if (!selectedRepeatType.commonData) delete this.scheduleConfig.weekdays;
  }
  
  @action
  setSelectedDays(value) {
    this.selectedDays = value;
  }
  
  @action
  clear() {
    this.selectedRepeatType = {};
    this.isNeverEnds = true;
    this.scheduleConfig = {};
    this.scheduleConfigHandler = null;
  }
}

export default ViewRepeatDataState;
