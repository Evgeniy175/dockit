import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { isEmpty, get } from 'lodash';
import PropTypes from 'prop-types';

import Calendar from '../../calendar-dropdown/index.jsx';
import RepeatTypeSelector from './selector/index.jsx';
import EndsSection from './ends-section/index.jsx';
import RepeaterDetails from './repeater-details/index.jsx';

import RepeatDataState from './state/data';

import { REPEAT_TYPES } from './constants';
import { INPUT_FORMATS } from '../../../constants';



@observer
class Repeat extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new RepeatDataState(this.props),
    });

    const { scheduleConfig } = this.props;
    if (!scheduleConfig || !isEmpty(scheduleConfig)) this.recognizeSelectedRepeatType(scheduleConfig);
  }
  
  recognizeSelectedRepeatType(scheduleConfig) {
    if (this.props.isBlocked) return;
    const selectedRepeatType = REPEAT_TYPES.find(rt => {
      const scr = rt.scheduleConfigRepeat;
      return scr && scr.type == scheduleConfig.type && scr.frequency == scheduleConfig.frequency
    });
    
    this.initSelectedRepeatType(selectedRepeatType);
  }
  
  componentWillMount() {
    if (isEmpty(this.data.selectedRepeatType)) this.initSelectedRepeatType(REPEAT_TYPES[0]);
    
    this.onRepeatTypeChange = ::this.onRepeatTypeChangeHandler;
    this.onNeverEndsToggle = ::this.onNeverEndsToggleHandler;
    this.onRepeatEndDateChange = ::this.onRepeatEndDateChangeHandler;
    this.onRepeatEndTimeChange = ::this.onRepeatEndTimeChangeHandler;
    this.onDayCheckboxClick = ::this.onDayCheckboxClickHandler;
    this.onSelectedDaysChange = ::this.onSelectedDaysChangeHandler;
  }
  
  componentWillUnmount() {
    this.data.clear();
  }
  
  initSelectedRepeatType(repeatType) {
    const selectedRepeatType = {
      label: repeatType.title,
      value: repeatType.title
    };
  
    this.data.setSelectedRepeatType(selectedRepeatType);
  }
  
  onRepeatTypeChangeHandler(repeatType) {
    this.data.setSelectedRepeatType(repeatType);
    this.data.updateScheduleConfig();
  }
  
  onNeverEndsToggleHandler() {
    this.data.toggleIsNeverEnds();
    this.data.updateScheduleConfig();
  }
  
  onRepeatEndDateChangeHandler(value) {
    this.data.setRepeatEndDate(value);
    this.data.updateScheduleConfig();
  }
  
  onRepeatEndTimeChangeHandler(value) {
    this.data.setRepeatEndTime(value);
    this.data.updateScheduleConfig();
  }
  
  onDayCheckboxClickHandler(idx) {
    this.data.handleSelectedDay(idx);
    this.data.updateScheduleConfig();
  }
  
  onSelectedDaysChangeHandler(data) {
    this.data.setSelectedDays(data.selectedDays);
    this.data.updateScheduleConfig();
  }
  
  render() {
    const {isBlocked} = this.data;

    if (isBlocked) return this.renderBlocked();

    const repeatType = this.data.selectedRepeatTypeFull;
    const repeatDetails = repeatType.commonData;
    
    const weekdays = toJS(get(this.data, 'scheduleConfig.weekdays', []));
    
    const selectedRepeatType = this.data.selectedRepeatType;
    
    const isEndsSectionHidden = this.data.isSchedule;
    const isNeverEnds = this.data.isNeverEnds;
    
    const endDate = this.data.repeatEndDate;
    const endTime = this.data.repeatEndTime;
    
    const calendarType = repeatType.calendarType;
    const selectedDays = toJS(this.data.selectedDays);
    
    const errors = this.props.errors;
    
    return (
      <div className='view-repeat'>
        <RepeatTypeSelector repeatTypes={REPEAT_TYPES} selectedRepeatType={selectedRepeatType} onRepeatTypeChange={this.onRepeatTypeChange} />
        <EndsSection isHidden={isEndsSectionHidden}
                     isNeverEnds={isNeverEnds}
                     endDate={endDate}
                     endTime={endTime}
                     errors={errors}
                     onNeverEndsToggle={this.onNeverEndsToggle}
                     onRepeatEndDateChange={this.onRepeatEndDateChange}
                     onRepeatEndTimeChange={this.onRepeatEndTimeChange} />
        {
          calendarType &&
          <div className='calendar-wrapper'>
            <Calendar type={calendarType} alwaysVisible={true} selectedDays={selectedDays} pattern={INPUT_FORMATS.DATE} onChange={this.onSelectedDaysChange} />
          </div>
        }
        <RepeaterDetails details={repeatDetails} weekdays={weekdays} errors={errors} onDayCheckboxClick={this.onDayCheckboxClick} />
      </div>
    );
  }

  renderBlocked() {
    return (
      <div className='view-repeat'>
        <RepeatTypeSelector isBlocked={true} />
      </div>
    );
  }
}

Repeat.propTypes = {
  isBlocked: PropTypes.bool,
  scheduleConfig: PropTypes.object,
  scheduleEnd: PropTypes.object,
  errors: PropTypes.object,
  scheduleConfigHandler: PropTypes.func,
};

export default Repeat;
