import React, { Component } from 'react';
import { Link } from 'react-router';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Image from '../../image/index.jsx'
import Tooltip from '../../tooltip-top/index.jsx';

import UserModel from '../../../models/users/index';

import { WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../image/constants';



@observer
class EventMemberImage extends Component {
  values;
  handlers;

  constructor(props) {
    super(props);
    const { user } = this.props;
    this.values = {
      src: UserModel.formatImageUrl(user.data ? user.data.profilePicture : user.profilePicture),
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s1x1,
      prepareWrapper: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.image !== this.props.image) this.setSrc(this.props.user);
  }

  @action
  setSrc(user) {
    this.values.src = UserModel.formatImageUrl(user.data ? user.data.profilePicture : user.profilePicture);
  }

  render() {
    const { user } = this.props;
    const userId = user.data ? user.data.id : user.id;
    const username = user.data ? user.data.name : user.name;
    return (
      <div className='event-member-image-wrapper'>
        <Link to={`/users/${userId}`} data-tip data-for={userId}>
          <Image values={this.values} />
        </Link>
        <Tooltip id={userId} content={username} />
      </div>
    );
  }
}

EventMemberImage.propTypes = {
  user: PropTypes.object.isRequired,
};

export default EventMemberImage;
