import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { get } from 'lodash';



@observer
class EventCreator extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onChange = ::this.onChangeHandler;
  }
  
  onChangeHandler() {
    this.props.onChange();
  }
  
  render() {
    const { item } = this.props;
    const userId = get(item, 'user.id');
    const userName = get(item, 'user.name', 'unknown');

    return (
      <div className='creation-creator'>
        Created by <Link to={`/users/${userId}`}>{ userName }</Link>
      </div>
    );
  }
}

EventCreator.propTypes = {
  item: PropTypes.object.isRequired,
};

export default EventCreator;
