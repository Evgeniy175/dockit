import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Svg from 'react-svg';

import PropTypes from 'prop-types';
import { CSS_MAPPING } from '../../../shared-components/button/constants';
import Button from './../../../shared-components/button/index.jsx';
import { ADD_IMG_TEXT } from "./constants";
import AddImageIcon from '../../../assets/images/icons/add-more-images.svg';

@observer
class AttachImage extends Component {
  id;

  constructor(props) {
    super(props);

    this.id = `attach-first-image_${props.values.id}`;

    this.onClick = ::this.onClickHandler;
  }

  componentWillUnmount(){
    this.onClick = null;
  }

  onClickHandler(e){
    e.preventDefault();
    const uploadInput = document.getElementById(this.id);
    uploadInput.value = null;
    uploadInput.click();
  }

  render(){
    const { onAddImage } = this.props.handlers;
    const { isImagesExists } = this.props.values;

    if (!isImagesExists){
      return (
        <div className="attach-image-drill-down">
          <input type="file" className="attach-first-image" id={this.id} onChange={onAddImage}/>
          <Button text={ADD_IMG_TEXT} className={CSS_MAPPING.BLUE_FILLED} onClick={this.onClick}/>
        </div>
      );
    }

    return(
      <div className="attach-more-images-wrapper">
        <input onChange={onAddImage} id={this.id} className="attach-more-images" type='file'/>
        <span className='attach-more-images-btn' onClick={this.onClick}>
          <Svg path={AddImageIcon}/>
        </span>
      </div>
    );
  }
}

AttachImage.propTypes = {
  id: PropTypes.string,
  onAddImage: PropTypes.func
};

export default AttachImage;