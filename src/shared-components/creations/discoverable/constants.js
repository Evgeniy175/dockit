export const DISCOVER_MODAL_TEXT = `Public events are able to be searched and viewed by anyone even if they aren't following you. Are you sure you'd like to make this public?`;
