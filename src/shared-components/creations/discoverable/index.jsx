import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Checkbox from '../../toggle/index.jsx';
import Modal from '../../question-modal/index.jsx';

import { DISCOVER_MODAL_TEXT } from './constants';



@observer
class EventDiscoverable extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.values.selectedPermission.canToggleDiscoverable) return null;
    const { displayInDiscover, isDiscoverModalShown, modalButtons } = this.props.values;
    const { onChange, onModalClose } = this.props.handlers;
    return (
      <FormGroup controlId='discoverableCheckbox'>
        <Checkbox label='Display in discover' value={displayInDiscover} isBoldLabel={false} onClick={onChange} />
        <Modal className='display-in-discover-modal' open={isDiscoverModalShown} text={DISCOVER_MODAL_TEXT} buttons={modalButtons} onClose={onModalClose} />
      </FormGroup>
    );
  }
}

EventDiscoverable.propTypes = {
  values: PropTypes.shape({
    displayInDiscover: PropTypes.bool.isRequired,
    isDiscoverModalShown: PropTypes.bool.isRequired,
    selectedPermission: PropTypes.object.isRequired,
    modalButtons: PropTypes.array.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    onModalClose: PropTypes.func.isRequired,
  }).isRequired,
};

export default EventDiscoverable;
