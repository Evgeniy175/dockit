import EventModel from '../../../models/events';
import ScheduleModel from '../../../models/schedules';

import { TYPES } from '../../../views/creations/constants';
import { CSS_MAPPING } from '../../button/constants';

const eventModel = new EventModel();
const scheduleModel = new ScheduleModel();



export const INTERVAL_TIMEOUT_MS = 5;
export const INTERVAL_MAX_COUNT = 10;

export const UPDATE_LINKS = {
  [TYPES.EVENT]: id => `/${TYPES.EVENT}/${id}/edit`,
  [TYPES.SCHEDULE]: id => `/${TYPES.SCHEDULE}/${id}/edit`,
};

export const REPORT_LINKS = {
  [TYPES.EVENT]: id => `/reports/${TYPES.EVENT}/${id}`,
  [TYPES.SCHEDULE]: id => `/reports/${TYPES.SCHEDULE}/${id}`,
};

export const REQUEST_RESOLVERS = {
  [TYPES.EVENT]: eventModel,
  [TYPES.SCHEDULE]: scheduleModel,
};

export const PEOPLE_FOR_INVITE_LIMIT = 25;
export const LOAD_BUFFER_FROM_BOTTOM = 20;

export const SAVED_POPUP_DATA = {
  KEY: 'is saved popup disabled',
  TEXT: 'You just saved this to your upcoming!',
  BUTTONS: handlers => [
    {
      text: 'Okay',
      className: CSS_MAPPING.BLUE_FILLED,
      onClick: handlers.success,
    },
    {
      text: `Don't show again`,
      className: CSS_MAPPING.BLUE_FILLED,
      onClick: handlers.cancel,
    },
  ]
};
