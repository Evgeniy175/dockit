import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';
import Svg from 'react-svg';
import { get } from 'lodash';

import InviteModal from '../inviter/invite-modal/index.jsx';
import QuestionModal from '../../question-modal/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import DropdownPositionHelper from '../../../utils/dropdown-menu/position';

import { Auth } from '../../../auth';

import { REPORT_LINKS, UPDATE_LINKS, REQUEST_RESOLVERS, SAVED_POPUP_DATA, INTERVAL_MAX_COUNT, INTERVAL_TIMEOUT_MS } from './constants';
import { TYPES } from '../../../views/creations/constants';
import { IS_MOBILE_OR_TABLET } from '../../../constants';

import { share, isSupportedShareApi } from '../../../utils/share-api';
import { getLocation } from '../../../utils/dom/window';
import { getRandomInt } from '../../../utils/random';

import DotsImage from '../../../assets/images/three-dots.svg';



@observer
class EventDescriptionMenu extends Component {
  intervalId;
  intervalCounter = 0;
  id = `event-menu${getRandomInt()}`;
  dropdownPositionHelper = new DropdownPositionHelper({
    buttonSelector: `#${this.id}`,
  });

  constructor(props) {
    super(props);
    this.onUpdate = ::this.onUpdateHandler;
    this.onDelete = ::this.onDeleteHandler;
    this.onReport = ::this.onReportHandler;
    this.onInvite = ::this.onInviteHandler;
    this.onSave = ::this.onSaveHandler;
    this.onShare = ::this.onShareHandler;
    this.onViewFullSchedule = ::this.onViewFullScheduleHandler;
    this.onInvitedPeopleModalClose = ::this.onInvitedPeopleModalCloseHandler;

    this.savedPopupButtonHandlers = {
      success: ::this.onSavedPopupClose,
      cancel: ::this.onSavedPopupCancelButtonClick,
      close: ::this.onSavedPopupClose,
    };

    Object.assign(this, {
      data: new DataState(this.props),
      ui: new UiState(),
    });
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  onUpdateHandler() {
    const { item, type } = this.data;
    const link = UPDATE_LINKS[type](item.id);
    browserHistory.push(link);
  }

  onDeleteHandler() {
    const { item, type } = this.data;
    if (this.props.onDelete) this.props.onDelete(item.id);
    REQUEST_RESOLVERS[type].deleteOne(item.id);
  }

  onReportHandler() {
    const { item, type } = this.data;
    const link = REPORT_LINKS[type](item.id);
    browserHistory.push(link);
  }

  onInviteHandler() {
    this.data.setIsInviteModalOpen(true);
  }

  onSaveHandler() {
    this.data.save();
  }

  onViewFullScheduleHandler() {
    browserHistory.push(`/${TYPES.SCHEDULE}/${get(this.data, 'item.schedule_id')}`);
  }

  onInvitedPeopleModalCloseHandler() {
    this.data.setIsInviteModalOpen(false);
  }

  clearInterval() {
    clearInterval(this.intervalId);
    this.intervalCounter = 0;
  }

  onShareHandler() {
    const { item } = this.props;
    share({
      title: 'DockIt',
      text: item.title,
      url: getLocation().href,
    });
  }

  onSavedPopupCancelButtonClick() {
    this.data.disableSavedPopup();
    this.data.isSavedPopupOpen = false;
  }

  onSavedPopupClose() {
    this.data.isSavedPopupOpen = false;
  }

  render() {
    if (!Auth.isSigned()) return null;

    const items = this.getItems();
    const modals = this.getModals();

    return (
      <div className='menu'>
        { modals }
        <DropdownButton id={this.id} title={<Svg path={DotsImage} />} onToggle={this.dropdownPositionHelper.onToggle}>
          { items.map(item => item.isShown ? item.render() : null) }
        </DropdownButton>
      </div>
    );
  }

  getModals() {
    const { item, type } = this.data;
    const { isInviteModalOpen, isSavedPopupOpen } = this.data;
    const savedPopupButtons = SAVED_POPUP_DATA.BUTTONS(this.savedPopupButtonHandlers);
    return [
      <InviteModal key='invite-modal' item={item} type={type} open={isInviteModalOpen} onClose={this.onInvitedPeopleModalClose} />,
      <QuestionModal key='saved-modal' className='saved-modal' open={isSavedPopupOpen} text={SAVED_POPUP_DATA.TEXT} buttons={savedPopupButtons} onClose={this.savedPopupButtonHandlers.close} />,
    ];
  }

  getItems() {
    const { item, isSaved } = this.data;
    const currentUser = Auth.getActiveUser();
    const isCurrentUserOwner = currentUser.id === item.user_id;
    const isFromSchedule = !!item.schedule_id;

    return [
      {
        isShown: isFromSchedule,
        render: () => <MenuItem key='menuItem1' eventKey='100' onClick={this.onViewFullSchedule}>View Full Schedule</MenuItem>,
      },
      {
        isShown: isCurrentUserOwner,
        render: () => <MenuItem key='menuItem2' eventKey='101' onClick={this.onUpdate}>Edit</MenuItem>,
      },
      {
        isShown: !isCurrentUserOwner,
        render: () => <MenuItem key='menuItem3' eventKey='102' onClick={this.onReport}>Report</MenuItem>,
      },
      {
        isShown: true,
        render: () => <MenuItem key='menuItem4' eventKey='103' onClick={this.onSave}>{ isSaved ? 'Un-Save' : 'Save' }</MenuItem>,
      },
      {
        isShown: true,
        render: () => <MenuItem key='menuItem5' eventKey='104' onClick={this.onInvite}>Invite</MenuItem>,
      },
      {
        isShown: IS_MOBILE_OR_TABLET && isSupportedShareApi(),
        render: () => <MenuItem key='menuItem6' eventKey='105' onClick={this.onShare}>Share</MenuItem>,
      },
      {
        isShown: isCurrentUserOwner,
        render: () => <MenuItem key='menuItem7' eventKey='106' onClick={this.onDelete}>Delete</MenuItem>,
      },
    ];
  }
}

EventDescriptionMenu.propTypes = {
  type: PropTypes.string.isRequired,
  item: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default EventDescriptionMenu;
