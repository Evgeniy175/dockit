import { action, observable, computed, toJS } from 'mobx';
import { get, set } from 'lodash';

import SavesModel from '../../../../models/saves';

import DataState from '../../../../utils/state/data';

import { SAVED_POPUP_DATA } from '../constants';

const savesModel = new SavesModel();



class EventDescriptionMenuDataState extends DataState {
  @observable isInviteModalOpen = false;

  @observable item;
  @observable type;

  @observable isSavedPopupVisible = false;

  @computed
  get isSaved() {
    return get(this.item, 'decorated.saves.mine');
  }

  @computed
  get isSavedPopupOpen() {
    return this.isSavedPopupVisible && !this.isUserDisabledSavedPopup();
  }
  set isSavedPopupOpen(value) {
    this.isSavedPopupVisible = value;
  }

  isUserDisabledSavedPopup() {
    return !!localStorage.getItem(SAVED_POPUP_DATA.KEY);
  }

  disableSavedPopup() {
    return localStorage.setItem(SAVED_POPUP_DATA.KEY, true);
  }

  constructor(props = {}) {
    super(props);
    this.init(props);
    this.doSave = ::savesModel.save;
    this.deleteSave = ::savesModel.deleteSave;
  }

  init(props) {
    const { item, type } = props;
    this.setItem(item);
    this.setType(type);
  }

  save() {
    const resolver = this.isSaved ? this.deleteSave : this.doSave;
    return resolver({
      routing: {
        target: this.type,
        id: get(this.item, 'id'),
      },
    })
    .then(() => {
      this.setSaved(this.isSaved ? null : {});
      this.isSavedPopupOpen = this.isSaved;
      return Promise.resolve();
    });
  }

  getSaved() {
    return get(this.item, 'decorated.saves.mine');
  }

  @action
  setSaved(value) {
    set(this.item, 'decorated.saves.mine', value);
  }

  @action
  setIsInviteModalOpen(value) {
    this.isInviteModalOpen = value;
  }

  @action
  setItem(value) {
    this.item = value;
  }

  @action
  setType(value) {
    this.type = value;
  }
}

export default EventDescriptionMenuDataState;
