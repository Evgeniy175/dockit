import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import DateTimeSelector from '../../date-time-selector/index.jsx';



@observer
class EventCreateStartDateTime extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onStartDateChange = ::this.onStartDateChangeHandler;
    this.onStartTimeChange = ::this.onStartTimeChangeHandler;
  }
  
  onStartDateChangeHandler(value) {
    this.props.onStartDateChange(value);
  }
  
  onStartTimeChangeHandler(value) {
    this.props.onStartTimeChange(value || '00:00');
  }
  
  render() {
    const { startTime, startDate, errors, timeShown } = this.props;
    const isErrorExists = get(errors, 'date') || get(errors, 'time') || get(errors, 'both');
    const className = isErrorExists ? 'form-group-error' : '';
    
    return (
      <FormGroup controlId='startDateTime' className={className}>
        <DateTimeSelector dateLabel='Starts'
                          date={startDate}
                          dateChangeHandler={this.onStartDateChange}
                          timeLabel='Time'
                          time={startTime}
                          timeChangeHandler={this.onStartTimeChange}
                          timeShown={timeShown}
                          errors={errors} />
      </FormGroup>
    );
  }
}

EventCreateStartDateTime.propTypes = {
  startDate: PropTypes.string.isRequired,
  startTime: PropTypes.string.isRequired,
  errors: PropTypes.object,
  onStartDateChange: PropTypes.func.isRequired,
  onStartTimeChange: PropTypes.func.isRequired,
  timeShown: PropTypes.bool,
};

EventCreateStartDateTime.defaultProps = {
  timeShown: true
};

export default EventCreateStartDateTime;
