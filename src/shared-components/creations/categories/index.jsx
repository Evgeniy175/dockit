import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import CategoriesModal from '../../categories-modal/index.jsx';

import { DEFAULT_CATEGORIES_LIST } from './constants';



@observer
class EventCategories extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const handler = get(this.props, 'handlers.onOpen');
    
    if (handler) handler();
  }
  
  render() {
    const data = this.props.data;
    const handlers = this.props.handlers;
    const selectedCategories = this.getSelectedCategoriesAsText();
    
    return (
      <FormGroup controlId='categories' className='categories-wrapper' onClick={this.onClick}>
        <span className='categories-label'>Categories</span>
        <span className='categories-list'>{selectedCategories}</span>
        <CategoriesModal data={data} handlers={handlers} />
      </FormGroup>
    );
  }
  
  getSelectedCategoriesAsText() {
    const data = this.props.data;
    const selectedCategories = get(data, 'selectedCategories');
    
    if (!selectedCategories) return DEFAULT_CATEGORIES_LIST;
    
    return selectedCategories.map(category => category.category).join(', ');
  }
}

EventCategories.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.object
};

EventCategories.defaultProps = {
  
};

export default EventCategories;
