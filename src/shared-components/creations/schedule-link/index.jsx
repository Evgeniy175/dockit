import React, { Component } from 'react';
import { Link } from 'react-router';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

@observer
class ScheduleLink extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const schedule = this.props.schedule;
    const message = this.props.message;
    const scheduleUrl = `/schedules/${schedule.id}`;

    return (
      <span>
        <Link to={scheduleUrl} className="schedule-link no-wrapping-text">{schedule.title}</Link>
        { message }
      </span>
    );
  }
}

ScheduleLink.propTypes = {
  schedule: PropTypes.object.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
};

export default ScheduleLink;