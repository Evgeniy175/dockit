import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Radiobutton from '../../radiobutton/index.jsx';

@observer
class EventCreatePermissions extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onPermissionChange = ::this.onPermissionChangeHandler;
  }
  
  onPermissionChangeHandler(value) {
    this.props.onPermissionChange(value);
  }
  
  render() {
    const permissions = this.props.permissions;
    const selectedPermission = this.props.selectedPermission;
    const isBorderVisible = this.props.borderVisible;
    
    return (
      <FormGroup controlId='permissions' className={`${isBorderVisible ? '' : ' no-border'}`}>
        <Row className='permissions'>
        {
          permissions.map(permission =>
            <Col className='center' xs={12 / permissions.length} key={permission.title}>
              <Radiobutton groupName='permissions'
                           data={permission}
                           value={permission.title}
                           isChecked={selectedPermission.title && selectedPermission.title == permission.title}
                           changeHandler={this.onPermissionChange} />
            </Col>
          )
        }
        </Row>
      </FormGroup>
    );
  }
}

EventCreatePermissions.propTypes = {
  permissions: PropTypes.array.isRequired,
  selectedPermission: PropTypes.object.isRequired,
  onPermissionChange: PropTypes.func.isRequired,
  borderVisible: PropTypes.bool
};

EventCreatePermissions.defaultProps = {
  borderVisible: true
};

export default EventCreatePermissions;
