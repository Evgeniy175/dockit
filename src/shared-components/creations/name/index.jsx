import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Input from '../../validation-input/index.jsx';

@observer
class EventCreateName extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { name, error, onChange, onBlur } = this.props;
    const className = `${ error ? 'form-group-error' : '' }`;
    
    return (
      <FormGroup controlId='eventName' className={className}>
        <Input value={name} error={error} placeholder='Name' messageShown={false} onChange={onChange} onBlur={onBlur} />
      </FormGroup>
    );
  }
}

EventCreateName.propTypes = {
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
};

export default EventCreateName;
