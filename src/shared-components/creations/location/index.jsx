import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Select from '../../select/index.jsx';


@observer
class EventCreateLocation extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onLocationChange = ::this.onLocationChangeHandler;
    this.onLocationInputChange = ::this.onLocationInputChangeHandler;
  }
  
  onLocationChangeHandler(value) {
    this.props.onLocationChange(value);
  }
  
  onLocationInputChangeHandler(value) {
    this.props.onLocationInputChange(value);
  }
  
  render() {
    const locations = this.props.locations;
    const selectedLocation = this.props.selectedLocation;
    const error = this.props.error;
    const className = `${ error ? 'form-group-error' : '' }`;
    
    return (
      <FormGroup controlId='eventLocation' className={className}>
        <Select value={selectedLocation}
                values={locations}
                placeholder='Location'
                error={error}
                changeHandler={this.onLocationChange}
                inputChangeHandler={this.onLocationInputChange} />
      </FormGroup>
    );
  }
}

EventCreateLocation.propTypes = {
  locations: PropTypes.array.isRequired,
  selectedLocation: PropTypes.object,
  error: PropTypes.string,
  onLocationChange: PropTypes.func.isRequired,
  onLocationInputChange: PropTypes.func.isRequired
};

EventCreateLocation.defaultProps = {
  error: ''
};

export default EventCreateLocation;
