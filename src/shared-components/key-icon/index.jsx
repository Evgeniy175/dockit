import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';

import Key from '../../assets/images/icons/key-big.png';



@observer
class KeyIcon extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className='key-icon-wrapper'>
        <img src={Key} />
      </div>
    );
  }
}

export default KeyIcon;
