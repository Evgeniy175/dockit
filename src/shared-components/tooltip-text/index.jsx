import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class TooltipText extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const text = this.props.text;
    
    return (
      <Row className='tooltip-text'>
        <Col xs={12}>
          {text}
        </Col>
      </Row>
    );
  }
}

TooltipText.propTypes = {
  text: PropTypes.string.isRequired
};

export default TooltipText;
