import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Alert } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class ValidationSummary extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const errors = this.props.errors.map(e => <li key={e}>{e}</li>);
    
    return (
      <div className='validation-summary'>
        <ul>{ errors }</ul>
      </div>
    );
  }
}

ValidationSummary.propTypes = {
  errors: PropTypes.array.isRequired
};

export default ValidationSummary;
