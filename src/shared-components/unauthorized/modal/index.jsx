import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Modal from '../../question-modal/index.jsx';

import { CSS_MAPPING } from '../../button/constants';



@observer
class UnauthorizedModal extends Component {
  constructor(props) {
    super(props);
    this.buttons = [
      {
        text: 'Log In',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: ::this.onLogIn,
      },
      {
        text: 'Close',
        className: CSS_MAPPING.BLUE,
        onClick: this.props.onClose,
      },
    ];
  }

  onLogIn() {
    browserHistory.push('/login')
  }

  render() {
    const { isOpen, onClose } = this.props;
    return <Modal className='unauthorized-modal' open={isOpen} text='You need to Log In to do that!' buttons={this.buttons} onClose={onClose} />;
  }
}

UnauthorizedModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default UnauthorizedModal;
