export const TARGETS = {
  COMMENTS: 'comments',
};

export const TEXTS = {
  [TARGETS.COMMENTS]: 'comments',
};

export const DEFAULT_TEXT = 'this';
