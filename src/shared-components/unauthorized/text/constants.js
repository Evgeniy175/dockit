export const TARGETS = {
  COMMENTS: 'comments',
  FEED: 'feed',
};

export const TEXTS = {
  [TARGETS.COMMENTS]: 'to view more!',
  [TARGETS.FEED]: 'to see more!',
};

export const DEFAULT_TEXT = 'this';
