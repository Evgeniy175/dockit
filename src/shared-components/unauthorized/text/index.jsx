import React, { Component } from 'react';
import { Link } from 'react-router';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { TEXTS, DEFAULT_TEXT } from './constants';



@observer
class UnauthorizedText extends Component {
  render() {
    const { target } = this.props;
    return (
      <div id='unauthorized-text'>
        <Link to='/login'>Log In</Link> or <Link to='/signup'>Sign Up</Link> {TEXTS[target] ? TEXTS[target] : DEFAULT_TEXT}
      </div>
    );
  }
}

UnauthorizedText.propTypes = {
  target: PropTypes.string,
};

export default UnauthorizedText;
