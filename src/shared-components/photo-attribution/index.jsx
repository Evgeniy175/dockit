import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { htmlToElements } from '../../utils/html';



@observer
class PhotoAttribution extends Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {

  }

  render() {
    const attrData = this.getAttributeData();
    if (!attrData) return null;

    return <div className='photo-attribution'>{ attrData.text }</div>;
  }

  getAttributeData() {
    const { idx, data } = this.props.values;
    const attr = get(data, `[${idx}]`);
    if (!attr) return null;
    const elem = htmlToElements(attr);
    return {
      text: get(elem, '[0].innerText'),
      href: get(elem, '[0].href'),
    };
  }
}

PhotoAttribution.propTypes = {
  values: PropTypes.shape({
    idx: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
  }).isRequired,
};

export default PhotoAttribution;
