import React, { Component } from 'react';
import { Link } from 'react-router';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';



@observer
class ViewChanger extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { linkTo } = this.props;

    if (!linkTo) return this.renderInner();
  
    return (
      <Link to={linkTo}>
        { this.renderInner() }
      </Link>
    );
  }

  renderInner() {
    const { label, isBoldLabel, onClick } = this.props;
    const className = `changer-label${isBoldLabel ? ' bold' : ''}`;

    return (
      <Row className='view-changer' onClick={onClick}>
        <Col xs={6} className={className}>
          {label}
        </Col>
        <Col xs={6}>
          <span className='arrow pull-right fa fa-angle-right fa-2x' />
        </Col>
      </Row>
    );
  }
}

ViewChanger.propTypes = {
  label: PropTypes.string.isRequired,
  linkTo: PropTypes.string,
  isBoldLabel: PropTypes.bool,
  onClick: PropTypes.func,
};

ViewChanger.defaultProps = {
  isBoldLabel: true
};

export default ViewChanger;
