import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import { getRandomInt } from '../../utils/random';

import DataState from './state/data';



@observer
class ExpandableText extends Component {
  constructor(props) {
    super(props);
    this.id = `expandable-text${getRandomInt()}`;
    Object.assign(this, {
      data: new DataState(props),
    });
    this.onClick = ::this.onClickHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    this.id = null;
    this.onClick = null;
    this.data.clear();
    this.data = null;
  }

  onClickHandler() {
    this.data.toggleExpanded();
  }

  render() {
    const { className } = this.props;
    const { isExpanded, isExpandable,isTextExists, parsed } = this.data;
    const dataClassName = `data${isTextExists ? ' no-top-padding' : ''}${className ? ` ${className}` : ''}`;
    const isNeedToShowExpandButton = !isExpanded && isExpandable;
    const classNameInner = `expandable-text${isNeedToShowExpandButton ? ' collapsed' : ''}`;

    return (
      <div className={dataClassName}>
        <div id={this.id} className={classNameInner}>
          { isTextExists ? parsed : null }
          { isNeedToShowExpandButton && <a className='expand-text' onClick={this.onClick}> See more</a> }
        </div>
      </div>
    );
  }
}

ExpandableText.propTypes = {
  text: PropTypes.string,
  isNeedToHandle: PropTypes.bool,
  usertags: PropTypes.array,
  className: PropTypes.string,
};

export default ExpandableText;
