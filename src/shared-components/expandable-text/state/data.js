import { action, observable, computed, toJS } from 'mobx';

import TextParser from '../../../utils/parsers/text';

import { truncateString } from '../../../utils/formatting/text';

import DataState from '../../../utils/state/data';

import { MAX_LENGTH } from '../constants';
import { EMPTY_CHAR } from '../../../constants';



class ExpandableTextDataState extends DataState {
  @observable text = '';
  @observable parsed = {};
  @observable isExpanded = false;
  @observable isExpandable = false;
  @observable isOpened = false;
  @observable isNeedToHandle = false;
  @observable usertags = [];

  parser = new TextParser();

  get isTextExists() {
    return this.text && this.text !== EMPTY_CHAR;
  }

  constructor(config = {}) {
    super(config);
    this.init(config);
  }

  init(props) {
    this.setIsNeedToHandle(props.isNeedToHandle);
    this.setUsertags(props.usertags);
    if (this.text !== props.text) this.setText(props.text);
  }

  formatText() {
    if (!this.isNeedToHandle || !this.isTextExists) return this.text;
    const text = this.isExpanded ? this.text : truncateString(this.text, MAX_LENGTH);
    this.setIsExpandable(this.text && text && text.length !== this.text.length);

    this.parser.init({
      string: text,
      userTags: this.usertags,
    });
    return this.parser.parse();
  }

  @action
  setText(value) {
    this.text = value;
    this.parsed = this.formatText();
  }

  @action
  setIsNeedToHandle(value) {
    this.isNeedToHandle = value;
  }

  @action
  setUsertags(value) {
    this.usertags = value;
  }
  
  @action
  toggleExpanded() {
    this.isExpanded = !this.isExpanded;
    this.parsed = this.formatText();
  }

  @action
  setIsExpandable(value) {
    this.isExpandable = value;
  }

  @action
  setIsOpened(value) {
    this.isOpened = value;
  }

  @action
  clear() {
    this.parser.clear();
    this.parser = null;
    this.text = null;
    this.parsed = null;
    this.isExpanded = null;
    this.isExpandable = null;
    this.isOpened = null;
    this.isNeedToHandle = null;
    this.usertags = null;
  }
}

export default ExpandableTextDataState;
