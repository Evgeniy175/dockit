import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Calendar from './calendar/index.jsx';

import DataState from './state/data';

import { PARENT_ELEMENT_ID, TYPES } from './constants';
import { INPUT_FORMATS } from '../../constants';

@observer
class CalendarDropdown extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    this.data.setSelectedDate(moment(props.value, this.props.pattern));
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
    this.onBodyClick = ::this.onBodyClickHandler;
    
    this.onDateChange = ::this.onDateChangeHandler;
    this.onPrevMonthClick = ::this.onPrevMonthClickHandler;
    this.onNextMonthClick = ::this.onNextMonthClickHandler;
    this.ignore = ::this.ignoreHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.setSelectedDate(moment(nextProps.value, this.props.pattern));
  }
  
  onClickHandler() {
    document.body.addEventListener('click', this.onBodyClick, false);
    this.data.setDropdownExpanded(true);
  }
  
  onBodyClickHandler(e) {
    let element = e.target;
    let isNeedToClose = element.id !== PARENT_ELEMENT_ID;
    
    while (isNeedToClose && element && element.parentElement) {
      element = element.parentElement;
      isNeedToClose = element.id !== PARENT_ELEMENT_ID;
    }
    
    if (isNeedToClose) {
      this.data.setDropdownExpanded(false);
      document.body.removeEventListener('click', this.onBodyClick, false);
    }
  }
  
  onDateChangeHandler(date) {
    const type = this.props.type;
  
    if (this.data.isOneDaySelect(type)) {
      this.data.setSelectedDate(date);
      this.data.setDropdownExpanded(false);
      this.props.onChange(date);
      return;
    }
  
    const dayOfMonth = date.date();
    this.data.addToSelectedDays(dayOfMonth);
  
    this.props.onChange({
      selectedDate: this.data.selectedDate,
      selectedDays: this.data.selectedDays
    });
  }
  
  onPrevMonthClickHandler() {
    const date = this.data.viewedDate.subtract(1, 'months');
    this.data.setViewedDate(date);
    this.data.setSelectedDays([]);
  }
  
  onNextMonthClickHandler() {
    const date = this.data.viewedDate.add(1, 'months');
    this.data.setViewedDate(date);
    this.data.setSelectedDays([]);
  }
  
  ignoreHandler() {
    
  }
  
  render() {
    const alwaysVisible = this.props.alwaysVisible;
    const isDropdownVisible = alwaysVisible || this.data.isDropdownVisible;
    const selectedDate = this.data.selectedDate;
    const selectedDays = this.data.selectedDays;
    const viewedDate = this.data.viewedDate;
    const pattern = this.props.pattern;
    
    const type = this.props.type;
    
    const isOneDaySelect = this.data.isOneDaySelect(type);
    const value = isOneDaySelect ? selectedDate.format(pattern) : selectedDays.join(', ');
    
    const data = {
      viewedDate,
      selectedDate,
      selectedDays,
      alwaysVisible,
      type
    };
    
    const handlers = {
      onDateChange: this.onDateChange,
      onPrevMonthClick: this.onPrevMonthClick,
      onNextMonthClick: this.onNextMonthClick
    };
    
    return (
      <div id={PARENT_ELEMENT_ID}>
        { !alwaysVisible && <input className='date-input no-select' type='text' readOnly onClick={this.onClick} value={value} onChange={this.ignore} /> }
        { isDropdownVisible && <Calendar data={data} handlers={handlers} /> }
      </div>
    );
  }
}

CalendarDropdown.propTypes = {
  onChange: PropTypes.func.isRequired,
  pattern: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.string,
  alwaysVisible: PropTypes.bool,
  selectedDays: PropTypes.array
};

CalendarDropdown.defaultProps = {
  pattern: INPUT_FORMATS.DATE,
  value: '',
  type: TYPES.NORMAL,
  alwaysVisible: false,
  selectedDays: []
};

export default CalendarDropdown;
