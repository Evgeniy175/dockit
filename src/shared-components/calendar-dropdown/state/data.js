import { action, computed, observable, toJS } from 'mobx';
import moment from 'moment';

import DataState from '../../../utils/state/data';

import { DATE_SELECT_TYPES } from '../constants';

class CalendarDropdownDataState extends DataState {
  @observable isDropdownVisible = false;
  @observable selectedDate = moment();
  @observable viewedDate = moment();
  @observable selectedDays = [];
  
  constructor(config = {}) {
    super(config);
  }
  
  isOneDaySelect(type) {
    return DATE_SELECT_TYPES.includes(type);
  }
  
  @action
  setDropdownExpanded(value) {
    this.isDropdownVisible = value;
  }
  
  @action
  toggleDropdownVisibility() {
    this.isDropdownVisible = !this.isDropdownVisible;
  }
  
  @action
  setSelectedDate(value) {
    if (!value.isValid() || value.isSame(this.selectedDate)) return;
    
    this.selectedDate = value.clone();
    this.setViewedDate(this.selectedDate);
  }
  
  @action
  setViewedDate(value) {
    this.viewedDate = value.clone();
  }
  
  @action
  setSelectedDays(value = []) {
    this.selectedDays = value;
  }
  
  @action
  addToSelectedDays(value) {
    const idx = this.selectedDays.indexOf(value);
    
    if (idx === -1) {
      this.selectedDays.push(value);
      return;
    }
    
    this.selectedDays.splice(idx, 1);
  }
}

export default CalendarDropdownDataState;
