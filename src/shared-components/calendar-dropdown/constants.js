export const PARENT_ELEMENT_ID = 'calendar-dropdown';

export const TYPES = {
  NORMAL: 'normal',
  DAYS_SELECT: 'days select'
};

export const DATE_SELECT_TYPES = [TYPES.NORMAL];
