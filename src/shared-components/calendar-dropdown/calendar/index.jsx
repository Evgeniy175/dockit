import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { TYPES } from './month/header/arrows/constants';

import Month from './month/index.jsx';

@observer
class Calendar extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const handlers = this.props.handlers;
    const data = this.props.data;
    const newData = Object.assign(data, {
      visibleArrows: [TYPES.PREV, TYPES.NEXT]
    });
    const className = `calendar no-select${data.alwaysVisible ? ' always-visible' : ''}`;
    
    return (
      <div className={className}>
        <Month data={newData} handlers={handlers} />
      </div>
    );
  }
}

Calendar.propTypes = {
  data: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired
};

export default Calendar;
