import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';

import { WEEK_DAYS } from './constants';

@observer
class CalendarWeekdays extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className='weekdays'>
        {
          WEEK_DAYS.map(day => <span key={day} className='weekday'>{ day }</span>)
        }
      </div>
    );
  }
}

export default CalendarWeekdays;
