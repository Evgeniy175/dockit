import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Day from './day/index.jsx';

@observer
class CalendarMonthDaysRow extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDayClick = ::this.onDayClickHandler;
  }
  
  onDayClickHandler(date) {
    this.props.onDayClick(date);
  }
  
  render() {
    const { days } = this.props;
    return (
      <div className='days'>
        {
          days.map((day, idx) => <Day key={get(day, 'date.format') ? day.date.format() : idx}
                                      day={day}
                                      isFirst={idx === 0}
                                      isLast={this.isLast(idx)}
                                      selected={this.isSelected(day)}
                                      onClick={this.onDayClick} />)
        }
      </div>
    );
  }
  
  isSelected(currentDay) {
    if (!currentDay) return false;

    const { selectedDay, selectedDays } = this.props;

    const selectedDate = selectedDay && selectedDay.date();
    const selectedMonth = selectedDay && selectedDay.month();

    const day = currentDay.date.date();
    const month = currentDay.date.month();
    return (selectedMonth === month && selectedDate === day) || selectedDays.includes(day);
  }

  isLast(idx) {
    const { days } = this.props;
    return idx === days.length - 1 || !days[idx + 1];
  }
}

CalendarMonthDaysRow.propTypes = {
  selectedDays: PropTypes.array.isRequired,
  selectedDay: PropTypes.object,
  days: PropTypes.array.isRequired,
  onDayClick: PropTypes.func.isRequired
};

export default CalendarMonthDaysRow;
