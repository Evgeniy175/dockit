import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

@observer
class CalendarMonthDay extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler(e) {
    e.preventDefault();
    
    const day = this.props.day;
    this.props.onClick(day.date);
  }
  
  render() {
    const { day, selected, isFirst, isLast } = this.props;
    
    if (!day) return <div className='empty-day' />;
    
    const className = `day${day.isPassed ? ' passed' : ''}${selected ? ' selected' : ''}${isFirst ? ' first-day-of-week' : ''}${isLast ? ' last-day-of-week' : ''}`;
    return (
      <div className={className} onClick={this.onClick}>
        { day.dayOfMonth }
      </div>
    );
  }
}

CalendarMonthDay.propTypes = {
  day: PropTypes.object,
  isFirst: PropTypes.bool,
  isLast: PropTypes.bool,
  selected: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

CalendarMonthDay.defaultProps = {
  selected: false
};

export default CalendarMonthDay;
