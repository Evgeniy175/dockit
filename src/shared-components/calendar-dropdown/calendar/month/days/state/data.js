import { action, observable, toJS } from 'mobx';

import DataState from '../../../../../../utils/state/data';

import { getCalendarForDate } from '../../../../../../utils/generators/calendar';

class CalendarMonthDaysDataState extends DataState {
  @observable selectedDate;
  @observable viewedDate;
  @observable calendar;
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }
  
  init(props) {
    this.setViewedDate(props.data.viewedDate);
    this.setSelectedDate(props.data.selectedDate);
  
    const calendar = getCalendarForDate(this.viewedDate);
    this.setCalendar(calendar);
  }
  
  @action
  setSelectedDate(value) {
    this.selectedDate = value;
  }
  
  @action
  setViewedDate(value) {
    this.viewedDate = value;
  }
  
  @action
  setCalendar(value) {
    this.calendar = value;
  }
}

export default CalendarMonthDaysDataState;
