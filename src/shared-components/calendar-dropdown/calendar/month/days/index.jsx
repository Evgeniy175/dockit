import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import DataState from './state/data';

import Row from './row/index.jsx';

@observer
class CalendarMonthDays extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
  }
  
  componentWillMount() {
    this.onDateChange = ::this.onDateChangeHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onDateChangeHandler(date) {
    this.props.handlers.onDateChange(date);
  }
  
  render() {
    const data = this.props.data;
    const selectedDays = toJS(data.selectedDays);
    const selectedDay = toJS(data.selectedDate);
    const calendar = toJS(this.data.calendar);
    
    return (
      <div className='days'>
        {
          calendar.map((row, idx) => <Row key={idx}
                                          days={row}
                                          selectedDays={selectedDays}
                                          selectedDay={selectedDay}
                                          onDayClick={this.onDateChange} />)
        }
      </div>
    );
  }
}

CalendarMonthDays.propTypes = {
  data: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired
};

export default CalendarMonthDays;
