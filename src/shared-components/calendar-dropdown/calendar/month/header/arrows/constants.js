export const TYPES = {
  PREV: 'prev',
  NEXT: 'next'
};

export const ARROW_CLASSES = {
  [TYPES.PREV]: 'fa fa-chevron-left left',
  [TYPES.NEXT]: 'fa fa-chevron-right right'
};
