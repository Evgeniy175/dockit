import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { ARROW_CLASSES } from './constants';

@observer
class CalendarMonthArrows extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler(e) {
    e.preventDefault();
    this.props.onClick();
  }
  
  render() {
    const visibleArrows = this.props.visibleArrows;
    const type = this.props.type;
    const isExists = visibleArrows.includes(type);
    
    if (!isExists) return null;
    
    const className = `arrow ${ARROW_CLASSES[type]}`;
  
    return <span className={className} onClick={this.onClick} />;
  }
}

CalendarMonthArrows.propTypes = {
  type: PropTypes.string.isRequired,
  visibleArrows: PropTypes.array,
  onClick: PropTypes.func.isRequired
};

CalendarMonthArrows.defaultProps = {
  visibleArrows: []
};

export default CalendarMonthArrows;
