import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { MONTH_FORMAT } from './constants';
import { TYPES } from './arrows/constants';

import Arrows from './arrows/index.jsx';



@observer
class CalendarMonthHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onPrevClick = ::this.onPrevClickHandler;
    this.onNextClick = ::this.onNextClickHandler;
  }
  
  onPrevClickHandler() {
    const handlers = this.props.handlers;
    const onPrevMonthClick = get(handlers, 'onPrevMonthClick', () => {});
    onPrevMonthClick();
  }
  
  onNextClickHandler() {
    const handlers = this.props.handlers;
    const onNextMonthClick = get(handlers, 'onNextMonthClick', () => {});
    onNextMonthClick();
  }
  
  render() {
    const data =this.props.data;
    const visibleArrows = get(data, 'visibleArrows', []);
    const date = get(data, 'viewedDate');
    const monthName = date.format(MONTH_FORMAT);
    const year = date.year();
    
    return (
      <div className='header'>
        <Arrows type={TYPES.PREV} visibleArrows={visibleArrows} onClick={this.onPrevClick} />
        <span className='text'>{ `${monthName} ${year}` }</span>
        <Arrows type={TYPES.NEXT} visibleArrows={visibleArrows} onClick={this.onNextClick} />
      </div>
    );
  }
}

CalendarMonthHeader.propTypes = {
  data: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired
};

export default CalendarMonthHeader;
