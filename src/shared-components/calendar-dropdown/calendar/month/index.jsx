import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Header from './header/index.jsx';
import Weekdays from './weekdays/index.jsx';
import Days from './days/index.jsx';



@observer
class CalendarMonth extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const data = this.props.data;
    const handlers = this.props.handlers;
    const visibleArrows = get(data, 'visibleArrows', []);
    
    return (
      <div className='month'>
        <Header data={data} handlers={handlers}  />
        <Weekdays />
        <Days data={data} handlers={handlers} />
      </div>
    );
  }
}

CalendarMonth.propTypes = {
  data: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired
};

export default CalendarMonth;
