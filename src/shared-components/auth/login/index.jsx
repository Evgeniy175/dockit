import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer, observable } from 'mobx-react';
import { browserHistory } from 'react-router';
import { Form, Grid, Row, Col } from 'react-bootstrap';
import { get, isEmpty } from 'lodash';
import PropTypes from 'prop-types';

import Input from '../../validation-input/index.jsx';
import Button from '../../../shared-components/button/index.jsx';
import ValidationSummary from '../../../shared-components/validation-summary/index.jsx';
import ForgotPasswordLink from './forgot-password-link/index.jsx';
import DownloadButton from './download-button/index.jsx';

import { FIELDS, ERRORS } from './constants';
import { SIGN_UP_PATH } from '../../../constants';
import { CSS_MAPPING } from '../../../shared-components/button/constants';

import DataState from './state/data';



@observer
class Login extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
  }
  
  componentWillMount() {
    this.onEmailChange = ::this.onEmailChangeHandler;
    this.onPasswordChange = ::this.onPasswordChangeHandler;
    this.onSubmit = ::this.onSubmitHandler;
    this.onSignUpClick = ::this.onSignUpClickHandler;
  }
  
  onEmailChangeHandler(value) {
    this.data.setEmail(value);
    this.data.clearValidation(FIELDS.EMAIL);
  }
  
  onPasswordChangeHandler(value) {
    this.data.setPassword(value);
    this.data.clearValidation(FIELDS.PASSWORD);
  }
  
  onSubmitHandler(e) {
    e.preventDefault();
    
    if (!this.data.isFormValid()) return;
    
    this.data.doLogin()
    .catch(err => {
      const message = get(err, 'res.text', ERRORS[FIELDS.SUMMARY]);
      this.data.addSummaryError(message);
    });
  }
  
  onSignUpClickHandler(e) {
    e.preventDefault();
    browserHistory.push(SIGN_UP_PATH);
  }

  render() {
    const errors = toJS(this.data.validation);
    const email = this.data.email;
    const password = this.data.password;
    const validationSummary = errors.summary;
    const isSummaryExists = !isEmpty(validationSummary);
    const { downloadIconVisible } = this.props;
    
    return (
      <div className='sign-in-container no-select'>
        <Grid fluid={true}>
          {
            isSummaryExists && <ValidationSummary errors={validationSummary} />
          }
          <Row>
            <Col xs={12}>
              <Form>
                <Input inputType='text' error={errors[FIELDS.EMAIL]} value={email} placeholder='Email' onChange={this.onEmailChange} />
                <Input inputType='password' error={errors[FIELDS.PASSWORD]} value={password} placeholder='Password' onChange={this.onPasswordChange} />
                <div className='buttons'>
                  <Button text='Log In' onClick={this.onSubmit} className={CSS_MAPPING.BLUE_FILLED} type='submit' />
                </div>
                <ForgotPasswordLink />
                <div className='text dark dont-have-account-label'>Don't have an account?</div>
                <div className='buttons sign-up-btn'>
                  <Button text='Sign Up' onClick={this.onSignUpClick} className={CSS_MAPPING.BLUE} type='submit' />
                </div>
                { /*downloadIconVisible && <DownloadButton />*/ }
              </Form>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Login.propTypes = {
  downloadIconVisible: PropTypes.bool
};

Login.defaultProps = {
  downloadIconVisible: true
};

export default Login;
