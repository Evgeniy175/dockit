export const DOWNLOAD_FROM_APP_STORE = 'https://itunes.apple.com/us/app/dockit-calendar/id1099065307';
export const DOWNLOAD_FROM_GOOGLE_PLAY = 'https://play.google.com/store/apps/details?id=com.dockitcalendar';
