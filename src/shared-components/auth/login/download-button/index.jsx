import React, { Component } from 'react';
import {toJS} from 'mobx';
import {observer, observable} from 'mobx-react';
import {Link} from 'react-router';

import {DOWNLOAD_FROM_APP_STORE, DOWNLOAD_FROM_GOOGLE_PLAY} from './constants';

import IosDownloadIcon from '../../../../assets/images/icons/download/download-on-app-store.svg';
import AndroidDownloadIcon from '../../../../assets/images/icons/download/download-on-google-play.svg';



@observer
class DownloadButtons extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='download-app'>
        <div className='download-text'>
          Get the app.
        </div>
        <div className='download-buttons-wrapper'>
          <Link to={DOWNLOAD_FROM_APP_STORE} target='_blank'>
            <img src={IosDownloadIcon} className='no-select' />
          </Link>
          <Link to={DOWNLOAD_FROM_GOOGLE_PLAY} target='_blank'>
            <img src={AndroidDownloadIcon} className='no-select' />
          </Link>
        </div>
      </div>
    );
  }
}

export default DownloadButtons;
