export const DOWNLOAD_FROM_APP_STORE = 'https://itunes.apple.com/us/app/dockit-calendar/id1099065307';

export const FIELDS = {
  EMAIL: 'email',
  PASSWORD: 'password',
  SUMMARY: 'summary'
};

export const ERRORS = {
  [FIELDS.EMAIL]: 'Please enter valid email',
  [FIELDS.PASSWORD]: 'Please enter valid password',
  [FIELDS.SUMMARY]: 'Error'
};
