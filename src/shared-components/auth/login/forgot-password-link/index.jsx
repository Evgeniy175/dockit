import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer, observable } from 'mobx-react';
import { Link } from 'react-router';

@observer
class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='common-links'>
        <div className='text bright forgot-password-link'>
          <Link to='/users/forgot-password'>
            Forgot password?
          </Link>
        </div>
      </div>
    );
  }
}

export default Login;
