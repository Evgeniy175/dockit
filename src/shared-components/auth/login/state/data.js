import { action, observable, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import { isEmpty } from 'lodash';
import EmailValidator from 'email-validator';

import { FIELDS, ERRORS } from '../constants';

import DataState from '../../../../utils/state/data';



class ViewLoginDataState extends DataState {
  @observable validation = {};
  
  @observable email = '';
  @observable password = '';
  
  constructor(props = {}) {
    super(props);
  }
  
  isFormValid() {
    this.clearValidation();
    
    if (!EmailValidator.validate(this.email)) this.addError(FIELDS.EMAIL);
    if (!this.password || this.password.length === 0 || !/^(?=.*\d)(?=.*[a-zA-Z])(\w+){6,}$/.test(this.password)) this.addError(FIELDS.PASSWORD);
    
    return isEmpty(toJS(this.validation));
  }
  
  @action
  addError(key) {
    this.validation[key] = ERRORS[key];
    this.validation = toJS(this.validation);
  }
  
  @action
  addSummaryError(value) {
    this.validation.summary = this.validation.summary || [];
    this.validation.summary.push(value);
    this.validation = toJS(this.validation);
  }
  
  doLogin() {
    return Auth.login(this.email, this.password);
  }
  
  @action
  setEmail(value) {
    this.email = value.toLowerCase();
  }
  
  @action
  setPassword(value) {
    this.password = value;
  }
  
  @action
  clearValidation(key) {
    if (key) {
      delete this.validation[key];
      this.validation = toJS(this.validation);
      return;
    }
  
    this.validation = {};
  }
}

export default ViewLoginDataState;