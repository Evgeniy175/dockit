import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer, observable } from 'mobx-react';

import { DOWNLOAD_FROM_APP_STORE } from '../login/constants';



@observer
class GetTheAppBar extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const newWindow = window.open(DOWNLOAD_FROM_APP_STORE, '_blank');
    newWindow.focus();
  }

  render() {
    return (
      <div className='get-the-app-bar' onClick={this.onClick}>
        <div className='text-wrapper'>
          <div className='text'>
            DockIt
          </div>
          <div className='text'>
            Find it for free in the App Store
          </div>
        </div>
        <div className='get-app-text'>
          GET
        </div>
      </div>
    );
  }
}

export default GetTheAppBar;
