import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga';



@observer
class LinkAnchor extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { analyticsData } = this.props;
    if (analyticsData) ReactGA.event(analyticsData);
  }

  render() {
    const { text, href } = this.props;
    return <a href={href} target='_blank' onClick={this.onClick}>{text}</a>;
  }
}

LinkAnchor.propTypes = {
  href: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  analyticsData: PropTypes.object,
  onClick: PropTypes.func,
};

export default LinkAnchor;
