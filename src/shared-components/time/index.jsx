import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { INPUT_FORMATS } from '../../constants';
import PropTypes from 'prop-types';
import { get } from 'lodash';



@observer
class TimeInput extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onChange = ::this.onChangeHandler;
  }

  onChangeHandler(e) {
    const handler = get(this.props, 'handlers.onChange');
    if (handler) handler(this.handleTime(e.target.value));
  }

  handleTime(value) {
    const oldTimeItems = get(this.props, 'values.value', '').split(':');
    const newTimeItems = value.split(':');

    const oldTime = {
      h: oldTimeItems[0],
      m: oldTimeItems[1],
    };

    const newTime = {
      h: newTimeItems[0],
      m: newTimeItems[1],
    };

    const newHour = Number.parseInt(newTime.h);

    if (newTime.h === oldTime.h) {
      if (newTime.m === '00' && oldTime.m === '59') newTime.h = newHour + 1;
      else if (newTime.m === '59' && oldTime.m === '00') newTime.h = newHour > 0 ? newHour - 1 : '23';
    } else if (newTime.m === oldTime.m) {
      if (oldTime.h === '00' && newTime.h === '11') newTime.h = '23';
      else if (oldTime.h === '12' && newTime.h === '23') newTime.h = '11';
      else if (oldTime.h === '11' && newTime.h === '00') newTime.h = '12';
      else if (oldTime.h === '23' && newTime.h === '12') newTime.h = '00';
    }

    return `${newTime.h}:${newTime.m}`;
  }
  
  render() {
    const { value, label, min, max, errors } = this.props.values;
    const className = `time-block${get(errors, 'time') ? ' error-text' : ''}`;
    
    return (
      <span className={className}>
        {
          value && <input className='time-input' type='time' pattern={INPUT_FORMATS.TIME} value={value} min={min} max={max} onChange={this.onChange} />
        }
        <label className='time-label' htmlFor='time'>{ label }</label>
      </span>
    );
  }
}

TimeInput.propTypes = {
  values: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    min: PropTypes.string,
    max: PropTypes.string,
    errors: PropTypes.object,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func,
  }).isRequired,
};

export default TimeInput;
