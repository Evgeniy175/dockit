import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { DEFAULT_SHOWN_ROWS } from './constants';



@observer
class CommentTextArea extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { id, value } = this.props.values;
    const { onChange } = this.props.handlers;
    const rows = get(this.props, 'values.rows', DEFAULT_SHOWN_ROWS);
    const placeholder = get(this.props, 'values.placeholder');
    return <textarea className='comment-area' id={id} rows={rows} placeholder={placeholder} value={value} onChange={onChange} />;
  }
}

CommentTextArea.propTypes = {
  values: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    rows: PropTypes.number,
    placeholder: PropTypes.string.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

export default CommentTextArea;
