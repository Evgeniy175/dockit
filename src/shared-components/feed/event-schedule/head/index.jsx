import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { action, observable, computed } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Image from '../../../image/index.jsx';
import Content from './content/index.jsx';

import Menu from '../../../creations/menu/index.jsx';

import { CREATOR_VALUES } from './constants';



@observer
class FeedHead extends Component {
  @observable item;

  @computed
  get itemData() {
    return this.item.data;
  }

  constructor(props) {
    super(props);
    this.init(props);
    this.creatorHandlers = {
      onClick: ::this.onImageClickHandler,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    this.setItem(props.data);
  }

  onImageClickHandler() {
    browserHistory.push(`/users/${this.itemData.user.id}`);
  }

  @action
  setItem(value) {
    this.item = value;
  }

  render() {
    const { type, onDelete } = this.props;
    const target = `${this.item.type}s`;

    return (
      <div className='heading' onClick={this.onClick}>
        <Image values={CREATOR_VALUES(this.itemData.user)} handlers={this.creatorHandlers} />
        <Content user={this.itemData.user} feedItem={this.item} type={type} />
        <Menu type={target} item={this.itemData} onDelete={onDelete} />
      </div>
    );
  }
}

FeedHead.propTypes = {
  data: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default FeedHead;
