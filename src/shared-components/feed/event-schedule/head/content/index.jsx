import React, { Component } from 'react';
import { Link } from 'react-router';
import { observer } from 'mobx-react';
import { getPretext } from '../../../../../utils/formatting/text';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import PrivateText from '../../../../creations/feed-item-private-title-text/index.jsx';

import jq from '../../../../../utils/jquery';

import { isPrivate } from '../../../../../utils/creations';
import { getRandomInt } from '../../../../../utils/random';

import { MAX_ITERATIONS, TIMEOUT_MS } from './constants';



const ACTIONS = {
  'create': 'created',
  'update': 'modified'
};



@observer
class FeedHeadContent extends Component {
  nOfIterations = 0;
  intervalId;

  constructor(props) {
    super(props);
    this.id = `creatorId${getRandomInt()}`;
    this.wrapperId = `wrapperId${getRandomInt()}`;
    this.onItemRender = ::this.onItemRenderHandler;
  }

  componentDidMount() {
    setInterval(this.onItemRender, TIMEOUT_MS);
  }

  onItemRenderHandler() {
    if (this.nOfIterations++ >= MAX_ITERATIONS) return clearInterval(this.intervalId);

    const item = jq.wrap(`#${this.id}`);
    if (!item.isValid() || item.clientHeight !== item.scrollHeight) return;
    item.css('display', 'inline');
    clearInterval(this.intervalId);
  }

  render() {
    const { type } = this.props;
    const user = this.props.user;
    const feedItem = this.props.feedItem;
    const target = feedItem.type;
    const action = feedItem.action;
    const name = user.username;
    const userId = user.id;
    const pretext = getPretext(target);

    return (
      <div className='heading-content'>
        <h4 className='description' id={this.wrapperId}>
          <Link to={`/users/${userId}`} className='creator' id={this.id}>
            {name}
          </Link>
          <span> {ACTIONS[action]} {pretext} {target}</span>
        </h4>
        <PrivateText type={type} isPrivate={isPrivate(get(feedItem, 'data.permission'))} />
      </div>
    );
  }
}

FeedHeadContent.propTypes = {
  user: PropTypes.object.isRequired,
  feedItem: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
};

export default FeedHeadContent;
