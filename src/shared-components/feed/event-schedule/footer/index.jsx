import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import moment from 'moment-timezone';

import LikeAction from '../../../action-command/like/index.jsx';
import DockAction from '../../../action-command/dock/index.jsx';
import Comment from '../../../action-command/comment/index.jsx';

import { formatFromNow } from '../../../../utils/formatting/time';
import { POSTFIX_POSITIONS } from '../../../action-command/dock/constants';



@observer
class FeedFooter extends Component {
  constructor(props) {
    super(props);
  }
  
  getTime(feedItem) {
    return feedItem.action === 'update' ? feedItem.timestamp : feedItem.data.created_at;
  }

  render() {
    const feedItem = this.props.feedItem;
    const dateBuff = moment(this.getTime(feedItem));
    const date = formatFromNow(dateBuff);
    
    const likeData = {
      type: `${feedItem.type}s`,
      id: get(feedItem, 'data.id'),
      likes: get(feedItem, 'data.decorated.likes')
    };

    const dockData = {
      type: `${feedItem.type}s`,
      event: get(feedItem, 'data'),
    };

    const commentData = {
      type: `${feedItem.type}s`,
      id: get(feedItem, 'data.id'),
      count: get(feedItem, 'data.decorated.comments.count', 0)
    };
    
    return (
      <div className='footer'>
        <div className='action-bar'>
          <LikeAction data={likeData} />
          <DockAction data={dockData} postfixPosition={POSTFIX_POSITIONS.RIGHT} />
          <Comment data={commentData} />
        </div>
        <div className='date'>
          {date}
        </div>
      </div>
    );
  }
}

FeedFooter.propTypes = {
  feedItem: PropTypes.object.isRequired
};

export default FeedFooter;
