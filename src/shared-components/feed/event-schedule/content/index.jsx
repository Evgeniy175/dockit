import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';

import ExpandableText from '../../../expandable-text/index.jsx';
import Image from '../../../image/index.jsx';

import { formatInterval } from '../../../../utils/formatting/date';

import DataState from './state/data';

import { IMAGE_VALUES } from './constants';



@observer
class FeedContent extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
    this.imageHandlers = {
      onClick: ::this.onImageClickHandler,
    };
  }

  componentWillUnmount() {
    this.data.clear();
    this.data = null;
    this.imageHandlers = null;
  }

  onImageClickHandler() {
    const { feedItem, targetType } = this.data;
    browserHistory.push(`/${targetType}/${feedItem.id}`);
  }

  render() {
    const { feedItem, targetType } = this.data;
    const { description, userTags } = feedItem;
    const start = moment(feedItem.startTime);
    const end = moment(feedItem.endTime);
    const interval = formatInterval(start, end);

    return (
      <div className='content' onClick={this.props.onClick}>
        <Image values={IMAGE_VALUES(feedItem, targetType)} handlers={this.imageHandlers} />
        <div className='data-wrapper'>
          <div className='head'>
            <div className='title strong'>
              <ExpandableText text={feedItem.title} />
            </div>
            <div className='timestamp'>{interval}</div>
          </div>
          { feedItem.description && <ExpandableText text={toJS(description)} usertags={toJS(userTags)} isNeedToHandle={true} /> }
        </div>
      </div>
    );
  }
}

FeedContent.propTypes = {
  data: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

export default FeedContent;
