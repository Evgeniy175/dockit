import { action, observable, computed, toJS } from 'mobx';

import DataState from '../../../../../utils/state/data';



class FeedEventContentDataState extends DataState {
  @observable feedItem = {};
  @observable targetType = {};
  @observable isExpanded = false;
  @observable isExpandable = true;
  
  constructor(config = {}) {
    super(config);
    this.setFeedItem(config.data.data);
    this.setTargetType(config.data.type);
  }
  
  @action
  setFeedItem(value) {
    this.feedItem = value;
  }
  
  @action
  setTargetType(value) {
    this.targetType = value;
  }
  
  @action
  toggleExpanded() {
    this.isExpanded = !this.isExpanded;
  }

  @action
  setIsExpandable(value) {
    this.isExpandable = value;
  }

  @action
  clear() {
    this.feedItem = null;
    this.targetType = null;
    this.isExpanded = null;
    this.isExpandable = null;
  }
}

export default FeedEventContentDataState;
