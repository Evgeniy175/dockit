import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import Head from '../../../event-schedule/head/index.jsx';
import Content from '../../../event-schedule/content/index.jsx';
import Footer from '../../../event-schedule/footer/index.jsx';

import jq from '../../../../../utils/jquery';

import DataState from './state/data';

import { TYPE } from './constants';
import { MODEL_PLURAL } from '../../../../../models/schedules/constants';



@observer
class FeedSchedule extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState(),
    }));
    this.data.setSchedule(props.schedule);

    this.onClick = ::this.onClickHandler;
  }

  componentWillUnmount() {
    this.onClick = null;
    this.data.clear();
    this.data = null;
  }
  
  onClickHandler(e) {
    if (jq.wrap(e.target).is('a')) return;
    const schedule = toJS(this.props.schedule);
    browserHistory.push(`/schedules/${schedule.data.id}`);
  }

  render() {
    const { schedule, onDelete } = this.props;
    return (
      <div className='feed-item-content schedule-feed-item'>
        <Head data={schedule} type={TYPE} onDelete={onDelete} />
        <Content data={schedule} onClick={this.onClick} />
        <Footer feedItem={schedule} />
      </div>
    );
  }
}

FeedSchedule.propTypes = {
  schedule: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default FeedSchedule;
