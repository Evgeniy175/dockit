import { action, observable, computed, toJS } from 'mobx';

import DataState from '../../../../../../utils/state/data';



class FeedScheduleDataState extends DataState {
  @observable schedule = {};

  constructor(config = {}) {
    super(config);
  }

  @action
  setSchedule(value = {}) {
    this.schedule = value;
  }

  @action
  clear() {
    this.schedule = null;
  }
}

export default FeedScheduleDataState;
