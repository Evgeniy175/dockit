import { action, observable, computed } from 'mobx';

import DataState from '../../../../../../utils/state/data';



class FeedEventDataState extends DataState {
  @observable event = {};

  constructor(config = {}) {
    super(config);
  }
  
  @action
  setEvent(value = {}) {
    this.event = value;
  }

  @action
  clear() {
    this.event = null;
  }
}

export default FeedEventDataState;
