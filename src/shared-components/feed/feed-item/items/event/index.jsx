import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import Head from '../../../event-schedule/head/index.jsx';
import Content from '../../../event-schedule/content/index.jsx';
import Footer from '../../../event-schedule/footer/index.jsx';

import jq from '../../../../../utils/jquery';

import DataState from './state/data';

import { TYPE } from './constants';



@observer
class FeedEvent extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState()
    }));
    this.data.setEvent(props.event);

    this.onClick = ::this.onClickHandler;
  }
  
  componentWillUnmount() {
    this.onClick = null;
    this.data.clear();
    this.data = null;
  }
  
  onClickHandler(e) {
    if (jq.wrap(e.target).is('a')) return;
    const event = toJS(this.props.event);
    browserHistory.push(`/events/${event.data.id}`);
  }

  render() {
    const { event, onDelete } = this.props;
    return (
      <div className='feed-item-content event-feed-item'>
        <Head data={event} type={TYPE} onDelete={onDelete} />
        <Content data={event} onClick={this.onClick} />
        <Footer feedItem={event} />
      </div>
    );
  }
}

FeedEvent.propTypes = {
  event: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default FeedEvent;
