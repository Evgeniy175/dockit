import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import jq from '../../../../../utils/jquery';

import Head from './head/index.jsx';
import Content from './content/index.jsx';
import Footer from './footer/index.jsx';



@observer
class FeedDock extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  componentWillUnmount() {
    this.onClick = null;
  }
  
  onClickHandler(e) {
    if (jq.wrap(e.target).is('a')) return;
    const data = toJS(this.props.dock.data);
    const target = this.getTarget(data);
    const feedItem = data[target];
    browserHistory.push(`/${target}s/${feedItem.id}`);
  }

  render() {
    const data = this.props.dock.data;
    const participatingFollowings = toJS(get(data, 'participatingFollowings', []));
    const target = this.getTarget(data);
    const feedItem = data[target];
    const user = get(data, 'user') || get(data, `${target}.user`);

    const headValues = {
      item: feedItem,
      user,
      target,
      participatingFollowings,
    };
    
    return (
      <div className='feed-item-content dock-feed-item'>
        <Head values={headValues} />
        <Content feedItem={feedItem} target={target} onClick={this.onClick} />
        <Footer data={data} target={target} />
      </div>
    );
  }
  
  getTarget(feedItem) {
    const schedule = get(feedItem, 'schedule');
    const event = get(feedItem, 'event');
    return schedule ? 'schedule' : 'event';
  }
}

FeedDock.propTypes = {
  dock: PropTypes.object.isRequired
};

export default FeedDock;
