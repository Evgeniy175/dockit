import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';

import { formatInterval } from '../../../../../../utils/formatting/date';

import Image from '../../../../../image/index.jsx';

import { IMAGE_VALUES } from './constants';



@observer
class FeedDockContent extends Component {
  constructor(props) {
    super(props);
    this.imageHandlers = {
      onClick: ::this.onImageClickHandler,
    };
  }

  onImageClickHandler() {
    const { feedItem, targetType } = this.data;
    browserHistory.push(`/${targetType}/${feedItem.id}`);
  }

  render() {
    const feedItem = this.props.feedItem;
    const target = this.props.target;
    
    const start = moment(feedItem.startTime);
    const end = moment(feedItem.endTime);
    
    const interval = formatInterval(start, end);
    
    return (
      <div className='content' onClick={this.props.onClick}>
        <Image values={IMAGE_VALUES(feedItem, target)} handlers={this.imageHandlers} />
        <div className='data'>
          <div className='head'>
            <div className='title strong'>{feedItem.title}</div>
            <div className='timestamp'>{interval}</div>
          </div>
        </div>
      </div>
    );
  }
}

FeedDockContent.propTypes = {
  feedItem: PropTypes.object.isRequired,
  target: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

export default FeedDockContent;
