import EventsModel from '../../../../../../models/events';
import SchedulesModel from '../../../../../../models/schedules';

import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../../../../../models/events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../../../../../models/schedules/constants';
import {DEFAULT_RESOLUTION} from '../../../../../../utils/images/resolution/constants';

const MODEL = type => {
  if (type === EVENTS_MODEL_PLURAL) return EventsModel;
  if (type === SCHEDULES_MODEL_PLURAL) return SchedulesModel;
};

import { ASPECT_RATIOS } from '../../../../../image/constants';

export const IMAGE_VALUES = (item, type) => {
  if (!item) return {};
  return {
    src: MODEL(`${type}s`).formatImageUrl(item.image, undefined, DEFAULT_RESOLUTION),
    wrapperClassName: 'image-container',
    aspectRatio: ASPECT_RATIOS.s16x9,
    prepareWrapper: true,
  };
};
