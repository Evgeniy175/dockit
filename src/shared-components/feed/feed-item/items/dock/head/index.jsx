import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { Auth } from '../../../../../../auth';

import Content from './content/index.jsx';
import Menu from './menu/index.jsx';



@observer
class FeedDockHead extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { user, target, item, participatingFollowings } = this.props.values;
    const userId = user.id;
    const currentUserId = Auth.isSigned() && Auth.getActiveUser().id;
    const isUserOwner = userId === currentUserId;

    return (
      <div className='heading'>
        <Content target={target} item={item} followingUsers={toJS(participatingFollowings)} />
        { !isUserOwner && <Menu targetId={item.id} target={target} /> }
      </div>
    );
  }
}

FeedDockHead.propTypes = {
  values: PropTypes.shape({
    user: PropTypes.object.isRequired,
    item: PropTypes.object.isRequired,
    target: PropTypes.string.isRequired,
    participatingFollowings: PropTypes.array.isRequired,
  })
};

export default FeedDockHead;
