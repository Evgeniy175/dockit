import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import Svg from 'react-svg';

import DotsImage from '../../../../../../../assets/images/three-dots.svg';



@observer
class ContentDockMenu extends Component {
  constructor(props) {
    super(props);
    this.onReport = ::this.onReportHandler;
  }
  
  componentWillUnmount() {
    this.onReport = null;
  }

  onReportHandler() {
    const id = this.props.targetId;
    const target = this.props.target;
    browserHistory.push(`reports/${target}s/${id}`);
  }
  
  getEventId() {
    return this.props.eventId;
  }
  
  render() {
    return (
      <span className='menu'>
        <DropdownButton id='dropdown-menu' title={ <Svg path={DotsImage} /> }>
          <MenuItem onClick={this.onReport}>Report</MenuItem>
        </DropdownButton>
      </span>
    );
  }
}

ContentDockMenu.propTypes = {
  targetId: PropTypes.string.isRequired,
  target: PropTypes.string.isRequired,
};

export default ContentDockMenu;
