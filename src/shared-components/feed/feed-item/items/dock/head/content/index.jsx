import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import UserLink from '../../../../../../users/username-link/index.jsx';
import DockAction from '../../../../../../action-command/dock/index.jsx';

import jq from '../../../../../../../utils/jquery';

import { getPretext } from '../../../../../../../utils/formatting/text';
import { getRandomInt } from '../../../../../../../utils/random';

import { MAX_ITERATIONS, TIMEOUT_MS } from './constants';
import { OUTPUT_TYPES } from '../../../../../../action-command/dock/constants';



@observer
class FeedDockHeadContentText extends Component {
  constructor(props) {
    super(props);
    this.id = `dockItemId${getRandomInt()}`;
    this.wrapperId = `wrapperId${getRandomInt()}`;
    this.postfixId = `feedDockPostfix${getRandomInt()}`;
    this.onItemRender = ::this.onItemRenderHandler;
  }

  componentDidMount() {
    setInterval(this.onItemRender, TIMEOUT_MS);
  }

  onItemRenderHandler() {
    if (this.nOfIterations++ >= MAX_ITERATIONS) return clearInterval(this.intervalId);

    const item = jq.wrap(`#${this.id}`);
    if (!item.isValid() || item.offsetHeight !== item.scrollHeight) return;
    item.css('display', 'inline');
    const postfix = jq.wrap(`#${this.postfixId}`);
    if (!postfix.isValid()) return;
    postfix.css('display', 'inline');
    clearInterval(this.intervalId);
  }

  render() {
    const { target, followingUsers } = this.props;
    const pretext = getPretext(target);
    const count = this.getCounter();

    return (
      <div className='heading-content'>
        <div id={this.wrapperId}>
          <h4 className='description' id={this.id}>
            { this.getUsersText() }
            { <span>{ (followingUsers.length > 0 && count > 0) ? ' and ' : '' }</span> }
            { this.getCounterText(count) }
          </h4>
          <span className='feed-dock-postfix' id={this.postfixId}> docked {pretext} {target}</span>
        </div>
      </div>
    );
  }

  getUsersText() {
    const { followingUsers } = this.props;
    return followingUsers.map((user, idx) =>
      <span key={user.id}>
        <UserLink user={user} />
        { idx === followingUsers.length - 1 ? '' : ', ' }
      </span>
    );
  }

  getCounterText(count) {
    const { target, item, followingUsers } = this.props;
    const dockData = {
      type: `${target}s`,
      event: item,
      text: followingUsers.length > 0 ? `${count} other${count > 1 ? 's' : ''}` : `${count} people`,
      outputType: OUTPUT_TYPES.TEXT_ONLY,
    };

    return count > 0 ? <DockAction data={dockData} /> : null;
  }

  getCounter() {
    const { item, followingUsers } = this.props;
    const count = get(item, 'decorated.intents.docked_count', 0);
    return count - followingUsers.length >= 0 ? count - followingUsers.length : count;
  }
}

FeedDockHeadContentText.propTypes = {
  item: PropTypes.object.isRequired,
  target: PropTypes.string.isRequired,
  followingUsers: PropTypes.array.isRequired,
};

export default FeedDockHeadContentText;
