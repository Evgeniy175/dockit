import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import moment from 'moment-timezone';

import { formatFromNow } from '../../../../../../utils/formatting/time';
import { POSTFIX_POSITIONS } from '../../../../../action-command/dock/constants';

import LikeAction from '../../../../../action-command/like/index.jsx';
import DockAction from '../../../../../action-command/dock/index.jsx';
import Comment from '../../../../../action-command/comment/index.jsx';



@observer
class FeedDockFooter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const target = this.props.target;
    const data = this.props.data;
    const feedItem = data[target];
    const dateBuff = moment(feedItem.created_at);
    const date = formatFromNow(dateBuff);
    const location = get(feedItem, 'data.location.description', '');

    const likeData = {
      type: `${target}s`,
      id: get(feedItem, 'id'),
      likes: get(feedItem, 'decorated.likes')
    };

    const dockData = {
      type: `${target}s`,
      event: feedItem,
    };

    const commentData = {
      type: `${target}s`,
      id: get(feedItem, 'id'),
      count: get(feedItem, 'decorated.comments.count', 0)
    };

    return (
      <div className='footer'>
        <div className='action-details'>
          <div className='location'>{ location }</div>
        </div>
        <div className='action-bar'>
          <LikeAction data={likeData} />
          <DockAction data={dockData} postfixPosition={POSTFIX_POSITIONS.RIGHT} />
          <Comment data={commentData} />
        </div>
        <div className='date'>
          { date }
        </div>
      </div>
    );
  }
}

FeedDockFooter.propTypes = {
  data: PropTypes.object.isRequired,
  target: PropTypes.string.isRequired
};

export default FeedDockFooter;
