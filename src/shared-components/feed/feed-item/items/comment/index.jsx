import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

import Head from './head/index.jsx';
import Content from './text/index.jsx';
import Footer from './footer/index.jsx';

import CommentModel from '../../../../../models/comments';

import jq from '../../../../../utils/jquery';
import { isPost } from '../../../../../utils/posts';

import { IGNORE_CLICK_SELECTORS } from './constants';
import { TARGETS } from '../../../../responsive-image/constants';



@observer
class FeedComment extends Component {
  itemIdResolvers = {
    [TARGETS.EVENT]: 'event_id',
    [TARGETS.SCHEDULE]: 'schedule_id',
    [TARGETS.USER]: 'user_id',
    [TARGETS.COMMENT]: 'comment_id',
  };

  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
    this.onReply = ::this.onReplyHandler;
  }

  componentWillUnmount() {
    this.onClick = null;
    this.onReply = null;
    this.itemIdResolvers = null;
  }

  onClickHandler(e) {
    const item = jq.wrap(e.target);
    if (IGNORE_CLICK_SELECTORS.some(selector => item.is(selector) || item.hasParents(selector))) return;
    const comment = this.props.comment.data;
    const { id } = comment;
    browserHistory.push(this.getUri(`commentId=${id}`));
  }

  onReplyHandler() {
    const comment = this.props.comment.data;
    const { username } = comment.user;
    browserHistory.push(this.getUri(`fromUser=${username}`));
  }

  getUri(query) {
    const comment = this.props.comment.data;
    const { id } = comment;
    const target = CommentModel.getTarget(comment);
    return isPost(comment) ? `/posts/${id}?${query}` : `/${target}s/${this.getTargetId(comment, target)}?${query}`;
  }

  getTargetId(comment, target) {
    return comment[this.itemIdResolvers[target]];
  }

  render() {
    const { comment, isUserFeed, onDelete } = this.props;
    return (
      <div className='feed-item-content comment-item-content'>
        <Head comment={comment} isUserFeed={isUserFeed} onClick={this.onClick} onDelete={onDelete} />
        <Content comment={comment.data} onClick={this.onClick} />
        <Footer comment={comment} onClick={this.onClick} onReply={this.onReply} />
      </div>
    );
  }
}

FeedComment.propTypes = {
  comment: PropTypes.object.isRequired,
  isUserFeed: PropTypes.bool,
  onDelete: PropTypes.func.isRequired,
};

export default FeedComment;
