import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';

import LikeAction from '../../../../../action-command/like/index.jsx';
import ReplyAction from '../../../../../action-command/reply/index.jsx';

import { formatFromNow } from '../../../../../../utils/formatting/time';



@observer
class FeedCommentFooter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { comment, onClick, onReply } = this.props;
    const dateBuff = moment(comment.data.created_at);
    const date = formatFromNow(dateBuff);
    
    const likeData = {
      type: 'comments',
      id: comment.data.id,
      likes: comment.data.decorated.likes
    };
    
    return (
      <div className='footer' onClick={onClick}>
        <div className='action-bar'>
          <ReplyAction comment={comment.data} onClick={onReply} />
          <LikeAction data={likeData} />
        </div>
        <div className='date'>{ date }</div>
      </div>
    );
  }
}

FeedCommentFooter.propTypes = {
  comment: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  onReply: PropTypes.func,
};

export default FeedCommentFooter;
