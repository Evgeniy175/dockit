import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { action, observable, computed } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Image from '../../../../../image/index.jsx';
import Content from './content/index.jsx';
import Menu from '../../../../../comment/top/menu/index.jsx';

import { CREATOR_VALUES } from './constants';



@observer
class FeedCommentHead extends Component {
  @observable comment;

  @computed
  get user() {
    return this.comment.data.user;
  }

  constructor(props) {
    super(props);
    const { onDelete } = props;
    this.setComment(props.comment);
    this.creatorHandlers = {
      onClick: ::this.onImageClickHandler,
    };
    this.menuHandlers = {
      onDelete,
    };
  }

  componentWillUnmount() {
    this.creatorHandlers = null;
    this.setComment(null);
  }

  @action
  setComment(value) {
    this.comment = value;
  }

  onImageClickHandler() {
    browserHistory.push(`/users/${this.user.id}`);
  }

  render() {
    const { isUserFeed, onClick } = this.props;
    const menuValues = {
      comment: this.comment.data,
      forceEnableDeleteAction: isUserFeed,
    };
    return (
      <div className='heading' onClick={onClick}>
        <Image values={CREATOR_VALUES(this.user)} handlers={this.creatorHandlers} />
        <Content user={this.user} comment={this.comment} />
        <Menu values={menuValues} handlers={this.menuHandlers} />
      </div>
    );
  }
}

FeedCommentHead.propTypes = {
  comment: PropTypes.object.isRequired,
  isUserFeed: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default FeedCommentHead;
