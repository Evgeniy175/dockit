import React, { Component } from 'react';
import { Link } from 'react-router';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import CommentModel from '../../../../../../../models/comments';

import jq from '../../../../../../../utils/jquery';

import { getRandomInt } from '../../../../../../../utils/random';

import { MAX_ITERATIONS, TIMEOUT_MS } from './constants';



@observer
class FeedCommentHeadContent extends Component {
  nOfIterations = 0;
  intervalId;

  constructor(props) {
    super(props);
    this.id = `creatorId${getRandomInt()}`;
    this.wrapperId = `wrapperId${getRandomInt()}`;
    this.onItemRender = ::this.onItemRenderHandler;
  }

  componentDidMount() {
    setInterval(this.onItemRender, TIMEOUT_MS);
  }

  componentWillUnmount() {
    this.nOfIterations = null;
    clearInterval(this.intervalId);
    this.intervalId = null;
    this.id = null;
    this.wrapperId = null;
    this.onItemRender = null;
  }

  onItemRenderHandler() {
    if (this.nOfIterations++ >= MAX_ITERATIONS) return clearInterval(this.intervalId);

    const item = jq.wrap(`#${this.id}`);
    if (!item.isValid() || item.clientHeight !== item.scrollHeight) return;
    item.css('display', 'inline');
    jq.wrap(`#${this.wrapperId}`).addClass('three-lines');
    clearInterval(this.intervalId);
  }

  render() {
    const comment = this.props.comment;
    const user = this.props.user;
    const target = CommentModel.getTarget(comment.data);
    const commentedItem = comment.data[target];
    const title = comment.data[target].title;
    const name = user.username;
    const itemUrl = `/${target}s/${commentedItem.id}`;
    const userUrl = `/users/${user.id}`;
    
    return (
      <div className='heading-content'>
        <h4 className='comment-description' id={this.wrapperId}>
          <Link to={userUrl} className='comment-creator' id={this.id}>
            {name}
          </Link>
          {
            title &&
            <span>
              <span> commented on </span>
              <Link to={itemUrl} className='item-name'>
                {title}
              </Link>
            </span>
          }
        </h4>
      </div>
    );
  }
}

FeedCommentHeadContent.propTypes = {
  user: PropTypes.object.isRequired,
  comment: PropTypes.object.isRequired
};

export default FeedCommentHeadContent;
