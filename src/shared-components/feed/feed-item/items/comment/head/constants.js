import UsersModel from '../../../../../../models/users';

import { ASPECT_RATIOS } from '../../../../../image/constants';
import {MIN_RESOLUTION} from '../../../../../../utils/images/resolution/constants';

export const CREATOR_VALUES = user => {
  if (!user) return {};
  return {
    src: UsersModel.formatImageUrl(user.profilePicture, MIN_RESOLUTION),
    wrapperClassName: 'thumbnail-container',
    aspectRatio: ASPECT_RATIOS.s1x1,
  };
};
