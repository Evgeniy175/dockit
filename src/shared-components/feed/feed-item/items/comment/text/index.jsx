import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import ExpandableText from '../../../../../expandable-text/index.jsx';
import LightboxWrapper from '../../../../../lightbox-wrapper/index.jsx';
import Image from '../../../../../image/index.jsx';
import Youtube from '../../../../../attached-youtube/index.jsx';

import { isContainsYoutubeLinks } from '../../../../../../utils/formatting/text';

import DataState from './state/data';

import { IMAGE_VALUES } from './constants';
import { TARGETS } from '../../../../../responsive-image/constants';



@observer
class FeedCommentContent extends Component {
  itemIdResolvers = {
    [TARGETS.EVENT]: 'event_id',
    [TARGETS.SCHEDULE]: 'schedule_id',
    [TARGETS.USER]: 'user_id',
    [TARGETS.COMMENT]: 'comment_id',
  };

  constructor(props) {
    super(props);
    const { onClick } = props;
    Object.assign(this, {
      data: new DataState(),
    });

    this.data.setComment(toJS(props.comment));

    this.onImageOpen = ::this.onImageOpenHandler;
    this.onImageClose = ::this.onImageCloseHandler;
    this.imageHandlers = {
      onClick,
    };
  }

  componentWillUnmount() {
    this.data.clear();
    this.clear();
  }

  clear() {
    this.onImageOpen = null;
    this.onImageClose = null;
    this.imageHandlers = null;
    this.data = null;
    this.itemIdResolvers = null;
  }

  onImageOpenHandler() {
    this.data.setIsOpened(true);
  }

  onImageCloseHandler() {
    this.data.setIsOpened(false);
  }

  render() {
    const { onClick } = this.props;
    const isEmpty = this.data.isMessageEmpty();
    const comment = this.data.comment || {};
    const message = toJS(comment.message);
    const userTags = toJS(comment.userTags);
    const imageUrl = this.data.getFormattedImageUrl();
    const isContainsVideoLinks = isContainsYoutubeLinks(message);
    const lightboxImages = [{ src: imageUrl }];
    const commentItem = isEmpty ? null : <ExpandableText className={imageUrl || isContainsVideoLinks ? '' : 'no-image'} text={toJS(message)} usertags={toJS(userTags)} isNeedToHandle={true} />;

    return (
      <div className='content' onClick={onClick}>
        { imageUrl && <LightboxWrapper open={this.data.isOpened} images={lightboxImages} onClose={this.onImageClose} />}
        { imageUrl && <Image values={IMAGE_VALUES(get(this.data, 'comment'))} handlers={this.imageHandlers} /> }
        { !imageUrl && !isEmpty && <Youtube text={this.data.getMessage()} /> }
        { commentItem }
      </div>
    );
  }
}

FeedCommentContent.propTypes = {
  comment: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default FeedCommentContent;
