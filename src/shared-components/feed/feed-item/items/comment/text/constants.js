import CommentsModel from '../../../../../../models/comments';

import {ASPECT_RATIOS} from '../../../../../image/constants';
import {DEFAULT_RESOLUTION} from '../../../../../../utils/images/resolution/constants';

export const IMAGE_VALUES = item => {
  if (!item) return {};
  return {
    src: CommentsModel.formatImageUrl(item.imageUrl, DEFAULT_RESOLUTION),
    wrapperClassName: 'image-container',
    aspectRatio: ASPECT_RATIOS.s16x9,
    prepareWrapper: true,
  };
};
