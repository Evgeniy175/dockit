import { action, observable, computed, toJS } from 'mobx';
import { get } from 'lodash';

import { EMPTY_CHAR } from '../../../../../../../constants';

import DataState from '../../../../../../../utils/state/data';

import CommentModel from '../../../../../../../models/comments/index';



class FeedCommentContentDataState extends DataState {
  @observable comment = {};
  @observable isOpened = false;

  constructor(config = {}) {
    super(config);
  }
  
  getFormattedImageUrl() {
    return CommentModel.formatImageUrl(this.comment.imageUrl);
  }

  isMessageEmpty() {
    const message = this.getMessage();
    return !message || message === EMPTY_CHAR;
  }

  getMessage() {
    return get(this.comment, 'message');
  }
  
  @action
  setComment(value) {
    this.comment = value;
  }

  @action
  setIsOpened(value) {
    this.isOpened = value;
  }

  @action
  clear() {
    this.comment = null;
    this.isOpened = null;
  }
}

export default FeedCommentContentDataState;
