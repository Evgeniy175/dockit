import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import CommentItem from './items/comment/index.jsx';
import EventItem from './items/event/index.jsx';
import ScheduleItem from './items/schedule/index.jsx';
import DockItem from './items/dock/index.jsx';

import { TYPES } from './constants';



@observer
class FeedItem extends Component {
  constructor(props) {
    super(props);

    this.renderHandlers = {
      [TYPES.COMMENT]: ::this.renderCommentHandler,
      [TYPES.INTENT]: ::this.renderDockHandler,
      [TYPES.EVENT]: ::this.renderEventHandler,
      [TYPES.SCHEDULE]: ::this.renderScheduleHandler,
    };
  }

  componentWillUnmount() {
    this.renderHandlers = null;
  }

  renderCommentHandler(item) {
    const { isUserFeed } = this.props.values;
    const { onDelete } = this.props.handlers;
    return <CommentItem comment={item} isUserFeed={isUserFeed} onDelete={onDelete} />;
  }

  renderDockHandler(item) {
    return <DockItem dock={item} />;
  }

  renderEventHandler(item) {
    const { onDelete } = this.props.handlers;
    return <EventItem event={item} onDelete={onDelete} />;
  }

  renderScheduleHandler(item) {
    const { onDelete } = this.props.handlers;
    return <ScheduleItem schedule={item} onDelete={onDelete} />;
  }

  render() {
    const { item } = this.props.values;
    if (!this.renderHandlers[item.type]) { console.error(`Can't resolve feed item.`); return null; }
    return (
      <div className='feed'>
        { this.renderHandlers[item.type](item) }
      </div>
    );
  }
}

FeedItem.propTypes = {
  values: PropTypes.shape({
    item: PropTypes.object.isRequired,
    isUserFeed: PropTypes.bool,
  }),
  handlers: PropTypes.shape({
    onDelete: PropTypes.func,
  }),
};

export default FeedItem;
