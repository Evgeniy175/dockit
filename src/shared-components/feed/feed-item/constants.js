export const TYPES = {
  EVENT: 'event',
  SCHEDULE: 'schedule',
  COMMENT: 'comment',
  INTENT: 'intent',
};
