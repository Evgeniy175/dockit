import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action, toJS } from 'mobx';
import PropTypes from 'prop-types';
import Lightbox from 'react-images';
import { get } from 'lodash';

import ImageList from './gallery/index.jsx';

import jq from '../../utils/jquery';



@observer
class ImageGallery extends Component {
  @observable isOpen = false;
  @observable currentImageIdx = 0;
  
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onOpen = ::this.onOpenHandler;
    this.onPrevClick = ::this.onPrevClickHandler;
    this.onNextClick = ::this.onNextClickHandler;
    this.onClose = ::this.onCloseHandler;
  }

  componentDidMount() {
    jq.wrapMany('.images-list').each(item => {
      item.children('img').each(imgItem => {
        item.css('height', imgItem.width() / 3);
      });
    });
  }
  
  onOpenHandler(idx) {
    this.setCurrentImage(idx);
    this.setIsOpen(true);
  }
  
  onPrevClickHandler() {
    this.setCurrentImage(this.currentImageIdx - 1);
  }
  
  onNextClickHandler() {
    this.setCurrentImage(this.currentImageIdx + 1);
  }
  
  onCloseHandler() {
    this.setIsOpen(false);
    this.setCurrentImage(0);
  }
  
  @action
  setIsOpen(value) {
    this.isOpen = value;
  }
  
  @action
  setCurrentImage(value) {
    this.currentImageIdx = value;
  }

  render() {
    const { images, lightBoxImages, previewEnabled } = this.props;

    if (!get(images, 'length') || !get(lightBoxImages, 'length') || images.length !== lightBoxImages.length) return null;

    return (
      <div className='images-gallery'>
        { previewEnabled && <ImageList images={images} onOpen={this.onOpen} /> }
        <Lightbox images={lightBoxImages}
                  currentImage={this.currentImageIdx}
                  backdropClosesModal={true}
                  isOpen={this.isOpen}
                  onClickPrev={this.onPrevClick}
                  onClickNext={this.onNextClick}
                  onClose={this.onClose} />
      </div>
    );
  }
}

ImageGallery.propTypes = {
  images: PropTypes.array.isRequired,
  lightBoxImages: PropTypes.array.isRequired,
  previewEnabled: PropTypes.bool
};

ImageGallery.defaultProps = {
  previewEnabled: true
};

export default ImageGallery;
