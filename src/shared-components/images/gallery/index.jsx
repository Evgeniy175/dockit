import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action, toJS } from 'mobx';
import PropTypes from 'prop-types';

import Image from '../../image/index.jsx';

import { IMAGE_VALUES } from './constants';



@observer
class ImageGalleryList extends Component {
  constructor(props) {
    super(props);
  }
  
  openLightboxHandler(idx, e) {
    e.preventDefault();
    this.props.onOpen(idx);
  }

  render() {
    const { images } = this.props;
    
    return (
      <div className='images-list'>
        {
          images.map((image, idx) => {
            const handlers = {
              onClick: this.openLightboxHandler.bind(this, idx),
            };
            return <Image key={`prev_${image.src}`} values={IMAGE_VALUES(image.src, image.target)} handlers={handlers} />;
          })
        }
      </div>
    );
  }
}

ImageGalleryList.propTypes = {
  images: PropTypes.array.isRequired,
  onOpen: PropTypes.func.isRequired
};

export default ImageGalleryList;
