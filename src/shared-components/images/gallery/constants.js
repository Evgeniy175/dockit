import EventsModel from '../../../models/events';
import SchedulesModel from '../../../models/schedules';
import CommentsModel from '../../../models/comments';
import UsersModel from '../../../models/users';


import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../../models/events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../../models/schedules/constants';
import { MODEL_PLURAL as COMMENTS_MODEL_PLURAL } from '../../../models/comments/constants';
import { MODEL_PLURAL as USERS_MODEL_PLURAL } from '../../../models/users/constants';

const GET_SRC = (uri, type) => {
  const size = 720;
  if (type === EVENTS_MODEL_PLURAL) return EventsModel.formatImageUrl(uri, undefined, size);
  if (type === SCHEDULES_MODEL_PLURAL) return SchedulesModel.formatImageUrl(uri, undefined, size);
  if (type === COMMENTS_MODEL_PLURAL) return CommentsModel.formatImageUrl(uri, size);
  if (type === USERS_MODEL_PLURAL) return UsersModel.formatImageUrl(uri, size);
};

import { ASPECT_RATIOS } from '../../image/constants';

export const IMAGE_VALUES = (uri, type) => {
  return {
    src: GET_SRC(uri, type),
    className: 'thumb',
    aspectRatio: ASPECT_RATIOS.s16x3,
  };
};
