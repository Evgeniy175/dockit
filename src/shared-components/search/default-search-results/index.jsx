import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';

@observer
class DefaultSearchResults extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className='default-search-results no-select'>
        <div>Search for friends, family,</div>
        <div>businesses, sports teams,</div>
        <div>organizations, restaurants, bars...</div>
        <div className='empty' />
        <div>Anything you can think of.</div>
      </div>
    );
  }
}

export default DefaultSearchResults;
