import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';

@observer
class EmptySearchResults extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className='default-search-results no-select'>
        No results
      </div>
    );
  }
}

export default EmptySearchResults;
