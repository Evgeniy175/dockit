import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class SettingsSectionHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return <div className='settings-section-header'>{ this.props.text }</div>;
  }
}

SettingsSectionHeader.propTypes = {
  text: PropTypes.string.isRequired
};

export default SettingsSectionHeader;
