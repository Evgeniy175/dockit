import { action, observable, computed, toJS } from 'mobx';

import moment from 'moment';
import Promise from 'bluebird';

import { VIRTUAL_EVENTS_DATE_FORMAT, EVENT_DATE_FORMAT, OFFSET, N_OF_SHOWN_ITEMS } from '../constants';

import DataState from '../../../../utils/state/data';

import SchedulesModel from '../../../../models/schedules';
import EventsModel from '../../../../models/events';

import { getSortedEvents } from '../../../../utils/sorting/upcoming';

const schedulesModel = new SchedulesModel();
const eventsModel = new EventsModel();



class ViewUpcomingDataState extends DataState {
  @observable upcomingItems = {};
  upcomingLength = 0;
  @observable nOfShownItems = N_OF_SHOWN_ITEMS;

  startTime = null;
  endTime = null;

  @action
  increaseNumberOfShownItems() {
    const expected = this.nOfShownItems + N_OF_SHOWN_ITEMS;
    this.nOfShownItems = expected <= this.upcomingLength ? expected : this.upcomingLength;
  }
  
  constructor(props = {}) {
    super(props);
  }
  
  fetchUpcomingItems(id, date) {
    const oneWeekBefore = moment().subtract(1, 'week').startOf('day');
    this.startTime = this.tryToResolveDate(date) ? moment(date).startOf('day') : moment().startOf('day');
    
    if (this.startTime.isBefore(oneWeekBefore)) this.startTime = oneWeekBefore;
    
    this.endTime = moment(this.startTime).add(OFFSET.COUNT, OFFSET.TYPE);
    
    return Promise.all([
      this.fetchEvents(id),
      this.fetchVirtualEvents(id)
    ])
    .spread((events, schedules) => {
      const sorted = getSortedEvents(events.data, schedules);
      this.setUpcomingItems(sorted);
    });
  }
  
  tryToResolveDate(dateString) {
    const time = moment(dateString, VIRTUAL_EVENTS_DATE_FORMAT);
    return time.isValid() ? time : undefined;
  }
  
  fetchEvents(id) {
    const time = [this.getFormattedStartTime(), this.getFormattedEndTime()];
    return id ? eventsModel.fetchDockedEventsForOtherUser(time, id) : eventsModel.fetchDockedEventsForUser(time);
  }
  
  fetchVirtualEvents(id) {
    return schedulesModel.fetchVirtualizedEvents(this.getScheduleFormattedStartTime(), this.getScheduleFormattedEndTime(), id);
  }
  
  getFormattedStartTime() {
    return this.startTime.format(EVENT_DATE_FORMAT);
  }
  
  getFormattedEndTime() {
    return this.endTime.format(EVENT_DATE_FORMAT);
  }
  
  getScheduleFormattedStartTime() {
    return this.startTime.format(VIRTUAL_EVENTS_DATE_FORMAT);
  }
  
  getScheduleFormattedEndTime() {
    return this.endTime.format(VIRTUAL_EVENTS_DATE_FORMAT);
  }
  
  @action
  setUpcomingItems(items) {
    this.upcomingItems = items;
    this.upcomingLength = this.getUpcomingLength();
  }

  getUpcomingLength() {
    const lengths = Object.keys(this.upcomingItems).map(key => this.upcomingItems[key].length);
    return lengths.reduce((a, b) => a + b, 0);
  }
}

export default ViewUpcomingDataState;
