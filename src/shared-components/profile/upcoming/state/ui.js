import { action, observable, toJS } from 'mobx';

import UIState from '../../../../utils/state/data';

import { BUFF_FROM_BOTTOM } from '../constants';

class ProfileUpcomingUiState extends UIState {
  constructor(props = {}) {
    super(props);
  }

  isNeedToLoadMore() {
    const container = this.getContainer();

    if (!container) return;

    const currentPercent = 100 - (container.scrollTop + container.clientHeight) / container.scrollHeight * 100;
    return currentPercent < BUFF_FROM_BOTTOM;
  }

  getContainer() {
    return document.getElementsByClassName('profile-upcoming')[0];
  }
}

export default ProfileUpcomingUiState;
