import { UPCOMING_KEY_FORMATTER, UPCOMING_KEY_FORMATTER_TEMP } from '../../../utils/sorting/constants';

export const OFFSET = {
  TYPE: 'month',
  COUNT: 1
};

export const DATE_FORMAT = UPCOMING_KEY_FORMATTER;
export const DATE_FORMAT_TEMP = UPCOMING_KEY_FORMATTER_TEMP;
export const TIME_FORMAT = 'LT';
export const VIRTUAL_EVENTS_DATE_FORMAT = 'YYYY-MM-DD';
export const EVENT_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss ZZ';
export const BUFF_FROM_BOTTOM = 20;
export const N_OF_SHOWN_ITEMS = 50;
