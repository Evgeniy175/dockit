import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import { Auth } from '../../../auth';

import Header from './header/index.jsx';
import Text from './text/index.jsx';
import Items from './items/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class ProfileUpcoming extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
      ui: new UiState(),
    });
    this.onWheel = ::this.onWheelHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
  }
  
  componentWillMount() {
    const { id } = this.props;
    this.data.fetchUpcomingItems(id);
  }

  componentDidMount() {
    const container = this.ui.getContainer();

    if (container) {
      container.addEventListener('mousewheel', this.onWheel);
      container.addEventListener('touchmove', this.onWheel);
      container.addEventListener('keydown', this.onKeyDown);
    }
  }
  
  componentWillUpdate(nextProps) {
    const { id, isNeedToUpdate } = nextProps;
    
    if (isNeedToUpdate) {
      this.data.fetchUpcomingItems(id);
      this.props.onUpdate();
    }
  }

  componentWillUnmount() {
    const container = this.ui.getContainer();

    if (container) {
      container.removeEventListener('keydown', this.onKeyDown);
      container.removeEventListener('touchmove', this.onWheel);
      container.removeEventListener('mousewheel', this.onWheel);
    }
  }

  onWheelHandler() {
    if (!this.ui.isNeedToLoadMore() || this.data.nOfShownItems >= this.upcomingLength) return;
    this.data.increaseNumberOfShownItems();
  }

  onKeyDownHandler(e) {
    const isModalTarget = e.target.className.includes('profile-upcoming');
    return isModalTarget ? null : handleButton(e, this.getContainer, this.onButtonScroll);
  }
  
  render() {
    const { upcomingItems, nOfShownItems } = this.data;
    //const isExists = upcomingItems && !isEmpty(upcomingItems);
    const user = Auth.getActiveUser();
    
    if (user.isUpcomingDisabled) return null;
    
    return (
      <Row>
        <Col xs={3} className='profile-upcoming' >
          <Header />
          <Text />
          <Items upcomingItems={upcomingItems} nOfShownItems={nOfShownItems} />
        </Col>
      </Row>
    );
  }
}

ProfileUpcoming.propTypes = {
  id: PropTypes.string,
  needToUpdate: PropTypes.bool,
  onUpdate: PropTypes.func
};

ProfileUpcoming.defaultProps = {
  needToUpdate: false,
  onUpdate: () => {}
};

export default ProfileUpcoming;
