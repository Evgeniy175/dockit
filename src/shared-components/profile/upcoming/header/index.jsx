import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';

@observer
class ProfileUpcomingHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Row className='header'>
        <Col xs={12}>
          Upcoming
        </Col>
      </Row>
    );
  }
}

export default ProfileUpcomingHeader;
