import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import moment from 'moment';

import { DATE_FORMAT } from '../../constants';

@observer
class ProfileUpcomingDayHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const currentDay = moment().format(DATE_FORMAT);
    const day = this.props.day;
    const header = (day == currentDay ? 'Today' : day).toUpperCase();
    
    return (
      <h2 className='day-header'>
        <span>
          { header }
        </span>
      </h2>
    );
  }
}

ProfileUpcomingDayHeader.propTypes = {
  day: PropTypes.string.isRequired
};

export default ProfileUpcomingDayHeader;
