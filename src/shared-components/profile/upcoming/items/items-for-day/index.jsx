import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Item from './item/index.jsx';

@observer
class ProfileUpcomingItemsForDay extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { itemsForDay, nOfShownItems } = this.props;
    let { count } = this.props;
    const lastIdx = itemsForDay.length - 1;
    
    return (
      <div className='items-for-day'>
        {
          itemsForDay.map((item, idx) => {
            count++;
            return count <= nOfShownItems && <Item key={`${item.id}_${item.title}`} item={item} isLast={idx == lastIdx} />;
          })
        }
      </div>
    );
  }
}

ProfileUpcomingItemsForDay.propTypes = {
  itemsForDay: PropTypes.object.isRequired,
  nOfShownItems: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
};

export default ProfileUpcomingItemsForDay;
