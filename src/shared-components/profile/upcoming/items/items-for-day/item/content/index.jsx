import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Row from './row/index.jsx';



@observer
class ProfileUpcomingItemContent extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const item = this.props.item;
    const title = item.title;
    const location = item.location;
    
    return (
      <OverlayTrigger placement='top' overlay={ this.getTooltip(title, location) }>
        <span className='content'>
          <Row text={title} className='title' />
          <Row text={location} className='location' />
        </span>
      </OverlayTrigger>
    );
  }
  
  getTooltip(title, location) {
    return (
      <Tooltip placement='bottom' className='in' id='tooltip-top'>
        <div className='title'>{ title }</div>
        <div>{ location }</div>
      </Tooltip>
    );
  }
}

ProfileUpcomingItemContent.propTypes = {
  item: PropTypes.object.isRequired
};

export default ProfileUpcomingItemContent;
