import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class ProfileUpcomingItemContentRow extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { text, className } = this.props;
    
    if (!text) return null;
    
    return (
      <div className={className}>
        { text }
      </div>
    );
  }
}

ProfileUpcomingItemContentRow.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string.isRequired
};

ProfileUpcomingItemContentRow.defaultProps = {
  text: ''
};

export default ProfileUpcomingItemContentRow;
