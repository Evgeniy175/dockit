import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

const CLASS_NAME_FROM_INTENT = {
  'going': 'going',
  'not_going': 'not-going',
  'maybe': 'maybe'
};

const TEXT_FROM_INTENT = {
  'going': 'Going',
  'not_going': 'Not going',
  'maybe': 'Maybe'
};

@observer
class ProfileUpcomingItemStatus extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const key = this.recognizeKey();
    const text = TEXT_FROM_INTENT[key];
    const className = CLASS_NAME_FROM_INTENT[key];
    
    return (
      <div className='status'>
        <span className={className}>
          { text }
        </span>
      </div>
    );
  }
  
  recognizeKey() {
    const intent = this.props.intent;
    return intent ? intent.intent : 'maybe';
  }
}

ProfileUpcomingItemStatus.propTypes = {
  intent: PropTypes.object
};

export default ProfileUpcomingItemStatus;
