import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import moment from 'moment-timezone';

import { TIME_FORMAT } from '../../../../constants';

import Status from './status/index.jsx';

@observer
class ProfileUpcomingItemRightContent extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const item = this.props.item;
    const startBuff = moment(item.date);
    const endBuff = moment(item.endTime);
    const start = moment().set({ h: startBuff.hours(), m: startBuff.minutes(), });
    const end = moment().set({ h: endBuff.hours(), m: endBuff.minutes(), });
    const isSameTime = end.isSame(start);
    const intent = item.myIntent;
    const isLocationExists = !!item.location;
    const containerClassName = `right-content${isSameTime ? ' padding-top' : ''}${isSameTime && isLocationExists ? '-x2' : ''}`;

    return (
      <span className={containerClassName}>
        <div className='time'>
          <div className='start'>{ start.format(TIME_FORMAT) }</div>
          { !isSameTime && <div className='end'>{ end.format(TIME_FORMAT) }</div> }
        </div>
        {
          /*
          temporary removed
          from https://trello.com/c/bo1nsLVQ/56-profile-upcoming-changes-checklist
           <Status intent={intent} />
           */
        }
      </span>
    );
  }
}

ProfileUpcomingItemRightContent.propTypes = {
  item: PropTypes.object.isRequired
};

export default ProfileUpcomingItemRightContent;
