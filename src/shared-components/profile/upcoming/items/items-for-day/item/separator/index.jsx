import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';


@observer
class ProfileUpcomingItemSeparator extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className='separator' />
    );
  }
}

export default ProfileUpcomingItemSeparator;
