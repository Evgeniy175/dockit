import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

import { UPCOMING_ITEMS_TYPES } from '../../../../../../utils/sorting/constants';

import EventModel from '../../../../../../models/events/index';

import Image from './image/index.jsx';
import Content from './content/index.jsx';
import RightContent from './right-content/index.jsx';
import Separator from './separator/index.jsx';

const eventModel = new EventModel();

@observer
class ProfileUpcomingItem extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const item = this.props.item;
    
    return item.type === UPCOMING_ITEMS_TYPES.EVENT
    ? this.redirectToEvent(item.id)
    : this.redirectToEventFromSchedule(item);
  }
  
  redirectToEvent(id) {
    browserHistory.push(`/events/${id}`);
  }
  
  redirectToEventFromSchedule(item) {
    const config = {
      body: JSON.stringify({ date: item.date.format() })
    };
    
    return eventModel.fromSchedule(item.id, config)
    .then(res => browserHistory.push(`/events/${res.id}`))
    .catch(console.error);
  }
  
  render() {
    const { item, isLast } = this.props;
    
    return (
      <div className='item-wrapper'>
        <div className='item' onClick={this.onClick}>
          <Image src={item.image} />
          <Content item={item} />
          <RightContent item={item} />
        </div>
        {
          !isLast && <Separator />
        }
      </div>
    );
  }
}

ProfileUpcomingItem.propTypes = {
  item: PropTypes.object.isRequired,
  isLast: PropTypes.bool.isRequired
};

export default ProfileUpcomingItem;
