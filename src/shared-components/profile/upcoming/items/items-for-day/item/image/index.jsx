import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import DefaultImage from '../../../../../../../assets/images/icons/event-default.svg';

@observer
class ProfileUpcomingItemImage extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const src = this.props.src;
    
    return (
      <span className='image'>
        <img src={src ? src : DefaultImage} />
      </span>
    );
  }
}

ProfileUpcomingItemImage.propTypes = {
  src: PropTypes.string
};

export default ProfileUpcomingItemImage;
