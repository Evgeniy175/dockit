import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import moment from 'moment';

import DayHeader from './day-header/index.jsx';
import ItemsForDay from './items-for-day/index.jsx';

import { DATE_FORMAT, DATE_FORMAT_TEMP } from '../constants';

@observer
class ProfileUpcomingItems extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { upcomingItems, nOfShownItems } = this.props;
    let currentCount = 0;
    let prevKey = null;
    
    let keys = Object.keys(upcomingItems);
    keys.sort((left, right) => moment.utc(left, DATE_FORMAT_TEMP).diff(moment.utc(right, DATE_FORMAT_TEMP)));

    return (
      <Row className='items'>
        <Col>
          {
            keys.map(key => {
              currentCount += prevKey ? upcomingItems[prevKey].length : 0;
              prevKey = key;

              return currentCount <= nOfShownItems && [
                <DayHeader day={moment(key, DATE_FORMAT_TEMP).format(DATE_FORMAT)} />,
                <ItemsForDay itemsForDay={upcomingItems[key]} nOfShownItems={nOfShownItems} count={currentCount} />
              ];
            })
          }
        </Col>
      </Row>
    );
  }
}

ProfileUpcomingItems.propTypes = {
  upcomingItems: PropTypes.object.isRequired,
  nOfShownItems: PropTypes.number.isRequired,
};

export default ProfileUpcomingItems;
