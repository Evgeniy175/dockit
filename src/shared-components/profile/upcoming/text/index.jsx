import React, { Component } from 'react';
import { observer } from 'mobx-react';

@observer
class ProfileUpcomingText extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div className='text'>
        <div>This shows items that you</div>
        <div>have docked or created.</div>
        <div className='bold'>Only you can see this.</div>
      </div>
    );
  }
}

export default ProfileUpcomingText;
