import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

@observer
class ImageDropdownListItem extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    this.props.item.action();
  }
  
  render() {
    const item = this.props.item;
    
    return (
      <div className='dropdown-list-item' onClick={this.onClick}>
        {item.title}
      </div>
    );
  }
}

ImageDropdownListItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default ImageDropdownListItem;
