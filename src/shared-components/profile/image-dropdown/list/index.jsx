import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import DropdownItem from './item/index.jsx';

@observer
class ImageDropdownList extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const isDropdownExpanded = this.props.expanded;
    const menuItems = this.props.menuItems;
    const className = this.props.className;
    
    if (!isDropdownExpanded) return null;
    
    return (
      <div id={className}>
        <div className='dropdown-list-item-container'>
          {
            menuItems.map(item => <DropdownItem key={item.title} item={item} />)
          }
        </div>
      </div>
    );
  }
}

ImageDropdownList.propTypes = {
  expanded: PropTypes.bool,
  menuItems: PropTypes.array,
  className: PropTypes.string
};

ImageDropdownList.defaultProps = {
  menuItems: [],
  className: ''
};

export default ImageDropdownList;
