import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Svg from 'react-svg';

import List from './list/index.jsx';

import UploadIcon from '../../../assets/images/icons/upload-image-large.svg';

import { PARENT_ELEMENT_ID } from './constants';
import { MENU_ITEMS_TITLES } from '../../background-image/constants';



@observer
class ImageDropdown extends Component {
  @observable isGalleryOpen = false;
  @observable isDropdownExpanded = false;
  
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onImageClick = ::this.onImageClickHandler;
    this.onBodyClick = ::this.onBodyClickHandler;
  }
  
  onImageClickHandler() {
    const { isDefaultImage, menuItems } = this.props;
    const updateMenuItem = menuItems.find(item => item.title === MENU_ITEMS_TITLES.UPDATE);

    if (isDefaultImage) {
      updateMenuItem.action();
      return;
    }
    
    document.body.addEventListener('click', this.onBodyClick, false);
    this.setDropdownExpanded(true);
  }
  
  onBodyClickHandler(e) {
    let element = e.target;
    let isNeedToClose = element.id !== PARENT_ELEMENT_ID;
    
    while (isNeedToClose && element && element.parentElement) {
      element = element.parentElement;
      isNeedToClose = element.id !== PARENT_ELEMENT_ID;
    }
    
    if (isNeedToClose) this.doClose();
  }
  
  doClose() {
    this.setDropdownExpanded(false);
    document.body.removeEventListener('click', this.onBodyClick, false);
  }
  
  @action
  setDropdownExpanded(value) {
    this.isDropdownExpanded = value;
  }
  
  render() {
    const { currentUserProfile, isDefaultImage, menuItems } = this.props;

    if (!currentUserProfile) return null;
  
    return (
      <div className='action-block'>
        <span onClick={this.onImageClick}>
          <Svg className='action-icon no-select' path={UploadIcon} />
        </span>
        { !isDefaultImage && <List expanded={this.isDropdownExpanded} menuItems={menuItems} className={PARENT_ELEMENT_ID} /> }
      </div>
    );
  }
}

ImageDropdown.propTypes = {
  currentUserProfile: PropTypes.bool,
  menuItems: PropTypes.array,
  isDefaultImage: PropTypes.bool
};

ImageDropdown.defaultProps = {
  currentUserProfile: false,
  menuItems: [],
  parentClassName: ''
};

export default ImageDropdown;
