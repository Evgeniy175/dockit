import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import Select from 'react-select';
import PropTypes from 'prop-types';



@observer
class SelectDropdown extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.inputChangeHandler = ::this.onInputChange;
  }
  
  onInputChange(value) {
    if (this.props.inputChangeHandler) this.props.inputChangeHandler(value);
  }
  
  isValueExists() {
    return this.props.value && (this.props.value.label || this.props.value.length > 0)
  }
  
  render() {
    const value = this.isValueExists() ? { label: this.props.value.label } : null;
    const { values, placeholder, multi, clearable, searchable, error, changeHandler } = this.props;
    const className = `select-dropdown${error ? ' error' : ''}`;
    return (
      <div className={className}>
        <Select value={value}
                options={values}

                onBlurResetsInput={false}
                onCloseResetsInput={false}
                onSelectResetsInput={false}

                backspaceRemoves={false}
                deleteRemoves={false}
                escapeClearsValue={false}

                placeholder={placeholder}
                multi={multi}
                clearable={clearable}
                searchable={searchable}
                onChange={changeHandler}
                onInputChange={this.inputChangeHandler} />
      </div>
    );
  }
}

SelectDropdown.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]),
  values: PropTypes.array.isRequired,
  changeHandler: PropTypes.func.isRequired,
  inputChangeHandler: PropTypes.func,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  multi: PropTypes.bool,
  clearable: PropTypes.bool,
  searchable: PropTypes.bool
};

export default SelectDropdown;
