import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Checkbox extends Component {
  toggleCheckboxChange = () => {
    const { handleCheckboxChange, label } = this.props;
    
    this.setState(({ isChecked }) => (
      {
        isChecked: !isChecked,
      }
    ));
    
    handleCheckboxChange(label);
  };
  
  render() {
    const { label } = this.props;
    const { isChecked } = this.props;
    
    return (
      <div className="checkbox">
        <label>
          <input type="checkbox" value={label} checked={isChecked} onChange={this.toggleCheckboxChange} />
          {label}
        </label>
      </div>
    );
  }
}

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  handleCheckboxChange: PropTypes.func.isRequired,
  isChecked: PropTypes.bool.isRequired
};

export default Checkbox;