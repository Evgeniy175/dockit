import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import ReactTooltip from 'react-tooltip';



@observer
class TopTooltip extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { id, content } = this.props;

    return (
      <ReactTooltip id={id} class='dockit-top-tooltip' type='warning' effect='solid'>
        { content }
      </ReactTooltip>
    );
  }
}

export default TopTooltip;
