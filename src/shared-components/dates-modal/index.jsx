import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import DateSelect from './date-select/index.jsx';
import DoneButton from '../modal/done-button/index.jsx';

import DataState from './state/data';



@observer
class DatesModal extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
  }
  
  componentWillMount() {
    this.onStartDateSelect = ::this.onStartDateSelectHandler;
    this.onEndDateSelect = ::this.onEndDateSelectHandler;
    this.onClose = ::this.onCloseHandler;
    this.onEndDateExpand = ::this.onEndDateExpandHandler;
    this.onClearAll = ::this.onClearAllHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  onStartDateSelectHandler(value) {
    this.data.setStartDate(value);

    const endDate = this.data.endDate;

    if (!value || !endDate) return;

    const minEndDate = this.data.startDate.clone().add(1, 'day');

    if (minEndDate.isAfter(endDate)) this.data.resetEndDate();
  }

  onEndDateSelectHandler(value) {
    const startDate = this.data.startDate;

    if (startDate.isBefore(value)) return this.data.setEndDate(value);

    this.data.resetEndDate();
  }

  onClickHandler(title) {
    this.data.onClick(title);
  }
  
  onCloseHandler() {
    const handler = get(this.props, 'handlers.onClose');
    const startDate = this.data.startDate ? this.data.startDate.format('YYYY-MM-DD') : null;
    const endDate = this.data.endDate ? this.data.endDate.format('YYYY-MM-DD') : null;

    const selectedDates = {
      $and: []
    };

    if (startDate) selectedDates.$and.push({ $gte: startDate });
    if (endDate) selectedDates.$and.push({ $lte: endDate });
    
    this.data.setIsOpen(false);

    if (handler) handler(selectedDates);
  }

  onEndDateExpandHandler() {
    this.data.toggleEndDateExpand();
  }

  onClearAllHandler() {
    const handler = get(this.props, 'handlers.onClose');

    this.data.setIsOpen(false);

    if (handler) handler(null);

    this.data.setStartDate(null);
    this.data.setEndDate(null);
    this.data.setIsEndDateExpanded(false);
  }
  
  render() {
    const isOpen = this.data.isOpen;
    const title = this.data.title;

    const startDateValues = {
      label: 'FROM',
      value: toJS(this.data.startDate),
      expanded: true,
      clearAllButtonPresented: true
    };

    const endDateValues = {
      label: 'TO +',
      value: toJS(this.data.endDate),
      hoverable: true,
      expanded: toJS(this.data.isEndDateExpanded)
    };

    const startDateHandlers = {
      onChange: this.onStartDateSelect,
      onClearAll: this.onClearAll
    };

    const endDateHandlers = {
      onChange: this.onEndDateSelect,
      onExpandToggle: this.onEndDateExpand,
    };
    
    return (
      <Modal show={isOpen} onHide={this.onClose} id='dates-modal' className='no-select'>
        <Modal.Header>
          <Modal.Title id={title}>{ title }</Modal.Title>
          <DoneButton />
        </Modal.Header>
        <Modal.Body>
          <DateSelect values={startDateValues} handlers={startDateHandlers} />
          <DateSelect values={endDateValues} handlers={endDateHandlers} />
        </Modal.Body>
      </Modal>
    );
  }
}

DatesModal.propTypes = {
  data: PropTypes.object,
  handlers: PropTypes.object
};

DatesModal.defaultProps = {
  
};

export default DatesModal;
