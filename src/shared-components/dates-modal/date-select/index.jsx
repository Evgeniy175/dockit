import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import DataState from './state/data';

import CalendarDropdown from '../../calendar-dropdown/index.jsx';



@observer
class DatesModalItem extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
    this.onChange = ::this.onChangeHandler;
    this.onClearAll = ::this.onClearAllHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  onClickHandler() {
    const handler = get(this.props, 'handlers.onExpandToggle');

    if (handler) handler();
  }

  onChangeHandler(value) {
    const handler = get(this.props, 'handlers.onChange');

    if (handler) handler(value);
  }

  onClearAllHandler() {
    const handler = get(this.props, 'handlers.onClearAll');

    if (handler) handler();
  }
  
  render() {
    const label = this.data.label;
    const value = this.data.value && this.data.value.format();
    const { hoverable, expanded, clearAllButtonPresented } = this.props.values;
    const labelClassName = `date-filter-picker-label ${hoverable ? ' hoverable': ''}`;

    return (
      <div className='date-filter-picker'>
        <div className='menu-wrapper'>
          <span className={labelClassName} onClick={this.onClick}>
            { label }
          </span>
          {
            clearAllButtonPresented &&
            <span className='date-filter-clear-all-button' onClick={this.onClearAll}>
              Clear all
            </span>
          }
        </div>
        {
          expanded &&
          <span className='date-filter-picker-input'>
            <CalendarDropdown alwaysVisible={true} value={value} pattern={null} onChange={this.onChange} />
          </span>
        }
      </div>
    );
  }
}

DatesModalItem.propTypes = {
  values: PropTypes.object,
  handlers: PropTypes.object
};

DatesModalItem.defaultProps = {
  
};

export default DatesModalItem;
