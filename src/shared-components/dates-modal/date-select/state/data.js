import { action, computed, observable, toJS } from 'mobx';
import { get } from 'lodash';

import DataState from '../../../../utils/state/data';



class DatesModalItemDataState extends DataState {
  @observable label = '';
  @observable value = null;
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }
  
  init(config) {
    const data = get(config, 'values');
    this.setLabel(get(data, 'label'));
    this.setValue(get(data, 'value'));
  }
  
  @action
  setLabel(value) {
    this.label = value;
  }
  
  @action
  setValue(value) {
    this.value = value;
  }
}

export default DatesModalItemDataState;
