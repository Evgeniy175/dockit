import { action, computed, observable, toJS } from 'mobx';
import { get } from 'lodash';
import moment from 'moment';

import { DEFAULT_TITLE } from '../constants';

import DataState from '../../../utils/state/data';



class DatesModalDataState extends DataState {
  @observable startDate = moment();
  @observable endDate = null;
  @observable isEndDateExpanded = false;

  @observable isOpen = false;
  @observable title = '';
  
  constructor(config = {}) {
    super(config);
    this.init(config);
  }
  
  init(config) {
    const data = get(config, 'data');
    const startDate = get(data, 'selectedDates.$and[0].$gte');
    const endDate = get(data, 'selectedDates.$and[1].$lte');

    this.setIsOpen(get(data, 'isOpen', false));
    this.setTitle(get(data, 'title', DEFAULT_TITLE));

    if (startDate) this.setStartDate(startDate);
    if (endDate) { this.setEndDate(endDate); this.setIsEndDateExpanded(true); }
  }

  resetEndDate() {
    const value = this.startDate.clone().add(1, 'day');
    this.setEndDate(value);
  }

  @action
  onClick(title) {

  }
  
  @action
  setIsOpen(value) {
    this.isOpen = value;
  }

  @action
  setTitle(value) {
    this.title = value;
  }

  @action
  setStartDate(value) {
    this.startDate = value ? moment(value) : value;
  }

  @action
  setEndDate(value) {
    this.endDate = value ? moment(value) : value;
  }

  @action
  toggleEndDateExpand() {
    this.isEndDateExpanded = !this.isEndDateExpanded;

    if (toJS(this.isEndDateExpanded)) this.resetEndDate();
    if (!toJS(this.isEndDateExpanded)) this.setEndDate(null);
  }

  @action
  setIsEndDateExpanded(value) {
    this.isEndDateExpanded = value;
    if (!toJS(this.isEndDateExpanded)) this.setEndDate(null);
  }
}

export default DatesModalDataState;
