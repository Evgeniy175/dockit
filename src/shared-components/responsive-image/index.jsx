import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { WIDTHS, IMAGE_RESOLVERS, DEFAULT_IMAGES } from './constants';



@observer
class ResponsiveImage extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler(e) {
    const handler = this.props.onClick;
    if (handler) handler(e);
  }

  render() {
    const { className, id, src, target, defaultSrc } = this.props;
    const defaultImage = defaultSrc ? defaultSrc : get(DEFAULT_IMAGES, target);
    const srcSetData = IMAGE_RESOLVERS[target].getSrcSetData(src, WIDTHS);
    const srcUrl = srcSetData ? null : IMAGE_RESOLVERS[target].formatImageUrl(src, defaultImage);
    return <img className={className} id={id} src={srcUrl} srcSet={srcSetData} onClick={this.onClick} />;
  }
}

ResponsiveImage.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  src: PropTypes.string,
  target: PropTypes.string.isRequired,
  defaultSrc: PropTypes.string,
  onClick: PropTypes.func,
};

export default ResponsiveImage;