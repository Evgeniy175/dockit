import EventModel from '../../models/events';
import ScheduleModel from '../../models/schedules';
import CommentModel from '../../models/comments';
import UserModel from '../../models/users';

import DEFAULT_EVENT_SCHEDULE_PICTURE from '../../assets/images/default-images/event-background-large.jpg';
import DEFAULT_PROFILE_PICTURE from '../../assets/images/default-images/profile-picture.png';

export const TARGETS = {
  EVENT: 'event',
  SCHEDULE: 'schedule',
  COMMENT: 'comment',
  USER: 'user',
};

export const IMAGE_RESOLVERS = {
  [TARGETS.EVENT]: EventModel,
  [TARGETS.SCHEDULE]: ScheduleModel,
  [TARGETS.COMMENT]: CommentModel,
  [TARGETS.USER]: UserModel,
};

export const DEFAULT_IMAGES = {
  [TARGETS.EVENT]: DEFAULT_EVENT_SCHEDULE_PICTURE,
  [TARGETS.SCHEDULES]: DEFAULT_EVENT_SCHEDULE_PICTURE,
  [TARGETS.COMMENT]: null,
  [TARGETS.USER]: DEFAULT_PROFILE_PICTURE,
};

export const WIDTHS = [
  200,
  400,
  600,
  800,
  1000,
  1200
];
