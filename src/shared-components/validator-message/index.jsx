import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Modal from '../question-modal/index.jsx';

import { MODAL_TITLE } from './constants';
import { CSS_MAPPING } from '../button/constants';



@observer
class ValidatorMessage extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { text, open, onClose } = this.props;
    const modalButtons = [
      {
        text: 'Okay',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: onClose
      },
    ];
    return <Modal open={open} title={MODAL_TITLE} text={text} buttons={modalButtons} onClose={onClose} />;
  }
}

ValidatorMessage.propTypes = {
  open: PropTypes.bool.isRequired,
  text: PropTypes.array.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ValidatorMessage;
