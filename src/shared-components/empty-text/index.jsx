import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import OopsFace from '../../assets/images/amazed.png';

@observer
class EmptyText extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const text = this.props.text;
    
    return (
      <div className='empty-text'>
        <div className='icon'><img src={OopsFace} /></div>
        <div className='text'>{text}</div>
      </div>
    );
  }
}

EmptyText.propTypes = {
  text: PropTypes.string.isRequired
};

export default EmptyText;
