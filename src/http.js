import Http from './utils/http';
import { API_BASE, HOME_PATH } from './constants';
import { Auth } from "./auth";
import { setLocation } from'./utils/dom/window';

export const http = new Http();

// Add current active token to requests going to dockit api.
http.addPreRequest((config = {}) => {
  if (config.url.match(API_BASE)) {
    config.mode = 'cors';
    config.headers = config.headers || {};
    
    if (Auth.getActiveToken()) config.headers.Authorization = Auth.getActiveToken();
  }
  
  return config;
});

// Handle response error.
http.addPostResponseError(err => {
  const isAuthorized = !!Auth.getActiveToken();
  
  if (err.res && err.res.status === 401 && isAuthorized) {
    Auth.removeActiveToken();
    Auth.removeActiveUser();

    setLocation(HOME_PATH);
  }
  
  return Promise.reject(err);
});
