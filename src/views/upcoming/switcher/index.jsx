import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class UpcomingSwitcher extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onPrevClick = ::this.onPrevClickHandler;
    this.onNextClick = ::this.onNextClickHandler;
  }
  
  onPrevClickHandler() {
    this.props.onPrevClick();
  }
  
  onNextClickHandler() {
    this.props.onNextClick();
  }

  render() {
    const viewedDate = this.props.viewedDate;
    
    return (
      <div className='switcher'>
        <span className='arrow fa fa-chevron-left' onClick={this.onPrevClick} />
        <span className='text'>
          { viewedDate.format('MMMM') }
        </span>
        <span className='arrow fa fa-chevron-right' onClick={this.onNextClick} />
      </div>
    );
  }
}

UpcomingSwitcher.propTypes = {
  viewedDate: PropTypes.object.isRequired,
  onPrevClick: PropTypes.func.isRequired,
  onNextClick: PropTypes.func.isRequired
};

export default UpcomingSwitcher;
