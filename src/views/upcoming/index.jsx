import React, { Component } from 'react';
import moment from 'moment-timezone';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import { setTitle } from '../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';

import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';
import Calendar from './calendar/index.jsx';
import Switcher from './switcher/index.jsx';
import ItemsList from './items-list/index.jsx';
import Link from './link/index.jsx';
import LinkCalendarsButton from './link-calendars-button/index.jsx';
import LinkCalendarsButtonMobile from './link-calendars-button-mobile/index.jsx';

import jq from '../../utils/jquery';

import { Auth } from '../../auth';
import { LINK_CALENDARS_PATH } from '../../constants';

import { getCalendarForDate } from '../../utils/generators/calendar';
import Refresher from '../../utils/refresher';

import DataState from './state/data';



@observer
class Upcoming extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.UPCOMING]());

    this.data.setIsFirstOpen();
    this.setWindowHeight();

    this.onDaySelect = ::this.onDaySelectHandler;
    this.onPrevClick = ::this.onPrevClickHandler;
    this.onNextClick = ::this.onNextClickHandler;
    this.onLinkCalendarsClick = ::this.onLinkCalendarsClickHandler;
    this.onLinkCalendarsCancelClick = ::this.onLinkCalendarsCancelClickHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.onResize = ::this.onResizeHandler;
    this.onBackToTodayClick = ::this.onBackToTodayClickHandler;

    this.data.fetchUpcoming()
    .then(() => this.data.initSelectedDayItems())
    .catch(console.error);

    /*if (toJS(this.data.isFirstOpen)) {
      localStorage.setItem('is-link-calendars-opened', 'already opened');
      return browserHistory.push(LINK_CALENDARS_PATH);
    }*/
  }
  
  componentWillMount() {
    document.body.addEventListener('keydown', this.onKeyDown);
    window.addEventListener('resize', this.onResize);
  }

  componentDidMount() {
    this.onResize(true);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
    document.body.removeEventListener('keydown', this.onKeyDown);
    this.data.clear();
  }
  
  onDaySelectHandler(day) {
    this.data.setDate(day);
  }
  
  onPrevClickHandler() {
    this.data.switchToPrevMonth();
  }
  
  onNextClickHandler() {
    this.data.switchToNextMonth();
  }
  
  onLinkCalendarsClickHandler() {
    //browserHistory.push(LINK_CALENDARS_PATH);
  }

  onLinkCalendarsCancelClickHandler() {
    localStorage.setItem('link-calendars-cancelled', true);
    this.data.setIsCancelled(!!localStorage.getItem('link-calendars-cancelled'));
  }

  onKeyDownHandler(e) {
    if (jq.wrap(e.target).is('input')) return;
    e.preventDefault();
    this.data.handleArrows(e.keyCode, e.shiftKey);
  }

  onResizeHandler(forceSetHeight = false) {
    if (!forceSetHeight && !this.isWindowHeightChanged()) return;

    const calendar = jq.wrap('.calendar');
    const wideDayItemsWrapper = jq.wrap('.wide-day-items-wrapper');
    if (!calendar.isValid() || !wideDayItemsWrapper.isValid()) return;

    wideDayItemsWrapper.css('height', `${calendar.height()}px`);
  }

  onBackToTodayClickHandler() {
    this.data.backToToday();
  }

  setWindowHeight() {
    this.windowHeight = window.innerHeight;
  }

  isWindowHeightChanged() {
    return this.windowHeight !== window.innerHeight;
  }

  render() {
    const { isFirstOpen, isCancelled, viewedDate, selectedDate, items, selectedDayItems } = this.data;
    const calendar = getCalendarForDate(this.data.viewedDate, this.data.selectedDate);
    const isSignedToGoogle = !!Auth.getActiveGoogleAuth();
    const dayItems = <ItemsList upcomingItems={toJS(selectedDayItems)} />;
    const isTodaySelected = selectedDate.isSame(new Date(), 'day');
    
    return (
      <div>
        <ScrollToTop />
        <Grid>
          <Row className='upcoming no-select'>
            <Col sm={12} md={12} className='upcoming-inner-container'>
              { !isTodaySelected && <Link text='Back to Today' onClick={this.onBackToTodayClick} /> }
              <Clearfix className='mobile-day-items-wrapper' visibleXsBlock visibleSmBlock>{dayItems}</Clearfix>
              <Switcher viewedDate={viewedDate} onPrevClick={this.onPrevClick} onNextClick={this.onNextClick} />
              {
                /*
                 !isCancelled && !isSignedToGoogle &&
                 <div className='link-calendars-button-visibility-container'>
                 <LinkCalendarsButtonMobile onClick={this.onLinkCalendarsClick} onCancelClick={this.onLinkCalendarsCancelClick} />
                 </div>
                 */
              }

              <div className='calendar-upcoming-wrapper'>
                <div className='calendar-wrapper'>
                  <Calendar calendar={calendar} upcomingItems={toJS(items)} viewedDate={viewedDate} onDaySelect={this.onDaySelect} />
                </div>
                <div className='wide-day-items-wrapper'>
                  <Clearfix visibleMdBlock visibleLgBlock>{dayItems}</Clearfix>
                </div>
              </div>
            </Col>
            {
              /*
               !isFirstOpen && !isSignedToGoogle &&
               <div className='link-calendars-button-large-visibility-container'>
               <LinkCalendarsButton onClick={this.onLinkCalendarsClick} />
               </div>
               */
            }
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Upcoming;
