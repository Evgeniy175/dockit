import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import LinkCalendarsIcon from '../../../assets/images/icons/link-calendars.png';

@observer
class LinkCalendarsButton extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const handler = get(this.props, 'onClick');
    
    if (handler) handler();
  }

  render() {
    return (
      <div className='link-calendars-button' onClick={this.onClick}>
        <span className='link-calendars-button-icon'>
          <img src={LinkCalendarsIcon} />
        </span>
        <span className='link-calendars-button-text'>
          Link calendars
        </span>
      </div>
    );
  }
}

LinkCalendarsButton.propTypes = {
  onClick: PropTypes.func
};

export default LinkCalendarsButton;
