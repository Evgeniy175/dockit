import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class UpcomingLink extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { text, className, onClick } = this.props;
    const cn = `upcoming-link${className ? ` ${className}` : ''}`;
    return <div className={cn} onClick={onClick}>{text}</div>;
  }
}

UpcomingLink.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default UpcomingLink;
