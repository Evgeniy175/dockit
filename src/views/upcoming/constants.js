export const DAYS_IN_WEEK = 7;
export const FIRST_DAY_OF_MONTH = 1;
export const WEEKEND_DAYS = [0, 6];
export const WEEKDAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export const DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss ZZ';
