import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

import { UPCOMING_ITEMS_TYPES } from '../../../../utils/sorting/constants';

import EventModel from '../../../../models/events/index';

import Owner from './owner/index.jsx';
import Content from './content/index.jsx';
import Time from './time/index.jsx';

const eventModel = new EventModel();

@observer
class UpcomingItemsListItem extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const item = this.props.item;
    
    return item.type === UPCOMING_ITEMS_TYPES.EVENT
    ? this.redirectToEvent(item.id)
    : this.redirectToEventFromSchedule(item);
  }
  
  redirectToEvent(id) {
    browserHistory.push(`/events/${id}`);
    return Promise.resolve();
  }
  
  redirectToEventFromSchedule(item) {
    const config = {
      body: JSON.stringify({ date: item.date.format() })
    };
    
    return eventModel.fromSchedule(item.id, config)
    .then(res => { browserHistory.push(`/events/${res.id}`); return Promise.resolve(); })
    .catch(console.error);
  }

  render() {
    const item = this.props.item;
    
    return (
      <div className='item' onClick={this.onClick}>
        <Owner item={item} />
        <Content item={item} />
        <Time item={item} />
      </div>
    );
  }
}

UpcomingItemsListItem.propTypes = {
  item: PropTypes.object.isRequired
};

export default UpcomingItemsListItem;
