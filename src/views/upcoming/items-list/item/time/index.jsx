import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { formatDate } from '../../../../../utils/formatting/date';

@observer
class UpcomingItemsListItemTime extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const item = this.props.item;
    const start = item.startTime;
    const end = item.endTime;
    const startFormatted = formatDate(start);
    const endFormatted = formatDate(end);
    const isAllDay = start.isSame(end);
    
    if (isAllDay) return (
      <div className='time'>
        <div className='start'>All day</div>
      </div>
    );
    
    return (
      <div className='time'>
        <div className='start'>
          {startFormatted}
        </div>
        <div className='end'>
          {endFormatted}
        </div>
      </div>
    );
  }
}

UpcomingItemsListItemTime.propTypes = {
  item: PropTypes.object.isRequired
};

export default UpcomingItemsListItemTime;
