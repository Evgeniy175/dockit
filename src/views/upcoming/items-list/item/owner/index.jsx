import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { Auth } from '../../../../../auth';

@observer
class UpcomingItemsListItemOwner extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const item = this.props.item;
    const currUserId = Auth.getActiveUser().id;
    const isCurrentUserEvent = item.userId === currUserId;
    const className = `owner${isCurrentUserEvent ? ' by-user' : ' by-other-user'}`;
    
    return (
      <div className={className} />
    );
  }
}

UpcomingItemsListItemOwner.propTypes = {
  item: PropTypes.object.isRequired
};

export default UpcomingItemsListItemOwner;
