import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class UpcomingItemsListItemContent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const item = this.props.item;
    
    return (
      <div className='content'>
        <div className='upcoming-item-title upcoming-list-data-item'>
          {item.title}
        </div>
        <div className='upcoming-item-location upcoming-list-data-item'>
          {item.location}
        </div>
      </div>
    );
  }
}

UpcomingItemsListItemContent.propTypes = {
  item: PropTypes.object.isRequired
};

export default UpcomingItemsListItemContent;
