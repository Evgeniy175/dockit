import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Item from './item/index.jsx';

import { EMPTY_TEXT } from './constants';



@observer
class UpcomingItemsList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const items = this.props.upcomingItems;
    
    if (!items || items.length === 0) return <div className='no-items'>{ EMPTY_TEXT }</div>;
    
    return (
      <div className='upcoming-items-list'>
        { items.map(item => <Item key={this.generateKey(item)} item={item} />) }
      </div>
    );
  }
  
  generateKey(item) {
    const keyItems = [
      get(item, 'date', ''),
      get(item, 'createdAt', ''),
      get(item, 'title', '')
    ];
    
    return keyItems.join('_');
  }
}

UpcomingItemsList.propTypes = {
  upcomingItems: PropTypes.array
};

UpcomingItemsList.defaultProps = {
  upcomingItems: []
};

export default UpcomingItemsList;
