import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';

import { WEEKDAYS, WEEKEND_DAYS } from '../../constants';

import Weekday from './weekday/index.jsx';

@observer
class UpcomingCalendarWeekdays extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const weekdays = WEEKDAYS.map((name, idx) => ({
      name,
      isWeekend: WEEKEND_DAYS.includes(idx)
    }));
    
    return (
      <div className='calendar-weekdays'>
        {
          weekdays.map(day => <Weekday key={day.name} data={day} />)
        }
      </div>
    );
  }
}

export default UpcomingCalendarWeekdays;
