import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class UpcomingCalendarWeekdaysItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.data;
    const className = `calendar-weekdays${data.isWeekend ? ' weekend' : ''}`;
    
    return (
      <Col sm={1} className={className}>
        { data.name }
      </Col>
    );
  }
}

UpcomingCalendarWeekdaysItem.propTypes = {
  data: PropTypes.object.isRequired
};

export default UpcomingCalendarWeekdaysItem;
