import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { UPCOMING_KEY_FORMATTER_TEMP } from '../../../../utils/sorting/constants';

import CalendarItem from './item/index.jsx';



@observer
class UpcomingCalendarRow extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { row, upcomingExistDays, nOfWeeks } = this.props;
    const handlers = {
      onClick: this.props.onDaySelect,
    };
    const items = row.map((data, idx) => {
      const values = {
        data,
        nOfWeeks,
        isUpcomingExists: data && upcomingExistDays.includes(data.date.format(UPCOMING_KEY_FORMATTER_TEMP)),
      };
      return <CalendarItem key={get(data, 'date.format') ? data.date.format() : idx} values={values} handlers={handlers} />
    });

    return <div className='calendar-row'>{ items }</div>;
  }
}

UpcomingCalendarRow.propTypes = {
  row: PropTypes.array.isRequired,
  upcomingExistDays: PropTypes.array.isRequired,
  nOfWeeks: PropTypes.number,
  onDaySelect: PropTypes.func.isRequired
};

export default UpcomingCalendarRow;
