import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';

@observer
class UpcomingCalendarRowItem extends Component {
  constructor(props) {
    super(props);

    this.weeksClasses = {
      4: ' four-weeks',
      6: ' six-weeks',
    };
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const { data } = this.props.values;
    if (data) this.props.handlers.onClick(data.date);
  }

  render() {
    const { data, isUpcomingExists, nOfWeeks } = this.props.values;
    const className = `calendar-item${data ? '' : ' empty-item'}${this.weeksClasses[nOfWeeks] ? this.weeksClasses[nOfWeeks] : ''}`;
    const dateClassName = `date${data && data.isActive ? ' active' : ''}${data && data.isWeekend ? ' weekend' : ''}`;
    
    return (
      <Col sm={1} className={className} onClick={this.onClick}>
        { data && <div className={dateClassName}>{ data.dayOfMonth }</div> }
        { isUpcomingExists && <div className='upcoming-exists-container'><div className='circle' /></div> }
      </Col>
    );
  }
}

UpcomingCalendarRowItem.propTypes = {
  values: PropTypes.shape({
    data: PropTypes.object,
    isUpcomingExists: PropTypes.bool,
    nOfWeeks: PropTypes.number,
  }),
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }),
};

UpcomingCalendarRowItem.defaultProps = {
  data: {},
  isUpcomingExists: false
};

export default UpcomingCalendarRowItem;
