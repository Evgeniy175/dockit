import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Weekdays from './weekdays/index.jsx';
import Title from './title/index.jsx';
import CalendarRow from './row/index.jsx';

import { getNumWeeksInMonth } from '../../../utils/moment';



@observer
class UpcomingCalendar extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDaySelect = ::this.onDaySelectHandler;
  }
  
  onDaySelectHandler(day) {
    this.props.onDaySelect(day);
  }

  render() {
    const { calendar, viewedDate, upcomingItems } = this.props;
    const upcomingExistDays = Object.keys(upcomingItems);
    const nOfWeeks = getNumWeeksInMonth(viewedDate);
    
    return (
      <div>
        <Weekdays />
        <div className='calendar'>
          <Title date={viewedDate} calendar={calendar} />
          {
            calendar.map((row, idx) => <CalendarRow key={idx} row={row} nOfWeeks={nOfWeeks} onDaySelect={this.onDaySelect} upcomingExistDays={upcomingExistDays} />)
          }
        </div>
      </div>
    );
  }
}

UpcomingCalendar.propTypes = {
  calendar: PropTypes.array.isRequired,
  upcomingItems: PropTypes.object.isRequired,
  viewedDate: PropTypes.object.isRequired,
  onDaySelect: PropTypes.func.isRequired
};

export default UpcomingCalendar;
