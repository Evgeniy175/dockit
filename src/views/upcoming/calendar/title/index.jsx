import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import moment from 'moment-timezone';
import PropTypes from 'prop-types';

import EmptyCols from './empty-cols/index.jsx';

@observer
class UpcomingCalendarTitle extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const calendar = this.props.calendar;
    const date = this.props.date;
    const firstDayOfMonth = moment(date).startOf('month');
  
    const isFirstDayOfWeek = firstDayOfMonth.day() === moment(firstDayOfMonth).startOf('week').day();
    const isLastDayOfWeek = firstDayOfMonth.day() === moment(firstDayOfMonth).endOf('week').day();
    
    const monthFormatter = isLastDayOfWeek ? 'MMM' : 'MMMM';
    const month = date.format(monthFormatter);
    const year = date.year();
    
    const className = `title${isFirstDayOfWeek ? ' add-margin' : ''}${isLastDayOfWeek ? ' last-day-of-week' : ''}`;
    
    return (
      <div className={className}>
        <EmptyCols calendar={calendar} />
        <span className={`month static`}>{month}</span>
        <span className='year'> {year}</span>
      </div>
    );
  }
}

UpcomingCalendarTitle.propTypes = {
  date: PropTypes.object.isRequired,
  calendar: PropTypes.array.isRequired
};

export default UpcomingCalendarTitle;
