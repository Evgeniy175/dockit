import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class UpcomingCalendarTitleEmptyCols extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const calendar = this.props.calendar;
    
    return (
      <div>
        {
          calendar[0].map((day, idx) => day ? null : <Col key={idx} sm={1} className='empty-col' />)
        }
      </div>
    );
  }
}

UpcomingCalendarTitleEmptyCols.propTypes = {
  calendar: PropTypes.array.isRequired
};

export default UpcomingCalendarTitleEmptyCols;
