import { action, observable, toJS } from 'mobx';
import moment from 'moment-timezone';
import Promise from 'bluebird';

import { DATE_FORMAT } from '../constants';
import { KEY_CODES } from '../../../constants';
import { UPCOMING_KEY_FORMATTER, UPCOMING_KEY_FORMATTER_TEMP } from '../../../utils/sorting/constants';
import { getSortedEvents } from '../../../utils/sorting/upcoming';
import Refresher from '../../../utils/refresher';

import SchedulesModel from '../../../models/schedules';
import EventsModel from '../../../models/events';

const schedulesModel = new SchedulesModel();
const eventsModel = new EventsModel();

import DataState from '../../../utils/state/data';



class UpcomingDataState extends DataState {
  @observable isFirstOpen = false;
  @observable isCancelled = false;

  @observable items = {};
  @observable selectedDayItems = [];
  
  @observable viewedDate = moment().startOf('day');
  @observable selectedDate = moment().startOf('day');
  
  lastDayOfMonth = moment().endOf('month').date();

  refresher;
  
  constructor(props = {}) {
    super(props);
    this.handleNormalArrows = ::this.handleNormalArrowsHandler;
    this.handleShiftedArrows = ::this.handleShiftedArrowsHandler;

    const refresherConfig = {
      cb: ::this.fetchUpcoming,
    };
    this.refresher = new Refresher(refresherConfig);
  }

  @action
  setIsFirstOpen() {
    this.isFirstOpen = !localStorage.getItem('is-link-calendars-opened');
  }

  @action
  setIsCancelled(value) {
    this.isCancelled = value;
  }
  
  @action
  setDate(value) {
    this.selectedDate = moment(value);
    this.initSelectedDayItems();
  }
  
  @action
  switchToPrevMonth() {
    this.setViewedDate(moment(this.viewedDate).subtract(1, 'months'));
    this.fetchUpcoming();
  }
  
  @action
  switchToNextMonth() {
    this.setViewedDate(moment(this.viewedDate).add(1, 'months'));
    this.fetchUpcoming();
  }

  backToToday() {
    this.setDate();
    this.setViewedDate();
    this.fetchUpcoming().then(() => this.setDate(this.selectedDate));
  }

  @action
  setViewedDate(value) {
    this.viewedDate = moment(value || moment().startOf('day'));
  }
  
  fetchUpcoming() {
    const start = this.viewedDate.startOf('month').format(DATE_FORMAT);
    const end = this.viewedDate.endOf('month').format(DATE_FORMAT);

    return Promise.all([
      this.fetchEvents(start, end),
      this.fetchVirtualEvents(start, end)
    ])
    .spread((events, schedules) => {
      const sorted = getSortedEvents(events.data, schedules);
      this.setItems(sorted);
      if (this.selectedDate.month() === this.viewedDate.month()) this.setDate(this.selectedDate);
    })
    .catch(console.error);
  }
  
  fetchEvents(start, end) {
    const time = [start, end];
    return eventsModel.fetchDockedEventsForUser(time);
  }
  
  fetchVirtualEvents(start, end) {
    return schedulesModel.fetchVirtualizedEvents(start, end);
  }
  
  @action setItems(value) {
    this.items = value;
  }
  
  @action
  clear() {
    this.viewedDate = moment().startOf('day');
    this.selectedDate = moment().startOf('day');
    this.lastDayOfMonth = moment().endOf('month').date();
    this.items = {};
    this.initSelectedDayItems();
  }
  
  @action
  initSelectedDayItems() {
    const key = this.selectedDate.format(UPCOMING_KEY_FORMATTER_TEMP);
    this.selectedDayItems = this.items[key];
  }

  @action
  handleArrows(keyCode, isShiftPressed) {
    const handler = isShiftPressed ? this.handleShiftedArrows : this.handleNormalArrows;
    const newSelectedDate = handler(keyCode);

    if (!newSelectedDate) return;

    this.selectedDate = newSelectedDate;
    const oldViewedDate = moment(this.viewedDate);
    this.viewedDate = moment(this.selectedDate).startOf('month');

    if (oldViewedDate.month() !== this.viewedDate.month()) return this.fetchUpcoming().then(() => this.setDate(this.selectedDate));
    if (!this.viewedDate.isSame(this.selectedDate)) this.initSelectedDayItems();
  }

  handleNormalArrowsHandler(keyCode) {
    switch (keyCode) {
      case KEY_CODES.UP_ARROW: return moment(this.selectedDate).subtract(1, 'weeks');
      case KEY_CODES.RIGHT_ARROW: return moment(this.selectedDate).add(1, 'days');
      case KEY_CODES.DOWN_ARROW: return moment(this.selectedDate).add(1, 'week');
      case KEY_CODES.LEFT_ARROW: return moment(this.selectedDate).subtract(1, 'days');
    }
  }

  handleShiftedArrowsHandler(keyCode) {
    switch (keyCode) {
      case KEY_CODES.UP_ARROW: return moment(this.selectedDate).subtract(1, 'month');
      case KEY_CODES.RIGHT_ARROW: return moment(this.selectedDate).add(1, 'month');
      case KEY_CODES.DOWN_ARROW: return moment(this.selectedDate).add(1, 'month');
      case KEY_CODES.LEFT_ARROW: return moment(this.selectedDate).subtract(1, 'month');
    }
  }
}

export default UpcomingDataState;
