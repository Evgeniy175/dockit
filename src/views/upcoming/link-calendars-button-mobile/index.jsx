import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Button from '../../../assets/images/link-calendars/button.svg';
import CancelButton from '../../../assets/images/link-calendars/cancel.svg';



@observer
class LinkCalendarsButtonMobile extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
    this.onCancelClick = ::this.onCancelClickHandler;
  }
  
  onClickHandler() {
    const handler = get(this.props, 'onClick');
    if (handler) handler();
  }

  onCancelClickHandler() {
    const handler = get(this.props, 'onCancelClick');
    if (handler) handler();
  }

  render() {
    return (
      <div className='link-calendars-button-mobile'>
        <span className='link-calendars-button-icon' onClick={this.onClick}>
          <img src={Button} />
        </span>
        <span className='link-calendars-cancel-icon' onClick={this.onCancelClick}>
          <img className='cancel-button' src={CancelButton} />
        </span>
      </div>
    );
  }
}

LinkCalendarsButtonMobile.propTypes = {
  onClick: PropTypes.func,
  onCancelClick: PropTypes.func,
};

export default LinkCalendarsButtonMobile;
