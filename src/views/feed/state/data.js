import { action, observable, computed } from 'mobx';
import { browserHistory } from 'react-router';

import { PORTION_SIZE } from '../constants';
import { TYPES } from '../../../shared-components/feed/feed-item/constants';

import Refresher from '../../../utils/refresher';
import { imageToFormData } from '../../../utils/formatting/image';
import { isPost } from '../../../utils/posts';

import DataState from '../../../utils/state/data';

import NotableModel from '../../../models/notables';
import CommentModel from '../../../models/comments';

const notableModel = new NotableModel();
const commentModel = new CommentModel();



class FeedDataState extends DataState {
  @observable feedItems = [];
  @observable commentForPost;

  // due to sometimes several items can disappear. E.g. you requested 20, returns 19
  refetchCount = 0;
  refresher;
  @observable isBusy = false;
  @observable isLoaded = false;
  @observable isEnd = false;

  nextPage;

  get isLoadMoreAvailable() {
    return !this.isEnd && !this.isBusy && !!this.nextPage;
  }

  @computed
  get itemsForRender() {
    return this.feedItems;
  }

  constructor(props = {}) {
    super(props);
    const refresherConfig = {
      cb: ::this.refreshData,
    };
    this.refresher = new Refresher(refresherConfig);
  }

  refreshData() {
    const config = {
      params: {
        limit: this.refetchCount,
      },
    };

    return notableModel.fetchFeed(config)
    .then(res => {
      if (!res) return Promise.reject();
      this.parseResponse(res);
      return Promise.resolve(res);
    })
    .catch(console.error);
  }

  fetchFeedItems() {
    const config = {
      params: {
        limit: PORTION_SIZE,
      },
    };
    this.refetchCount = PORTION_SIZE;
    this.setIsBusy(true);
    
    return notableModel.fetchFeed(config)
    .then(res => {
      if (!res) return Promise.reject();
      this.parseResponse(res);
      this.setIsBusy(false);
      return Promise.resolve(res);
    })
    .catch(console.error)
    .finally(() => this.setIsLoaded(true));
  }
  
  fetchMoreFeedItems() {
    const config = {
      params: {
        page: this.nextPage,
        limit: PORTION_SIZE,
      },
    };

    this.refetchCount += PORTION_SIZE;
    
    return notableModel.fetchFeed(config)
    .then(res => {
      if (!res) return Promise.reject();
      this.nextPage = res.nextPage;
      this.attachFeedItems(res.data);
      return Promise.resolve(res);
    })
    .catch(console.error);
  }

  deleteItem(id) {
    return new Promise(resolve => {
      this.deleteItemFromList(id);
      return resolve();
    });
  }

  addComment(userId, message, image) {
    return commentModel.addCommentToUser(userId, message)
    .then(res => {
      if (!image) return Promise.resolve(res);

      const formData = imageToFormData(image);
      const config = { body: formData };

      return commentModel.attachPictureToComment(res.id, config)
      .then(imgRes => Promise.resolve(Object.assign(res, imgRes)));
    })
    .then(res => {
      const item = {
        action: 'create',
        data: res,
        actionData: res,
        type: TYPES.COMMENT,
        timestamp: res.created_at,
      };
      this.attachFeedItemsToTheTop([item]);
      this.refetchCount++;
      return Promise.resolve();
    })
    .catch(console.error);
  }

  onCommentReply(comment) {

    // if comment should be redirected to the drilldown
    // const uri = isPost(comment) ? `/posts/${comment.id}` : `/${CommentModel.getTarget(comment)}s/${this.getTargetId(comment)}?commentId=${comment.id}`;

    if (isPost(comment)) return browserHistory.push(`/posts/${comment.id}`);
    this.setCommentForPost(comment);
  }

  @action
  setCommentForPost(value) {
    this.commentForPost = value;
  }

  @action
  deleteItemFromList(id) {
    this.feedItems = this.feedItems.filter(item => item.data.id !== id);
    this.refetchCount--;
  }
  
  @action
  parseResponse(res) {
    this.nextPage = res.nextPage;
    this.feedItems = res.data.filter(item => item !== null);
  }
  
  @action
  attachFeedItemsToTheTop(feedItems = []) {
    this.feedItems = feedItems.filter(item => item !== null).concat(...this.feedItems);
  }

  @action
  attachFeedItems(feedItems = []) {
    this.setIsEnd(feedItems.length === 0);
    this.feedItems = this.feedItems.concat(feedItems.filter(item => item !== null));
  }

  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }

  @action
  setIsBusy(value) {
    this.isBusy = value;
  }

  @action
  setIsEnd(value) {
    this.isEnd = value;
  }

  @action
  clear() {
    this.feedItems = null;
    this.refresher.clear();
    this.refresher = null;
    this.refetchCount = 0;
  }
}

export default FeedDataState;
