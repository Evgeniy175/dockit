import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { setTitle } from '../../utils/formatting/title';

import EmptyFeed from './empty-feed/index.jsx';
import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';
import Loader from '../../shared-components/loader-bottom/index.jsx';
import CenteredLoader from '../../shared-components/loader/index.jsx';
import FeedItem from '../../shared-components/feed/feed-item/index.jsx';
import CommentAdd from '../../shared-components/create-comment/index.jsx';
import ReachedTheEndText from '../../shared-components/end-text/index.jsx';

import DataState from './state/data';

import LoadMoreHelper from '../../utils/dom/events/scroll-helper';
import { Auth } from '../../auth';
import { generateKey } from '../../utils/generators/feed';

import { NEW_COMMENT_ROWS, NEW_COMMENT_PLACEHOLDER } from './constants';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';
import { TYPES } from '../../shared-components/create-comment/constants';
import { POSITIONS } from '../../shared-components/users/dropdown/constants';
import { EVENTS } from '../../utils/dom/events/scroll-helper/constants';



@observer
class Feed extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
    });
    setTitle(PAGE_TITLES[PAGES.FEED]());
    this.data.fetchFeedItems();

    this.loadMoreHelper = new LoadMoreHelper({
      handlers: {
        [EVENTS.WHEEL.name]: ::this.onWheelHandler,
        [EVENTS.TOUCH_MOVE.name]: ::this.onWheelHandler,
        [EVENTS.KEY_DOWN.name]: ::this.onWheelHandler,
      },
    });

    this.itemHandlers = {
      onPostCommentReply: ::this.onPostCommentReplyHandler,
      onDelete: ::this.onItemDeleteHandler,
    };
    this.commentHandlers = {
      onSubmit: ::this.onCommentAddingHandler,
    };
  }

  componentWillReceiveProps() {
    const { isLoaded } = this.data;
    if (!isLoaded) return;
    this.data.fetchFeedItems();
  }

  componentWillUnmount() {
    this.loadMoreHelper.dispose();
    this.data.clear();
    this.data = null;
    this.itemHandlers = null;
    this.commentHandlers = null;
    this.loadMoreHelper = null;
  }
  
  onWheelHandler() {
    if (!this.data.isLoadMoreAvailable) return;
    this.data.setIsBusy(true);
    return this.data.fetchMoreFeedItems()
    .catch(console.error)
    .finally(() => this.data.setIsBusy(false));
  }

  onItemDeleteHandler(id) {
    this.data.deleteItem(id);
  }

  onPostCommentReplyHandler(comment) {
    this.data.onCommentReply(comment);
  }

  onCommentAddingHandler(message, image) {
    return this.data.addComment(Auth.getActiveUser().id, message, image);
  }

  render() {
    const { handlers } = this.props;
    const { isEnd, isLoaded, isBusy, feedItems, commentForPost } = this.data;

    if (isBusy && !isLoaded) return <CenteredLoader />;
    if (feedItems.length === 0) return <EmptyFeed handlers={handlers} />;

    const commentValues = {
      userTags: commentForPost && commentForPost.user ? new Set([commentForPost.user.username]) : null,
      type: TYPES.CREATE,
      rows: NEW_COMMENT_ROWS,
      placeholder: NEW_COMMENT_PLACEHOLDER,
      usersDropdownPosition: POSITIONS.BOTTOM,
    };

    return (
      <div>
        <ScrollToTop />
        <Grid className='view-feed'>
          <Row>
            <Col xs={12} mdOffset={3} md={6}>
              <CommentAdd values={commentValues} handlers={this.commentHandlers} />
            </Col>
          </Row>
          <Row>
            <Col xs={12} mdOffset={3} md={6}>
              { feedItems.map((item, idx) => <FeedItem key={generateKey(item, idx)} values={{ item }} handlers={this.itemHandlers} />) }
              { isBusy && <Loader /> }
            </Col>
          </Row>
          <Row>
            {isEnd && <ReachedTheEndText />}
          </Row>
        </Grid>
      </div>
    );
  }
}

Feed.propTypes = {
  handlers: PropTypes.object
};

export default Feed;
