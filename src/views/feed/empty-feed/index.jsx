import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { Grid, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Button from '../../../shared-components/button/index.jsx';

@observer
class EmptyFeed extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onSearchExpand = ::this.onSearchExpandHandler;
  }
  
  onSearchExpandHandler() {
    const handler = get(this.props, 'handlers.onSearchExpand');
    if (handler) handler();
  }

  render() {
    return (
      <Grid className='empty-feed-container'>
        <div className='empty-feed'>
          <Col xs={12} smOffset={2} sm={8}>
            <div>Your feed is empty!</div>
            <div>Connect with your friends already on DockIt.</div>
            <div className='buttons'>
              <Button text='Search for Friends' onClick={this.onSearchExpand}/>
            </div>
          </Col>
        </div>
      </Grid>
    );
  }
}

EmptyFeed.propTypes = {
  handlers: PropTypes.object
};

export default EmptyFeed;
