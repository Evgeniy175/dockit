import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../models/events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../models/schedules/constants';

export const ITEMS = {
  [EVENTS_MODEL_PLURAL]: EVENTS_MODEL_PLURAL,
  [SCHEDULES_MODEL_PLURAL]: SCHEDULES_MODEL_PLURAL,
};

export const TIMEOUT_MS = 20;

export const PORTION_SIZE = 20;

export const NEW_COMMENT_ROWS = 4;
export const NEW_COMMENT_PLACEHOLDER = `What's going on with you?`;
