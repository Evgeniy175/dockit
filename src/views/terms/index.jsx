import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { setTitle } from '../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';

import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';



@observer
class ProfileSettingsAboutTerms extends Component {
  constructor(props) {
    super(props);
    setTitle(PAGE_TITLES[PAGES.TERMS]());
  }
  
  render() {
    return (
      <div className='terms-wrapper'>
        <ScrollToTop />
        <Grid>
          <Row className='terms'>
            <Col xs={12}>
              <h2>DOCKIT TERMS OF SERVICE AND STANDARDS</h2>
              
              <h3>1. Basic Terms</h3>
              <h4>Welcome to DockIt</h4>
              <p>The following Terms of Service (“TOS”) are between you and DockIt and constitute a legal agreement that governs your use of the DockIt product, software, services and websites (collectively referred to as the “Service”). By using the Service, you agree to these TOS.  If you do not agree to any of the following terms, please do not use the Service. You should print or otherwise save a copy of these TOS for your records. "DockIt”  as used herein means DockIt Inc. doing business as DockIt Inc., a Delaware corporation.</p>
              <h4>Legal Authority</h4>
              <p>To use and/or register for the Service you must be cannot be a person barred from receiving the Service under the laws of the United States or other applicable jurisdiction, including the country in which you reside or from where you use the Service.</p>
              <h4>Updates</h4>
              <p>DockIt may update or change these TOS from time to time and recommends that you review the TOS on a regular basis. You can review the most current version of the TOS at any time at dockit.me/policies. If DockIt makes a change to the TOS, it will post the revised TOS on our website at the link as herein noted. You understand and agree that your continued use of the Service after the TOS has changed constitutes your acceptance of the TOS as revised. Without limiting the foregoing, if DockIt makes a change to the TOS that materially impacts your use of the Service, DockIt may post notice of any such change on our website and/or email you notice of any such change to the contact information provided at sign-up.</p>
              
              <h3>2. Description of the Service Requirements</h3>
              <h4>Changing the Service</h4>
              <p>The Services that DockIt provides are always evolving and the form and nature of the Services that DockIt provides may change from time to time without prior notice to you. In addition, DockIt may stop (permanently or temporarily) providing the Services (or any features within the Services) to you or to users generally and may not be able to provide you with prior notice. We also retain the right to create limits on use and storage at our sole discretion at any time without prior notice to you.</p>
              <h4>Limitations on Use</h4>
              <p>You agree to use the Service only for purposes as permitted by these TOS and any applicable law, regulation, or generally accepted practice in the applicable jurisdiction.</p>
              <h4>Availability of the Service</h4>
              <p>The Service, or any feature or part thereof, may not be available in all languages or in all countries and DockIt makes no representation that the Service, or any feature or part thereof, is appropriate or available for use in any particular location. To the extent you choose to access and use the Service, you do so at your own initiative and are responsible for compliance with any applicable laws, including, but not limited to, any applicable local laws.</p>
              
              <h3>3. Content and Your Conduct</h3>
              <h4>Content</h4>
              <p>"Content" means any information that may be generated or encountered through use of the Service, such as media files, metadata, graphics, images, photographs and other like materials for use in the App. You understand that all Content whether publicly posted or privately transmitted on the Service is the sole responsibility of the person from whom such Content originated. This means that you, and not DockIt, are solely responsible for any Content you upload, download, post, email, transmit, store or otherwise make available through your use of the Service. DockIt does not control the Content posted via the Service, nor does it guarantee the accuracy, integrity or quality of such Content. You understand and agree that your use of the Service and any Content is solely at your own risk. In addition, DockIt may remove content that expresses support for groups that are involved in violent or criminal behavior, including but not limited to terrorism, organized criminal activity or the support or praise of leaders of these organizations, or condoning their violent activities.</p>
              <h4>Community Standards</h4>
              <p>You agree that you will NOT use the Service to:</p>
              <ol>
                <li>Upload, download, post, email, transmit, store or otherwise make available any Content that is unlawful, harassing, threatening, harmful, tortuous, defamatory, libelous, abusive, violent, obscene, vulgar, invasive of another’s privacy, hateful, racially or ethnically offensive, or otherwise objectionable;</li>
                <li>Stalk, harass, threaten or harm another;</li>
                <li>Promote or threaten the use of sexual violence or exploitation. This includes the sexual exploitation of minors, and sexual assault. DockIt will also remove photographs or videos depicting incidents of sexual violence and images shared in revenge or without permissions from the people in the images;</li>
                <li>Promote self-injury or suicide;</li>
                <li>Create an organizational presence for terrorist activity or organized criminal activity;</li>
                <li>Purchase, sell or trade illegal or regulated goods including but not limited to prescription drugs and marijuana;</li>
                <li>If you are an adult, request personal or other information from a minor (any person under the age of 18 or such other age as local law defines as a minor) who is not personally known to you, including but not limited to any of the following: full name or last name, home address, zip/postal code, telephone number, picture, or the names of the minor's school, church, athletic team or friends;</li>
                <li>Pretend to be anyone, or any entity, you are not — you may not impersonate or misrepresent yourself as another person (including celebrities), entity, another Service subscriber, a DockIt employee, or a civic or government leader, or otherwise misrepresent your affiliation with a person or entity, (DockIt reserves the right to reject or block any user which could be deemed to be an impersonation or misrepresentation of your identity, or a misappropriation of another person's name or identity). Clear and obvious parody accounts are acceptable;</li>
                <li>Engage in any copyright infringement or other intellectual property infringement, or disclose any trade secret or confidential information in violation of a confidentiality, employment, or nondisclosure agreement(DockIt reserves the right to reject or block any account, which could be deemed to be in violation of copyright or intellectual property infringement);</li>
                <li>Plan or engage in any illegal activity; and/or</li>
                <li>Gather and store personal information on any other users of the Service to be used in connection with any of the foregoing prohibited activities.</li>
              </ol>
              <h4>Liability for Content</h4>
              <p>We do not endorse, support, represent or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the Services or endorse any opinions expressed via the Services. You understand that by using the Services, you may be exposed to Content that might be offensive, harmful, inaccurate or otherwise inappropriate, or in some cases, postings that have been mislabeled or are otherwise deceptive. Under no circumstances will DockIt be liable in any way for any Content, including, but not limited to, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of any Content posted, emailed, transmitted or otherwise made available via the Services or broadcast elsewhere.</p>
              <h4>Removal of Content</h4>
              <p>DockIt reserves the right at all times to determine whether Content is appropriate and in compliance with these TOS, and may pre-screen, move, refuse, modify and/or remove Content at any time, without prior notice and in its sole discretion, if such Content is found to be in violation of these TOS or is otherwise objectionable.</p>
              <h4>Access to Your Account and Content</h4>
              <p>You acknowledge and agree that DockIt may access, use, preserve and/or disclose your account information and Content if legally required to do so or if we have a good faith belief that such access, use, disclosure, or preservation is reasonably necessary to: (a) comply with legal process or request; (b) enforce these TOS, including investigation of any potential violation thereof; (c) detect, prevent or otherwise address security, fraud or technical issues; or (d) protect the rights, property or safety of DockIt, its users or the public as required or permitted by law.</p>
              
              <h3>4. Your Use of the Service</h3>
              <h4>Account Obligations</h4>
              <p>You agree that all account information including but not limited to contact information, payment and billing information you provide to DockIt during the signup process and use of the Service will be true, accurate, complete and current information, and that you shall maintain and update your account information as needed throughout your use of the Service to keep it accurate and current. Failure to provide accurate, current and complete account information may result in the suspension and/or termination of your account.</p>
              <p>You must be at least 13 years old to use the Service. DockIt does not knowingly collect or solicit any information from anyone under the age of 13 or knowingly allow such persons to register for the Service. The Service and its content are not directed at children under the age of 13.</p>
              <h4>Additional Obligations or Terms of Use</h4>
              <p>Particular components or features of the Service, provided by DockIt, may be subject to separate software or other license agreements or terms of use. You must read, accept, and agree to be bound by any such separate agreement as a condition of using these particular components or features of the Service. Except as otherwise agreed to in writing, you have control over the distribution of Your Content through the Service that you use, including but not limited to the DockIt Site/Application, iTunes, Google Marketplace, third-party websites, devices and platforms. By use of the Service for distribution, you agree that DockIt may make Your Content available and agree to abide to third-party conditions when opting in to a Service that involves a third-party, including but not limited to Apple and iTunes and Google Marketplace. DockIt makes no warranties with regard to the submission of Your Content for distribution and has no direct control or influence over the third party process. Submission and approval is governed solely by the third-party and DockIt will make every reasonable effort to follow guidelines for the process.</p>
              <h4>No Conveyance</h4>
              <p>Nothing in these TOS shall be construed to convey to you any interest, title, or license in the App or similar resource used by you in connection with the Service.</p>
              <h4>Use of Location-based Services</h4>
              <p>DockIt may provide certain features or services through the Service that rely upon device-based location information. To provide such features or services, where available, DockIt may collect, use, transmit, process and maintain your location data, including the real-time geographic location of your device, and you hereby agree and consent to DockIt’s collection, use, transmission, processing and maintenance of such location data to provide such services. In addition, by enabling and/or using any location-based services or features within the Service (e.g. allow the app to use your location), you agree and consent to DockIt collecting, using, processing and maintaining information related to your account, and any devices registered there under, for purposes of providing such location-based service or feature to you. Such information may include, but is not limited to, App ID, device ID and name, device type and real-time geographic location of your device at time of your request. You may withdraw this consent at any time by not using the location-based features or by turning off the Location Services settings (as applicable) on your device. When using third party services that use or provide location data as part of the Service, you are subject to and should review such third party's terms and privacy policy on use of location data by such third party services. Any location data provided by the Service is not intended to be relied upon in situations where precise location information is needed or where erroneous, inaccurate, time-delayed or incomplete location data may lead to death, personal injury, property or environmental damage. DockIt does not guarantee the availability, accuracy, completeness, reliability, or timeliness of location data or any other data displayed by the Service.</p>
              <h4>Passwords</h4>
              <p>You are responsible for safeguarding the password that you use to access the Services and for any activities or actions under your password. We encourage you to use “strong” passwords (passwords that use a combination of upper and lower case letters, numbers and symbols) with your account. DockIt cannot and will not be liable for any loss or damage arising from your failure to comply with the above.</p>
              
              <h3>5. DockIt Privacy Policy</h3>
              <p>You understand that by using the Service, you consent and agree to the collection and use of certain information about you and your use of the Service in accordance with DockIt’s Privacy Policy. You further consent and agree that DockIt may collect, use, transmit, process and maintain information related to your account, and any devices utilizing the application, for purposes of providing the Service, and any features therein, to you. Information collected by DockIt when you use the Service may also include technical or diagnostic information related to your use that may be used by DockIt to maintain, improve and enhance the Service. Furthermore, you agree to be bound by the DockIt Privacy Policy.</p>
              <p>Any information that you or other users provide to DockIt is subject to our Privacy Policy, which governs our collection and use of your information. You understand that through your use of the Services you consent to the collection and use (as set forth in the Privacy Policy) of this information, including the transfer of this information to the United States, and/or other countries for storage, processing and use by DockIt. As part of providing you the Services, we may need to provide you with certain communications, such as service announcements and administrative messages. These communications are considered part of the Services and your account, which you may not be able to opt-out from receiving.</p>
              
              <h3>6. Content Submitted or Made Available by You on the Service</h3>
              <h4>License from You</h4>
              <p>Except for material we may license to you, DockIt does not claim ownership of the materials and/or content you submit or make available on the Service. However, by submitting or posting such Content on areas of the Service that are accessible by the public, you grant DockIt a worldwide, royalty-free, non-exclusive license to use, distribute, reproduce, modify, adapt, publish, translate, publicly perform and publicly display such Content on the Service solely for the purpose for which such Content was submitted or made available. Said license will terminate within a commercially reasonable time after you or DockIt remove such Content from the public area. By submitting or posting such Content on areas of the Service that are accessible by the public, you are representing that you are the owner of such material and/or have authorization to distribute it.</p>
              <h4>Changes to Content</h4>
              <p>You understand that in order to provide the Service and make your Content available thereon, DockIt may transmit your Content across various public networks, in various media, and modify or change your Content to comply with technical requirements of connecting networks or devices. You agree that the license herein permits Dockit to take any such actions.</p>
              
              <h3>7. Trademark Information</h3>
              <p>DockIt, the DockIt logo, and other DockIt trademarks, service marks, graphics, and logos used in connection with the Service are trademarks or registered trademarks of DockIt Inc. in the US and/or other countries. Other trademarks, service marks, graphics, and logos used in connection with the Service may be the trademarks of their respective owners. You are granted no right or license in any of the aforesaid trademarks, and further agree that you shall not remove, obscure, or alter any proprietary notices (including trademark and copyright notices) that may be affixed to or contained within the Service.</p>
              
              <h3>8. Application and Software</h3>
              <h4>DockIt’s Proprietary Rights</h4>
              <p>You acknowledge and agree that DockIt owns all legal right, title and interest in and to the Service, and any software provided to you as a part of and/or in connection with the Service (the “Software”), including any and all intellectual property rights that exist therein, whether registered or not, and wherever in the world they may exist. You further agree that the Service (including the Software or any other part thereof) contains proprietary and confidential information that is protected by applicable intellectual property and other laws.</p>
              <h4>License From DockIt</h4>
              <p>DockIt grants you a non-exclusive, non-transferable, limited license for use of the application as provided to you by DockIt as a part of the Service and in accordance with these TOS; provided that you do not (and do not permit anyone else to) copy, modify, create a derivative work of, reverse engineer, decompile, or otherwise attempt to discover the source code (unless expressly permitted or required by law), sell, lease, sublicense, assign, grant a security interest in or otherwise transfer any right in DockIt.</p>
              <h4>Export Control</h4>
              <p>Use of the Service, including transferring, posting, or uploading data, software or other Content via the Service, may be subject to the export and import laws of the United States and other countries. You agree to comply with all applicable export and import laws and regulations. In particular, but without limitation, the Software may not be exported or re- exported (a) into any U.S. embargoed countries or (b) to anyone on the U.S. Treasury Department's list of Specially Designated Nationals or the U.S. Department of Commerce Denied Person’s List or Entity List. By using the Software or Service, you represent and warrant that you are not located in any such country or on any such list. You also agree that you will not use the Service for any purposes prohibited by United States law, including, without limitation, the development, design, manufacture or production of missiles, nuclear, chemical or biological weapons. You further agree not to upload to DockIt any data or software that cannot be exported without prior written government authorization, including, but not limited to, certain types of encryption software. This assurance and commitment shall survive termination of this Agreement.</p>
              <h4>Updates</h4>
              <p>As part of the Service, DockIt may elect to update the application. These updates may be automatically downloaded and installed to third party devices or platforms. These updates may include bug fixes, feature enhancements or improvements, or entirely new versions of the App. You agree that DockIt may automatically deliver such updates to you as part of the Service and you shall receive and install them as required.</p>
              
              <h3>9. Links and Other Third Party Materials</h3>
              <h4>Links</h4>
              <p>Certain Content, components or features of the Service may include materials from third parties and/or hyperlinks to other web sites, resources or Content. Because DockIt may have no control over such third party sites and/or materials, you acknowledge and agree that DockIt is not responsible for the availability of such sites or resources, and does not endorse or warrant the accuracy of any such sites or resources, and shall in no way be liable or responsible for any Content, advertising, products or materials on or available from such sites or resources. You further acknowledge and agree that DockIt shall not be responsible or liable in any way for any damages you incur or allege to have incurred, either directly or indirectly, as a result of your use and/or reliance upon any such Content, advertising, products or materials on or available from such sites or resources.</p>
              
              <h3>10. Disclaimer</h3>
              <p>DOCKIT DOES NOT PROMISE THAT THE SERVICE OR FEATURE OF THE SERVICE WILL BE ERROR-FREE OR UNINTERRUPTED, OR THAT ANY DEFECTS WILL BE CORRECTED, OR THAT YOUR USE OF THE SITE/APPLICATION WILL PROVIDE SPECIFIC RESULTS. THE SITE/APPLICATION, SERVICE AND DOCKIT’S CONTENT ARE DELIVERED ON AN “AS-IS” AND “AS-AVAILABLE” BASIS. ALL INFORMATION PROVIDED ON THE APPLICATION IS SUBJECT TO CHANGE WITHOUT NOTICE. DOCKIT CANNOT ENSURE THAT ANY FILES OR OTHER DATA YOU DOWNLOAD FROM OR UPLOAD TO THE SERVICE WILL BE FREE OF VIRUSES OR CONTAMINATION OR DESTRUCTIVE FEATURES. DOCKIT DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING ANY WARRANTIES OF ACCURACY, NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. DOCKIT DISCLAIMS ANY AND ALL LIABILITY FOR THE ACTS, OMISSIONS AND CONDUCT OF ANY THIRD PARTIES IN CONNECTION WITH OR RELATED TO YOUR USE OF THE APPLICATION. YOU ASSUME TOTAL RESPONSIBILITY FOR YOUR USE OF THE APPLICATION, YOUR CONTENT AND ANY LINKED SITES. YOUR SOLE REMEDY AGAINST DOCKIT FOR DISSATISFACTION WITH THE SITE/APPLICATION OR ANY CONTENT IS TO STOP USING THE SITE/APPLICATION OR ANY SUCH CONTENT. THIS LIMITATION OF RELIEF IS A PART OF THE BARGAIN BETWEEN THE PARTIES.</p>
              <p>The above disclaimer applies to any damages, liability or injuries caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction of or unauthorized access to, alteration of, or use, whether for breach of contract, tort, negligence or any other cause of action.</p>
              <p>DockIt reserves the right to do any of the following, at any time, without notice: (1) to modify, suspend or terminate operation of or access to the Site/Application, or any portion of the Site/Application, including, but not limited to, the Service, for any reason; (2) to modify or change the Site/Application, or any portion of the Site/Application, including, but not limited to, the Service, and any applicable policies or terms; and (3) to interrupt the operation of the Site/Application, or any portion of the Site/Application, including, but not limited to, the Service, as necessary to perform routine or non-routine maintenance, error correction, or other changes.</p>
              
              <h3>11. Limitation of Liability</h3>
              <p>Except where prohibited by law, in no event will DockIt be liable to you for any indirect, consequential, exemplary, incidental, statutory or punitive damages, including lost profits, even if DockIt has been advised of the possibility of such damages.</p>
              <p>If, notwithstanding the other provisions of these Terms of Service, DockIt is found to be liable to you for any damage or loss which arises out of or is in any way connected with your use of the Service, DockIt’s liability shall in no event exceed US$100.00. Some jurisdictions do not allow limitations of liability, so the foregoing limitation may not apply to you.</p>
              
              <h3>12. Indemnity</h3>
              <p>You agree to defend, indemnify and hold DockIt, its affiliates, subsidiaries, directors, officers, employees, agents, partners and licensors harmless from any claim or demand, including reasonable attorneys’ fees, made by a third party, relating to or arising from: (a) any Content you submit, post, transmit, or otherwise make available through the Service; (b) your use of the Service; (c) any violation by you of these TOS; or (d) your violation of any rights of another. This obligation shall survive the termination or expiration of these TOS and/or your use of the Service. You acknowledge that you are responsible for all use of the Service using your account, including any use by Sub-accounts, and that these TOS apply to any and all usage of your account, including any use by Sub-accounts. You agree to comply with these TOS and to defend, indemnify and hold harmless DockIt from and against any and all claims and demands arising from usage of your account or any Sub-account, whether or not such usage is expressly authorized by you.</p>
              
              <h3>13. Notices</h3>
              <p>DockIt may provide you with notices regarding the Service, including changes to these TOS, by email to the address you provided with your contact information upon Service activation, (and/or other alternate email address if provided), by regular mail, or by postings on our website/application and/or the Service.</p>
              
              <h3>14. Governing Law</h3>
              <p>You agree that all matters relating to your access to or use of the Site/Application or any of its Content, including all disputes, will be governed by the laws of the United States and by the laws of the State of Arizona without regard to its conflicts of laws provisions. You agree to the personal jurisdiction by and venue in the state and federal courts in the State of Arizona, and waive any objection to such jurisdiction or venue. Any claim under these Terms of Service must be brought within one (1) year after the cause of action arises, or such claim or cause of action is barred. No recovery may be sought or received for damages other than out-of-pocket expenses, except that the prevailing party will be entitled to costs and attorney's’ fees. In the event of any controversy or dispute between DockIt and you arising out of or in connection with your use of the Site of any of its Content, the parties shall attempt, promptly and in good faith, to resolve any such dispute. If we are unable to resolve any such dispute within a reasonable time (not to exceed thirty (30) days), then either party may submit such controversy or dispute to mediation. If the dispute cannot be resolved through mediation, then the parties shall be free to pursue any right or remedy available to them under applicable law.</p>
              
              <h3>15. Entire Agreement</h3>
              <p>These TOS, including the DockIt Standards for the DockIt Services, and our Privacy Policy constitute the entire agreement between you and DockIt, govern your use of the Service and completely replace any prior agreements between you and DockIt in relation to the Service. You may also be subject to additional terms and conditions that may apply when you use affiliate services, third-party content, or third-party software. If any part of these TOS is held invalid or unenforceable, that portion shall be construed in a manner consistent with applicable law to reflect, as nearly as possible, the original intentions of the parties, and the remaining portions shall remain in full force and effect. The failure of DockIt to exercise or enforce any right or provision of these TOS shall not constitute a waiver of such right or provision</p>
              <p>We may revise these Terms from time to time, the most current version will always be available to you. If the revision, in our sole discretion, is material we will notify you via e-mail to the email associated with your account. By continuing to access or use the Services after those revisions become effective, you agree to be bound by the revised Terms.</p>
              <p>Have any thoughts or questions about this policy, please contact us at <a href='mailto:hello@dockit.me'>hello@dockit.me</a>.</p>
              
              <h2>The DockIt Standards</h2>
              <h3>DockIt Rules and Policies</h3>
              <p>The goal is to provide a service that allows you to discover, create and share content from sources that interest you. We respect the ownership of the content that users share and each user is responsible for the content he or she provides. Because of these principles, we do not actively monitor and will not censor user content, except in limited circumstances described below.</p>
              
              <h4>Content Boundaries and Use of DockIt</h4>
              <p>In order to provide the DockIt service, there are some limitations on the type of content that can be published with DockIt. These limitations comply with legal requirements and make DockIt a better experience for all. We may need to change these rules from time to time and reserve the right to do so. Please check back here to see the latest.</p>
              <ol>
                <li><b>Impersonation:</b> You may not impersonate others through the DockIt service in a manner that does or is intended to mislead, confuse, or deceive others. Clear and obvious parody accounts are acceptable.</li>
                <li><b>Trademark:</b> We reserve the right to reclaim usernames on behalf of businesses or individuals that hold legal claim or trademark on those usernames. Accounts using business names and/or logos to mislead others may be permanently suspended.</li>
                <li><b>Private information:</b> You may not publish or post other people's private and confidential information, such as credit card numbers, street address or Social Security/National Identity numbers, without their express authorization and permission. You may not post intimate photos or videos that were taken or distributed without the subject's consent.</li>
                <li><b>Violence and Threats:</b> You may not publish or post threats of violence against others or promote violence against others.</li>
                <li><b>Copyright:</b> We will respond to clear and complete notices of alleged copyright infringement. Our copyright procedures are set forth in the Terms of Service.</li>
                <li><b>Unlawful Use:</b> You may not use our service for any unlawful purposes or in furtherance of illegal activities. International users agree to comply with all local laws regarding online conduct and acceptable content.</li>
              </ol>
              
              <h4>Abuse and Spam</h4>
              <p>DockIt aims to protect its users from abuse and spam. User abuse and technical abuse are not tolerated on the DockIt smartphone application or web version, and may result in permanent suspension. Any accounts engaging in the activities specified below may be subject to permanent suspension.</p>
              <ol>
                <li><b>Serial Accounts:</b> You may not create multiple accounts for disruptive or abusive purposes, or with overlapping use cases. Mass account creation may result in suspension of all related accounts. Please note that any violation of the DockIt Standards is cause for permanent suspension of all accounts.</li>
                <li><b>Targeted Abuse:</b> You may not engage in targeted abuse or harassment.</li>
                <li><b>Username Squatting:</b> You may not engage in username squatting. Accounts that are inactive for more than six months may also be removed without further notice. Some of the factors that we take into account when determining what conduct is considered to be username squatting are:</li>
                <ol>
                  <li>the number of accounts created</li>
                  <li>creating accounts for the purpose of preventing others from using those account names</li>
                  <li>creating accounts for the purpose of selling those accounts</li>
                  <li>using feeds of third-party content to update and maintain accounts under the names of those third parties</li>
                </ol>
                <li><b>Selling usernames:</b> You may not buy or sell DockIt usernames.</li>
                <li><b>Malware/Phishing:</b> You may not publish or link to malicious content intended to damage or disrupt another user’s browser or computer or to compromise a user’s privacy.</li>
                <li><b>Spam:</b> You may not use the DockIt service for the purpose of spamming anyone.</li>
                <li><b>Graphic Content:</b> You may not use pornographic or excessively violent media in your profile image, cover image, event image, or images posted to any subsequent page.</li>
              </ol>
              <p>Your account may be suspended for Terms of Service violations if any of the above is true. Accounts created to replace suspended accounts will be permanently suspended.</p>
              <p>Accounts engaging in any of these behaviors may be investigated for abuse. Accounts under investigation may be removed from Search for quality. DockIt reserves the right to immediately terminate your account without further notice in the event that, in its judgment, you violate these Rules or the Terms of Service.</p>
              <p>We may revise these Standards from time to time; the most current version will always be available.</p>
  
              <h3>Have Questions?</h3>
              <p>Have any thoughts or questions about this policy, or wish to report an account for violation of The DockIt Standards, please contact us at <a href='mailto:hello@dockit.me'>hello@dockit.me</a>.</p>
              <p className='copyright'>Copyright © 2017 DockIt Inc. All rights reserved.</p>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ProfileSettingsAboutTerms;
