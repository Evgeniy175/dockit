import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { CSS_MAPPING } from '../../../shared-components/button/constants';
import DataState from './state/data';
import { Form, Grid, Row, Col } from 'react-bootstrap';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import Button from '../../../shared-components/button/index.jsx';
import Textarea from '../../../shared-components/textarea/index.jsx';



@observer
class Reports extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState()
    }));
    setTitle(PAGE_TITLES[PAGES.REPORTS]());
  }

  componentWillMount() {
    this.onChange = ::this.onChangeHandler;
    this.onSend = ::this.onSendHandler;
  }

  onChangeHandler(e) {
    this.data.setValue(e.target.value);
  }

  onSendHandler() {
    const target = this.props.params.target;
    const id = this.props.params.id;
    const text = this.data.value;

    return this.data.createReport(target, id, text)
    .then(() => this.data.setValue(''))
    .catch(console.error);
  }

  render() {
    const textAreaValues = {
      id: 'report-description',
      value: this.data.value,
      placeholder: 'Message',
    };
    const textAreaHandlers = {
      onChange: this.onChange,
    };

    return (
    <Grid className='view-reports'>
      <Row>
        <Col xsOffset={0} xs={12} smOffset={3} sm={6} mdOffset={4} md={4}>
          <Form>
            <div className='text'>
              <Textarea values={textAreaValues} handlers={textAreaHandlers} />
            </div>
            <div className='buttons'>
              <Button text='Send' onClick={this.onSend} className={CSS_MAPPING.BLUE} />
            </div>
          </Form>
        </Col>
      </Row>
    </Grid>
    );
  }
}

export default Reports;