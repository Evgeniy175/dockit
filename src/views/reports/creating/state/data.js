import { action, observable } from 'mobx';
import ReportModel from '../../../../models/reports';
import { browserHistory } from 'react-router';
import { isEmpty } from 'lodash';

import DataState from '../../../../utils/state/data';


const reportModel = new ReportModel();

class ReportCreatingDataState extends DataState {
  @observable value = '';

  reportTypes = {
    events: (eventId, text) => reportModel.addEventReport(eventId, text),
    comments: (commentId, text) => reportModel.addCommentReport(commentId, text),
    schedules: (scheduleId, text) => reportModel.addScheduleReport(scheduleId, text),
  };

  @action
  setValue(value) {
    this.value = value;
  }

  createReport(target, id, text) {
    if (isEmpty(text.trim())) return Promise.reject();

    return this.reportTypes[target](id, text)
    .catch(console.error)
    .finally(() => browserHistory.push(`/feed`));
  }
}


export default ReportCreatingDataState;
