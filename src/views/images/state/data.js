import { action, observable} from 'mobx';
import { observer } from 'mobx-react';

import { recognizeCreationType } from '../../../utils/creations';
import ImageModel from '../../../models/images/index.js';
import {LIMIT} from "../constants"
const imagesModel = new ImageModel();

class ImagesViewDataStore{
  @observable isLoaded = false;
  @observable images = [];
  isCanLoadMore = true;
  id;
  type;

  constructor(props){
    this.id = props.params.id;
    this.type = recognizeCreationType();

    this.fetch();
  }

  fetch(){
    this.setIsLoaded(false);
    return imagesModel.fetchImages(this.id, this.type, this.getConfig())
    .then(result => {
      this.setImages(result.data);
      this.setIsLoaded(true);
    });
  }

  fetchMore() {
    this.setIsLoaded(false);
    return imagesModel.fetchImages(this.id, this.type, this.getConfig())
    .then(images => this.handleSuccessFetchingMore(images))
    .catch(console.error)
    .finally(() => this.setIsLoaded(true));
  }

  handleSuccessFetchingMore(images) {
    this.appendImages(images.data);
    this.isCanLoadMore = images.data.length > 0;
    return Promise.resolve();
  }

  getConfig() {
    return {
      params: {
        offset: this.images.length,
        limit: LIMIT
      },
    };
  }

  clear(){
    this.id = null;
    this.type = null;
    this.isCanLoadMore = null;
    this.setImages(null);
    this.setIsLoaded(null);
  }

  @action
  setImages(value){
    this.images = value;
  }

  @action
  appendImages(value){
    this.images = this.images.concat(value);
  }

  @action
  setIsLoaded(value){
    this.isLoaded = value;
  }
}

export default ImagesViewDataStore;