import React from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';

import Loader from '../../shared-components/loader/index.jsx';
import DataState from './state/data.js';
import ImageItem from '../../shared-components/creations/images/all-images-item/index.jsx';
import WheelHelper from '../../utils/dom/events/scroll-helper';
import { EVENTS } from '../../utils/dom/events/scroll-helper/constants';
import ReachedTheEndText from '../../shared-components/end-text/index.jsx';
import LoaderBottom from '../../shared-components/loader-bottom/index.jsx';

@observer
class ImagesView extends React.Component{
  constructor(props){
    super(props);

    Object.assign(this, {
      data: new DataState(props)
    });

    const onWheel = ::this.onWheelHandler;

    this.loadMoreHelper = new WheelHelper({
      handlers: {
        [EVENTS.WHEEL.name]: onWheel,
        [EVENTS.TOUCH_MOVE.name]: onWheel,
        [EVENTS.KEY_DOWN.name]: onWheel,
      },
    });
  }

  componentWillUnmount(){
    this.onWheel = null;
    this.loadMoreHelper.dispose();
    this.loadMoreHelper = null;
    this.data.clear();
    this.data = null;
  }

  onWheelHandler(){
    if (!this.data.isCanLoadMore || !this.data.isLoaded) return;
    this.data.setIsLoaded(false);
    return this.data.fetchMore()
    .catch(console.error)
    .finally(() => this.data.setIsLoaded(true));
  }

  render(){
    const { isLoaded, images, type, isCanLoadMore } = this.data;

    if(!isLoaded && !images) return <Loader/>;

    return(
      <Grid>
        <Row>
          <Col>
            <div className="all-images-wrapper">
              {images.map(image => <ImageItem target={type} key={image.id} value={image}/>)}
            </div>
            {!isLoaded && isCanLoadMore && <LoaderBottom/>}
          </Col>
        </Row>
        <Row>
          {!isCanLoadMore && <ReachedTheEndText/>}
        </Row>
      </Grid>
    );
  }
}

export default ImagesView;