export const EMPTY_TEXT = 'No notifications yet. Check back soon!';
export const PORTION_SIZE = 50;
export const LOAD_BUFFER_FROM_BOTTOM = 10;
