import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { get } from 'lodash';
import PropTypes from 'prop-types';

import { handleString } from '../../../../../utils/formatting/text';

import EventLink from '../../../../../shared-components/creations/event-link/index.jsx';
import ScheduleLink from '../../../../../shared-components/creations/schedule-link/index.jsx';

import { PARENTS } from '../constants';
import { NOTIFICATION_SUBJECT_TYPES } from '../constants';
import { EMPTY_CHAR } from '../../../../../constants';



@observer
class NotificationSubject extends Component {
  @observable notification;
  
  SUBJECT_RENDERS = {
    [NOTIFICATION_SUBJECT_TYPES.EVENT]: notification => <EventLink event={notification.event} message={this.getFormattedComment(notification)} />,
    [NOTIFICATION_SUBJECT_TYPES.SCHEDULE]: notification => <ScheduleLink schedule={notification.schedule} message={this.getFormattedComment(notification)} />,
    [NOTIFICATION_SUBJECT_TYPES.CALENDAR]: notification => <span>{ notification.calendar.title }</span>
  };

  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    const { notification } = props.values;
    this.setNotification(notification);
  }

  @action
  setNotification(value) {
    this.notification = value;
  }

  render() {
    const notification = this.notification.data;
    const subject = this.getNotificationSubject(notification);
    
    if (!this.SUBJECT_RENDERS[subject]) return null;
    
    return this.SUBJECT_RENDERS[subject](notification);
  }
  
  getNotificationSubject(notification) {
    for (const parent of PARENTS)
      if (notification[parent])
        return parent.toUpperCase();
  }

  getFormattedComment(notification) {
    const comment = get(notification, 'comment') || notification;

    if (get(comment, 'message', EMPTY_CHAR) === EMPTY_CHAR) return null;

    const text = handleString(comment.message, comment.userTags).map((item, index) => <span key={index}>{item}</span>);

    return (
      <span>
        { `: ` }
        { text }
      </span>
    );
  }
}

NotificationSubject.propTypes = {
  values: PropTypes.shape({
    type: PropTypes.string,
    notification: PropTypes.object,
  }).isRequired,
};

export default NotificationSubject;
