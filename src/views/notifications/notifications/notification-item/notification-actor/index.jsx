import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import UsernameLink from '../../../../../shared-components/users/username-link/index.jsx';

import { Auth } from '../../../../../auth';
import { NOTIFICATION_TYPES } from '../constants';



@observer
class NotificationActor extends Component {
  TARGET_MODELS = ['comment', 'event', 'schedule'];
  
  @observable notification;
  type;

  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    const { notification, type } = props.values;
    this.setNotification(notification);
    this.type = type;
  }

  @action
  setNotification(value) {
    this.notification = value;
  }

  render() {
    const notification = this.notification.data;
    const user = Auth.getActiveUser();

    switch(this.type) {
      case NOTIFICATION_TYPES.INVITE:
        return <UsernameLink user={notification.invitee} />;
      case NOTIFICATION_TYPES.FOLLOW:
        return <UsernameLink user={notification.source} />;
      case NOTIFICATION_TYPES.USERTAG:
        const targetUser = this.getTargetUser(notification);
        const isSelf = user.id === targetUser.user_id;
        return <UsernameLink user={targetUser} self={isSelf} />;
      default:
        return <UsernameLink user={notification.user} />;
    }
  }
  
  getTargetUser(notification) {
    for (const model of this.TARGET_MODELS)
      if (notification[model])
        return notification[model].user;
  }
}

NotificationActor.propTypes = {
  values: PropTypes.shape({
    type: PropTypes.string,
    notification: PropTypes.object,
  }).isRequired,
};

export default NotificationActor;
