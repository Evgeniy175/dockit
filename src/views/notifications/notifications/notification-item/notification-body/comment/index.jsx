import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { handleString } from '../../../../../../utils/formatting/text';
import { NOTIFICATION_BODIES, NOTIFICATION_TYPES } from '../../constants';
import { EMPTY_CHAR } from '../../../../../../constants';

import { Auth } from '../../../../../../auth';



@observer
class NotificationCommentBody extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const notification = this.props.notification;
    const userId = notification.data.comment.user_id;
    const currentUser = Auth.getActiveUser();
    const isCurrentUser = currentUser.id === userId;
    const isContainsImage = !!notification.data.imageUrl;

    if (!isCurrentUser) return this.renderWithComment(NOTIFICATION_BODIES[NOTIFICATION_TYPES.LIKE].TAGGED_COMMENT, notification);

    return isContainsImage
    ? <span> { NOTIFICATION_BODIES[NOTIFICATION_TYPES.LIKE].COMMENT.CREATE() } </span>
    : this.renderWithComment(NOTIFICATION_BODIES[NOTIFICATION_TYPES.LIKE].COMMENT, notification);
  }
  
  renderWithComment(resolver, notification) {
    const comment = get(notification, 'data.comment') || notification.data;
    const isTextExists = comment.message !== EMPTY_CHAR;

    return (
      <span>
        { ' ' }
        { isTextExists ? resolver.CREATE() : resolver.CREATE_NO_IMAGE() }
        { ' ' }
        { this.getCommentText(notification.data) }
      </span>
    );
  }
  
  getCommentText(notification) {
    const comment = get(notification, 'comment');
  
    if (!get(comment, 'message.length')) return null;

    return handleString(comment.message, comment.userTags).map((item, index) => <span key={index}>{item}</span>);
  }
}

NotificationCommentBody.propTypes = {
  notification: PropTypes.object.isRequired
};

export default NotificationCommentBody;
