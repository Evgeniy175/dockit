import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { get } from 'lodash';
import PropTypes from 'prop-types';

import { NOTIFICATION_TYPES } from '../constants';
import { NOTIFICATION_BODIES } from '../constants';

import EventSchedule from './event-schedule/index.jsx';
import Comment from './comment/index.jsx';
import Usertag from './usertag/index.jsx';



@observer
class NotificationBody extends Component {
  @observable notification;
  subject;
  action;

  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    const { subject, action, notification } = props.values;
    this.subject = subject;
    this.action = action;
    this.setNotification(notification);
  }

  @action
  setNotification(value) {
    this.notification = value;
  }

  render() {
    const type = get(this.notification, 'type', '');

    switch(type) {
      case NOTIFICATION_TYPES.EVENT:
      case NOTIFICATION_TYPES.SCHEDULE:
        return <EventSchedule notification={this.notification} type={type} />;
      case NOTIFICATION_TYPES.LIKE:
        return this.subject.toLowerCase() == NOTIFICATION_TYPES.COMMENT
        ? <Comment notification={this.notification} />
        : <span> { NOTIFICATION_BODIES[type][this.subject][this.action](this.notification) } </span>;
      case NOTIFICATION_TYPES.USERTAG:
        return this.subject.toLowerCase() === NOTIFICATION_TYPES.COMMENT
        ? <Usertag notification={this.notification} />
        : <span> { NOTIFICATION_BODIES[type][this.subject][this.action](this.notification) } </span>;
      default:
        const resolver = get(NOTIFICATION_BODIES, `${type}.${this.subject}.${this.action}`);
        return resolver ? <span> { resolver(this.notification) } </span> : null;
    }
  }
}

NotificationBody.propTypes = {
  values: PropTypes.shape({
    subject: PropTypes.string,
    action: PropTypes.string,
    type: PropTypes.string,
    notification: PropTypes.object,
  }).isRequired,
};

export default NotificationBody;
