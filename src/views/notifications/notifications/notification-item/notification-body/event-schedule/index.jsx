import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { NOTIFICATION_TYPES } from '../../constants';
import { NOTIFICATION_BODIES } from '../../constants';

import EventLink from '../../../../../../shared-components/creations/event-link/index.jsx';
import ScheduleLink from '../../../../../../shared-components/creations/schedule-link/index.jsx';

@observer
class NotificationEventScheduleBody extends Component {
  render() {
    const notification = this.props.notification;
    const type = this.props.type;
    const action = notification.action.toUpperCase();

    switch(type) {
      case NOTIFICATION_TYPES.EVENT:
        return (
          <span>
            <span> { NOTIFICATION_BODIES[NOTIFICATION_TYPES.EVENT][action](notification) } </span>
            <EventLink event={notification.data} />
          </span>
        );
      case NOTIFICATION_TYPES.SCHEDULE:
        return (
          <span>
            <span> { NOTIFICATION_BODIES[NOTIFICATION_TYPES.SCHEDULE][action](notification) } </span>
            <ScheduleLink schedule={notification.data} />
          </span>
        );
      default:
        return null;
    }
  }
}

NotificationEventScheduleBody.propTypes = {
  type: PropTypes.string.isRequired,
  notification: PropTypes.object.isRequired
};

export default NotificationEventScheduleBody;
