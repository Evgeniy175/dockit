import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { handleString } from '../../../../../../utils/formatting/text';
import { NOTIFICATION_BODIES, NOTIFICATION_TYPES } from '../../constants';



@observer
class NotificationUsertagBody extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const notification = this.props.notification;
    return <span> { NOTIFICATION_BODIES[NOTIFICATION_TYPES.USERTAG].COMMENT.CREATE(notification) } { this.getCommentText(notification.data) }</span>;
  }
  
  getCommentText(notification) {
    const comment = get(notification, 'comment') || notification;
    if (!comment || !comment.message) return null;
    return handleString(comment.message, comment.userTags).map((item, index) => <span key={index}>{item}</span>);
  }
}

NotificationUsertagBody.propTypes = {
  notification: PropTypes.object.isRequired
};

export default NotificationUsertagBody;
