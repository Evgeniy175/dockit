import { get } from 'lodash';

import { TYPES } from '../../../../models/intents/constants';

import { MODEL as COMMENT_MODEL } from '../../../../models/comments/constants';
import { MODEL as EVENT_MODEL } from '../../../../models/events/constants';
import { MODEL as SCHEDULE_MODEL } from '../../../../models/schedules/constants';

const FIELDS_FOR_UPDATE = {
  TITLE: 'title',
  LOCATION: 'location_id',
  START_TIME: 'startTime',
  END_TIME: 'endTime',
  DISCOVERABLE: 'discoverable',
};

const EVENT_FIELDS_NAMES = {
  [FIELDS_FOR_UPDATE.TITLE]: 'name',
  [FIELDS_FOR_UPDATE.LOCATION]: 'location',
  [FIELDS_FOR_UPDATE.START_TIME]: 'time',
  [FIELDS_FOR_UPDATE.END_TIME]: 'end time',
  [FIELDS_FOR_UPDATE.DISCOVERABLE]: 'discoverable',
};

const SCHEDULE_FIELDS_NAMES = {
  [FIELDS_FOR_UPDATE.TITLE]: 'name',
  [FIELDS_FOR_UPDATE.LOCATION]: 'location',
  [FIELDS_FOR_UPDATE.START_TIME]: 'time',
  [FIELDS_FOR_UPDATE.END_TIME]: 'end time',
  [FIELDS_FOR_UPDATE.DISCOVERABLE]: 'discoverable',
};

export const NOTIFICATION_TYPES = {
  COMMENT: COMMENT_MODEL,
  DOCK: 'dock',
  EVENT: EVENT_MODEL,
  SCHEDULE: SCHEDULE_MODEL,
  USERTAG: 'usertag',
  INTENT: 'intent',
  LIKE: 'like',
};

export const HOVERABLE_NOTIFICATIONS = [
  NOTIFICATION_TYPES.COMMENT,
  NOTIFICATION_TYPES.DOCK,
  NOTIFICATION_TYPES.EVENT,
  NOTIFICATION_TYPES.SCHEDULE,
  NOTIFICATION_TYPES.USERTAG,
  NOTIFICATION_TYPES.INTENT,
  NOTIFICATION_TYPES.LIKE,
];

export const NOTIFICATION_BODIES = {
  [NOTIFICATION_TYPES.DOCK]: {
    EVENT: {
      CREATE: () => 'docked your event:'
    },
    SCHEDULE: {
      CREATE: () => 'docked your schedule:'
    }
  },
  [NOTIFICATION_TYPES.LIKE]: {
    SCHEDULE: {
      CREATE: () => 'liked your schedule:'
    },
    EVENT: {
      CREATE: () => 'liked your event:'
    },
    COMMENT: {
      CREATE: () => 'liked your comment:',
      CREATE_NO_IMAGE: () => 'liked your comment',
    },
    TAGGED_COMMENT: {
      CREATE: () => 'liked a comment you were tagged in:'
    },
    CALENDAR: {
      CREATE: () => 'liked your calendar:'
    }
  },
  [NOTIFICATION_TYPES.COMMENT]: {
    EVENT: {
      CREATE: () => 'commented on'
    },
    SCHEDULE: {
      CREATE: () => 'commented on'
    },
  },
  [NOTIFICATION_TYPES.USERTAG]: {
    EVENT: {
      CREATE: () => 'mentioned you in an event:'
    },
    SCHEDULE: {
      CREATE: () => 'mentioned you in a schedule:'
    },
    COMMENT: {
      CREATE: notification => {
        const isPost = get(notification, 'data.comment.target_user_id');
        return `mentioned you in a ${isPost ? 'post' : 'comment'}:`
      }
    }
  },
  [NOTIFICATION_TYPES.EVENT]: {
    UPDATE: notification => `updated the ${EVENT_FIELDS_NAMES[notification.changes.name]} of:`
  },
  [NOTIFICATION_TYPES.SCHEDULE]: {
    UPDATE: notification => `updated the ${SCHEDULE_FIELDS_NAMES[notification.changes.name]} of:`
  },
  [NOTIFICATION_TYPES.INTENT]: {
    EVENT: {
      CREATE: notification => INTENT_TEXTS[notification.data.intent],
      UPDATE: notification => INTENT_TEXTS[notification.data.intent],
    },
    SCHEDULE: {
      CREATE: notification => INTENT_TEXTS[notification.data.intent],
      UPDATE: notification => INTENT_TEXTS[notification.data.intent],
    },
  },
};

const INTENT_TEXTS = {
  [TYPES.DOCKED]: `docked`,
  [TYPES.NOT_GOING]: `responded 'can't go' to`,
  [TYPES.MAYBE]: `responded 'maybe' to`,
};

export const NOTIFICATION_SUBJECT_TYPES = {
  EVENT: 'EVENT',
  SCHEDULE: 'SCHEDULE',
  COMMENT: 'COMMENT',
  CALENDAR: 'CALENDAR',
};

export const PARENTS = [
  'comment',
  'dock',
  'event',
  'usertag',
  'like',
  'schedule',
];