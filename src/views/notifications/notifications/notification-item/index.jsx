import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import DataState from './state/data';

import NotificationActor from './notification-actor/index.jsx';
import NotificationBody from './notification-body/index.jsx';
import NotificationSubject from './notification-subject/index.jsx';

import jq from '../../../../utils/jquery';

import { HOVERABLE_NOTIFICATIONS } from './constants';



@observer
class NotificationItem extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState(props),
    }));

    this.onClick = ::this.onClickHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  onClickHandler(e) {
    if (jq.wrap(e.target).is('a')) return;
    this.data.onClickHandler();
  }

  render() {
    const profilePicture = this.data.getUserPicture();
    const { notification, action, subject, type } = this.data;
    const isHoverable = HOVERABLE_NOTIFICATIONS.includes(type);
    const className = `notification-item${isHoverable ? ' hoverable' : ''}`;

    const values = {
      subject,
      action,
      notification,
      type,
    };

    return (
      <ul className={className} onClick={this.onClick}>
        <div className='content'>
          <div className='image-container inline'>
            <img className='image' src={profilePicture} />
          </div>
          <div className='notification-data inline'>
            <div className='notification-text'>
              <NotificationActor values={values} />
              <NotificationBody values={values} />
              <NotificationSubject values={values} />
            </div>
            <div className='time-ago'>{this.data.getTimeAgo()}</div>
          </div>
        </div>
      </ul>
    );
  }
}

NotificationItem.propTypes = {
  notification: PropTypes.object.isRequired,
};

export default NotificationItem;
