import { action, observable, toJS } from 'mobx';
import { get } from 'lodash';
import moment from 'moment';
import { browserHistory } from 'react-router';

import { formatFromNow } from '../../../../../utils/formatting/time';

import UserModel from '../../../../../models/users/index';
import CommentModel from '../../../../../models/comments';
import LikeModel from '../../../../../models/action-commands/likes';

import DataState from '../../../../../utils/state/data';

import { PARENTS, NOTIFICATION_TYPES } from '../constants';
import { MODEL as EVENT_MODEL } from '../../../../../models/events/constants';
import { MODEL as SCHEDULE_MODEL } from '../../../../../models/schedules/constants';



class NotificationItemDataState extends DataState {
  @observable notification;
  subject;
  action;
  type;

  constructor(props) {
    super(props);
    this.init(props);

    this.targetResolvers = {
      [NOTIFICATION_TYPES.COMMENT]: data => CommentModel.getTarget(data),
      [NOTIFICATION_TYPES.EVENT]: () => EVENT_MODEL,
      [NOTIFICATION_TYPES.SCHEDULE]: () => SCHEDULE_MODEL,
      [NOTIFICATION_TYPES.INTENT]: data => CommentModel.getTarget(data),
      [NOTIFICATION_TYPES.LIKE]: data => LikeModel.getTarget(data),
      [NOTIFICATION_TYPES.USERTAG]: data => LikeModel.getTarget(data),
    };

    this.uriResolvers = {
      [NOTIFICATION_TYPES.COMMENT]: (data, target) => `/${target}s/${data[`${target}_id`]}?commentId=${data.id}`,
      [NOTIFICATION_TYPES.EVENT]: (data, target, id) => `/${target}s/${id || data.id}`,
      [NOTIFICATION_TYPES.SCHEDULE]: (data, target, id) => `/${target}s/${id || data.id}`,
      [NOTIFICATION_TYPES.INTENT]: (data, target) => `/${target}s/${data[`${target}_id`]}`,
      [NOTIFICATION_TYPES.LIKE]: () => this.onItemClick(),
      [NOTIFICATION_TYPES.USERTAG]: () => this.onItemClick(),
    };

    this.onLikeClickHandlers = {
      [NOTIFICATION_TYPES.EVENT]: ::this.onEventLikeClick,
      [NOTIFICATION_TYPES.SCHEDULE]: ::this.onScheduleLikeClick,
      [NOTIFICATION_TYPES.COMMENT]: ::this.onCommentLikeClick,
    };
  }

  init(props) {
    this.setNotification(props.notification);
    this.subject = this.getSubject();
    this.action = this.getAction();
    this.type = get(this.notification, 'type');
  }

  getSubject() {
    const notification = this.notification.data;

    for (let subject of PARENTS)
      if (notification[subject])
        return subject.toUpperCase();
  }

  getAction() {
    return this.notification.action.toUpperCase();
  }

  getTimeAgo() {
    const time = moment(this.notification.timestamp);
    return formatFromNow(time);
  }

  getUserPicture() {
    const picture = get(this.notification, `data.user.profilePicture`, null);
    return UserModel.formatImageUrl(picture);
  }

  @action
  setNotification(value) {
    this.notification = value;
  }

  onClickHandler(type = this.type, incomingData = null, incomingTarget = null, id) {
    const data = incomingData || get(this.notification, 'data');
    const target = incomingTarget || this.targetResolvers[type](data);
    const uri = this.uriResolvers[type](data, target, id);
    if (uri) browserHistory.push(uri);
  }

  onItemClick() {
    const data = get(this.notification, 'data');
    const target = LikeModel.getTarget(data);
    if (this.onLikeClickHandlers[target]) this.onLikeClickHandlers[target]();
  }

  onCommentLikeClick() {
    const data = get(this.notification, 'data.comment');
    const target = CommentModel.getTargetThrowId(data);
    this.onClickHandler(NOTIFICATION_TYPES.COMMENT, data, target);
  }

  onEventLikeClick() {
    this.onClickHandler(NOTIFICATION_TYPES.EVENT, null, null, get(this.notification, `data.${EVENT_MODEL}_id`));
  }

  onScheduleLikeClick() {
    this.onClickHandler(NOTIFICATION_TYPES.SCHEDULE, null, null, get(this.notification, `data.${SCHEDULE_MODEL}_id`));
  }
}

export default NotificationItemDataState;
