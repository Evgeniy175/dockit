import { action, observable, toJS } from 'mobx';

import DataState from '../../../../utils/state/data';
import Refresher from '../../../../utils/refresher';

import NotificationModel from '../../../../models/notifications';

import { PORTION_SIZE } from '../constants';
import { PARENTS } from '../../notifications/notification-item/constants';

const notificationModel = new NotificationModel();



class ViewNotificationsDataState extends DataState {
  @observable notifications = [];
  nextPage;
  isBusy = false;

  // due to sometimes several items can disappear. E.g. you requested 20, returns 19
  refetchCount = 0;
  refresher;

  get isLoadMoreAvailable() {
    return !!this.nextPage;
  }

  constructor(props = {}) {
    super(props);
    const refresherConfig = {
      cb: ::this.refreshData,
    };
    this.refresher = new Refresher(refresherConfig);
  }

  refreshData() {
    const config = {
      params: {
        limit: this.refetchCount,
        query: {
          request: false,
        },
      },
    };

    return notificationModel.notification(config)
    .then(res => {
      this.parseResponse(res);
      return Promise.resolve();
    })
    .catch(console.error);
  }

  isNotificationsEmpty() {
    return !this.notifications.length;
  }

  getNotificationBase(notification) {
    for(let base of PARENTS)
      if(notification[base]) return base;
  }

  isLikesFromOneGroup(likeA, likeB) {
    const baseA = this.getNotificationBase(likeA.data);
    const baseB = this.getNotificationBase(likeB.data);
    return baseA === baseB && likeA.data[baseA].id === likeB.data[baseB].id;
  }

  fetchNotifications() {
    const config = {
      params: {
        limit: PORTION_SIZE,
        query: {
          request: false,
        },
      },
    };
    this.refetchCount = PORTION_SIZE;
    return notificationModel.notification(config)
    .then(res => {
      this.parseResponse(res);
      return Promise.resolve();
    });
  }

  fetchMoreNotifications() {
    const config = {
      params: {
        page: this.nextPage,
        limit: PORTION_SIZE,
        query: {
          request: false,
        },
      },
    };

    this.refetchCount += PORTION_SIZE;
    return notificationModel.notification(config)
    .then(res => {
      this.attachNotifications(res);
      return Promise.resolve();
    });
  }

  markReadNotifications() {
    return notificationModel.markReadNotifications();
  }

  @action
  parseResponse(res) {
    this.nextPage = res.nextPage;
    this.notifications = res.data;
  }

  @action
  attachNotifications(res) {
    this.nextPage = res.nextPage;
    this.notifications = this.notifications.concat(res.data);
  }
}

export default ViewNotificationsDataState;
