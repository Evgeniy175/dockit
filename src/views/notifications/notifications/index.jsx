import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';
import { get, intersection } from 'lodash';

import NotificationItem from './notification-item/index.jsx';
import Loader from '../../../shared-components/loader/index.jsx';
import EmptyText from '../../../shared-components/empty-text/index.jsx';

import jq from '../../../utils/jquery';

import DataState from './state/data';
import UiState from './state/ui';

import { EMPTY_TEXT } from './constants';
import { PARENTS } from './notification-item/constants';

import { handleButton } from '../../../utils/scrolling';



@observer
class Notifications extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
      ui: new UiState()
    });
    setTitle(PAGE_TITLES[PAGES.NOTIFICATIONS]());

    this.getContainer = ::this.getContainerHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
    this.onWheel = ::this.onWheelHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
  }

  componentWillMount() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);

    this.fetch();
  }

  componentWillUnmount() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.nOfNotifications === 0 || !this.ui.isLoaded) return;
    this.fetch();
  }

  onWheelHandler() {
    const isNeedToLoad = this.ui.isNeedToLoadMore();

    if (!isNeedToLoad || this.data.isBusy || !this.data.isLoadMoreAvailable) return;
    this.data.isBusy = true;
    return this.data.fetchMoreNotifications()
    .catch(console.error)
    .finally(() => this.data.isBusy = false);
  }

  onKeyDownHandler(e) {
    const target = jq.wrap(e.target);
    const isForSearch = target.hasClass('navbar-search-box-container') || target.hasParents('.navbar-search-box-container');
    const isModalTarget = target.hasClass('modal');
    return isModalTarget || isForSearch ? null : handleButton(e, this.getContainer, this.onButtonScroll);
  }

  fetch() {
    if (!this.ui.isLoaded) return Promise.resolve();
    this.ui.setIsLoaded(false);
    return this.data.fetchNotifications().then(() => {
      const handler = get(this.props, 'handlers.onNotificationsRead');
      if (handler) handler();
      this.ui.setIsLoaded(true);
      return this.data.markReadNotifications();
    })
    .catch(console.error);
  }

  getContainerHandler() {
    return document.body;
  }

  onButtonScrollHandler() {
    this.onWheel();
  }

  render() {
    const isLoaded = this.ui.isLoaded;
    const notifications = this.data.notifications;

    if (isLoaded && this.data.isNotificationsEmpty()) return <EmptyText text={EMPTY_TEXT} />;

    const className = `notifications-wrapper${isLoaded && notifications.length > 0 ? ' wrapper-border' : ''}`;
    const filteredNotifications = notifications.filter(notification => intersection(Object.keys(notification.data), PARENTS).length > 0);

    return (
      <div className={className}>
        { !isLoaded && <Loader /> }
        { filteredNotifications.map(notification => <NotificationItem key={`${notification.timestamp}_${notification.data.id}`} notification={notification} />) }
      </div>
    );
  }
}

export default Notifications;