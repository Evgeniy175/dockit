import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';
import { get } from 'lodash';

import RequestsItem from './request-item/index.jsx';
import Loader from '../../../shared-components/loader/index.jsx';
import EmptyText from '../../../shared-components/empty-text/index.jsx';

import jq from '../../../utils/jquery';

import DataState from './state/data';
import UiState from './state/ui';

import { handleButton } from '../../../utils/scrolling';

import { EMPTY_TEXT } from './constants';



@observer
class Requests extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
      ui: new UiState()
    });
    setTitle(PAGE_TITLES[PAGES.REQUESTS]());

    this.getContainer = ::this.getContainerHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
    this.onWheel = ::this.onWheelHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
  }

  componentDidMount() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
    document.body.addEventListener('keydown', this.onKeyDown);

    this.fetch();
  }

  componentWillUnmount() {
    document.body.removeEventListener('keydown', this.onKeyDown);
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.nOfRequests === 0 || !this.ui.isLoaded) return;
    this.fetch();
  }

  getContainerHandler() {
    return document.body;
  }

  onButtonScrollHandler() {
    this.onWheel();
  }

  onWheelHandler() {
    const isNeedToLoad = this.ui.isNeedToLoadMore();

    if (!isNeedToLoad || this.data.isBusy || !this.data.isLoadMoreAvailable) return;

    this.data.isBusy = true;
    return this.data.fetchMoreRequests()
    .catch(console.error)
    .finally(() => this.data.isBusy = false);
  }

  onKeyDownHandler(e) {
    const target = jq.wrap(e.target);
    const isForSearch = target.hasClass('navbar-search-box-container') || target.hasParents('.navbar-search-box-container');
    const isModalTarget = target.hasClass('modal');
    return isModalTarget || isForSearch ? null : handleButton(e, this.getContainer, this.onButtonScroll);
  }

  fetch() {
    this.ui.setIsLoaded(false);
    return this.data.fetchRequests().then(() => {
      const handler = get(this.props, 'handlers.onRequestsRead');
      if (handler) handler();
      this.ui.setIsLoaded(true);
      return this.data.markReadRequests();
    });
  }

  render() {
    const isLoaded = this.ui.isLoaded;
    const requests = this.data.requests;

    if (isLoaded && this.data.isRequestsEmpty()) return <EmptyText text={EMPTY_TEXT} />;

    const className = `requests-wrapper${isLoaded && requests.length > 0 ? ' wrapper-border' : ''}`;

    return (
      <div className={className}>
        { !isLoaded && <Loader /> }
        {
          requests.map(request => <RequestsItem key={`${request.timestamp}_${request.data.id}`} request={request} onRemoveRequest={this.data.removeRequest} />)
        }
      </div>
    );
  }
}

export default Requests;