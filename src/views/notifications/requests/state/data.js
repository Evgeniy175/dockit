import { action, observable, toJS } from 'mobx';

import DataState from '../../../../utils/state/data';
import Refresher from '../../../../utils/refresher';

import { PORTION_SIZE } from '../constants';

import NotificationModel from '../../../../models/notifications';

const notificationModel = new NotificationModel();



class ViewRequestsDataState extends DataState {
  @observable requests = [];
  nextPage;
  isBusy = false;

  // due to sometimes several items can disappear. E.g. you requested 20, returns 19
  refetchCount = 0;
  refresher;

  get isLoadMoreAvailable() {
    return !!this.nextPage;
  }

  constructor(props = {}) {
    super(props);

    this.removeRequest = ::this.handleRemoveRequest;

    const refresherConfig = {
      cb: ::this.refreshData,
    };
    this.refresher = new Refresher(refresherConfig);
  }

  refreshData() {
    const config = {
      params: {
        limit: this.refetchCount,
        query: {
          request: true,
        },
      },
    };
    return notificationModel.notification(config)
    .then(res => {
      this.parseResponse(res);
      return Promise.resolve();
    })
    .catch(console.error);
  }

  isRequestsEmpty() {
    return !this.requests.length;
  }

  fetchRequests() {
    const config = {
      params: {
        limit: PORTION_SIZE,
        query: {
          request: true,
        },
      },
    };
    this.refetchCount = PORTION_SIZE;
    return notificationModel.notification(config)
    .then(res => {
      this.parseResponse(res);
      return Promise.resolve();
    });
  }

  fetchMoreRequests() {
    const config = {
      params: {
        page: this.nextPage,
        limit: PORTION_SIZE,
        query: {
          request: true,
        },
      },
    };
    this.refetchCount += PORTION_SIZE;
    return notificationModel.notification(config)
    .then(res => {
      this.attachRequests(res);
      return Promise.resolve();
    });
  }

  markReadRequests() {
    return notificationModel.markReadRequests();
  }

  @action
  handleRemoveRequest(id) {
    this.requests = this.requests.filter(request => request.data.id !== id);
  }

  @action
  parseResponse(res) {
    this.nextPage = res.nextPage;
    this.requests = res.data;
  }

  @action
  attachRequests(res) {
    this.nextPage = res.nextPage;
    this.requests = this.requests.concat(res.data);
  }
}

export default ViewRequestsDataState;
