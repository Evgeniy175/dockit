import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import AcceptedIcon from '../../../../../../assets/images/icons/green-check.png';
import AcceptIcon from '../../../../../../assets/images/icons/check.svg';
import RejectIcon from '../../../../../../assets/images/icons/reject.svg';

import FollowModel from './../../../../../../models/follows/index';

const followModel = new FollowModel();

@observer
class AcceptBlock extends Component {
  @observable request;

  constructor(props) {
    super(props);
    this.setRequest(props.request);
    this.accept = ::this.handleAccept;
    this.reject = ::this.handleReject;
  }

  handleAccept() {
    return followModel.accept(this.request.data.source.id)
    .then(res => this.setAccepted(true));
  }

  handleReject() {
    const id = this.request.data.id;
    return followModel.reject(id)
    .then(() => {
      this.setAccepted(false);
      this.props.onReject(id);
    });
  }

  @action
  setRequest(value) {
    this.request = value;
  }

  @action
  setAccepted(value) {
    this.request.data.accepted = value;
  }

  render() {
    return this.isAccepted()
    ? (
      <div className='accept-block'>
        <img className='accepted' src={AcceptedIcon} />
      </div>
    )
    : (
      <div className='accept-block btn-list'>
        <img className='reject' src={RejectIcon} onClick={this.reject} />
        <img className='accept' src={AcceptIcon} onClick={this.accept} />
      </div>
    );
  }

  isAccepted() {
    return this.request.data.accepted;
  }
}

AcceptBlock.propTypes = {
  request: PropTypes.object.isRequired,
};

export default AcceptBlock;
