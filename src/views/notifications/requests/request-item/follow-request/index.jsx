import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { Auth } from '../../../../../auth';

import UserImage from './../../../../../shared-components/creations/event-member-image/index.jsx';
import Userlink from './../../../../../shared-components/users/username-link/index.jsx';
import AcceptBlock from './accept-block/index.jsx';



@observer
class FollowRequest extends Component {
  @observable request;

  constructor(props) {
    super(props);
    this.setRequest(props.request);
  }

  render() {
    const user = Auth.getActiveUser();
    
    return (
      <div className='request-content flexible'>
        <div className='default-wrapper'>
          <div className='left'>
            <UserImage user={this.request.data.source} />
          </div>
          <div className='request-text'><Userlink user={this.request.data.source} />{ this.resolveText(user) }</div>
        </div>
        <AcceptBlock request={this.request} onReject={this.props.onRemoveRequest} />
      </div>
    );
  }
  
  resolveText(user) {
    return user.isPublic ? ` followed you` : ` requested to follow you`;
  };

  @action
  setRequest(value) {
    this.request = value;
  }
}

FollowRequest.propTypes = {
  request: PropTypes.object.isRequired,
};

export default FollowRequest;
