import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import UserImage from './../../../../../shared-components/creations/event-member-image/index.jsx';
import UsernameLink from './../../../../../shared-components/users/username-link/index.jsx';
import AcceptBlock from '../follow-request/accept-block/index.jsx';

@observer
class AcceptedFollowRequest extends Component {
  @observable request;

  constructor(props) {
    super(props);
    this.setRequest(props.request);
  }

  render() {
    return (
      <div className='request-content flexible'>
        <div className='default-wrapper'>
          <div className='left'>
            <UserImage user={this.request.data.target} />
          </div>
          <div className='request-text'><UsernameLink user={this.request.data.target} /> accepted your follow request </div>
        </div>
        <AcceptBlock request={this.request} />
      </div>
    );
  }

  @action
  setRequest(value) {
    this.request = value;
  }
}

AcceptedFollowRequest.propTypes = {
  request: PropTypes.object.isRequired
};

export default AcceptedFollowRequest;
