import {action, observable, toJS} from 'mobx';

import DataState from '../../../../../utils/state/data';

class RequestItemDataState extends DataState {
  @observable request;

  constructor(props = { request: null }) {
    super(props);
    this.setRequest(props.request);
  }

  @action
  setRequest(value) {
    this.request = value;
  }
}

export default RequestItemDataState;
