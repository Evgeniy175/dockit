import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import RequestItemDataState from './state/data';

import FollowRequest from './follow-request/index.jsx';
import AcceptedFollowRequest from './accepted-follow-request/index.jsx';
import EventInviteRequest from './invite-request/index.jsx';

import { TYPES } from '../../../creations/constants';

const REQUEST_TYPES = {
  INVITE: 'INVITE',
  FOLLOW: 'FOLLOW',
};

const PARENTS = [
  'dock',
  'event',
  'intent',
  'invite',
  'usertag',
  'comment',
  'like',
  'follow',
  'schedule'
];

@observer
class RequestItem extends Component {
  REQUEST_RESOLVERS = {
    [REQUEST_TYPES.FOLLOW]: (action, request) => this.TILE_TEMPLATES[REQUEST_TYPES.FOLLOW][action](request),
    [REQUEST_TYPES.INVITE]: (action, request, base) => this.TILE_TEMPLATES[REQUEST_TYPES.INVITE][base][action](request)
  };
  
  TILE_TEMPLATES = {
    FOLLOW: {
      CREATE: (item) => <FollowRequest request={item} onRemoveRequest={this.props.onRemoveRequest} />,
      UPDATE: (item) => <AcceptedFollowRequest request={item} />
    },
    INVITE: {
      EVENT: {
        CREATE: (item) => <EventInviteRequest request={item} type={TYPES.EVENT} />
      },
      SCHEDULE: {
        CREATE: (item) => <EventInviteRequest request={item} type={TYPES.SCHEDULE} />
      }
    }
  };

  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new RequestItemDataState(props)
    }));
  }

  render() {
    return (
      <div className='request-item'>
        <div className='content'>
          { this.getRequestContent() }
        </div>
      </div>
    );
  }
  
  getRequestContent() {
    const request = this.data.request;
    const type = request.type.toUpperCase();
    const action = request.action.toUpperCase();
    const base = (this.getBaseRequest(request.data) || '').toUpperCase();
    return this.REQUEST_RESOLVERS[type] ? this.REQUEST_RESOLVERS[type](action, request, base) : null;
  }
  
  getBaseRequest(request) {
    for (let base of PARENTS)
      if (request[base])
        return base.toUpperCase();
  }
}

RequestItem.propTypes = {
  request: PropTypes.object.isRequired,
};

export default RequestItem;
