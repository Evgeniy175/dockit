import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import IntentModel from './../../../../../models/intents/index';

import UserImage from './../../../../../shared-components/creations/event-member-image/index.jsx';
import UsernameLink from './../../../../../shared-components/users/username-link/index.jsx';
import EventLink from './../../../../../shared-components/creations/event-link/index.jsx';
import ScheduleLink from './../../../../../shared-components/creations/schedule-link/index.jsx';
import IntentPanel from './../../../../../shared-components/notifications/requests/intent-panel/index.jsx';

import { TYPES } from '../../../../creations/constants';

const intentModel = new IntentModel();



@observer
class EventInviteRequest extends Component {
  @observable request;
  
  INTENT_RESOLVERS = {
    [TYPES.EVENT]: (id, intent) => intentModel.intentEvent(id, intent),
    [TYPES.SCHEDULE]: (id, intent) => intentModel.intentSchedule(id, intent)
  };
  
  ITEM_RESOLVERS = {
    [TYPES.EVENT]: () => this.request.data.event,
    [TYPES.SCHEDULE]: () => this.request.data.schedule
  };
  
  LINK_RESOLVERS = {
    [TYPES.EVENT]: (event) => <EventLink event={event} />,
    [TYPES.SCHEDULE]: (schedule) => <ScheduleLink schedule={schedule} />
  };

  constructor(props) {
    super(props);
    this.setRequest(props.request);
  }

  componentWillReceiveProps(nextProps) {
    this.setRequest(nextProps.request);
  }
  
  @action
  setRequest(value) {
    this.request = value;
  }

  render() {
    const { type } = this.props;
    const item = this.ITEM_RESOLVERS[type]();
    const link = this.LINK_RESOLVERS[type](item);
    const invitee = get(this.request, 'data.invitee');

    const intentPanelData = {
      values: {
        data: get(this.request, 'data'),
        type,
        dockData: this.getDockData(type),
      },
    };
    
    return (
      <div className='request-content'>
        <div className='default-wrapper'>
          <div className='left'>
            <UserImage user={invitee} />
          </div>
          <div className='request-text'><UsernameLink user={invitee} /> invited you to: { link }</div>
        </div>
        <IntentPanel values={intentPanelData.values} />
      </div>
    );
  }
  
  // todo Evgen: after API fix reduce this
  getDockData(type) {
    switch (type) {
      case TYPES.EVENT:
        return get(this.request, 'data.decorated.docks', {});
      case TYPES.SCHEDULE:
        return get(this.request, 'data.schedule.decorated.docks', {});
      default:
        return null;
    }
  }
}

EventInviteRequest.propTypes = {
  request: PropTypes.object.isRequired,
  type: PropTypes.string
};

export default EventInviteRequest;
