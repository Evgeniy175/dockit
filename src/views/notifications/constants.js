export const TABS_KEYS = {
  NOTIFICATIONS: 'notifications',
  REQUESTS: 'requests',
};

export const TABS = [
  {
    key: TABS_KEYS.NOTIFICATIONS,
    uri: '/notifications/notifications',
  },
  {
    key: TABS_KEYS.REQUESTS,
    uri: '/notifications/requests',
  },
];

export const DEFAULT_ACTIVE_TAB = TABS[0];

export const SWIPE_EVENT_DELTA = 20;
