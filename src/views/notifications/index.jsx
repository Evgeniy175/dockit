import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';
import { Link } from 'react-router';
import { Col, Row, Grid } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

import SwipeHandler from '../../utils/swipe';

import { SWIPE_EVENT_DELTA, TABS, DEFAULT_ACTIVE_TAB } from './constants';
import { HANDLERS, DIRECTIONS } from '../../utils/swipe/constants';

import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';

import NotificationsIcon from '../../assets/images/icons/notifications-marker.png';

const swipeHandler = new SwipeHandler();



@observer
class Notifications extends Component {
  activeTab = DEFAULT_ACTIVE_TAB;
  swipeHandlers;
  isSwipeEnded = true;

  constructor(props) {
    super(props);
    this.swipeHandlers = {
      [HANDLERS[DIRECTIONS.RIGHT]]: ::this.onSwipeRightHandler,
      [HANDLERS[DIRECTIONS.LEFT]]: ::this.onSwipeLeftHandler,
      [HANDLERS.END]: ::this.onEndHandler,
    };
  }

  componentDidMount() {
    swipeHandler.init(this.getContainer(), this.swipeHandlers, SWIPE_EVENT_DELTA);
    this.handleActiveTabChange();
  }

  componentWillUnmount() {
    swipeHandler.dispose();
  }

  getContainer() {
    return document.getElementById('notifications-container');
  }

  onSwipeRightHandler() {
    if (!this.isSwipeEnded) return;
    const activeTabIdx = this.getActiveTabIdx();
    if (activeTabIdx === 0) return;
    this.isSwipeEnded = false;
    this.activeTab = TABS[activeTabIdx - 1];
    this.handleActiveTabChange();
  }

  onSwipeLeftHandler() {
    if (!this.isSwipeEnded) return;
    const activeTabIdx = this.getActiveTabIdx();
    if (activeTabIdx === TABS.length - 1) return;
    this.isSwipeEnded = false;
    this.activeTab = TABS[activeTabIdx + 1];
    this.handleActiveTabChange();
  }

  onEndHandler() {
    this.isSwipeEnded = true;
  }

  handleActiveTabChange() {
    browserHistory.push(this.activeTab.uri);
  }

  getActiveTabIdx() {
    return TABS.findIndex(tab => tab.key === this.activeTab.key);
  }

  render() {
    const { nOfNotifications, nOfRequests } = this.props;

    const activeTabIndex = this.getActiveTabIdx();
    const notificationsClassName = `link-wrapper${activeTabIndex === 0 ? ' active' : ''}`;
    const requestsClassName = `link-wrapper${activeTabIndex === 1 ? ' active' : ''}`;

    const childProps = {
      nOfNotifications,
      nOfRequests,
      handlers: this.props.handlers
    };
    const child = React.cloneElement(this.props.children, childProps);

    return (
      <div id='notifications-container'>
        <ScrollToTop />
        <Grid>
          <Row>
            <Col xs={12} md={8} mdOffset={2} lg={6} lgOffset={3} className='notifications'>
              <div className='nav-block'>
                <span className={notificationsClassName}>
                  <Link to='/notifications/notifications' activeClassName='active'>Notifications</Link>
                  { nOfNotifications > 0 && <img src={NotificationsIcon} className='notifications-marker' /> }
                </span>
                <span className={requestsClassName}>
                  <Link to='/notifications/requests' activeClassName='active'>Requests</Link>
                  { nOfRequests > 0 && <img src={NotificationsIcon} className='notifications-marker' /> }
                </span>
              </div>
              { child }
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

Notifications.propTypes = {
  nOfNotifications: PropTypes.number,
  nOfRequests: PropTypes.number,
};

export default Notifications;