import React, {Component} from 'react';
import {toJS} from 'mobx';
import {observer} from 'mobx-react';
import {get} from 'lodash';
import qs from 'query-string';

import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';
import Controls from './controls/index.jsx';
import SelectedDateLabel from './selected-date/index.jsx';
import Items from './items/index.jsx';

import DataState from './state/data';

import ScrollHelper from '../../utils/dom/events/scroll-helper';
import {setTitle} from '../../utils/formatting/title';
import {getDirection} from '../../utils/mouse/wheel';
import {getLocation} from '../../utils/dom/window';

import {FILTERS_VISIBILITIES, TABS} from './constants';
import {FILTERS} from './controls/filters/constants';
import {PAGES, PAGE_TITLES} from '../../utils/formatting/constants';
import {EVENTS} from '../../utils/dom/events/scroll-helper/constants';
import {DIRECTIONS} from '../../utils/mouse/wheel/constants';

// todo (by evgen): due to https://trello.com/c/D7zRUfmp
const state = {
  data: new DataState(),
};



@observer
class NewDiscover extends Component {
  controlsHandlers;

  loadMoreHelper;
  onWheel;

  constructor(props) {
    super(props);
    Object.assign(this, state);
    this.initHandlers();
    this.init();
    this.data.fetchInitial();
    setTitle(PAGE_TITLES[PAGES.DISCOVER]());
  }

  initHandlers() {
    this.controlsHandlers = {
      dateSelector: {
        onDateClick: ::this.onDateClick,
        onViewedDateChange: ::this.onViewedDateChange,
      },
      menu: {
        onClick: ::this.onMenuClick,
        onFiltersClick: ::this.onFiltersClick,
      },
      filters: {
        [FILTERS.CATEGORIES]: {
          onApply: ::this.onCategoriesApply,
        },
        [FILTERS.LOCATION]: {
          onSelect: ::this.onLocationSelect,
        },
        [FILTERS.DATE]: {
          onSelect: ::this.onDateSelect,
        },
        [FILTERS.RANGE]: {
          onChange: ::this.onRangeChange,
        },
        [FILTERS.CLEAR]: {
          onClick: ::this.onFiltersClearClick,
        },
      },
      hashTag: {
        onChange: ::this.onHashTagChange,
      },
    };
    this.onWheel = ::this.onWheelHandler;
  }

  componentWillReceiveProps() {
    this.init();
  }

  componentWillUnmount() {
    this.loadMoreHelper.dispose();
    this.data = null;
    this.loadMoreHelper = null;
    this.onWheel = null;
  }

  init() {
    this.initHashTag();
    this.initHelpers();
  }

  initHashTag() {
    const params = qs.parse(getLocation().search);
    const hashTag = get(params, 'hashtag');
    this.updateFilter(FILTERS.HASH_TAG, hashTag ? hashTag : '');
  }

  initHelpers() {
    this.loadMoreHelper = new ScrollHelper({
      handlers: {
        [EVENTS.WHEEL.name]: this.onWheel,
        [EVENTS.TOUCH_MOVE.name]: this.onWheel,
        [EVENTS.KEY_DOWN.name]: this.onWheel,
      },
    });
  }

  onWheelHandler(e) {
    const direction = getDirection(e);
    if (direction !== DIRECTIONS.DOWN) return;
    this.data.fetchMore();
  }

  onDateClick(newDate) {
    const {date, today} = this.data;
    if (!newDate || !newDate.isValid || !newDate.isValid() || newDate.isSame(date) || newDate.isBefore(today)) return;
    this.data.date = newDate;
    this.data.fetchInitial();
  }

  onViewedDateChange(newDate) {
    this.data.updateFilters(FILTERS.DATE, {
      selectedDate: this.data.selectedDate,
      viewedDate: newDate,
    });
  }

  onMenuClick(value) {
    if (value === this.data.currentTab) return;
    this.data.resetFilterValue(FILTERS.LOCATION, { reset: true });
    this.data.lastConfig = null;
    this.data.setIsLoaded(false);
    this.data.setIsEnd(false);
    this.data.setIsBusy(false);
    this.data.setCurrentTab(value);
    this.data.fetchInitial();
  }

  onFiltersClick() {
    this.data.setFiltersVisibility(!this.data.isFiltersVisible);
    this.data.fetchInitial();
  }

  onCategoriesApply(selectedCategories) {
    this.updateFilter(FILTERS.CATEGORIES, selectedCategories);
  }

  onLocationSelect(value) {
    this.updateFilter(FILTERS.LOCATION, value);
  }

  onDateSelect(value) {
    if (this.data.date.isSame(value)) return;
    this.data.date = value;
    this.data.fetchInitial();
  }

  onRangeChange(value) {
    this.updateFilter(FILTERS.RANGE, value);
  }

  onFiltersClearClick() {
    this.data.resetAllFilters();
  }

  onHashTagChange(value) {
    this.updateFilter(FILTERS.HASH_TAG, value);
  }

  updateFilter(type, value) {
    this.data.updateFilters(type, value)
    .then(res => { if (res) this.data.fetchInitial(); });
  }

  render() {
    return (
      <div id='discover-wrapper'>
        <ScrollToTop />
        <div id='discover-content'>
          <Controls values={this.getControlsValues()} handlers={this.controlsHandlers} />
          <SelectedDateLabel values={this.getSelectedDateValues()} />
          <Items values={this.getItemsValues()} />
        </div>
      </div>
    );
  }

  getControlsValues() {
    const { isFiltersVisible, selectedDate, viewedDate } = this.data;
    return {
      dateSelector: {
        selectedDate,
      },
      menu: {
        activeTab: this.data.currentTab,
      },
      filters: {
        isVisible: isFiltersVisible,
        [FILTERS.CATEGORIES]: this.getFilterValue(FILTERS.CATEGORIES),
        [FILTERS.LOCATION]: this.getFilterValue(FILTERS.LOCATION),
        [FILTERS.DATE]: {
          ...this.getFilterValue(FILTERS.DATE),
          selectedDate,
          viewedDate,
        },
        [FILTERS.RANGE]: this.getFilterValue(FILTERS.RANGE),
      },
      hashTag: {
        value: this.getFilterValue(FILTERS.HASH_TAG).value,
        isFiltersVisible,
      },
    };
  }

  getFilterValue(key) {
    const { currentTab } = this.data;
    return {
      isShown: FILTERS_VISIBILITIES[currentTab].includes(key),
      ...this.data.getFilterValue(key),
    };
  }

  getSelectedDateValues() {
    return {
      date: this.data.selectedDate,
    };
  }

  getItemsValues() {
    const { isLoaded, isBusy, isEnd, items } = this.data;
    return {
      isLoaded,
      isBusy,
      isEnd,
      items: toJS(items),
    };
  }
}

export default NewDiscover;