import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class NewDiscoverMenuIcons extends Component {
  render() {
    const { handlers } = this.props;
    return (
      <span id='discover-menu-icons'>
        <div className='filters' onClick={handlers.onFiltersClick}>Filters</div>
      </span>
    );
  }
}

NewDiscoverMenuIcons.propTypes = {
  handlers: PropTypes.shape({
    onFiltersClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverMenuIcons;
