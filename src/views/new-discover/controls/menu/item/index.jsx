import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class NewDiscoverMenuItem extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { key } = this.props.values;
    const { onClick } = this.props.handlers;
    onClick(key);
  }

  render() {
    const { text, isActive } = this.props.values;
    return <div className={`discover-menu-item${isActive ? ' active' : ''}`} onClick={this.onClick}>{text}</div>;
  }
}

NewDiscoverMenuItem.propTypes = {
  values: PropTypes.shape({
    key: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverMenuItem;
