import { TABS } from '../../constants';

export const TEXTS = {
  [TABS.LOCATION]: 'My Location',
  [TABS.FOLLOWINGS]: 'Following',
};
