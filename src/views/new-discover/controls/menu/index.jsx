import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import Item from './item/index.jsx';
import Icons from './icons/index.jsx';

import {TEXTS} from './constants';
import {TABS} from '../../constants';



@observer
class NewDiscoverMenu extends Component {
  render() {
    const { values, handlers } = this.props;
    const { activeTab } = values;
    const items = Object.keys(TABS).map(key => {
      const tabValues = {
        key: TABS[key],
        text: TEXTS[TABS[key]],
        isActive: activeTab === TABS[key],
      };
      return <Item key={key} values={tabValues} handlers={handlers} />;
    });
    return (
      <div id='discover-menu'>
        {items}
        <Icons handlers={handlers} />
      </div>
    );
  }
}

NewDiscoverMenu.propTypes = {
  values: PropTypes.shape({
    activeTab: PropTypes.string.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    onFiltersClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverMenu;
