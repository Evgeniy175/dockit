import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import DataState from './state/data';
import UiState from './state/ui';

import jq from '../../../../utils/jquery';

import CurrentDateArrow from './current-day-arrow/index.jsx';
import Arrow from './arrow/index.jsx';
import ScrollWrapper from './scroll-wrapper/index.jsx';

import {WRAPPER_ID, DATE_SELECTOR_ID} from './constants';
import {TYPES as CURRENT_DAY_ARROW_TYPES} from './current-day-arrow/constants';
import {TYPES as ARROW_TYPES} from './arrow/constants';
import {EVENT_TYPES} from '../../../../utils/dom/events/constants';



@observer
class NewDiscoverDateSelector extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(props),
    });

    this.onResize = ::this.onResizeHandler;
    this.onKeyDown = ::this.onKeyDownHandler;

    this.arrowHandlers = {
      [ARROW_TYPES.LEFT]: {
        onClick: ::this.onPrevClickHandler,
      },
      [ARROW_TYPES.RIGHT]: {
        onClick: ::this.onNextClickHandler,
      },
    };

    this.scrollWrapperHandlers = {
      dayHandlers: {
        onClick: ::this.onDateClickHandler,
      },
      increaseCurrentDate: ::this.onIncreaseCurrentDate,
      subtractCurrentDate: ::this.onSubtractCurrentDate,
      onRendered: ::this.onScrollRenderedHandler,
      onRecalcInit: ::this.onRecalcInitHandler,
    };
    document.addEventListener('keydown', this.onKeyDown);
  }

  componentWillMount() {
    window.addEventListener('resize', this.onResize, false);
  }

  componentWillReceiveProps(nextProps) {
    const { values } = nextProps;
    if (values.selectedDate.isSame(this.data.selectedDate)) return;
    this.data.setSelectedDate(values.selectedDate);
    this.data.setCurrentDate(values.selectedDate);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize, false);
    document.removeEventListener('keydown', this.onKeyDown);
    this.onResize = null;
    this.arrowHandlers = null;
    this.scrollWrapperHandlers = null;
    this.onKeyDown = null;
  }

  onResizeHandler() {
    this.ui.handleResize();
  }

  onKeyDownHandler(e) {
    if (jq.wrap(e.target).is('input')) return;
    e.preventDefault();
    this.data.handleArrows(e.keyCode);
  }

  onPrevClickHandler() {
    this.data.onPrevArrowClick();
  }

  onNextClickHandler() {
    this.data.onNextArrowClick();
  }

  onDateClickHandler(newDate) {
    const {selectedDate, today} = this.data;
    if (!newDate || !newDate.isValid || !newDate.isValid() || newDate.isSame(selectedDate) || newDate.isBefore(today)) return;
    this.data.setSelectedDate(newDate);
    this.props.handlers.onDateClick(newDate);
  }

  onScrollRenderedHandler() {
    this.ui.centerWrapper();
  }

  onRecalcInitHandler() {
    this.ui.initRecalc(this.data.recalcHandlers);
  }

  onIncreaseCurrentDate(type) {
    this.data.increaseCurrentDate();
    if (type === EVENT_TYPES.WHEEL) this.ui.centerWrapper();
    else if (type === EVENT_TYPES.TOUCH) this.ui.handleTouchToPrev();
  }

  onSubtractCurrentDate(type) {
    this.data.subtractCurrentDate();
    if (type === EVENT_TYPES.WHEEL) this.ui.centerWrapper();
    else if (type === EVENT_TYPES.TOUCH) this.ui.handleTouchToNext();
  }

  render() {
    return (
      <div id={WRAPPER_ID}>
        {/*<CurrentDateArrow values={{ type: CURRENT_DAY_ARROW_TYPES.TOP }} />*/}
        <div id={DATE_SELECTOR_ID}>
          <Arrow values={{ type: ARROW_TYPES.LEFT }} handlers={this.arrowHandlers[ARROW_TYPES.LEFT]} />
          <ScrollWrapper values={this.getScrollWrapperValues()} handlers={this.scrollWrapperHandlers} />
          <Arrow values={{ type: ARROW_TYPES.RIGHT }} handlers={this.arrowHandlers[ARROW_TYPES.RIGHT]} />
        </div>
        {/*<CurrentDateArrow values={{ type: CURRENT_DAY_ARROW_TYPES.BOTTOM }} />*/}
      </div>
    );
  }

  getScrollWrapperValues() {
    return {
      dayWidth: this.ui.dayWidth,
      dates: this.data.dates,
      lastDate: this.data.lastDate,
    };
  }
}

NewDiscoverDateSelector.propTypes = {
  values: PropTypes.shape({
    selectedDate: PropTypes.object.isRequired,
  }),
  handlers: PropTypes.shape({
    onDateClick: PropTypes.func.isRequired,
    onViewedDateChange: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverDateSelector;