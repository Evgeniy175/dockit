import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import Day from './day/index.jsx';

import jq from '../../../../../utils/jquery';
import WheelHelper from '../../../../../utils/dom/events/scroll-helper';

import {SCROLL_WRAPPER_ID} from '../constants';
import {EVENT_TYPES} from '../../../../../utils/dom/events/constants';
import {EVENTS} from '../../../../../utils/dom/events/scroll-helper/constants';



@observer
class NewDiscoverDateSelectorScrollWrapper extends Component {
  wheelHelper;
  touchStart = {
    x: 0,
  };

  componentDidMount() {
    const onWheel = ::this.onWheelHandler;
    this.wheelHelper = new WheelHelper({
      container: jq.getElem(`#${SCROLL_WRAPPER_ID}`).context,
      handlers: {
        [EVENTS.WHEEL.name]: onWheel,
        [EVENTS.TOUCH_START.name]: ::this.onTouchStartHandler,
        [EVENTS.TOUCH_MOVE.name]: ::this.onTouchMoveHandler,
        [EVENTS.TOUCH_END.name]: ::this.onTouchEndHandler,
      },
    });
    this.props.handlers.onRendered();
  }

  componentWillUnmount() {
    this.touchStart = null;
    this.wheelHelper.dispose();
    this.wheelHelper = null;
  }

  onTouchStartHandler(e) {
    this.touchStart.x = e.touches[0].pageX;
  }

  onTouchMoveHandler(e) {
    this.onTouchHandler(e);
  }

  onTouchEndHandler(e) {
    this.onTouchHandler(e);
    this.props.handlers.onRecalcInit();
  }

  onTouchHandler(e) {
    const dayWidth = this.props.values.dayWidth;
    const deltaY = this.touchStart.x - e.changedTouches[0].pageX;
    if (deltaY >= dayWidth) this.props.handlers.increaseCurrentDate(EVENT_TYPES.TOUCH);
    else if (deltaY <= -dayWidth) this.props.handlers.subtractCurrentDate(EVENT_TYPES.TOUCH);
    else return;
    this.touchStart.x = e.changedTouches[0].pageX;
  }

  onWheelHandler(e) {
    e.preventDefault();
    if (e.deltaY > 0) this.props.handlers.increaseCurrentDate(EVENT_TYPES.WHEEL);
    else if (e.deltaY < 0) this.props.handlers.subtractCurrentDate(EVENT_TYPES.WHEEL);
  }

  render() {
    const { dates } = this.props.values;
    return (
      <div id={SCROLL_WRAPPER_ID}>
        { dates.map(data => <Day key={data.date.format()} values={data} handlers={this.props.handlers.dayHandlers} />) }
      </div>
    );
  }
}

NewDiscoverDateSelectorScrollWrapper.propTypes = {
  values: PropTypes.shape({
    dates: PropTypes.object.isRequired,
    lastDate: PropTypes.object.isRequired,
    dayWidth: PropTypes.number,
  }).isRequired,
  handlers: PropTypes.shape({
    dayHandlers: PropTypes.shape({
      onClick: PropTypes.func.isRequired,
    }),
    increaseCurrentDate: PropTypes.func.isRequired,
    subtractCurrentDate: PropTypes.func.isRequired,
    onRendered: PropTypes.func.isRequired,
    onRecalcInit: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverDateSelectorScrollWrapper;