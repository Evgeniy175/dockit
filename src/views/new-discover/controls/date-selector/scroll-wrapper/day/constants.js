export const CLASS_NAME = 'discover-date-selector-date';

export const TOP_LINE_FORMATTER = 'dd';
export const BOTTOM_LINE_FORMATTER = 'D';

export const ITEM_WIDTH = 40;
