import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import {CLASS_NAME, TOP_LINE_FORMATTER, BOTTOM_LINE_FORMATTER, ITEM_WIDTH} from './constants';



@observer
class NewDiscoverDateSelectorDay extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { date } = this.props.values;
    this.props.handlers.onClick(date);
  }

  render() {
    const { date, isSelected, isToday, isBeforeToday } = this.props.values;
    if (!date.isValid()) return null;
    const className = `${CLASS_NAME}${isSelected ? ' selected' : ''}${isToday ? ' today' : ''}${isBeforeToday ? ' before-today' : ''}`;
    return (
      <div className={className} onClick={this.onClick} style={this.getStyle()}>
        <div className='top-line'>{ date.format(TOP_LINE_FORMATTER) }</div>
        <div className='bottom-line'>{ date.format(BOTTOM_LINE_FORMATTER) }</div>
      </div>
    );
  }

  getStyle() {
    return {
      width: ITEM_WIDTH,
    };
  }
}

NewDiscoverDateSelectorDay.propTypes = {
  values: PropTypes.shape({
    date: PropTypes.object.isRequired,
    isSelected: PropTypes.bool.isRequired,
    isBeforeToday: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverDateSelectorDay;