export const WRAPPER_ID = 'discover-date-selector-wrapper';
export const SELECTED_DATE_LABEL_ID = 'selected-date-label-wrapper';
export const DATE_SELECTOR_ID = 'discover-date-selector';
export const CURRENT_DATE_CLASS_NAME = 'discover-date-current-date';
export const LEFT_ARROW_WRAPPER_ID = 'discover-date-selector-left-arrow';
export const SCROLL_WRAPPER_ID = 'discover-date-selector-scroll-wrapper';
export const RIGHT_ARROW_WRAPPER_ID = 'discover-date-selector-right-arrow';

export const RENDER_BEFORE_DATES = 30;
export const RENDER_AFTER_DATES = 30;

export const RECALC_INTERVAL = 100; // ms

export const SELECTED_DATE_FORMATTER = 'dddd, LL';
