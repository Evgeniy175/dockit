import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import {STYLES, CLASSES} from './constants';
import {CURRENT_DATE_CLASS_NAME} from '../constants';



@observer
class NewDiscoverDateSelectorCurrentDayArrow extends Component {
  constructor(props) {
    super(props);
    this.arrowClassName = `fa ${CLASSES[props.values.type]}`;
  }

  render() {
    const { type } = this.props.values;
    return <div className={CURRENT_DATE_CLASS_NAME} style={STYLES[type]}><span className={this.arrowClassName} /></div>;
  }
}

NewDiscoverDateSelectorCurrentDayArrow.propTypes = {
  values: PropTypes.shape({
    type: PropTypes.string.isRequired,
  }).isRequired,
};

export default NewDiscoverDateSelectorCurrentDayArrow;