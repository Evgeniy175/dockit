export const TYPES = {
  TOP: 'top',
  BOTTOM: 'bottom',
};

export const STYLES = {
  [TYPES.TOP]: {
    marginBottom: '-10px',
  },
  [TYPES.BOTTOM]: {
    marginTop: '-10px',
  },
};

export const CLASSES = {
  [TYPES.TOP]: 'fa-caret-down',
  [TYPES.BOTTOM]: 'fa-caret-up',
};
