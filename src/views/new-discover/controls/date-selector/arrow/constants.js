import { LEFT_ARROW_WRAPPER_ID, RIGHT_ARROW_WRAPPER_ID } from '../constants';

export const TYPES = {
  LEFT: 'left',
  RIGHT: 'right',
};

export const IDS = {
  [TYPES.LEFT]: LEFT_ARROW_WRAPPER_ID,
  [TYPES.RIGHT]: RIGHT_ARROW_WRAPPER_ID,
};

export const CLASSES = {
  [TYPES.LEFT]: 'fa-chevron-left',
  [TYPES.RIGHT]: 'fa-chevron-right',
};
