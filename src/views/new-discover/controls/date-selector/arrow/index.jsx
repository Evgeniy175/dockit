import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import {IDS, CLASSES} from './constants';



@observer
class NewDiscoverDateSelectorSwitchArrow extends Component {
  constructor(props) {
    super(props);
    this.className = `arrow fa fa ${CLASSES[props.values.type]}`;
  }

  render() {
    const { type } = this.props.values;
    return <span id={IDS[type]} className={this.className} onClick={this.props.handlers.onClick} />;
  }
}

NewDiscoverDateSelectorSwitchArrow.propTypes = {
  values: PropTypes.shape({
    type: PropTypes.string.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverDateSelectorSwitchArrow;