import {action, observable, toJS} from 'mobx';
import moment from 'moment';

import DataState from '../../../../../utils/state/data';

import {RENDER_BEFORE_DATES, RENDER_AFTER_DATES} from '../constants';
import {KEY_CODES} from '../../../../../constants';



class NewDiscoverDateSelectorDataState extends DataState {
  @observable dates = []; // array of dates for render
  @observable currentDate = moment(this.today); // date that shown at the center of the scroll
  @observable selectedDate = moment(this.today); // date that currently selected
  handlers;

  get lastDate() {
    if (!this.dates || this.dates.length === 0) return;
    return this.dates[this.dates.length - 1];
  }

  get today() {
    return moment().startOf('day');
  }

  constructor(props = {}) {
    super(props);
    this.handlers = props.handlers;
    this.calcDates();
    this.recalcHandlers = {
      add: ::this.increaseDays,
      subtract: ::this.subtractDays,
    }
  }

  onPrevArrowClick() {
    this.subtractDays(2);
  }

  onNextArrowClick() {
    this.increaseDays(2);
  }

  increaseCurrentDate() {
    this.increaseDays(1);
  }

  subtractCurrentDate() {
    this.subtractDays(1);
  }

  increaseDays(n = 1) {
    this.setCurrentDate(moment(this.currentDate).add(n, 'days'));
  }

  subtractDays(n = 1) {
    const expectedDate = moment(this.currentDate).subtract(n, 'days');
    if (expectedDate.isBefore(moment(this.today))) return;
    this.setCurrentDate(expectedDate);
  }

  handleArrows(keyCode) {
    switch (keyCode) {
      case KEY_CODES.UP_ARROW: { this.subtractDays(2); break; }
      case KEY_CODES.RIGHT_ARROW: { this.increaseDays(2); break; }
      case KEY_CODES.DOWN_ARROW: { this.increaseDays(2); break; }
      case KEY_CODES.LEFT_ARROW: { this.subtractDays(2); break; }
      default: return false;
    }
    return true;
  }

  @action
  setCurrentDate(value) {
    this.currentDate = value.startOf('day');
    this.calcDates();
    this.handlers.onViewedDateChange(this.currentDate);
  }

  @action
  setSelectedDate(value) {
    this.selectedDate = value.startOf('day');
    this.calcDates();
  }

  @action
  calcDates() {
    const { today, selectedDate } = this;
    const newLength = RENDER_BEFORE_DATES + 1 + RENDER_AFTER_DATES;
    const newDates = new Array(newLength);
    const date = moment(this.currentDate).subtract(RENDER_BEFORE_DATES, 'days');

    for (let i = 0; i < newLength; i++) {
      newDates.push({
        date: moment(date),
        isToday: date.isSame(today),
        isSelected: date.isSame(selectedDate),
        isBeforeToday: date.isBefore(today),
      });
      date.add(1, 'days');
    }

    this.dates = newDates;
  }
}

export default NewDiscoverDateSelectorDataState;
