import UiState from '../../../../../utils/state/ui';

import jq from '../../../../../utils/jquery';

import {SCROLL_WRAPPER_ID, RECALC_INTERVAL} from '../constants';
import {CLASS_NAME as DAY_CLASS_NAME} from '../scroll-wrapper/day/constants';



class NewDiscoverDateSelectorUiState extends UiState {
  prevScrollWrapperWidth;
  dayWidth;

  recalcData = {};

  get wrapper() {
    return jq.wrap(`#${SCROLL_WRAPPER_ID}`);
  }

  get day() {
    return jq.wrap(`.${DAY_CLASS_NAME}`);
  }

  constructor(props) {
    super(props);
    this.onRecalc = ::this.onRecalcHandler;
  }

  handleResize() {
    const { wrapper } = this;
    if (wrapper.width() === this.prevScrollWrapperWidth) return;
    this.centerWrapper();
  }

  centerWrapper() {
    const { wrapper } = this;
    wrapper.scrollLeft = this.getExpectedCenter(wrapper);
    this.prevScrollWrapperWidth = wrapper.width();
  }

  handleTouchToPrev() {
    this.initDayWidth();
    const { wrapper } = this;
    wrapper.scrollLeft -= this.dayWidth;
  }

  handleTouchToNext() {
    this.initDayWidth();
    const { wrapper } = this;
    wrapper.scrollLeft += this.dayWidth;
  }

  // recalc section start
  initRecalc(handlers) {
    if (this.recalcData.intervalId) return;

    this.initDayWidth();

    this.recalcData = {
      intervalId: setInterval(this.onRecalc, RECALC_INTERVAL),
      isNeedToRecalculatePosition: true,
      handlers,
      wrapper: this.wrapper,
    };

    this.onRecalc();
  }

  onRecalcHandler() {
    const { wrapper, handlers } = this.recalcData;
    const expectedCenter = this.getExpectedCenter(wrapper);
    const isEnd = wrapper.scrollLeft === expectedCenter;
    if (isEnd) return this.clearRecalc();
    const diff = Math.round(Math.abs(wrapper.scrollLeft - expectedCenter) / this.dayWidth);
    if (wrapper.scrollLeft > expectedCenter) handlers.add(diff);
    else if (wrapper.scrollLeft < expectedCenter) handlers.subtract(diff);
    wrapper.scrollLeft = expectedCenter;
  }

  clearRecalc() {
    clearInterval(this.recalcData.intervalId);
    Object.assign(this.recalcData, {
      intervalId: null,
      isNeedToRecalculatePosition: null,
      handlers: null,
    });
  }
  // recalc section end

  getExpectedCenter(wrapper) {
    return wrapper.scrollWidth / 2 - wrapper.offsetWidth / 2;
  }

  initDayWidth() {
    if (this.dayWidth) return;
    this.dayWidth = this.day.width();
  }
}

export default NewDiscoverDateSelectorUiState;
