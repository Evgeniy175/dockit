import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import {getRandomInt} from '../../../../utils/random';

import {PLACEHOLDER, TIMEOUT_MS} from './constants';



@observer
class NewDiscoverHashTagSearch extends Component {
  @observable value = '';
  intervalId;

  constructor(props) {
    super(props);
    this.onChange = ::this.onChangeHandler;
    this.onSubmit = ::this.onSubmitHandler;
    this.init(props);
  }

  init(props) {
    if (this.value !== props.values.value) this.setValue(props.values.value);
  }

  onChangeHandler(e) {
    this.setValue(e.target.value);
    this.intervalId = getRandomInt();
    setTimeout(this.onSubmit, TIMEOUT_MS, this.intervalId);
  }

  onSubmitHandler(intervalId) {
    if (this.intervalId !== intervalId) return;
    this.props.handlers.onChange(this.value);
  }

  @action
  setValue(value) {
    this.value = value;
  }

  render() {
    const { values } = this.props;
    const className = values.isFiltersVisible ? '' : 'filters-hidden';
    return (
      <div id='discover-hashtag-search-wrapper' className={className}>
        <input id='discover-hashtag-search' type='text' placeholder={PLACEHOLDER} value={this.value} onChange={this.onChange} />
      </div>
    );
  }
}

NewDiscoverHashTagSearch.propTypes = {
  values: PropTypes.shape({
    value: PropTypes.string.isRequired,
    isFiltersVisible: PropTypes.bool.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverHashTagSearch;