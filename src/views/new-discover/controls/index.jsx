import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import DateSelector from './date-selector/index.jsx';
import Menu from './menu/index.jsx';
import Filters from './filters/index.jsx';
import HashTagSearch from './hashtag-search/index.jsx';

import {FILTERS} from './filters/constants';



@observer
class NewDiscoverControls extends Component {
  render() {
    const { values, handlers } = this.props;
    return (
      <div id='discover-controls'>
        <DateSelector values={values.dateSelector} handlers={handlers.dateSelector} />
        <Menu values={values.menu} handlers={handlers.menu} />
        <Filters values={values.filters} handlers={handlers.filters} />
        <HashTagSearch values={values.hashTag} handlers={handlers.hashTag} />
      </div>
    );
  }
}

NewDiscoverControls.propTypes = {
  values: PropTypes.shape({
    dateSelector: PropTypes.shape({
      selectedDate: PropTypes.object.isRequired,
    }).isRequired,
    menu: PropTypes.shape({
      activeTab: PropTypes.string.isRequired,
    }).isRequired,
    filters: PropTypes.shape({
      [FILTERS.CATEGORIES]: PropTypes.shape({
        isShown: PropTypes.bool,
        selectedCategories: PropTypes.array,
      }).isRequired,
      [FILTERS.LOCATION]: PropTypes.shape({
        isShown: PropTypes.bool,
        coords: PropTypes.object,
        selected: PropTypes.object,
      }).isRequired,
      [FILTERS.DATE]: PropTypes.shape({
        selectedDate: PropTypes.object,
      }).isRequired,
      [FILTERS.RANGE]: PropTypes.shape({
        isShown: PropTypes.bool,
        value: PropTypes.number,
      }).isRequired,
    }).isRequired,
    hashTag: PropTypes.shape({
      value: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    dateSelector: PropTypes.shape({
      onDateClick: PropTypes.func.isRequired,
      onViewedDateChange: PropTypes.func.isRequired,
    }).isRequired,
    menu: PropTypes.shape({
      onClick: PropTypes.func.isRequired,
      onFiltersClick: PropTypes.func.isRequired,
    }).isRequired,
    filters: PropTypes.shape({
      [FILTERS.CATEGORIES]: PropTypes.shape({
        onApply: PropTypes.func.isRequired,
      }).isRequired,
      [FILTERS.LOCATION]: PropTypes.shape({
        onSelect: PropTypes.func.isRequired,
      }).isRequired,
      [FILTERS.DATE]: PropTypes.shape({
        onSelect: PropTypes.func.isRequired,
      }).isRequired,
      [FILTERS.RANGE]: PropTypes.shape({
        onChange: PropTypes.func.isRequired,
      }).isRequired,
    }).isRequired,
    hashTag: PropTypes.shape({
      onChange: PropTypes.func.isRequired,
    }).isRequired,
  }).isRequired,
};

export default NewDiscoverControls;