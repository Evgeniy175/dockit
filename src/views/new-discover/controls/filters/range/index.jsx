import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import PropTypes from 'prop-types';
import {get} from 'lodash';

import InputRange from 'react-input-range';

import {MIN_VALUE, MAX_VALUE, DEFAULT_VALUE, MEASURE_UNIT} from './constants';



@observer
class NewDiscoverFiltersRangeFilter extends Component {
  @observable isShown = true;
  @observable value = DEFAULT_VALUE;

  constructor(props) {
    super(props);
    this.setValue(get(props, 'values.value', DEFAULT_VALUE));
    this.onValueChange = ::this.onValueChangeHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    const { isShown, value } = props.values;
    this.setVisibility(isShown);
    this.setValue(value);
  }
  
  onValueChangeHandler(value) {
    this.setValue(value);
  }

  @action
  setVisibility(value) {
    if (value !== undefined) this.isShown = value;
  }
  
  @action
  setValue(value) {
    this.value = value;
  }

  render() {
    if (!this.isShown) return null;
    return (
      <div id='discover-range-filter' onClick={this.onCategoriesOpen}>
        <div className='range-label'>Distance</div>
        <div className='range-value'>{`${this.value} ${MEASURE_UNIT}`}</div>
        <div className='scroll-wrapper'>
          <InputRange minValue={MIN_VALUE}
                      maxValue={MAX_VALUE}
                      value={this.value}
                      onChange={this.onValueChange}
                      onChangeComplete={this.props.handlers.onChange} />
        </div>
      </div>
    );
  }
}

NewDiscoverFiltersRangeFilter.propTypes = {
  values: PropTypes.shape({
    isShown: PropTypes.bool,
    value: PropTypes.number,
  }),
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverFiltersRangeFilter;