export const MIN_VALUE = 5;
export const MAX_VALUE = 250;
export const DEFAULT_VALUE = 25;
export const MEASURE_UNIT = 'mi';