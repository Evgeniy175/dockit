import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import PropTypes from 'prop-types';
import {get} from 'lodash';

import CategoriesModal from'../../../../../shared-components/categories-modal/index.jsx';



@observer
class NewDiscoverFiltersCategoryFilter extends Component {
  @observable isModalVisible = false;
  
  constructor(props) {
    super(props);
    this.onCategoriesOpen = ::this.onCategoriesOpenHandler;
    this.onCategoriesClose = ::this.onCategoriesCloseHandler;
    this.modalHandlers = {
      onClose: ::this.onCategoriesCloseHandler,
    };
  }
  
  onCategoriesOpenHandler() {
    this.setModalVisibility(true);
  }
  
  onCategoriesCloseHandler(selectedCategories) {
    this.setModalVisibility(false);
    this.props.handlers.onApply(selectedCategories);
  }
  
  @action
  setModalVisibility(value) {
    this.isModalVisible = value;
  }

  render() {
    const modalData = {
      isOpen: this.isModalVisible,
      selectedCategories: get(this.props, 'values.selectedCategories'),
    };
    
    return (
      <div id='discover-category-filter' onClick={this.onCategoriesOpen}>
        <span className='cat-label'>Categories</span>
        <CategoriesModal data={modalData} handlers={this.modalHandlers} />
      </div>
    );
  }
}

NewDiscoverFiltersCategoryFilter.propTypes = {
  values: PropTypes.shape({
    isShown: PropTypes.bool,
    selectedCategories: PropTypes.array,
  }),
  handlers: PropTypes.shape({
    onApply: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverFiltersCategoryFilter;