import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import CategoriesFilter from './categories/index.jsx';
import LocationFilter from '../../../../shared-components/location-select/index.jsx';
import DateFilter from './date/index.jsx';
import RangeFilter from './range/index.jsx';

import { values as ObjectValues } from '../../../../utils/object';

import { FILTERS } from './constants';



@observer
class NewDiscoverFilters extends Component {
  constructor(props) {
    super(props);
    this.filtersRenders = {
      // todo by evgen: commented due to no.3 https://trello.com/c/aW9aFhWu
      // [FILTERS.CATEGORIES]: ::this.renderCategoryFilter,
      [FILTERS.LOCATION]: ::this.renderLocationFilter,
      [FILTERS.DATE]: ::this.renderDateFilter,
      [FILTERS.RANGE]: ::this.renderRangeFilter,
      [FILTERS.CLEAR]: ::this.renderClearFilter,
    };
  }

  componentWillUnmount() {
    this.filtersRenders = null;
  }

  render() {
    const { values, handlers } = this.props;
    if (!values.isVisible) return null;
    return (
      <div id='discover-filters'>
        { ObjectValues(FILTERS).map(key => this.filtersRenders[key] && this.filtersRenders[key](key, values[key], handlers[key])) }

      </div>
    );
  }

  renderCategoryFilter(key, values, handlers) {
    return <CategoriesFilter key={key} values={values} handlers={handlers} />;
  }

  renderLocationFilter(key, values, handlers) {
    return <LocationFilter key={key} values={values} handlers={handlers} />;
  }

  renderDateFilter(key, values, handlers) {
    return <DateFilter key={key} values={values} handlers={handlers} />;
  }

  renderRangeFilter(key, values, handlers) {
    return <RangeFilter key={key} values={values} handlers={handlers} />;
  }

  renderClearFilter(key, values, handlers) {
    return <div key={key} className='discover-filters-clear' onClick={handlers.onClick}>Clear all filters</div>;
  }
}

NewDiscoverFilters.propTypes = {
  values: PropTypes.shape({
    isVisible: PropTypes.bool.isRequired,
    [FILTERS.CATEGORIES]: PropTypes.shape({
      isShown: PropTypes.bool,
      selectedCategories: PropTypes.array,
    }).isRequired,
    [FILTERS.LOCATION]: PropTypes.shape({
      isShown: PropTypes.bool,
      coords: PropTypes.object,
      selected: PropTypes.object,
    }).isRequired,
    [FILTERS.DATE]: PropTypes.shape({
      selectedDate: PropTypes.object,
    }).isRequired,
    [FILTERS.RANGE]: PropTypes.shape({
      isShown: PropTypes.bool,
      value: PropTypes.number,
    }).isRequired,
  }),
  handlers: PropTypes.shape({
    [FILTERS.CATEGORIES]: PropTypes.shape({
      onApply: PropTypes.func.isRequired,
    }).isRequired,
    [FILTERS.LOCATION]: PropTypes.shape({
      onSelect: PropTypes.func.isRequired,
    }).isRequired,
    [FILTERS.RANGE]: PropTypes.shape({
      onChange: PropTypes.func.isRequired,
    }).isRequired,
    [FILTERS.CLEAR]: PropTypes.shape({
      onClick: PropTypes.func.isRequired,
    }).isRequired,
  }).isRequired,
};

export default NewDiscoverFilters;