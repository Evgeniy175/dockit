import {action, computed, observable, toJS} from 'mobx';
import moment from 'moment';

import DataState from '../../../../../../utils/state/data';

import {KEYS} from './constants';
import {TYPES} from '../../../../../../shared-components/calendar-dropdown/constants';



class NewDiscoverDateFilterDataState extends DataState {
  isDisposed = false;

  @observable values;
  @observable handlers;

  calendarHandlers = {
    onDateChange: ::this.onDateChange,
    onPrevMonthClick: ::this.onPrevMonthClick,
    onNextMonthClick: ::this.onNextMonthClick,
  };

  get today() {
    return moment().startOf('day');
  }

  @computed
  get calendarValues() {
    return {
      type: TYPES.NORMAL,
      selectedDays: [],
      selectedDate: this.selectedDate,
      viewedDate: this.viewedDate,
    };
  }

  @computed
  get isVisible() {
    return this.getValue(KEYS.IS_VISIBLE);
  }
  @action
  setIsVisible(value) {
    this.setValue(KEYS.IS_VISIBLE, value);
  }

  @computed
  get isCalendarVisible() {
    return this.getValue(KEYS.IS_CALENDAR_VISIBLE);
  }
  @action
  setIsCalendarVisible(value) {
    this.setValue(KEYS.IS_CALENDAR_VISIBLE, value);
  }

  @computed
  get selectedDate() {
    return this.getValue(KEYS.SELECTED_DATE);
  }
  @action
  setSelectedDate(value) {
    this.setValue(KEYS.SELECTED_DATE, moment(value));
  }

  @computed
  get viewedDate() {
    return this.getValue(KEYS.VIEWED_DATE);
  }
  @action
  setViewedDate(value) {
    this.setValue(KEYS.VIEWED_DATE, moment(value));
  }

  getValue(key) {
    return this.isDisposed ? null : this.values.get(key);
  }

  setValue(key, value) {
    return this.isDisposed ? null : this.values.set(key, value);
  }

  constructor(props = {}) {
    super(props);
    this.init(props);
  }

  init(props) {
    this.initValues(props.values);
    this.initHandlers(props.handlers);
  }

  onDateChange(value) {
    if (moment(value).startOf('day').isBefore(this.today)) return;
    this.setSelectedDate(value);
    this.handlers.get('onSelect')(value);
    this.setIsCalendarVisible(false);
  }

  onPrevMonthClick() {
    this.setViewedDate(moment(this.viewedDate).subtract(1, 'month'));
  }

  onNextMonthClick() {
    this.setViewedDate(moment(this.viewedDate).add(1, 'month'));
  }

  @action
  initValues(values) {
    if (!this.values) return this.setValues(values);
    Object.keys(values).forEach(key => this.values.set(key, values[key]));
  }

  @action
  setValues(values) {
    this.values = observable.map(values);
  }

  @action
  initHandlers(values) {
    if (!this.handlers) return this.setHandlers(values);
  }

  @action
  setHandlers(values) {
    this.handlers = observable.map(values);
  }

  @action
  clear() {
    this.isDisposed = true;
    this.values = null;
  }
}

export default NewDiscoverDateFilterDataState;
