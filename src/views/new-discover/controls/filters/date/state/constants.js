export const KEYS = {
  IS_VISIBLE: 'isShown',
  IS_CALENDAR_VISIBLE: 'isCalendarVisible',
  SELECTED_DATE: 'selectedDate',
  VIEWED_DATE: 'viewedDate',
};
