import React, { Component } from 'react';
import { observer } from 'mobx-react';
import {action, observable, toJS} from 'mobx';
import PropTypes from 'prop-types';

import Calendar from '../../../../../shared-components/calendar-dropdown/calendar/index.jsx';

import DataState from './state/data';

import jq from '../../../../../utils/jquery';



@observer
class NewDiscoverDateSelectorFilter extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
    this.onClick = ::this.onClickHandler;
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  onClickHandler(e) {
    if (jq.wrap(e.target).id !== 'discover-date-filter') return;
    this.data.setIsCalendarVisible(!this.data.isCalendarVisible);
  }

  render() {
    const { calendarValues, calendarHandlers, isVisible, isCalendarVisible } = this.data;
    if (!isVisible) return null;
    return (
      <div id='discover-date-filter' onClick={this.onClick}>
        Date
        { isCalendarVisible && <Calendar data={calendarValues} handlers={calendarHandlers} /> }
      </div>
    );
  }
}

NewDiscoverDateSelectorFilter.propTypes = {
  values: PropTypes.shape({
    selectedDate: PropTypes.object.isRequired,
    viewedDate: PropTypes.object.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onSelect: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverDateSelectorFilter;