export const FILTERS = {
  CATEGORIES: 'categories',
  LOCATION: 'location',
  DATE: 'date',
  RANGE: 'range',
  HASH_TAG: 'hashtag',
  CLEAR: 'clear',
};
