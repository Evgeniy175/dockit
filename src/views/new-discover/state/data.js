import {action, observable, toJS} from 'mobx';
import {get} from 'lodash';
import Promise from 'bluebird';
import moment from 'moment';

import DataState from '../../../utils/state/data';

import EventModel from '../../../models/events';
import ScheduleModel from '../../../models/schedules';
import LocationModel from '../../../models/locations';

import {DeviceLocation} from '../../../utils/location';
import {milesToMeters} from '../../../utils/converters/distance';

import {PORTION_SIZE, LOCATION_FILTER_PLACEHOLDER} from './constants';
import {TABS} from '../constants';
import {FILTERS} from '../controls/filters/constants';

const eventModel = new EventModel();
const locationModel = new LocationModel();

const DEFAULT_FILTERS_VALUES = {
  [FILTERS.CATEGORIES]: {
    selectedCategories: [],
  },
  [FILTERS.LOCATION]: {
    placeholder: LOCATION_FILTER_PLACEHOLDER,
    coords: DeviceLocation.getDeviceLocation(),
    selected: null,
    value: '',
  },
  [FILTERS.RANGE]: {
    value: 25,
  },
  [FILTERS.HASH_TAG]: {
    value: '',
  },
};



class NewDiscoverDataState extends DataState {
  @observable currentTab = TABS.LOCATION;
  @observable selectedDate = moment();
  @observable viewedDate = moment();
  lastConfig = '';

  @observable items = [];

  @observable isFiltersVisible = false;
  @observable isLoaded = false;
  @observable isBusy = false;
  @observable isEnd = false;

  filtersData = {
    values: JSON.parse(JSON.stringify(DEFAULT_FILTERS_VALUES)),
    isUpdatedHandlers: {
      [FILTERS.CATEGORIES]: ::this.isCategoriesFilterUpdated,
      [FILTERS.LOCATION]: ::this.isLocationFilterUpdated,
      [FILTERS.DATE]: ::this.isDateFilterUpdated,
      [FILTERS.RANGE]: ::this.isRangeFilterUpdated,
      [FILTERS.HASH_TAG]: ::this.isHashTagFilterUpdated,
    },
    updateHandlers: {
      [FILTERS.CATEGORIES]: ::this.updateCategoriesFilter,
      [FILTERS.LOCATION]: ::this.updateLocationFilter,
      [FILTERS.DATE]: ::this.updateDateFilter,
      [FILTERS.RANGE]: ::this.updateRangeFilter,
      [FILTERS.HASH_TAG]: ::this.updateHashTagFilter,
    },
  };

  fetchResolvers = {
    [TABS.LOCATION]: ::this.fetchInitialLocation,
    [TABS.FOLLOWINGS]: ::this.fetchInitialFollowing,
  };

  fetchMoreResolvers = {
    [TABS.LOCATION]: ::this.fetchMoreLocation,
    [TABS.FOLLOWINGS]: ::this.fetchMoreFollowing,
  };

  get today() {
    return moment().startOf('day');
  }

  get fetcher() {
    return this.fetchResolvers[this.currentTab];
  }

  get moreFetcher() {
    return this.fetchMoreResolvers[this.currentTab];
  }

  get date() {
    return this.selectedDate;
  }
  set date(value) {
    this.setSelectedDate(value);
  }

  constructor(props = {}) {
    super(props);
  }

  fetchInitial() {
    const config = this.getConfig();
    if (JSON.stringify(config) === this.lastConfig) return;
    this.lastConfig = JSON.stringify(config);
    this.setIsEnd(false);
    this.setIsLoaded(false);
    return this.fetcher(config)
    .then(res => {
      this.setItems(res.data);
      this.setIsLoaded(true);
    });
  }

  fetchMore() {
    if (!this.isLoaded || this.isBusy || this.isEnd || this.items.length === 0) return;
    this.setIsBusy(true);
    return this.moreFetcher(this.getConfig({ offset: this.items.length }))
    .then(res => {
      this.attachItems(res.data);
      this.setIsBusy(false);
    });
  }

  //////////////////////////
  //////////////////////////
  //  fetch section start //
  //////////////////////////
  //////////////////////////
  fetchInitialLocation(config) {
    return eventModel.discoverByPlaceId(Object.assign({}, config));
  }

  fetchInitialFollowing(config) {
    return eventModel.discoverFollowers(Object.assign({}, config));
  }

  fetchMoreLocation(config) {
    return eventModel.discoverByPlaceId(Object.assign({}, config));
  }

  fetchMoreFollowing(config) {
    return eventModel.discoverFollowers(Object.assign({}, config));
  }
  //////////////////////////
  //////////////////////////
  //  fetch section end   //
  //////////////////////////
  //////////////////////////



  //////////////////////////
  //////////////////////////
  // config section start //
  //////////////////////////
  //////////////////////////
  getConfig(data = {}) {
    return {
      params: {
        query: {
          ...this.getStartTime(),
          ...this.getFilters(),
        },
        limit: PORTION_SIZE,
        offset: data && data.offset ? data.offset : 0,
        order: [
          'startTime',
          'asc',
        ],
      },
    };
  }

  getStartTime() {
    const startDate = moment(this.date).startOf('day');
    const endDate = moment(this.date).endOf('day');
    return {
      startTime: {
        $between: [
          startDate.format(),
          endDate.format(),
        ],
      },
    };
  }

  getFilters() {
    const rangeFilterData = this.getFilterValue(FILTERS.RANGE);
    const categoriesFilterData = this.getFilterValue(FILTERS.CATEGORIES);
    const hashTag = this.getFilterValue(FILTERS.HASH_TAG).value;
    const selectedCategories = toJS(categoriesFilterData.selectedCategories);

    const filters = {
      radius: milesToMeters(rangeFilterData.value),
      hashtags: hashTag ? [hashTag] : [],
      categories: {
        ids: selectedCategories.map(cat => cat.id)
      },
      ...this.getLocationCoords(),
    };
    if (selectedCategories.length === 0) delete filters.categories;

    return filters;
  }

  getLocationCoords() {
    const locationFilterData = JSON.parse(JSON.stringify(this.getFilterValue(FILTERS.LOCATION)));
    if (!locationFilterData.selected) locationFilterData.coords = DeviceLocation.getDeviceLocation();
    return {
      lat: get(locationFilterData, 'coords.lat'),
      lng: get(locationFilterData, 'coords.lon'),
      placeId: get(locationFilterData, 'selected.value'),
    };
  }
  //////////////////////////
  //////////////////////////
  //  config section end  //
  //////////////////////////
  //////////////////////////


  updateFilters(key, value) {
    if (!key || !this.filtersData.isUpdatedHandlers[key](value)) return Promise.resolve(false);
    return this.filtersData.updateHandlers[key](value);
  }

  isCategoriesFilterUpdated(categories) {
    const currValue = this.getFilterValue(FILTERS.CATEGORIES).selectedCategories;
    return currValue.length !== categories.length || categories.some((category, i) => category !== currValue[i]);
  }

  isLocationFilterUpdated(value) {
    const currValue = this.getFilterValue(FILTERS.LOCATION);
    const { coords, selectedLocation } = value;
    return (coords !== currValue.coords) || (selectedLocation !== currValue.selectedLocation);
  }

  isDateFilterUpdated(values) {
    return !this.selectedDate.isSame(values.selectedDate) || !this.viewedDate.isSame(values.viewedDate);
  }

  isRangeFilterUpdated(value) {
    const currValue = this.getFilterValue(FILTERS.RANGE).value;
    return currValue !== value;
  }

  isHashTagFilterUpdated(value) {
    const currValue = this.getFilterValue(FILTERS.HASH_TAG).value;
    return currValue !== value;
  }

  updateCategoriesFilter(value) {
    this.filtersData.values[FILTERS.CATEGORIES].selectedCategories = value;
    return Promise.resolve(true);
  }

  updateLocationFilter(value) {
    return locationModel.fetchLocationByPlaceId(value.value)
    .then(res => {
      this.filtersData.values[FILTERS.LOCATION] = {
        coords: {
          lat: get(res, 'loc[1]'),
          lon: get(res, 'loc[0]'),
        },
        selected: value,
      };
      return Promise.resolve(true);
    });
  }

  updateDateFilter({ selectedDate, viewedDate }) {
    this.setSelectedDate(selectedDate);
    this.setViewedDate(viewedDate);
    return Promise.resolve(true);
  }

  @action
  setSelectedDate(value) {
    this.selectedDate = moment(value);
  }

  @action
  setViewedDate(value) {
    this.viewedDate = moment(value);
  }

  updateRangeFilter(value) {
    this.filtersData.values[FILTERS.RANGE].value = value;
    return Promise.resolve(true);
  }

  updateHashTagFilter(value) {
    this.filtersData.values[FILTERS.HASH_TAG].value = value;
    return Promise.resolve(true);
  }

  getFilterValue(key) {
    return this.filtersData.values[key];
  }

  resetAllFilters() {
    this.filtersData.values = JSON.parse(JSON.stringify(DEFAULT_FILTERS_VALUES));
    this.resetFilterValue(FILTERS.LOCATION, { reset: true })
    this.setSelectedDate(this.today);
    this.setViewedDate(this.today);
  }

  resetFilterValue(key, expandBy = {}) {
    this.filtersData.values[key] = Object.assign(expandBy, DEFAULT_FILTERS_VALUES[key]);
  }

  @action
  setCurrentTab(value) {
    this.currentTab = value;
  }

  @action
  setFiltersVisibility(value) {
    this.isFiltersVisible = value;
  }

  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }

  @action
  setIsBusy(value) {
    this.isBusy = value;
  }

  @action
  setIsEnd(value) {
    this.isEnd = value;
  }

  @action
  setItems(value) {
    this.items = value.map(item => this.prepareItem(item));
  }

  @action
  attachItems(value) {
    this.setIsEnd(value.length === 0);
    const items = value.map(item => this.prepareItem(item));
    this.items = this.items.concat(items);
  }

  prepareItem(item) {
    return Object.assign(item, {
      image: item.schedule_id ? ScheduleModel.formatImageUrl(get(item, 'schedule.image')) : EventModel.formatImageUrl(item.image),
    });
  }

  @action
  clear() {
    this.items = null;
    this.currentTab = null;
    this.selectedDate = null;
    this.filtersData = null;
  }
}

export default NewDiscoverDataState;
