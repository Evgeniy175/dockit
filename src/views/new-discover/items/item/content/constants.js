export const ITEMS = {
  TITLE: 'title',
  DISTANCE: 'distance',
  TIME: 'time',
  LOCATION: 'location',
  USER_LINK: 'user link',
  DOCKS: 'docks',
};

export const COMMON = {
  [ITEMS.TITLE]: {
    wrapperClassName: 'title-wrapper',
  },
  [ITEMS.DISTANCE]: {
    wrapperClassName: 'distance-wrapper',
  },
  [ITEMS.TIME]: {},
  [ITEMS.LOCATION]: {},
  [ITEMS.USER_LINK]: {
    wrapperClassName: 'username-wrapper',
  },
  [ITEMS.DOCKS]: {
    wrapperClassName: 'dock-wrapper',
  },
};

export const ITEMS_ORDER = [
  [
    ['left', ITEMS.TITLE],
    ['right', ITEMS.DISTANCE],
  ],
  [
    ['left', ITEMS.TIME],
    ['right', ITEMS.LOCATION],
  ],
  [
    ['left', ITEMS.USER_LINK],
    ['right', ITEMS.DOCKS],
  ],
];
