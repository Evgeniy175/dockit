import React, { Component } from 'react';
import {observable, action} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class NewDiscoverItemContentRow extends Component {
  render() {
    const { leftContent, rightContent } = this.props;
    return (
      <div className='new-discover-content-row'>
        { this.isNeedRender(leftContent) && <div className={this.getClassName('left', leftContent)}>{leftContent.render}</div> }
        { this.isNeedRender(rightContent) && <div className={this.getClassName('right', rightContent)}>{rightContent.render}</div> }
      </div>
    );
  }

  isNeedRender(content) {
    return !!content && !!content.render;
  }

  getClassName(prefix, content) {
    const { wrapperClassName } = content;
    return `${prefix}${wrapperClassName ? ` ${wrapperClassName}` : ''}`;
  }
}

NewDiscoverItemContentRow.propTypes = {
  leftContent: PropTypes.shape({
    wrapperClassName: PropTypes.string,
    render: PropTypes.node,
  }),
  rightContent: PropTypes.shape({
    wrapperClassName: PropTypes.string,
    render: PropTypes.node,
  }),
};

export default NewDiscoverItemContentRow;
