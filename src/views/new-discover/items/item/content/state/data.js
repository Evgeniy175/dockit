import React from 'react';
import {action, observable} from 'mobx';
import {get, fromPairs} from 'lodash';
import moment from 'moment';

import UserLink from '../../../../../../shared-components/users/username-link/index.jsx';
import EventLink from '../../../../../../shared-components/creations/event-link/index.jsx';
import Location from '../../../../../../shared-components/location/index.jsx';
import DockAction from '../../../../../../shared-components/action-command/dock/index.jsx';

import {metersToMiles} from '../../../../../../utils/converters/distance';
import {formatInterval} from '../../../../../../utils/formatting/date';
import {getRandomInt} from '../../../../../../utils/random';

import DataState from '../../../../../../utils/state/data';

import {ITEMS, COMMON, ITEMS_ORDER} from '../constants';
import {TYPES} from '../../../../../creations/constants';
import {POSTFIX_POSITIONS} from '../../../../../../shared-components/action-command/dock/constants';



class NewDiscoverItemContentDataState extends DataState {
  @observable values;

  items = [];
  itemsValues = {};

  getters = {
    [ITEMS.TITLE]: values => values,
    [ITEMS.DISTANCE]: values => get(values, 'location.distance'),
    [ITEMS.TIME]: values => ({
      startTime: get(values, 'startTime'),
      endTime: get(values, 'endTime'),
    }),
    [ITEMS.LOCATION]: values => get(values, 'location'),
    [ITEMS.USER_LINK]: values => get(values, 'user'),
    [ITEMS.DOCKS]: values => ({
      type: TYPES.EVENT,
      event: values,
    }),
  };

  renders = {
    [ITEMS.TITLE]: values => <EventLink event={values} />,
    [ITEMS.DISTANCE]: distance => distance && <div className='distance'>{metersToMiles(distance)}mi</div>,
    [ITEMS.TIME]: ({ startTime, endTime }) => formatInterval(moment(startTime), moment(endTime)),
    [ITEMS.LOCATION]: location => location && <Location item={this.values} />,
    [ITEMS.USER_LINK]: user => <UserLink user={user} />,
    [ITEMS.DOCKS]: dockActionData => <DockAction data={dockActionData} postfix postfixPosition={POSTFIX_POSITIONS.LEFT} />,
  };

  constructor(props = {}) {
    super(props);
    this.init(props);
  }

  init(props) {
    if (!this.isChanged(props)) return;
    this.setValues(props.values);
    this.items = ITEMS_ORDER.map(data => this.getRow(fromPairs(data)));
  }

  isChanged({ values }) {
    return !this.values || Object.keys(this.itemsValues).some(key => {
      const newValue = this.getters[key] && this.getters[key](values);
      return JSON.stringify(newValue) !== JSON.stringify(this.itemsValues[key]);
    });
  }

  getRow({ left, right }) {
    const leftValue = this.getters[left] && this.getters[left](this.values);
    const rightValue = this.getters[right] && this.getters[right](this.values);
    const leftRender = this.renders[left];
    const rightRender = this.renders[right];
    this.itemsValues[left] = leftValue;
    this.itemsValues[right] = rightValue;
    return {
      key: this.getKey(leftValue, rightValue),
      left: {
        wrapperClassName: COMMON[left].wrapperClassName,
        render: leftRender ? leftRender(leftValue) : null,
      },
      right: {
        wrapperClassName: COMMON[right].wrapperClassName,
        render: rightRender ? rightRender(rightValue) : null,
      },
    };
  }

  getKey(left, right) {
    try {
      return `${JSON.stringify(left)}${JSON.stringify(right)}`;
    } catch (e) {
      return `rand${getRandomInt()}`;
    }
  }

  @action
  setValues(value) {
    this.values = value;
  }

  @action
  clear() {
    this.values = null;
    this.items = null;
    this.itemsValues = null;
    this.getters = null;
    this.renders = null;
  }
}

export default NewDiscoverItemContentDataState;
