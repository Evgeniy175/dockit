import React, { Component } from 'react';
import {observable, action} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import Row from './row/index.jsx';

import DataState from './state/data';



@observer
class NewDiscoverItemContent extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    this.data.clear();
    this.data = null;
  }

  render() {
    return (
      <div className='new-discover-item-content-wrapper' onClick={this.props.handlers.onClick}>
        {this.data.items.map(item => <Row key={item.key} leftContent={item.left} rightContent={item.right} />)}
      </div>
    );
  }
}

NewDiscoverItemContent.propTypes = {
  values: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    startTime: PropTypes.string.isRequired,
    decorated: PropTypes.object.isRequired,
    user_id: PropTypes.string.isRequired,
    schedule_id: PropTypes.string,
    image: PropTypes.string,
    location: PropTypes.shape({
      distance: PropTypes.number,
      title: PropTypes.string,
    }),
    schedule: PropTypes.shape({
      image: PropTypes.string,
    }),
    user: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      username: PropTypes.string,
      profilePicture: PropTypes.string,
    }),
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverItemContent;
