import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {browserHistory} from 'react-router';
import PropTypes from 'prop-types';

import Image from './cover-image/index.jsx';
import Content from './content/index.jsx';

import jq from '../../../../utils/jquery';

import {MODEL_PLURAL as EVENTS_MODEL_PLURAL} from '../../../../models/events/constants';



@observer
class NewDiscoverItem extends Component {
  constructor(props) {
    super(props);
    this.childHandlers = {
      onClick: ::this.onClickHandler,
    };
  }

  componentWillUnmount() {
    this.childHandlers = null;
  }

  onClickHandler(e) {
    const elem = jq.wrap(e.target);
    if (elem.is('a') || elem.hasParents('.action-item')) return;
    const {id} = this.props.values;
    browserHistory.push(`/${EVENTS_MODEL_PLURAL}/${id}`);
  }

  render() {
    return (
      <div className='new-discover-item'>
        <Image {...this.props} handlers={this.childHandlers} />
        <Content {...this.props} handlers={this.childHandlers} />
      </div>
    );
  }
}

NewDiscoverItem.propTypes = {
  values: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    decorated: PropTypes.object.isRequired,
    user_id: PropTypes.string.isRequired,
    schedule_id: PropTypes.string,
    image: PropTypes.string,
    location: PropTypes.shape({
      distance: PropTypes.number,
      title: PropTypes.string,
    }),
    schedule: PropTypes.shape({
      image: PropTypes.string,
    }),
    user: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      username: PropTypes.string,
      profilePicture: PropTypes.string,
    }),
  }).isRequired,
};

export default NewDiscoverItem;
