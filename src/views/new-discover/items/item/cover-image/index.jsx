import React, { Component } from 'react';
import {observable, action} from 'mobx';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import ProfileImage from '../../../../../shared-components/creations/event-member-image/index.jsx';
import Image from '../../../../../shared-components/image/index.jsx';

import {WRAPPER_CLASS_NAME, CLASS_NAME} from './constants';
import {ASPECT_RATIOS} from '../../../../../shared-components/image/constants';



@observer
class NewDiscoverItemCoverImage extends Component {
  @observable imageValues;

  onResize;

  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    if (!this.imageValues || this.imageValues.src !== props.values.image) this.setImageValues(props.values.image);
  }

  @action
  setImageValues(src) {
    this.imageValues = {
      src,
      className: CLASS_NAME,
      wrapperClassName: WRAPPER_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s16x9,
      prepareWrapper: true,
    };
  }

  render() {
    return (
      <div className={WRAPPER_CLASS_NAME} onClick={this.props.handlers.onClick}>
        <Image values={this.imageValues} />
        <ProfileImage user={this.props.values.user} />
      </div>
    );
  }
}

NewDiscoverItemCoverImage.propTypes = {
  values: PropTypes.shape({
    image: PropTypes.string,
    user: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      username: PropTypes.string,
      profilePicture: PropTypes.string,
    }),
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default NewDiscoverItemCoverImage;
