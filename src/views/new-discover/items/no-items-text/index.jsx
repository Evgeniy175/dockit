import React, {Component} from 'react';



class NewDiscoverItemsNoItemsText extends Component {
  render() {
    return (
      <div className='new-discover-items-no-items-text-wrapper'>
        <div>It doesn't look like anything is happening on this day.</div>
        <div>Check back soon to see if there is anything new!</div>
      </div>
    );
  }
}

export default NewDiscoverItemsNoItemsText;
