import React, {Component} from 'react';
import {observer} from 'mobx-react';
import PropTypes from 'prop-types';

import Item from './item/index.jsx';
import NoItems from './no-items-text/index.jsx';
import Loader from '../../../shared-components/loader/index.jsx';
import LoaderBottom from '../../../shared-components/loader-bottom/index.jsx';
import EndText from '../../../shared-components/end-text/index.jsx';



@observer
class NewDiscoverItems extends Component {
  render() {
    const { isLoaded, isBusy, isEnd, items } = this.props.values;
    if (!isLoaded) return <Loader />;
    return (
      <div className='new-discover-items-wrapper'>
        {items.map(item => <Item key={item.id} values={item} />)}
        {isBusy && <LoaderBottom />}
        {items.length > 0 && isEnd && <EndText />}
        {!isBusy && items.length === 0 && <NoItems />}
      </div>
    );
  }
}

NewDiscoverItems.propTypes = {
  values: PropTypes.shape({
    isLoaded: PropTypes.bool.isRequired,
    isBusy: PropTypes.bool.isRequired,
    isEnd: PropTypes.bool.isRequired,
    items: PropTypes.array.isRequired,
  }).isRequired,
};

export default NewDiscoverItems;
