import React, {Component} from 'react';
import {observable, action} from 'mobx';
import {observer} from 'mobx-react';
import moment from 'moment';
import PropTypes from 'prop-types';

import {ID, FORMATTER} from './constants';

import {getNavBarHeight} from '../../../utils/dom/elements/navbar';



@observer
class NewDiscoverSelectedDate extends Component {
  @observable date = moment();
  @observable top;

  constructor(props) {
    super(props);
    this.onResize = ::this.onResizeHandler;
    this.init(props);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
    this.onResize = null;
  }

  init(props) {
    this.onResize();
    if (this.date.isSame(props.values.date)) return;
    this.setDate(props.values.date);
  }

  onResizeHandler() {
    const newHeight = getNavBarHeight();
    if (newHeight === this.top) return;
    this.setTop(newHeight);
  }

  @action
  setDate(value) {
    this.date = moment(value);
  }

  @action
  setTop(value) {
    this.top = value;
  }

  render() {
    return <div id={ID} style={this.getStyle()}>{this.date.format(FORMATTER)}</div>;
  }

  getStyle() {
    return {
      top: this.top,
    };
  }
}

NewDiscoverSelectedDate.propTypes = {
  values: PropTypes.shape({
    date: PropTypes.object.isRequired,
  }).isRequired,
};

export default NewDiscoverSelectedDate;