import { FILTERS } from './controls/filters/constants';

export const TABS = {
  LOCATION: 'location',
  FOLLOWINGS: 'followings',
};

export const FILTERS_VISIBILITIES = {
  [TABS.LOCATION]: [
    FILTERS.LOCATION,
    FILTERS.DATE,
    FILTERS.CATEGORIES,
    FILTERS.RANGE,
  ],
  [TABS.FOLLOWINGS]: [
    FILTERS.DATE,
    FILTERS.CATEGORIES,
    FILTERS.RANGE,
  ],
};
