import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { setTitle } from '../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';

import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';



@observer
class ProfileSettingsAboutPrivacyPolicy extends Component {
  constructor(props) {
    super(props);
    setTitle(PAGE_TITLES[PAGES.PRIVACY_POLICY]());
  }
  
  render() {
    const year = new Date().getFullYear();

    return (
      <div className='privacy-policy-wrapper'>
        <ScrollToTop />
        <Grid>
          <Row className='privacy-policy'>
            <Col xs={12}>
              <h2>DockIt Privacy Policy (Updated 4/12/2018)</h2>
              <p>Our Services instantly allow people everywhere to discover events that matter most to them. Any registered user of the DockIt Services can create events and schedules, invite their friends, easily add events to their personal calendars and make sure that all of their friends are aware of the excitement that’s happening nearby.</p>
              <p>Protecting your privacy is important to us. With this in mind, we're providing this Privacy Policy to explain our practices regarding the collection, use and disclosure of information that we receive through our Services. This Privacy Policy does not apply to any third-party websites, services or applications, even if they are accessible through our Services.</p>
              <p><b>Collected Information:</b> Our primary goals in collecting information are to provide and improve our Services, to administer your use of the Services, and to enable you to enjoy and easily navigate our Services. If you create an Account, we will collect certain information that can be used to identify you, such as your email address or your username. You have the option to choose to provide other information about yourself, which will be made part of your Account's profile ("Profile").</p>
              <p><b>Log Data:</b> Our servers automatically record certain information about how a person uses our Services (we refer to this information as "Log Data"), including both Account holders and non-Account holders (either, a "User"). Log Data may include information such as a User's Internet Protocol (IP) address, operating system, device, the features of our Services to which a User browsed and the time spent on those features, search terms, the links on our Services that a User clicked on and other statistics. We use Log Data to administer the Services and we analyze (and may engage third parties to analyze) Log Data to improve, customize and enhance our Services by expanding their features and functionality and tailoring them to our Users' needs and preferences. We may use a person's IP address to generate aggregate, non-identifying information about how our Services are used. We use automated data collection tools to collect certain information. Some third party services providers that we engage with may also track and report information about how and when you interact with our App and information about your mobile device (such as device hardware, operating system, and location). We may use that information to customize and improve our Services or for advertising purposes. Note that this Privacy Policy covers only our use of technologies and does not include use of technologies by third parties.</p>
              <p><b>Device Data:</b> We may collect certain information that your mobile device sends when you use our Services, like a device identifier, user settings, and the operating system of your device, as well as information about your use of our Services. When you use our App and have enabled location services on your mobile device, we may collect and store information about your location by converting your IP address into a rough geolocation or by accessing your mobile device's GPS coordinates or course location. With your permission, we may use location information to improve, personalize and offer certain features of our Services to you, such as offering lists of nearby events/venues. If you do not want us to collect location information, you may disable that feature on your mobile device.</p>
              <p><b>Information Shared with Third Parties:</b> Your User Content and Profile, which includes your username, display name and, if you choose to include it, your photograph or picture, will be visible to other Account holders whom you have approved as followers to view your Profile and User Content. The email address you provide to create your Account is not shared by us with other Account holders (but may help other users find you via the “Search” functionality). We may engage third party services providers to work with us to administer and provide the Services. These third party services providers will have access to your account information only for the purpose of performing services on our behalf and will be expressly obligated not to disclose or use your information for any
                other purpose. We may share aggregated information and non-identifying information with third parties for industry research and analysis, advertising, demographic profiling and other similar purposes. Information Disclosed in Connection with Business Transactions. Information that we collect from our users is considered to be a business asset. Thus, if we are acquired by a third party as a result of a transaction such as a merger, acquisition or asset sale or if our assets are acquired by a third party in the event we go out of business or enter bankruptcy, some or all of our assets may be disclosed or transferred to a third party acquirer in connection with the transaction.</p>
              <p><b>Law and Harm:</b> We cooperate with government and law enforcement officials or private parties to enforce and comply with the law. We may disclose any information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate: (i) to respond to claims, legal process (including subpoenas); (ii) to protect our property, rights and safety and the property, rights and safety of a third party or the public in general; and (iii) to stop any activity that we consider illegal or legally actionable activity.</p>
              <p><b>Modifying Your Information:</b> You can access and modify the information contained in your Profile through the App. Additionally, you have the ability to delete your profile entirely. We may archive/backup some information for our records or as otherwise required by law.</p>
              <p><b>Security:</b> We take reasonable administrative, physical and electronic measures designed to protect the information that we collect from or about you from unauthorized access, use or disclosure. However, that no method of transmitting information over the Internet or storing information is completely secure. Accordingly, we cannot guarantee the absolute security of any information.</p>
              <p><b>Links:</b> Our Services may contain links to websites and services that are owned or operated by third parties (each, a "Third Party Service"). Any information that you provide on or to a Third Party Service or that is collected by a Third Party Service is provided directly to the owner or operator of the Third Party Service and is subject to the owner's or operator's privacy policy. We're not responsible for the content, privacy or security practices and policies of any Third Party Service. To protect your information we recommend that you carefully review the privacy policies of all Third Party Services that you access.</p>
              <p><b>International Transfer:</b> Your account information may be transferred to, and maintained on, computers located outside of your state, province, country or other governmental jurisdiction where the privacy laws may not be as protective as those in your jurisdiction. If you're located outside the United States and choose to provide your account information to us, we may transfer your account information to the United States and process it there.</p>
              <p><b>Age:</b> You must be at least 13 years old to use the Service. DockIt does not knowingly collect or solicit any information from anyone under the age of 13 or knowingly allow such persons to register for the Service. The Service and its content are not directed at children under the age of 13</p>
              <p>If you have any thoughts or questions about this policy, please contact us at <a href='mailto:hello@dockit.me'>hello@dockit.me</a>.</p>
              <p className='copyright'>Copyright © {year} DockIt Inc. All rights reserved.</p>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ProfileSettingsAboutPrivacyPolicy;
