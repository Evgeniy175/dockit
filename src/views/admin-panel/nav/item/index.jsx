import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { TITLES } from '../constants';

@observer
class AdminPanelNavItem extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    const handler = get(this.props, 'onClick');
    const itemKey = this.props.itemKey;
    
    if (handler) handler(itemKey);
  }

  render() {
    const itemKey = this.props.itemKey;
    const isActive = this.props.active;
    const className = `admin-panel-nav-item${isActive ? ' active' : ''}`;
    
    return <span className={className} onClick={this.onClick}>{ TITLES[itemKey] }</span>;
  }
}

AdminPanelNavItem.propTypes = {
  itemKey: PropTypes.string,
  active: PropTypes.bool,
  onClick: PropTypes.func
};

export default AdminPanelNavItem;