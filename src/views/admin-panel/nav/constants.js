export const ITEMS = {
  USERS: 'users',
  REPORTS: 'reports'
};

export const TITLES = {
  [ITEMS.USERS]: 'Users',
  [ITEMS.REPORTS]: 'Reports'
};
