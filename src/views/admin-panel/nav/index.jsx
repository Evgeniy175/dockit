import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import NavItem from './item/index.jsx';

import { ITEMS } from './constants';

@observer
class AdminPanelNav extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onItemClick = ::this.onItemClickHandler;
  }
  
  onItemClickHandler(value) {
    const handler = get(this.props, 'handlers.onTabClick');
    
    if (handler) handler(value);
  }

  render() {
    const activeTab = get(this.props, 'values.activeTab');
    
    return (
      <div className='admin-panel-nav no-select'>
        {
          Object.keys(ITEMS).map(key => <NavItem key={key} itemKey={ITEMS[key]} active={activeTab == ITEMS[key]} onClick={this.onItemClick} />)
        }
      </div>
    );
  }
}

AdminPanelNav.propTypes = {
  values: PropTypes.object,
  handlers: PropTypes.object
};

export default AdminPanelNav;