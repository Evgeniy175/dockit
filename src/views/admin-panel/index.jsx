import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Col } from 'react-bootstrap';

import { setTitle } from '../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';

import Nav from './nav/index.jsx';
import Content from './content/index.jsx';

import DataState from './state/data';

@observer
class AdminPanel extends Component {
  constructor(props) {
    super(props);
    
    Object.assign(this, {
      data: new DataState()
    });
  
    this.data.fetchContent();
    setTitle(PAGE_TITLES[PAGES.ADMIN_PANEL]());
  }
  
  componentWillMount() {
    this.onTabClick = ::this.onTabClickHandler;
  }
  
  onTabClickHandler(value) {
    this.data.setActiveTab(value);
    
    this.data.fetchContent();
  }

  render() {
    const navProps = {
      values: {
        activeTab: this.data.activeTab
      },
      handlers: {
        onTabClick: this.onTabClick
      }
    };
    
    return (
      <Grid>
        <Col xs={12}>
          <div className='admin-panel'>
            <Nav values={navProps.values} handlers={navProps.handlers} />
            <Content />
          </div>
        </Col>
      </Grid>
    );
  }
}

export default AdminPanel;