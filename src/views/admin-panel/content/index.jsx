import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

@observer
class AdminPanelContent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='admin-panel-content'>
        content
      </div>
    );
  }
}

AdminPanelContent.propTypes = {
  values: PropTypes.object,
  handlers: PropTypes.object
};

export default AdminPanelContent;