import { action, observable, toJS } from 'mobx';

import { ITEMS } from '../nav/constants';

import DataState from '../../../utils/state/data';

class AdminPanelDataState extends DataState {
  @observable activeTab = ITEMS.USERS;
  
  RESOLVERS = {
    [ITEMS.USERS]: ::this.fetchUsersHandler,
    [ITEMS.REPORTS]: ::this.fetchReportsHandler
  };
  
  constructor(props = {}) {
    super(props);
  }
  
  @action
  setActiveTab(value) {
    if (value === this.activeTab) return;
    
    this.activeTab = value;
  }
  
  fetchContent() {
    return this.RESOLVERS[this.activeTab]()
    .then(res => { console.dir(res); });
  }
  
  // todo
  fetchUsersHandler() {
    return Promise.resolve(1);
  }
  
  // todo
  fetchReportsHandler() {
    return Promise.resolve(2);
  }
}

export default AdminPanelDataState;
