import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { EVENT_SHOW_CONSTANTS } from '../../../../../../constants';
import { formatDate } from '../../../../../../utils/formatting/date';



class ScheduleEnd extends Component {
  schedule;

  constructor(props) {
    super(props);
    this.schedule = props.schedule;
  }

  render() {
    if (this.isNeverEnds()) return null;

    return (
      <div className='date-block'>
        <span>
          Ends { this.getEndDate() }
        </span>
      </div>
    );
  }

  isNeverEnds() {
    return !this.schedule.endDate;
  }

  getEndDate() {
    return formatDate(moment(this.schedule.endDate), EVENT_SHOW_CONSTANTS.DATE_FORMAT);
  }

  getEndTime() {
    return formatDate(moment(this.schedule.endTime), EVENT_SHOW_CONSTANTS.TIME_FORMAT);
  }
}

ScheduleEnd.propTypes = {
  schedule: PropTypes.object.isRequired,
  isAllDay: PropTypes.bool.isRequired,
};

export default ScheduleEnd;
