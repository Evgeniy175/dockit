import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';
import PropTypes from 'prop-types';
import moment from 'moment';
import { get } from 'lodash';

import ScheduleEnd from './schedule-end/index.jsx';

import { EVENT_SHOW_CONSTANTS } from '../../../../../constants';
import { formatDate } from '../../../../../utils/formatting/date';
import { getTimeZone, initDateWithTimezone } from '../../../../../utils/timezones';



@observer
class ScheduleDateInfo extends Component {
  @observable schedule;
  @observable isTimeLoaded = false;

  id;

  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    if (this.id === nextProps.schedule.id) return;
    this.init(nextProps);
  }

  init(props) {
    this.setSchedule(props.schedule);
    this.initTimeZone();
  }

  initTimeZone() {
    const data = this.schedule;
    const placeId = get(data, 'location.place_id');

    if (!placeId) return this.initTime(data);

    return getTimeZone(placeId)
    .then(tzData => {
      const timeZoneId = get(tzData, 'timeZoneId', Intl.DateTimeFormat().resolvedOptions().timeZone);
      const startDateTime = initDateWithTimezone(moment.utc(data.startTime), timeZoneId);
      const endDateTime = initDateWithTimezone(moment.utc(data.endTime), timeZoneId);
      this.setStartTime(startDateTime.format());
      this.setEndTime(endDateTime.format());
      this.setIsTimeLoaded(true);
      return Promise.resolve();
    });
  }

  initTime(data) {
    const startDateTime = moment(data.startTime);
    const endDateTime = moment(data.endTime);
    this.setStartTime(startDateTime.format());
    this.setEndTime(endDateTime.format());
    this.setIsTimeLoaded(true);
    return Promise.resolve();
  }

  @action
  setStartTime(value) {
    this.schedule.startTime = value;
  }

  @action
  setEndTime(value) {
    this.schedule.endTime = value;
  }

  @action
  setSchedule(value) {
    this.schedule = value;
  }

  @action
  setIsTimeLoaded(value) {
    this.isTimeLoaded = value;
  }

  render() {
    const isAllDay = this.isAllDay();
    return (
      <div className='start-container'>
        <div className='date-block'>
          <span>
            <span className='starts-word'>Starts</span> { this.getStartDate() }
          </span>
          { this.isTimeLoaded && <div className='time-block'>{ !isAllDay && `${this.getStartTime()} - ${this.getEndTime()}` }</div> }
          <ScheduleEnd schedule={this.schedule} isAllDay={isAllDay} />
          { isAllDay && <div className='all-day-label'>All Day</div> }
        </div>
      </div>
    );
  }

  isAllDay() {
    const start = moment(this.schedule.startTime);
    const end = moment(this.schedule.endTime);
    return start.isSame(end);
  }

  isNeverEnds() {
    return !this.schedule.endDate;
  }

  getStartDate() {
    return this.schedule.allDay
    ? moment.utc(this.schedule.startTime).format(EVENT_SHOW_CONSTANTS.DATE_FORMAT)
    : moment(this.schedule.startTime).format(EVENT_SHOW_CONSTANTS.DATE_FORMAT);
  }

  getStartTime() {
    return formatDate(moment(this.schedule.startTime), EVENT_SHOW_CONSTANTS.TIME_FORMAT);
  }

  getEndTime() {
    return formatDate(moment(this.schedule.endTime), EVENT_SHOW_CONSTANTS.TIME_FORMAT);
  }
}

ScheduleDateInfo.propTypes = {
  schedule: PropTypes.object.isRequired,
};

export default ScheduleDateInfo;
