import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { toJS, action, observable } from 'mobx';

import Arrow from './arrow/index.jsx';

import jq from '../../../../../utils/jquery';
import TextParser from '../../../../../utils/parsers/text';

import { TEXT_CLASS_NAME } from './constants';
import { DIRECTIONS } from './arrow/constants';



@observer
class ExpandableArrowText extends Component {
  @observable isExpandable = false;
  @observable isExpanded = false;
  parser = new TextParser();
  text;
  arrowHandlers;

  constructor(props) {
    super(props);
    this.arrowHandlers = {
      onClick: ::this.onArrowClick,
    };
  }

  componentDidMount() {
    const elem = jq.wrap(`.${TEXT_CLASS_NAME}`);
    this.setIsExpandable(elem.offsetHeight !== elem.scrollHeight);
  }

  onArrowClick() {
    if (!this.isExpandable) return;
    this.setIsExpanded(!this.isExpanded);
  }

  @action
  setIsExpanded(value) {
    this.isExpanded = value;
  }

  @action
  setIsExpandable(value) {
    this.isExpandable = value;
  }

  render() {
    const { text } = this.props.values;
    if (!text) return null;
    this.initText();
    const textClassName = `${TEXT_CLASS_NAME}${this.isExpanded ? '' : ' creation-description-collapsed'}`;
    const arrowValues = {
      direction: this.isExpanded ? DIRECTIONS.UP : DIRECTIONS.DOWN,
    };
    return (
      <div className='creation-info-description'>
        <div className={textClassName}>{this.text}</div>
        {this.isExpandable && <Arrow values={arrowValues} handlers={this.arrowHandlers} />}
      </div>
    );
  }

  initText() {
    const { text, userTags } = this.props.values;
    this.parser.init({
      string: text,
      userTags,
    });
    this.text = this.parser.parse();
  }
}

ExpandableArrowText.propTypes = {
  values: PropTypes.shape({
    text: PropTypes.string,
    userTags: PropTypes.array,
  }).isRequired,
};

export default ExpandableArrowText;
