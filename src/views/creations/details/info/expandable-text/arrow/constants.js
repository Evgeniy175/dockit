export const DIRECTIONS = {
  UP: 'up',
  DOWN: 'down',
};

export const CLASSES = {
  [DIRECTIONS.UP]: 'fa-chevron-up',
  [DIRECTIONS.DOWN]: 'fa-chevron-down',
};

export const DEFAULT_DIRECTION = DIRECTIONS.DOWN;
