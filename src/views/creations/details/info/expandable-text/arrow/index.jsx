import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';

import { CLASSES, DEFAULT_DIRECTION } from './constants';



@observer
class ExpandableArrowTextArrow extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { direction } = this.props.values;
    const { onClick } = this.props.handlers;
    return (
      <div className='creation-info-description-arrow-wrapper' onClick={onClick}>
        <i className={`creation-info-description-arrow fa ${CLASSES[direction || DEFAULT_DIRECTION]}`} />
      </div>
    );
  }
}

ExpandableArrowTextArrow.propTypes = {
  values: PropTypes.shape({
    direction: PropTypes.string.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onClick: PropTypes.func.isRequired,
  }).isRequired,
};

export default ExpandableArrowTextArrow;
