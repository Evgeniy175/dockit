import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { get } from 'lodash';

import Repeater from './repeater/index.jsx';
import Description from './expandable-text/index.jsx';
import Title from '../../../../shared-components/creations/title/index.jsx';
import Creator from '../../../../shared-components/creations/creator/index.jsx';
import ActionBar from '../../../../shared-components/action-bar/index.jsx';
import Members from '../../../../shared-components/creations/members/index.jsx';
import Location from '../../../../shared-components/location/index.jsx';
import ScheduleDataInfo from './schedule-date-info/index.jsx';
import Images from '../../../../shared-components/creations/images/index.jsx';

import { TYPES } from '../../constants';

import DataState from './state/data';



@observer
class CreationInfo extends Component {
  isNeedToRenderTime = false;

  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    this.data.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }

  componentWillUnmount() {
    this.data.clear();
  }
  
  render() {
    const { type } = this.props.values;
    const { creation } = this.data;
    const { description, userTags, attached_images_preview } = creation;
    const members = toJS(this.data.members);
    const dockActionData = this.data.getDockActionData();
    const likeActionData = this.data.getLikeActionData();
    const location = this.data.getLocation();
    const repeater = get(creation, 'repeater');
    const descriptionValues = {
      text: toJS(description),
      userTags: toJS(userTags),
    };
    const imagesValues = {
      target: type,
      id: creation.id,
      images: toJS(attached_images_preview)
    };

    return (
      <div className='creation-info'>
        <Title type={type} item={creation} />
        <Creator item={creation} />
        { repeater ? <ScheduleDataInfo schedule={creation} /> : <div className='date'>{ this.renderDate() }</div> }
        { this.isNeedToRenderTime && <div className='time'>{ this.data.getFormattedTime() }</div> }
        { repeater && <Repeater repeater={repeater} /> }
        { location && location.length > 0 && <Location item={ creation } /> }
        <ActionBar dockActionData={dockActionData} likeActionData={likeActionData} schedule={creation} isSchedule={type === TYPES.SCHEDULE} />
        <Members members={members} />
        <Description values={descriptionValues} />
        {/*<Images values={imagesValues}/>*/}
      </div>
    );
  }

  renderDate() {
    const { creation, formattedStartDate, formattedStartTime, formattedEndDate, formattedEndTime } = this.data;
    const { startTime, endTime } = creation;

    if (!startTime) return null;
    if (startTime === endTime) { this.isNeedToRenderTime = true; return formattedStartDate; }

    this.isNeedToRenderTime = false;

    return (
      <div>
        <div>{ formattedStartDate }, { formattedStartTime }</div>
        <div>{ formattedEndDate }, { formattedEndTime }</div>
      </div>
    );
  }
}

CreationInfo.propTypes = {
  values: PropTypes.shape({
    creation: PropTypes.object.isRequired,
    members: PropTypes.array.isRequired,
    type: PropTypes.string.isRequired,
  }).isRequired,
};

export default CreationInfo;
