import { action, observable, computed, toJS } from 'mobx';
import moment from 'moment';

import { ALL_DAY_TEXT } from '../constants';
import { EVENT_SHOW_CONSTANTS } from '../../../../../constants';

import { getLocation, getLocationCoordinates } from '../../../../../utils/location';

import DataState from '../../../../../utils/state/data';



class CreationInfoDataState extends DataState {
  @observable creation;
  @observable members;
  @observable isExpanded = false;
  @observable isExpandable = true;
  type;

  @computed
  get isAllDay() {
    return this.creation.allDay;
  }

  @computed
  get formattedStartDate() {
    const date = this.creation.startTime;
    return this.isAllDay ? moment.utc(date).format(EVENT_SHOW_CONSTANTS.DATE_FORMAT) : moment(date).format(EVENT_SHOW_CONSTANTS.DATE_FORMAT);
  }

  @computed
  get formattedStartTime() {
    const startTime = moment(this.creation.startTime);
    const endTime = moment(this.creation.endTime);
    return this.isAllDay ? ALL_DAY_TEXT : startTime.format(EVENT_SHOW_CONSTANTS.TIME_FORMAT);
  }

  @computed
  get formattedEndDate() {
    const date = this.creation.endTime;
    return this.isAllDay ? moment.utc(date).format(EVENT_SHOW_CONSTANTS.DATE_FORMAT) : moment(date).format(EVENT_SHOW_CONSTANTS.DATE_FORMAT);
  }

  @computed
  get formattedEndTime() {
    const startTime = moment(this.creation.startTime);
    const endTime = moment(this.creation.endTime);
    return this.isAllDay ? ALL_DAY_TEXT : endTime.format(EVENT_SHOW_CONSTANTS.TIME_FORMAT);
  }

  constructor(props = {}) {
    super(props);
  }

  init(props) {
    const { creation, members, type } = props.values;
    this.setCreation(creation);
    this.setMembers(members);
    this.type = type;
  }

  getFormattedTime() {
    const startTime = moment(this.creation.startTime);
    const endTime = moment(this.creation.endTime);

    if (startTime.isSame(endTime)) return ALL_DAY_TEXT;

    const startDateText = startTime.format(EVENT_SHOW_CONSTANTS.TIME_FORMAT);
    const endDateText = endTime.format(EVENT_SHOW_CONSTANTS.TIME_FORMAT);
    return `${startDateText} - ${endDateText}`;
  }
  
  getLocation() {
    return getLocation(this.creation);
  }
  
  getLocationCoords() {
    return getLocationCoordinates(this.creation);
  }

  getDockActionData() {
    return {
      type: this.type,
      event: this.creation,
    };
  }

  getLikeActionData() {
    return {
      type: this.type,
      id: this.creation.id,
      likes: this.creation.decorated.likes,
    };
  }

  @action
  setMembers(value) {
    this.members = value;
  }

  @action
  setCreation(value) {
    this.creation = value;
  }

  @action
  toggleExpanded() {
    this.isExpanded = !this.isExpanded;
  }

  @action
  setIsExpandable(value) {
    this.isExpandable = value;
  }
  
  @action
  clear() {
    this.creation = null;
    this.members = null;
  }
}

export default CreationInfoDataState;