import { DAYS_OF_WEEK } from '../../../../../constants';

export function formatDay(dayNumber) {
  return DAYS_OF_WEEK[dayNumber];
}
export const REPEATER_TILES = {
  'w': repeater => `${repeater.frequency === 1 ? '' : repeater.frequency} week on ${repeater.weekdays.map(formatDay).join(', ')}`,
  'm': repeater => `month`,
  'd': repeater => `day`,
  'y': repeater => `year`,
};
