import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { REPEATER_TILES } from './constants';

@observer
class Repeater extends Component {
  render() {
    const repeater = this.props.repeater;
    if (!repeater) return null;
    const repeatTerm = REPEATER_TILES[repeater.type](repeater);
    return (
      <div className='repeat'>
        {`Repeats every ${repeatTerm}`}
      </div>
    );
  }
}

Repeater.propTypes = {
  repeater: PropTypes.object,
};

export default Repeater;