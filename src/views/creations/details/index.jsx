import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';

import Info from './info/index.jsx';
import ScrollToTop from '../../../shared-components/scroll-to-top/index.jsx';
import LightboxWrapper from '../../../shared-components/lightbox-wrapper/index.jsx';
import BackgroundImage from '../../../shared-components/background-image/index.jsx';
import CommentsBlock from '../../../shared-components/creations/comments-block/index.jsx';
import Loader from '../../../shared-components/loader/index.jsx';
import Modal from '../../../shared-components/question-modal/index.jsx';

import DataState from './state/data';

import { Auth } from '../../../auth';

import { MODAL_TITLE } from './constants';
import { TYPES } from '../constants';
import { FEED_PATH } from '../../../constants';
import { CSS_MAPPING } from '../../../shared-components/button/constants';



@observer
class CreationDetails extends Component {
  @observable isBackgroundImageOpened = false;
  
  constructor(props) {
    super(props);
    Object.assign(this, Object.freeze({
      data: new DataState(props),
    }));

    this.onImageClick = ::this.onImageClickHandler;
    this.onImageClose = ::this.onImageCloseHandler;
    this.onModalClose = ::this.onModalCloseHandler;

    this.data.fetch();
    this.data.fetchMembers();
  }

  componentWillReceiveProps(nextProps) {
    const oldId = this.data.id;
    if (oldId === nextProps.routeParams.id) return;
    this.data.init(nextProps);
    this.data.fetch();
    this.data.fetchMembers();
  }
  
  componentWillUnmount() {
    this.data.clear();
  }
  
  onImageClickHandler() {
    this.setIsBackgroundImageOpened(true);
  }
  
  onImageCloseHandler() {
    this.setIsBackgroundImageOpened(false);
  }

  onModalCloseHandler() {
    browserHistory.push(FEED_PATH);
  }
  
  @action
  setIsBackgroundImageOpened(value) {
    this.isBackgroundImageOpened = value;
  }

  render() {
    const { isSigned, isPrivate } = this.data;
    if (isPrivate) return <Modal className='creation-details-private-modal' open={isPrivate} title={MODAL_TITLE} onClose={this.onModalClose} buttons={this.getModalButtons()} />;
    if (!this.data.isLoaded) return <Loader />;

    const { type, creation, members } = this.data;
    const isUserCreator = isSigned && creation.user_id === Auth.getActiveUser().id;
    const imgUrl = this.data.getImage(creation);
    const lightboxImages = [{ src: imgUrl }];
    const infoValues = {
      creation,
      members: toJS(members),
      type,
    };

    return (
      <div>
        <ScrollToTop />
        <BackgroundImage imageUrl={imgUrl} onClickForLightboxOpen={this.onImageClick} />
        <LightboxWrapper open={this.isBackgroundImageOpened} images={lightboxImages} onClose={this.onImageClose} />
        <Grid className='creation-item-show'>
          <Row>
            <Col xs={12} md={8} mdOffset={2} lg={6} lgOffset={3}>
              <Info values={infoValues} />
              <CommentsBlock id={creation.id} isUserCreation={isUserCreator} isForSchedule={type === TYPES.SCHEDULE} />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

  getModalButtons() {
    return [
      {
        text: 'Okay',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: this.onModalClose
      },
    ];
  }
}

export default CreationDetails;
