import { action, observable, computed } from 'mobx';
import Promise from 'bluebird';
import { get } from 'lodash';

import EventModel from '../../../../models/events';
import ScheduleModel from '../../../../models/schedules';
import UserModel from '../../../../models/users';

import { NEED_MEMBERS_COUNT, RESOLUTION } from '../constants';
import { TYPES } from '../../constants';
import { FORBIDDEN_STATUS } from '../../../../constants';
import { PAGES, PAGE_TITLES } from '../../../../utils/formatting/constants';

import { setTitle } from '../../../../utils/formatting/title';
import { recognizeCreationType } from '../../../../utils/creations';

import { Auth } from '../../../../auth';

import DataState from '../../../../utils/state/data';

const eventModel = new EventModel();
const scheduleModel = new ScheduleModel();
const userModel = new UserModel();



class CreationDescriptionDataState extends DataState {
  @observable creation = {};
  @observable members = [];
  @observable isPrivate = false;

  type;
  id = '';

  fetchResolvers;
  imageResolvers;

  get isLoaded() {
    return !!get(this.creation, 'id');
  }

  get isSigned() {
    return Auth.isSigned();
  }
  
  constructor(props = {}) {
    super(props);
    this.init(props);

    this.fetchResolvers = {
      [TYPES.EVENT]: ::this.fetchEvent,
      [TYPES.SCHEDULE]: ::this.fetchSchedule,
    };

    this.imageResolvers = {
      [TYPES.EVENT]: ::this.getEventImage,
      [TYPES.SCHEDULE]: ::this.getScheduleImage,
    };

    this.titleKeyResolvers = {
      [TYPES.EVENT]: PAGES.EVENT,
      [TYPES.SCHEDULE]: PAGES.SCHEDULE,
    };
  }

  init(props) {
    this.id = props.routeParams.id;
    this.type = recognizeCreationType();
  }

  fetch() {
    return this.fetchResolvers[this.type](this.id)
    .then(creation => {
      this.setCreation(creation);
      setTitle(PAGE_TITLES[this.titleKeyResolvers[this.type]](creation.title));
      return Promise.resolve();
    })
    .catch(err => {
      if (err && err.res && err.res.status === FORBIDDEN_STATUS) this.setIsPrivate(true);
    });
  }
  
  fetchEvent(id) {
    return eventModel.fetchById(id);
  }

  fetchSchedule(id) {
    return scheduleModel.fetchById(id);
  }

  fetchMembers(limit = NEED_MEMBERS_COUNT) {
    const config = {
      params: {
        limit,
        query: {
          intent: 'docked',
        },
      },
    };

    if (!this.isSigned) return Promise.resolve();

    return Promise.all([
      userModel.fetchFollowingsWhoIntended(this.type, this.id, Object.assign({}, config)),
      userModel.fetchNonFollowingsWhoIntended(this.type, this.id, Object.assign({}, config)),
    ])
    .spread((followings, nonFollowings) => {
      let members = followings.data.slice(0, limit);
      members = members.concat(nonFollowings.data).slice(0, limit);
      this.setMembers(members);
    });
  }

  getImage(creation) {
    return this.imageResolvers[this.type](creation);
  }

  getEventImage(creation) {
    const creationImage = get(creation, 'image');
    return creationImage ? EventModel.formatImageUrl(creationImage, undefined, RESOLUTION) : ScheduleModel.formatImageUrl(get(creation, 'schedule.image'), undefined, RESOLUTION);
  }

  getScheduleImage(creation) {
    return ScheduleModel.formatImageUrl(get(creation, 'image'), undefined, RESOLUTION);
  }
  
  @action
  setCreation(value) {
    this.creation = value;
  }

  @action
  setMembers(value) {
    this.members = value;
  }

  @action
  setIsPrivate(value) {
    this.isPrivate = value;
  }
  
  @action
  clear() {
    this.setCreation({});
    this.setMembers([]);
    this.id = '';
  }
}

export default CreationDescriptionDataState;
