export const NEED_MEMBERS_COUNT = 7;

export const MODAL_TITLE = 'Unfortunately, you do not have access to this event.';

export const RESOLUTION = screen.width > screen.height ? screen.width : screen.height;
