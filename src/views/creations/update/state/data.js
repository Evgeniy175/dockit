import React, { Component } from 'react';
import { action, observable, computed, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import { get, isEmpty, debounce } from 'lodash';
import moment from 'moment-timezone';
import Promise from 'bluebird';

import { setTitle } from '../../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../../utils/formatting/constants';
import { initNotifications } from '../../../../utils/notifications';
import { values } from '../../../../utils/object';

import { FIELDS, ERRORS, LOCATION_LOAD_DELAY, PERMISSIONS, EVENT_FIELDS, SCHEDULE_FIELDS } from '../constants';
import { TYPES } from '../../constants';
import { INPUT_FORMATS, EVENTS_CONSTANTS } from '../../../../constants';
import { DISCOVERABLE_MODAL_KEY } from '../../creating/constants';

import LocationModel from '../../../../models/locations';
import EventModel from '../../../../models/events';
import ScheduleModel from '../../../../models/schedules';
import CategoriesModel from '../../../../models/categories';
import InviteModel from '../../../../models/invites';

import DataState from '../../../../utils/state/data';

import { getTimeZone, formatDateWithTimezone, formatDateWithTimezoneToUtc, initDateWithTimezone } from '../../../../utils/timezones';
import { recognizeCreationType } from '../../../../utils/creations';

import { isNameValid, isMomentValid, isEndAfterStartDateTime } from '../../../../utils/validation/creation';
import { dataUriToBlob } from '../../../../utils/formatting/image';

const locationModel = new LocationModel();
const categoriesModel = new CategoriesModel();
const inviteModel = new InviteModel();



class CreationEditDataState extends DataState {
  type;
  permissions = PERMISSIONS;
  id;
  
  @observable isLoaded = false;

  @observable creation = {};

  @observable name = '';
  @observable description = '';
  
  @observable image = null;
  @observable imageUrl = null;
  
  @observable selectedPermission = {};
  @observable selectedLocation = {};
  @observable locationInputValue;
  initialLocation;

  isLocationChanged = false;

  get isLocationRemoved() {
    return this.isLocationChanged && (!this.selectedLocation || isEmpty(this.selectedLocation));
  }

  get isLocationSameToInitial() {
    return !this.isLocationChanged || JSON.stringify(this.selectedLocation) === this.initialLocation;
  }

  @observable isCategoriesModalVisible = false;
  @observable selectedCategories = [];

  @observable isAlertsModalVisible = false;
  @observable alerts = [];

  @observable invitedPeople = [];
  
  @observable isAllDay = false;

  @observable discoverable = false;
  @observable isDiscoverModalShown = false;
  
  @observable startDateTime = moment().add(1, 'hour').startOf('hour');
  @observable endDateTime = moment().add(2, 'hour').startOf('hour');

  @observable validation = {};
  @observable isValidatorMessageVisible = false;
  validationMessage = null;

  @computed
  get startDate() {
    const dateBuff = moment(this.startDateTime);
    const utcDate = moment().utc().set({ year: dateBuff.get('year'), month: dateBuff.get('month'), date: dateBuff.get('date'), hour: 0, minute: 0, second: 0 });
    const date = this.isAllDay ? utcDate : dateBuff;
    return date.format(INPUT_FORMATS.DATE);
  }
  set startDate(value) {
    this.startDateTime = this.getDate(this.startDateTime, value);
    this.endDateTime = moment(this.startDateTime).add(1, 'hours');
  }
  
  @computed
  get startTime() {
    const timeBuff = moment(this.startDateTime);
    const utcTime = moment().utc().set({ hour: timeBuff.get('hour'), minute: timeBuff.get('minute'), second: timeBuff.get('second') });
    const time = this.isAllDay ? utcTime : timeBuff;
    return time.format(INPUT_FORMATS.TIME);
  }
  set startTime(value) {
    this.startDateTime = this.getTime(this.startDateTime, value);
    this.endDateTime = moment(this.startDateTime).add(1, 'hours');
  }
  
  @computed
  get endDate() {
    const dateBuff = moment(this.endDateTime);
    const utcDate = moment().utc().set({ year: dateBuff.get('year'), month: dateBuff.get('month'), date: dateBuff.get('date'), hour: 0, minute: 0, second: 0 });
    const date = this.isAllDay ? utcDate : dateBuff;
    return date.format(INPUT_FORMATS.DATE);
  }
  set endDate(value) {
    this.endDateTime = this.getDate(this.endDateTime, value);
  }
  
  @computed
  get endTime() {
    const timeBuff = moment(this.endDateTime);
    const utcTime = moment().utc().set({ hour: timeBuff.get('hour'), minute: timeBuff.get('minute'), second: timeBuff.get('second') });
    const time = this.isAllDay ? utcTime : timeBuff;
    return time.format(INPUT_FORMATS.TIME);
  }
  set endTime(value) {
    this.endDateTime = this.getTime(this.endDateTime, value);
  }

  @computed
  get Model() {
    return this.ModelResolver[this.type];
  }

  @computed
  get model() {
    return this.modelResolver[this.type];
  }

  @computed
  get submitFields() {
    return this.submitFieldsResolver[this.type];
  }

  @computed
  get isSchedule() {
    return this.type === TYPES.SCHEDULE;
  }

  getDate(date, value) {
    value = moment(value, INPUT_FORMATS.DATE);
    return moment(date).set({ year: value.get('year'), month: value.get('month'), date: value.get('date') });
  }

  getTime(dateTime, value) {
    value = moment(value, INPUT_FORMATS.TIME);
    return moment(dateTime).set({ hour: value.get('hour'), minute: value.get('minute'), second: value.get('second') });
  }
  
  constructor(props = {}) {
    super(props);
    this.id = props.params.id;
    this.type = recognizeCreationType();
    this.setSelectedPermission(this.permissions[0]);

    this.titleKeyResolvers = {
      [TYPES.EVENT]: PAGES.EVENT,
      [TYPES.SCHEDULE]: PAGES.SCHEDULE,
    };

    this.submitFieldsResolver = {
      [TYPES.EVENT]: EVENT_FIELDS,
      [TYPES.SCHEDULE]: SCHEDULE_FIELDS,
    };

    this.ModelResolver = {
      [TYPES.EVENT]: EventModel,
      [TYPES.SCHEDULE]: ScheduleModel,
    };

    this.modelResolver = {
      [TYPES.EVENT]: new EventModel(),
      [TYPES.SCHEDULE]: new ScheduleModel(),
    };
  }

  fetch() {
    return this.model.fetchById(this.id)
    .then(res => {
      this.parseResponse(res);
      setTitle(PAGE_TITLES[this.titleKeyResolvers[this.type]](this.name));
      return Promise.resolve(res);
    })
    .catch(console.error);
  }
  
  parseResponse(data) {
    const selectedPermission = this.permissions.find(permission => permission.value === data.permission);

    const isAllDay = data.allDay;
    const discoverable = !!data.discoverable;

    const startDateTime = moment(data.startTime);
    const endDateTime = moment(data.endTime);
  
    this.creationId = data.id;
    this.setLocationInputValue(get(data, 'location.title') || get(data, 'locationRaw.title'));
    this.setCreation(data);
    this.setName(data.title);
    this.setSelectedCategories(data.categories);
    this.setDescription(data.description);
    this.setAlerts(data.myAlerts);
    this.setImageUrl(data.image);
    this.setSelectedPermission(selectedPermission);
    this.setIsAllDay(isAllDay);
    this.setDiscoverable(discoverable);
    this.setStartDateTime(startDateTime);
    this.setEndDateTime(endDateTime);

    this.initLocation(data);
    this.initDateTime(data);
    this.setIsLoaded(true);
  }

  initLocation(data) {
    const locationLabel = get(data, 'location.title');
    const locationValue = get(data, 'location.place_id');
    if (!locationLabel || !locationValue) return;
    this.initialLocation = JSON.stringify({ label: locationLabel, value: locationValue });
    this.setLocation({ label: locationLabel, value: locationValue });
  }

  initDateTime(data) {
    const placeId = get(data, 'location.place_id');

    if (!placeId) {
      this.setStartDateTime(moment(data.startTime));
      this.setEndDateTime(moment(data.endTime));
      return Promise.resolve();
    }

    return getTimeZone(placeId)
    .then(tzData => {
      const timeZoneId = get(tzData, 'timeZoneId', Intl.DateTimeFormat().resolvedOptions().timeZone);
      const startDateTime = initDateWithTimezone(moment.utc(data.startTime), timeZoneId);
      const endDateTime = initDateWithTimezone(moment.utc(data.endTime), timeZoneId);
      this.setStartDateTime(startDateTime);
      this.setEndDateTime(endDateTime);
      return Promise.resolve();
    });
  }

  validate(field, isNeedToShowValidatorMessage) {
    this.clearValidation();
  
    if (!isNameValid(this.name)) this.addError(FIELDS.NAME);
    if (!isMomentValid(this.startDateTime)) this.addError(FIELDS.START_DATE_TIME);
    if (!this.isAllDay && !isEndAfterStartDateTime(this.startDateTime, this.endDateTime)) this.addError(FIELDS.END_DATE_TIME);

    if (!isNeedToShowValidatorMessage || (field && !this.validation[field])) return;

    this.validationMessage = this.validation[field] ? <div>{this.validation[field]}</div> : values(this.validation).map(err => <div key={err}>{err}</div>);
    this.setValidatorMessageVisibility(isNeedToShowValidatorMessage && values(this.validationMessage).length > 0);
  }

  update() {
    const placeId = get(this.selectedLocation, 'value');
    const removeLocation = this.isLocationRemoved || !this.isLocationSameToInitial ? this.model.removeLocation(this.creationId) : Promise.resolve();

    return removeLocation.then(() => getTimeZone(placeId))
    .then(tzData => {
      const timeZoneId = get(tzData, 'timeZoneId', Intl.DateTimeFormat().resolvedOptions().timeZone);
      const creation = this.getCreation(timeZoneId);
      return this.model.updateOne(this.creationId, { body: JSON.stringify(creation) });
    })
    .then(res => {
      return Promise.all([
        //this.getCategoriesRequests(res.id),
        this.getAlertsRequests(res.id),
        //this.getNotificationsRequests(res.id),
      ]);
    })
    .then(initNotifications);
  }

  getCategoriesRequests(id) {
    return categoriesModel.addTo(this.type, id, toJS(this.selectedCategories).map(cat => cat.id));
  }

  getAlertsRequests(creationId) {
    return this.model.addAlerts(creationId, { body: JSON.stringify({ alerts: this.alerts }) });
  }

  // todo (evgeniy)
  getNotificationsRequests(creationId) {
    return Promise.resolve();

    return this.invitedPeople.filter(item => item.isChecked)
    .map(item => inviteModel.sendScheduleInvite(creationId, item.id));
  }
  
  getCreation(timeZoneId) {
    const location = !isEmpty(this.selectedLocation)
    ? { place_id: get(this.selectedLocation, 'value') }
    : { title: this.locationInputValue };

    const startDate = formatDateWithTimezone(this.startDateTime, timeZoneId, EVENTS_CONSTANTS.DATE_FORMAT);

    let data = {
      title: this.name,
      allDay: this.isAllDay,
      startDate: startDate,
      startTime: startDate,
      endTime: formatDateWithTimezone(this.endDateTime, timeZoneId, EVENTS_CONSTANTS.DATE_FORMAT),
      user_id: Auth.getActiveUser().id,
      permission: get(this.selectedPermission, 'value'),
      discoverable: get(this.selectedPermission, 'canToggleDiscoverable') && toJS(this.discoverable),
      description: this.description,
      location,
    };

    let creation = {};
    this.submitFields.forEach(key => creation[key] = data[key]);
    if (!get(location, 'title.length') && !get(location, 'place_id.length')) delete creation.location;
    if (this.isAllDay) creation.endTime = creation.startTime;

    return creation;
  }
  
  sendImage(creationId) {
    if (!this.image || isEmpty(this.image)) return Promise.resolve();
  
    const formData = new FormData();
    formData.append('image', dataUriToBlob(this.image));
    const config = { body: formData };
    return this.model.uploadImage(creationId, config);
  }

  deleteCoverImage() {
    return this.model.deleteImage(this.id);
  }
  
  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }
  
  @action
  addError(key) {
    this.validation[key] = ERRORS[key];
    this.validation = toJS(this.validation);
  }
  
  @action
  addSummaryError(value) {
    this.validation.summary = this.validation.summary || [];
    this.validation.summary.push(value);
    this.validation = toJS(this.validation);
  }
  
  @action
  clearValidation(key) {
    if (key) {
      delete this.validation[key];
      this.validation = toJS(this.validation);
      return;
    }
    
    this.validation = {};
  }
  
  @action
  clear() {
    this.discoverable = false;
    this.isAllDay = false;
    this.image = {};
    this.selectedLocation = {};
    this.validation = {};
    this.name = '';
    this.description = '';
    this.startDateTime = moment().add(1, 'hour').startOf('hour');
    this.endDateTime = moment().add(2, 'hour').startOf('hour');
    this.selectedPermission = {};
    this.isLoaded = false;
  }
  
  @action
  setImage(value) {
    this.image = value;
  }
  
  @action
  setImageUrl(value) {
    this.imageUrl = this.Model.formatImageUrl(value);
  }
  
  @action
  setSelectedPermission(value) {
    this.selectedPermission = value;
    this.setDiscoverable(value.canToggleDiscoverable && value.defaultDiscoverable);
  }
  
  @action
  setDiscoverable(value) {
    this.discoverable = value;
  }
  
  @action
  toggleDiscoverable() {
    const expectedValue = !this.discoverable;
    if (!expectedValue || localStorage.getItem(DISCOVERABLE_MODAL_KEY)) return this.setDiscoverable(expectedValue);
    this.setDiscoverModalVisibility(expectedValue && !localStorage.getItem(DISCOVERABLE_MODAL_KEY));
  }

  @action
  setDiscoverModalVisibility(value) {
    this.isDiscoverModalShown = value;
  }
  
  @action
  toggleIsAllDay() {
    this.isAllDay = !this.isAllDay;
    this.startTime = '00:00';
    this.endTime = this.isAllDay ? this.startTime : '01:00';
    this.repeatEndTime = '00:00';
  }
  
  @action
  setIsAllDay(value) {
    this.isAllDay = value;
    this.startTime = '00:00';
    this.endTime = this.isAllDay ? this.startTime : '01:00';
    this.repeatEndTime = '00:00';
  }
  
  @action
  setLocation(value) {
    this.isLocationChanged = true;
    this.selectedLocation = value;
  }

  @action
  setLocationInputValue(value) {
    this.locationInputValue = value;
  }

  @action
  setCategoriesModalVisibility(value) {
    this.isCategoriesModalVisible = value;
  }

  @action
  setSelectedCategories(value) {
    this.selectedCategories = value;
  }

  @action
  setAlertsModalVisibility(value) {
    this.isAlertsModalVisible = value;
  }

  @action
  setAlerts(value) {
    this.alerts = value;
  }

  @action
  setInvitedPeople(value) {
    this.invitedPeople = value;
  }

  @action
  setCreation(value) {
    this.creation = value;
  }
  
  @action
  setName(value) {
    this.name = value;
  }
  
  @action
  setDescription(value) {
    this.description = value;
  }
  
  @action
  setStartDate(value) {
    this.startDate = value;
  }
  
  @action
  setStartTime(value) {
    this.startTime = value;
  }
  
  @action
  setEndDate(value) {
    this.endDate = value;
  }
  
  @action
  setEndTime(value) {
    this.endTime = value;
  }
  
  @action
  setStartDateTime(value) {
    this.startDateTime = value;
  }
  
  @action
  setEndDateTime(value) {
    this.endDateTime = value;
  }

  @action
  setValidatorMessageVisibility(value) {
    this.isValidatorMessageVisible = value;
  }
}

export default CreationEditDataState;
