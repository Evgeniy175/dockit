import { action, observable, toJS } from 'mobx';

import UIState from '../../../../utils/state/data';



class CreationEditUiState extends UIState {
  @observable isCreatingPerforms = false;
  
  constructor(props = {}) {
    super(props);
  }

  @action
  setIsCreatingPerforms(value) {
    this.isCreatingPerforms = value;
  }
}

export default CreationEditUiState;
