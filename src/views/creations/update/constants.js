export const LOCATION_LOAD_DELAY = 250;

export const EVENT_FIELDS = [
  'title',
  'allDay',
  'startTime',
  'endTime',
  'user_id',
  'permission',
  'discoverable',
  'description',
  'location',
];

export const SCHEDULE_FIELDS = [
  'title',
  'allDay',
  'startDate',
  'startTime',
  'endTime',
  'timezone',
  'permission',
  'discoverable',
  'description',
  'location',
];

export const FIELDS = {
  NAME: 'name',
  LOCATION: 'location',
  START_DATE_TIME: 'start date time',
  START_DATE: 'start date',
  START_TIME: 'start time',
  END_DATE_TIME: 'end date time',
  END_DATE: 'end date',
  END_TIME: 'end time',
  SUMMARY: 'summary'
};

export const ERRORS = {
  [FIELDS.NAME]: 'Please enter an event name',
  [FIELDS.LOCATION]: 'Select location',
  [FIELDS.START_DATE_TIME]: 'Select start date and time',
  [FIELDS.START_DATE]: 'Start date wrong',
  [FIELDS.START_TIME]: 'Start time wrong',
  [FIELDS.END_DATE_TIME]: 'The end time should be after the start time',
  [FIELDS.END_DATE]: 'End date wrong',
  [FIELDS.END_TIME]: 'End time wrong',
  [FIELDS.SUMMARY]: 'Error'
};

export const PERMISSIONS = [
  {
    title: 'Private',
    value: 'private',
    canToggleDiscoverable: false
  }, {
    title: 'Followers',
    value: 'followers',
    canToggleDiscoverable: false
  }, {
    title: 'Public',
    value: 'global',
    canToggleDiscoverable: true,
    defaultDiscoverable: true
  },
];
