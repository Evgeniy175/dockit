import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Form, Row, Col } from 'react-bootstrap';
import { get, isEmpty } from 'lodash';

import { FIELDS, ERRORS } from './constants';

import { browserHistory } from 'react-router';

import Loader from '../../../shared-components/loader/index.jsx';

import Name from '../../../shared-components/creations/name/index.jsx';
import Location from '../../../shared-components/location-select/index.jsx';
import Categories from '../../../shared-components/creations/categories/index.jsx';
import AllDay from '../../../shared-components/creations/all-day/index.jsx';
import StartDateTime from '../../../shared-components/creations/start-date-time/index.jsx';
import EndDateTime from '../../../shared-components/creations/end-date-time/index.jsx';
import Description from '../../../shared-components/creations/description/index.jsx';
import Alerts from '../../../shared-components/alerts/index.jsx';
import Permissions from '../../../shared-components/creations/permissions/index.jsx';
import Discoverable from '../../../shared-components/creations/discoverable/index.jsx';
import Inviter from '../../../shared-components/creations/inviter/index.jsx';
import Repeater from '../../../shared-components/creations/repeater/index.jsx';
import Buttons from './buttons/index.jsx';

import ValidatorMessage from '../../../shared-components/validator-message/index.jsx';
import ValidationSummary from '../../../shared-components/validation-summary/index.jsx';
import BackgroundImage from '../../../shared-components/background-image/index.jsx';

import { values } from '../../../utils/object';
import jq from '../../../utils/jquery';

import DataState from './state/data';
import UiState from './state/ui';

import { CSS_MAPPING } from '../../../shared-components/button/constants';



@observer
class CreationEdit extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(props),
    });
    this.discoverModalButtons = [
      {
        text: 'Okay',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: ::this.onDiscoverModalOk,
      },
      {
        text: `Don't show again`,
        className: CSS_MAPPING.BLUE,
        onClick: ::this.onDiscoverModalDontShowAgain,
      },
    ];
    this.discoverableHandlers = {
      onChange: ::this.onDiscoverableToggleHandler,
      onModalClose: ::this.onDiscoverModalClose,
    };

    this.onUpdate = ::this.onUpdateHandler;
    this.onCancel = ::this.onCancelHandler;
    this.onImageUpload = ::this.onImageUploadHandler;
    this.onImageDelete = ::this.onImageDeleteHandler;
    this.onNameChange = ::this.onNameChangeHandler;
    this.onNameBlur = ::this.onNameBlurHandler;
    this.onLocationChange = ::this.onLocationChangeHandler;
    this.onLocationInputChange = ::this.onLocationInputChangeHandler;
    this.onAllDayChange = ::this.onAllDayChangeHandler;
    this.onStartDateChange = ::this.onStartDateChangeHandler;
    this.onStartTimeChange = ::this.onStartTimeChangeHandler;
    this.onEndDateChange = ::this.onEndDateChangeHandler;
    this.onEndTimeChange = ::this.onEndTimeChangeHandler;
    this.onPermissionChange = ::this.onPermissionChangeHandler;
    this.onDiscoverableToggle = ::this.onDiscoverableToggleHandler;
    this.onInvitedPeopleChange = ::this.onInvitedPeopleChangeHandler;
    this.onDescriptionChange = ::this.onDescriptionChangeHandler;
    this.onCategoriesOpen = ::this.onCategoriesOpenHandler;
    this.onCategoriesClose = ::this.onCategoriesCloseHandler;
    this.onValidatorMessageModalClose = ::this.onValidatorMessageModalCloseHandler;
    this.onAlertsOpen = ::this.onAlertsOpenHandler;
    this.onAlertsClose = ::this.onAlertsCloseHandler;

    this.data.fetch();
  }
  
  componentWillUnmount() {
    this.data.clear();
  }

  onDiscoverModalOk() {
    this.data.setDiscoverable(true);
    this.data.setDiscoverModalVisibility(false);
  }

  onDiscoverModalDontShowAgain() {
    this.onDiscoverModalOk();
    localStorage.setItem(DISCOVERABLE_MODAL_KEY, DISCOVERABLE_MODAL_KEY);
  }

  onDiscoverModalClose() {
    this.data.setDiscoverable(false);
    this.data.setDiscoverModalVisibility(false);
  }
  
  onUpdateHandler(e) {
    e.preventDefault();
    this.data.validate(null, true);

    if (this.ui.isCreatingPerforms || !isEmpty(toJS(this.data.validation))) return;

    this.ui.setIsCreatingPerforms(true);

    return this.data.update()
    .then(() => {
      this.data.clear();
      this.goToCreationDescriptionPage();
      return Promise.resolve();
    })
    .catch(err => {
      console.error(err);
      const message = get(err, 'res.text', ERRORS[FIELDS.SUMMARY]);
      this.data.addSummaryError(message);
      return Promise.reject();
    })
    .finally(() => this.ui.setIsCreatingPerforms(false));
  }
  
  onCancelHandler() {
    this.goToCreationDescriptionPage();
  }

  goToCreationDescriptionPage() {
    browserHistory.push(`/${this.data.type}/${this.data.id}`);
  }
  
  onImageUploadHandler(image) {
    this.data.setImage(image);
  
    return this.data.sendImage(this.data.id)
    .then(res => {
      this.data.setImageUrl(res.image);
      return this.data.imageUrl;
    })
    .catch(console.error);
  }

  onImageDeleteHandler() {
    return this.data.deleteCoverImage();
  }
  
  onNameChangeHandler(value) {
    this.data.setName(value);
    this.doValidation(FIELDS.NAME);
  }

  onNameBlurHandler(e) {
    if (!this.isNeedToValidateOnBlur(e)) return;
    this.doValidation(FIELDS.NAME, true);
  }
  
  onLocationChangeHandler(value) {
    this.data.setLocation(value);
    this.doValidation(FIELDS.LOCATION);
  }

  onLocationInputChangeHandler(e) {
    this.data.setLocation();
    this.data.setLocationInputValue(e.target.value);
  }
  
  onAllDayChangeHandler() {
    this.data.toggleIsAllDay();
  }
  
  onStartDateChangeHandler(value) {
    this.data.setStartDate(value);
    this.doValidation(FIELDS.START_DATE_TIME, true);
    this.doValidation(FIELDS.START_DATE, true);
  }
  
  onStartTimeChangeHandler(value) {
    this.data.setStartTime(value);
    this.doValidation(FIELDS.START_DATE_TIME, true);
    this.doValidation(FIELDS.START_TIME, true);
  }
  
  onEndDateChangeHandler(value) {
    this.data.setEndDate(value);
    this.doValidation(FIELDS.END_DATE_TIME, true);
    this.doValidation(FIELDS.END_DATE, true);
  }
  
  onEndTimeChangeHandler(value) {
    this.data.setEndTime(value);
    this.doValidation(FIELDS.END_DATE_TIME, true);
    this.doValidation(FIELDS.END_TIME, true);
  }
  
  onDescriptionChangeHandler(value) {
    this.data.setDescription(value);
  }
  
  onPermissionChangeHandler(value) {
    this.data.setSelectedPermission(value);
  }
  
  onDiscoverableToggleHandler() {
    this.data.toggleDiscoverable();
  }

  onCategoriesOpenHandler() {
    this.data.setCategoriesModalVisibility(true);
  }

  onCategoriesCloseHandler(selectedCategories) {
    this.data.setSelectedCategories(selectedCategories);
    this.data.setCategoriesModalVisibility(false);
  }
  
  onInvitedPeopleChangeHandler(value) {
    this.data.setInvitedPeople(value);
  }

  onValidatorMessageModalCloseHandler() {
    this.data.setValidatorMessageVisibility(false);
  }

  onAlertsOpenHandler() {
    this.data.setAlertsModalVisibility(true);
  }

  onAlertsCloseHandler(alerts) {
    this.data.setAlerts(alerts);
    this.data.setAlertsModalVisibility(false);
  }

  isNeedToValidateOnBlur(e) {
    return jq.wrap(e.relatedTarget).hasParents('.event-creating-container');
  }

  doValidation(field, isNeedToShowValidatorMessage) {
    if (field) this.data.clearValidation(field);
    this.data.validate(field, isNeedToShowValidatorMessage);
  }
  
  render() {
    if (!this.data.isLoaded) return <Loader />;

    const { type, creation, imageUrl, permissions, selectedPermission, startDate, startTime, endDate, endTime, name,
      isAllDay, discoverable, isDiscoverModalShown, description, isValidatorMessageVisible, selectedLocation,
      locationInputValue, isSchedule } = this.data;

    const errors = toJS(this.data.validation);
    const validationMessage = values(errors).map(err => <div key={err}>{err}</div>);
    const validationSummary = errors.summary;
    const isSummaryExists = !isEmpty(validationSummary);

    const categoriesModalData = {
      isOpen: this.data.isCategoriesModalVisible,
      selectedCategories: this.data.selectedCategories,
    };

    const categoriesModalHandlers = {
      onOpen: this.onCategoriesOpen,
      onClose: this.onCategoriesClose,
    };

    const alertsModalData = {
      isOpen: toJS(this.data.isAlertsModalVisible),
      selectedAlerts: toJS(this.data.alerts),
      boldLabel: true,
    };

    const alertsModalHandlers = {
      onOpen: this.onAlertsOpen,
      onClose: this.onAlertsClose,
    };
  
    const startDateTimeErrors = {
      date: errors[FIELDS.START_DATE],
      time: errors[FIELDS.START_TIME],
      both: errors[FIELDS.START_DATE_TIME]
    };
  
    const endDateTimeErrors = {
      date: errors[FIELDS.END_DATE],
      time: errors[FIELDS.END_TIME],
      both: errors[FIELDS.END_DATE_TIME]
    };

    const inviterHandlers = {
      onInvitedPeopleChange: this.onInvitedPeopleChange,
    };

    const buttonsValues = {
      isCreateButtonDisabled: this.ui.isCreatingPerforms,
    };

    const buttonsHandlers = {
      onUpdate: this.onUpdate,
      onCancel: this.onCancel,
    };
    const discoverableValues = {
      displayInDiscover: discoverable,
      isDiscoverModalShown,
      selectedPermission,
      modalButtons: this.discoverModalButtons,
    };

    return (
      <div>
        { this.ui.isCreatingPerforms && <Loader /> }
        <BackgroundImage isCurrentUser={true} imageUrl={imageUrl} onChange={this.onImageUpload} onDelete={this.onImageDelete} />
        <div className='creation-editing-container'>
          <Form>
            { isSummaryExists && <ValidationSummary errors={validationSummary} /> }
            <ValidatorMessage open={toJS(isValidatorMessageVisible)} text={validationMessage} onClose={this.onValidatorMessageModalClose} />
            <Grid>
              <Row>
                <Col className='event-updating-fields-wrapper' xsOffset={0} xs={12} smOffset={2} sm={8} mdOffset={3} md={6}>
                  <Name name={name} error={errors[FIELDS.NAME]} onChange={this.onNameChange} onBlur={this.onNameBlur} />
                  <Location values={{ placeholder: 'Location', value: locationInputValue, selected: selectedLocation }} handlers={{ onSelect: this.onLocationChange, onInputChange: this.onLocationInputChange }} />
                  { /*<Categories data={categoriesModalData} handlers={categoriesModalHandlers} />*/ }
                  <AllDay isAllDay={isAllDay} onAllDayChange={this.onAllDayChange} />
                  <StartDateTime startDate={startDate} startTime={startTime} timeShown={!isAllDay} errors={startDateTimeErrors} onStartDateChange={this.onStartDateChange} onStartTimeChange={this.onStartTimeChange} />
                  <EndDateTime isAllDay={isAllDay} endDate={endDate} endTime={endTime} errors={endDateTimeErrors} onEndDateChange={this.onEndDateChange} onEndTimeChange={this.onEndTimeChange} />
                  <Description value={description} onDescriptionChange={this.onDescriptionChange} />
                  <Alerts data={alertsModalData} handlers={alertsModalHandlers} />
                  <Permissions permissions={permissions} selectedPermission={selectedPermission} onPermissionChange={this.onPermissionChange} />
                  <Discoverable values={discoverableValues} handlers={this.discoverableHandlers} />
                  <Inviter item={toJS(creation)} type={type} handlers={inviterHandlers} />
                  {isSchedule && <Repeater isBlocked={true} />}
                  <Buttons values={buttonsValues} handlers={buttonsHandlers} />
                </Col>
              </Row>
            </Grid>
          </Form>
        </div>
      </div>
    );
  }
}

export default CreationEdit;
