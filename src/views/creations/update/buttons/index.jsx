import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class CreationEditButtons extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { values, handlers } = this.props;
    return (
      <FormGroup controlId='buttons' className='margin-top'>
        <input type='button' disabled={values.isCreateButtonDisabled} className='btn btn-create' value='Save' onClick={handlers.onUpdate} />
        <input type='button' disabled={values.isCancelButtonDisabled} className='btn btn-cancel' value='Cancel' onClick={handlers.onCancel} />
      </FormGroup>
    );
  }
}

CreationEditButtons.propTypes = {
  values: PropTypes.shape({
    isCreateButtonDisabled: PropTypes.bool,
    isCancelButtonDisabled: PropTypes.bool,
  }),
  handlers: PropTypes.shape({
    onUpdate: PropTypes.isRequired,
    onCancel: PropTypes.isRequired
  }).isRequired,
};

export default CreationEditButtons;
