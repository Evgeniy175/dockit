import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { FormGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';



@observer
class EventCreateButtons extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { values, handlers } = this.props;
    return (
      <FormGroup controlId='buttons' className='margin-top'>
        <input type='button' disabled={values.isCreateButtonDisabled} className='btn btn-create' value='Create' onClick={handlers.onCreate} />
        <input type='button' disabled={values.isCancelButtonDisabled} className='btn btn-cancel' value='Cancel' onClick={handlers.onCancel} />
      </FormGroup>
    );
  }
}

EventCreateButtons.propTypes = {
  values: PropTypes.shape({
    isCreateButtonDisabled: PropTypes.bool,
    isCancelButtonDisabled: PropTypes.bool,
  }),
  handlers: PropTypes.shape({
    onCreate: PropTypes.isRequired,
    onCancel: PropTypes.isRequired
  }).isRequired,
};

export default EventCreateButtons;
