import React, { Component } from 'react';
import { action, observable, computed, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import { get, isEmpty, debounce } from 'lodash';
import moment from 'moment-timezone';

import { FIELDS, ERRORS, PERMISSIONS, EVENT_FIELDS, SCHEDULE_FIELDS, DISCOVERABLE_MODAL_KEY } from '../constants';
import { INPUT_FORMATS, EVENTS_CONSTANTS } from '../../../../constants';

import CategoriesModel from '../../../../models/categories';
import InviteModel from '../../../../models/invites';
import EventModel from '../../../../models/events';
import ScheduleModel from '../../../../models/schedules';

import DataState from '../../../../utils/state/data';

import { getTimeZone, formatDateWithTimezone, formatDateWithTimezoneToUtc } from '../../../../utils/timezones';
import { isNameValid, isMomentValid, isEndAfterStartDateTime } from '../../../../utils/validation/creation';
import { values } from '../../../../utils/object';
import { initNotifications } from '../../../../utils/notifications';

const categoriesModel = new CategoriesModel();
const inviteModel = new InviteModel();
const eventModel = new EventModel();
const scheduleModel = new ScheduleModel();



class EventCreatingDataState extends DataState {
  permissions = PERMISSIONS;
  
  @observable name = '';
  @observable description = '';
  
  @observable image = null;
  
  @observable selectedPermission = {};
  @observable selectedLocation = {};
  locationInputValue = '';

  @observable isCategoriesModalVisible = false;
  @observable selectedCategories = [];

  @observable isAlertsModalVisible = false;
  @observable alerts = [];

  @observable invitedPeople = [];
  
  @observable validation = {};
  
  @observable isAllDay = false;

  @observable discoverable = false;
  @observable isDiscoverModalShown = false;
  
  @observable startDateTime = moment().add(1, 'hour').startOf('hour');
  @observable endDateTime = moment().add(2, 'hour').startOf('hour');
  
  @observable scheduleConfig = {};

  @observable isValidatorMessageVisible = false;
  validationMessage = null;
  
  @computed
  get startDate() {
    return this.startDateTime.format(INPUT_FORMATS.DATE);
  }
  set startDate(value) {
    this.startDateTime = this.getDate(this.startDateTime, value);
    this.endDateTime = moment(this.startDateTime).add(1, 'hours');
  }
  
  @computed
  get startTime() {
    return moment(this.startDateTime).format(INPUT_FORMATS.TIME);
  }
  set startTime(value) {
    this.startDateTime = this.getTime(this.startDateTime, value);
    this.endDateTime = moment(this.startDateTime).add(1, 'hours');
  }
  
  @computed
  get endDate() {
    return this.endDateTime.format(INPUT_FORMATS.DATE);
  }
  set endDate(value) {
    this.endDateTime = this.getDate(this.endDateTime, value);
  }
  
  @computed
  get endTime() {
    return moment(this.endDateTime).format(INPUT_FORMATS.TIME);
  }
  set endTime(value) {
    this.endDateTime = this.getTime(this.endDateTime, value);
  }
  
  @computed
  get isSchedule() {
    return !!(this.scheduleConfig && this.scheduleConfig.type);
  }

  getDate(date, value) {
    value = moment(value, INPUT_FORMATS.DATE);
    return moment(date).set({ year: value.get('year'), month: value.get('month'), date: value.get('date') });
  }

  getTime(dateTime, value) {
    value = moment(value, INPUT_FORMATS.TIME);
    return moment(dateTime).set({hour: value.get('hour'), minute: value.get('minute'), second: value.get('second')});
  }

  @computed
  get isNeverEnds() {
    return !this.scheduleConfig.endDate;
  }
  
  constructor(props = {}) {
    super(props);
  }
  
  validate(field, isNeedToShowValidatorMessage) {
    this.clearValidation();
  
    if (!isNameValid(this.name)) this.addError(FIELDS.NAME);
    if (!isMomentValid(this.startDateTime)) this.addError(FIELDS.START_DATE_TIME);
    if (!this.isAllDay && !isEndAfterStartDateTime(this.startDateTime, this.endDateTime)) this.addError(FIELDS.END_DATE_TIME);
    if (this.isSchedule) this.validateSchedule();

    if (!isNeedToShowValidatorMessage || (field && !this.validation[field])) return;

    this.validationMessage = this.validation[field] ? <div>{this.validation[field]}</div> : values(this.validation).map(err => <div key={err}>{err}</div>);
    this.setValidatorMessageVisibility(isNeedToShowValidatorMessage && values(this.validationMessage).length > 0);
  }

  validateSchedule() {
    const endDate = this.scheduleConfig.endDate ? moment(this.scheduleConfig.endDate, EVENTS_CONSTANTS.DATE_FORMAT) : null;
    const isEndDateValid = isMomentValid(endDate);

    if (!this.isNeverEnds && !isEndDateValid) this.addError(FIELDS.REPEAT_END_DATE_TIME);
    if (this.isAllDay && isEndDateValid && !this.isEndDateSameOrAfter(endDate, this.startDateTime)) this.addError(FIELDS.REPEAT_END_DATE_TIME);
    if (!this.isNeverEnds && isEndDateValid) {
      if (this.isAllDay && !this.isEndDateSameOrAfter(endDate, this.endDateTime)) this.addError(FIELDS.REPEAT_END_DATE_TIME);
      if (!this.isAllDay && !isEndAfterStartDateTime(this.endDateTime, endDate)) this.addError(FIELDS.REPEAT_END_DATE_TIME);
    }
    if (this.scheduleConfig.weekdays && !this.isWeekDaysValid()) this.addError(FIELDS.REPEAT_DAY);
  }

  isEndDateSameOrAfter(start, end) {
    if (!isMomentValid(start) || !isMomentValid(end)) return false;
    const endV = this.getDateValuesForCompare(start);
    const startV = this.getDateValuesForCompare(end);
    return endV.year >= startV.year && endV.month >= startV.month && endV.day >= startV.day;
  }

  getDateValuesForCompare(momentObj) {
    return {
      day: momentObj.date(),
      month: momentObj.month(),
      year: momentObj.year(),
    };
  }
  
  isWeekDaysValid() {
    return get(this.scheduleConfig, 'weekdays.length', 0) > 0;
  }
  
  create() {
    const placeId = get(this.selectedLocation, 'value');
    return getTimeZone(placeId)
    .then(tzData => {
      const timeZoneId = get(tzData, 'timeZoneId', Intl.DateTimeFormat().resolvedOptions().timeZone);
      return this.isSchedule
      ? this.createSchedule(timeZoneId)
      : this.createEvent(timeZoneId);
    });
  }
  
  createSchedule(timeZoneId) {
    const schedule = this.getNewSchedule(timeZoneId);
    return scheduleModel.createOne({ body: JSON.stringify(schedule) })
    .then(schedule => this.handleCreatedSchedule(schedule.json.id))
    .then(initNotifications);
  }
  
  getNewSchedule(timeZoneId) {
    const location = !isEmpty(this.selectedLocation)
    ? { place_id: get(this.selectedLocation, 'value') }
    : { title: this.locationInputValue };

    let schedule = {
      title: this.name,
      allDay: this.isAllDay,
      startTime: this.isAllDay ? formatDateWithTimezoneToUtc(this.startDateTime, EVENTS_CONSTANTS.DATE_FORMAT) : formatDateWithTimezone(this.startDateTime, timeZoneId, EVENTS_CONSTANTS.DATE_FORMAT),
      endTime: this.isAllDay ? formatDateWithTimezoneToUtc(this.endDateTime, EVENTS_CONSTANTS.DATE_FORMAT) : formatDateWithTimezone(this.endDateTime, timeZoneId, EVENTS_CONSTANTS.DATE_FORMAT),
      endDate: this.scheduleConfig.endDate,
      timezone: timeZoneId,
      permission: get(this.selectedPermission, 'value'),
      description: this.description,
      location,
      repeater: this.scheduleConfig,
      discoverable: get(this.selectedPermission, 'canToggleDiscoverable') && toJS(this.discoverable)
    };

    if (!get(location, 'title.length') && !get(location, 'place_id.length')) delete schedule.location;
    if (!schedule.endDate) delete schedule.endDate;
    if (!timeZoneId || timeZoneId.length === 0) schedule.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

    delete schedule.repeater.endDate;
    
    return schedule;
  }
  
  handleCreatedSchedule(scheduleId) {
    return Promise.all([
      this.sendScheduleImage(scheduleId),
      this.getScheduleNotificationsRequests(scheduleId),
      //this.getScheduleCategoriesRequests(scheduleId),
      this.getScheduleAlertsRequests(scheduleId),
    ]);
  }

  getScheduleAlertsRequests(scheduleId) {
    if (this.alerts.length === 0) return Promise.resolve();
    return scheduleModel.addAlerts(scheduleId, { body: this.getAlertsBody() });
  }

  getScheduleNotificationsRequests(scheduleId) {
    return this.invitedPeople.filter(item => item.isChecked)
    .map(item => inviteModel.sendScheduleInvite(scheduleId, item.id));
  }

  getScheduleCategoriesRequests(scheduleId) {
    if (this.selectedCategories.length === 0) return Promise.resolve();
    return categoriesModel.addToSchedule(scheduleId, toJS(this.selectedCategories).map(cat => cat.id));
  }
  
  sendScheduleImage(id) {
    if (!this.image || isEmpty(this.image)) return Promise.resolve();
    
    const formData = new FormData();
    formData.append('image', this.dataUriToBlob(this.image));
    const config = { body: formData };
    return scheduleModel.uploadImage(id, config);
  }
  
  dataUriToBlob(dataUri) {
    const binary = atob(dataUri.split(',')[1]);
    let array = [];
    
    for (let i = 0; i < binary.length; i++) array.push(binary.charCodeAt(i));
    
    return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
  }
  
  createEvent(timeZoneId) {
    const event = this.getNewEvent(timeZoneId);
    return eventModel.createOne({ body: JSON.stringify(event) })
    .then(event => this.handleCreatedEvent(event.id))
    .then(initNotifications);
  }
  
  getNewEvent(timeZoneId) {
    const user = Auth.getActiveUser();

    const location = !isEmpty(this.selectedLocation)
    ? { place_id: get(this.selectedLocation, 'value') }
    : { title: this.locationInputValue };
    
    let event = {
      title: this.name,
      allDay: this.isAllDay,
      startTime: this.isAllDay ? formatDateWithTimezoneToUtc(this.startDateTime, EVENTS_CONSTANTS.DATE_FORMAT) : formatDateWithTimezone(this.startDateTime, timeZoneId, EVENTS_CONSTANTS.DATE_FORMAT),
      endTime: this.isAllDay ? formatDateWithTimezoneToUtc(this.endDateTime, EVENTS_CONSTANTS.DATE_FORMAT) : formatDateWithTimezone(this.endDateTime, timeZoneId, EVENTS_CONSTANTS.DATE_FORMAT),
      user_id: user.id,
      permission: get(this.selectedPermission, 'value'),
      description: this.description,
      location,
      discoverable: get(this.selectedPermission, 'canToggleDiscoverable') && toJS(this.discoverable)
    };

    if (!get(location, 'title.length') && !get(location, 'place_id.length')) delete event.location;
    
    return event;
  }
  
  handleCreatedEvent(eventId) {
    return Promise.all([
      this.sendEventImage(eventId),
      this.getEventNotificationsRequests(eventId),
      //this.getEventCategoriesRequests(eventId),
      this.getEventAlertsRequests(eventId),
    ]);
  }
  
  sendEventImage(id) {
    if (!this.image || isEmpty(this.image)) return Promise.resolve();
  
    const formData = new FormData();
    formData.append('image', this.dataUriToBlob(this.image));
    const config = { body: formData };
    return eventModel.uploadImage(id, config);
  }

  getEventNotificationsRequests(eventId) {
    return this.invitedPeople.filter(item => item.isChecked)
    .map(item => inviteModel.sendEventInvite(eventId, item.id));
  }

  getEventCategoriesRequests(eventId) {
    if (this.selectedCategories.length === 0) return Promise.resolve();
    return categoriesModel.addToEvent(eventId, toJS(this.selectedCategories).map(cat => cat.id));
  }

  getEventAlertsRequests(eventId) {
    if (this.alerts.length === 0) return Promise.resolve();
    return eventModel.addAlerts(eventId, { body: this.getAlertsBody() });
  }

  getAlertsBody() {
    return JSON.stringify({ alerts: this.alerts });
  }

  @action
  addError(key) {
    this.validation[key] = ERRORS[key];
    this.validation = toJS(this.validation);
  }
  
  @action
  addSummaryError(value) {
    this.validation.summary = this.validation.summary || [];
    this.validation.summary.push(value);
    this.validation = toJS(this.validation);
  }
  
  @action
  clearValidation(key) {
    if (key) {
      delete this.validation[key];
      this.validation = toJS(this.validation);
      return;
    }
    
    this.validation = {};
  }
  
  @action
  clear() {
    this.isAllDay = false;
    this.discoverable = false;
    this.image = {};
    this.selectedLocation = {};
    this.validation = {};
    this.name = '';
    this.description = '';
    this.startDateTime = moment().add(1, 'hour').startOf('hour');
    this.endDateTime = moment().add(2, 'hour').startOf('hour');
    this.selectedPermission = {};
    this.scheduleConfig = {};
  }
  
  @action
  setImage(value) {
    this.image = value;
  }
  
  @action
  setSelectedPermission(value) {
    this.selectedPermission = value;
    this.setDiscoverable(false);
  }
  
  @action
  setDiscoverable(value) {
    this.discoverable = value;
  }

  @action
  toggleDiscoverable() {
    const expectedValue = !this.discoverable;
    if (!expectedValue || localStorage.getItem(DISCOVERABLE_MODAL_KEY)) return this.setDiscoverable(expectedValue);
    this.setDiscoverModalVisibility(expectedValue && !localStorage.getItem(DISCOVERABLE_MODAL_KEY));
  }

  @action
  setDiscoverModalVisibility(value) {
    this.isDiscoverModalShown = value;
  }
  
  @action
  toggleIsAllDay() {
    this.isAllDay = !this.isAllDay;
  }

  @action
  setCategoriesModalVisibility(value) {
    this.isCategoriesModalVisible = value;
  }

  @action
  setSelectedCategories(value) {
    this.selectedCategories = value;
  }

  @action
  setAlertsModalVisibility(value) {
    this.isAlertsModalVisible = value;
  }

  @action
  setAlerts(value) {
    this.alerts = value;
  }
  
  @action
  setLocation(value) {
    this.selectedLocation = value;
  }
  
  @action
  setName(value) {
    this.name = value;
  }
  
  @action
  setDescription(value) {
    this.description = value;
  }
  
  @action
  setStartDate(value) {
    this.startDate = value;
  }
  
  @action
  setStartTime(value) {
    this.startTime = value;
  }
  
  @action
  setEndDate(value) {
    this.endDate = value;
  }
  
  @action
  setEndTime(value) {
    this.endTime = value;
  }

  @action
  setInvitedPeople(value) {
    this.invitedPeople = value;
  }

  @action
  setScheduleConfig(value) {
    this.scheduleConfig = value;
  }

  @action
  setValidatorMessageVisibility(value) {
    this.isValidatorMessageVisible = value;
  }
}

export default EventCreatingDataState;
