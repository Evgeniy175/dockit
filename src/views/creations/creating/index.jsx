import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Form, Row, Col } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import { get, isEmpty } from 'lodash';
import moment from 'moment';

import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';
import { values } from '../../../utils/object';

import { FIELDS, ERRORS, DISCOVERABLE_MODAL_KEY } from './constants';
import { EVENTS_CONSTANTS, INPUT_FORMATS } from '../../../constants';
import { CSS_MAPPING } from '../../../shared-components/button/constants';

import Loader from '../../../shared-components/loader/index.jsx';
import Name from '../../../shared-components/creations/name/index.jsx';
import Location from '../../../shared-components/location-select/index.jsx';
import Categories from '../../../shared-components/creations/categories/index.jsx';
import AllDay from '../../../shared-components/creations/all-day/index.jsx';
import StartDateTime from '../../../shared-components/creations/start-date-time/index.jsx';
import EndDateTime from '../../../shared-components/creations/end-date-time/index.jsx';
import Description from '../../../shared-components/creations/description/index.jsx';
import Permissions from '../../../shared-components/creations/permissions/index.jsx';
import Discoverable from '../../../shared-components/creations/discoverable/index.jsx';
import Inviter from '../../../shared-components/creations/inviter/index.jsx';
import Repeater from '../../../shared-components/creations/repeater/index.jsx';
import Alerts from '../../../shared-components/alerts/index.jsx';
import Buttons from './buttons/index.jsx';

import ValidatorMessage from '../../../shared-components/validator-message/index.jsx';
import ValidationSummary from '../../../shared-components/validation-summary/index.jsx';
import BackgroundImage from '../../../shared-components/background-image/index.jsx';

import jq from '../../../utils/jquery';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class EventCreate extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
      ui: new UiState(),
    });
    this.data.setSelectedPermission(this.data.permissions[0]);
    setTitle(PAGE_TITLES[PAGES.CREATE_EVENT]());

    this.categoriesModalHandlers = {
      onOpen: ::this.onCategoriesOpenHandler,
      onClose: ::this.onCategoriesCloseHandler
    };
    this.alertsModalHandlers = {
      onOpen: ::this.onAlertsOpenHandler,
      onClose: ::this.onAlertsCloseHandler,
    };
    this.inviterHandlers = {
      onInvitedPeopleChange: ::this.onInvitedPeopleChangeHandler,
    };
    this.buttonsHandlers = {
      onCreate: ::this.onCreateHandler,
      onCancel: ::this.onCancelHandler,
    };
    this.discoverModalButtons = [
      {
        text: 'Okay',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: ::this.onDiscoverModalOk,
      },
      {
        text: `Don't show again`,
        className: CSS_MAPPING.BLUE,
        onClick: ::this.onDiscoverModalDontShowAgain,
      },
    ];
    this.discoverableHandlers = {
      onChange: ::this.onDiscoverableToggleHandler,
      onModalClose: ::this.onDiscoverModalClose,
    };

    this.onImageUpload = ::this.onImageUploadHandler;
    this.onImageDelete = ::this.onImageDeleteHandler;
    this.onNameChange = ::this.onNameChangeHandler;
    this.onNameBlur = ::this.onNameBlurHandler;
    this.onLocationChange = ::this.onLocationChangeHandler;
    this.onLocationInputChange = ::this.onLocationInputChangeHandler;
    this.onAllDayChange = ::this.onAllDayChangeHandler;
    this.onStartDateChange = ::this.onStartDateChangeHandler;
    this.onStartTimeChange = ::this.onStartTimeChangeHandler;
    this.onEndDateChange = ::this.onEndDateChangeHandler;
    this.onEndTimeChange = ::this.onEndTimeChangeHandler;
    this.onPermissionChange = ::this.onPermissionChangeHandler;
    this.onDescriptionChange = ::this.onDescriptionChangeHandler;
    this.onValidatorMessageModalClose = ::this.onValidatorMessageModalCloseHandler;
    this.onRepeatDataChanged = ::this.onRepeatDataChangedHandler;
  }

  componentWillUnmount() {
    this.data.clear();
  }

  onDiscoverModalOk() {
    this.data.setDiscoverable(true);
    this.data.setDiscoverModalVisibility(false);
  }

  onDiscoverModalDontShowAgain() {
    this.onDiscoverModalOk();
    localStorage.setItem(DISCOVERABLE_MODAL_KEY, DISCOVERABLE_MODAL_KEY);
  }

  onDiscoverModalClose() {
    this.data.setDiscoverable(false);
    this.data.setDiscoverModalVisibility(false);
  }

  onCreateHandler(e) {
    e.preventDefault();
    this.data.validate(null, true);
    
    if (this.ui.isCreatingPerforms || !isEmpty(toJS(this.data.validation))) return;

    this.ui.setIsCreatingPerforms(true);
    
    return this.data.create()
    .then(() => {
      browserHistory.push(EVENTS_CONSTANTS.AFTER_CREATING_ROUTE);
      return Promise.resolve();
    })
    .catch(err => {
      console.error(err);
      const message = get(err, 'res.text', ERRORS[FIELDS.SUMMARY]);
      this.data.addSummaryError(message);
      return Promise.reject();
    })
    .finally(() => this.ui.setIsCreatingPerforms(false));
  }
  
  onCancelHandler() {
    browserHistory.push('/feed');
  }
  
  onImageUploadHandler(image) {
    this.data.setImage(image);
    return Promise.resolve(image);
  }

  onImageDeleteHandler() {
    this.data.setImage();
    return Promise.resolve();
  }
  
  onNameChangeHandler(value) {
    this.data.setName(value);
    this.doValidation(FIELDS.NAME);
  }

  onNameBlurHandler(e) {
    if (!this.isNeedToValidateOnBlur(e)) return;
    this.doValidation(FIELDS.NAME, true);
  }

  onLocationChangeHandler(value) {
    this.data.setLocation(value);
    this.doValidation(FIELDS.LOCATION);
  }

  onLocationInputChangeHandler(e) {
    this.data.locationInputValue = e.target.value;
  }
  
  onCategoriesOpenHandler() {
    this.data.setCategoriesModalVisibility(true);
  }
  
  onCategoriesCloseHandler(selectedCategories) {
    this.data.setSelectedCategories(selectedCategories);
    this.data.setCategoriesModalVisibility(false);
  }
  
  onAllDayChangeHandler() {
    this.data.toggleIsAllDay();
  }
  
  onStartDateChangeHandler(value) {
    this.data.setStartDate(value);
    this.doValidation(FIELDS.START_DATE_TIME, true);
    this.doValidation(FIELDS.START_DATE, true);
  }
  
  onStartTimeChangeHandler(value) {
    this.data.setStartTime(value);
    this.doValidation(FIELDS.START_DATE_TIME, true);
    this.doValidation(FIELDS.START_TIME, true);
  }
  
  onEndDateChangeHandler(value) {
    this.data.setEndDate(value);
    this.doValidation(FIELDS.END_DATE_TIME, true);
    this.doValidation(FIELDS.END_DATE, true);
  }
  
  onEndTimeChangeHandler(value) {
    this.data.setEndTime(value);
    this.doValidation(FIELDS.END_DATE_TIME, true);
    this.doValidation(FIELDS.END_TIME, true);
  }
  
  onInvitedPeopleChangeHandler(value) {
    this.data.setInvitedPeople(value);
  }
  
  onPermissionChangeHandler(value) {
    this.data.setSelectedPermission(value);
  }
  
  onDiscoverableToggleHandler() {
    this.data.toggleDiscoverable();
  }
  
  onDescriptionChangeHandler(value) {
    this.data.setDescription(value);
  }
  
  onRepeatDataChangedHandler(value) {
    this.data.setScheduleConfig(value);
    this.doValidation(FIELDS.REPEAT_END_DATE_TIME, true);
    this.doValidation(FIELDS.REPEAT_DAY, true);
    this.doValidation(FIELDS.REPEAT_TYPE, true);
  }

  onValidatorMessageModalCloseHandler() {
    this.data.setValidatorMessageVisibility(false);
  }

  onAlertsOpenHandler() {
    this.data.setAlertsModalVisibility(true);
  }

  onAlertsCloseHandler(alerts) {
    this.data.setAlerts(alerts);
    this.data.setAlertsModalVisibility(false);
  }

  isNeedToValidateOnBlur(e) {
    return jq.wrap(e.relatedTarget).hasParents('.event-creating-container');
  }

  doValidation(field, isNeedToShowValidatorMessage) {
    if (field) this.data.clearValidation(field);
    this.data.validate(field, isNeedToShowValidatorMessage);
  }
  
  render() {
    const { permissions, selectedPermission, startDate, startTime, endDate, endTime, name, isAllDay,
      isCategoriesModalVisible, selectedCategories, discoverable, description, scheduleConfig, isAlertsModalVisible,
      alerts, validation, isDiscoverModalShown, image } = this.data;

    const validationMessage = values(validation).map(err => <div key={err}>{err}</div>);
    const validationSummary = validation.summary;
    const isSummaryExists = !isEmpty(validationSummary);

    const categoriesModalData = {
      isOpen: isCategoriesModalVisible,
      selectedCategories,
    };
    const alertsModalData = {
      isOpen: isAlertsModalVisible,
      selectedAlerts: toJS(alerts),
      boldLabel: true,
    };
    const startDateTimeErrors = {
      date: validation[FIELDS.START_DATE],
      time: validation[FIELDS.START_TIME],
      both: validation[FIELDS.START_DATE_TIME]
    };
    const endDateTimeErrors = {
      date: validation[FIELDS.END_DATE],
      time: validation[FIELDS.END_TIME],
      both: validation[FIELDS.END_DATE_TIME]
    };
    const repeatEndDateTimeErrors = {
      both: validation[FIELDS.REPEAT_END_DATE_TIME],
      type: validation[FIELDS.REPEAT_TYPE],
      day: validation[FIELDS.REPEAT_DAY]
    };
    const buttonsValues = {
      isCreateButtonDisabled: this.ui.isCreatingPerforms,
    };
    const discoverableValues = {
      displayInDiscover: discoverable,
      isDiscoverModalShown,
      selectedPermission,
      modalButtons: this.discoverModalButtons,
    };

    return (
      <div>
        { this.ui.isCreatingPerforms && <Loader /> }
        <BackgroundImage isCreating={true} imageUrl={image} isCurrentUser={true} onChange={this.onImageUpload} onDelete={this.onImageDelete} />
        <div className='event-creating-container'>
          <Form>
            { isSummaryExists && <ValidationSummary errors={validationSummary} /> }
            <ValidatorMessage open={this.data.isValidatorMessageVisible} text={validationMessage} onClose={this.onValidatorMessageModalClose} />
            <Grid>
              <Row>
                <Col className='event-creating-fields-wrapper' xsOffset={0} xs={12} smOffset={2} sm={8} mdOffset={3} md={6}>
                  <Name name={name} error={validation[FIELDS.NAME]} onChange={this.onNameChange} onBlur={this.onNameBlur} />
                  <Location values={{ placeholder: 'Location' }} handlers={{ onSelect: this.onLocationChange, onInputChange: this.onLocationInputChange }} />
                  { /*<Categories data={categoriesModalData} handlers={this.categoriesModalHandlers} />*/ }
                  <AllDay isAllDay={isAllDay} onAllDayChange={this.onAllDayChange} />
                  <StartDateTime timeShown={!isAllDay} startDate={startDate} startTime={startTime} errors={startDateTimeErrors} onStartDateChange={this.onStartDateChange} onStartTimeChange={this.onStartTimeChange} />
                  <EndDateTime isAllDay={isAllDay} endDate={endDate} endTime={endTime} errors={endDateTimeErrors} onEndDateChange={this.onEndDateChange} onEndTimeChange={this.onEndTimeChange} />
                  <Description value={description} onDescriptionChange={this.onDescriptionChange} />
                  <Alerts data={alertsModalData} handlers={this.alertsModalHandlers} />
                  <Permissions permissions={permissions} selectedPermission={selectedPermission} onPermissionChange={this.onPermissionChange} />
                  <Discoverable values={discoverableValues} handlers={this.discoverableHandlers} />
                  <Inviter handlers={this.inviterHandlers} />
                  <Repeater scheduleConfig={scheduleConfig} scheduleEnd={moment(endTime, INPUT_FORMATS.TIME)} errors={repeatEndDateTimeErrors} onRepeatDataChanged={this.onRepeatDataChanged} />
                  <Buttons values={buttonsValues} handlers={this.buttonsHandlers} />
                </Col>
              </Row>
            </Grid>
          </Form>
        </div>
      </div>
    );
  }
}

export default EventCreate;
