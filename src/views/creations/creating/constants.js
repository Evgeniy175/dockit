export const PEOPLE_FOR_INVITE_LIMIT = 25;
export const LOAD_BUFFER_FROM_BOTTOM = 20;

export const DISCOVERABLE_MODAL_KEY = 'is discoverable modal question shown';

export const EVENT_FIELDS = [
  'title',
  'allDay',
  'startTime',
  'endTime',
  'user_id',
  'permission',
  'discoverable',
  'description',
  'location',
];

export const SCHEDULE_FIELDS = [
  'title',
  'allDay',
  'startDate',
  'startTime',
  'endTime',
  'timezone',
  'permission',
  'discoverable',
  'description',
  'location',
];

export const FIELDS = {
  NAME: 'name',
  LOCATION: 'location',
  START_DATE_TIME: 'start date time',
  START_DATE: 'start date',
  START_TIME: 'start time',
  END_DATE_TIME: 'end date time',
  END_DATE: 'end date',
  END_TIME: 'end time',
  REPEAT_END_DATE_TIME: 'repeat end',
  REPEAT_DAY: 'repeat day',
  REPEAT_TYPE: 'repeat type',
  SUMMARY: 'summary'
};

export const ERRORS = {
  [FIELDS.NAME]: 'Please enter an event name',
  [FIELDS.LOCATION]: 'Select location',
  [FIELDS.START_DATE_TIME]: 'Select start date and time',
  [FIELDS.START_DATE]: 'Start date wrong',
  [FIELDS.START_TIME]: 'Start time wrong',
  [FIELDS.END_DATE_TIME]: 'The end time should be after the start time',
  [FIELDS.END_DATE]: 'End date wrong',
  [FIELDS.END_TIME]: 'End time wrong',
  [FIELDS.REPEAT_END_DATE_TIME]: 'Repeat end datetime wrong',
  [FIELDS.REPEAT_DAY]: 'Please select one or more days',
  [FIELDS.REPEAT_TYPE]: 'Please select repeat type',
  [FIELDS.SUMMARY]: 'Error'
};

export const PERMISSIONS = [
  {
    title: 'Private',
    value: 'private',
    canToggleDiscoverable: false
  }, {
    title: 'Followers',
    value: 'followers',
    canToggleDiscoverable: false
  }, {
    title: 'Public',
    value: 'global',
    canToggleDiscoverable: true,
    defaultDiscoverable: true
  },
];
