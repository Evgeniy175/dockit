import { MODEL_PLURAL as EVENT_MODEL_PLURAL } from '../../models/events/constants';
import { MODEL_PLURAL as SCHEDULE_MODEL_PLURAL } from '../../models/schedules/constants';

export const TYPES = {
  EVENT: EVENT_MODEL_PLURAL,
  SCHEDULE: SCHEDULE_MODEL_PLURAL,
};
