import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';

import Header from '../../../../shared-components/profile/settings-section-header/index.jsx';
import ViewChanger from '../../../../shared-components/view-changer/index.jsx';

@observer
class ProfileSettingsSupport extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Row className='support'>
        <Header text='Support' />
        <div className='item'>
          <ViewChanger label='Report a Problem' isBoldLabel={false} linkTo={null} />
        </div>
      </Row>
    );
  }
}

export default ProfileSettingsSupport;
