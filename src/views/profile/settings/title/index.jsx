import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class ProfileSettingsTitle extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return <div className='title'>{ this.props.text }</div>;
  }
}

ProfileSettingsTitle.propTypes = {
  text: PropTypes.string.isRequired
};

export default ProfileSettingsTitle;
