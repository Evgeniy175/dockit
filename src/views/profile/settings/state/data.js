import { action, observable, computed, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import Promise from 'bluebird';

import DataState from '../../../../utils/state/data';

import UserModel from '../../../../models/users';

import { initNotifications, setIsWebNotificationsDisabled } from '../../../../utils/notifications';

const userModel = new UserModel();



class UserProfileSettingsDataState extends DataState {
  @observable profile = {};
  isNotificationsDisabled;
  
  constructor(props = {}) {
    super(props);
  }
  
  fetchUserProfile(userId) {
    return userModel.fetchProfileById(userId)
    .then(res => {
      this.handleReceivedProfile(res.data);
      this.setProfile(res.data);
    });
  }
  
  handleReceivedProfile(profile) {
    delete profile.location_id;
  }
  
  saveProfile() {
    const config = {
      body: JSON.stringify(this.profile)
    };

    setIsWebNotificationsDisabled(this.isNotificationsDisabled);
  
    return userModel.removeLocation()
    .then(() => userModel.updateProfile(config))
    .then(user => {
      return Promise.all([
        Auth.setActiveUser(user),
        userModel.addAlerts({ body: JSON.stringify({ alerts: toJS(this.profile.defaultAlerts) }) }),
      ])
    })
    .then(initNotifications)
    .catch(console.error);
  }

  @action
  setAlerts(value) {
    this.profile.defaultAlerts = value;
    this.setProfile(toJS(this.profile));
  }
  
  @action
  setProfile(value) {
    this.profile = value;
  }
}

export default UserProfileSettingsDataState;
