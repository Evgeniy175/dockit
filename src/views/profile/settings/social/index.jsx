import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';
import { get } from 'lodash';
import deepAssign from 'deep-assign';
import PropTypes from 'prop-types';

import Header from '../../../../shared-components/profile/settings-section-header/index.jsx';
import ViewChanger from '../../../../shared-components/view-changer/index.jsx';

import Modal from '../../activity/modals/index.jsx';

import { TYPES } from './constants';



@observer
class ProfileSettingsSocial extends Component {
  @observable isModalsOpened = {
    [TYPES.FOLLOWERS]: false,
    [TYPES.FOLLOWINGS]: false,
  };

  constructor(props) {
    super(props);

    this.totalCount = {
      [TYPES.FOLLOWERS]: get(props, 'values.profile.follows.count.followers', 0),
      [TYPES.FOLLOWINGS]: get(props, 'values.profile.follows.count.following', 0),
    };

    this.items = [
      {
        label: 'Invite people to DockIt',
        isBoldLabel: false,
      },
      {
        label: 'Followers',
        isBoldLabel: false,
        onClick: ::this.onShowFollowersModalHandler,
      },
      {
        label: 'Following',
        isBoldLabel: false,
        onClick: ::this.onShowFollowingsModalHandler,
      },
    ];

    this.onCloseHandlers = {
      [TYPES.FOLLOWERS]: ::this.onCloseFollowersModalHandler,
      [TYPES.FOLLOWINGS]: ::this.onCloseFollowingsModalHandler,
    };

    this.userId = get(props, 'values.profile.id');
  }

  @action
  onShowFollowersModalHandler() {
    this.setModalsVisibility({ [TYPES.FOLLOWERS]: true });
  }

  @action
  onShowFollowingsModalHandler() {
    this.setModalsVisibility({ [TYPES.FOLLOWINGS]: true });
  }

  @action
  onCloseFollowersModalHandler() {
    this.setModalsVisibility({ [TYPES.FOLLOWERS]: false });
  }

  @action
  onCloseFollowingsModalHandler() {
    this.setModalsVisibility({ [TYPES.FOLLOWINGS]: false });
  }

  @action
  setModalsVisibility(value) {
    this.isModalsOpened = deepAssign(this.isModalsOpened, value);
  }

  render() {
    return (
      <Row className='social with-separators'>
        { Object.keys(TYPES).map(key => this.renderModal(TYPES[key])) }
        <Header text='Social' />
        { this.items.map(item => this.renderItem(item)) }
      </Row>
    );
  }

  renderModal(type) {
    return <Modal key={type} type={type} userId={this.userId} open={this.isModalsOpened[type]} handlers={{}} onClose={this.onCloseHandlers[type]} totalCount={this.totalCount} />;
  }

  renderItem({ label, isBoldLabel, linkTo, onClick }) {
    return (
      <div key={label} className='item'>
        <ViewChanger label={label} isBoldLabel={isBoldLabel} linkTo={linkTo} onClick={onClick} />
      </div>
    );
  }
}

ProfileSettingsSocial.propTypes = {
  values: PropTypes.shape({
    profile: PropTypes.object.isRequired,
  }).isRequired,
};

export default ProfileSettingsSocial;
