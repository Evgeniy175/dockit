import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';

import Header from '../../../../shared-components/profile/settings-section-header/index.jsx';
import ViewChanger from '../../../../shared-components/view-changer/index.jsx';



@observer
class ProfileSettingsAbout extends Component {
  constructor(props) {
    super(props);

    this.items = [
      {
        label: 'Privacy Policy',
        isBoldLabel: false,
        linkTo: '/privacy-policy',
      },
      {
        label: 'Terms',
        isBoldLabel: false,
        linkTo: '/terms',
      },
    ];
  }
  
  render() {
    return (
      <Row className='about with-separators'>
        <Header text='About'/>
        { this.items.map(item => this.renderItem(item)) }
      </Row>
    );
  }

  renderItem({ label, isBoldLabel, linkTo, onClick }) {
    return (
      <div key={label} className='item'>
        <ViewChanger label={label} isBoldLabel={isBoldLabel} linkTo={linkTo} onClick={onClick} />
      </div>
    );
  }
}

export default ProfileSettingsAbout;
