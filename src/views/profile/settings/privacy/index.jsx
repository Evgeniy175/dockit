import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Header from '../../../../shared-components/profile/settings-section-header/index.jsx';
import Toggle from '../../../../shared-components/toggle/index.jsx';
import TooltipText from '../../../../shared-components/tooltip-text/index.jsx';

@observer
class ProfileSettingsPrivacy extends Component {
  @observable profile = {};
  
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onPrivateProfileClick = ::this.onPrivateProfileClickHandler;
    this.onDisableUpcomingClick = ::this.onDisableUpcomingClickHandler;
    this.setProfile(this.props.profile);
  }
  
  onPrivateProfileClickHandler() {
    this.togglePrivate();
    this.props.onProfileChange(this.profile);
  }
  
  onDisableUpcomingClickHandler() {
    this.toggleDisableUpcoming();
    this.props.onProfileChange(this.profile);
  }
  
  @action
  setProfile(value) {
    this.profile = value;
  }
  
  @action
  togglePrivate() {
    this.profile.public = !this.profile.public;
  }
  
  @action
  toggleDisableUpcoming() {
    this.profile.isUpcomingDisabled = !this.profile.isUpcomingDisabled;
  }
  
  render() {
    const isPrivate = !this.profile.public;
    
    return (
      <Row className='privacy'>
        <Header text='Privacy' />
        <Toggle label='Private Profile' value={isPrivate} isBoldLabel={false} onClick={this.onPrivateProfileClick} />
        <TooltipText text='If you choose to have a private profile, only users you approve can view your full profile on DockIt.' />
      </Row>
    );
  }
}

ProfileSettingsPrivacy.propTypes = {
  profile: PropTypes.object.isRequired,
  onProfileChange: PropTypes.func.isRequired
};

export default ProfileSettingsPrivacy;
