import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class ProfileSettingsSave extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    this.props.onClick();
  }
  
  render() {
    return (
      <Row className='save'>
        <span className='save-btn' onClick={this.onClick}>Save</span>
      </Row>
    );
  }
}

ProfileSettingsSave.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default ProfileSettingsSave;
