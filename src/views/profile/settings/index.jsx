import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { Auth } from '../../../auth';
import { isEmpty } from 'lodash';

import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';
import { DEFAULT_PROFILE_ALERTS } from '../../../shared-components/alerts-modal/constants';

import Loader from '../../../shared-components/loader/index.jsx';

import Title from './title/index.jsx';
import MyAccount from './my-account/index.jsx';
import AdditionalServices from './additional-services/index.jsx';
import Social from './social/index.jsx';
import Privacy from './privacy/index.jsx';
import Support from './support/index.jsx';
import About from './about/index.jsx';
import Save from './save/index.jsx';
import Logout from './logout/index.jsx';
import DeleteAccount from './delete-account/index.jsx';

import DataState from './state/data';



@observer
class UserProfileSettings extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.SETTINGS]());

    this.onProfileChange = ::this.onProfileChangeHandler;
    this.onSave = ::this.onSaveHandler;
    this.onAlertsClose = ::this.onAlertsCloseHandler;
    this.onNotificationsVisibilityChange = ::this.onNotificationsVisibilityChangeHandler;
  }
  
  componentWillMount() {
    const userId = Auth.getActiveUser().id;
    this.data.fetchUserProfile(userId)
    .catch(console.error);
  }
  
  onProfileChangeHandler(value) {
    this.data.setProfile(value);
  }
  
  onSaveHandler() {
    const userId = Auth.getActiveUser().id;
    
    this.data.saveProfile()
    .then(() => {
      this.data.setProfile({});
      browserHistory.push(`/users/${userId}`);
      return this.data.fetchUserProfile(userId);
    })
    .catch(console.error);
  }

  onAlertsCloseHandler(alerts) {
    this.data.setAlerts(alerts);
  }

  onNotificationsVisibilityChangeHandler(value) {
    this.data.isNotificationsDisabled = value;
  }
  
  render() {
    const profile = this.data.profile;
    const isProfileLoaded = !isEmpty(profile);
    
    if (!isProfileLoaded) return <Loader />;

    const additionalServicesHandlers = {
      onNotificationsVisibilityChange: this.onNotificationsVisibilityChange,
      onClose: this.onAlertsClose,
    };

    return (
      <Grid className='profile-settings'>
        <Row>
          <Col xs={12} smOffset={2} sm={8} mdOffset={3} md={6}>
            <Title text='Settings' />
            <MyAccount profile={profile} onProfileChange={this.onProfileChange} />
            <AdditionalServices data={this.getAdditionalServicesData()} handlers={additionalServicesHandlers} />
            <Social values={{ profile }} />
            <Privacy profile={profile} onProfileChange={this.onProfileChange} />
            <Support />
            <About />
            <Save onClick={this.onSave} />
            <Logout />
            <DeleteAccount />
          </Col>
        </Row>
      </Grid>
    );
  }

  getAdditionalServicesData() {
    const profile = this.data.profile;
    return {
      alerts: toJS(profile.defaultAlerts && profile.defaultAlerts.length > 0 ? profile.defaultAlerts : DEFAULT_PROFILE_ALERTS),
    };
  }
}

export default UserProfileSettings;
