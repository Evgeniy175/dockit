import { action, observable, toJS } from 'mobx';

import DataState from '../../../../../../utils/state/data';



class ProfileSettingsProfileImageDataState extends DataState {
  @observable isCropVisible = false;
  @observable isImageUploadingPerforms = true;
  
  @observable image = '';
  @observable croppedImage = {};
  
  cropper = {};
  
  constructor(config = {}) {
    super(config);
  }
  
  initCanvas(canvas, url) {
    this.loadImage(url, (err, image) => {
      if (err) {
        console.error(err);
        return;
      }
    
      this.drawImage(canvas, image);
      this.setIsImageUploadingPerforms(false);
    });
  }
  
  drawImage(canvas, img) {
    const context = canvas.getContext('2d');
    canvas.width = img.width;
    canvas.height = img.height;
    context.drawImage(img, 0, 0);
  }
  
  loadImage(src, callback) {
    const img = new Image;
    img.onload = () => { callback(null, img); };
    img.src = src;
  }
  
  @action
  setCropperHandler(value) {
    this.cropper = value;
  }
  
  @action
  setCropVisibility(value) {
    this.isCropVisible = value;
  }
  
  @action
  setIsImageUploadingPerforms(value) {
    this.isImageUploadingPerforms = value;
  }
  
  @action
  setImage(value) {
    this.image = value;
  }
  
  @action
  setCroppedImage(value) {
    this.croppedImage = value;
  }
  
  @action
  clear() {
    this.isCropVisible = false;
    this.isImageUploadingPerforms = true;
    this.image = '';
    this.croppedImage = {};
    this.cropper = {};
  }
}

export default ProfileSettingsProfileImageDataState;
