import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import ImageCropper from '../../../../../shared-components/image-crop/index.jsx';
import Loader from '../../../../../shared-components/loader/index.jsx';

import UserModel from '../../../../../models/users';

import DEFAULT_PROFILE_PICTURE from '../../../../../assets/images/default-images/profile-picture-large.png';

import DataState from './state/data';

import { HORIZONTAL_RATIO, VERTICAL_RATIO } from './constants';



@observer
class ProfileSettingsProfileImage extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
  }
  
  componentWillMount() {
    this.onChange = ::this.onChangeHandler;
    this.onImageClick = ::this.onImageClickHandler;
    this.onCrop = ::this.onCropHandler;
    this.onImageSelectCancel = ::this.onImageSelectCancelHandler;
    this.setCropper = ::this.onCropSet;
  }
  
  componentDidMount() {
    const canvas = this.getTitleImageCanvas();
    const url = this.props.imageUrl ? UserModel.formatImageUrl(this.props.imageUrl) : DEFAULT_PROFILE_PICTURE;
    this.data.initCanvas(canvas, url);
  }
  
  componentWillUnmount() {
    this.data.clear();
  }
  
  onCropSet(value) {
    this.data.setCropperHandler(value);
  }
  
  onChangeHandler(e) {
    let files;
    
    e.preventDefault();
    
    if (e.dataTransfer) files = e.dataTransfer.files;
    else if (e.target) files = e.target.files;
    
    if (!files || !files[0]) return;
    
    const reader = new FileReader();
    
    reader.onload = () => { this.data.setImage(reader.result); };
    reader.readAsDataURL(files[0]);
    
    this.data.setCropVisibility(true);
  }
  
  onImageClickHandler() {
    if (this.data.isCropVisible) return;
    
    this.data.cropper = {};
    
    const uploadInput = document.getElementById('upload-input');
    uploadInput.value = null;
    uploadInput.click();
  }
  
  onCropHandler() {
    const canvas = this.getTitleImageCanvas();
    
    this.data.setIsImageUploadingPerforms(true);
    
    const croppedImage = this.data.cropper.getCroppedCanvas().toDataURL('image/jpeg');
    this.data.setCroppedImage(croppedImage);
    
    this.props.onClick(this.data.croppedImage)
    .then(res => {
      const url = UserModel.formatImageUrl(res.profilePicture);
      this.data.initCanvas(canvas, url);
    })
    .catch(err => { console.error(err); this.data.setIsImageUploadingPerforms(false); });
    
    this.data.setCropVisibility(false);
  }
  
  onImageSelectCancelHandler() {
    this.data.setCropVisibility(false);
  }
  
  getTitleImageCanvas() {
    return document.getElementById('profile-image');
  }
  
  render() {
    const isImageUploadingPerforms = this.data.isImageUploadingPerforms;
    const isCropperVisible = this.data.isCropVisible;
    const cropperImage = toJS(this.data.image);
    
    return (
      <Row className='profile-image-upload'>
        <Col xs={12} className='no-margin no-padding'>
          <input id='upload-input' type='file' onChange={this.onChange} />
          { isImageUploadingPerforms && <Loader /> }
          <canvas id='profile-image' onClick={this.onImageClick} />
        </Col>
        <ImageCropper setCropper={this.setCropper}
                      isVisible={isCropperVisible}
                      horizontalRatio={HORIZONTAL_RATIO}
                      verticalRatio={VERTICAL_RATIO}
                      onCropFinish={this.onCrop}
                      onCancel={this.onImageSelectCancel}
                      src={cropperImage} />
      </Row>
    );
  }
}

ProfileSettingsProfileImage.propTypes = {
  imageUrl: PropTypes.string,
  onClick: PropTypes.func.isRequired
};

export default ProfileSettingsProfileImage;
