import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';
import { get, isEmpty } from 'lodash';
import PropTypes from 'prop-types';

import PasswordChange from './password-change/index.jsx';

import Header from '../../../../shared-components/profile/settings-section-header/index.jsx';
import Input from '../../../../shared-components/input/index.jsx';
import Select from '../../../../shared-components/select/index.jsx';
import Textarea from '../../../../shared-components/textarea/index.jsx';
import ProfileImage from './profile-image/index.jsx';
import ChangePassword from './change-password/index.jsx';

import { imageToFormData } from '../../../../utils/formatting/image';

import UserModel from '../../../../models/users';

import DataState from './state/data';

const userModel = new UserModel();



@observer
class ProfileSettingsMyAccount extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props)
    });
  }
  
  componentWillMount() {
    this.data.setProfile(this.props.profile);
    
    this.onNameChange = ::this.onNameChangeHandler;
    this.onUsernameChange = ::this.onUsernameChangeHandler;
    this.onEmailChange = ::this.onEmailChangeHandler;
    this.onLocationChange = ::this.onLocationChangeHandler;
    this.onLocationInputChange = ::this.onLocationInputChangeHandler;
    this.onDescriptionChange = ::this.onDescriptionChangeHandler;
    this.onBirthdayChange = ::this.onBirthdayChangeHandler;
    this.onProfileImageUpload = ::this.onBackgroundImageUploadHandler;
  }
  
  onNameChangeHandler(e) {
    this.data.setName(e.target.value);
    this.props.onProfileChange(this.data.profile);
  }
  
  onUsernameChangeHandler(e) {
    this.data.setUsername(e.target.value);
    this.props.onProfileChange(this.data.profile);
  }
  
  onEmailChangeHandler(e) {
    this.data.setEmail(e.target.value);
    this.props.onProfileChange(this.data.profile);
  }
  
  onLocationChangeHandler(value) {
    this.data.setSelectedLocation(value);
    this.data.locationInputValue = '';
    this.props.onProfileChange(this.data.profile);
  }
  
  onLocationInputChangeHandler(value) {
    this.data.handleLocationInput(value);
  }
  
  onDescriptionChangeHandler(e) {
    this.data.setDescription(e.target.value);
    this.props.onProfileChange(this.data.profile);
  }
  
  onBirthdayChangeHandler(e) {
    this.data.setBirthday(e.target.value);
    this.props.onProfileChange(this.data.profile);
  }
  
  onBackgroundImageUploadHandler(image) {
    const formData = imageToFormData(image);
    const config = { body: formData };
    
    return userModel.updateProfilePhoto(config)
    .catch(console.error);
  }
  
  render() {
    const profile = get(this.data, 'profile', {});
    const name = get(profile, 'name', '');
    const username = get(profile, 'username', '');
    const email = get(profile, 'email', '');
    const profilePicture = profile.profilePicture;

    const locations = toJS(this.data.locations);
    const selectedLocation = toJS(this.data.selectedLocation);
    const location = isEmpty(selectedLocation) ? { label: this.data.locationInputValue } : selectedLocation;
    
    const description = get(profile, 'description', '');
    const birthday = get(profile, 'birthday', '');

    const textAreaValues = {
      id: 'about-my-account',
      value: description,
      placeholder: 'About',
    };
    const textAreaHandlers = {
      onChange: this.onDescriptionChange,
    };
    
    return (
      <Row className='my-account with-separators'>
        <Header text='My Account' />
        <ProfileImage onClick={this.onProfileImageUpload} imageUrl={profilePicture} />
        <div className='item'>
          <Input value={name} changeHandler={this.onNameChange} placeholder='Your real name' />
        </div>
        <div className='item'>
          <Input value={username} changeHandler={this.onUsernameChange} placeholder='Username' />
        </div>
        <div className='item'>
          <Input value={email} changeHandler={this.onEmailChange} placeholder='Email' />
        </div>
        {
          /*
            // by evgen: not implemented on server for 2017-08-25
            <div className='item'>
              <PasswordChange />
            </div>
          */
        }
        <div className='item'>
          <ChangePassword />
        </div>
        <div className='item'>
          <Select value={location}
                  values={locations}
                  placeholder='Location'
                  changeHandler={this.onLocationChange}
                  inputChangeHandler={this.onLocationInputChange} />
        </div>
        <div className='item'>
          <Textarea values={textAreaValues} handlers={textAreaHandlers} />
        </div>
        <div className='item'>
          <Input value={birthday} changeHandler={this.onBirthdayChange} placeholder='Birthday' />
        </div>
      </Row>
    );
  }
}

ProfileSettingsMyAccount.propTypes = {
  profile: PropTypes.object.isRequired,
  onProfileChange: PropTypes.func.isRequired
};

export default ProfileSettingsMyAccount;
