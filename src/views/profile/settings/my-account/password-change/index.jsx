import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';

import Button from '../../../../../shared-components/button/index.jsx';
import PasswordEditingBlock from './editing-block/index.jsx';

import { CSS_MAPPING } from '../../../../../shared-components/button/constants';



@observer
class PasswordChange extends Component {
  @observable isExpanded = false;
  
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onButtonClick = ::this.onButtonClickHandler;
    this.onSave = ::this.onSaveHandler;
  }
  
  onButtonClickHandler() {
    this.toggleExpand();
  }
  
  onSaveHandler() {
    this.toggleExpand();
  }
  
  @action
  toggleExpand() {
    this.isExpanded = !this.isExpanded;
  }
  
  render() {
    return (
      <Grid fluid={true}>
        <Row className='password-change'>
          <Col>
            <Button text='Change password' onClick={this.onButtonClick} className={CSS_MAPPING.TOGGLE} />
            {
              this.isExpanded && <PasswordEditingBlock onSave={this.onSave} />
            }
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default PasswordChange;
