import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Input from '../../../../../../shared-components/input/index.jsx';
import Button from '../../../../../../shared-components/button/index.jsx';

import { CSS_MAPPING } from '../../../../../../shared-components/button/constants';

import DataState from './state/data';



@observer
class PasswordEditingBlock extends Component {
  @observable isExpanded = false;
  
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
  }
  
  componentWillMount() {
    this.onPasswordChange = ::this.onPasswordChangeHandler;
    this.onConfirmPasswordChange = ::this.onConfirmPasswordChangeHandler;
    this.onButtonClick = ::this.onButtonClickHandler;
    this.onSave = ::this.onSaveHandler;
  }
  
  onPasswordChangeHandler(e) {
    this.data.setPassword(e.target.value);
  }
  
  onConfirmPasswordChangeHandler(e) {
    this.data.setConfirmPassword(e.target.value);
  }
  
  onButtonClickHandler() {
    this.toggleExpand();
  }
  
  onSaveHandler() {
    const password = this.data.password;
    const confirmPassword = this.data.confirmPassword;
    
    // todo (Evgeniy): send request and call this.props.onSave();
  }
  
  @action
  toggleExpand() {
    this.isExpanded = !this.isExpanded;
  }
  
  render() {
    const password = this.data.password;
    const confirmPassword = this.data.confirmPassword;
    const className = `save ${CSS_MAPPING.BLUE_FILLED}`;
    
    return (
      <Grid fluid={true}>
        <Row>
          <Col>
            <Input type='password' value={password} placeholder='Password' changeHandler={ this.onPasswordChange } />
            <Input type='password' value={confirmPassword} placeholder='Confirm password' changeHandler={ this.onConfirmPasswordChange } />
            <Button text='Save' onClick={this.onSave} className={className} />
          </Col>
        </Row>
      </Grid>
    );
  }
}

PasswordEditingBlock.propTypes = {
  onSave: PropTypes.func.isRequired
};

export default PasswordEditingBlock;
