import {action, observable, computed, extendObservable} from 'mobx';

import DataState from '../../../../../../../utils/state/data';



class PasswordEditingBlockDataState extends DataState {
  @observable password = '';
  @observable confirmPassword = '';
  
  constructor(props = {}) {
    super(props);
  }
  
  @action
  setPassword(value) {
    this.password = value;
  }
  
  @action
  setConfirmPassword(value) {
    this.confirmPassword = value;
  }
}

export default PasswordEditingBlockDataState;
