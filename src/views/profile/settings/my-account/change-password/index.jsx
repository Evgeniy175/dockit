import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import { Grid, Row, Col } from 'react-bootstrap';

import Button from '../../../../../shared-components/button/index.jsx';

import { CSS_MAPPING }  from '../../../../../shared-components/button/constants';



@observer
class ProfileSettingsChangePassword extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    browserHistory.push(`/users/forgot-password`);
  }
  
  render() {
    return (
      <Grid fluid={true}>
        <Row className='change-password'>
          <Col>
            <Button text='Change password' onClick={this.onClick} className={CSS_MAPPING.TOGGLE} />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default ProfileSettingsChangePassword;
