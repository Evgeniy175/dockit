import { action, observable, computed, extendObservable } from 'mobx';
import { debounce, isEmpty, get } from 'lodash';

import { LOCATION_LOAD_DELAY } from '../constants';

import DataState from '../../../../../utils/state/data';
import LocationModel from '../../../../../models/locations';

const locationModel = new LocationModel();



class ProfileSettingsMyAccountDataState extends DataState {
  @observable profile = {};
  
  @observable selectedLocation = {};
  @observable locations = [];
  locationInputValue;
  
  constructor(props = {}) {
    super(props);
    this.locationFetcher = ::this.fetchLocations;
    this.init(props);
  }

  init(props) {
    this.setProfile(props.profile);

    const location = this.profile.location || this.profile.locationRaw || {};
    const locationTitle = location.title;
    const placeId = location.place_id;
    const selectedLocation = locationTitle ? { label: locationTitle, value: placeId } : {};

    this.setSelectedLocation(selectedLocation);
    this.locationInputValue = locationTitle;
  }
  
  handleLocationInput(value) {
    const locationFetchInvoker = debounce(this.locationFetcher, LOCATION_LOAD_DELAY, {
      leading: false,
      trailing: true
    });
    locationFetchInvoker(value);
    this.locationInputValue = value;
    this.setSelectedLocation({});
  }
  
  fetchLocations(text) {
    return locationModel.getPlaceAutocomplete(text)
    .then(results => results.map(result => ({ label: result.description, value: result.place_id })))
    .then(locations => this.setLocations(locations));
  }
  
  @action
  setProfile(value) {
    this.profile = value;
  }
  
  @action
  setLocations(value) {
    this.locations = value;
  }
  
  @action
  setSelectedLocation(value) {
    this.selectedLocation = value;

    if (!value) {
      this.profile.location = null;
      return;
    }
    
    const location = isEmpty(this.selectedLocation)
    ? { title: this.locationInputValue }
    : { place_id: get(this.selectedLocation, 'value') };
    
    if (this.profile.location) {
      this.profile.location = location;
      return;
    }
  
    extendObservable(this.profile, { location });
  }
  
  @action
  setName(value) {
    this.profile.name = value;
  }
  
  @action
  setUsername(value) {
    this.profile.username = value;
  }
  
  @action
  setEmail(value) {
    this.profile.email = value.toLowerCase();
  }
  
  @action
  setDescription(value) {
    this.profile.description = value;
  }
  
  @action
  setBirthday(value) {
    this.profile.birthday = value;
  }
}

export default ProfileSettingsMyAccountDataState;
