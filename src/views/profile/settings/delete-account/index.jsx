import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';

import Modal from '../../../../shared-components/question-modal/index.jsx';
import { CSS_MAPPING } from '../../../../shared-components/button/constants';

import UserModel from '../../../../models/users/index.js';

const userModel = new UserModel();



@observer
class ProfileSettingsDeleteAccount extends Component {
  @observable isModalShown = false;
  @observable isSecondModalShown = false;

  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
    this.onDelete = ::this.onDeleteHandler;
    this.onCancel = ::this.onCancelHandler;
    this.onSecondModalDelete = ::this.onSecondModalDeleteHandler;
    this.onSecondModalCancel = ::this.onSecondModalCancelHandler;
  }

  onClickHandler() {
    this.setIsModalOpen(true);
  }

  onDeleteHandler() {
    this.setIsSecondModalOpen(true);
    this.setIsModalOpen(false);
  }

  onCancelHandler() {
    this.setIsModalOpen(false);
  }

  @action
  setIsModalOpen(value) {
    this.isModalShown = value;
  }

  onSecondModalDeleteHandler() {
    userModel.deleteUser();
    this.setIsSecondModalOpen(false);
  }

  onSecondModalCancelHandler() {
    this.setIsSecondModalOpen(false);
  }

  @action
  setIsSecondModalOpen(value) {
    this.isSecondModalShown = value;
  }
  
  render() {
    const isModalShown = toJS(this.isModalShown);
    const title = 'Are you sure?';
    const text = 'Delete your account?';

    const modalButtons = [
      {
        text: 'Delete',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: this.onDelete
      },
      {
        text: 'Cancel',
        className: CSS_MAPPING.GRAY,
        onClick: this.onCancel
      },
    ];

    const isSecondModalShown = toJS(this.isSecondModalShown);
    const secondModalTitle = 'Are you sure?';
    const secondModalText = 'This will PERMANENTLY DELETE your profile and everything you have created.';

    const secondModalButtons = [
      {
        text: 'Cancel',
        className: CSS_MAPPING.GRAY,
        onClick: this.onSecondModalCancel
      },
      {
        text: 'Delete',
        className: CSS_MAPPING.RED_FILLED,
        onClick: this.onSecondModalDelete
      },
    ];

    return (
      <div className='delete-account'>
        <span className='delete-btn' onClick={this.onClick}>Delete account</span>
        <Modal open={isModalShown} title={title} text={text} onClose={this.onCancel} buttons={modalButtons} />
        <Modal open={isSecondModalShown} title={secondModalTitle} text={secondModalText} onClose={this.onCancel} buttons={secondModalButtons} />
      </div>
    );
  }
}

export default ProfileSettingsDeleteAccount;
