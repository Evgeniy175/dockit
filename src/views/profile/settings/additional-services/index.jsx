import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Header from '../../../../shared-components/profile/settings-section-header/index.jsx';
import Toggle from '../../../../shared-components/toggle/index.jsx';
import ViewChanger from '../../../../shared-components/view-changer/index.jsx';
import Alerts from '../../../../shared-components/alerts/index.jsx';

import { isWebNotificationsDisabled, setIsWebNotificationsDisabled } from '../../../../utils/notifications';



@observer
class ProfileSettingsAdditionalServices extends Component {
  @observable isWebNotificationsDisabled = false;
  @observable isAlertsModalVisible = false;
  @observable alerts = [];

  constructor(props) {
    super(props);
    this.onWebNotificationsToggle = ::this.onWebNotificationsToggleHandler;
    this.onAlertsOpen = ::this.onAlertsOpenHandler;
    this.onAlertsClose = ::this.onAlertsCloseHandler;
    this.setIsWebNotificationsDisabled(isWebNotificationsDisabled());
    this.setAlerts(props.data.alerts);
  }

  onWebNotificationsToggleHandler() {
    this.setIsWebNotificationsDisabled(!this.isWebNotificationsDisabled);
    const handler = this.props.handlers.onNotificationsVisibilityChange;
    if (handler) handler(this.isWebNotificationsDisabled);
  }

  onAlertsOpenHandler() {
    this.setAlertsModalVisibility(true);
  }

  onAlertsCloseHandler(alerts) {
    this.setAlerts(alerts);
    this.setAlertsModalVisibility(false);
    this.props.handlers.onClose(alerts);
  }

  @action
  setIsWebNotificationsDisabled(value) {
    this.isWebNotificationsDisabled = value;
  }

  @action
  setAlertsModalVisibility(value) {
    this.isAlertsModalVisible = value;
  }

  @action
  setAlerts(value) {
    this.alerts = value;
  }
  
  render() {
    const alertsModalData = {
      isOpen: toJS(this.isAlertsModalVisible),
      selectedAlerts: toJS(this.alerts),
    };

    const alertsModalHandlers = {
      onOpen: this.onAlertsOpen,
      onClose: this.onAlertsClose,
    };

    return (
      <Row className='additional-services with-separators'>
        <Header text='Additional Services'/>
        {
          /*
           <div className='item'>
           <Toggle label='Enable location finder' value={false} onClick={() => {}} isBoldLabel={false} />
           </div>
           */
        }
        <div className='item'>
          <Toggle label='Web Notifications' value={!this.isWebNotificationsDisabled} onClick={this.onWebNotificationsToggle} isBoldLabel={false} />
        </div>
        {
          /*
          Commented due to this is local device settings. Push notifications could be used only with mobile/tablet.
          <div className='item'>
            <Toggle label='Push Notifications' value={false} onClick={() => {}} isBoldLabel={false} />
          </div>
          */
        }
        {
          /*
           <div className='item'>
           <ViewChanger label='Link to additional calendars' isBoldLabel={false} linkTo='/link-calendars' />
           </div>
           */
        }
         <div className='item'>
           <Alerts data={alertsModalData} handlers={alertsModalHandlers} />
         </div>
      </Row>
    );
  }
}

ProfileSettingsAdditionalServices.propTypes = {
  data: PropTypes.shape({
    alerts: PropTypes.array.isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onNotificationsVisibilityChange: PropTypes.func,
    onClose: PropTypes.func.isRequired,
  }).isRequired,
};

export default ProfileSettingsAdditionalServices;
