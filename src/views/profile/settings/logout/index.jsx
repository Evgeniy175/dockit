import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';

import { Auth } from '../../../../auth';



@observer
class ProfileSettingsLogout extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler() {
    Auth.logout();
  }
  
  render() {
    return (
      <div className='logout'>
        <span className='logout-btn' onClick={this.onClick}>Log Out</span>
      </div>
    );
  }
}

export default ProfileSettingsLogout;
