import { action, observable, computed } from 'mobx';
import Promise from 'bluebird';

import DataState from '../../../utils/state/data';

import UserModel from '../../../models/users';
import EventModel from '../../../models/events';
import ScheduleModel from '../../../models/schedules';

import { Auth } from '../../../auth';

import { imageToFormData } from '../../../utils/formatting/image';

import { TYPES as CREATION_TYPES } from '../../creations/constants';

const userModel = new UserModel();
const eventModel = new EventModel();
const scheduleModel = new ScheduleModel();



class ProfileDataState extends DataState {
  PUBLIC_CREATIONS_DELETE_HANDLERS = {
    [CREATION_TYPES.EVENT]: () => this.setCreatedEventsCount(this.createdEventsCount - 1),
    [CREATION_TYPES.SCHEDULE]: () => this.setCreatedSchedulesCount(this.createdSchedulesCount - 1),
  };

  @observable data = {
    id: '',
    isNeedToUpdateUpcoming: false,
    isCurrent: false,
    profile: {},
    meta: {},
    backgroundImage: {},
    profileImageUrl: '',
  };

  @observable counters = {
    followers: 0,
    followings: 0,
    createdEvents: 0,
    createdSchedules: 0,
  };

  get isNeedToUpdateUpcoming() {
    return this.data && this.data.isNeedToUpdateUpcoming;
  }

  get isSigned() {
    return Auth.isSigned();
  }

  @action
  setIsCurrent(value) {
    this.data.isCurrent = value;
  }
  get isCurrent() {
    return this.data && this.data.isCurrent;
  }

  @action
  setProfile(value) {
    this.data.profile = value;
  }
  get profile() {
    return this.data && this.data.profile;
  }

  @action
  setMeta(value) {
    this.data.meta = value;
  }
  get meta() {
    return this.data && this.data.meta;
  }

  @action
  setId(value) {
    this.data.id = value;
  }
  get id() {
    return this.data && this.data.id;
  }

  @action
  setBackgroundImage(value) {
    this.data.backgroundImage = value;
  }
  get backgroundImage() {
    return this.data && this.data.backgroundImage;
  }

  @action
  setProfileImageUrl(value) {
    this.data.profileImageUrl = value;
  }
  get profileImageUrl() {
    return this.data && this.data.profileImageUrl;
  }

  @action
  setCoverPhoto(value) {
    this.data.profile.coverPhoto = value;
  }
  get coverPhoto() {
    return this.data && this.data.profile.coverPhoto;
  }

  @action
  setFollowersCount(value) {
    this.counters.followers = value;
  }
  get followersCount() {
    return this.counters.followers;
  }

  @action
  setFollowingsCount(value) {
    this.counters.followings = value;
  }
  get followingsCount() {
    return this.counters.followings;
  }

  @action
  setCreatedEventsCount(value) {
    this.counters.createdEvents = value;
  }
  get createdEventsCount() {
    return this.counters.createdEvents;
  }

  @action
  setCreatedSchedulesCount(value) {
    this.counters.createdSchedules = value;
  }
  get createdSchedulesCount() {
    return this.counters.createdSchedules;
  }

  get nOfPublicCreations() {
    return this.createdEventsCount + this.createdSchedulesCount;
  }
  
  constructor(props = {}) {
    super(props);
    this.init(props);
  }

  init(props) {
    if (this.id === props.params.id || !this.data) return;

    this.setId(props.params.id);
    this.setIsCurrent(this.isSigned && this.data.id === Auth.getActiveUser().id);

    this.fetchUserProfile();
  }

  fetchUserProfile() {
    return Promise.all([
      userModel.fetchProfileById(this.id),
      eventModel.getPublicCreationsCount(this.id),
      scheduleModel.getPublicCreationsCount(this.id),
    ])
    .spread((profile, eventsCount, schedulesCount) => this.handleInitialResponse(profile, eventsCount, schedulesCount));
  }
  
  handleInitialResponse(profile, eventsCount, schedulesCount) {
    if (this.isCurrent) Auth.setActiveUser(profile.data);
    const resolution = (window.innerHeight > window.innerWidth ? window.innerHeight : window.innerWidth) / 2;
    const profileImgUrl = UserModel.formatImageUrl(profile.data.profilePicture, resolution);
    this.setProfile(profile.data);
    this.setMeta(profile.follows);
    this.setCoverPhoto(userModel.formatProfileCoverUrl(profile.data.coverPhoto));
    this.setProfileImageUrl(profileImgUrl);
    this.setFollowersCount(profile.follows.count.followers);
    this.setFollowingsCount(profile.follows.count.following);
    this.setCreatedEventsCount(eventsCount);
    this.setCreatedSchedulesCount(schedulesCount);
    return Promise.resolve();
  }

  deleteBackgroundImage() {
    return userModel.deleteProfileCover();
  }

  onCreationDelete(type) {
    if (!this.PUBLIC_CREATIONS_DELETE_HANDLERS[type]) return;
    this.PUBLIC_CREATIONS_DELETE_HANDLERS[type]();
    this.setIsNeedToUpdateUpcoming(true);
  }

  onBackgroundImageUpload(image) {
    this.setBackgroundImage(image);

    const formData = imageToFormData(image);
    const config = { body: formData };

    return userModel.updateProfileCover(config)
    .then(res => {
      this.setCoverPhoto(userModel.formatProfileCoverUrl(res.coverPhoto));
      return this.coverPhoto;
    })
    .catch(console.error);
  }
  
  @action
  setIsNeedToUpdateUpcoming(value) {
    this.data.isNeedToUpdateUpcoming = value;
  }
  
  @action
  clear() {
    this.data = null;
    this.counters = null;
  }
}

export default ProfileDataState;
