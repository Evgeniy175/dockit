import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Image from '../../../shared-components/image/index.jsx';
import ImageCropper from '../../../shared-components/image-crop/index.jsx';
import LightboxWrapper from '../../../shared-components/lightbox-wrapper/index.jsx';
import Dropdown from '../../../shared-components/profile/image-dropdown/index.jsx';
import PhotoAttribution from '../../../shared-components/photo-attribution/index.jsx';

import DefaultProfilePicture from '../../../assets/images/default-images/profile-picture.png';

import DataState from './state/data';

import { imageToFormData } from '../../../utils/formatting/image';

import UserModel from '../../../models/users/index';

import { HORIZONTAL_RATIO, VERTICAL_RATIO, WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../../shared-components/image/constants';

const userModel = new UserModel();



@observer
class ProfileImage extends Component {
  @observable isGalleryOpen = false;

  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
    });

    this.onChange = ::this.onChangeHandler;
    this.onCrop = ::this.onCropHandler;
    this.setCropper = ::this.setCropperHandler;
    this.onImageClick = ::this.onImageClickHandler;
    this.onLightboxClose = ::this.onLightboxCloseHandler;
    this.onImageSelectCancel = ::this.onImageSelectCancelHandler;
    this.onUpdateClick = ::this.onUpdateClickHandler;
    this.onDeleteClick = ::this.onDeleteClickHandler;

    this.data.setShownImage(this.props.image);
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.setShownImage(nextProps.image);
  }

  onChangeHandler(e) {
    let files;
    
    e.preventDefault();
    
    if (e.dataTransfer) files = e.dataTransfer.files;
    else if (e.target) files = e.target.files;
    
    if (!files || !files[0]) return;
    
    const reader = new FileReader();
    
    reader.onload = () => { this.data.setImage(reader.result); };
    reader.readAsDataURL(files[0]);
    
    this.data.setCropVisibility(true);
  }
  
  onCropHandler() {
    const prevImage = this.data.shownImage;
    
    const croppedImage = this.data.cropper.getCroppedCanvas().toDataURL('image/jpeg');
    this.data.setCroppedImage(croppedImage);
    this.data.setShownImage(this.data.croppedImage);
    this.data.setCropVisibility(false);
  
    const formData = imageToFormData(croppedImage);
    const config = { body: formData };
  
    return userModel.updateProfilePhoto(config)
    .catch(err => {
      console.error(err);
      this.data.setShownImage(prevImage);
    });
  }
  
  setCropperHandler(value) {
    this.data.setCropperHandler(value);
  }
  
  onImageClickHandler() {
    this.setLightboxVisibility(true);
  }
  
  onLightboxCloseHandler() {
    this.setLightboxVisibility(false);
  }
  
  onImageSelectCancelHandler() {
    this.data.setCropVisibility(false);
  }
  
  onUpdateClickHandler() {
    if (this.data.isCropVisible) return;
  
    this.data.cropper = {};
  
    const uploadInput = document.getElementById('profile-image-upload-input');
    uploadInput.value = null;
    uploadInput.click();
    
    this.props.onChange(this.data.shownImage);
  }
  
  onDeleteClickHandler() {
    return userModel.deleteProfilePhoto()
    .then(() => {
      this.data.setShownImage();
      this.props.onChange(this.data.shownImage);
    })
    .catch(console.error);
  }
  
  @action
  setLightboxVisibility(value) {
    if (!!value && this.isDefaultImage()) return;
    
    this.isGalleryOpen = value;
  }
  
  isDefaultImage() {
    return this.data.shownImage === DefaultProfilePicture;
  }
  
  render() {
    const { isCurrentUser, attributions, currentUserProfile } = this.props;
    const { image, isCropVisible, shownImage } = this.data;

    const photoAttributionsValues = {
      idx: this.data.imageIdx,
      data: attributions,
    };

    const lightboxImages = [{ src: shownImage, caption: attributions && <PhotoAttribution values={photoAttributionsValues} /> }];
    const isCurrentUserProfile = !!currentUserProfile;

    const menuItems = [
      {
        title: 'Edit',
        action: this.onUpdateClick
      },
      {
        title: 'Delete',
        action: this.onDeleteClick
      }
    ];

    const profileImageData = {
      values: {
        src: shownImage,
        wrapperClassName: WRAPPER_CLASS_NAME,
        className: `${IMAGE_CLASS_NAME} no-select${this.isDefaultImage() ? '' : ` hoverable`}`,
        aspectRatio: ASPECT_RATIOS.s1x1,
        prepareWrapper: true,
      },
      handlers: {
        onClick: this.onImageClick,
      },
    };

    return (
      <Row className='profile-image'>
        <Col xs={12}>
          <div className='profile-image-container'>
            <Image key={shownImage} values={profileImageData.values} handlers={profileImageData.handlers} />
            <LightboxWrapper open={this.isGalleryOpen} images={lightboxImages} onClose={this.onLightboxClose} />
            { isCurrentUser && <Dropdown currentUserProfile={isCurrentUserProfile} menuItems={menuItems} parentClassName='profile-image-container' /> }
          </div>
          <input id='profile-image-upload-input' type='file' onChange={this.onChange} hidden />
          { isCurrentUser &&
            <ImageCropper setCropper={this.setCropper}
                          isVisible={isCropVisible}
                          horizontalRatio={HORIZONTAL_RATIO}
                          verticalRatio={VERTICAL_RATIO}
                          onCropFinish={this.onCrop}
                          onCancel={this.onImageSelectCancel}
                          src={toJS(image)}/>
          }
        </Col>
      </Row>
    );
  }
}

ProfileImage.propTypes = {
  isCurrentUser: PropTypes.bool,
  image: PropTypes.string,
  currentUserProfile: PropTypes.bool,
  onChange: PropTypes.func,
};

ProfileImage.defaultProps = {
  currentUserProfile: false
};

export default ProfileImage;
