export const HORIZONTAL_RATIO = 1;
export const VERTICAL_RATIO = 1;
export const MIN_CONTAINER_WIDTH = 480;
export const MIN_CONTAINER_HEIGHT = 600;

export const WRAPPER_CLASS_NAME = 'profile-image-wrapper';
export const IMAGE_CLASS_NAME = 'pic';
