import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';



@observer
class ProfileInfoDescription extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const description = this.props.description;
    
    return (
      <Row className='description'>
        <Col xs={12}>{description}</Col>
      </Row>
    );
  }
}

ProfileInfoDescription.propTypes = {
  description: PropTypes.string
};

export default ProfileInfoDescription;
