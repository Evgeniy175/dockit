import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';

import Description from './description/index.jsx';
import Name from './name/index.jsx';
import UserTag from './usertag/index.jsx';
import Location from '../../../shared-components/location/index.jsx';

import { getLocation } from '../../../utils/location';



@observer
class ProfileInfo extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { isCurrentUser, profile, meta, iconClass } = this.props;
    if (isEmpty(profile)) return null;
    const { username, description } = profile;
    const location = getLocation(profile);

    return (
      <Row className='profile-info'>
        <Col xs={12}>
          <Name isCurrentUser={isCurrentUser} profile={profile} meta={meta} iconClass={iconClass} />
          <UserTag usertag={username} />
          { location && <Location item={profile} /> }
          { description && <Description description={description} /> }
        </Col>
      </Row>
    );
  }
}

ProfileInfo.propTypes = {
  isCurrentUser: PropTypes.bool.isRequired,
  profile: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  iconClass: PropTypes.string.isRequired
};

export default ProfileInfo;
