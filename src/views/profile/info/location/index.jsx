import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';



@observer
class ProfileInfoLocation extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Row className='location'>
        <Col xs={12}>{this.props.location}</Col>
      </Row>
    );
  }
}

ProfileInfoLocation.propTypes = {
  location: PropTypes.string
};

export default ProfileInfoLocation;
