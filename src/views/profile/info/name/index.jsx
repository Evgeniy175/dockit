import React, { Component } from 'react';
import { Link } from 'react-router'
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import {get} from 'lodash';
import Svg from 'react-svg';

import { Auth } from '../../../../auth';

import SettingsIcon from '../../../../assets/images/icons/settings.svg';
import Menu from '../../../../shared-components/users/menu/index.jsx';



@observer
class ProfileInfoName extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { isCurrentUser, profile, meta, iconClass } = this.props;
    const isSigned = Auth.isSigned();
    const { id, name } = profile;

    const usersMenuValues = {
      userId: id,
      personName: name,
      isUserPrivate: !profile.public,
      isFollowed: !!meta.mine,
      isAccepted: !!get(meta, 'mine.accepted'),
    };

    return (
      <Row className='name'>
        <Col xsOffset={2} xs={8}>
          <span className='user-name-text'>
            { name }
          </span>
          {
            isCurrentUser && <Link to={'/profile/settings'} >
              <Svg path={SettingsIcon} className={iconClass} />
            </Link>
          }
          { isSigned && !isCurrentUser && <Menu values={usersMenuValues} /> }
        </Col>
      </Row>
    );
  }
}

ProfileInfoName.propTypes = {
  profile: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  isCurrentUser: PropTypes.bool.isRequired,
  iconClass: PropTypes.string,
};

export default ProfileInfoName;
