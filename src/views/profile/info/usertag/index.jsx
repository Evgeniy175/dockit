import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';



@observer
class ProfileInfoUserTag extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Row className='usertag'>
        <Col xs={12}>{`@${this.props.usertag}`}</Col>
      </Row>
    );
  }
}

ProfileInfoUserTag.propTypes = {
  usertag: PropTypes.string.isRequired,
};

export default ProfileInfoUserTag;
