import { action, observable, computed, toJS } from 'mobx';

import { Auth } from '../../../../auth'

import Refresher from '../../../../utils/refresher';
import { imageToFormData } from '../../../../utils/formatting/image';

import DataState from '../../../../utils/state/data';

import NotableModel from '../../../../models/notables';
import CommentModel from '../../../../models/comments';

import { PORTION_SIZE } from '../constants';
import { TYPES } from '../../../../shared-components/feed/feed-item/constants';

const notableModel = new NotableModel();
const commentModel = new CommentModel();



class ProfileFeedDataState extends DataState {
  @observable userId;

  @observable feedItems = [];
  @observable isBusy = false;
  @observable isLoaded = false;
  @observable isEnd = false;

  @observable commentForPost;
  // due to sometimes several items can disappear. E.g. you requested 20, returns 19
  refetchCount = 0;
  refresher;

  nextPage;

  get isLoadMoreAvailable() {
    return !this.isEnd && !this.isBusy && !!this.nextPage;
  }

  get isSigned() {
    return Auth.isSigned();
  }
  
  constructor(props = {}) {
    super(props);
    const refresherConfig = {
      cb: ::this.refreshData,
    };
    this.refresher = new Refresher(refresherConfig);
  }

  refreshData() {
    const config = {
      params: {
        limit: this.refetchCount,
      },
    };

    return notableModel.fetchProfileFeed(this.userId, config)
    .then(res => Promise.resolve(this.parseResponse(res)))
    .catch(console.error);
  }
  
  fetchFeedItems() {
    const config = {
      params: {
        limit: PORTION_SIZE,
      },
    };
    this.refetchCount = PORTION_SIZE;
    this.setIsBusy(true);
    
    this.setIsLoaded(false);
    return notableModel.fetchProfileFeed(this.userId, config)
    .then(res => {
      if (!res) return Promise.reject();
      this.parseResponse(res);
      this.setIsBusy(false);
      return Promise.resolve(res);
    })
    .catch(console.error)
    .finally(() => this.setIsLoaded(true));
  }
  
  fetchMoreFeedItems() {
    const config = {
      params: {
        limit: PORTION_SIZE,
        page: this.nextPage,
      }
    };
    this.refetchCount += PORTION_SIZE;
    return notableModel.fetchProfileFeed(this.userId, config)
    .then(res => this.handleLoadMore(res))
    .catch(console.error);
  }

  handleLoadMore(res) {
    this.attachFeedItems(res);
    return Promise.resolve(res);
  }

  deleteItem(id) {
    return new Promise(resolve => {
      this.deleteItemFromList(id);
      return resolve();
    })
  }

  addComment(message, image) {
    return commentModel.addCommentToUser(this.userId, message)
    .then(res => {
      if (!image) return Promise.resolve(res);

      const formData = imageToFormData(image);
      const config = { body: formData };

      return commentModel.attachPictureToComment(res.id, config)
      .then(imgRes => Promise.resolve(Object.assign(res, imgRes)));
    })
    .then(res => {
      this.attachFeedItemsToTheTop([{
        action: 'create',
        data: res,
        actionData: res,
        type: TYPES.COMMENT,
        timestamp: res.created_at,
      }]);
      this.refetchCount++;
      return Promise.resolve();
    })
    .catch(console.error);
  }

  @action
  setCommentForPost(value) {
    this.commentForPost = value;
  }

  @action
  setUserId(value) {
    this.userId = value;
  }

  @action
  deleteItemFromList(id) {
    this.feedItems = this.feedItems.filter(item => item.data.id !== id);
  }

  @action
  parseResponse(res) {
    this.nextPage = res.nextPage;
    this.feedItems = res.data;
  }

  @action
  attachFeedItemsToTheTop(feedItems = []) {
    this.feedItems = feedItems.filter(item => item !== null).concat(...this.feedItems);
  }
  
  @action
  attachFeedItems(res) {
    this.setIsEnd(res.data.length === 0);
    this.nextPage = res.nextPage;
    this.feedItems = this.feedItems.concat(res.data);
  }

  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }

  @action
  setIsEnd(value) {
    this.isEnd = value;
  }

  @action
  setIsBusy(value) {
    this.isBusy = value;
  }

  @action
  clear() {
    this.feedItems = null;
  }
}

export default ProfileFeedDataState;
