import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import { Auth } from '../../../auth';

import EmptyText from '../../../shared-components/empty-text/index.jsx';
import Item from '../../../shared-components/feed/feed-item/index.jsx';
import Loader from '../../../shared-components/loader-bottom/index.jsx';
import CenteredLoader from '../../../shared-components/loader/index.jsx';
import CommentAdd from '../../../shared-components/create-comment/index.jsx';
import UnauthText from '../../../shared-components/unauthorized/text/index.jsx';
import ReachedTheEndText from '../../../shared-components/end-text/index.jsx';
//import Upcoming from '../upcoming/index.jsx';

import DataState from './state/data';

import { EMPTY_TEXT, NEW_COMMENT_ROWS, NEW_COMMENT_PLACEHOLDER } from './constants';
import { generateKey } from '../../../utils/generators/feed';
import LoadMoreHelper from '../../../utils/dom/events/scroll-helper';

import { TARGETS } from '../../../shared-components/unauthorized/text/constants';
import { TYPES } from '../../../shared-components/create-comment/constants';
import { EVENTS } from '../../../utils/dom/events/scroll-helper/constants';



@observer
class ProfileFeed extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(),
    });
    this.data.setUserId(this.props.userId);
    this.data.fetchFeedItems();

    this.loadMoreHelper = new LoadMoreHelper({
      handlers: {
        [EVENTS.WHEEL.name]: ::this.onWheelHandler,
        [EVENTS.TOUCH_MOVE.name]: ::this.onWheelHandler,
        [EVENTS.KEY_DOWN.name]: ::this.onWheelHandler,
      },
    });

    this.itemHandlers = {
      onDelete: ::this.onItemDeleteHandler,
    };
    this.commentHandlers = {
      onSubmit: ::this.onCommentAddingHandler,
    };
    this.onUpcomingUpdate = ::this.onUpcomingUpdateHandler;
  }

  componentWillReceiveProps(nextProps) {
    if (this.data.userId === nextProps.userId) return;
    this.data.setUserId(nextProps.userId);
    this.data.fetchFeedItems();
  }

  componentWillUnmount() {
    this.loadMoreHelper.dispose();
    this.data.clear();
    this.data = null;
    this.itemHandlers = null;
    this.commentHandlers = null;
    this.loadMoreHelper = null;
  }

  onUpcomingUpdateHandler() {
    this.props.onUpcomingUpdate();
  }

  onWheelHandler() {
    if (!this.data.isLoadMoreAvailable) return;
    this.data.setIsBusy(true);
    return this.data.fetchMoreFeedItems()
    .catch(console.error)
    .finally(() => this.data.setIsBusy(false));
  }

  onItemDeleteHandler(id) {
    this.data.deleteItem(id);
  }

  onCommentAddingHandler(message, image) {
    return this.data.addComment(message, image);
  }

  render() {
    const { user } = this.props;
    let { userId, isSigned } = this.data;
    const isPrivate = user && !user.public;
    if (!isSigned && isPrivate) return <UnauthText target={TARGETS.FEED} />;
    const { isEnd, isBusy, isLoaded, commentForPost, feedItems } = this.data;
    const currUserId = isSigned && Auth.getActiveUser().id;
    const isUserProfile = userId === currUserId;
    const isNeedToUpdateUpcoming = this.props.needToUpdateUpcoming;
    userId = isUserProfile ? '' : userId;

    if (isBusy && !isLoaded) return <CenteredLoader />;
    if (feedItems.length === 0) return <EmptyText text={EMPTY_TEXT} />;

    const commentValues = {
      userTags: commentForPost && commentForPost.user ? new Set([commentForPost.user.username]) : null,
      type: TYPES.CREATE,
      rows: NEW_COMMENT_ROWS,
      placeholder: isUserProfile ? NEW_COMMENT_PLACEHOLDER : `Say something to ${user.username}...`,
    };

    return (
      <Grid className='view-feed'>
        <Row>
          <Col xs={12} mdOffset={3} md={6}>
            <CommentAdd values={commentValues} handlers={this.commentHandlers} />
          </Col>
        </Row>
        <Row>
          <Col xs={12} mdOffset={3} md={6} className='feed-container'>
            { feedItems.map((item, idx) => <Item key={generateKey(item, idx)} values={{ item, isUserFeed: isUserProfile }} handlers={this.itemHandlers} />) }
            { isBusy && <Loader /> }
          </Col>
          {
            /*isUserProfile &&
            <Clearfix visibleLgBlock={true}>
              <Col>
                <Upcoming id={userId} needToUpdate={isNeedToUpdateUpcoming} onUpdate={this.onUpcomingUpdate} />
              </Col>
            </Clearfix>*/
          }
        </Row>
        <Row>
          {isEnd && <ReachedTheEndText />}
        </Row>
      </Grid>
    );
  }
}

ProfileFeed.propTypes = {
  user: PropTypes.object.isRequired,
  needToUpdateUpcoming: PropTypes.bool,
  onUpcomingUpdate: PropTypes.func,
};

export default ProfileFeed;
