import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Grid } from 'react-bootstrap';
import { isEmpty } from 'lodash';

import ScrollToTop from '../../shared-components/scroll-to-top/index.jsx';
import BackgroundImage from '../../shared-components/background-image/index.jsx';
import Loader from '../../shared-components/loader/index.jsx';

import Feed from './feed/index.jsx';
import ProfileActivity from './activity/index.jsx';
import ProfileImage from './image/index.jsx';
import ProfileInfo from './info/index.jsx';

import { setTitle } from '../../utils/formatting/title';

import { ICONS_CLASSES } from './info/constants';
import { PAGES, PAGE_TITLES } from '../../utils/formatting/constants';

import DataState from './state/data';



@observer
class UserProfile extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
    setTitle(PAGE_TITLES[PAGES.PROFILE]());

    this.onProfileImageChange = ::this.onProfileImageChangeHandler;
    this.onImageUpload = ::this.onBackgroundImageUploadHandler;
    this.onImageDelete = ::this.onBackgroundImageDeleteHandler;
    this.onUpcomingUpdate = ::this.onUpcomingUpdateHandler;

    this.profileActivityHandlers = {
      onFollowerAdding: ::this.onFollowerAddingHandler,
      onFollowerDeleting: ::this.onFollowerDeletingHandler,
      onFollowingDeleting: ::this.onFollowingDeletingHandler,
      onCreationDelete: ::this.onCreationDeleteHandler,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  componentWillUnmount() {
    this.data.clear();
  }
  
  onProfileImageChangeHandler(value) {
    this.data.setProfileImageUrl(value);
  }

  onBackgroundImageUploadHandler(image) {
    return this.data.onBackgroundImageUpload(image);
  }

  onBackgroundImageDeleteHandler() {
    return this.data.deleteBackgroundImage();
  }
  
  onFollowerAddingHandler(isAccepted) {
    const { isCurrent } = this.data;
    if (isCurrent && isAccepted) this.data.setFollowingsCount(this.data.followingsCount + 1);
  }
  
  onFollowerDeletingHandler(isAccepted) {
    const { isCurrent } = this.data;
    if (isCurrent && isAccepted) this.data.setFollowingsCount(this.data.followingsCount - 1);
  }
  
  onFollowingDeletingHandler() {
    this.data.setFollowingsCount(this.data.followingsCount - 1);
  }
  
  onCreationDeleteHandler(type) {
    this.data.onCreationDelete(type);
  }
  
  onUpcomingUpdateHandler() {
    this.data.setIsNeedToUpdateUpcoming(false);
  }
  
  render() {
    const { isCurrent, id, counters, coverPhoto, isNeedToUpdateUpcoming, profile, meta, profileImageUrl } = this.data;
    const isProfileLoaded = !isEmpty(profile);

    if (!isProfileLoaded) return <Loader />;

    return (
      <div>
        <ScrollToTop />
        <BackgroundImage isCurrentUser={isCurrent} imageUrl={coverPhoto} onChange={this.onImageUpload} onDelete={this.onImageDelete} />
        <Grid className='profile'>
          <ProfileImage isCurrentUser={isCurrent} currentUserProfile image={profileImageUrl} onChange={this.onProfileImageChange} />
          <ProfileInfo isCurrentUser={isCurrent} profile={profile} meta={meta} iconClass={ICONS_CLASSES.SETTINGS} />
          <ProfileActivity userId={id} isFollowedByCurrent={!!meta.mine} isPrivate={!profile.public} totalCount={counters} handlers={this.profileActivityHandlers} />
        </Grid>
        <Feed user={profile} userId={id} needToUpdateUpcoming={isNeedToUpdateUpcoming} onUpcomingUpdate={this.onUpcomingUpdate} />
      </div>
    );
  }
}

export default UserProfile;
