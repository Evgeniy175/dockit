import { TYPES } from '../constants.js';

export const TITLES = {
  [TYPES.FOLLOWERS]: 'Followers',
  [TYPES.FOLLOWINGS]: 'Following',
  [TYPES.PUBLIC_CREATIONS]: 'Public Creations'
};
