import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import Modal from 'react-bootstrap-modal';
import PropTypes from 'prop-types';

import { TITLES } from './constants';
import { TYPES } from '../constants.js'

import { handleButton } from '../../../../utils/scrolling';

import Followers from '../followers/index.jsx';
import Followings from '../followings/index.jsx';
import Creations from '../creations/index.jsx';



@observer
class ProfileModal extends Component {
  RESOLVERS = {
    [TYPES.FOLLOWERS]: (userId, handlers) => <Followers userId={userId} handlers={handlers} />,
    [TYPES.FOLLOWINGS]: (userId, handlers) => <Followings userId={userId} handlers={handlers} />,
    [TYPES.PUBLIC_CREATIONS]: (userId, handlers, totalCount) => <Creations userId={userId} totalCount={totalCount} handlers={handlers} />
  };
  
  constructor(props) {
    super(props);
    this.onModalClose = ::this.onModalCloseHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.getContainer = ::this.getContainerHandler;
    this.onButtonScroll = ::this.onButtonScrollHandler;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open) document.body.addEventListener('keydown', this.onKeyDown);
  }
  
  onModalCloseHandler() {
    this.props.onClose();
    document.body.removeEventListener('keydown', this.onKeyDown);
  }

  onKeyDownHandler(e) {
    return handleButton(e, this.getContainer, this.onButtonScroll);
  }

  getContainerHandler() {
    return document.getElementsByClassName('modal-body')[0];
  }

  onButtonScrollHandler() {
    const elem = document.getElementsByClassName('modal-data-container')[0];

    if (!elem) return;

    const event = new Event('touchmove', { bubbles: true, cancelable: false });
    elem.dispatchEvent(event);
  }
  
  render() {
    const { type, open, userId, handlers, totalCount } = this.props;
    const title = TITLES[type];
    return (
      <Modal show={open} onHide={this.onModalClose} className='profile-activity-modal'>
        <Modal.Header closeButton>
          <Modal.Title id={title}>{ title }</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          { this.RESOLVERS[type](userId, handlers, totalCount) }
        </Modal.Body>
      </Modal>
    );
  }
}

ProfileModal.propTypes = {
  type: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handlers: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  totalCount: PropTypes.object.isRequired
};

export default ProfileModal;
