import { action, observable, computed, toJS } from 'mobx';

import Promise from 'bluebird';

import EventModel from '../../../../../models/events';
import ScheduleModel from '../../../../../models/schedules';

import moment from 'moment-timezone';

import { PORTION_SIZE } from '../constants';
import { TYPES as CREATIONS_TYPES } from '../../../../creations/constants';
import { descCompare } from '../../../../../utils/sorting/moment';

import DataState from '../../../../../utils/state/data';

const eventModel = new EventModel();
const scheduleModel = new ScheduleModel();



class CreationsDataState extends DataState {
  @observable creations = [];
  
  isMoreEventsAvailable = false;
  isMoreSchedulesAvailable = false;
  
  eventsTotalCount = 0;
  schedulesTotalCount = 0;
  
  constructor(props = {}) {
    super(props);
    this.init(props);
  }
  
  init(props) {
    this.eventsTotalCount = props.totalCount.createdEvents;
    this.schedulesTotalCount = props.totalCount.createdSchedules;
  }
  
  fetchCreations(userId) {
    return Promise.all([
      eventModel.fetchEventsForUser(userId, PORTION_SIZE),
      scheduleModel.fetchSchedulesForUser(userId, PORTION_SIZE)
    ])
    .spread((events, schedules) => { this.setCreations(events, schedules); return Promise.resolve(); });
  }
  
  @action
  setCreations(events, schedules) {
    events.data.forEach(event => { event.type = CREATIONS_TYPES.EVENT; });
    schedules.data.forEach(schedule => { schedule.type = CREATIONS_TYPES.SCHEDULE; });
    
    const creations = events.data.concat(schedules.data);
    creations.sort(descCompare);
    this.creations = creations.slice(0, PORTION_SIZE);
  
    const nOfEvents = this.getNumberOfCreations(CREATIONS_TYPES.EVENT);
    const nOfSchedules = this.getNumberOfCreations(CREATIONS_TYPES.SCHEDULE);
    
    this.isMoreEventsAvailable = nOfEvents < this.eventsTotalCount;
    this.isMoreSchedulesAvailable = nOfSchedules < this.schedulesTotalCount;
  }
  
  fetchMoreCreations(userId) {
    const requests = this.getFetchMoreMethods(userId);
    
    return Promise.all(requests)
    .spread((events, schedules) => this.attachCreations(events, schedules));
  }
  
  getFetchMoreMethods(userId) {
    const min = this.getStartDateForFetch();
  
    return [
      eventModel.fetchEventsForUser(userId, PORTION_SIZE, min),
      scheduleModel.fetchSchedulesForUser(userId, PORTION_SIZE, min)
    ];
  }
  
  @action
  attachCreations(events, schedules) {
    events.data.forEach(event => { event.type = CREATIONS_TYPES.EVENT; });
    schedules.data.forEach(schedule => { schedule.type = CREATIONS_TYPES.SCHEDULE; });
    
    let creations = events.data.concat(schedules.data);
    creations.sort(descCompare);
    creations = creations.slice(0, PORTION_SIZE);
    
    this.creations = this.creations.concat(creations);
    
    const nOfEvents = this.getNumberOfCreations(CREATIONS_TYPES.EVENT);
    const nOfSchedules = this.getNumberOfCreations(CREATIONS_TYPES.SCHEDULE);
  
    this.isMoreEventsAvailable = nOfEvents < this.eventsTotalCount;
    this.isMoreSchedulesAvailable = nOfSchedules < this.schedulesTotalCount;
  }
  
  getNumberOfCreations(type) {
    return !!type ? this.creations.filter(creation => creation.type === type).length : 0;
  }
  
  getStartDateForFetch() {
    if (this.creations.length == 0) return null;
    
    let min = this.creations[0].created_at;
    
    this.creations.forEach(creation => {
      const minMoment = moment(min);
      const current = moment(creation.created_at);
      
      if (current.isBefore(minMoment)) min = creation.created_at;
    });
  
    return min;
  }
  
  canLoadMore() {
    return this.isMoreEventsAvailable || this.isMoreSchedulesAvailable;
  }
  
  @action
  deleteCreation(id) {
    this.creations = this.creations.filter(creation => creation.id !== id);
  }
  
  @action
  clearCreations() {
    this.creations = [];
  }
}

export default CreationsDataState;
