import { action, observable, toJS } from 'mobx';

import UIState from '../../../../../utils/state/data';

import jq from '../../../../../utils/jquery';

import { LOAD_BUFFER_FROM_BOTTOM, WRAPPER_CLASS_NAME } from '../constants';



class UserCreationsUIState extends UIState {
  @observable isLoaded = false;

  get wrapper() {
    return jq.wrap(`.${WRAPPER_CLASS_NAME}`);
  }

  constructor(props = {}) {
    super(props);
  }
  
  isNeedToLoadMore() {
    const container = document.getElementsByClassName('modal-body')[0];
  
    if (!container) return false;
    
    const fullHeight = container.scrollHeight;
    const currentScrollHeight = container.scrollTop + container.clientHeight;
    const currentPercent = currentScrollHeight / fullHeight * 100;
    return 100 - currentPercent < LOAD_BUFFER_FROM_BOTTOM;
  }
  
  @action
  setIsLoaded(value) {
    this.isLoaded = value;
  }
}

export default UserCreationsUIState;
