import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

import { Auth } from '../../../../../auth';

import Image from '../common/image/index.jsx';
import Text from './text/index.jsx';
import Menu from '../../../../../shared-components/creations/menu/index.jsx';
import Dock from '../../../../../shared-components/action-command/dock/index.jsx';

import { TARGETS } from '../../../../../shared-components/responsive-image/constants';
import { TYPES } from '../../../../../views/creations/constants';



@observer
class CreationsEvent extends Component {
  constructor(props) {
    super(props);
    this.onDelete = ::this.onDeleteHandler;
  }
  
  onDeleteHandler(id) {
    this.props.onDelete(id);
  }
  
  getEventId() {
    return this.props.event.id;
  }
  
  render() {
    const id = this.getEventId();
    const event = this.props.event;
    const text = event.title;
    const userId = event.user_id;
    const currUserId = Auth.isSigned() && Auth.getActiveUser().id;
    const isCurrUserEvent = userId === currUserId;

    const dockActionData = {
      type: TYPES.EVENT,
      event,
    };
    
    return (
      <div className='creation-item'>
        <Link to={`/${TYPES.EVENT}/${id}`}>
          <Image src={event.image} target={TARGETS.EVENT} />
          <Text text={text} />
        </Link>
        {
          isCurrUserEvent
          ? <Menu type={TYPES.EVENT} item={event} onDelete={this.onDelete} />
          : <Dock data={dockActionData} countShown={false} />
        }
      </div>
    );
  }
}

CreationsEvent.propTypes = {
  event: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default CreationsEvent;
