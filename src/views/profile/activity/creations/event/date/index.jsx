import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import moment from 'moment-timezone';

import { MAX_DIFF_IN_DAYS, DATE_FORMAT } from './constants';
import { formatFromNow } from '../../../../../../utils/formatting/time';



@observer
class CreationsEventTitleDate extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const date = this.getFormattedDate();
    
    return (
      <span className='date'>
        { date }
      </span>
    );
  }
  
  getFormattedDate() {
    const now = moment();
    const date = moment(this.props.createdAt);
    const diffInDays = date.diff(now, 'months');
    return diffInDays < MAX_DIFF_IN_DAYS ? formatFromNow(date) : date.format(DATE_FORMAT);
  }
}

CreationsEventTitleDate.propTypes = {
  createdAt: PropTypes.string.isRequired
};

export default CreationsEventTitleDate;
