import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class CreationsEventTitleText extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <span className='text'>
        <div className='name'>{this.props.text}</div>
        <div>Event</div>
      </span>
    );
  }
}

CreationsEventTitleText.propTypes = {
  text: PropTypes.string.isRequired
};

export default CreationsEventTitleText;
