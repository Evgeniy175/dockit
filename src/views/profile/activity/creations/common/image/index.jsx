import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import ResponsiveImage from '../../../../../../shared-components/responsive-image/index.jsx';

import DefaultImage from '../../../../../../assets/images/icons/event-default.svg';



@observer
class CreationsEventTitleImage extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { src, target } = this.props;
      
    return (
      <span className='image'>
        <ResponsiveImage src={src} target={target} defaultSrc={DefaultImage} />
      </span>
    );
  }
}

CreationsEventTitleImage.propTypes = {
  src: PropTypes.string,
  target: PropTypes.string,
};

export default CreationsEventTitleImage;
