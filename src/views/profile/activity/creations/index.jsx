import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Loader from '../../../../shared-components/loader/index.jsx';
import Event from './event/index.jsx';
import Schedule from './schedule/index.jsx';
import CenteredText from '../../../../shared-components/centered-text/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import { EMPTY_TEXT, WRAPPER_CLASS_NAME } from './constants.js';
import { TYPES as CREATIONS_TYPES } from '../../../creations/constants';



@observer
class ProfileCreations extends Component {
  CHILDS = {
    [CREATIONS_TYPES.EVENT]: event => <Event key={`${event.id}_${event.title}`} event={event} onDelete={this.onEventDelete} />,
    [CREATIONS_TYPES.SCHEDULE]: schedule => <Schedule key={`${schedule.id}_${schedule.title}`} schedule={schedule} onDelete={this.onScheduleDelete} />
  };
  
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState()
    });
  }
  
  componentWillMount() {
    const userId = this.getUserId();
    
    this.onWheel = ::this.onWheelHandler;
    this.onEventDelete = ::this.onEventDeleteHandler;
    this.onScheduleDelete = ::this.onScheduleDeleteHandler;
  
    this.ui.setIsLoaded(false);
  
    return this.data.fetchCreations(userId)
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }

  componentDidMount() {
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
  }
  
  componentWillUpdate(nextProps) {
    this.data.init(nextProps);
  }
  
  componentWillUnmount() {
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
    this.data.clearCreations();
  }
  
  onWheelHandler(e) {
    if (!this.data.canLoadMore()) return;

    e.stopPropagation();
    
    const isNeedToLoad = this.ui.isNeedToLoadMore();
    
    if (!isNeedToLoad || !this.ui.isLoaded) return;
    
    this.ui.setIsLoaded(false);
    
    const userId = this.getUserId();
    
    return this.data.fetchMoreCreations(userId)
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onEventDeleteHandler(id) {
    this.data.deleteCreation(id);
    if (!this.props.handlers.onCreationDelete) return;
    this.props.handlers.onCreationDelete(CREATIONS_TYPES.EVENT);
  }
  
  onScheduleDeleteHandler(id) {
    this.data.deleteCreation(id);
    if (!this.props.handlers.onCreationDelete) return;
    this.props.handlers.onCreationDelete(CREATIONS_TYPES.SCHEDULE);
  }
  
  getUserId() {
    return this.props.userId;
  }
  
  render() {
    const items = this.data.creations;

    if (!this.ui.isLoaded && (!items || items.length === 0)) return <Loader />;
    if (!items || items.length === 0) return <CenteredText text={EMPTY_TEXT} />;

    return (
      <div className={`modal-data-container ${WRAPPER_CLASS_NAME}`}>
        { !this.ui.isLoaded && <Loader /> }
        { items.map(item => this.CHILDS[item.type](item)) }
      </div>
    );
  }
}

ProfileCreations.propTypes = {
  userId: PropTypes.string.isRequired,
  totalCount: PropTypes.object.isRequired,
  handlers: PropTypes.object.isRequired
};

export default ProfileCreations;
