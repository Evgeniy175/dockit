import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';

import { Auth } from '../../../../../auth';

import Image from '../common/image/index.jsx';
import Text from './text/index.jsx';
import Menu from '../../../../../shared-components/creations/menu/index.jsx';
import Dock from '../../../../../shared-components/action-command/dock/index.jsx';

import { TARGETS } from '../../../../../shared-components/responsive-image/constants';
import { TYPES } from '../../../../../views/creations/constants';



@observer
class CreationsSchedule extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onDelete = ::this.onDeleteHandler;
  }
  
  onDeleteHandler(id) {
    this.props.onDelete(id);
  }
  
  getScheduleId() {
    return this.props.schedule.id;
  }
  
  render() {
    const id = this.getScheduleId();
    const schedule = this.props.schedule;
    const text = schedule.title;
    const userId = schedule.user_id;
    const currUserId = Auth.isSigned() && Auth.getActiveUser().id;
    const isCurrUserSchedule = userId === currUserId;

    const dockActionData = {
      type: TYPES.SCHEDULE,
      event: schedule,
    };

    return (
      <div className='creation-item'>
        <Link to={`/${TYPES.SCHEDULE}/${id}`}>
          <Image src={schedule.image} target={TARGETS.SCHEDULE} />
          <Text text={text} />
        </Link>
        {
          isCurrUserSchedule
          ? <Menu type={TYPES.SCHEDULE} item={schedule} onDelete={this.onDelete} />
          : <Dock data={dockActionData} countShown={false}/>
        }
      </div>
    );
  }
}

CreationsSchedule.propTypes = {
  schedule: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default CreationsSchedule;
