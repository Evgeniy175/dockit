import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class CreationsScheduleTitleText extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <span className='text'>
        <div className='name'>{this.props.text}</div>
        <div>Schedule</div>
      </span>
    );
  }
}

CreationsScheduleTitleText.propTypes = {
  text: PropTypes.string.isRequired
};

export default CreationsScheduleTitleText;
