export const CREATIONS_TYPES = {
  EVENT: 'EVENT',
  SCHEDULE: 'SCHEDULE'
};

export const WRAPPER_CLASS_NAME = 'profile-public-creations';

export const PORTION_SIZE = 20;
export const LOAD_BUFFER_FROM_BOTTOM = 20;
export const EMPTY_TEXT = 'No public creations yet';
