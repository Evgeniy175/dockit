import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import FollowModel from '../../../../../../models/follows';
import Modal from '../../../../../../shared-components/question-modal/index.jsx';

import RejectIcon from '../../../../../../assets/images/icons/reject.svg';

import DataState from './state/data';

import { MODAL_TITLE } from './constants';
import { CSS_MAPPING } from '../../../../../../shared-components/button/constants';

const followModel = new FollowModel();



@observer
class FollowingMenu extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
  }
  
  componentWillMount() {
    this.openModal = ::this.openModalHandler;
    this.onModalClose = ::this.onModalCloseHandler;
    this.onUnfollowClick = ::this.onUnfollowClickHandler;
    this.onCancelClick = ::this.onCancelClickHandler;
    this.data.setFollowAvailability(true);
  }
  
  openModalHandler() {
    this.data.setModalVisibility(true);
  }
  
  onModalCloseHandler() {
    this.data.setModalVisibility(false);
  }
  
  onUnfollowClickHandler() {
    if (!this.data.isFollowActionAvailable) return;
    
    const userId = this.props.userId;
    this.data.setFollowAvailability(false);
    
    followModel.unfollow(userId)
    .then(() => { this.props.onFollowingDeleting(userId); })
    .catch(console.error)
    .finally(() => { this.data.setFollowAvailability(true); });
  }
  
  onCancelClickHandler() {
    this.data.setModalVisibility(false);
  }
  
  render() {
    const name = this.props.personName;
    const isModalOpen = this.data.isModalOpen;
    const text = `Unfollow ${name}?`;
    const modalButtons = [
      {
        text: 'Unfollow',
        className: CSS_MAPPING.BLUE_FILLED,
        onClick: this.onUnfollowClick
      },
      {
        text: 'Cancel',
        className: CSS_MAPPING.GRAY,
        onClick: this.onCancelClick
      }
    ];
    
    return (
      <span className='menu'>
        <img src={RejectIcon} className='cancel-icon' onClick={this.openModal} />
        <Modal open={isModalOpen} title={MODAL_TITLE} text={text} onClose={this.onModalClose} buttons={modalButtons} />
      </span>
    );
  }
}

FollowingMenu.propTypes = {
  userId: PropTypes.string.isRequired,
  personName: PropTypes.string.isRequired,
  onFollowingDeleting: PropTypes.func.isRequired
};

export default FollowingMenu;
