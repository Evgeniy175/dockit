import { action, observable, computed } from 'mobx';

import DataState from '../../../../../../../utils/state/data';


class FollowingMenuDataState extends DataState {
  @observable isFollowActionAvailable = false;
  @observable isModalOpen = false;
  
  constructor(props = {}) {
    super(props);
  }
  
  @action
  setFollowAvailability(value) {
    this.isFollowActionAvailable = value;
  }
  
  @action
  setModalVisibility(value) {
    this.isModalOpen = value;
  }
}

export default FollowingMenuDataState;
