import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import {get} from 'lodash';

import { Link } from 'react-router';

import Image from './image/index.jsx';
import Text from './text/index.jsx';
import UsersMenu from '../../../../../shared-components/users/menu/index.jsx';

import { Auth } from '../../../../../auth';



@observer
class ProfileFollowing extends Component {
  constructor(props) {
    super(props);
    this.onDeleting = ::this.onDeletingHandler;
  }

  onDeletingHandler() {
    this.props.onFollowingDeleting(this.props.following.data.id);
  }
  
  render() {
    const { following, isCurrentUser, onFollowingDeleting } = this.props;
    const { id, name, username } =  following.data;
    const isSigned = Auth.isSigned();
    const currUserId = isSigned ? Auth.getActiveUser().id : null;

    const usersMenuValues = {
      userId: id,
      personName: name,
      isUserPrivate: !following.data.public,
      isFollowed: !!following.follows.mine,
      isAccepted: !!get(following, 'follows.mine.accepted'),
    };

    const usersMenuHandlers = {
      onDeleting: this.onDeleting,
    };

    return (
      <div className='following'>
        <Link to={`/users/${id}`}>
          <Image src={following.data.profilePicture} />
          <Text name={name} username={username} />
        </Link>
        { isSigned && isCurrentUser && <UsersMenu values={usersMenuValues} handlers={usersMenuHandlers} /> }
        { isSigned && !isCurrentUser && id !== currUserId && <UsersMenu values={usersMenuValues} /> }
      </div>
    );
  }
}

ProfileFollowing.propTypes = {
  isCurrentUser: PropTypes.bool.isRequired,
  following: PropTypes.object.isRequired,
  profileUserId: PropTypes.string.isRequired,
  onFollowingDeleting: PropTypes.func.isRequired
};

export default ProfileFollowing;
