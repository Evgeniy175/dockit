import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Following from './following/index.jsx';
import CenteredText from '../../../../shared-components/centered-text/index.jsx';
import Loader from '../../../../shared-components/loader/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import { Auth } from '../../../../auth';

import { EMPTY_TEXT } from './constants';



@observer
class ProfileFollowings extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState()
    });
  }
  
  componentWillMount() {
    const userId = this.getUserId();
  
    this.onFollowingDeleting = ::this.onFollowingDeletingHandler;
    this.onWheel = ::this.onWheelHandler;
  
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
  
    this.ui.setIsLoaded(false);
  
    this.data.fetchFollowings(userId)
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  componentWillUnmount() {
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
    
    this.data.clearFollowings();
  }
  
  onWheelHandler() {
    if (!this.data.followings.nextPage) return;
  
    const isNeedToLoad = this.ui.isNeedToLoadMore();
  
    if (!isNeedToLoad || !this.ui.isLoaded) return;
  
    this.ui.setIsLoaded(false);
  
    const userId = this.getUserId();
    
    return this.data.fetchMoreFollowings(userId)
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onFollowingDeletingHandler(userId) {
    this.data.handleDeleting(userId);
  }
  
  render() {
    const items = get(this.data, 'followings.data', []);
    const userId = this.getUserId();
    if (this.ui.isLoaded && items.length === 0) return <CenteredText text={EMPTY_TEXT} />;
    const isCurrentUser = Auth.isSigned() && userId === Auth.getActiveUser().id;
    
    return (
      <div className='modal-data-container profile-followings'>
        { !this.ui.isLoaded && <Loader /> }
        {
          items.map(item =>
            <Following key={`${item.data.id}_${item.data.created_at}`}
                       isCurrentUser={isCurrentUser}
                       following={item}
                       profileUserId={userId}
                       onFollowingDeleting={this.onFollowingDeleting} />
          )
        }
      </div>
    );
  }
  
  getUserId() {
    return this.props.userId;
  }
}

ProfileFollowings.propTypes = {
  userId: PropTypes.string.isRequired,
  handlers: PropTypes.object.isRequired
};

export default ProfileFollowings;
