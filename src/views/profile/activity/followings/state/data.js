import { action, observable, computed } from 'mobx';

import { PORTION_SIZE } from '../constants';

import DataState from '../../../../../utils/state/data';

import UserModel from '../../../../../models/users';

const userModel = new UserModel();



class FollowingDataState extends DataState {
  @observable followings = {};
  
  handlers = {};
  page = 1;
  
  constructor(props = {}) {
    super(props);
    this.handlers = props.handlers;
  }
  
  fetchFollowings(userId) {
    return userModel.fetchFollowings(userId, PORTION_SIZE)
    .then(followings => this.setFollowings(followings));
  }
  
  fetchMoreFollowings(userId) {
    return userModel.fetchFollowings(userId, PORTION_SIZE, ++this.page)
    .then(followings => this.appendFollowings(followings));
  }
  
  refetchFollowingsFromCurrent(userId) {
    const limit = PORTION_SIZE * this.page;
    
    return userModel.fetchFollowings(userId, limit)
    .then(followings => this.setFollowings(followings));
  }
  
  @action
  setFollowings(value) {
    this.followings = value;
  }
  
  @action
  appendFollowings(value) {
    this.followings.data = this.followings.data.concat(value.data);
    this.followings.maxPage = value.maxPage;
    this.followings.nextPage = value.nextPage;
  }
  
  @action
  handleDeleting(userId) {
    const idx = this.followings.data.findIndex(following => following.data.id === userId);
  
    this.handlers.onFollowingDeleting();
    
    if (idx === -1) return;
    
    this.followings.data.splice(idx, 1);
  }
  
  @action
  clearFollowings() {
    this.followings = {};
  }
}

export default FollowingDataState;
