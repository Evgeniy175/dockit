import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

import { TYPES } from './constants';

import Item from './item/index.jsx';



@observer
class ProfileActivity extends Component {
  render() {
    return (
      <Row className='profile-activity'>
        <Col xs={4} mdOffset={3} md={2}>
          <Item type={TYPES.FOLLOWERS} {...this.props} />
        </Col>
        <Col xs={4} md={2}>
          <Item type={TYPES.FOLLOWINGS} {...this.props} />
        </Col>
        <Col xs={4} md={2}>
          <Item type={TYPES.PUBLIC_CREATIONS} {...this.props} />
        </Col>
      </Row>
    );
  }
}

ProfileActivity.propTypes = {
  userId: PropTypes.string.isRequired,
  totalCount: PropTypes.object.isRequired,
  isPrivate: PropTypes.bool.isRequired,
  isFollowedByCurrent: PropTypes.bool.isRequired,
  handlers: PropTypes.object.isRequired
};

export default ProfileActivity;
