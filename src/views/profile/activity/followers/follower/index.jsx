import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { Link } from 'react-router';

import Image from './image/index.jsx';
import Text from './text/index.jsx';
import Menu from '../../../../../shared-components/users/menu/index.jsx';

import { Auth } from '../../../../../auth';



@observer
class ProfileFollower extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onFollowerAdding = ::this.onFollowerAddingHandler;
    this.onFollowerDeleting = ::this.onFollowerDeletingHandler;
  }
  
  onFollowerAddingHandler(isAccepted) {
    this.props.onFollowerAdding(isAccepted);
  }
  
  onFollowerDeletingHandler(isAccepted) {
    this.props.onFollowerDeleting(isAccepted);
  }
  
  render() {
    const { follower } = this.props;
    const { id, name, username } = follower.data;
    const currUserId = Auth.isSigned() && Auth.getActiveUser().id;
    const isCurrUser = id === currUserId;

    const usersMenuValues = {
      userId: id,
      personName: name,
      isUserPrivate: !follower.data.public,
      isFollowed: !!follower.follows.mine,
      isAccepted: !!get(follower, 'follows.mine.accepted'),
    };
    const usersMenuHandlers = {
      onAdding: this.onFollowerAdding,
      onDeleting: this.onFollowerDeleting,
    };

    return (
      <div className='follower'>
        <Link to={`/users/${id}`}>
          <Image src={get(follower, 'data.profilePicture')} />
          <Text name={name} username={username} />
        </Link>
        { !isCurrUser && <Menu values={usersMenuValues} handlers={usersMenuHandlers} /> }
      </div>
    );
  }
}

ProfileFollower.propTypes = {
  follower: PropTypes.object.isRequired,
  onFollowerAdding: PropTypes.func.isRequired,
  onFollowerDeleting: PropTypes.func.isRequired
};

export default ProfileFollower;
