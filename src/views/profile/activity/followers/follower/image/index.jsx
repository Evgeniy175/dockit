import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { RESOLUTION } from './constants';

import UserModel from '../../../../../../models/users';



@observer
class FollowerImage extends Component {
  render() {
    const { src } = this.props;
    const url = UserModel.formatImageUrl(src, RESOLUTION);
    return <span className='image'><img src={url} /></span>;
  }
}

FollowerImage.propTypes = {
  src: PropTypes.string,
};

export default FollowerImage;
