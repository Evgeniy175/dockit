import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class FollowerText extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const name = this.props.name;
    const usertag = `@${this.props.username}`;
    
    return (
      <span className='text'>
        <div className='user-name'>{name}</div>
        <div>{usertag}</div>
      </span>
    );
  }
}

FollowerText.propTypes = {
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired
};

export default FollowerText;
