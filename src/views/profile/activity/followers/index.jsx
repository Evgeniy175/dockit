import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Loader from '../../../../shared-components/loader/index.jsx';
import Follower from './follower/index.jsx';
import CenteredText from '../../../../shared-components/centered-text/index.jsx';

import { EMPTY_TEXT } from './constants';

import DataState from './state/data';
import UiState from './state/ui';



@observer
class ProfileFollowers extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState()
    });
  }
  
  componentWillMount() {
    const userId = this.getUserId();
  
    this.ui.setIsLoaded(false);
  
    this.onFollowerAdding = ::this.onFollowerAddingHandler;
    this.onFollowerDeleting = ::this.onFollowerDeletingHandler;
    this.onWheel = ::this.onWheelHandler;
  
    document.body.addEventListener('mousewheel', this.onWheel);
    document.body.addEventListener('touchmove', this.onWheel);
  
    this.data.fetchFollowers(userId)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  componentWillUnmount() {
    document.body.removeEventListener('touchmove', this.onWheel);
    document.body.removeEventListener('mousewheel', this.onWheel);
    
    this.data.clearFollowers();
  }
  
  onWheelHandler() {
    if (!this.data.followers.nextPage) return;
    
    const isNeedToLoad = this.ui.isNeedToLoadMore();
    
    if (!isNeedToLoad || !this.ui.isLoaded) return;
    
    this.ui.setIsLoaded(false);
    
    const userId = this.getUserId();
    
    return this.data.fetchMoreFollowers(userId)
    .catch(console.error)
    .finally(() => this.ui.setIsLoaded(true));
  }
  
  onFollowerAddingHandler(isAccepted) {
    if (!this.data.handlers.onFollowerAdding) return;
    this.data.handlers.onFollowerAdding(isAccepted);
  }
  
  onFollowerDeletingHandler(isAccepted) {
    if (!this.data.handlers.onFollowerDeleting) return;
    this.data.handlers.onFollowerDeleting(isAccepted);
  }
  
  getUserId() {
    return this.props.userId;
  }
  
  render() {
    const items = get(this.data, 'followers.data', []);

    if (this.ui.isLoaded && items.length === 0) return <CenteredText text={EMPTY_TEXT} />;
    
    return (
      <div className='modal-data-container profile-followers'>
        { !this.ui.isLoaded && <Loader /> }
        {
          items.map(item =>
            <Follower key={`${item.data.id}_${item.follows.mine}_${item.data.created_at}`}
                      follower={item}
                      onFollowerAdding={this.onFollowerAdding}
                      onFollowerDeleting={this.onFollowerDeleting} />
          )
        }
      </div>
    );
  }
}

ProfileFollowers.propTypes = {
  userId: PropTypes.string.isRequired,
  handlers: PropTypes.object.isRequired,
};

export default ProfileFollowers;
