import { action, observable, computed } from 'mobx';

import DataState from '../../../../../utils/state/data';

import { PORTION_SIZE } from '../constants';

import UserModel from '../../../../../models/users';

const userModel = new UserModel();



class FollowerDataState extends DataState {
  @observable followers = {};
  
  handlers = {};
  page = 1;
  
  constructor(props = {}) {
    super(props);
    this.handlers = props.handlers;
  }
  
  fetchFollowers(userId) {
    return userModel.fetchFollowers(userId, PORTION_SIZE)
    .then(followers => { this.setFollowers(followers); })
  }
  
  fetchMoreFollowers(userId) {
    return userModel.fetchFollowers(userId, PORTION_SIZE, ++this.page)
    .then(followers => this.appendFollowers(followers));
  }
  
  @action
  setFollowers(value) {
    this.followers = value;
  }
  
  @action
  appendFollowers(value) {
    this.followers.data = this.followers.data.concat(value.data);
    this.followers.maxPage = value.maxPage;
    this.followers.nextPage = value.nextPage;
  }
  
  @action
  clearFollowers() {
    this.followers = {};
  }
}

export default FollowerDataState;
