export const TYPES = {
  FOLLOWERS: 'followers',
  FOLLOWINGS: 'followings',
  PUBLIC_CREATIONS: 'public creations'
};

export const TITLES = {
  [TYPES.FOLLOWERS]: 'Followers',
  [TYPES.FOLLOWINGS]: 'Following',
  [TYPES.PUBLIC_CREATIONS]: 'Public Creations'
};
