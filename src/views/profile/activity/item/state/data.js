import { action, observable, computed } from 'mobx';

import DataState from '../../../../../utils/state/data';

import { Auth } from '../../../../../auth';



class ProfileActivityItemDataState extends DataState {
  @observable isModalOpen = false;
  @observable type = '';
  @observable userId = '';
  @observable totalCount = {};
  @observable handlers = {};

  get isUserProfile() {
    return Auth.isSigned() && this.userId === Auth.getActiveUser().id;
  }
  
  constructor(props) {
    super(props);
    this.init(props);
  }

  init(props) {
    if (this.userId !== props.userId) this.setIsModalOpen(false);

    this.setType(props.type);
    this.setUserId(props.userId);
    this.setTotalCount(props.totalCount);
    this.setHandlers(props.handlers);
  }

  @action
  setType(value) {
    this.type = value;
  }

  @action
  setUserId(value) {
    this.userId = value;
  }

  @action
  setTotalCount(value) {
    this.totalCount = value;
  }

  @action
  setHandlers(value) {
    this.handlers = value;
  }
  
  @action
  setIsModalOpen(value) {
    this.isModalOpen = value;
  }
  
  @action
  clear() {
    this.type = '';
    this.userId = '';
    this.totalCount = {};
    this.handlers = {};
  }
}

export default ProfileActivityItemDataState;
