import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { formatNumber } from '../../../../utils/formatting/number';

import { TYPES, TITLES } from '../constants.js';

import Modal from '../modals/index.jsx';

import DataState from './state/data';



@observer
class ProfileActivityItem extends Component {
  TOTAL_COUNT_RESOLVERS = {
    [TYPES.FOLLOWERS]: totalCount => totalCount.followers,
    [TYPES.FOLLOWINGS]: totalCount => totalCount.followings,
    [TYPES.PUBLIC_CREATIONS]: totalCount => (totalCount.createdEvents + totalCount.createdSchedules),
  };

  get onItemClick() {
    return this.isCanInteract ? this.onClick : null;
  }

  get isCanInteract() {
    const { isFollowedByCurrent, isPrivate } = this.props;
    const { isUserProfile } = this.data;
    return isUserProfile || isFollowedByCurrent || !isPrivate;
  }
  
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
    });
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
    this.onModalClose = ::this.onModalCloseHandler;
  }
  
  componentWillReceiveProps(nextProps) {
    this.data.init(nextProps);
  }
  
  onClickHandler() {
    this.data.setIsModalOpen(true);
  }
  
  onModalCloseHandler() {
    this.data.setIsModalOpen(false);
  }
  
  render() {
    const { userId, type, handlers } = this.props;
    const { isCanInteract } = this;
    const totalCountSummary = this.props.totalCount;
    const isModalOpen = this.data.isModalOpen;
    const totalCount = this.TOTAL_COUNT_RESOLVERS[type](totalCountSummary);
    const formattedTotalCount = formatNumber(totalCount);
    const className = `profile-activity-item${isCanInteract ? ' hoverable' : ''}`;

    return (
      <div onClick={this.onItemClick} className={className}>
        <div className='activity-label'>{ TITLES[type] }</div>
        <div className='value'>{ formattedTotalCount }</div>
        { isCanInteract && <Modal type={type} userId={userId} open={isModalOpen} totalCount={totalCountSummary} handlers={handlers} onClose={this.onModalClose} /> }
      </div>
    );
  }
}

ProfileActivityItem.propTypes = {
  type: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  totalCount: PropTypes.object.isRequired,
  isPrivate: PropTypes.bool.isRequired,
  isFollowedByCurrent: PropTypes.bool.isRequired,
  handlers: PropTypes.object.isRequired
};

export default ProfileActivityItem;
