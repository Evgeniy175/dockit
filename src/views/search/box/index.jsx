import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { get } from 'lodash';
import PropTypes from 'prop-types';

import Results from '../results/index.jsx';
import Input from './input/index.jsx';

import { PARENT_ELEMENT_ID, DELAY } from '../constants';
import { KEY_CODES } from '../../../constants';

import DataState from './state/data';

import { getRandomInt } from '../../../utils/random';



@observer
class SearchBox extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    this.onChange = ::this.onChangeHandler;
    this.onKeyDown = ::this.onKeyDownHandler;
    this.onActiveHeaderChange = ::this.onActiveHeaderChangeHandler;
    this.onSearchBoxClick = ::this.onSearchBoxClickHandler;
    this.onSearchClose = ::this.onSearchCloseHandler;
    this.onItemClick = ::this.onItemClickHandler;
    this.onLoadMore = ::this.onLoadMoreHandler;
    this.doSearch = ::this.doSearchHandler;

    this.inputHandlers = {
      onChange: ::this.onChangeHandler,
      onKeyDown: ::this.onKeyDownHandler,
    };
  }

  componentDidMount() {
    const body = document.body;
    body.addEventListener('click', this.onSearchClose, false);
  }
  
  componentWillUpdate(nextProps) {
    const handler = get(nextProps, 'handlers.onForceSearchExpandFinished');
    const isForceExpand = get(nextProps, 'values.isForceSearchExpand');
    if (isForceExpand) { this.doForceExpand(); this.doSearch(); }
    if (handler) handler();
  }
  
  doForceExpand() {
    this.data.setIsFocused(true);
    setTimeout(() => document.getElementById('searchBox').focus(), 0);
  }
  
  componentWillUnmount() {
    this.data.clear();
    const body = document.body;
    body.removeEventListener('click', this.onSearchClose, false);
  }
  
  onChangeHandler(e) {
    this.data.setValue(e.target.value);
  }
  
  onKeyDownHandler(e) {
    const { keyCode } = e;
    if (keyCode === KEY_CODES.ENTER) return this.doSearch();
    if ([KEY_CODES.PAGE_DOWN, KEY_CODES.PAGE_UP].some(code => code === keyCode)) return;
    this.data.searchId = getRandomInt();
    setTimeout(this.doSearch, DELAY, this.data.searchId);
  }

  doSearchHandler(searchId) {
    if (searchId && searchId !== this.data.searchId) return;
    this.data.doSearch();
    this.data.searchId = null;
  }
  
  onActiveHeaderChangeHandler(value) {
    this.data.setActiveHeader(value);
  }
  
  onSearchBoxClickHandler() {
    this.data.setIsFocused(true);
  }
  
  onSearchCloseHandler(e) {
    let element = e.target;
    let isNeedToClose = element.id !== PARENT_ELEMENT_ID;

    while (isNeedToClose && element && element.parentElement) {
      element = element.parentElement;
      isNeedToClose = element.id !== PARENT_ELEMENT_ID && !element.classList.contains('modal');
    }

    if (!isNeedToClose) return;

    this.data.setIsFocused(false);
    this.data.surfaceClear();
  }
  
  onItemClickHandler() {
    this.data.setIsFocused(false);
    this.props.handlers.onSelect();
  }
  
  onLoadMoreHandler() {
    this.doSearch();
  }
  
  render() {
    const { isBusy, isFirstSearch, activeHeader, isFocused, value } = this.data;
    const searchResults = this.data.getSearchResults();
    const inputValues = {
      value,
      isFocused,
    };
    return (
      <div id={PARENT_ELEMENT_ID} onMouseDown={this.onSearchBoxClick}>
        <Input values={inputValues} handlers={this.inputHandlers} />
        {
          inputValues.isFocused &&
          <Results busy={isBusy}
                   isFirstSearch={isFirstSearch}
                   searchValue={inputValues.value}
                   results={searchResults}
                   activeHeader={activeHeader}
                   onActiveHeaderChange={this.onActiveHeaderChange}
                   onItemClick={this.onItemClick}
                   onLoadMore={this.onLoadMore} />
        }
      </div>
    );
  }
}

SearchBox.propTypes = {
  values: PropTypes.object,
  handlers: PropTypes.object,
};

export default SearchBox;
