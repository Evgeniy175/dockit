import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class SearchBoxInput extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { value, isFocused, autoFocus } = this.props.values;
    const { onChange, onKeyDown } = this.props.handlers;
    const isValueExists = value && value.length > 0;
    const searchBoxClassName = `search-box${isFocused || isValueExists ? ' no-background-image' : ''}`;
    return <input id='searchBox' className={searchBoxClassName} type='text' autoFocus={autoFocus} placeholder='Search' value={value} onChange={onChange} onKeyDown={onKeyDown} />;
  }
}

SearchBoxInput.propTypes = {
  values: PropTypes.shape({
    value: PropTypes.string.isRequired,
    isFocused: PropTypes.bool.isRequired,
    autoFocus: PropTypes.bool,
  }).isRequired,
  handlers: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    onKeyDown: PropTypes.func.isRequired,
  }).isRequired,
};

export default SearchBoxInput;
