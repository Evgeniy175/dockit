import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Headers from './headers/index.jsx';
import SearchResultItems from './items/index.jsx';



@observer
class SearchResultsContainer extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { activeHeader, searchValue, results, busy, onActiveHeaderChange, onLoadMore, onItemClick } = this.props;
    return (
      <div className='search-results'>
        <Headers activeHeader={activeHeader} onClick={onActiveHeaderChange} />
        <SearchResultItems busy={busy} searchValue={searchValue} type={activeHeader} searchResults={results} onLoadMore={onLoadMore} onItemClick={onItemClick} />
      </div>
    );
  }
}

SearchResultsContainer.propTypes = {
  busy: PropTypes.bool,
  results: PropTypes.object,
  searchValue: PropTypes.string,
  activeHeader: PropTypes.string.isRequired,
  onActiveHeaderChange: PropTypes.func.isRequired,
  onItemClick: PropTypes.func.isRequired,
  onLoadMore: PropTypes.func.isRequired
};

export default SearchResultsContainer;
