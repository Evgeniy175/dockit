import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';

import Header from './header/index.jsx';

import { HEADERS } from '../constants';

@observer
class SearchResultsHeaders extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler(value) {
    this.props.onClick(value);
  }
  
  render() {
    const activeHeader = this.props.activeHeader;
    const keys = Object.keys(HEADERS);
    
    return (
      <Grid className='search-results-header no-select' fluid={true}>
        <Row>
          { keys.map(key => <Header key={HEADERS[key]} header={HEADERS[key]} active={HEADERS[key] === activeHeader} onClick={this.onClick} />) }
        </Row>
      </Grid>
    );
  }
}

SearchResultsHeaders.propTypes = {
  activeHeader: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default SearchResultsHeaders;
