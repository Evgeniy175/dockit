import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import { Col } from 'react-bootstrap';
import PropTypes from 'prop-types';

@observer
class SearchResultsHeader extends Component {
  constructor(props) {
    super(props);
  }
  
  componentWillMount() {
    this.onClick = ::this.onClickHandler;
  }
  
  onClickHandler(value) {
    this.props.onClick(value);
  }
  
  render() {
    const header = this.props.header;
    const active = this.props.active;
    const className = `search-results-header-item${active ? ' active' : ''}`;
    
    return <Col className={className} xs={4} onClick={() => this.onClick(header)}>{header}</Col>;
  }
}

SearchResultsHeader.propTypes = {
  header: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default SearchResultsHeader;
