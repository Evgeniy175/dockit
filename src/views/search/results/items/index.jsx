import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { get, isEmpty } from 'lodash';

import DefaultSearchResults from '../../../../shared-components/search/default-search-results/index.jsx';
import EmptySearchResults from '../../../../shared-components/search/empty-search-results/index.jsx';
import CenteredLoader from '../../../../shared-components/loader/index.jsx';
import Loader from '../../../../shared-components/loader-bottom/index.jsx';
import UsersList from './users/index.jsx';
import CreationsList from './creations/index.jsx';

import LoadMoreHelper from '../../../../utils/dom/events/scroll-helper';
import { getDirection } from '../../../../utils/mouse/wheel';

import { HEADERS } from '../constants';
import { DIRECTIONS } from '../../../../utils/mouse/wheel/constants';
import { EVENTS } from '../../../../utils/dom/events/scroll-helper/constants';



@observer
class SearchResultsItems extends Component {
  LISTS = {
    [HEADERS.USERS]: users => <UsersList users={users} onSelect={this.props.onItemClick} />,
    [HEADERS.EVENTS]: events => <CreationsList events={events} type={HEADERS.EVENTS} onSelect={this.props.onItemClick} />,
    [HEADERS.SCHEDULES]: schedules => <CreationsList events={schedules} type={HEADERS.SCHEDULES} onSelect={this.props.onItemClick} />
  };

  loadMoreHelper;

  componentDidUpdate() {
    if (this.loadMoreHelper) return;
    const onWheel = ::this.onWheelHandler;
    const container = document.getElementsByClassName('search-results-items')[0];
    if (!container) return;
    this.loadMoreHelper = new LoadMoreHelper({
      handlers: {
        [EVENTS.WHEEL.name]: onWheel,
        [EVENTS.TOUCH_MOVE.name]: onWheel,
        [EVENTS.KEY_DOWN.name]: onWheel,
      },
      container,
      bottomOffset: container.offsetHeight * 5
    });
  }
  
  onWheelHandler(e) {
    const { searchResults, busy } = this.props;
    const direction = getDirection(e);
    if (busy || !searchResults.nextPage || direction !== DIRECTIONS.DOWN) return;
    this.props.onLoadMore();
  }

  render() {
    const { searchResults, searchValue, busy, isFirstSearch, type } = this.props;
    const isAnyResults = get(searchResults, 'data.length', 0) > 0;
    const isSearchPerformed = !isEmpty(searchResults);

    if (busy && isFirstSearch) return <Loader />;

    if (!isSearchPerformed || (!busy && !searchValue && !isAnyResults)) return <DefaultSearchResults />;
    if (!isAnyResults) return <EmptySearchResults />;
    if (!this.LISTS[type]) throw new Error('Search list type not supported');
    
    return (
      <div className='search-results-items'>
        { this.LISTS[type](searchResults) }
        { busy && <Loader /> }
      </div>
    );
  }
}

SearchResultsItems.propTypes = {
  busy: PropTypes.bool,
  isFirstSearch: PropTypes.bool,
  searchResults: PropTypes.object,
  searchValue: PropTypes.string,
  type: PropTypes.string.isRequired,
  onLoadMore: PropTypes.func.isRequired,
  onItemClick: PropTypes.func.isRequired,
};

export default SearchResultsItems;
