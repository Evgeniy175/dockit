import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import User from './user/index.jsx';

import UsersModel from '../../../../../models/users';

import { RESOLUTION } from './constants';



@observer
class SearchResultsUsers extends Component {
  render() {
    const users = this.props.users.data;
    return (
      <div className='search-results-container users-results'>
        { users.map(user => <User key={user.data.id}
                                  imgSrc={this.getImgSrc(user)}
                                  user={user}
                                  onSelect={this.props.onSelect} />) }
      </div>
    );
  }

  getImgSrc(user) {
    return UsersModel.formatImageUrl(user.data.profilePicture, RESOLUTION);
  }
}

SearchResultsUsers.propTypes = {
  users: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SearchResultsUsers;
