import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Link, browserHistory } from 'react-router';
import {get} from 'lodash';
import PropTypes from 'prop-types';

import Image from '../../../../../../shared-components/image/index.jsx';
import Content from './content/index.jsx';
import Menu from '../../../../../../shared-components/users/menu/index.jsx';

import { Auth } from '../../../../../../auth';
import {isTargetIcon} from '../../../../../../utils/dom/users-menu-helpers';

import { WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../../../../../shared-components/image/constants';



@observer
class SearchResultsUser extends Component {
  constructor(props) {
    super(props);
    this.onLinkClick = ::this.onLinkClickHandler;
  }

  onLinkClickHandler(e) {
    if (isTargetIcon(e)) return;
    this.props.onSelect();
    const user = this.props.user;
    const id = user.data.id;
    browserHistory.push(`/users/${id}`);
  }
  
  render() {
    const { user, imgSrc } = this.props;
    const follows = user.follows;
    const id = user.data.id;
    const name = user.data.name;
    const currUserId = Auth.getActiveUser().id;
    const isCurrUser = id === currUserId;

    const imageData = {
      values: {
        src: imgSrc,
        wrapperClassName: WRAPPER_CLASS_NAME,
        className: IMAGE_CLASS_NAME,
        aspectRatio: ASPECT_RATIOS.s1x1,
      },
    };

    const usersMenuValues = {
      userId: id,
      personName: name,
      isUserPrivate: !user.data.public,
      isFollowed: !!user.follows.mine,
      isAccepted: !!get(user, 'follows.mine.accepted'),
    };

    return (
      <div className='search-results-item no-select'>
        <Link className='search-results-item-link-wrapper' onClick={this.onLinkClick}>
          <Image values={imageData.values} />
          <Content user={user.data} />
          { !isCurrUser && <Menu values={usersMenuValues} /> }
        </Link>
      </div>
    );
  }
}

SearchResultsUser.propTypes = {
  user: PropTypes.object.isRequired,
  imgSrc: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SearchResultsUser;
