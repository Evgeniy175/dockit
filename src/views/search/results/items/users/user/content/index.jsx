import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class SearchResultsUserContent extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const { user } = this.props;
    return (
      <div className='search-results-item-data'>
        <div className='top'>{ user.name }</div>
        <div className='bottom'>{ `@${user.username}` }</div>
      </div>
    );
  }
}

SearchResultsUserContent.propTypes = {
  user: PropTypes.object.isRequired,
};

export default SearchResultsUserContent;
