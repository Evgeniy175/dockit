import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';



@observer
class SearchResultsEventContent extends Component {
  render() {
    const { title, date } = this.props;
    
    return (
      <div className='search-results-item-data'>
        <div className='top'>{ title }</div>
        { date && <div className='bottom'>{ date }</div> }
      </div>
    );
  }
}

SearchResultsEventContent.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string,
};

export default SearchResultsEventContent;
