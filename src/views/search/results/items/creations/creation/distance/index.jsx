import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import { metersToMiles } from '../../../../../../../utils/converters/distance';

import { MEASURE_UNIT } from './constants';



@observer
class SearchResultsCreationDistance extends Component {
  @observable distance = null;

  constructor(props) {
    super(props);
    this.init(props);
  }

  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  init(props) {
    const { creation } = props;
    let distance = creation && creation.location && creation.location.distance ? creation.location.distance : null;
    if (!distance) return;
    distance = metersToMiles(distance);
    distance = distance > 10 ? parseInt(distance) : parseFloat(distance).toFixed(2);
    this.setDistance(distance);
  }

  @action
  setDistance(value = null) {
    this.distance = value;
  }

  render() {
    return this.distance ? <span className='search-results-creation-distance'>{this.distance}{MEASURE_UNIT}</span> : null;
  }
}

SearchResultsCreationDistance.propTypes = {
  creation: PropTypes.object.isRequired,
};

export default SearchResultsCreationDistance;
