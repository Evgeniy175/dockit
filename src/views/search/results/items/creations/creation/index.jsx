import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Link, browserHistory } from 'react-router';
import moment from 'moment';

import { DATE_FORMATTER, WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { HEADERS } from '../../../constants';
import { TYPES } from '../../../../../creations/constants';
import { ASPECT_RATIOS } from '../../../../../../shared-components/image/constants';


import Image from '../../../../../../shared-components/image/index.jsx';
import Content from './content/index.jsx';
import Distance from './distance/index.jsx';



@observer
class SearchResultsEvent extends Component {
  linkModelResolver = {
    [HEADERS.EVENTS]: TYPES.EVENT,
    [HEADERS.SCHEDULES]: TYPES.SCHEDULE,
  };

  constructor(props) {
    super(props);
    this.onLinkClick = ::this.onLinkClickHandler;
  }

  onLinkClickHandler(e) {
    let element = e.target;
    let isDockIcon = element.classList.contains('dock');

    while (!isDockIcon && element && element.parentElement) {
      element = element.parentElement;
      isDockIcon = element.classList.contains('dock')
    }

    if (isDockIcon) return;
    this.props.onSelect();
    const { type } = this.props;
    const event = this.props.data;
    browserHistory.push(`/${this.linkModelResolver[type]}/${event.id}`);
  }
  
  render() {
    const { imgSrc } = this.props;
    const event = this.props.data;
    const date = moment(event.startTime).format(DATE_FORMATTER);
    const imageData = {
      values: {
        src: imgSrc,
        wrapperClassName: WRAPPER_CLASS_NAME,
        className: IMAGE_CLASS_NAME,
        aspectRatio: ASPECT_RATIOS.s1x1,
      },
    };
    return (
      <div className='search-results-item no-select'>
        <Link className='search-results-item-link-wrapper' onClick={this.onLinkClick}>
          <Image values={imageData.values} />
          <Content title={event.title} date={date} />
          <Distance creation={event} />
        </Link>
      </div>
    );
  }
}

SearchResultsEvent.propTypes = {
  data: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SearchResultsEvent;
