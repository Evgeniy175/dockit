import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Creation from './creation/index.jsx';

import EventsModel from '../../../../../models/events';
import SchedulesModel from '../../../../../models/schedules';

import { RESOLUTION } from './constants';
import { TYPES } from '../../../../creations/constants';



@observer
class SearchResultsEvents extends Component {
  modelResolver = {
    [TYPES.EVENT]: EventsModel,
    [TYPES.SCHEDULE]: SchedulesModel,
  };

  render() {
    const { type } = this.props;
    const events = this.props.events.data;

    return (
      <div className='search-results-container creations-results'>
        { events.map(event => <Creation key={event.id} data={event} imgSrc={this.getImgSrc(event)} type={type} onSelect={this.props.onSelect} />) }
      </div>
    );
  }

  getImgSrc(event) {
    const { type } = this.props;
    return this.modelResolver[type.toLowerCase()].formatImageUrl(event.image, undefined, RESOLUTION);
  }
}

SearchResultsEvents.propTypes = {
  events: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SearchResultsEvents;
