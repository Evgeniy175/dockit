export const PARENT_ELEMENT_ID = 'search-block';
export const MOBILE_PARENT_ELEMENT_ID = 'mobile-search-block';
export const ENTER_KEY_CODE = 13;
export const LOAD_BUFFER_FROM_BOTTOM = 20;
export const LIMIT = 20;
export const DELAY = 1000;
export const USERNAME_PREFIX = '@';
