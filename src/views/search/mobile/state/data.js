import { action, observable, computed, toJS } from 'mobx';
import { get } from 'lodash';

import { LIMIT, USERNAME_PREFIX } from '../../constants';
import { HEADERS } from '../../results/constants';

import DataState from '../../../../utils/state/data';

import UserModel from '../../../../models/users';
import EventModel from '../../../../models/events';
import ScheduleModel from '../../../../models/schedules';

const userModel = new UserModel();
const eventModel = new EventModel();
const scheduleModel = new ScheduleModel();



class SearchBlockMobileDataState extends DataState {
  requestResolvers = {
    [HEADERS.USERS]: (searchQuery, page) => userModel.search(searchQuery, LIMIT, page),
    [HEADERS.EVENTS]: (searchQuery, page) => eventModel.search(searchQuery, LIMIT, page),
    [HEADERS.SCHEDULES]: (searchQuery, page) => scheduleModel.search(searchQuery, LIMIT, page)
  };
  
  resultSetterResolvers = {
    [HEADERS.USERS]: data => this.setUsers(data),
    [HEADERS.EVENTS]: data => this.setEvents(data),
    [HEADERS.SCHEDULES]: data => this.setSchedules(data)
  };
  
  resultAttachResolvers = {
    [HEADERS.USERS]: data => this.attachUsers(data),
    [HEADERS.EVENTS]: data => this.attachEvents(data),
    [HEADERS.SCHEDULES]: data => this.attachSchedules(data)
  };
  
  resultsResolvers = {
    [HEADERS.USERS]: () => toJS(this.users),
    [HEADERS.EVENTS]: () => toJS(this.events),
    [HEADERS.SCHEDULES]: () => toJS(this.schedules)
  };
  
  @observable isBusy = false;
  @observable value = '';
  @observable isFocused = false;
  
  @observable activeHeader = HEADERS.USERS;

  isValueChanged = false;
  @observable users = {};
  @observable events = {};
  @observable schedules = {};

  constructor(props = {}) {
    super(props);
  }

  doSearch() {
    const header = this.activeHeader;
    const value = this.getValue();
    const searchQuery = `%${value}%`;
    let isValueChanged = false;

    if (!value || value === '') {
      this.resultSetterResolvers[header]({});
      return this.setBusy(false);
    }
    if (!this.requestResolvers[header]) throw new Error('Search request handler wrong');
    if (!this.resultSetterResolvers[header]) throw new Error('Search results setter handler wrong');
    if (this.isValueChanged) this.clearResults();

    const page = get(this.resultsResolvers[header](), 'nextPage', 1);
    const resolver = page == 1 ? this.resultSetterResolvers : this.resultAttachResolvers;

    this.setBusy(true);

    return this.requestResolvers[header](searchQuery, page)
    .then(res => {
      isValueChanged = value !== this.getValue();
      if (isValueChanged) return Promise.resolve();
      return resolver[header](res);
    })
    .finally(() => { if (!isValueChanged) this.setBusy(false); });
  }

  getValue() {
    if (!this.value || this.activeHeader !== HEADERS.USERS || this.value.length === 0) return this.value;
    return this.value[0] === USERNAME_PREFIX ? this.value.substring(1) : this.value;
  }
  
  @action
  setBusy(value) {
    this.isBusy = value;
  }
  
  @action
  setActiveHeader(value) {
    this.activeHeader = value;
    this.doSearch();
  }
  
  @action
  setValue(value) {
    this.isValueChanged = value !== this.value;
    this.value = value;
  }

  clearResults() {
    this.setUsers({});
    this.setEvents({});
    this.setSchedules({});
    this.isValueChanged = false;
  }
  
  @action
  setIsFocused(value) {
    this.isFocused = value;
    
    if (!this.isFocused) this.surfaceClear();
  }
  
  @action
  setUsers(value) {
    this.users = value;
  }
  
  @action
  setEvents(value) {
    this.events = value;
  }
  
  @action
  setSchedules(value) {
    this.schedules = value;
  }
  
  @action
  attachUsers(value) {
    const currData = get(this.users, 'data', null);
  
    if (!currData) return;
    
    this.users = {
      data: this.users.data.concat(value.data),
      nextPage: value.nextPage,
      maxPage: value.maxPage,
    };
  }
  
  @action
  attachEvents(value) {
    const currData = get(this.events, 'data', null);
    
    if (!currData) return;
    
    this.events = {
      data: this.events.data.concat(value.data),
      nextPage: value.nextPage,
      maxPage: value.maxPage,
    };
  }
  
  @action
  attachSchedules(value) {
    const currData = get(this.schedules, 'data', null);
  
    if (!currData) return;
    
    this.schedules = {
      data: this.schedules.data.concat(value.data),
      nextPage: value.nextPage,
      maxPage: value.maxPage,
    };
  }
  
  getSearchResults() {
    return this.resultsResolvers[this.activeHeader] ? this.resultsResolvers[this.activeHeader]() : {};
  }

  @action
  clear() {
    this.isBusy = false;
    this.activeHeader = HEADERS.USERS;
    this.users = {};
    this.events = {};
    this.schedules = {};
  }

  @action
  surfaceClear() {
    this.isBusy = false;
  }
}

export default SearchBlockMobileDataState;
