import { HEADERS } from '../results/constants';

export const TABS = [
  {
    key: HEADERS.USERS
  },
  {
    key: HEADERS.EVENTS
  },
  {
    key: HEADERS.SCHEDULES
  },
];

export const SWIPE_EVENT_DELTA = 20;
