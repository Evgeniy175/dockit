import React, { Component } from 'react';
import { action, observable, toJS } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';

import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';
import { getRandomInt } from '../../../utils/random';
import SwipeHandler from '../../../utils/swipe';
import jq from '../../../utils/jquery';

import Results from '../results/index.jsx';
import Input from '../box/input/index.jsx';

import { TABS, SWIPE_EVENT_DELTA } from './constants';
import { MOBILE_PARENT_ELEMENT_ID, DELAY } from '../constants';
import { KEY_CODES } from '../../../constants';
import { HANDLERS, DIRECTIONS } from '../../../utils/swipe/constants';

import DataState from './state/data';

import SearchIcon from '../../../assets/images/search/search-icon.png';

const swipeHandler = new SwipeHandler();



@observer
class SearchBoxMobile extends Component {
  swipeHandlers;
  isSwipeEnded = true;

  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.SEARCH]());
  }
  
  componentWillMount() {
    this.onActiveHeaderChange = ::this.onActiveHeaderChangeHandler;
    this.onSearchBoxClick = ::this.onSearchBoxClickHandler;
    this.onSearchClose = ::this.onSearchCloseHandler;
    this.onItemClick = ::this.onItemClickHandler;
    this.onLoadMore = ::this.onLoadMoreHandler;
    this.doSearch = ::this.doSearchHandler;

    this.inputHandlers = {
      onChange: ::this.onChangeHandler,
      onKeyDown: ::this.onKeyDownHandler,
    };

    this.swipeHandlers = {
      [HANDLERS[DIRECTIONS.RIGHT]]: ::this.onSwipeRightHandler,
      [HANDLERS[DIRECTIONS.LEFT]]: ::this.onSwipeLeftHandler,
      [HANDLERS.END]: ::this.onEndHandler,
    };
  }
  
  componentDidMount() {
    const body = document.body;
    body.addEventListener('click', this.onSearchClose, false);

    const container = document.getElementById('mobile-search-wrapper');
    swipeHandler.init(container, this.swipeHandlers, SWIPE_EVENT_DELTA);
  }

  componentWillUnmount() {
    swipeHandler.dispose();
  }

  onSwipeRightHandler() {
    if (!this.isSwipeEnded) return;
    const activeTabIdx = this.getActiveTabIdx();
    if (activeTabIdx === 0) return;
    this.isSwipeEnded = false;
    this.data.setActiveHeader(TABS[activeTabIdx - 1].key);
  }

  onSwipeLeftHandler() {
    if (!this.isSwipeEnded) return;
    const activeTabIdx = this.getActiveTabIdx();
    if (activeTabIdx === TABS.length - 1) return;
    this.isSwipeEnded = false;
    this.data.setActiveHeader(TABS[activeTabIdx + 1].key);
  }

  onEndHandler() {
    this.isSwipeEnded = true;
  }

  getActiveTabIdx() {
    return TABS.findIndex(tab => tab.key === this.data.activeHeader);
  }
  
  doForceExpand() {
    this.data.setIsFocused(true);
    setTimeout(() => jq.wrap('#searchBox').focus(), 0);
  }
  
  componentWillUnmount() {
    this.data.clear();
  
    const body = document.body;
    body.removeEventListener('click', this.onSearchClose, false);
  }
  
  onChangeHandler(e) {
    this.data.setValue(e.target.value);
  }
  
  onKeyDownHandler(e) {
    const { keyCode } = e;
    if (keyCode === KEY_CODES.ENTER) return this.doSearch();
    if ([KEY_CODES.PAGE_DOWN, KEY_CODES.PAGE_UP].some(code => code === keyCode)) return;
    this.data.searchId = getRandomInt();
    setTimeout(this.doSearch, DELAY, this.data.searchId);
  }

  doSearchHandler(searchId) {
    if (searchId && searchId !== this.data.searchId) return;
    this.data.doSearch();
    this.data.searchId = null;
  }
  
  onActiveHeaderChangeHandler(value) {
    this.data.setActiveHeader(value);
    this.doSearch();
  }
  
  onSearchBoxClickHandler() {
    this.data.setIsFocused(true);
  }
  
  onSearchCloseHandler(e) {
    let element = e.target;
    let isNeedToClose = element.id !== MOBILE_PARENT_ELEMENT_ID;

    while (isNeedToClose && element && element.parentElement) {
      element = element.parentElement;
      isNeedToClose = element.id !== MOBILE_PARENT_ELEMENT_ID && element.id !== 'question-modal';
    }

    if (isNeedToClose) {
      this.data.setIsFocused(false);
      this.data.surfaceClear();
    }
  }
  
  onItemClickHandler() {
    this.data.setIsFocused(false);
  }
  
  onLoadMoreHandler() {
    this.doSearch();
  }
  
  render() {
    const isBusy = toJS(this.data.isBusy);
  
    const activeHeader = toJS(this.data.activeHeader);
    const searchResults = this.data.getSearchResults();

    const inputValues = {
      value: toJS(this.data.value),
      isFocused: toJS(this.data.isFocused),
      autoFocus: true,
    };

    return (
      <Grid id='mobile-search-wrapper'>
        <Row>
          <Col xs={12} smOffset={2} sm={8} mdOffset={3} md={6}>
            <div id={MOBILE_PARENT_ELEMENT_ID} onMouseDown={this.onSearchBoxClick}>
              <div className='top-wrapper'>
                <img className='search-icon' src={SearchIcon} />
                <Input values={inputValues} handlers={this.inputHandlers} />
              </div>
              <Results busy={isBusy}
                       searchValue={inputValues.value}
                       results={searchResults}
                       activeHeader={activeHeader}
                       onActiveHeaderChange={this.onActiveHeaderChange}
                       onItemClick={this.onItemClick}
                       onLoadMore={this.onLoadMore} />
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }
}

SearchBoxMobile.propTypes = {
  handlers: PropTypes.object,
  values: PropTypes.object
};

export default SearchBoxMobile;
