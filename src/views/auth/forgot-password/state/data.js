import { action, observable, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import { isEmpty } from 'lodash';

import { FIELDS, ERRORS } from '../constants';

import DataState from '../../../../utils/state/data';



class ViewForgotPasswordDataState extends DataState {
  @observable validation = {};
  @observable email = '';
  
  constructor(props = {}) {
    super(props);
  }
  
  forgotPassword() {
    return Auth.forgotPassword(this.email);
  }
  
  isFormValid() {
    this.clearValidation();
    
    if (!this.email || this.email.length === 0) this.addError(FIELDS.EMAIL);
  
    return isEmpty(toJS(this.validation));
  }
  
  @action
  addError(key) {
    this.validation[key] = ERRORS[key];
    this.validation = toJS(this.validation);
  }
  
  @action
  addSummaryError(value) {
    this.validation.summary = this.validation.summary || [];
    this.validation.summary.push(value);
    this.validation = toJS(this.validation);
  }
  
  @action
  setEmail(value) {
    this.email = value.toLowerCase();
  }
  
  @action
  clearValidation(key) {
    if (key) {
      delete this.validation[key];
      this.validation = toJS(this.validation);
      return;
    }
    
    this.validation = {};
  }
}

export default ViewForgotPasswordDataState;
