import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { get, isEmpty } from 'lodash';

import { setTitle } from '../../../utils/formatting/title';

import ValidationSummary from '../../../shared-components/validation-summary/index.jsx';
import Input from '../../../shared-components/validation-input/index.jsx';
import Button from '../../../shared-components/button/index.jsx';
import KeyIcon from '../../../shared-components/key-icon/index.jsx';

import { FIELDS, ERRORS } from './constants';
import { CSS_MAPPING } from '../../../shared-components/button/constants';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import DataState from './state/data';



@observer
class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.FORGOT_PASSWORD]());
  }
  
  componentWillMount() {
    this.onEmailChange = ::this.onEmailChangeHandler;
    this.onSubmit = ::this.onSubmitHandler;
  }
  
  onEmailChangeHandler(value) {
    this.data.setEmail(value);
    this.data.clearValidation(FIELDS.EMAIL);
  }
  
  onSubmitHandler(e) {
    e.preventDefault();
    
    if (!this.data.isFormValid()) return;
    
    this.data.forgotPassword()
    .catch(err => {
      const message = get(err, 'res.text', ERRORS[FIELDS.SUMMARY]);
      this.data.addSummaryError(message);
    });
  }
  
  render() {
    const errors = toJS(this.data.validation);
    const value = this.data.email;
    const validationSummary = errors.summary;
    const isSummaryExists = !isEmpty(validationSummary);
    
    return (
      <div className='forgot-password-wrapper'>
        <Grid className='forgot-password'>
          {
            isSummaryExists && <ValidationSummary errors={validationSummary} />
          }
          <Row>
            <Col xs={12} smOffset={2} sm={8} mdOffset={3} md={6} lgOffset={4} lg={4}>
              <form>
                <KeyIcon />
                <div className='text-wrapper'>
                  Enter the email address associated with your account and we will shoot you an email with reset information.
                </div>
                <Input inputType='email' value={value} error={errors[FIELDS.EMAIL]} placeholder='Email' onChange={this.onEmailChange} />
                <div className='buttons'>
                  <Button type='submit' text='Reset' className={CSS_MAPPING.BLUE_FILLED} onClick={this.onSubmit} />
                </div>
              </form>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ForgotPassword;
