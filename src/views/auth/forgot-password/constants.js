export const FIELDS = {
  EMAIL: 'email'
};

export const ERRORS = {
  [FIELDS.EMAIL]: 'Please enter valid email'
};
