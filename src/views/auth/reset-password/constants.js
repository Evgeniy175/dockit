export const FIELDS = {
  PASSWORD: 'password',
  CONFIRM_PASSWORD: 'confirm password',
  SUMMARY: 'Error'
};

export const ERRORS = {
  [FIELDS.PASSWORD]: 'Please enter valid password',
  [FIELDS.CONFIRM_PASSWORD]: 'Passwords are not equals'
};