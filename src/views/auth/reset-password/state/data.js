import { action, observable, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import { isEmpty } from 'lodash';

import { FIELDS, ERRORS } from '../constants';

import DataState from '../../../../utils/state/data';



class ViewResetPasswordDataState extends DataState {
  @observable validation = {};
  
  @observable code = '';
  @observable email = '';
  @observable password = '';
  @observable confirmPassword = '';
  
  constructor(props = {}) {
    super(props);
  }

  init(props) {
    this.setEmail(props.email);
    this.setCode(props.code);
  }
  
  resetPassword() {
    return Auth.resetPassword(this.email, this.code, this.password, this.confirmPassword);
  }
  
  isFormValid() {
    this.clearValidation();
    
    if (!this.password || this.password.length == 0 || !/^(?=.*\d)(?=.*[a-zA-Z])(\w+){6,}$/.test(this.password)) this.addError(FIELDS.PASSWORD);
    if (!this.confirmPassword || this.password !== this.confirmPassword) this.addError(FIELDS.CONFIRM_PASSWORD);
    
    return isEmpty(toJS(this.validation))
  }
  
  @action
  addError(key) {
    this.validation[key] = ERRORS[key];
    this.validation = toJS(this.validation);
  }
  
  @action
  addSummaryError(value) {
    this.validation.summary = this.validation.summary || [];
    this.validation.summary.push(value);
    this.validation = toJS(this.validation);
  }
  
  @action
  setCode(value) {
    this.code = value;
  }
  
  @action
  setEmail(value) {
    this.email = value.toLowerCase();
  }
  
  @action
  setPassword(value) {
    this.password = value;
  }
  
  @action
  setConfirmPassword(value) {
    this.confirmPassword = value;
  }
  
  @action
  clearValidation(key) {
    if (key) {
      delete this.validation[key];
      this.validation = toJS(this.validation);
      return;
    }
    
    this.validation = {};
  }
}

export default ViewResetPasswordDataState;
