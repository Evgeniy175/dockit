import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { get, isEmpty } from 'lodash';
import queryString from 'query-string';
import { Grid, Row, Col } from 'react-bootstrap';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import ValidationSummary from '../../../shared-components/validation-summary/index.jsx';
import Input from '../../../shared-components/validation-input/index.jsx';
import Button from '../../../shared-components/button/index.jsx';
import KeyIcon from '../../../shared-components/key-icon/index.jsx';

import { FIELDS, ERRORS } from './constants';
import { CSS_MAPPING } from '../../../shared-components/button/constants';
import { SUCCESS_PASSWORD_RESET_PATH } from '../../../constants';

import { getLocation } from '../../../utils/dom/window';

import DataState from './state/data';



@observer
class ResetPassword extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.RESET_PASSWORD]());
    this.doInit();

    this.onPasswordChange = ::this.onPasswordChangeHandler;
    this.onConfirmPasswordChange = ::this.onConfirmPasswordChangeHandler;
    this.onSubmit = ::this.onSubmitHandler;
  }

  componentWillUpdate() {
    this.doInit();
  }

  doInit() {
    const query = queryString.extract(getLocation().href);
    this.data.init(queryString.parse(query));
  }
  
  onPasswordChangeHandler(value) {
    this.data.setPassword(value);
    this.data.clearValidation(FIELDS.PASSWORD);
  }
  
  onConfirmPasswordChangeHandler(value) {
    this.data.setConfirmPassword(value);
    this.data.clearValidation(FIELDS.CONFIRM_PASSWORD);
  }
  
  onSubmitHandler(e) {
    e.preventDefault();
    
    if (!this.data.isFormValid()) return;

    this.data.resetPassword()
    .then(() => browserHistory.push(SUCCESS_PASSWORD_RESET_PATH))
    .catch(err => {
      const message = get(err, 'res.text', ERRORS[FIELDS.SUMMARY]);
      this.data.addSummaryError(message);
    });
  }
  
  render() {
    const errors = toJS(this.data.validation);
    const password = this.data.password;
    const confirmPassword = this.data.confirmPassword;
    const validationSummary = errors.summary;
    const isSummaryExists = !isEmpty(validationSummary);
    
    return (
      <div className='reset-password-wrapper'>
        <Grid className='reset-password'>
          <Row>
            <Col xs={12} smOffset={2} sm={8} mdOffset={3} md={6} lgOffset={4} lg={4}>
              <KeyIcon />
              <div className='text'>
                Enter a new password for your account
              </div>
              {
                isSummaryExists && <ValidationSummary errors={validationSummary} />
              }
              <form>
                <Input inputType='password' value={password} error={errors[FIELDS.PASSWORD]} placeholder='New Password' onChange={this.onPasswordChange} />
                <Input inputType='password' value={confirmPassword} error={errors[FIELDS.CONFIRM_PASSWORD]} placeholder='Confirm Password' onChange={this.onConfirmPasswordChange} />
                <div className='buttons'>
                  <Button type='submit' text='Reset Password' className={CSS_MAPPING.BLUE_FILLED} onClick={this.onSubmit} />
                </div>
              </form>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default ResetPassword;
