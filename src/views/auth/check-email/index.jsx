import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import { REDIRECT_DELAY_MS } from './constants';
import { LOGIN_PATH } from '../../../constants';



@observer
class CheckEmail extends Component {
  constructor(props) {
    super(props);
    setTitle(PAGE_TITLES[PAGES.CHECK_EMAIL]());
  }
  
  componentDidMount() {
    setTimeout(() => { browserHistory.push(LOGIN_PATH); }, REDIRECT_DELAY_MS);
  }

  render() {
    return (
      <div className='check-email-wrapper'>
        <div className='check-email'>
          <div className='text'>
            Link to reset your password has been sent.
          </div>
          <div className='dark-text'>
            Check your email please.
          </div>
        </div>
      </div>
    );
  }
}

export default CheckEmail;
