import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { browserHistory } from 'react-router';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import { REDIRECT_DELAY_MS } from './constants';
import { LOGIN_PATH } from '../../../constants';



@observer
class ResetPasswordSuccess extends Component {
  constructor(props) {
    super(props);
    setTitle(PAGE_TITLES[PAGES.SUCCESS_PASSWORD_RESET]());
  }
  
  componentDidMount() {
    setTimeout(() => { browserHistory.push(LOGIN_PATH); }, REDIRECT_DELAY_MS);
  }

  render() {
    return (
      <div className='success-password-reset-wrapper'>
        <div className='success-password-reset'>
          <h3 className='success'>
            Success!
          </h3>
          <div className='text'>
            Your password has been changed.
          </div>
        </div>
      </div>
    );
  }
}

export default ResetPasswordSuccess;
