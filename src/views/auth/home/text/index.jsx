import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';

import { DELAY_FOR_WORD } from './constants';

@observer
class Home extends Component {
  isAnimationEnabled = false;

  constructor(props) {
    super(props);
  }
  
  componentDidMount() {
    if (!this.isAnimationEnabled) return;
    const words = document.getElementsByClassName('word');
    
    for (let i = 0; i < words.length; i++)
      setTimeout(() => {
        if (!words || !words[i]) return;
        
        words[i].className = 'word fade-in-animation';
      }, i * DELAY_FOR_WORD);
  }
  
  render() {
    const wordClassName = `word${this.isAnimationEnabled ? ' invisible' : ''}`;
    return (
      <div className='never-miss-words no-select'>
        <span className={wordClassName}>NEVER </span>
        <span className={wordClassName}>MISS </span>
        <span className={wordClassName}>ANOTHER </span>
        <span className={wordClassName}>MOMENT.</span>
      </div>
    );
  }
}

export default Home;
