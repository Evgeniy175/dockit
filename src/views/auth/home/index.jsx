import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import Text from './text/index.jsx';
import YouTube from './youtube/index.jsx';
import Login from '../../../shared-components/auth/login/index.jsx';
import SocialIcons from './social-icons/index.jsx';
import DownloadButtons from '../../../shared-components/auth/login/download-button/index.jsx';

import PhonesPreview from '../../../assets/images/home-preview-phones.svg';



@observer
class Home extends Component {
  constructor(props) {
    super(props);
    setTitle(PAGE_TITLES[PAGES.HOME]());
  }

  render() {
    return (
      <Grid className='home'>
        <Row className='center-row'>
          <Text />
          <Clearfix visibleMdBlock visibleLgBlock>
            <div id='lg-wrapper'>
              <div className='lg-youtube-video-wrapper'>
                <YouTube id='lg-youtube-video' height='253px' width='450px' autoplay={false} muted={false} />
              </div>
              <div className='lg-login-wrapper'>
                <Login />
              </div>
            </div>
          </Clearfix>
          <Clearfix visibleSmBlock>
            <div className='home-sm-wrapper'>
              <Login downloadIconVisible={false} />
              <YouTube id='sm-youtube-video' autoplay={false} muted={false} />
              <div className='download-button-wrapper'>
                <DownloadButtons />
              </div>
            </div>
          </Clearfix>
          <Clearfix visibleXsBlock>
            <div className='home-xs-wrapper'>
              <img className='phones-preview' src={PhonesPreview} />
              <Login downloadIconVisible={false} />
              <YouTube id='xs-youtube-video' width='100%' height='400px' autoplay={false} muted={false} />
              <div className='download-button-wrapper'>
                <DownloadButtons />
              </div>
            </div>
          </Clearfix>
        </Row>
        <Clearfix visibleMdBlock visibleLgBlock>
          <Row className='large-download-buttons-wrapper'>
            <DownloadButtons />
          </Row>
        </Clearfix>
        <SocialIcons />
      </Grid>
    );
  }
}

export default Home;
