import React, { Component } from 'react';
import { Link } from 'react-router';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

@observer
class Icon extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    const url = this.props.url;
    const target = this.props.openInNewWindow ? '_blank' : '_self';
    const img = this.props.img;
    
    return (
      <div className='social-icon'>
        <Link to={url} target={target}>
          <img className='no-select' src={img} />
        </Link>
      </div>
    );
  }
}

Icon.propTypes = {
  img: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  openInNewWindow: PropTypes.bool
};

Icon.defaultProps = {
  openInNewWindow: true
};

export default Icon;
