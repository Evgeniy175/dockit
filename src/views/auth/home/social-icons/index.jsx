import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';
import { Row, Col } from 'react-bootstrap';

import Icon from './icon/index.jsx';

import MailIcons from '../../../../assets/images/social/mail.png';
import MediumIcons from '../../../../assets/images/social/medium.png';
import FacebookIcons from '../../../../assets/images/social/facebook.png';

@observer
class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Row className='social'>
        <Col xs={12}>
          <div>
            <Icon img={MailIcons} url='mailto:hello@dockit.me' />
            <Icon img={FacebookIcons} url='http://facebook.com/dockithq' />
            <Icon img={MediumIcons} url='https://medium.com/@DockIt' />
          </div>
        </Col>
      </Row>
    );
  }
}

export default Home;
