import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';
import PropTypes from 'prop-types';
import YouTube from 'react-youtube';

import jq from '../../../../utils/jquery';



@observer
class YouTubeContainer extends Component {
  player;

  constructor(props) {
    super(props);
    this.onReady = ::this.onReadyHandler;
  }

  onReadyHandler(event) {
    const { muted, autoplay } = this.props;

    this.player = event.target;

    if (autoplay && this.isVisible()) this.player.playVideo();
    if (muted) this.player.mute();
  }
  
  render() {
    const { id, height, width } = this.props;

    const opts = {
      height,
      width,
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 0,
      },
    };

    return (
      <div id={id} className='youtube-container no-select'>
        <YouTube videoId="ttlGzNgXv8E" opts={opts} onReady={this.onReady} />
      </div>
    );
  }

  isVisible() {
    return jq.wrap(`#${this.props.id}`).isVisible();
  }
}

YouTubeContainer.propTypes = {
  id: PropTypes.string.isRequired,
  height: PropTypes.string,
  width: PropTypes.string,
  autoplay: PropTypes.bool,
  muted: PropTypes.bool,
};

YouTubeContainer.defaultProps = {
  height: '354',
  width: '630'
};

export default YouTubeContainer;
