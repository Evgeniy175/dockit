export const USERNAME_PREFIX = '@';

export const FIELDS = {
  NAME: 'name',
  EMAIL: 'email',
  USERNAME: 'username',
  PASSWORD: 'password',
  SUMMARY: 'summary'
};

export const ERRORS = {
  [FIELDS.NAME]: 'Please enter your name',
  [FIELDS.EMAIL]: 'Please enter valid email',
  [FIELDS.USERNAME]: 'Please enter user name',
  [FIELDS.PASSWORD]: 'Please enter valid password',
  [FIELDS.SUMMARY]: 'Can\'t create user. Probably user with typed credentials exists.'
};
