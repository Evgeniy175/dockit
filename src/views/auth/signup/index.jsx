import React, { Component } from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import { Form, Grid, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router';
import { get, isEmpty } from 'lodash';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import ValidationSummary from '../../../shared-components/validation-summary/index.jsx';
import Input from '../../../shared-components/validation-input/index.jsx';
import Button from '../../../shared-components/button/index.jsx';
import { CSS_MAPPING } from '../../../shared-components/button/constants';

import { USERNAME_PREFIX, FIELDS, ERRORS } from './constants';

import DataState from './state/data';



@observer
class SignUp extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState()
    });
    setTitle(PAGE_TITLES[PAGES.SIGN_UP]());
  }
  
  componentWillMount() {
    this.onNameChange = ::this.onNameChangeHandler;
    this.onEmailChange = ::this.onEmailChangeHandler;
    this.onUsernameChange = ::this.onUsernameChangeHandler;
    this.onPasswordChange = ::this.onPasswordChangeHandler;
    this.onSubmit = ::this.onSubmitHandler;
  }
  
  onNameChangeHandler(value) {
    this.data.setName(value);
    this.data.clearValidation(FIELDS.NAME);
  }
  
  onEmailChangeHandler(value) {
    this.data.setEmail(value);
    this.data.clearValidation(FIELDS.EMAIL);
  }
  
  onUsernameChangeHandler(value) {
    this.data.setUserName(value);
    this.data.clearValidation(FIELDS.USERNAME);
  }
  
  onPasswordChangeHandler(value) {
    this.data.setPassword(value);
    this.data.clearValidation(FIELDS.PASSWORD);
  }
  
  onSubmitHandler(e) {
    e.preventDefault();
    
    if (!this.data.isFormValid()) return;
    
    this.data.doSignUp()
    .catch(err => {
      const message = get(err, 'res.text', ERRORS[FIELDS.SUMMARY]);
      this.data.addSummaryError(message);
    });
  }

  render() {
    const errors = toJS(this.data.validation);
    const userName = this.data.userName;
    const password = this.data.password;
    const name = this.data.name;
    const email = this.data.email;
    const validationSummary = errors.summary;
    const isSummaryExists = !isEmpty(validationSummary);

    return (
      <Grid className='sign-up'>
        {
          isSummaryExists && <ValidationSummary errors={validationSummary} />
        }
        <Row>
          <Col xsOffset={0} xs={12} smOffset={3} sm={6} mdOffset={4} md={4}>
            <div className='text bright'>
              Create your DockIt account
            </div>
            <Form>
              <Input inputType='text' value={name} error={errors[FIELDS.NAME]} placeholder='Full name' onChange={this.onNameChange} />
              <Input inputType='text' value={email} error={errors[FIELDS.EMAIL]} placeholder='Email' onChange={this.onEmailChange} />
              <Input inputType='text' value={userName} error={errors[FIELDS.USERNAME]} prefix={USERNAME_PREFIX} placeholder='@Username' onChange={this.onUsernameChange} />
              <Input inputType='password' value={password} error={errors[FIELDS.PASSWORD]} placeholder='Password' onChange={this.onPasswordChange} />
              <div className='buttons'>
                <Button text='Sign Up' onClick={this.onSubmit} className={CSS_MAPPING.BLUE_FILLED} type='submit' />
              </div>
            </Form>
            <div className='bottom-text'>
              <div className='text gloomy'>
                By signing up, you agree
              </div>
              <div className='text gloomy'>
                to our <Link to='/terms'>Terms</Link> & <Link to='/privacy-policy'>Privacy Policy</Link>
              </div>
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default SignUp;
