import { action, observable, toJS } from 'mobx';
import { Auth } from '../../../../auth';
import { isEmpty } from 'lodash';
import EmailValidator from 'email-validator';

import { FIELDS, ERRORS, USERNAME_PREFIX } from '../constants';

import DataState from '../../../../utils/state/data';



class ViewSignUpDataState extends DataState {
  @observable validation = [];
  
  @observable userName = '';
  @observable password = '';
  @observable name = '';
  @observable email = '';
  
  constructor(props = {}) {
    super(props);
  }
  
  doSignUp() {
    const username = this.userName.substring(USERNAME_PREFIX.length);
    return Auth.signUp(username, this.password, this.name, this.email);
  }
  
  isFormValid() {
    this.clearValidation();
    
    if (!this.name || this.name.length === 0) this.addError(FIELDS.NAME);
    if (!EmailValidator.validate(this.email)) this.addError(FIELDS.EMAIL);
    if (!this.userName || this.userName.length <= USERNAME_PREFIX.length) this.addError(FIELDS.USERNAME);
    if (!this.password || this.password.length === 0 || !/^(?=.*\d)(?=.*[a-zA-Z])(\w+){6,}$/.test(this.password)) this.addError(FIELDS.PASSWORD);
  
    return isEmpty(toJS(this.validation));
  }
  
  @action
  addError(key) {
    this.validation[key] = ERRORS[key];
    this.validation = toJS(this.validation);
  }
  
  @action
  addSummaryError(value) {
    this.validation.summary = this.validation.summary || [];
    this.validation.summary.push(value);
    this.validation = toJS(this.validation);
  }
  
  @action
  setUserName(value) {
    this.userName = value;
  }
  
  @action
  setPassword(value) {
    this.password = value;
  }
  
  @action
  setName(value) {
    this.name = value;
  }
  
  @action
  setEmail(value) {
    this.email = value.toLowerCase();
  }
  
  @action
  clearValidation(key) {
    if (key) {
      delete this.validation[key];
      this.validation = toJS(this.validation);
      return;
    }
    
    this.validation = {};
  }
}

export default ViewSignUpDataState;
