import React, { Component } from 'react';
import { observer, observable } from 'mobx-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { setTitle } from '../../../utils/formatting/title';
import { PAGES, PAGE_TITLES } from '../../../utils/formatting/constants';

import Login from '../../../shared-components/auth/login/index.jsx';



@observer
class Home extends Component {
  constructor(props) {
    super(props);
    setTitle(PAGE_TITLES[PAGES.SIGN_IN]());
  }
  
  render() {
    return (
      <Grid className='sign-in-separated'>
        <Row>
          <Col xs={12} smOffset={3} sm={6} lgOffset={4} lg={4}>
            <Login />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default Home;
