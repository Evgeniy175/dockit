import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { action, observable, useStrict } from 'mobx';
import { observer } from 'mobx-react';
import { Clearfix } from 'react-bootstrap';

import { HOME_PATH, LOGIN_PATH, SIGN_UP_PATH } from '../../constants';

import GetTheAppBar from '../../shared-components/auth/get-the-app-bar/index.jsx';

import Brand from '../../assets/images/brand_beta.png';

useStrict(true);



@observer class DockItUnauthorized extends Component {
  componentWillMount() {
    this.onBrandClick = ::this.onBrandClickHandler;
  }
  
  onBrandClickHandler() {
    browserHistory.push(HOME_PATH);
  }
  
  onLogInClickHandler() {
    browserHistory.push(LOGIN_PATH);
  }
  
  onSignUpClickHandler() {
    browserHistory.push(SIGN_UP_PATH);
  }
  
  render() {
    return (
      <div>
        <Clearfix visibleXsBlock>
          <GetTheAppBar />
        </Clearfix>
        <div className='unauth-top-container'>
          <div className='top-container'>
            <div className='top'>
              <div className='brand no-select'>
                <img src={Brand} onClick={this.onBrandClick} />
              </div>
            </div>
          </div>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default DockItUnauthorized;
