export const POST_ANSWERS_ID = 'post-answers';
export const ANSWER_FIELD_ID = 'create-comment-wrapper';

export const LIMIT = 50;
export const TEXT_AREA_ROWS_MIN = 2;
