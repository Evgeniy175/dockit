import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';

import Item from './post/index.jsx';
import Loader from '../../shared-components/loader/index.jsx';
import Answer from '../../shared-components/create-comment/index.jsx';

import DataState from './state/data';
import UiState from './state/ui';

import { Auth } from '../../auth';

import { POST_ANSWERS_ID, TEXT_AREA_ROWS_MIN } from './constants';
import { EVENTS } from '../../utils/dom/events/scroll-helper/constants';

import WheelHelper from '../../utils/dom/events/scroll-helper';



@observer
class PostDrillDown extends Component {
  constructor(props) {
    super(props);
    Object.assign(this, {
      data: new DataState(props),
      ui: new UiState(props),
    });
    this.answerHandlers = {
      onSubmit: ::this.onSubmitHandler,
      onRendered: this.ui.onAnswerFieldRendered.bind(this.ui),
    };
    this.postHandlers = {
      onReply: ::this.onPostReply,
      onDelete: ::this.onDelete,
      onRemoved: ::this.onRemoved,
    };
    const onWheel = ::this.onWheelHandler;
    this.loadMoreHelper = new WheelHelper({
      handlers: {
        [EVENTS.WHEEL.name]: onWheel,
        [EVENTS.TOUCH_MOVE.name]: onWheel,
        [EVENTS.KEY_DOWN.name]: onWheel,
      },
    });
  }

  componentWillUnmount() {
    this.data = null;
    this.ui = null;
    this.answerHandlers = null;
    this.postHandlers = null;
    this.loadMoreHelper.dispose();
    this.loadMoreHelper = null;
  }

  onSubmitHandler(message, image) {
    return this.data.reply(message, image);
  }

  onPostReply(comment) {
    this.data.setAnswerUserTags(comment, true);
  }

  onDelete(post) {
    if (post.isRoot) return this.data.prepareForDeleteRoot(true);
    this.data.deleteAnswer(post.id);
  }

  onRemoved(post) {
    if (post.isRoot) browserHistory.push('/feed');
  }

  onWheelHandler() {
    if (!this.data.isCanLoadMore || this.data.isBlocked) return;
    this.data.setIsBlocked(true);
    return this.data.fetchMore()
    .catch(console.error)
    .finally(() => this.data.setIsBlocked(false));
  }

  render() {
    const { isBlocked, isSuccess, post, answers } = this.data;
    if (isBlocked && !post) return <Loader />;
    if (!isSuccess) return null;

    const answerValues = {
      userTags: this.data.answerUserTags,
      placeholder: this.getPlaceholder(post),
      rows: TEXT_AREA_ROWS_MIN,
    };

    return (
      <div className='posts-drilldown'>
        <Item values={this.getValues(post)} handlers={this.postHandlers} />
        {
          answers.length > 0 &&
          <div id={POST_ANSWERS_ID}>
            { answers.map(answer => <Item key={answer.id} values={this.getValues(answer)} handlers={this.postHandlers} />) }
          </div>
        }
        <Answer values={answerValues} handlers={this.answerHandlers} />
      </div>
    );
  }

  getValues(answer) {
    const { isCreatedByCurrentUser } = this.data;

    return {
      post: answer,
      forceEnableDeleteAction: isCreatedByCurrentUser,
    };
  }

  getPlaceholder(post) {
    const currentUser = Auth.getActiveUser();
    return `Say something${ currentUser.id === post.user_id ? '' : ` to ${post.user.username}` }...`;
  }
}

export default PostDrillDown;
