import { toJS, action, observable, computed } from 'mobx';
import Promise from 'bluebird';

import CommentsModel from '../../../models/comments';

import { imageToFormData } from '../../../utils/formatting/image';
import DataState from '../../../utils/state/data';

import { Auth } from '../../../auth';
import { LIMIT } from '../constants';

const commentsModel = new CommentsModel();



class PostDrillDownDataState extends DataState {
  @observable isBlocked = false;
  isSuccess = true;
  isCanLoadMore;
  isCreatedByCurrentUser = false;
  @observable post;
  @observable answers = [];
  @observable answerUserTags = {};
  id;

  constructor(props = {}) {
    super(props);
    this.id = props.params.id;
    this.fetch();
  }

  fetch() {
    this.setIsBlocked(true);
    return Promise.all([
      this.fetchComment(),
      this.fetchAnswers(),
    ])
    .spread((root, answers) => this.handleSuccessFetching(root, answers))
    .catch(err => {
      this.isSuccess = false;
      console.error(err);
    })
    .finally(() => this.setIsBlocked(false));
  }

  fetchComment() {
    return commentsModel.fetchComment(this.id).catch(console.error);
  }

  fetchAnswers() {
    return new Promise(resolve => {
      return commentsModel.fetchCommentAnswers(this.id, this.getConfig())
      .then(resolve)
      .catch(console.error);
    });
  }

  handleSuccessFetching(root, answers) {
    this.setPost({ isRoot: true, ...root });
    this.setAnswers(answers.data);
    this.isSuccess = true;
    this.isCanLoadMore = answers.data.length > 0;
    return Promise.resolve();
  }

  fetchMore() {
    this.setIsBlocked(true);
    return commentsModel.fetchCommentAnswers(this.id, this.getConfig())
    .then(answers => this.handleSuccessFetchingMore(answers))
    .catch(console.error)
    .finally(() => this.setIsBlocked(false));
  }

  handleSuccessFetchingMore(answers) {
    this.appendAnswers(answers.data);
    this.isCanLoadMore = answers.data.length > 0;
    return Promise.resolve();
  }

  getConfig() {
    return {
      params: {
        offset: this.answers.length,
        limit: LIMIT,
      },
    };
  }

  reply(message, image) {
    return commentsModel.addCommentToComment(this.post.id, message)
    .then(res => Promise.all([
      Promise.resolve(res),
      this.attachImage(res.id, image)
    ]))
    .spread((item, image) => {
      item.imageUrl = image ? image.imageUrl : null;
      this.appendAnswer(item);
      this.setAnswerUserTags();
    })
    .catch(console.error);
  }

  attachImage(id, image) {
    if (!image) return Promise.resolve();
    const formData = imageToFormData(image);
    return commentsModel.attachPictureToComment(id, { body: formData });
  }

  @action
  setIsBlocked(value) {
    this.isBlocked  = value;
  }

  @action
  setPost(value) {
    this.isCreatedByCurrentUser = value ? value.target_user_id === Auth.getActiveUser().id : false;
    this.post = value;
  }

  @action
  setAnswers(value) {
    this.answers = value;
  }

  @action
  appendAnswers(value) {
    this.answers = this.answers.concat(value);
  }

  @action
  appendAnswer(value) {
    this.answers.unshift(value);
  }

  @action
  setAnswerUserTags(comment, isWithUserTags = false) {
    if (!comment || !comment.user) return this.answerUserTags = new Set();
    const commentUserTags = isWithUserTags ? comment.userTags.map(userTag => userTag.user.username) : [];
    const userTags = new Set([comment.user.username, ...commentUserTags]);
    this.answerUserTags = Array.from(userTags).filter(userTag => userTag !== Auth.getActiveUser().username);
  }

  @action
  deleteAnswer(id) {
    this.answers = this.answers.filter(answer => answer.id !== id);
  }

  @action
  prepareForDeleteRoot() {
    this.setPost(null);
    this.setIsBlocked(true);
  }
}

export default PostDrillDownDataState;
