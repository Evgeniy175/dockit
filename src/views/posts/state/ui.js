import { action, observable, toJS } from 'mobx';

import jq from '../../../utils/jquery';

import UIState from '../../../utils/state/data';

import { ANSWER_FIELD_ID, POST_ANSWERS_ID } from '../constants';



class PostDrillDownUiState extends UIState {
  get answer() {
    return jq.wrap(`#${ANSWER_FIELD_ID}`);
  }

  get answers() {
    return jq.wrap(`#${POST_ANSWERS_ID}`);
  }

  onAnswerFieldRendered() {
    const { answer, answers } = this;
    answers.css('marginBottom', answer.height());
  }
}

export default PostDrillDownUiState;
