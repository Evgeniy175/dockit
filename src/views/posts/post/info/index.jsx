import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import moment from 'moment';

import Like from '../../../../shared-components/action-command/like/index.jsx';

import { formatFromNow } from '../../../../utils/formatting/time';

import { MODEL_PLURAL } from '../../../../models/comments/constants';



@observer
class PostDrillDownItemInfo extends Component {
  constructor(props) {
    super(props);
    this.onReply = ::this.onReplyHandler;
  }

  onReplyHandler() {
    this.props.handlers.onReply(this.props.values.post);
  }

  render() {
    const { post } = this.props.values;
    const likeData = {
      type: MODEL_PLURAL,
      id: post.id,
      likes: post.decorated.likes,
    };
    return (
      <div className='posts-drilldown-item-info'>
        <div className='posts-drilldown-item-info-left'>
          <div className='post-item-reply-button' onClick={this.onReply}>Reply</div>
          <Like data={likeData} />
        </div>
        <div className='post-item-time'>{ formatFromNow(moment(post.created_at)) }</div>
      </div>
    );
  }
}

PostDrillDownItemInfo.propTypes = {
  values: PropTypes.shape({
    post: PropTypes.shape({
      isRoot: PropTypes.bool,
    }).isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onReply: PropTypes.func.isRequired,
  }).isRequired,
};

export default PostDrillDownItemInfo;
