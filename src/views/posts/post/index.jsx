import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';

import Head from './head/index.jsx';
import Info from './info/index.jsx';
import Content from './content/index.jsx';



@observer
class PostDrillDownItem extends Component {
  get post() {
    return this.props.values.post;
  }

  constructor(props) {
    super(props);
    this.headHandlers = {
      onDelete: ::this.onDelete,
      onRemoved: props.handlers.onRemoved,
    };
  }

  componentWillUnmount() {
    this.headHandlers = null;
  }

  onDelete() {
    this.props.handlers.onDelete(this.post);
  }

  render() {
    return (
      <div className={`posts-drilldown-item${this.post.isRoot ? ' posts-root' : ''}`}>
        <Head values={this.props.values} handlers={this.headHandlers} />
        <div className='right-side'>
          <Content post={this.post} />
          <Info {...this.props} />
        </div>
      </div>
    );
  }
}

PostDrillDownItem.propTypes = {
  values: PropTypes.shape({
    forceEnableDeleteAction: PropTypes.bool,
    post: PropTypes.shape({
      isRoot: PropTypes.bool,
    }).isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onReply: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onRemoved: PropTypes.func.isRequired,
  }).isRequired,
};

export default PostDrillDownItem;
