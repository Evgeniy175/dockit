import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS, observable, action } from 'mobx';
import PropTypes from 'prop-types';

import Image from '../../../../shared-components/image/index.jsx';
import ExpandableText from '../../../../shared-components/expandable-text/index.jsx';
import Lightbox from '../../../../shared-components/lightbox-wrapper/index.jsx';

import CommentsModel from '../../../../models/comments';

import { RESOLUTION, WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../../../shared-components/image/constants';



@observer
class PostDrillDownItemContent extends Component {
  @observable isOpened = false;

  imageValues;
  imageHandlers;

  constructor(props) {
    super(props);
    this.imageValues = {
      src: CommentsModel.formatImageUrl(props.post.imageUrl, RESOLUTION),
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s16x9,
      prepareWrapper: true,
    };
    this.imageHandlers = {
      onClick: ::this.onImageOpenHandler,
    };

    this.lightboxValues = {
      images: [{ src: this.imageValues.src }],
    };
    this.lightboxHandlers = {
      onClose: ::this.onImageCloseHandler,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.post.imageUrl === nextProps.post.imageUrl) return;
    this.imageValues.src = CommentsModel.formatImageUrl(nextProps.post.imageUrl, RESOLUTION);
  }

  onImageOpenHandler() {
    this.setImageVisibility(true);
  }

  onImageCloseHandler() {
    this.setImageVisibility(false);
  }

  @action
  setImageVisibility(value) {
    this.isOpened = value;
  }

  render() {
    const { post } = this.props;
    return (
      <div className='posts-drilldown-content-wrapper'>
        { this.imageValues.src && <Lightbox open={this.isOpened} images={this.lightboxValues.images} onClose={this.lightboxHandlers.onClose} /> }
        { post.imageUrl && <Image values={this.imageValues} handlers={this.imageHandlers} /> }
        <ExpandableText className='posts-drilldown-item-text' text={post.message} usertags={toJS(post.userTags)} isNeedToHandle={true} />
      </div>
    );
  }
}

PostDrillDownItemContent.propTypes = {
  post: PropTypes.shape({
    isRoot: PropTypes.bool,
  }).isRequired,
};

export default PostDrillDownItemContent;
