import {DEFAULT_RESOLUTION} from '../../../../utils/images/resolution/constants';

export const RESOLUTION = DEFAULT_RESOLUTION;

export const WRAPPER_CLASS_NAME = 'posts-drilldown-item-image-wrapper';
export const IMAGE_CLASS_NAME = 'posts-drilldown-item-image';
