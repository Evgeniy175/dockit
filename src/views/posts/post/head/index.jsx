import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router';
import { toJS, observable, action } from 'mobx';
import PropTypes from 'prop-types';

import ProfileImage from './profile-image/index.jsx';
import Menu from '../../../../shared-components/comment/top/menu/index.jsx';



@observer
class PostDrillDownItemHead extends Component {
  render() {
    const { values, handlers } = this.props;
    const { post, forceEnableDeleteAction } = values;
    const { id, username } = post.user;
    const menuValues = {
      comment: post,
      forceEnableDeleteAction,
    };
    return (
      <div className='posts-drilldown-header'>
        <ProfileImage post={post} />
        <Link to={`/users/${id}`} className='creator'>{username}</Link>
        <Menu values={menuValues} handlers={handlers} />
      </div>
    );
  }
}

PostDrillDownItemHead.propTypes = {
  values: PropTypes.shape({
    forceEnableDeleteAction: PropTypes.bool,
    post: PropTypes.shape({
      isRoot: PropTypes.bool,
    }).isRequired,
  }).isRequired,
  handlers: PropTypes.shape({
    onDelete: PropTypes.func.isRequired,
    onRemoved: PropTypes.func.isRequired,
  }).isRequired,
};

export default PostDrillDownItemHead;
