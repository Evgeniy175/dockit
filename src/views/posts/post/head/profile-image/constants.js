import {MIN_RESOLUTION} from '../../../../../utils/images/resolution/constants';

export const RESOLUTION = MIN_RESOLUTION;

export const WRAPPER_CLASS_NAME = 'post-image-wrapper';
export const IMAGE_CLASS_NAME = 'post-image';
