import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { toJS } from 'mobx';
import { browserHistory } from 'react-router';
import PropTypes from 'prop-types';

import Image from '../../../../../shared-components/image/index.jsx';

import UsersModel from '../../../../../models/users';

import { RESOLUTION, WRAPPER_CLASS_NAME, IMAGE_CLASS_NAME } from './constants';
import { ASPECT_RATIOS } from '../../../../../shared-components/image/constants';



@observer
class PostDrillDownItemProfileImage extends Component {
  imageValues;
  imageHandlers;

  constructor(props) {
    super(props);
    this.imageValues = {
      src: UsersModel.formatImageUrl(props.post.user.profilePicture, RESOLUTION),
      wrapperClassName: WRAPPER_CLASS_NAME,
      className: IMAGE_CLASS_NAME,
      aspectRatio: ASPECT_RATIOS.s16x9,
      prepareWrapper: true,
    };
    this.imageHandlers = {
      onClick: ::this.onClickHandler,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.post.user.profilePicture === nextProps.post.user.profilePicture) return;
    this.imageValues.src = UsersModel.formatImageUrl(nextProps.post.user.profilePicture, RESOLUTION);
  }

  onClickHandler() {
    browserHistory.push(`/users/${this.props.post.user_id}`);
  }

  render() {
    return <Image values={this.imageValues} handlers={this.imageHandlers} />;
  }
}

PostDrillDownItemProfileImage.propTypes = {
  post: PropTypes.shape({
    isRoot: PropTypes.bool,
  }).isRequired,
};

export default PostDrillDownItemProfileImage;
