export const getRandomInt = (multiplyBy = 10000000) => Math.floor(Math.random() * multiplyBy);
