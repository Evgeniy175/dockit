import {getEventType} from '../../dom/events';

import {DIRECTIONS} from './constants';
import {KEY_CODES} from '../../../constants';
import {EVENT_TYPES} from '../../dom/events/constants';

export const getDirection = (e, options) => {
  if (!e) return DIRECTIONS.UNKNOWN;
  const type = getEventType(e);
  return HANDLERS_MAPPING[type] ? HANDLERS_MAPPING[type](e, options) : DIRECTIONS.UNKNOWN;
};

export const getKeyboardEventDirection = e => {
  if (e.keyCode === KEY_CODES.PAGE_DOWN) return DIRECTIONS.DOWN;
  if (e.keyCode === KEY_CODES.PAGE_UP) return DIRECTIONS.UP;
  return DIRECTIONS.UNKNOWN;
};

export const getTouchEventDirection = (e, options) => {
  if (!options) return DIRECTIONS.UNKNOWN;
  const { deltas } = options;
  const isUpDown = Math.abs(deltas.y) > Math.abs(deltas.x);
  if (isUpDown && deltas.y > 0) return DIRECTIONS.UP;
  if (isUpDown && deltas.y < 0) return DIRECTIONS.DOWN;
  if (!isUpDown && deltas.x > 0) return DIRECTIONS.RIGHT;
  if (!isUpDown && deltas.x < 0) return DIRECTIONS.LEFT;
  return DIRECTIONS.UNKNOWN;
};

export const getWheelEventDirection = e => {
  const isUpDown = Math.abs(e.deltaY) > Math.abs(e.deltaX);
  if (isUpDown && e.deltaY > 0) return DIRECTIONS.DOWN;
  if (isUpDown && e.deltaY < 0) return DIRECTIONS.UP;
  if (!isUpDown && e.deltaX > 0) return DIRECTIONS.RIGHT;
  if (!isUpDown && e.deltaX < 0) return DIRECTIONS.LEFT;
  return DIRECTIONS.UNKNOWN;
};

const HANDLERS_MAPPING = {
  [EVENT_TYPES.KEYBOARD]: getKeyboardEventDirection,
  [EVENT_TYPES.TOUCH]: getTouchEventDirection,
  [EVENT_TYPES.WHEEL]: getWheelEventDirection,
};
