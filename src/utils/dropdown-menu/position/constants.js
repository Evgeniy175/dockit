export const DEFAULT_PROPS = {
  timeout: 1, // ms
  maxCount: 100,
  buttonSelector: '',
  menuSelector: '.dropdown-menu',
};
