import jq from '../../jquery';

import { DEFAULT_PROPS } from './constants';



export default class DropdownMenuPositionHelper {
  onToggle = ::this.onToggleHandler;
  intervalId;
  counter = 0;

  get button() {
    return jq.wrap(this.buttonSelector);
  }

  get menu() {
    const { button } = this;
    if (!button || !button.isValid()) return null;
    return button.parent.children(this.menuSelector).first();
  }

  constructor(props) {
    Object.assign(this, DEFAULT_PROPS, props);
  }

  onToggleHandler(isShown) {
    this.clear();
    if (!isShown) return;
    this.init();
  }

  init() {
    this.calcPosition();
    this.intervalId = setInterval(::this.calcPosition, this.timeout);
  }

  calcPosition() {
    if (this.counter++ > this.maxCount) return this.clear();
    const { button, menu } = this;
    if (!menu || !menu.isVisible() || !button.isVisible()) return;
    menu.css('left', `-${(menu.width() - button.width()) / 2}px`);
    this.clear();
  }

  clear() {
    clearInterval(this.intervalId);
    this.counter = 0;
  }
}
