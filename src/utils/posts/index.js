export const isPost = comment => comment && (!!comment.target_user_id || (!!comment.data && !!comment.data.target_user_id));
