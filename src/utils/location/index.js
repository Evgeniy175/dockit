import { get } from 'lodash';

import DeviceLocationHelper from './device-location';



export const getLocation = container => get(container, 'locationRaw.title', get(container, 'location.title'));
export const getLocationCoordinates = container => {
  const coords = get(container, 'location.loc.coordinates') || get(container, 'locationRaw.loc.coordinates');
  return coords ? {
    lat: coords[1],
    lng: coords[0],
  } : null;
};

export const DeviceLocation = DeviceLocationHelper;
