export const API_DEVICE_LOCATION_KEY = 'api device location';
export const GEOLOCATION_DEVICE_LOCATION_KEY = 'geo device location';

export const DEFAULT_OPTIONS = {
  locationHtml5Options: {
    timeout: 60000,
  },
};
