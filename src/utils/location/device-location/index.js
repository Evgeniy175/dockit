import Promise from 'bluebird';

import LocationModel from '../../../models/locations';

import { DEFAULT_OPTIONS, API_DEVICE_LOCATION_KEY, GEOLOCATION_DEVICE_LOCATION_KEY } from './constants';

const locationModel = new LocationModel();



export default class DeviceLocation {
  isFetched = false;
  isGeoLocationAvailable;

  constructor(props) {
    Object.assign(this, DEFAULT_OPTIONS, props);
    this.geoLocationSuccess = ::this.geoLocationSuccessHandler;
    this.geoLocationFailed = ::this.geoLocationFailedHandler;
  }

  getDeviceLocation() {
    this.getLocationFromApi();
    this.getGeoLocation();
    this.isFetched = true;
  }

  getLocationFromApi() {
    return locationModel.fetchLocationByIp()
    .then(res => {
      DeviceLocation.storeApiDeviceLocation({
        lat: res.latitude,
        lon: res.longitude,
      });
      return Promise.resolve();
    })
    .catch(console.error);
  }

  getGeoLocation() {
    if (!navigator.geolocation || !navigator.geolocation.watchPosition) return;
    navigator.geolocation.watchPosition(this.geoLocationSuccess, this.geoLocationFailed, this.locationHtml5Options);
  }

  geoLocationSuccessHandler(res) {
    this.isGeoLocationAvailable = true;

    return DeviceLocation.storeDeviceGeoLocation({
      lat: res.coords.latitude,
      lon: res.coords.longitude,
    });
  }

  geoLocationFailedHandler(err) {
    this.isGeoLocationAvailable = false;
    console.error(err);
  }

  static getDeviceLocation() {
    const apiValue = localStorage.getItem(API_DEVICE_LOCATION_KEY);
    const geoLocationValue = localStorage.getItem(GEOLOCATION_DEVICE_LOCATION_KEY);
    const location = geoLocationValue ? geoLocationValue : apiValue;
    return location ? JSON.parse(location) : null;
  }

  static storeApiDeviceLocation(loc) {
    localStorage.setItem(API_DEVICE_LOCATION_KEY, JSON.stringify(loc));
  }

  static storeDeviceGeoLocation(loc) {
    localStorage.setItem(GEOLOCATION_DEVICE_LOCATION_KEY, JSON.stringify(loc));
  }
}
