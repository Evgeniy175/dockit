import Promise from 'bluebird';
import moment from 'moment-timezone';

import LocationModel from '../../models/locations';

const locationModel = new LocationModel();



export function getTimeZone(placeId) {
  return new Promise(resolve => {
    if (!placeId) return resolve();

    return locationModel.fetchLocationByPlaceId(placeId)
    .then(res => {
      return locationModel.getTimezoneByCoords({
        params: {
          lat: res.loc[1],
          lng: res.loc[0],
        },
      })
      .then(res => Promise.resolve(res))
      .catch(err => Promise.resolve());
    })
    .then(res => resolve(res))
    .catch(err => resolve());
  });
}

export function formatDateWithTimezone(time, timezone, formatter) {
  if (!timezone) return moment(time).format(formatter);

  return moment(time)
  .utc()
  .tz(timezone)
  .set({ hours: time.hours(), minutes: time.minutes(), seconds: time.seconds() })
  .format(formatter);
}

export function formatDateWithTimezoneToUtc(time, formatter) {
  return moment().utc().set({ date: time.date(), months: time.month(), years: time.year(), hours: 0, minutes: 0, seconds: 0 }).format(formatter);
}

export function initDateWithTimezone(time, timezone) {
  return moment(time).tz(timezone);
}
