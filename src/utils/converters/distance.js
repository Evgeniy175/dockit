import { MILES_TO_METERS_DIFF } from './constants';

export function milesToMeters(value) {
  return value * MILES_TO_METERS_DIFF;
}

export function metersToMiles(value) {
  return Math.round(value / MILES_TO_METERS_DIFF * 10) / 10 ;
}
