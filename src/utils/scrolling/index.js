import { SCROLL_OFFSET_PX } from './constants';
import { KEY_CODES } from '../../constants';



export function handleButton(e, getContainer, dispatchScrollCallback) {
  switch (e.keyCode) {
    case KEY_CODES.PAGE_UP: return scrollTop(getContainer(), dispatchScrollCallback);
    case KEY_CODES.PAGE_DOWN: return scrollBottom(getContainer(), dispatchScrollCallback);
    default: break;
  }
}

export function scrollTop(container, dispatchScrollCallback) {
  container.scrollTop -= SCROLL_OFFSET_PX;
  if (dispatchScrollCallback) dispatchScrollCallback({ deltaY: -1 });
  return true;
}

export function scrollBottom(container, dispatchScrollCallback) {
  container.scrollTop += SCROLL_OFFSET_PX;
  if (dispatchScrollCallback) dispatchScrollCallback({ deltaY: 1 });
  return true;
}
