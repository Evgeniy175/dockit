import Dom from './dom';
import Styles from './styles';
import Events from './events';

export default {
  ...Dom,
  ...Styles,
  ...Events,
};
