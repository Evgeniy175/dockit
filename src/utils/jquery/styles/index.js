export const setStyle = (container, property, value) => {
  container = getContainer(container);
  if (container && container.style) container.style[property] = value;
};

export const getStyle = (container, property) => {
  container = getContainer(container);
  if (container && container.style) return container.style[property];
};

const getContainer = container => container ? container.context || container : container;



export default {
  setStyle,
  getStyle,
};
