import ListWrapper from '../list-wrapper';

import Animate from './animate';

import { getElem } from '../dom';
import { handleSelector, isSelectorValid } from '../dom/selectors';
import { getStyle, setStyle } from '../styles';
import { attachEvent, detachEvent } from '../events';

import { SELECTOR_TYPES } from '../dom/selectors/constants';



export default class ElementWrapper {
  context;
  _isContextEqualsHandlers = {
    [SELECTOR_TYPES.TAG]: (context, selector) => context.tagName.toUpperCase() === selector.toUpperCase(),
    [SELECTOR_TYPES.CLASS]: (context, selector) => context.classList.contains(selector),
    [SELECTOR_TYPES.ID]: (context, selector) => context.id === selector,
  };

  get style() {
    return this.isValid() ? this.context.style : {};
  }

  get id() {
    return this.isValid() ? this.context.id : null;
  }

  /**
   * Provides `clientHeight` value of the DOM element
   */
  get clientHeight() {
    return this.isValid() ? this.context.clientHeight : 0;
  }
  set clientHeight(value) {
    if (this.isValid()) this.context.clientHeight = value;
  }

  /**
   * Provides `clientWidth` value of the DOM element
   */
  get clientWidth() {
    return this.isValid() ? this.context.clientWidth : 0;
  }
  set clientWidth(value) {
    if (this.isValid()) this.context.clientWidth = value;
  }

  /**
   * Provides `scrollHeight` value of the DOM element
   */
  get scrollHeight() {
    return this.isValid() ? this.context.scrollHeight : 0;
  }
  set scrollHeight(value) {
    if (this.isValid()) this.context.scrollHeight = value;
  }

  /**
   * Provides `scrollWidth` value of the DOM element
   */
  get scrollWidth() {
    return this.isValid() ? this.context.scrollWidth : 0;
  }
  set scrollWidth(value) {
    if (this.isValid()) this.context.scrollWidth = value;
  }

  /**
   * Provides `scrollLeft` value of the DOM element
   */
  get scrollLeft() {
    return this.isValid() ? this.context.scrollLeft : 0;
  }
  set scrollLeft(value) {
    if (this.isValid()) this.context.scrollLeft = value;
  }

  /**
   * Provides `offsetHeight` value of the DOM element
   */
  get offsetHeight() {
    return this.isValid() ? this.context.offsetHeight : 0;
  }
  set offsetHeight(value) {
    if (this.isValid()) this.context.offsetHeight = value;
  }

  /**
   * Provides `offsetWidth` value of the DOM element
   */
  get offsetWidth() {
    return this.isValid() ? this.context.offsetWidth : 0;
  }
  set offsetWidth(value) {
    if (this.isValid()) this.context.offsetWidth = value;
  }

  /**
   * Provides `scrollTop` value of the DOM element
   */
  get scrollTop() {
    return this.isValid() ? this.context.scrollTop : 0;
  }
  set scrollTop(value) {
    if (this.isValid()) this.context.scrollTop = value;
  }

  get parent() {
    return this.isValid() ? new ElementWrapper(this.context.parentElement) : null;
  }

  constructor(elementOrSelector) {
    if (elementOrSelector instanceof HTMLElement) this.context = elementOrSelector;
    else if (typeof elementOrSelector === 'string') this.context = getElem(elementOrSelector).context;
    else this.recognizeElement(elementOrSelector);
  }

  /**
   * Helps to recognize element. E.g. if selector is DOM SVG element - it will be `object` type
   * @param elem - DOM element
   */
  recognizeElement(elem) {
    if (!elem || !elem.parentElement) return;
    while (elem && elem.parentElement && !(elem instanceof HTMLElement)) elem = elem.parentElement;
    this.context = elem;
  }

  /**
   * Provides `width` value of the DOM element
   * @returns {number} - `width` value of the DOM element
   */
  width() {
    return this.isValid() ? this.context.clientWidth : 0;
  }

  /**
   * Provides `height` value of the DOM element
   * @returns {number} - `height` value of the DOM element
   */
  height() {
    return this.isValid() ? this.context.clientHeight : 0;
  }

  /**
   * Attaches event to the DOM element
   */
  on(event, cb) {
    attachEvent(this.context, event, cb);
  }

  /**
   * Removes event from the DOM element
   */
  detachEvent(event, cb) {
    detachEvent(this.context, event, cb);
  }

  /**
   * Indicates is DOM element refers to `targetTagName` elements
   * @param targetTagName - Target tag name of the elements
   * @returns {boolean} - True if DOM element refers to `targetTagName` elements
   */
  is(targetTagName) {
    const tagName = this.isValid() && this.context.tagName ? this.context.tagName : null;
    return tagName && tagName.toUpperCase() === targetTagName.toUpperCase();
  }

  /**
   * Indicates is element hidden
   * @returns {boolean} - True if element is hidden
   */
  isHidden() {
    return !this.isVisible();
  }

  /**
   * Indicates is element visible
   * @returns {boolean} - True if element is visible
   */
  isVisible() {
    return this.isValid() && this.context.offsetWidth > 0 && this.context.offsetHeight > 0;
  }

  /**
   * If `value` is set, attaches value to the CSS `property` of the DOM element.
   * Otherwise - returns CSS `property` value of the DOM element.
   * @returns {string|number} - CSS `property` value of the DOM element
   */
  css(property, value = '') {
    let isValueExists = false;
    switch (typeof value) {
      case 'string':
        isValueExists = value && value.hasOwnProperty('length') && value.length > 0;
        break;
      case 'number':
        isValueExists = !!value;
        break;
      default: break
    }
    return isValueExists ? setStyle(this.context, property, value) : getStyle(this.context, property);
  }

  /**
   * Indicates is DOM element contains class name
   * @param value - Class name for check
   * @returns {boolean} - True if DOM element contains class name
   */
  hasClass(value) {
    if (this.isValid()) return this.context.classList.contains(value);
  }

  /**
   * Attaches class name to the DOM element
   * @param value - Class name
   */
  addClass(value) {
    if (this.isValid()) return this.context.classList.add(value);
  }

  /**
   * Removes class name from the DOM element
   * @param value - Class name
   */
  removeClass(value) {
    if (this.isValid()) this.context.classList.remove(value);
  }

  /**
   * Returns offset data of the DOM element
   * @returns {Object} - Offset data of the DOM element
   */
  offset() {
    return this.isValid() ? {
      top: this.context.offsetTop,
      left: this.context.offsetLeft,
    } : {};
  }

  listen(name, handler, useCapture) {
    if (!this.isValid()) return;
    this.context.addEventListener(name, handler, useCapture);
  }

  unlisten(name, handler, useCapture) {
    if (!this.isValid()) return;
    this.context.removeEventListener(name, handler, useCapture);
  }

  /**
   * Do focus to the DOM element
   */
  focus() {
    if (!this.isValid() || !this.context.focus || !this.context.setSelectionRange) return;
    const strLength = this.context.value.length * 2;
    this.context.focus();
    this.context.setSelectionRange(strLength, strLength);
  }

  /**
   * Indicates is DOM element has parents that according to selector
   * @param selector - Selector of the DOM element
   * @returns {boolean} - True if DOM element has parents that according to selector
   */
  hasParents(selector) {
    if (!this.isValid() || !isSelectorValid(selector)) return false;

    const selectorData = handleSelector(selector);
    let parent = this.context.parentElement;

    while(!!parent) {
      if (this._isContextEquals(parent, selectorData)) return true;
      parent = parent.parentElement;
    }

    return false;
  }

  /**
   * Search for children (only one node)
   * Behavior like children() in JQuery
   * @param {String} selector - Selector for children
   * @returns {ElementWrapper|null} - Result of searching
   */
  children(selector) {
    if (!this.isValid() || !isSelectorValid(selector)) return false;
    const selectorData = handleSelector(selector);
    const childs = Array.from(this.context.children);
    const result = childs.filter(child => this._isContextEquals(child, selectorData));
    return new ListWrapper(result);
  }

  /**
   * Search for children (many nodes)
   * Behavior like find() in JQuery
   * @param {String} selector - Selector for children
   * @returns {ElementWrapper|null} - Result of searching
   */
  find(selector) {
    if (!this.isValid() || !isSelectorValid(selector)) return false;
    const selectorData = handleSelector(selector);
    const results = this._find(this.context, selectorData);
    return new ListWrapper(results);
  }

  /**
   * Search for children (many nodes)
   * @param {HTMLElement} context - Current DOM element
   * @param {Object} selectorData - Selector data
   * @returns {Array} - Search results
   * @private
   */
  _find(context, selectorData) {
    const childs = Array.from(context.children);
    let results = [];

    childs.forEach(child => {
      if (this._isContextEquals(child, selectorData)) results.push(child);
      results = results.concat(this._find(child, selectorData));
    });

    return results;
  }

  /**
   * Do animation
   * @param {Object} props - Object that contains properties that should be changed
   * @param {Object} options - Object that contains options for animation
   */
  animate(props, options) {
    if (!this.isValid() || !options || Object.keys(props).length === 0 || Object.keys(options).length === 0) return;
    const animation = new Animate(this.context, props, options);
    animation.animate();
  }

  centerVertically(container = document.body) {
    if (!this.isValid()) return;
    this.context.scrollIntoView();
    container.scrollTop -= window.innerHeight / 2 - this.height() / 2;
    this.context.scrollIntoViewIfNeeded();
  }

  /**
   * Is DOM element according to selector
   * @param {HTMLElement} context - DOM element
   * @param {Object} selectorData - Selector data
   * @returns {boolean} - True if DOM element according to selector
   * @private
   */
  _isContextEquals(context, selectorData) {
    return this._isContextEqualsHandlers[selectorData.type](context, selectorData.selector);
  }

  /**
   * Indicates is DOM element valid
   * @returns {boolean} - True if DOM element is valid
   */
  isValid() {
    return !!this.context;
  }
}