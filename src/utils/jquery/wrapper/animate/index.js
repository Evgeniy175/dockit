import { DEFAULT_OPTIONS } from './constants';
import { NUMBER_REGEX } from '../../../formatting/constants';



export default class ElementWrapperAnimate {
  context;
  props;
  keys;
  options = {};
  initialValues = new Map();
  deltas = new Map();

  nOfTimes;
  timesCounter = 0;

  intervalId;

  constructor(context, props, options) {
    this.context = context;
    this.props = props;
    this.keys = Object.keys(props);
    this.options = Object.assign(DEFAULT_OPTIONS, options);
    this.nOfTimes = this.options.duration / this.options.interval;

    this.keys.forEach(key => {
      const initialValue = this.getValue(context, key);
      this.initialValues.set(key, initialValue);
      this.deltas.set(key, (props[key] - initialValue) / this.nOfTimes);
    });

    this._doStep = ::this._doStepHandler;
  }

  animate() {
    this.intervalId = setInterval(this._doStep, this.options.interval);
  }

  _doStepHandler() {
    const isWithClear = this.timesCounter++ >= this.nOfTimes;
    this.keys.forEach(key => {
      if (isWithClear) return this.resetValue(key);
      const delta = this.deltas.get(key);
      this.handleParentProperty(key, delta);
      this.handleStyleProperty(key, delta);
    });
  }

  handleParentProperty(key, delta) {
    if (!(key in this.context)) return;
    if (!this.isValueLess(delta, this.context[key] + delta, this.props[key])) return this.resetValue(key);
    this.context[key] = `${this.context[key] + delta}${this.options.measure ? this.options.measure : ''}`;
  }

  handleStyleProperty(key, delta) {
    if (!(key in this.context.style)) return;
    const value = this.getValue(this.context, key) + delta;
    if (!this.isValueLess(delta, value + delta, this.props[key])) return this.resetValue(key);
    this.context.style[key] = `${value}${this.options.measure ? this.options.measure : ''}`;
  }

  resetValue(key) {
    if (key in this.context) this.context[key] = this.props[key];
    if (key in this.context.style) this.context.style[key] = this.props[key];
    this.clearInterval();
  }

  getValue(context, key) {
    if (key in context) return context[key];
    const value = this.context.style[key];
    if (!value) return 0;
    const matched = value.match(NUMBER_REGEX);
    return matched ? Number.parseFloat(matched[0]) : 0;
  }

  isValueLess(delta, newValue, maxValue) {
    return delta > 0 ? newValue < maxValue : newValue > maxValue;
  }

  clearInterval() {
    clearInterval(this.intervalId);
    this.timesCounter = 0;
  }
}