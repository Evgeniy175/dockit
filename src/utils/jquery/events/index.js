export const attachEvent = (elem, eventName, cb) => {
  if (elem && cb && elem.addEventListener) elem.addEventListener(eventName, cb);
};

export const detachEvent = (elem, eventName, cb) => {
  if (elem && cb && elem.removeEventListener) elem.removeEventListener(eventName, cb);
};

export default {
  attachEvent,
  detachEvent,
};
