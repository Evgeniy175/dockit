import { getElems } from '../dom';
import { isSelectorValid } from '../dom/selectors';

import Wrapper from '../wrapper';



export default class ListOfElementsWrapper {
  context;

  constructor(selectorOrArray) {
    if (selectorOrArray instanceof Array) this.initByArray(selectorOrArray);
    else if (typeof selectorOrArray === 'string') this.initBySelector(selectorOrArray);
  }

  initBySelector(selector) {
    if (!isSelectorValid(selector)) return;
    this.initByArray(Array.from(getElems(selector)));
  }

  initByArray(array) {
    this.context = array.map(elem => new Wrapper(elem));
  }

  each(cb) {
    this.context.forEach(elem => cb(elem));
  }

  first() {
    return this.context && this.context.length > 0 ? this.context[0] : null;
  }
}