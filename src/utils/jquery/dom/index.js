import Wrapper from '../wrapper';
import ListWrapper from '../list-wrapper';

import { handleSelector } from './selectors';

import { SELECTOR_TYPES } from './selectors/constants';



export const wrap = elementOrSelector => new Wrapper(elementOrSelector);
export const wrapMany = selector => new ListWrapper(selector);

export const getElem = selector => {
  const selectorData = handleSelector(selector);
  return itemResolvers[selectorData.type](selectorData.selector);
};

export const getElems = selector => {
  const selectorData = handleSelector(selector);
  if (selectorData.type !== SELECTOR_TYPES.CLASS) return;
  return document.getElementsByClassName(selectorData.selector);
};

export const getElemByTagName = selector => wrap(document.getElementsByTagName(selector)[0]);
export const getElemByClassName = selector => wrap(document.getElementsByClassName(selector)[0]);
export const getElemById = selector => wrap(document.getElementById(selector));



const itemResolvers = {
  [SELECTOR_TYPES.TAG]: getElemByTagName,
  [SELECTOR_TYPES.CLASS]: getElemByClassName,
  [SELECTOR_TYPES.ID]: getElemById,
};



export default {
  wrap,
  wrapMany,
  getElem,
  getElems,
  getElemByClassName,
  getElemById,
};
