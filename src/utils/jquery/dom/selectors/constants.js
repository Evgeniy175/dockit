export const CLASS_SELECTOR_PREFIX = '.';
export const ID_SELECTOR_PREFIX = '#';

export const SELECTOR_TYPES = {
  TAG: 'tag',
  ID: 'id',
  CLASS: 'class',
};
