import { CLASS_SELECTOR_PREFIX, ID_SELECTOR_PREFIX, SELECTOR_TYPES } from './constants';

const selectorHandlers = {
  [SELECTOR_TYPES.TAG]: selector => selector,
  [SELECTOR_TYPES.CLASS]: selector => selector.substr(1),
  [SELECTOR_TYPES.ID]: selector => selector.substr(1),
};

export const handleSelector = selector => {
  const isValid = isSelectorValid(selector);
  const type = getSelectorType(selector);
  return {
    isValid,
    type,
    selector: isValid ? selectorHandlers[type](selector) : selector,
  };
};

export const getSelectorType = selector => {
  if (isSelectorByClass(selector)) return SELECTOR_TYPES.CLASS;
  if (isSelectorById(selector)) return SELECTOR_TYPES.ID;
  return SELECTOR_TYPES.TAG;
};

export const isSelectorByClass = selector => isSelectorValid(selector) && selector[0] === CLASS_SELECTOR_PREFIX;
export const isSelectorById = selector => isSelectorValid(selector) && selector[0] === ID_SELECTOR_PREFIX;
export const isSelectorValid = selector => selector && selector.length && selector.length > 0;
