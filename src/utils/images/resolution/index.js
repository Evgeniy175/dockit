import {RESOLUTIONS} from './constants';
import {closest} from '../../math';

export const getResolution = expected => {
  const closestResolution = closest(expected, RESOLUTIONS);
  return {
    width: closestResolution,
    height: closestResolution,
  }
};
