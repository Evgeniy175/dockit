export const DEFAULT_RESOLUTION = 640;
export const MIN_RESOLUTION = 50;
export const MAX_RESOLUTION = 1920;

export const RESOLUTIONS = [
  50,
  100,
  160,
  320,
  640,
  1280,
  1920,
];
