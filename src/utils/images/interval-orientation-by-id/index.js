import { INTERVAL_MS, MAX_ITERATIONS } from './constants';



export default class IntervalCropperById {
  nOfIterations;
  intervalId;
  isValid = false;

  constructor() {
    this.setImageMeasures = ::this.setImageMeasuresHandler;
    this.setImageMeasures = ::this.setImageMeasuresHandler;
  }

  initInterval(props) {
    this.validate(props);
    if (!this.isValid) return;
    Object.assign(this, props);
    if (!!this.intervalId) clearInterval(this.intervalId);
    this.nOfIterations = 0;
    this.intervalId = setInterval(this.setImageMeasures, INTERVAL_MS);
  }

  validate(props) {
    if (!props.imageId) console.error('Interval orientation: imageId is required.');
    if (!props.shownImage) console.warn('Interval orientation: shownImage not set.');
    this.isValid = !!props.imageId;
  }

  setImageMeasuresHandler() {
    if (this.nOfIterations++ > MAX_ITERATIONS) return clearInterval(this.intervalId);
    const elem = document.getElementById(this.imageId);
    if (!elem || elem.clientHeight === 0 || elem.clientWidth === 0 || (this.shownImage && !elem.src.endsWith(this.shownImage))) return;
    const property = elem.clientWidth < elem.clientHeight ? 'width' : 'height';
    elem.style[property] = '100%';
    clearInterval(this.intervalId);
  }
}
