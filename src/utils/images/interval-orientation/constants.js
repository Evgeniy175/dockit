export const DEFAULT_PROPS = {
  timeout: 50,
  maxIterations: 1000,
};

export const INTERVAL_MS = 50;
export const MAX_ITERATIONS = 1000;
