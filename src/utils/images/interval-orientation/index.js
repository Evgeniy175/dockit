import { DEFAULT_PROPS, INTERVAL_MS, MAX_ITERATIONS } from './constants';


/**
 * Helps to make square image from non-square
 */
export default class IntervalCropper {
  nOfIterations;
  intervalId;
  isValid = false;

  constructor() {
    this.setImageMeasures = ::this.setImageMeasuresHandler;
    this.setImageMeasuresById = ::this.setImageMeasuresByIdHandler;
  }

  initInterval(props) {
    this.validate(props);
    if (!this.isValid) return;
    Object.assign(this, DEFAULT_PROPS, props);
    if (!!this.intervalId) clearInterval(this.intervalId);
    this.nOfIterations = 0;
    this.setImageMeasures();
    this.intervalId = setInterval(this.setImageMeasures, this.timeout);
  }

  validate(props) {
    if (!props.wrapperClassName) console.error('Interval orientation: wrapperClassName is required.');
    if (!props.imageClassName) console.error('Interval orientation: imageClassName is required.');
    if (!props.shownImage) console.warn('Interval orientation: shownImage not set.');
    this.isValid = !!props.wrapperClassName && !!props.imageClassName;
  }

  setImageMeasuresHandler() {
    if (this.nOfIterations++ > this.maxIterations) return clearInterval(this.intervalId);
    const wrapper = document.getElementsByClassName(this.wrapperClassName)[0];
    if (!wrapper) return;
    const elem = wrapper.getElementsByClassName(this.imageClassName)[0];
    if (!elem || elem.clientHeight === 0 || elem.clientWidth === 0 || (this.shownImage && !elem.src.endsWith(this.shownImage))) return;
    const property = elem.clientWidth < elem.clientHeight ? 'width' : 'height';
    elem.style[property] = '100%';
    clearInterval(this.intervalId);
  }

  initIntervalById(props) {
    this.validate(props);
    if (!this.isValid) return;
    Object.assign(this, props);
    if (!!this.intervalId) clearInterval(this.intervalId);
    this.nOfIterations = 0;
    this.intervalId = setInterval(this.setImageMeasuresById, INTERVAL_MS);
  }

  validateById(props) {
    if (!props.wrapperClassName) console.error('Interval orientation: wrapperClassName is required.');
    if (!props.imageClassName) console.error('Interval orientation: imageClassName is required.');
    if (!props.shownImage) console.warn('Interval orientation: shownImage not set.');
    this.isValid = !!props.wrapperClassName && !!props.imageClassName;
  }

  setImageMeasuresByIdHandler() {
    if (this.nOfIterations++ > MAX_ITERATIONS) return clearInterval(this.intervalId);
    const wrapper = document.getElementsByClassName(this.wrapperClassName)[0];
    if (!wrapper) return;
    const elem = wrapper.getElementsByClassName(this.imageClassName)[0];
    if (!elem || elem.clientHeight === 0 || elem.clientWidth === 0 || (this.shownImage && !elem.src.endsWith(this.shownImage))) return;
    const property = elem.clientWidth < elem.clientHeight ? 'width' : 'height';
    elem.style[property] = '100%';
    clearInterval(this.intervalId);
  }
}
