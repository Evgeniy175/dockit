import { get } from 'lodash';

export function generateKey(feedItem, idx = 0) {
  feedItem = feedItem.data;

  const keyItems = [
    get(feedItem, 'id'),
    get(feedItem, 'identityKey'),
    feedItem.type,
    feedItem.action,
    feedItem.timestamp,
    idx,
  ];
  
  return keyItems.join('_');
}
