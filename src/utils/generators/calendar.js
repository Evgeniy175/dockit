import moment from 'moment';

import { DAYS_IN_WEEK, FIRST_DAY_OF_MONTH, WEEKEND_DAYS } from './constants';

export function getCalendarForDate(viewedDate, selectedDate) {
  let calendar = [];
  let date = moment(viewedDate).startOf('month');
  let isLastDayOfMonth = false;
  
  const lastDayOfMonth = moment(viewedDate).endOf('month').date();
  
  while (!isLastDayOfMonth) {
    const rowData = getRow(date, selectedDate, lastDayOfMonth);
    date = rowData.date;
    calendar.push(rowData.row);
    isLastDayOfMonth = rowData.isLastDayOfMonth;
  }
  
  return calendar;
}

function getRow(date, selectedDate, lastDayOfMonth) {
  const currentDate = moment().startOf('day');
  let row = [];
  let nOfDays = 0;
  let isFirstDayOfMonth = date.date() === FIRST_DAY_OF_MONTH;
  let isLastDayOfMonth = false;
  
  if (isFirstDayOfMonth) nOfDays = fillEmptyCellsBefore(row, date, nOfDays);
  
  // fill cells with data
  while (nOfDays < DAYS_IN_WEEK && !isLastDayOfMonth) {
    isLastDayOfMonth = date.date() === lastDayOfMonth;
    
    const month = date.month();
    const dayOfMonth = date.date();
    const dayOfWeek = date.day();
    
    row.push({
      date: date.clone(),
      dayOfMonth,
      isActive: selectedDate && dayOfMonth === selectedDate.date() && month === selectedDate.month(),
      isWeekend: WEEKEND_DAYS.includes(dayOfWeek),
      isPassed: date.isBefore(currentDate)
    });
    
    date = date.add(1, 'days');
    nOfDays++;
  }
  
  fillEmptyCellsAfter(row, nOfDays);
  
  return {
    row,
    date,
    isLastDayOfMonth
  };
}

function fillEmptyCellsBefore(row, date, nOfDays) {
  const dayOfWeek = date.day();
  
  while (nOfDays < dayOfWeek) {
    row.push(null);
    nOfDays++;
  }
  
  return nOfDays;
}

function fillEmptyCellsAfter(row, nOfDays) {
  while (nOfDays < DAYS_IN_WEEK) {
    row.push(null);
    nOfDays++;
  }
}
