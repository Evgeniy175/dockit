import { DEFAULT_DELTA, DIRECTIONS, HANDLERS, INTERVAL_TIMEOUT } from './constants';

import { values } from '../object';

const privateMethod = Symbol('privateMethod');



class SwipeHandler {
  offset;
  events = {};
  elem;

  touchStartX;
  touchStartY;

  init(target, handlers, delta = DEFAULT_DELTA) {
    if (!target || !this.isHandlersValid(handlers)) return;

    this.offset = delta;
    this.events = handlers;
    this.elem = target;

    this.onStart = ::this.startHandler;
    this.onMove = ::this.moveHandler;
    this.onEnd = ::this.endHandler;
    this.onCancel = ::this.cancelHandler;

    this.addHandler('touchstart', this.onStart);
    this.addHandler('touchmove', this.onMove);
    this.addHandler('touchend', this.onEnd);
    this.addHandler('touchcancel', this.onCancel);
  }

  dispose() {
    this.removeHandler('touchstart', this.onStart);
    this.removeHandler('touchmove', this.onMove);
    this.removeHandler('touchend', this.onEnd);
    this.removeHandler('touchcancel', this.onCancel);
  }

  isHandlersValid(handlers) {
    const allHandlers = values(HANDLERS);
    return handlers && allHandlers.some(handlerKey => !!handlers[handlerKey]);
  }

  startHandler(e) {
    this.callHandler(e, HANDLERS.CLEAR_END_INTERVAL);
    this.touchStartX = e.touches[0].clientX;
    this.touchStartY = e.touches[0].clientY;
    this.callHandler(e, HANDLERS.START);
  }

  moveHandler(e) {
    const currTouchX = e.changedTouches[0].clientX;
    const currTouchY = e.changedTouches[0].clientY;
    let xHandled = false;
    let yHandled = false;

    if (this.touchStartX > currTouchX + this.offset) { this.callHandler(e, HANDLERS[DIRECTIONS.LEFT]); xHandled = true; }
    else if (this.touchStartX < currTouchX - this.offset) { this.callHandler(e, HANDLERS[DIRECTIONS.RIGHT]); xHandled = true; }
    else if (this.touchStartY > currTouchY + this.offset) { this.callHandler(e, HANDLERS[DIRECTIONS.UP]); yHandled = true; }
    else if (this.touchStartY < currTouchY - this.offset) { this.callHandler(e, HANDLERS[DIRECTIONS.DOWN]); yHandled = true; }

    if (xHandled) this.touchStartX = currTouchX;
    if (yHandled) this.touchStartY = currTouchY;
    this.callHandler(e, HANDLERS.MOVE);
  }

  endHandler(e) {
    this.callHandler(e, HANDLERS.END);
    if (!this.isHandlerExists(HANDLERS.SET_END_INTERVAL) || !this.isHandlerExists(HANDLERS.END)) return;
    const intervalId = setInterval(() => { this.callHandler(e, HANDLERS.END); }, INTERVAL_TIMEOUT);
    this.callHandler(intervalId, HANDLERS.SET_END_INTERVAL);
  }

  cancelHandler(e) {
    this.callHandler(e, HANDLERS.CANCEL);
  }

  callHandler(e, name) {
    return this.isHandlerExists(name) ? this.events[name](e) : null;
  }

  isHandlerExists(name) {
    return !!this.events[name];
  }

  addHandler(name, handler) {
    window.addEventListener(name, handler, false);
  }

  removeHandler(name, handler) {
    window.removeEventListener(name, handler, false);
  }
}

export default SwipeHandler;
