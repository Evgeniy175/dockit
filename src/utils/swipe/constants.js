export const DEFAULT_DELTA = 10;
export const INTERVAL_TIMEOUT = 250;

export const DIRECTIONS = {
  UP: 'up',
  RIGHT: 'right',
  DOWN: 'down',
  LEFT: 'left',
};

export const HANDLERS = {
  [DIRECTIONS.UP]: 'onUp',
  [DIRECTIONS.RIGHT]: 'onRight',
  [DIRECTIONS.DOWN]: 'onDown',
  [DIRECTIONS.LEFT]: 'onLeft',

  START: 'onStart',
  MOVE: 'onMove',
  END: 'onEnd',
  CANCEL: 'onCancel',
  SET_END_INTERVAL: 'setEndInterval',
  CLEAR_END_INTERVAL: 'clearEndInterval',
};
