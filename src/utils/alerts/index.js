import { isEqual } from 'lodash';

import { DEFAULT_CATEGORIES_LIST } from './constants';
import { ALERTS } from '../../shared-components/alerts-modal/constants';



export const getSelectedAlertsAsText = (selectedAlerts) => {
  if (!selectedAlerts || selectedAlerts.length === 0) return DEFAULT_CATEGORIES_LIST;
  const alertsResult = selectedAlerts.map(selectedAlert => {
    const fullAlert = ALERTS.find(alert => isEqual(alert.data, selectedAlert));
    return fullAlert ? fullAlert.title : null;
  }).filter(alert => !!alert);
  return alertsResult.length > 0 ? alertsResult.join(', ') : DEFAULT_CATEGORIES_LIST;
};
