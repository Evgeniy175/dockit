export const SWIPE_MIN_DELTA = 30;
export const ANIMATION_TIME = 200;
export const SCROLL_TOP_ANIMATION_OFFSET = 200;
export const INTERVAL_TIMEOUT = 250;

export const EXPANDED_MENU_MARGIN = 0;
export const COLLAPSED_MENU_MARGIN = -66;

export const ACTIONS = {
  EXPAND: 'expand',
  COLLAPSE: 'collapse',
};
