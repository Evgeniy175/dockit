import jq from '../jquery';
import {getDirection} from '../mouse/wheel';

import {DIRECTIONS} from '../mouse/wheel/constants';

import { INTERVAL_TIMEOUT, SWIPE_MIN_DELTA, ANIMATION_TIME, SCROLL_TOP_ANIMATION_OFFSET, ACTIONS, EXPANDED_MENU_MARGIN, COLLAPSED_MENU_MARGIN } from './constants';

let touchStartX = 0;
let touchStartY = 0;
let menuElem;
let isScrollable = false;
let isAlreadyHidden = false;

let lastDirection;

let intervalId;
let prevBodyScrollTop;

let isScrollToTopInitComplete = false;



export const init = () => {
  menuElem = getMenu();

  const scrollToTop = getScrollToTop();
  scrollToTop.css('height', getCssPropertyValue(menuElem, 'height', 'px'));

  addHandler('touchstart', startHandler);
  addHandler('touchmove', moveHandler);
  addHandler('touchend', endHandler);
  addHandler('touchcancel', cancelHandler);
};

export const dispose = () => {
  removeHandler('touchstart', startHandler);
  removeHandler('touchmove', moveHandler);
  removeHandler('touchend', endHandler);
  removeHandler('touchcancel', cancelHandler);
};



const startHandler = e => {
  clearScrollInterval();
  isScrollable = getIsScrollable();
  if (!isScrollable) return handleNonScrollable();
  initScrollToTop();
  touchStartY = e.touches[0].clientY;
  touchStartX = e.touches[0].clientX;
};

const getIsScrollable = () => {
  const container = document.body;
  return container.scrollHeight > container.clientHeight;
};

const handleNonScrollable = () => menuHandler(ACTIONS.EXPAND);

const initScrollToTop = () => {
  if (isScrollToTopInitComplete) return;
  isScrollToTopInitComplete = true;
  const scrollToTopWrapper = getScrollToTopWrapper();
  if (!scrollToTopWrapper.isValid()) return;
  scrollToTopWrapper.on('click', () => menuHandler(ACTIONS.EXPAND, () => { isAlreadyHidden = false; }));
};

const moveHandler = e => {
  if (!isScrollable) return handleNonScrollable;
  const currTouchY = e.changedTouches[0].clientY;
  const currTouchX = e.changedTouches[0].clientX;
  const deltas = {
    x: touchStartX - currTouchX,
    y: touchStartY - currTouchY,
  };

  const direction = getDirection(e, { deltas });

  clearScrollInterval();

  if (direction === DIRECTIONS.UP && deltas.y > SWIPE_MIN_DELTA) upHandler(currTouchY);
  else if (direction === DIRECTIONS.DOWN && deltas.y < SWIPE_MIN_DELTA) downHandler(currTouchY);
  else return;

  touchStartY = currTouchY;
  touchStartX = currTouchX;
};

const endHandler = () => {
  if (!isScrollable) return handleNonScrollable;
  intervalId = setInterval(() => {
    if (document.body.scrollTop === 0) return clearScrollInterval();
    if (lastDirection === DIRECTIONS.UP) upHandler();
    if (lastDirection === DIRECTIONS.DOWN) downHandler();
    if (prevBodyScrollTop === document.body.scrollTop) clearScrollInterval();
    prevBodyScrollTop = document.body.scrollTop;
  }, INTERVAL_TIMEOUT);
};

const cancelHandler = e => {};



const upHandler = () => {
  lastDirection = DIRECTIONS.UP;
  menuHandler(ACTIONS.COLLAPSE);
};

const downHandler = () => {
  lastDirection = DIRECTIONS.DOWN;
  menuHandler(ACTIONS.EXPAND);
};



const menuHandler = (action, cb) => {
  if (!menuElem) return;
  const marginTop = menuElem.css('margin-top');
  MENU_HANDLERS[action](menuElem, marginTop, cb);
};

const MENU_HANDLERS = {
  [ACTIONS.EXPAND]: (menu, currMarginTop, cb) => {
    if (!isAlreadyHidden && document.body.scrollTop > SCROLL_TOP_ANIMATION_OFFSET) return;
    isAlreadyHidden = false;
    window.menu = menu;
    animate(menu, { marginTop: EXPANDED_MENU_MARGIN });
    if (cb) cb();
  },
  [ACTIONS.COLLAPSE]: (menu, currMarginTop, cb) => {
    if (isAlreadyHidden) return;
    isAlreadyHidden = true;
    animate(menu, { marginTop: COLLAPSED_MENU_MARGIN });
    if (cb) cb();
  },
};



const animate = (elem, data) => {
  elem.animate(data, {
    duration: ANIMATION_TIME,
  });
};



const clearScrollInterval = () => {
  if (!intervalId) return;
  clearInterval(intervalId);
  intervalId = null;
};



const getMenu = () => jq.wrap('.navbar');
const getScrollToTopWrapper = () => jq.wrap('.scroll-to-top-wrapper');
const getScrollToTop = () => jq.wrap('.scroll-to-top');

const getCssPropertyValue = (elem, property, splitBy) => Number.parseFloat(elem.css(property).split(splitBy)[0]);
const setCssPropertyValue = (elem, property, value) => elem.css(property, value);

const addHandler = (name, handler) => window.addEventListener(name, handler, false);
const removeHandler = (name, handler) => window.removeEventListener(name, handler, false);
