import moment from 'moment';
import { get, uniqBy } from 'lodash';

import EventModel from '../../models/events/index';
import ScheduleModel from '../../models/schedules/index';

import DefaultImage from '../../assets/images/icons/event-default.svg';

import { UPCOMING_ITEMS_TYPES, UPCOMING_KEY_FORMATTER, UPCOMING_KEY_FORMATTER_TEMP } from './constants';

import { getLocation } from '../location';




export function getSortedEvents(events, schedules) {
  const scheduleKeys = Object.keys(schedules);
  let result = {};
  
  scheduleKeys.forEach(key => {
    const schedule = schedules[key].schedule;
    const dates = schedules[key].dates;
    
    const startTime = moment(schedule.startTime);
    
    const buff = dates.reduce((arr, nextDate) => {
      const newTime = moment(nextDate);
      setTime(startTime, newTime);
      
      const key = getKey(newTime);
      const value = getFormattedSchedule(schedule, newTime);
      
      arr[key] = arr[key] || [];
      arr[key].push(value);
      
      return arr;
    }, {});
    
    Object.keys(buff).forEach(key => { result[key] = result[key] ? result[key].concat(buff[key]) : buff[key]; });
  });
  
  const eventsBuff = events.reduce((arr, event) => {
    const key = getKey(event.startTime);
    const value = getFormattedEvent(event);
    
    arr[key] = arr[key] || [];
    arr[key].push(value);
    
    return arr;
  }, {});
  
  Object.keys(eventsBuff).forEach(key => { result[key] = result[key] ? result[key].concat(eventsBuff[key]) : eventsBuff[key]; });
  Object.keys(result).forEach(key => {
    result[key] = uniqBy(result[key], itemsComparer);
    result[key].sort((left, right) => moment(left.date).diff(moment(right.date)));
  });
  
  return result;
}

function getKey(time) {
  return moment(time).format(UPCOMING_KEY_FORMATTER_TEMP);
}

function getFormattedSchedule(schedule, newTime) {
  let formattedAddress = get(schedule, 'location.title', null);
  let location = get(schedule, 'location', null);
  
  if (!formattedAddress && location) location = `${location.name}, ${location.country}`;
  
  return {
    id: schedule.id,
    type: UPCOMING_ITEMS_TYPES.SCHEDULE,
    image: ScheduleModel.formatImageUrl(schedule.image, DefaultImage),
    title: schedule.title,
    date: newTime,
    startTime: moment(schedule.startTime),
    endTime: moment(schedule.endTime),
    location: formattedAddress ? formattedAddress : location,
    createdAt: schedule.created_at,
    myIntent: schedule.myIntent,
    userId: schedule.user_id
  };
}

function getFormattedEvent(event) {
  let formattedAddress = getLocation(event);
  let location = get(event, 'location', null);
  
  if (!formattedAddress && location) location = `${location.name}, ${location.country}`;
  
  return {
    id: event.id,
    type: UPCOMING_ITEMS_TYPES.EVENT,
    image: EventModel.formatImageUrl(event.image, DefaultImage),
    title: event.title,
    date: moment(event.startTime),
    startTime: moment(event.startTime),
    endTime: moment(event.endTime),
    location: formattedAddress ? formattedAddress : location,
    createdAt: event.created_at,
    myIntent: event.myIntent,
    userId: event.user_id
  };
}

function setTime(from, to) {
  let time = {
    h: from.hour(),
    m: from.minute(),
    s: from.second(),
    ms: from.millisecond()
  };
  
  to.set({ hour: time.h, minute: time.m, second: time.s, millisecond: time.ms });
}

function itemsComparer(item) {
  return `${item.createdAt}${item.title}${item.location}`;
}
