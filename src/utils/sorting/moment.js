import moment from 'moment';

export function ascCompare(a, b) {
  const t1 = moment(a.created_at);
  const t2 = moment(b.created_at);
  
  if (t2.isBefore(t1)) return 1;
  if (t2.isAfter(t1)) return -1;
  
  return 0;
}

export function descCompare(a, b) {
  const t1 = moment(a.created_at);
  const t2 = moment(b.created_at);
  
  if (t2.isBefore(t1)) return -1;
  if (t2.isAfter(t1)) return 1;
  
  return 0;
}
