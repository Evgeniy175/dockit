export const UPCOMING_ITEMS_TYPES = {
  EVENT: 'event',
  SCHEDULE: 'schedule'
};

export const UPCOMING_KEY_FORMATTER = 'ddd MMM D';
export const UPCOMING_KEY_FORMATTER_TEMP = 'ddd MMM D YYYY';
