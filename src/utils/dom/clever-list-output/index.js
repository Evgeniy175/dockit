import { DEFAULT_DELTA, DEFAULT_CONTAINER, DEFAULT_ROW_HEIGHT } from './constants';



export const calculateIndexes = ({ list, delta, container, rowHeight }) => {
  if (!list || !list.length) return null;

  delta = delta || DEFAULT_DELTA;
  container = container || DEFAULT_CONTAINER;
  rowHeight = rowHeight || DEFAULT_ROW_HEIGHT;

  const scrollTop = container.scrollTop;
  const scrollBottom = scrollTop + window.innerHeight;

  return {
    startIdx: Math.max(0, Math.floor(scrollTop / rowHeight) - delta),
    endIdx: Math.min(list.length, Math.floor(scrollBottom / rowHeight) + delta),
  };
};
