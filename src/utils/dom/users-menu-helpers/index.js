import jq from '../../jquery';
import {values} from '../../object';

import {CLASSES} from '../../../shared-components/users/menu/constants';

export const isTargetIcon = e => {
  const classes = values(CLASSES);
  const target = jq.wrap(e.target);
  return classes.some(className => target.hasClass(className));
};
