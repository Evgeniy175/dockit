export const EVENT_TYPES = {
  TOUCH: 'touch',
  KEYBOARD: 'keyboard',
  WHEEL: 'wheel',
  UNKNOWN: 'unknown',
};
