import jq from '../../../jquery';

import { MODULE_NAME, DEFAULT_OPTIONS, EVENTS, EVENT_ACTIONS } from './constants';
import { KEY_CODES } from '../../../../constants';



export default class LoadMoreHelper {
  constructor(options) {
    Object.assign(this, DEFAULT_OPTIONS, options);
    this.eventHandlers = {
      [EVENTS.WHEEL.name]: this.onWheelHandler.bind(this, EVENTS.WHEEL),
      [EVENTS.KEY_DOWN.name]: this.onKeyDownHandler.bind(this, EVENTS.KEY_DOWN),
      [EVENTS.TOUCH_START.name]: this.onTouchStartHandler.bind(this, EVENTS.TOUCH_START),
      [EVENTS.TOUCH_MOVE.name]: this.onTouchMoveHandler.bind(this, EVENTS.TOUCH_MOVE),
      [EVENTS.TOUCH_END.name]: this.onTouchEndHandler.bind(this, EVENTS.TOUCH_END),
      [EVENTS.TOUCH_CANCEL.name]: this.onTouchCancelHandler.bind(this, EVENTS.TOUCH_CANCEL),
    };
    Object.keys(EVENTS).forEach(key => this.handleEvent(key, EVENT_ACTIONS.ADD));
  }

  onWheelHandler(event, e) {
    if (!this.doWork) return this.continueHandling(event, e);
    if (!this.isNeedToLoadMore(event)) return;
    this.continueHandling(event, e);
  }

  onKeyDownHandler(event, e) {
    if (!this.doWork) return this.continueHandling(event, e);
    if (e.keyCode !== KEY_CODES.PAGE_DOWN || !this.isNeedToLoadMore(event)) return;
    const target = jq.wrap(e.target);
    const isForSearch = target.hasClass('navbar-search-box-container') || target.hasParents('.navbar-search-box-container');
    const isModalTarget = target.hasClass('modal');
    if (isModalTarget || isForSearch) return;
    this.continueHandling(event, e);
  }

  onTouchStartHandler(event, e) {
    this.continueHandling(event, e);
  }

  onTouchMoveHandler(event, e) {
    if (!this.doWork) return this.continueHandling(event, e);
    if (!this.isNeedToLoadMore(event)) return;
    this.continueHandling(event, e);
  }

  onTouchEndHandler(event, e) {
    this.continueHandling(event, e);
  }

  onTouchCancelHandler(event, e) {
    this.continueHandling(event, e);
  }

  isNeedToLoadMore() {
    const container = this.container;
    if (!container) return;
    const fullHeight = container.scrollHeight;
    const currentScrollHeight = container.scrollTop + container.clientHeight;
    return fullHeight - currentScrollHeight <= this.bottomOffset;
  }

  continueHandling(event, e) {
    if (this.handlers && this.handlers[event.name]) this.handlers[event.name](e, event);
  }

  dispose() {
    Object.keys(EVENTS).forEach(key => this.handleEvent(key, EVENT_ACTIONS.REMOVE));

    this.container = null;
    this.keyContainer = null;
    this.eventHandlers = null;
  }

  handleEvent(key, action) {
    const event = EVENTS[key];
    if (!event) return this.printError(`event not found for key '${key}'`);
    if (!this.eventHandlers[event.name]) return this.printError(`event listener not found for '${event.name}'`);
    const container = this.getContainer(EVENTS[key]);
    if (!container || !container[action]) return this.printError(`container or action not found. Expected action: '${action}'`);
    container[action](event.name, this.eventHandlers[event.name]);
  }

  getContainer(event) {
    const containerKey = event.container;
    return this[containerKey];
  }

  printError(text) {
    console.error(`${MODULE_NAME}: ${text}`);
  }
}
