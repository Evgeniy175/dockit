export const MODULE_NAME = 'Load more helper';

export const EVENTS = {
  WHEEL: {
    name: 'mousewheel',
    container: 'container',
  },
  KEY_DOWN: {
    name: 'keydown',
    container: 'keyContainer',
  },
  TOUCH_START: {
    name: 'touchstart',
    container: 'container',
  },
  TOUCH_MOVE: {
    name: 'touchmove',
    container: 'container',
  },
  TOUCH_END: {
    name: 'touchend',
    container: 'container',
  },
  TOUCH_CANCEL: {
    name: 'touchcancel',
    container: 'container',
  },
};

export const EVENT_ACTIONS = {
  ADD: 'addEventListener',
  REMOVE: 'removeEventListener',
};

export const DEFAULT_OPTIONS = {
  doWork: true, // if true, then module will handle all necessary operations. Otherwise - just pass to the creator component
  container: document.body,
  keyContainer: document.body,
  bottomOffset: screen.height * 5, // px
};
