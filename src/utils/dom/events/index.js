import { EVENT_TYPES } from './constants';

export const getEventType = e => {
  if (e instanceof TouchEvent) return EVENT_TYPES.TOUCH;
  else if (e instanceof WheelEvent) return EVENT_TYPES.WHEEL;
  else if (e instanceof KeyboardEvent) return EVENT_TYPES.KEY;
  return EVENT_TYPES.UNKNOWN;
};
