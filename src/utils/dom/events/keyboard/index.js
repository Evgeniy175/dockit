import { KEY_CODES } from '../../../../constants';

export const isArrow = e => {
  if (!e || !e.keyCode) return false;
  const { keyCode } = e;
  return keyCode === KEY_CODES.UP_ARROW
      || keyCode === KEY_CODES.RIGHT_ARROW
      || keyCode === KEY_CODES.DOWN_ARROW
      || keyCode === KEY_CODES.LEFT_ARROW;
};
