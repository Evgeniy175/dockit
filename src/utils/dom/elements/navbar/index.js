import jq from '../../../jquery';

export const getNavBarHeight = () => {
  const elem = jq.wrap('.dockit-navbar');
  return elem.offsetHeight;
};
