import { isAndroid } from '../../device';

export const setLocation = uri => {
  if (isAndroid()) return document.location = uri;
  window.location = uri;
};

export const getLocation = () => isAndroid() ? document.location : window.location;
