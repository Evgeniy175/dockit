export const share = data => {
  if (!isSupportedShareApi()) return Promise.resolve();
  return navigator.share(data)
  .then(() => console.log('Successful share'))
  .catch((error) => console.log('Error sharing', error));
};

export const isSupportedShareApi = () => !!navigator.share;
