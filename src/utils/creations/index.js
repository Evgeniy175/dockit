import { PERMISSIONS, LIST_OF_PERMISSIONS } from './constants';
import { TYPES } from '../../views/creations/constants';

import { values } from '../object';
import { getLocation } from '../dom/window';

export const isPrivate = permission => permission === PERMISSIONS.PRIVATE;

export const recognizeCreationType = () => {
  let type;
  const types = values(TYPES);

  try {
    type = getLocation().pathname.split('/').filter(item => !!item)[0];
  } catch(e) {
    console.error('Error on creation type recognizing.');
  }

  if (!types.includes(type)) throw new Error('Creation type not recognized.');
  return type;
};
