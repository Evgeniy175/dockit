export const PERMISSIONS = {
  PUBLIC: 'public',
  FOLLOWERS: 'followers',
  PRIVATE: 'private',
};

export const LIST_OF_PERMISSIONS = [
  PERMISSIONS.PUBLIC,
  PERMISSIONS.FOLLOWERS,
  PERMISSIONS.PRIVATE,
];
