const Promise = require('bluebird');
import 'whatwg-fetch'; // Polyfill for fetch
import param from 'jquery-param';

// This establishes a private namespace.
const namespace = new WeakMap();
function p(object) {
  if (!namespace.has(object)) namespace.set(object, {});
  return namespace.get(object);
}


/**
 *
 */
export default class Http {
  constructor() {
    p(this).preRequests = new Set();
    p(this).postResponse = {
      successes: new Set(),
      errors: new Set()
    };
  }
  
  addPreRequest(func) {
    p(this).preRequests.add(func);
  }
  
  addPostResponse(func) {
    p(this).postResponse.successes.add(func);
  }
  
  addPostResponseError(func) {
    p(this).postResponse.errors.add(func);
  }
  
  generateQueryParams(req) {
    if (req.params) {
      req.url += `?${param(req.params)}`;
    }
    return req.url;
  }
  
  get(req = {}) {
    req.headers = req.headers || {};
    req.url = this.generateQueryParams(req);
    return this._wrap(req, () => fetch(req.url, req));
  }
  
  post(req = {}) {
    req.headers = req.headers || {};
    return this._wrap(req, () => fetch(req.url, req));
  }
  
  put(req = {}) {
    req.headers = req.headers || {};
    return this._wrap(req, () => fetch(req.url, req));
  }
  
  delete(req = {}) {
    req.headers = req.headers || {};
    return this._wrap(req, () => fetch(req.url, req));
  }
  
  _wrap(payload, func) {
    payload = payload || {};
    let chain = Promise.resolve(payload);
    // Call prerequests.
    p(this).preRequests.forEach(func => {
      chain = chain.then(result => func(result || payload));
    });
    
    
    let res;
    // Make actual HTTP call.
    chain = chain.then(result => func(result || payload))
    .then(response => {
      res = response;
      if (response.status >= 400) {
        let err = new Error(`Error response status code: ${res.status}`);
        err.res = res;
        return response.text()
        .then((text) => {
          response.text = text;
          return Promise.reject(err);
        })
      }

      return response.json()
      .then(json => {
        response.json = json;
        return payload.fullResponse ? response : json;
      })
    });
    
    // Call postrequests.
    chain = chain.then(response => {
      let chain = Promise.resolve(response);
      p(this).postResponse.successes.forEach(func => {
        chain = chain.then(result => func(result || response));
      });
      return chain;
    });
    chain = chain.catch(err => {
      let chain = Promise.reject(err);
      p(this).postResponse.errors.forEach(func => {
        chain = chain.catch(err => func(err));
      });
      return chain;
    });
    
    return Promise.resolve(chain);
  }
}
