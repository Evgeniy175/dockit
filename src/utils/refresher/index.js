import { DEFAULT_CONFIG } from './constants';



export default class Refresher {
  intervalId;

  constructor(config = {}) {
    if (!config.cb) return;
    const data = Object.assign({}, DEFAULT_CONFIG, config);
    this.intervalId = setInterval(data.cb, data.timeout);
  }

  clear() {
    clearInterval(this.intervalId);
  }
}
