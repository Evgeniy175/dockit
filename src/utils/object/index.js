export const values = object => Object.keys(object).map(key => object[key]);
