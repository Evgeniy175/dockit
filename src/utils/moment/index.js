import moment from 'moment-timezone';
import { minBy, get } from 'lodash';

import { DATE_FORMATTER } from './constants';



export const getNumWeeksInMonth = momentObj => {
  const start = moment(momentObj).startOf('month').startOf('week');
  const end = moment(momentObj).endOf('month').endOf('week');
  return end.diff(start, 'week') + 1;
};

export const getClosest = (target, items) => {
  if (!Array.isArray(items)) return;

  const diffs = items.map(item => ({
    diff: Math.abs(moment(item).diff(target)),
    item,
  }));

  return minBy(diffs, 'diff').item;
};

export const isSameDates = (d1, d2) => d1.format(DATE_FORMATTER) === d2.format(DATE_FORMATTER);

export const order = arr => arr.sort((a, b) => a - b);
export const orderBy = (arr, path) => arr.sort((a, b) => get(a, path) - get(b, path));
