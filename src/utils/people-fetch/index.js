import Promise from 'bluebird';
import { get } from 'lodash';
import deepAssign from 'deep-assign';

import UserModel from '../../models/users';

const userModel = new UserModel();

import { REQUEST_TYPES } from '../../shared-components/action-command/dock/modal/constants';
import { REQUEST_TARGETS, REQUEST_TARGETS_TYPES } from './constants';



const FETCH_RESOLVERS = {
  [REQUEST_TYPES.INTEND]: {
    [REQUEST_TARGETS.FOLLOWINGS]: ::userModel.fetchFollowingsWhoIntended,
    [REQUEST_TARGETS.NON_FOLLOWINGS]: ::userModel.fetchNonFollowingsWhoIntended,
  },
  [REQUEST_TYPES.INVITE]: {
    [REQUEST_TARGETS.FOLLOWINGS]: ::userModel.fetchFollowingsWhoInvited,
    [REQUEST_TARGETS.NON_FOLLOWINGS]: ::userModel.fetchNonFollowingsWhoInvited,
  },
  [REQUEST_TYPES.SAVE]: {
    [REQUEST_TARGETS.FOLLOWINGS]: ::userModel.fetchFollowingsWhoSaved,
    [REQUEST_TARGETS.NON_FOLLOWINGS]: ::userModel.fetchNonFollowingsWhoSaved,
  },
};



/** Presents people fetching functionality */
export default class PeopleFetch {
  /**
   * Do initial request for fetch people
   * @param {object} values - contains values for request
   * @param {object} handlers - contains handlers for handle results
   * @returns {Promise} Promise that represents people fetching method
   */
  fetch(values, handlers) {
    const { setFollowings, setNonFollowings } = handlers;
    setFollowings();
    setNonFollowings();

    return this.fetchFollowings(values)
    .spread((eventFollowings, scheduleFollowings) => this.handleFollowings(handlers, eventFollowings, scheduleFollowings))
    .then(realDataLength => this.fetchNonFollowings(values, realDataLength))
    .spread((eventNonFollowings, scheduleNonFollowings) => setNonFollowings(eventNonFollowings, scheduleNonFollowings));
  }

  /**
   * Send fetch request for retrieve followings
   * @param {object} values - contains values for request
   * @returns {Promise} Promise that represents followings requests
   */
  fetchFollowings(values) {
    const { requestType, eventId, scheduleId, config } = values;
    const fetcher = FETCH_RESOLVERS[requestType][REQUEST_TARGETS.FOLLOWINGS];
    return Promise.all([
      eventId ? fetcher(REQUEST_TARGETS_TYPES.EVENTS, eventId, deepAssign({}, config)) : Promise.resolve(),
      scheduleId ? fetcher(REQUEST_TARGETS_TYPES.SCHEDULES, scheduleId, deepAssign({}, config)) : Promise.resolve(),
    ]);
  }

  /**
   * Handler for attach followings after initial request
   * @param {object} handlers - contains handlers for handle results
   * @param {object} eventFollowings - event followings response
   * @param {object} scheduleFollowings - schedule followings response
   * @returns {Promise} Promise that returns total fetched items count
   */
  handleFollowings(handlers, eventFollowings, scheduleFollowings) {
    handlers.setFollowings(eventFollowings, scheduleFollowings);
    return Promise.resolve(get(eventFollowings, 'data.length', 0) + get(scheduleFollowings, 'data.length', 0));
  }

  /**
   * Send fetch request for retrieve non-followings
   * @param {object} values - contains values for request
   * @param {number} followingsLength - count of fetched items
   * @returns {Promise} Promise that represents non-followings requests
   */
  fetchNonFollowings(values, followingsLength) {
    const { requestType, eventId, scheduleId, config, limit } = values;
    const fetcher = FETCH_RESOLVERS[requestType][REQUEST_TARGETS.NON_FOLLOWINGS];

    return followingsLength < limit
    ? Promise.all([
      eventId ? fetcher(REQUEST_TARGETS_TYPES.EVENTS, eventId, deepAssign({}, config)) : Promise.resolve(),
      scheduleId ? fetcher(REQUEST_TARGETS_TYPES.SCHEDULES, scheduleId, deepAssign({}, config)) : Promise.resolve(),
    ])
    : Promise.resolve([]);
  }

  /**
   * Do fetch request for retrieve more people
   * @param {object} values - contains values for request
   * @param {object} handlers - contains handlers for handle results
   * @returns {Promise} Promise that represents people fetching method
   */
  fetchMore(values, handlers) {
    const { attachNonFollowings } = handlers;

    return this.fetchMoreFollowings(values)
    .spread((eventFollowings, scheduleFollowings) => this.handleFollowingsAttaching(handlers, eventFollowings, scheduleFollowings))
    .then(res => this.fetchMoreNonFollowings(values, res))
    .then(res => attachNonFollowings(res.eventNonFollowings, res.scheduleNonFollowings));
  }

  /**
   * Send fetch request for retrieve more followings
   * @param {object} values - contains values for request
   * @returns {Promise} Promise that represents fetch more followings requests
   */
  fetchMoreFollowings(values) {
    const { requestType, eventId, scheduleId } = values;
    const requestTarget = REQUEST_TARGETS.FOLLOWINGS;
    const fetcher = FETCH_RESOLVERS[requestType][requestTarget];
    const eventConfig = eventId ? this.getConfigForFetchMore(values, requestTarget, REQUEST_TARGETS_TYPES.EVENTS) : null;
    const scheduleConfig = scheduleId ? this.getConfigForFetchMore(values, requestTarget, REQUEST_TARGETS_TYPES.SCHEDULES) : null;

    return Promise.all([
      eventId ? fetcher(REQUEST_TARGETS_TYPES.EVENTS, eventId, eventConfig) : Promise.resolve(),
      scheduleId ? fetcher(REQUEST_TARGETS_TYPES.SCHEDULES, scheduleId, scheduleConfig) : Promise.resolve(),
    ]);
  }

  /**
   * Handler for attach followings after fetch more request
   * @param {object} handlers - contains handlers for handle results
   * @param {object} eventFollowings - event followings response
   * @param {object} scheduleFollowings - schedule followings response
   * @returns {Promise} Promise that returns total fetch results
   */
  handleFollowingsAttaching(handlers, eventFollowings, scheduleFollowings) {
    handlers.attachFollowings(eventFollowings, scheduleFollowings);
    return Promise.resolve({
      eventFollowings,
      scheduleFollowings,
    });
  }

  /**
   * Send fetch request for retrieve more non-followings
   * @param {object} values - contains values for request
   * @param {object} res - contains result of 'fetch more followings' request
   * @returns {Promise} Promise that represents fetch more non-followings requests
   */
  fetchMoreNonFollowings(values, res) {
    const { requestType, eventId, scheduleId, limit } = values;
    const requestTarget = REQUEST_TARGETS.NON_FOLLOWINGS;
    const fetcher = FETCH_RESOLVERS[requestType][requestTarget];
    const totalFetchedLength = get(res, 'eventFollowings.data.length', 0) + get(res, 'scheduleFollowings.data.length', 0);

    if (totalFetchedLength >= limit) return Promise.resolve({ eventNonFollowings: { limitExceeded: true }, scheduleNonFollowings: { limitExceeded: true } });

    const dataForOverride = {
      params: {
        limit: limit - totalFetchedLength,
      },
    };

    const eventConfig = eventId ? this.getConfigForFetchMore(values, requestTarget, REQUEST_TARGETS_TYPES.EVENTS, dataForOverride) : null;
    const scheduleConfig = scheduleId ? this.getConfigForFetchMore(values, requestTarget, REQUEST_TARGETS_TYPES.SCHEDULES, dataForOverride) : null;

    return Promise.all([
      eventConfig ? fetcher(REQUEST_TARGETS_TYPES.EVENTS, eventId, eventConfig) : Promise.resolve(),
      scheduleConfig ? fetcher(REQUEST_TARGETS_TYPES.SCHEDULES, scheduleId, scheduleConfig) : Promise.resolve(),
    ])
    .spread((eventNonFollowings, scheduleNonFollowings) => {
      if (eventNonFollowings) eventNonFollowings.limit = totalFetchedLength;
      if (scheduleNonFollowings) scheduleNonFollowings.limit = totalFetchedLength;
      return Promise.resolve({ eventNonFollowings, scheduleNonFollowings });
    });
  }

  /**
   * Get config for fetch more items
   * @param {object} values - contains values for request
   * @param {string} reqTarget - request target
   * @param {string} type - type of request
   * @param {object} dataForOverride - data that will have max priority on assigning
   * @returns {object} Config for request
   */
  getConfigForFetchMore(values, reqTarget, type, dataForOverride = {}) {
    const { sizes, config } = values;
    const offsetData = {
      params: {
        offset: sizes[reqTarget][type],
      },
    };

    return deepAssign({}, config, offsetData, dataForOverride);
  }
}
