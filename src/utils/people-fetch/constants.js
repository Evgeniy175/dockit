import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../models/events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../models/schedules/constants';

export const REQUEST_TARGETS = {
  FOLLOWINGS: 'followings',
  NON_FOLLOWINGS: 'non-followings',
};

export const REQUEST_TARGETS_TYPES = {
  EVENTS: EVENTS_MODEL_PLURAL,
  SCHEDULES: SCHEDULES_MODEL_PLURAL,
};
