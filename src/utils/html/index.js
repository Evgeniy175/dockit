export const htmlToElements = html => {
  const template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
};
