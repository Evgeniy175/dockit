import { get } from 'lodash';

export const isNameValid = name => get(name, 'length') > 0;

export const isEndAfterStartDateTime = (start, end) => isMomentValid(start) && isMomentValid(end) && start.isBefore(end);

export const isMomentValid = momentObj => momentObj && momentObj.isValid && momentObj.isValid();
