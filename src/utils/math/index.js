export const closest = (value, arr) => {
  return arr.reduce((prev, curr) => Math.abs(curr - value) < Math.abs(prev - value) ? curr : prev);
};
