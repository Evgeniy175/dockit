import cron from 'cron';
import moment from 'moment-timezone';
import Promise from 'bluebird';
import { get } from 'lodash';

import { NOTIFICATIONS_INTERVAL, WEB_NOTIFICATIONS_KEY } from './constants';
import { DATE_FORMAT } from '../../views/upcoming/constants';
import { MOMENT_UNITS, TEXTS_FOR_ONE, TEXTS_FOR_MANY } from '../../shared-components/alerts-modal/constants';
import { orderBy } from '../../utils/moment';
import { Auth } from '../../auth';
import { APP_BASE } from '../../constants';
import { MODEL_PLURAL as EVENTS_MODEL_PLURAL } from '../../models/events/constants';
import { MODEL_PLURAL as SCHEDULES_MODEL_PLURAL } from '../../models/schedules/constants';

import EventsModel from '../../models/events';
import UsersModel from '../../models/users';

const eventModel = new EventsModel();
const userModel = new UsersModel();
const CronJob = cron.CronJob;

let now = moment();
let defaultAlerts = [];
let jobs = [];



export const requestNotificationsPermission = () => {
  return new Promise((resolve, reject) => {
    Notification.requestPermission(result => resolve(console.log(`Notification permission ${result}.`)));
  });
};



export const initNotifications = () => {
  if (isWebNotificationsDisabled()) { console.error('Web notifications is disabled.'); return Promise.resolve(); }

  console.log('Notifications init started.');
  const startDate = moment().format(DATE_FORMAT);
  const endDate = moment().add(2, 'weeks').endOf('day').format(DATE_FORMAT);
  const user = Auth.getActiveUser();
  const userId = get(user, 'id');

  if (!userId) { console.error('Notifications init cancelled: unauthorised.'); return Promise.resolve(); }

  return Promise.all([
    eventModel.fetchUpcoming(startDate, endDate),
    userModel.fetchProfileById(userId),
  ])
  .spread(prepare)
  .then(convertToNotifications)
  .then(getNearestNotifications)
  .then(notifications => orderBy(notifications, 'notifyAt'))
  .then(createNotificationsJobs)
  .then(setJobs);
};

export const isWebNotificationsDisabled = () => localStorage.getItem(WEB_NOTIFICATIONS_KEY) === 'true';
export const setIsWebNotificationsDisabled = isDisabled => {
  localStorage.setItem(WEB_NOTIFICATIONS_KEY, isDisabled);
  if (isDisabled) removeJobs();
  else initNotifications();
};

const prepare = (items, user) => {
  console.log('Notifications data fetched.');
  removeJobs();
  now = moment();
  defaultAlerts = get(user, 'data.defaultAlerts');
  return items.map(event => handleItem(event));
};

const handleItem = item => {
  let isEventAlertsExists = isAlertsExists(item);
  if (!isEventAlertsExists && !isAlertsExists(item.schedule)) {
    item.myAlerts = defaultAlerts;
    isEventAlertsExists = true;
  }
  const alerts = isEventAlertsExists ? getAlerts(item) : getAlerts(item.schedule);
  return getEvent(item, alerts);
};

const getAlerts = event => {
  return event.myAlerts
  .map(alert => {
    if (!MOMENT_UNITS[alert.unit]) return;
    const notifyAt = moment(event.startTime).subtract(alert.amount, alert.unit);
    return notifyAt.isBefore(now) ? null : { alert, notifyAt };
  })
  .filter(alert => !!alert);
};

const isAlertsExists = event => get(event, 'myAlerts.length', 0) > 0;

const getEvent = (event, alerts) => {
  return {
    id: event.id,
    scheduleId: event.schedule_id,
    title: event.title,
    alerts,
  };
};

const convertToNotifications = events => {
  return events.reduce((arr, { alerts, id, scheduleId, title }) => {
    alerts.forEach(alert => {
      if (alert) arr.push({ id, scheduleId, title, ...alert });
    });
    return arr;
  }, []);
};

const getNearestNotifications = notifications => {
  const max = moment(now).add(NOTIFICATIONS_INTERVAL.amount, NOTIFICATIONS_INTERVAL.unit);
  return notifications.filter(notification => notification.notifyAt.isBetween(now, max));
};

const createNotificationsJobs = notifications => {
  return notifications.map(notification => {
    return new CronJob(
      notification.notifyAt.toDate(),
      () => displayNotification(getData(notification)),
      null, // This function is executed when the job stops
      true, /* Start the job right now */
      moment.tz.guess(), /* Time zone of this job. */
    );
  });
};

const displayNotification = data => {
  if (Notification.permission !== 'granted') return requestNotificationsPermission();

  const options = {
    body: data.text,
    icon: '/dockit-icon.png',
    vibrate: [100, 50, 100],
    data: data.data,
  };
  const notification = new Notification(data.title, options);
  const isSchedule = !!data.data.scheduleId;
  const id = isSchedule ? data.data.scheduleId : data.data.id;

  if (!id) return notification.close();

  const target = isSchedule ? SCHEDULES_MODEL_PLURAL : EVENTS_MODEL_PLURAL;
  
  notification.onclick = () => {
    window.open(`${APP_BASE}/${target}/${id}`);
    notification.close();
  };
};

const getData = notification => {
  return {
    title: getNotificationTitle(notification),
    text: getNotificationText(notification),
    data: getNotificationData(notification),
  }
};

const getNotificationTitle = notification => {
  return `Reminder: ${notification.title}`;
};

const getNotificationText = notification => {
  const { unit, amount } = notification.alert;
  if (amount === 0) return 'Right now';
  return `In ${amount} ${amount === 1 ? TEXTS_FOR_ONE[unit] : TEXTS_FOR_MANY[unit]}`;
};

const getNotificationData = notification => {
  return {
    id: notification.id,
    scheduleId: notification.scheduleId,
  };
};

export const removeJobs = () => {
  jobs.forEach(job => job.stop());
  console.log('Old notifications jobs stopped.');
};

const setJobs = value => {
  jobs = value;
  console.log('Notifications init complete.');
};
