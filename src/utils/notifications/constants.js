export const NOTIFICATIONS_INTERVAL = {
  unit: 'days',
  amount: 10,
};

export const WEB_NOTIFICATIONS_KEY = 'web-notifications-disabled';
