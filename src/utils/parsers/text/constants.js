export const MODULE_NAME = 'Text parser';

export const ERRORS = {
  NO_POSITION_FUNC: name => `${MODULE_NAME}: position func not found for '${name}'`,
  NO_RENDER_FUNC: name => `${MODULE_NAME}: render func not found for '${name}'`,
};

export const TYPES = {
  STRING: 'string',
  EMAIL: 'emails',
  USERTAG: 'usertags',
  MARKDOWN: 'markdowns',
  URI: 'uris',
  HASHTAG: 'hashtags',
  NEW_LINE: 'new lines',
};

export const DEFAULT_PROTOCOL = 'https://';

export const USERTAG_START = '@';
export const HASHTAG_START = '#';

export const VALID_HREF_PREFIX_REGEX = /^(([A-Za-z]{3,9}:\/\/)|(mailto:))/;
const URI_REGEX_BODY = '((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[\\-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9\\.\\-]+|(?:www\\.|[\\-;:&=\\+\\$,\\w]+@)[A-Za-z0-9\\.\\-]+)((?:\\/[\\+~%\\/\\.\\w\\-_]*)?\\??(?:[\\-\\+=&;%@\\.\\w_]*)#?(?:[\\.\\!\\/\\\\\\w]*))?)';

export const EMAIL_REGEX = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/igm;

const MARKDOWN_TEXT_REGEX_BODY = '[\\w|\\s]+';
export const MARKDOWN_REGEXES = {
  MAIN: new RegExp(`\\[${MARKDOWN_TEXT_REGEX_BODY}\\]\\(${URI_REGEX_BODY}\\)`, 'gi'),
  TEXT: new RegExp(`(?!\\[)${MARKDOWN_TEXT_REGEX_BODY}(?=\\])`, 'gi'),
  URI: new RegExp(`(?!\\()${URI_REGEX_BODY}(?=\\))`, 'gi'),
};

export const URI_REGEX = new RegExp(URI_REGEX_BODY, 'gi');
export const HASHTAG_REGEX = new RegExp(`(^|(?![\\s]+))(${HASHTAG_START}\\w+)`, 'gi');
export const NEW_LINE_REGEX = /\n/gi;

export const REGEXES = {
  [TYPES.EMAIL]: EMAIL_REGEX,
  [TYPES.MARKDOWN]: MARKDOWN_REGEXES.MAIN,
  [TYPES.URI]: URI_REGEX,
  [TYPES.HASHTAG]: HASHTAG_REGEX,
  [TYPES.NEW_LINE]: NEW_LINE_REGEX,
};

export const DEFAULT_OPTIONS = {
  order: [TYPES.EMAIL, TYPES.USERTAG, TYPES.MARKDOWN, TYPES.URI, TYPES.HASHTAG, TYPES.NEW_LINE],
  string: '',
  userTags: [],
  userTagStart: USERTAG_START,
};

export const DEFAULT_RESULT = null;
