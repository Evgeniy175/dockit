import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactGA from 'react-ga';



class Anchor extends Component {
  constructor(props) {
    super(props);
    this.onClick = ::this.onClickHandler;
  }

  onClickHandler() {
    const { analyticsData } = this.props;
    if (analyticsData) ReactGA.event(analyticsData);
  }

  render() {
    const { text, href } = this.props;
    return <a href={href} target='_blank' onClick={this.onClick}>{text}</a>;
  }
}

Anchor.propTypes = {
  values: PropTypes.shape({
    href: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    analyticsData: PropTypes.object,
  }),
  handlers: PropTypes.shape({
    onClick: PropTypes.func,
  }),
};

export default Anchor;
