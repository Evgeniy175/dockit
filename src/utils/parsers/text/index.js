import React from 'react';
import { Link } from 'react-router';

import Anchor from './anchor';

import { getRandomInt } from '../../random';

import { ERRORS, TYPES, DEFAULT_OPTIONS, DEFAULT_RESULT, DEFAULT_PROTOCOL, REGEXES, MARKDOWN_REGEXES, VALID_HREF_PREFIX_REGEX } from './constants';



export default class TextParser {
  constructor(props) {
    this.init(props);
    this.positionResolvers = {
      [TYPES.EMAIL]: ::this.getEmailsPositions,
      [TYPES.USERTAG]: ::this.getUserTagsPositions,
      [TYPES.MARKDOWN]: ::this.getMarkdownsPositions,
      [TYPES.URI]: ::this.getUrisPositions,
      [TYPES.HASHTAG]: ::this.getHashTagsPositions,
      [TYPES.NEW_LINE]: ::this.getNewLinesPositions,
    };
    this.renderResolvers = {
      [TYPES.STRING]: ::this._renderString,
      [TYPES.EMAIL]: ::this._renderEmail,
      [TYPES.USERTAG]: ::this._renderUserTag,
      [TYPES.MARKDOWN]: ::this._renderMarkdown,
      [TYPES.URI]: ::this._renderUri,
      [TYPES.HASHTAG]: ::this._renderHashTag,
      [TYPES.NEW_LINE]: ::this._renderNewLine,
    };
  }

  /**
   * Performs init of the parser
   * @param props
   */
  init(props) {
    Object.assign(this, DEFAULT_OPTIONS, props);
    this._prepareUserTags();
  }

  /**
   * Perform cleaning
   */
  clear() {
    this.positionResolvers = null;
    this.renderResolvers = null;
    this.order = null;
    this.string = null;
    this.userTags = null;
    this.userTagStart = null;
  }

  /**
   * Prepares user tags
   * @private
   */
  _prepareUserTags() {
    if (!this.userTags) return;
    this.userTags = new Map(this.userTags.map(({ user }) => [`${this.userTagStart}${user.username}`, user.id]));
  }

  /**
   * Do parsing
   * @returns {Array} - array of React items
   */
  parse() {
    if (!this.string || !this.order) return DEFAULT_RESULT;
    const positions = this._calculateItemsPositions();
    return this._renderItems(positions);
  }

  /**
   * Calculates items positions
   * @returns {Array} - array of items with their positions
   * @private
   */
  _calculateItemsPositions() {
    let positions = this.order.reduce((previousValue, currentValue, i) => {
      if (!this.positionResolvers[this.order[i]]) {
        console.error(ERRORS.NO_POSITION_FUNC(this.order[i]));
        return previousValue;
      }
      const result = this.positionResolvers[this.order[i]]();
      if (!result || result.length === 0) return previousValue;
      return previousValue.concat(result);
    }, []);
    positions = this._removeIntersections(positions);
    positions = this._sortPositions(positions);
    return this._calculateTextPositions(positions);
  }

  /**
   * Removes intersections in positions
   * @param {Array} positions
   * @returns {Array} - handled positions
   * @private
   */
  _removeIntersections(positions) {
    for (let i = 0; i < positions.length; i++)
      for (let j = i + 1; j < positions.length; j++)
        if (positions[i] && positions[j] && this._isIntersection(positions[i], positions[j]))
          positions[j] = null;
    return positions.filter(item => item);
  }

  /**
   *
   * @param top
   * @param bottom
   * @returns {boolean}
   * @private
   */
  _isIntersection(top, bottom) {
    const borders = {
      min: top.startAt,
      max: top.startAt + top.value.length - 1,
    };
    return this._isValueIn(bottom.startAt, borders) || this._isValueIn(bottom.startAt + bottom.value.length - 1, borders);
  }

  /**
   * Indicates, is value between min and max
   * @param value
   * @param min
   * @param max
   * @returns {boolean} - true if value between min and max
   * @private
   */
  _isValueIn(value, { min, max }) {
    return value >= min && value <= max;
  }

  /**
   * Calculates positions of common text
   * @param {Array} positions
   * @private
   */
  _calculateTextPositions(positions) {
    const textValues = [];
    const textBefore = this.getTextBefore(positions);
    if (textBefore) textValues.push(textBefore);

    for (let i = 0; i < positions.length - 1; i++) {
      const text = this.getTextBetween(positions[i], positions[i + 1]);
      if (text) textValues.push(text);
    }

    const textAfter = this.getTextAfter(positions);
    if (textAfter) textValues.push(textAfter);
    return this._sortPositions(positions.concat(textValues));
  }

  /**
   * Returns text before first position
   * @param positions
   * @returns {{value: string, startAt: number}}
   * @private
   */
  getTextBefore(positions) {
    if (!positions || (positions.length > 0 && positions[0].startAt === 0)) return;
    if (positions.length === 0) return this._decorateOne(this.string, 0, { type: TYPES.STRING });
    const value = this.string.substr(0, positions[0].startAt);
    return this._decorateOne(value, 0, { type: TYPES.STRING });
  }

  /**
   * Returns text after last position
   * @param positions
   * @returns {{value: string, startAt: number}}
   * @private
   */
  getTextAfter(positions) {
    if (!positions || positions.length === 0) return;
    const last = positions[positions.length - 1];
    const startAt = last.startAt + last.value.length;
    if (startAt === this.string.length) return;
    const value = this.string.substr(startAt, this.string.length);
    return this._decorateOne(value, startAt, { type: TYPES.STRING });
  }

  /**
   * Returns text between 2 items
   * @param position1
   * @param position2
   * @returns {{value: *, startAt: *}}
   * @private
   */
  getTextBetween(position1, position2) {
    const type = TYPES.STRING;
    const startAt = position1.startAt + position1.value.length;
    const length = position2.startAt - startAt;
    const value = this.string.substr(startAt, length);
    return this._decorateOne(value, startAt, { type });
  }

  /**
   * Sorts positions by `startAt`
   * @param positions
   * @returns {Array.<T>|*}
   * @private
   */
  _sortPositions(positions) {
    return positions.sort((a, b) => a.startAt - b.startAt);
  }

  /**
   * Performs items render
   * @param positions
   * @private
   */
  _renderItems(positions) {
    return positions.map(position => {
      if (!this.renderResolvers[position.type]) {
        console.error(ERRORS.NO_RENDER_FUNC(position.type));
        return null;
      }
      return this.renderResolvers[position.type](position);
    });
  }

  /**
   * Handles emails
   * @returns {Array} - handled items
   * @private
   */
  getEmailsPositions() {
    return this.getData(TYPES.EMAIL);
  }

  /**
   * Handles user tags
   * @returns {Array} - handled items
   * @private
   */
  getUserTagsPositions() {
    const type = TYPES.USERTAG;
    if (!this.userTags || this.userTags.size === 0) return [];
    const userTags = Array.from(this.userTags.keys()).join('|');
    const re = new RegExp(userTags, 'gi');
    const matches = this.string.match(re);
    return this._decorate(matches, { type }).map(item => {
      return {
        id: this.userTags.get(item.value),
        ...item,
      };
    });
  }

  /**
   * Handles markdowns
   * @returns {Array} - handled items
   * @private
   */
  getMarkdownsPositions() {
    const markdowns = this.getData(TYPES.MARKDOWN);
    return markdowns.map(item => {
      return {
        data: {
          text: item.value.match(MARKDOWN_REGEXES.TEXT)[0],
          href: item.value.match(MARKDOWN_REGEXES.URI)[0],
        },
        ...item,
      };
    });
  }

  /**
   * Handles URIs
   * @returns {Array} - handled items
   * @private
   */
  getUrisPositions() {
    return this.getData(TYPES.URI);
  }

  /**
   * Handles hashtags
   * @returns {Array} - handled items
   * @private
   */
  getHashTagsPositions() {
    return this.getData(TYPES.HASHTAG);
  }

  /**
   * Handles new lines
   * @returns {Array} - handled items
   * @private
   */
  getNewLinesPositions() {
    return this.getData(TYPES.NEW_LINE);
  }

  /**
   * Matches and decorates data for specific type
   * @param {String} type - type of items that will be handled
   * @returns {Array} - handled items
   * @private
   */
  getData(type) {
    const matches = this.string.match(REGEXES[type]);
    return this._decorate(matches, { type });
  }

  /**
   * Decorates array by including indexes of items
   * @param {Array} arr - array of items
   * @param {Object} expandBy - object that will used for expand each result
   * @returns {Array} - decorated items
   * @private
   */
  _decorate(arr, expandBy) {
    if (!arr) return [];
    let startAt = 0;
    return arr.map((value, idx) => {
      startAt = this.string.indexOf(value, idx === 0 ? startAt : startAt + 1);
      return this._decorateOne(value, startAt, expandBy);
    });
  }

  /**
   * Decorates one item
   * @param value
   * @param startAt
   * @param expandBy
   * @returns {{value: string, startAt: number}}
   * @private
   */
  _decorateOne(value, startAt, expandBy) {
    return {
      value,
      startAt,
      ...expandBy,
    };
  }

  /**
   * Renders native string
   * @param data
   * @private
   */
  _renderString(data) {
    return data.value;
  }

  /**
   * Renders email link
   * @param data
   * @returns React object
   * @private
   */
  _renderEmail(data) {
    return this.getAnchor(`mailto:${data.value}`, data.value, `Mailing to: ${data.value}`);
  }

  /**
   * Renders usertag link
   * @param id
   * @param value
   * @returns React object
   * @private
   */
  _renderUserTag({ id, value }) {
    return this.getLink(`/users/${id}`, value);
  }

  /**
   * Renders markdown
   * @param data
   * @returns React object
   * @private
   */
  _renderMarkdown({ data }) {
    return this.getAnchor(data.href, data.text, `Redirect to: ${data.href}`);
  }

  /**
   * Renders external link
   * @param value
   * @returns React object
   * @private
   */
  _renderUri({ value }) {
    return this.getAnchor(value, value, `Redirect to: ${value}`);
  }

  /**
   * Renders hashtag
   * @param value
   * @returns React object
   * @private
   */
  _renderHashTag({ value }) {
    return this.getLink(`/discover?hashtag=${value.substring(1)}`, value);
  }

  /**
   * Renders new line
   * @returns React object
   * @private
   */
  _renderNewLine() {
    return <br key={getRandomInt()} />;
  }

  /**
   * Renders internal link
   * @param href
   * @param text
   * @returns React object
   * @private
   */
  getLink(href, text) {
    return <Link key={getRandomInt()} to={href}>{text}</Link>;
  }

  /**
   * Renders external link
   * @param href
   * @param text
   * @param action
   * @returns React object
   * @private
   */
  getAnchor(href, text, action) {
    href = VALID_HREF_PREFIX_REGEX.test(href) ? href : `${DEFAULT_PROTOCOL}${href}`;
    return <Anchor key={getRandomInt()} href={href} text={text} analyticsData={this.getGoogleAnalyticsData(action)} />;
  }

  /**
   * Returns Google Analytics data
   * @param action
   * @returns {{category: string, action: string}}
   * @private
   */
  getGoogleAnalyticsData(action) {
    return {
      category: 'Interaction with link',
      action,
    };
  }
}
