import { get } from 'lodash';

export function getLocation(item) {
  const location = get(item, 'location.description');

  if (location) return location;

  const city = get(item, 'location.city');
  const country = get(item, 'location.country');

  return city && country ? `${city}, ${country}` : '';
}