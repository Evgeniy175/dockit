import moment from 'moment';
import { INVALID_DATE, DATE_AFTER_NOW } from './constants';
import { TYPES as TIME_MODAL_ROW_CONSTANTS, FIELDS as TIME_MODAL_FIELDS } from '../../shared-components/action-command/dock/time-modal/time-select/row/constants';

const TIME_SELECT_HANDLERS = {
  [TIME_MODAL_ROW_CONSTANTS.ONE_BEFORE]: time => ({
    [TIME_MODAL_FIELDS.H]: formatHours(moment(time).subtract(1, 'hours')),
    [TIME_MODAL_FIELDS.M]: formatMinutes(moment(time).subtract(1, 'minutes')),
    [TIME_MODAL_FIELDS.A]: formatPartOfDay(time),
  }),
  [TIME_MODAL_ROW_CONSTANTS.CURRENT]: time => ({
    [TIME_MODAL_FIELDS.H]: formatHours(time),
    [TIME_MODAL_FIELDS.M]: formatMinutes(time),
    [TIME_MODAL_FIELDS.A]: formatPartOfDay(time),
  }),
  [TIME_MODAL_ROW_CONSTANTS.ONE_AFTER]: time => ({
    [TIME_MODAL_FIELDS.H]: formatHours(moment(time).add(1, 'hours')),
    [TIME_MODAL_FIELDS.M]: formatMinutes(moment(time).add(1, 'minutes')),
    [TIME_MODAL_FIELDS.A]: formatPartOfDay(time),
  }),
};

export function formatFromNow(value) {
  const now = moment();
  if (!value || !value.isValid()) return INVALID_DATE;
  if (value.isSameOrAfter(now)) return DATE_AFTER_NOW;
  return value.fromNow().toUpperCase();
}

export function format(type, time) {
  if (!TIME_SELECT_HANDLERS[type] || !moment.isMoment(time)) return;
  return TIME_SELECT_HANDLERS[type](time);
}

export function formatHours(time) {
  return doFormat(time, 'hh');
}

export function formatMinutes(time) {
  return doFormat(time, 'mm');
}

export function formatPartOfDay(time) {
  return doFormat(time, 'A');
}

function doFormat(time, format) {
  return moment(time).format(format);
}
