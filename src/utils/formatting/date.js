import { INTERVAL_DATE_FORMAT, TIME_FORMAT } from './constants';

import moment from 'moment';

export function formatInterval(start, end, formatter = INTERVAL_DATE_FORMAT) {
  if (!start.isValid || !end.isValid) return 'Not a moment objects';
  if (!start.isValid()) return 'Invalid start date';
  if (!end.isValid()) return 'Invalid end date';
  
  const isAllDay = start.isSame(end);
  return isAllDay ? 'All day' : `${start.format(formatter)} - ${end.format(formatter)}`;
}

export function formatDate(date, formatter = INTERVAL_DATE_FORMAT) {
  if (!date.isValid) return 'Not a moment objects';
  if (!date.isValid()) return 'Invalid date';
  return date.format(formatter);
}

export function formatNotificationTimeAgo(createdAt) {
  const now = moment();

  const diffSeconds = now.diff(createdAt, 'seconds');
  const diffMinutes = now.diff(createdAt, 'minutes');
  const diffHours = now.diff(createdAt, 'hours');
  const diffDays = now.diff(createdAt, 'days');

  if (diffSeconds < 60) return `${diffSeconds}s`;
  if (diffMinutes < 60) return `${diffMinutes}m`;
  if (diffHours < 24) return `${diffHours}h`;
  if (diffDays < 7) return `${diffDays}d`;

  return `Long ago`;
}
