import React, { Component } from 'react';
import { Link } from 'react-router';
import emoji from 'node-emoji';
import { get, orderBy, uniq, uniqBy, findLastIndex, minBy } from 'lodash';
import getEmails from 'get-emails';

import { URI_REGEXP, PROTOCOL_REGEXP, DEFAULT_PROTOCOL_PREFIX, TARGETS_PREFIXES, USERTAG_START, DEFAULT_TRUNCATE_LIMIT, EMOJI_SPLITTER, HASHTAG_START, MARKDOWN_VIEW_TICKETS_REGEXP, MARKDOWN_URI_REGEXP } from './constants';
import { EMPTY_CHAR } from '../../constants';

import { getRandomInt } from '../random';

import Hashtag from '../../shared-components/hashtag/index.jsx';
import Anchor from '../../shared-components/link-anchor/index.jsx';


const COMMENT_ITEMS = {
  TAG: userTag => (
    <span key={getRandomInt()}>
      <Link className='usertag' to={`/users/${userTag.id}`}>{userTag.tag}</Link>
      <span> </span>
    </span>
  )
};


/**
 * Truncate string
 * @param {string} str - String for truncating
 * @param {number} limit - Max length of truncated string
 * @param {boolean} strictly - Is truncate strictly?
 * @returns {string} - Truncated string
 */
export function truncateString(str, limit = DEFAULT_TRUNCATE_LIMIT, strictly = false) {
  const isStringShouldBeTruncated = str && str.length >= limit;
  if (strictly || !isStringShouldBeTruncated) return isStringShouldBeTruncated ? `${ str.substring(0, limit).trim() }...` : str;

  limit--;
  while (limit < str.length && !/\s/gi.test(str[limit])) limit++;
  const newStr = str.substring(0, limit).trim();
  return newStr !== str ? `${ str.substring(0, limit).trim() }...` : str;
}

/**
 * Get pretext that depends on first char of string
 * @param {string} string - Text that should have pretext
 * @returns {string} - Pretext
 */
export function getPretext(string) {
  return isStartsWithVowel(string) ? TARGETS_PREFIXES.VOWEL : TARGETS_PREFIXES.CONSONANT;
}

/**
 * Returns true if starts with vowel
 * @param {string} string - String for check
 * @returns {boolean} - True if starts with vowel
 */
export function isStartsWithVowel(string) {
  return (/^[aeiou]/i).test(string);
}

/**
 * Handle string: usertags, emails, urls
 * @param {string} text - Text for handling
 * @param {Array} userTags - Usertags that text should contains
 * @returns {Array} - Handled text with separated strings and items (usertags, emails, urls)
 */
export function handleString(text, userTags) {
  const textWithEmails = handleEmails(text);
  const textWithUsertags = handleUsertags(textWithEmails, userTags);
  const textWithMarkdownUris = handleMarkdownUris(textWithUsertags);
  const textWithUrls = handleUris(textWithMarkdownUris);
  const textWithHashtags = handleHashtags(textWithUrls);
  return handleNewLines(textWithHashtags);
}

/**
 * Handle string into separated emails into React objects
 * @param {string} text - String for search for emails
 * @returns {Array} Handled string with separated emails into React objects
 */
export function handleEmails(text) {
  if (!text || text.length === 0 || text === EMPTY_CHAR) return [text];

  const emails = Array.from(getEmails(text)).filter(email => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email));

  if (emails.size === 0) return [text];

  const results = [];
  let prevStartIdx = -1;
  const emailsWithPositions = getEmailsWithPositions(text, emails, prevStartIdx);

  emailsWithPositions.forEach(item => {
    const textBefore = text.substring(prevStartIdx, item.startIdx);
    if (textBefore.length > 0) results.push(textBefore);

    const analyticsData = {
      category: 'Interaction with link',
      action: `Mailing to: ${item.email}`,
    };
    results.push(<Anchor key={getRandomInt()} href={`mailto:${item.email}`} text={item.email} analyticsData={analyticsData} />);
    prevStartIdx = item.startIdx + item.email.length;
  });

  const trailer = text.substring(prevStartIdx);
  if (trailer.length > 0) results.push(trailer);
  return results;
}

/**
 * Get array of emails with positions
 * @param {string} text - Text that contains emails
 * @param {Set} emails - Set of emails that presented into text
 * @param {number} prevStartIdx - Previous start index
 * @returns {Array} - Array of emails with positions
 */
function getEmailsWithPositions(text, emails, prevStartIdx) {
  const nextPositions = [];

  emails.forEach(email => {
    let startIdx = text.indexOf(email, prevStartIdx);

    while (startIdx !== -1) {
      nextPositions.push({
        startIdx,
        email,
      });

      prevStartIdx = startIdx + email.length;
      startIdx = text.indexOf(email, prevStartIdx);
    }

    prevStartIdx = startIdx;
  });

  return orderBy(nextPositions, ['startIdx'], ['asc']);
}

/**
 * Handle usertags in text
 * @param {Array} text - Splitted text into array
 * @param {Array} userTags - List of usertags
 * @returns {Array} - Array of items that contains React objects and strings
 */
export function handleUsertags(text, userTags) {
  if (!text || text.length === 0 || !userTags || userTags.length === 0) return text;

  const results = [];
  const realUserTags = formatUserTags(userTags);

  text.forEach(item => {
    if (typeof(item) !== 'string') return results.push(item);

    let startIdx = 0;
    let prevStartIdx = -1;

    while ((prevStartIdx = item.indexOf(USERTAG_START, startIdx)) !== -1) {
      const textBefore = item.substring(startIdx, prevStartIdx);
      if (textBefore.length > 0) results.push(textBefore);

      const userTag = getUserTag(item, realUserTags, prevStartIdx);
      startIdx = userTag.startIdx;

      if (!userTag.object) { results.push(userTag.string); continue; }

      results.push(COMMENT_ITEMS.TAG(userTag.object));
    }

    const trailer = item.substring(startIdx);
    if (trailer.length > 0) results.push(trailer);
  });

  return results;
}

/**
 * Get formatted usertags
 * @param {Array} userTags - Items for formatting
 */
function formatUserTags(userTags) {
  return userTags.map(userTag => {
    return {
      tag: `@${userTag.user.username}`,
      id: userTag.user.id
    };
  });
}

/**
 * Get usertag
 * @param {string} text - Text for search usertag
 * @param {Array} realUserTags - List of usertags
 * @param {number} startIdx - Start index for search
 * @returns {{string: string, object, startIdx: number}} - Get usertag data
 */
function getUserTag(text, realUserTags, startIdx) {
  const textRegexp = /\w|\d/;
  let userTagStr = '';

  while (isUserTagAccumulating(text, startIdx, textRegexp)) userTagStr = `${userTagStr}${text[startIdx++]}`;

  return {
    string: userTagStr,
    object: realUserTags.find(tag => tag.tag === userTagStr),
    startIdx,
  };
}

/**
 * Returns true if currently performs usertag accumulating
 * @param {string} text - Text where usertag search performs
 * @param {number} startIdx - Start index for search
 * @param {RegExp} textRegexp - Regular expression that presents valid symbols for usertag
 * @returns {boolean} - True if currently performs usertag accumulating
 */
function isUserTagAccumulating(text, startIdx, textRegexp) {
  return startIdx < text.length && (text[startIdx] === USERTAG_START || textRegexp.test(text[startIdx]));
}













/**
 *
 * @param {Array} text - Array of items (strings and react objects)
 * @returns {Array} - Array that contains parsed elements
 */
export function handleMarkdownUris(text) {
  if (!text || text.length === 0) return text;

  const results = [];

  text.forEach(item => {
    if (typeof(item) !== 'string') { results.push(item); return results; }

    const markdownUris = getMarkdownUris(item);

    if (!markdownUris || markdownUris.length === 0) return results.push(item);

    let start = 0;

    markdownUris.forEach(uri => {
      if (start < uri.startIdx) results.push(item.substring(start, uri.startIdx));

      const analyticsData = {
        category: 'Interaction with link',
        action: `View Tickets: ${uri.uri}`,
      };
      results.push(<Anchor key={uri.uri} href={uri.uri} text='View Tickets' analyticsData={analyticsData} />);
      start = uri.startIdx + uri.markdown.length;
    });

    if (start < item.length) results.push(item.substring(start));
  });

  return results;
}

/**
 *
 * @param str
 * @returns {*}
 */
function getMarkdownUris(str) {
  if (typeof(str) !== 'string') return null;

  let results = [];
  const markdownUris = uniq(str.match(MARKDOWN_VIEW_TICKETS_REGEXP));
  if (markdownUris.length === 0) return null;

  markdownUris.forEach(markdown => {
    let i = -1;
    while ((i = str.indexOf(markdown, i + 1)) !== -1) results.push({ startIdx: i, markdown, uri: markdown.match(MARKDOWN_URI_REGEXP)[0] });
  });

  const sorted = results.sort((a, b) => b.uri.length - a.uri.length);
  const uniq = uniqBy(sorted, 'startIdx');
  return uniq.sort((a, b) => a.startIdx > b.startIdx);
}



/**
 * Handle URLs
 * @param {Array} text - Array of items (strings and React objects)
 * @returns {Array} - Handled URLs
 */
export function handleUris(text) {
  if (get(text, 'length', 0) === 0) return text;

  const results = [];

  text.forEach(item => {
    if (typeof(item) !== 'string') { results.push(item); return results; }

    const urls = getUrls(item);

    if (urls.size === 0) return results.push(item);

    let start = 0;
    let nextUrl = getNextUrl(item, urls);

    while (nextUrl) {
      if (start < nextUrl.idx) results.push(item.substring(start, nextUrl.idx));

      const analyticsData = {
        category: 'Interaction with link',
        action: `Redirect to ${nextUrl.url}`,
      };
      results.push(<Anchor key={getRandomInt()} href={nextUrl.href} text={nextUrl.url} analyticsData={analyticsData} />);
      start = nextUrl.idx + nextUrl.url.length;
      nextUrl = getNextUrl(item, urls, start);
    }

    if (start < item.length) results.push(item.substring(start));
  });

  return results;
}



/**
 * Get all URIs from string as Set
 * @param {string} text - String that can contain URIs
 * @returns {Set} - Founded URIs
 */
export function getUrls(text) {
  return new Set(text.match(URI_REGEXP));
}

/**
 * Get next URL
 * @param {string} str - String for check
 * @param {Array} urls - List of URLs, presented in str
 * @param {number} start - Start index for search
 * @returns {Object|null} - Returns object if URL exists from start
 */
function getNextUrl(str, urls = [], start = 0) {
  let results = [];

  urls.forEach(url => {
    let idx = str.indexOf(url, start);
    if (idx !== -1) return results.push({ url, href: handleUri(url), idx });
  });

  return results.length === 0 ? null : minBy(results, item => item.idx);
}

/**
 * Handle URI: add protocol if needed
 * @param {string} uri - URI
 * @returns {string} - Handled URI
 */
function handleUri(uri) {
  return uri.match(PROTOCOL_REGEXP) ? uri : `${DEFAULT_PROTOCOL_PREFIX}${uri}`;
}

/**
 * Format comment
 * @param {object} comment - Comment
 * @param {number} length - Required length of comment
 * @param {bool} isExpanded - Is comment expanded?
 * @returns {string} - Truncated comment
 */
export function getFormattedComment(comment, length, isExpanded = true) {
  let message = comment.message;
  return truncate(message, length, isExpanded);
}

/**
 * Truncate message to required length
 * @param {string} message - Message text
 * @param {number} length - Required length of message
 * @param {bool} isExpanded - Is message expanded?
 * @returns {string} - Truncated message
 */
export function truncate(message, length, isExpanded = true) {
  if (!message) return;

  const isExpandable = message.length > length;

  if (!isExpandable || isExpanded) return message;

  message = message.substring(0, length);

  let idx = findLastIndex(message, c => c === USERTAG_START);
  const startIdx = idx;

  if (idx === -1) return `${message.substring(0, length)}...`;

  while (/\w/.test(message[idx++])) { }

  const lastIdx = idx === message.length ? startIdx : length;

  return `${message.substring(0, lastIdx)}...`;
}



/**
 *
 * @param {Array} text -
 * @returns {Array} - Array that contains parsed hashtags
 */
export function handleHashtags(text) {
  if (!text || text.length === 0) return text;

  const results = [];

  text.forEach(item => {
    if (typeof(item) !== 'string') { results.push(item); return results; }

    const hashtags = getHashtags(item);

    if (!hashtags || hashtags.length === 0) return results.push(item);

    let start = 0;

    hashtags.forEach(hashtag => {
      if (start < hashtag.startIdx) results.push(item.substring(start, hashtag.startIdx));

      results.push(<Hashtag key={getRandomInt()} hashtag={hashtag.hashtag} />);
      start = hashtag.startIdx + hashtag.hashtag.length;
    });

    if (start < item.length) results.push(item.substring(start));
  });

  return results;
}

/**
 *
 * @param str
 * @returns {*}
 */
function getHashtags(str) {
  if (typeof(str) !== 'string') return null;

  let results = [];
  const hashtags = uniq(str.match(/#[\w|\d]+/g));

  if (hashtags.length === 0) return null;

  hashtags.forEach(hashtag => {
    let i = -1;

    while ((i = str.indexOf(hashtag, i + 1)) !== -1) results.push({ startIdx: i, hashtag });
  });

  const sorted = results.sort((a, b) => b.hashtag.length - a.hashtag.length);
  const uniq = uniqBy(sorted, 'startIdx');
  return uniq.sort((a, b) => a.startIdx > b.startIdx);
}



/**
 * Handle new lines in text
 * @param {Array} text - Array of items that contains React objects and strings
 * @returns {Array} - Array of items with handled new lines
 */
export function handleNewLines(text) {
  let results = [];

  text.forEach(item => {
    if (typeof item !== 'string') return results.push(item);
    const items = handleNewLine(item);
    results = results.concat(items);
  });

  return results;
}

/**
 * Handle new lines in string
 * @param {string} str - String that should be handled
 * @returns {Array} - List of items with handled new lines
 */
function handleNewLine(str) {
  return str.split('\n').map((item, idx, arr) => {
    const isLast = idx === arr.length - 1;
    const value = handleEmoji(item);
    return isLast ? <span key={getRandomInt()}>{value}</span> : <span key={getRandomInt()}>{value}<br /></span>;
  });
}

/**
 * Handle emojis in text
 * @param {Array} text - Array of items that should be handled
 * @returns {Array} - List of items with handled emojis
 */
export function handleEmojis(text) {
  return text.map(item => item ? handleEmoji(item) : item);
}

/**
 * Handle emojis in string
 * @param {string} str - String for handling
 * @returns {Array} - Array of items with handled emojis
 */
function handleEmoji(str) {
  const unemojified = emoji.unemojify(str);
  const emojisList = unemojified.split(EMOJI_SPLITTER);
  let isEmoji = true;

  if (emojisList.length === 1) return str;

  return emojisList.map(item => {
    const isPrevEmoji = isEmoji;

    if (!item || item.length <= 0) return item;

    const expectedEmoji = `${EMOJI_SPLITTER}${item}${EMOJI_SPLITTER}`;
    const emojified = emoji.emojify(expectedEmoji);
    isEmoji = emojified !== expectedEmoji;

    if (!isEmoji && isPrevEmoji) return item;

    return isEmoji ? <span key={getRandomInt()} className='emoji'>{emojified}</span> : `${EMOJI_SPLITTER}${item}`;
  });
}

export function isContainsYoutubeLinks(text) {
  return /(http(?:s?):\/\/)?(?:www\.)?(?:m\.)?youtu(?:be\.com|\.be)/.test(text);
}
