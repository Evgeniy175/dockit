export const EMOJI_SPLITTER = ':';
export const HASHTAG_START = '#';
export const USERTAG_START = '@';

export const NUMBERS = {
  MILLION: 1000000,
  THOUSAND: 1000
};

export const RATIO = {
  WIDTH: 16,
  HEIGHT: 9
};

export const INTERVAL_DATE_FORMAT = 'hh:mm A';
export const TIME_FORMAT = 'LT';
export const DATE_FORMAT = 'LL';

export const TARGETS_PREFIXES = {
  VOWEL: 'an',
  CONSONANT: 'a'
};

export const PROTOCOLS = 'ftp|http|https';
export const URI_REGEXP = new RegExp(`(${PROTOCOLS}):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*;/?:~-])`, 'g');
export const PROTOCOL_REGEXP = new RegExp(`(${PROTOCOLS}):`, 'g');
export const MARKDOWN_VIEW_TICKETS_REGEXP = new RegExp(`\\[View Tickets\\]\\((${PROTOCOLS}):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*;/?:~-])\\)`, 'g');
export const MARKDOWN_URI_REGEXP = new RegExp(`(${PROTOCOLS}):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*;/?:~-])`, 'g');
export const DEFAULT_PROTOCOL_PREFIX = 'https://';
export const NUMBER_REGEX = new RegExp('^(\\+|\\-)?(\\d)+.?(\\d)*', 'g');

export const DEFAULT_TRUNCATE_LIMIT = 20;

export const DEFAULT_IMAGE_W = 1000;
export const DEFAULT_IMAGE_H = 1000;

export const INVALID_DATE = 'INVALID DATE';
export const DATE_AFTER_NOW = 'NOW';

export const TITLE_PREFIX = 'DockIt';

export const PAGES = {
  ADMIN_PANEL: 'admin panel',
  SIGN_IN: 'sign in',
  SIGN_UP: 'sign up',
  CHECK_EMAIL: 'check email',
  FORGOT_PASSWORD: 'forgot password',
  RESET_PASSWORD: 'reset password',
  HOME: 'home',
  FEED: 'feed',
  UPCOMING: 'upcoming',
  DISCOVER: 'discover',
  NOTIFICATIONS: 'notifications',
  REQUESTS: 'requests',
  SEARCH: 'search',
  EVENT: 'event',
  SCHEDULE: 'schedule',
  LINK_CALENDARS: 'link calendars',
  CREATE_EVENT: 'create event',
  PRIVACY_POLICY: 'privacy policy',
  TERMS: 'terms',
  REPORTS: 'reports',
  PROFILE: 'profile',
  OTHER_PROFILE: 'other profile',
  SETTINGS: 'settings',
  SUCCESS_PASSWORD_RESET: 'success password reset',
};

export const PAGE_TITLES = {
  [PAGES.ADMIN_PANEL]: () => 'Admin Panel',
  [PAGES.SIGN_IN]: () => 'Sign In',
  [PAGES.SIGN_UP]: () => 'Sign Up',
  [PAGES.CHECK_EMAIL]: () => 'Check Email',
  [PAGES.FORGOT_PASSWORD]: () => 'Forgot Password',
  [PAGES.RESET_PASSWORD]: () => 'Reset Password',
  [PAGES.HOME]: () => 'Home',
  [PAGES.FEED]: () => 'Home',
  [PAGES.UPCOMING]: () => 'Upcoming',
  [PAGES.DISCOVER]: () => 'Discover',
  [PAGES.NOTIFICATIONS]: () => 'Notifications',
  [PAGES.REQUESTS]: () => 'Requests',
  [PAGES.SEARCH]: () => 'Search',
  [PAGES.EVENT]: title => title,
  [PAGES.SCHEDULE]: title => title,
  [PAGES.LINK_CALENDARS]: () => 'Link Calendars',
  [PAGES.CREATE_EVENT]: () => 'Create Event',
  [PAGES.PRIVACY_POLICY]: () => 'Privacy Policy',
  [PAGES.TERMS]: () => 'Terms of Service',
  [PAGES.REPORTS]: () => 'Reports',
  [PAGES.PROFILE]: name => name ? name : 'Profile',
  [PAGES.OTHER_PROFILE]: name => name,
  [PAGES.SETTINGS]: () => 'Settings',
  [PAGES.SUCCESS_PASSWORD_RESET]: () => 'Success!',
};
