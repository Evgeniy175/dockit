import { get } from 'lodash';

import { DEFAULT_IMAGE_H, DEFAULT_IMAGE_W, RATIO } from './constants';



export function imageToFormData(image, fieldName = 'image') {
  const formData = new FormData();
  formData.append(fieldName, dataUriToBlob(image));
  return formData;
}

export function dataUriToBlob(dataUri) {
  const binary = atob(dataUri.split(',')[1]);
  let array = [];
  for (let i = 0; i < binary.length; i++) array.push(binary.charCodeAt(i));
  return new Blob([new Uint8Array(array)], {type: 'image/jpeg'});
}

export function getImageUrl(model, imageKey, resolution) {
  const w = get(resolution, 'width', DEFAULT_IMAGE_W);
  const h = get(resolution, 'height', DEFAULT_IMAGE_H);
  return `https://d37ifxawxxt4jh.cloudfront.net/images/${model}/${w}x${h}_${imageKey}`;
}

export function getImageSrcSetItem(model, imageKey, width) {
  const w = width;
  const h = Math.round(width / RATIO.WIDTH * RATIO.HEIGHT);
  return `https://d37ifxawxxt4jh.cloudfront.net/images/${model}/${w}x${h}_${imageKey}`;
}
