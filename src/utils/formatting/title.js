import { TITLE_PREFIX } from './constants';
import { get } from 'lodash';

export function setTitle(postfix) {
  const postfixExists = get(postfix, 'length', 0) > 0;
  document.title = `${TITLE_PREFIX}${postfixExists ? ` - ${postfix}` : ''}`;
}
