module.exports = function(env = 'production') {
  const HtmlWebpackPlugin = require('html-webpack-plugin');
  const resolve = require('path').resolve;
  const webpack = require('webpack');
  const autoprefixer = require('autoprefixer');
  const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
  const ExtractTextPlugin = require('extract-text-webpack-plugin');
  const CopyWebpackPlugin = require('copy-webpack-plugin');
  const path = require('path');
  const constants =  require(`./config/${env}.js`);
  
  console.log('Environment:', env);
  
  const ENTRY = {
    index: './src/index.jsx',
    vendor: [
      'react',
      'react-dom',
      'bluebird',
      'cron',
      //'cropperjs', // todo not add
      'mobx',
      'mobx-react',
      'mobx-react-devtools',
      'deep-assign',
      'email-validator',
      'file-loader',
      //'get-emails', // todo not add
      'jquery-param',
      'loaders.css',
      'lodash',
      'moment',
      'moment-timezone',
      'node-emoji',
      'prop-types',
      'query-string',
      'react-bootstrap',
      'react-bootstrap-modal',
      'react-cropper',
      'react-date-picker',
      'react-ga',
      'react-images',
      'react-inlinesvg',
      'react-input-range',
      'react-radio-group',
      'react-router',
      'react-router-bootstrap',
      'react-select',
      'react-svg',
      'react-toggle',
      'react-tooltip',
      'react-youtube',
      'style-loader',
      'uuid',
      'whatwg-fetch',
    ]
  };
  
  const PLUGINS = [];
  PLUGINS.push(new ExtractTextPlugin('css/[name].css'));
  PLUGINS.push(new UglifyJSPlugin());
  PLUGINS.push(new webpack.optimize.CommonsChunkPlugin({name: 'vendor', filename: 'js/vendor.js'}));
  PLUGINS.push(new HtmlWebpackPlugin({
    inject: true,
    filename: `index.html`,
    template: `./src/index.hbs`,
    favicon: 'src/assets/images/favicon.ico'
  }));
  PLUGINS.push(new webpack.DefinePlugin(constants));
  PLUGINS.push(new CopyWebpackPlugin([{ from: 'src/static' }]));
  
  return {
    entry: ENTRY,
    output: {
      filename: 'js/[name].js',
      // the output bundle
      
      path: resolve(__dirname, 'dist'),
      
      publicPath: '/'
      // necessary for HMR to know where to load the hot update chunks
    },
    
    devtool: 'inline-source-map',
    
    devServer: {
      contentBase: resolve(__dirname, 'dist'),
      // match the output path

      publicPath: '/',
      // match the output `publicPath`

      historyApiFallback: true
      // fix the react router problem for missing pages
    },
    
    module: {
      loaders: [
        {
          test: /.*\.(gif|png|jpe?g|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                query: {
                  name:'assets/[name].[ext]'
                }
              }
            },
            {
              loader: 'image-webpack-loader',
              options: {
                query: {
                  mozjpeg: {
                    progressive: true,
                  },
                  gifsicle: {
                    interlaced: true,
                  },
                  optipng: {
                    optimizationLevel: 7,
                  }
                }
              }
            }
          ]
        },
        {
          test: /\.(hbs|html)$/,
          loader: 'handlebars-template-loader'
        },
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract({ use: ['css-loader', 'sass-loader'].join('!')})
        },
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
        },
        {
          test: /\.(woff2?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file-loader',
        }
      ],
    },
    
    plugins: PLUGINS,
    
    resolve: {
      extensions: ['.js', '.scss']
    }
  };
};